<?php
require('configuration.php');
$config = new JConfig();
// db connection
$con = mysql_connect($config->host,$config->user,$config->password);
			
// select db
mysql_select_db($config->db);	

date_default_timezone_set("US/Eastern");
error_reporting(1);

ini_set('memory_limit','1536M'); // 1.5 GB
ini_set('max_execution_time', 200000); // 5 hours
ini_set('max_input_time', 200000); // 5 hours
date_default_timezone_set("US/Eastern");

$url = "http://partners2.agestanet.it/data/agenzia_xml/gfed4d84fnjf85r4r5f.xml";

$ch = curl_init($url);
curl_setopt($ch, CURLOPT_HEADER, false);
curl_setopt($ch, CURLOPT_USERAGENT,'Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US; rv:1.8.1.13) Gecko/20080311 Firefox/2.0.0.13');
curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
curl_setopt($ch, CURLOPT_BINARYTRANSFER, true);
curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
curl_setopt($ch, CURLOPT_CONNECTTIMEOUT ,0);
curl_setopt($ch, CURLOPT_TIMEOUT, 10000);
$xmlData = curl_exec($ch);
$redirectURL = curl_getinfo($ch,CURLINFO_EFFECTIVE_URL );
curl_close($ch);

$rows = simplexml_load_string($xmlData);
$row_value = $rows->immobili;

if (!empty($row_value))
{
	$deleteQuery = "TRUNCATE cp_property";
	mysql_query($deleteQuery);

	$totalchunk = 0;
	$count = 0;
	$insertArr = array();
	foreach($row_value as $propertyList)
	{
		foreach($propertyList as $property){
			$immobile = $property['id'];

			if($immobile != ''){
				$images = array();
				$i = '';
				for($i = 1; $i < 20; $i++){
					$urlvar = '';
					$url = '';
					$url = 'url'.$i;
					$urlvar = $property->$url;
					if($urlvar != ''){
						$images[] = $urlvar;
					}
				}

				$mainImage = $images[0];
				$images = implode(',',$images);

				//Property Table						
				$rif = $property->rif;
				$id_agenzia = $property->id_agenzia;
				$contratto = $property->contratto;	

				$date = str_replace('/', '-', $property->data);
				$data = date('Y-m-d', strtotime($date));

				$date1 = str_replace('/', '-', $property->data_upd);
				$data_upd = date('Y-m-d', strtotime($date1));

				$cod_regione = $property->cod_regione;	
				$regione = $property->regione;
				$cod_provincia = $property->cod_provincia;
				$provincia = $property->provincia;
				$sigla_provincia = $property->sigla_provincia;
				$cod_comune = $property->cod_comune;
				$comune = $property->comune;
				$cod_istat = $property->cod_istat;
				$cod_zona_comune = $property->cod_zona_comune;
				$localita = $property->localita;
				$ubicazione = $property->ubicazione;
				$indirizzo = $property->indirizzo;						
				$latitudine = $property->latitudine;
				$longitudine = $property->longitudine;
				$mappa_visibile = $property->mappa_visibile;
				$mappa_immobile_visibile = $property->mappa_immobile_visibile;
				$cod_tipologia = $property->cod_tipologia;
				//$tipologia = $property->tipologia;
				$cod_categoria = $property->cod_categoria;
				$prezzo = $property->prezzo;
				$trattativa_riservata = $property->trattativa_riservata;
				$mq = $property->mq;
				$vani = $property->vani;
				$camere = $property->camere;
				$bagni = $property->bagni;
				$cod_condizioni = $property->cod_condizioni;
				//$condizioni = $property->condizioni;
				$cod_riscaldamento = $property->cod_riscaldamento;
				//$riscaldamento = $property->riscaldamento;						
				$classe_energetica = $property->classe_energetica;
				$epi_um = $property->epi_um;
				$classe_EnEpglRen = $property->classe_EnEpglRen;
				$classe_EnEpglNRen = $property->classe_EnEpglNRen;
				$classe_EnEpiInv = $property->classe_EnEpiInv;
				$classe_EnEpeEst = $property->classe_EnEpeEst;
				$classe_EnPefInverno = $property->classe_EnPefInverno;
				$classe_EnPefEstate = $property->classe_EnPefEstate;
				$classe_EnQuasiZero = $property->classe_EnQuasiZero;						
				$cod_cucina = $property->cod_cucina;
				//$cucina = $property->cucina;
				$garage = $property->garage;
				$ascensore = $property->ascensore;
				$arredato = $property->arredato;
				$condizionatore = $property->condizionatore;
				$giardino = $property->giardino;
				$giardino_tipo = $property->giardino_tipo;
				$postoauto = $property->postoauto;
				$balcone = $property->balcone;
				$terrazza = $property->terrazza;						
				$mansarda = $property->mansarda;
				$cantina = $property->cantina;
				$piano = $property->piano;
				$piani_totali = $property->piani_totali;
				$distanza_tipo = $property->distanza_tipo;
				$distanza = $property->distanza;
				$evidenza = $property->evidenza;
				$sitoweb = $property->sitoweb;
				$prestigio = $property->prestigio;						
				$rent_tobuy = $property->rent_tobuy;
				$permuta = $property->permuta;
				$asta = $property->asta;
				$investimento = $property->investimento;
				$tipo_incarico = $property->tipo_incarico;
				$anno_immobile = $property->anno_immobile;
				$livelli_immobile = $property->livelli_immobile;						
				$unita_immobile = $property->unita_immobile;
				$vista_mare = $property->vista_mare;
				$spese_condominiali = $property->spese_condominiali;
				$email = $property->email;
				if($property->civico != ""){ $civico = $property->civico; }else{ $civico = ''; }
				if($property->epi != ""){ $epi = $property->epi; }else{ $epi = ''; }
				if($property->garage_mq != ""){ $garage_mq = $property->garage_mq; }else{ $garage_mq = ''; }
				if($property->giardino_mq != ""){ $giardino_mq = $property->giardino_mq; }else{ $giardino_mq = ''; }
				if($property->postoauto_mq != ""){ $postoauto_mq = $property->postoauto_mq; }else{ $postoauto_mq = ''; }
				if($property->balcone_mq != ""){ $balcone_mq = $property->balcone_mq; }else{ $balcone_mq = ''; }
				if($property->terrazza_mq != ""){ $terrazza_mq = $property->terrazza_mq; }else{ $terrazza_mq = ''; }
				if($property->mansarda_mq != ""){ $mansarda_mq = $property->mansarda_mq; }else{ $mansarda_mq = ''; }
				if($property->cantina_mq != ""){ $cantina_mq = $property->cantina_mq; }else{ $cantina_mq = ''; }
				if($property->agente != ""){ $agente = $property->agente; }else{ $agente = ''; }
				if($property->note_riservate != ""){ $note_riservate = $property->note_riservate; }else{ $note_riservate = ''; }
				if($property->note_condivise != ""){ $note_condivise = $property->note_condivise; }else{ $note_condivise = ''; }
				if($property->spese_condominiali_note != ""){ $spese_condominiali_note = $property->spese_condominiali_note; }else{ $spese_condominiali_note = ''; }

				$testo 	= mysql_real_escape_string($property->testo);	
				$testo_eng 	= mysql_real_escape_string($property->testo_eng);
				$testo_ted 	= mysql_real_escape_string($property->testo_ted);
				$testo_fra 	= mysql_real_escape_string($property->testo_fra);
				$testo_rus 	= mysql_real_escape_string($property->testo_rus);
				
				$insertArr[] = '(
					"'. $immobile .'",
					"'. $id_agenzia .'",
					"'. $rif .'",
					"'. $contratto .'",
					"'. $data .'",
					"'. $data_upd .'",
					"'. $cod_regione .'",
					"'. $regione .'",	
					"'. $cod_provincia .'",	
					"'. $provincia .'",	
					"'. $sigla_provincia .'",
					"'. $cod_comune .'",	
					"'. $comune .'",	
					"'. $cod_istat .'",
					"'. $cod_zona_comune .'",
					"'. $localita .'",
					"'. $ubicazione .'",
					"'. $indirizzo .'",	
					"'. $civico .'",
					"'. $latitudine .'",
					"'. $longitudine .'",
					"'. $mappa_visibile .'",
					"'. $mappa_immobile_visibile .'",
					"'. $cod_tipologia .'",
					"'. $cod_categoria .'",
					"'. $prezzo .'",					
					"'. $trattativa_riservata .'",
					"'. $mq .'",
					"'. $vani .'",
					"'. $camere .'",
					"'. $bagni .'",
					"'. $cod_condizioni .'",
					"'. $cod_riscaldamento .'",
					"'. $classe_energetica .'",
					"'. $epi .'",
					"'. $epi_um .'",
					"'. $classe_EnEpglRen .'",
					"'. $classe_EnEpglNRen .'",
					"'. $classe_EnEpiInv .'",						
					"'. $classe_EnEpeEst .'",
					"'. $classe_EnPefInverno .'",
					"'. $classe_EnPefEstate .'",
					"'. $classe_EnQuasiZero .'",
					"'. $cod_cucina .'",
					"'. $garage .'",
					"'. $garage_mq .'",
					"'. $ascensore .'",
					"'. $arredato .'",
					"'. $condizionatore .'",						 
					"'. $giardino .'",
					"'. $giardino_tipo .'",
					"'. $postoauto .'",
					"'. $giardino_mq .'",
					"'. $postoauto_mq .'",
					"'. $balcone .'",
					"'. $balcone_mq .'",
					"'. $terrazza .'",
					"'. $terrazza_mq .'",
					"'. $mansarda .'",
					"'. $mansarda_mq .'",
					"'. $cantina .'",
					"'. $cantina_mq .'",
					"'. $piano .'",
					"'. $piani_totali .'",
					"'. $distanza_tipo .'",
					"'. $distanza .'",
					"'. $evidenza .'",
					"'. $sitoweb .'",
					"'. $agente .'",
					"'. $prestigio .'",
					"'. $rent_tobuy .'",
					"'. $permuta .'",
					"'. $asta .'",
					"'. $investimento .'",
					"'. $tipo_incarico .'",
					"'. $note_riservate .'",
					"'. $note_condivise .'",
					"'. $anno_immobile .'",
					"'. $livelli_immobile .'",
					"'. $unita_immobile .'",
					"'. $vista_mare .'",
					"'. $spese_condominiali .'",
					"'. $spese_condominiali_note .'",
					"'. $email .'",
					"'. $testo .'",
					"'. $testo_eng .'",
					"'. $testo_ted .'",
					"'. $testo_fra .'",
					"'. $testo_rus .'",
					"'. $mainImage .'",
					"'. $images .'"
				)';
			}
			$count++;
			$totalchunk++;

			if($count == 10)
			{
				$insertQuery = 'INSERT INTO cp_property (immobile, id_agenzia, rif, contratto, data, data_upd, cod_regione, regione, cod_provincia, provincia, sigla_provincia, cod_comune, comune, cod_istat, cod_zona_comune, localita, ubicazione, indirizzo, civico, latitudine, longitudine, mappa_visibile, mappa_immobile_visibile, cod_tipologia, cod_categoria, prezzo, trattativa_riservata, mq, vani, camere, bagni, cod_condizioni, cod_riscaldamento, classe_energetica, epi, epi_um, classe_EnEpglRen, classe_EnEpglNRen, classe_EnEpiInv, classe_EnEpeEst, classe_EnPefInverno, classe_EnPefEstate, classe_EnQuasiZero, cod_cucina, garage, garage_mq, ascensore, arredato, condizionatore, giardino, giardino_tipo, giardino_mq, postoauto, postoauto_mq, balcone, balcone_mq, terrazza, terrazza_mq, mansarda, mansarda_mq, cantina, cantina_mq, piano, piani_totali, distanza_tipo, distanza, evidenza, sitoweb, agente, prestigio, rent_tobuy, permuta, asta, investimento, tipo_incarico, note_riservate, note_condivise, anno_immobile, livelli_immobile, unita_immobile, vista_mare, spese_condominiali, spese_condominiali_note, email, testo, testo_eng, testo_ted, testo_fra, testo_rus, mainImage, images) VALUES '.implode(",", $insertArr).';';
				mysql_query($insertQuery);
				
				$count = 0;
				$insertArr = array();
			}
		}
	}
	if($count < '10')
	{
		$insertQuery = 'INSERT INTO cp_property (immobile, id_agenzia, rif, contratto, data, data_upd, cod_regione, regione, cod_provincia, provincia, sigla_provincia, cod_comune, comune, cod_istat, cod_zona_comune, localita, ubicazione, indirizzo, civico, latitudine, longitudine, mappa_visibile, mappa_immobile_visibile, cod_tipologia, cod_categoria, prezzo, trattativa_riservata, mq, vani, camere, bagni, cod_condizioni, cod_riscaldamento, classe_energetica, epi, epi_um, classe_EnEpglRen, classe_EnEpglNRen, classe_EnEpiInv, classe_EnEpeEst, classe_EnPefInverno, classe_EnPefEstate, classe_EnQuasiZero, cod_cucina, garage, garage_mq, ascensore, arredato, condizionatore, giardino, giardino_tipo, giardino_mq, postoauto, postoauto_mq, balcone, balcone_mq, terrazza, terrazza_mq, mansarda, mansarda_mq, cantina, cantina_mq, piano, piani_totali, distanza_tipo, distanza, evidenza, sitoweb, agente, prestigio, rent_tobuy, permuta, asta, investimento, tipo_incarico, note_riservate, note_condivise, anno_immobile, livelli_immobile, unita_immobile, vista_mare, spese_condominiali, spese_condominiali_note, email, testo, testo_eng, testo_ted, testo_fra, testo_rus, mainImage, images) VALUES '.implode(" , ", $insertArr).';';
		mysql_query($insertQuery);
	}
	echo 'Total Data '.$totalchunk.' import successfully.';
	exit;
}else{
	echo 'Url not get Property.';
	exit;
}
?>