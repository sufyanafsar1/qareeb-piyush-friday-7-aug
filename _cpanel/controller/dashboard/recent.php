<?php
class ControllerDashboardRecent extends Controller {
	public function index() {
		$this->load->language('dashboard/recent');

		$data['heading_title'] = $this->language->get('heading_title');

		$data['text_no_results'] = $this->language->get('text_no_results');

		$data['column_order_id'] = $this->language->get('column_order_id');
		$data['column_customer'] = $this->language->get('column_customer');
		$data['column_status'] = $this->language->get('column_status');
		$data['column_date_added'] = $this->language->get('column_date_added');
		$data['column_total'] = $this->language->get('column_total');
		$data['column_action'] = $this->language->get('column_action');

		$data['button_view'] = $this->language->get('button_view');

		$data['token'] = $this->session->data['token'];

		// Last 5 Orders
		$data['orders'] = array();

		$filter_data = array(
			'sort'  => 'o.order_id',
			'order' => 'DESC',
			'start' => 0,
			'limit' => 10
		);

		$results = $this->model_sale_order->getOrders($filter_data);

		foreach ($results as $result) {
			
			$Total1 = $this->model_sale_order->getOrderTotals1($result['order_id']);
			$Total2 = $this->model_sale_order->getOrderTotals2($result['order_id']);
			
			$date_added = date("Y-m-d", strtotime($result['date_added']));				
			$date = '2018-11-02';
	
			$beforDate = 0;
			$afterDate = 0;
			if($result['date_added'] != '0000-00-00 00:00:00'){
				if($date_added <= $date){
					$Final_Total = ($Total1 + $Total2);				
				} else {
					$Final_Total = $Total1;		
				}				
			} else {
				$Final_Total = ($Total1 + $Total2);
			}
			
			$order_info = $this->model_sale_order->getOrder($result['order_id']);
			$s_name = $this->model_sale_order->getsellername($result['order_id']);
			foreach ($s_name as $name) {
				 $sellername = $name['firstname'];
			}
			
			$data['orders'][] = array(
				'order_id'   => $result['order_id'],
				'sellername' => $sellername,
				'payment_method'=> $order_info['payment_method'],
                'shipping_city'=> $order_info['shipping_city'],
				'customer'   => $result['customer'],
				'status'     => $result['status'],
				'date_added' => date($this->language->get('datetime_format'), strtotime($result['date_added'])),
				'total'      => $this->currency->format($Final_Total, $result['currency_code'], $result['currency_value']),
				'view'       => $this->url->link('sale/order/info', 'token=' . $this->session->data['token'] . '&order_id=' . $result['order_id'], 'SSL'),
			);
		}
		return $this->load->view('dashboard/recent.tpl', $data);
	}
}