<?php
class ControllerDashboardUpc extends Controller {
	public function index() {
		$this->load->language('dashboard/upc');

		$data['heading_title'] = $this->language->get('heading_title');

		$data['text_no_results'] = $this->language->get('text_no_results');


		$data['column_product'] = $this->language->get('column_product');
		$data['column_upc_current'] = $this->language->get('column_upc_current');
		$data['column_upc_new'] = $this->language->get('column_upc_new');
		$data['column_status'] = $this->language->get('column_status');
		$data['column_action'] = $this->language->get('column_action');
		$data['column_date_added'] = $this->language->get('column_date_added');
		$data['button_view'] = $this->language->get('button_view');

		$data['token'] = $this->session->data['token'];


		$data['products'] = array();

		$this->load->model('catalog/product');


		$results = $this->model_catalog_product->getPendingBarCodes();

		foreach ($results as $result) {
			$data['products'][] = array(
				'upc_id'   => $result['id'],
				'name'   => $result['name'],
				'upc_current'   => $result['upc_current'],
				'upc_new'     => $result['upc'],
				'status'     => $result['status'],
				'date_added' => date($this->language->get('datetime_format'), strtotime($result['date_added']))
				//'view'       => $this->url->link('sale/order/info', 'token=' . $this->session->data['token'] . '&order_id=' . $result['order_id'], 'SSL'),
			);
		}

		return $this->load->view('dashboard/upc.tpl', $data);
	}


	public function acceptBarCode(){

		$this->load->model('catalog/product');
		$data = $this->request->post;
		$responceData = $this->model_catalog_product->acceptBarCode($data);

		print_r(json_encode($responceData));
	}

	public function rejectBarCode(){
		$this->load->model('catalog/product');
		$data = $this->request->post;

		$responceData = $this->model_catalog_product->rejectBarCode($data);

		print_r(json_encode($responceData));
	}
}