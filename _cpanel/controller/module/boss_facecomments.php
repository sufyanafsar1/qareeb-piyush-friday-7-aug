<?php
class ControllerModuleBossFaceComments extends Controller {
	private $error = array();
	
	public function index() {   
		$this->load->language('module/boss_facecomments');

		$this->document->setTitle($this->language->get('heading_title'));
		
		$this->load->model('setting/setting');

		if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validate()) {
			$this->model_setting_setting->editSetting('boss_facecomments', $this->request->post);

			$this->session->data['success'] = $this->language->get('text_success');

			$this->response->redirect($this->url->link('extension/module', 'token=' . $this->session->data['token'], 'SSL'));
		}
				
		$data['heading_title'] = $this->language->get('heading_title') . ' ' . $this->version;
		$data['text_edit'] = $this->language->get('text_edit');
		
		$data['text_enabled'] = $this->language->get('text_enabled');
		$data['text_disabled'] = $this->language->get('text_disabled');
		$data['text_light'] = $this->language->get('text_light');
		$data['text_dark'] = $this->language->get('text_dark');
		$data['text_social'] = $this->language->get('text_social');
		$data['text_reverse_time'] = $this->language->get('text_reverse_time');
		$data['text_time'] = $this->language->get('text_time');
		
		$data['entry_name'] = $this->language->get('entry_name');
		$data['entry_app_id'] = $this->language->get('entry_app_id');
		$data['entry_color_scheme'] = $this->language->get('entry_color_scheme');
		$data['entry_num_posts'] = $this->language->get('entry_num_posts');
		$data['entry_order_by'] = $this->language->get('entry_order_by');
		$data['entry_status'] = $this->language->get('entry_status');
		
		$data['help_app_id'] = $this->language->get('help_app_id');
		$data['help_color_scheme'] = $this->language->get('help_color_scheme');
		$data['help_num_posts'] = $this->language->get('help_num_posts');
		$data['help_order_by'] = $this->language->get('help_order_by');
		
		$data['button_save'] = $this->language->get('button_save');
		$data['button_cancel'] = $this->language->get('button_cancel');

		if (isset($this->error['warning'])) {
			$data['error_warning'] = $this->error['warning'];
		} else {
			$data['error_warning'] = '';
		}
		
		if (isset($this->error['app_id'])) {
			$data['error_app_id'] = $this->error['app_id'];
		} else {
			$data['error_app_id'] = '';
		}	

		if (isset($this->error['app_id'])) {
			$data['error_app_id'] = $this->error['app_id'];
		} else {
			$data['error_app_id'] = '';
		}

		if (isset($this->error['num_posts'])) {
			$data['error_num_posts'] = $this->error['num_posts'];
		} else {
			$data['error_num_posts'] = '';
		}
        
  		$data['breadcrumbs'] = array();

   		$data['breadcrumbs'][] = array(
       		'text'      => $this->language->get('text_home'),
			'href'      => $this->url->link('common/home', 'token=' . $this->session->data['token'], 'SSL')
   		);

   		$data['breadcrumbs'][] = array(
       		'text'      => $this->language->get('text_module'),
			'href'      => $this->url->link('extension/module', 'token=' . $this->session->data['token'], 'SSL')
   		);
		
   		$data['breadcrumbs'][] = array(
       		'text'      => $this->language->get('heading_title'),
			'href'      => $this->url->link('module/boss_facecomments', 'token=' . $this->session->data['token'], 'SSL')
   		);
		
		$data['action'] = $this->url->link('module/boss_facecomments', 'token=' . $this->session->data['token'], 'SSL');

		$data['cancel'] = $this->url->link('extension/module', 'token=' . $this->session->data['token'], 'SSL');

		
		if (isset($this->request->post['boss_facecomments_status'])) {
			$data['boss_facecomments_status'] = $this->request->post['boss_facecomments_status'];
		} else {
			$data['boss_facecomments_status'] = $this->config->get('boss_facecomments_status');
		}
		
		if (isset($this->request->post['boss_facecomments_app_id'])) {
			$data['boss_facecomments_app_id'] = $this->request->post['boss_facecomments_app_id'];
		} else {
			$data['boss_facecomments_app_id'] = $this->config->get('boss_facecomments_app_id');
		}
		
		if (isset($this->request->post['boss_facecomments_color_scheme'])) {
			$data['boss_facecomments_color_scheme'] = $this->request->post['boss_facecomments_color_scheme'];
		} else {
			$data['boss_facecomments_color_scheme'] = $this->config->get('boss_facecomments_color_scheme');
		}
		
		if (isset($this->request->post['boss_facecomments_num_posts'])) {
			$data['boss_facecomments_num_posts'] = $this->request->post['boss_facecomments_num_posts'];
		} else {
			$data['boss_facecomments_num_posts'] = $this->config->get('boss_facecomments_num_posts');
		}
		
		if (isset($this->request->post['boss_facecomments_order_by'])) {
			$data['boss_facecomments_order_by'] = $this->request->post['boss_facecomments_order_by'];
		} else {
			$data['boss_facecomments_order_by'] = $this->config->get('boss_facecomments_order_by');
		}

		$data['header'] = $this->load->controller('common/header');
		$data['column_left'] = $this->load->controller('common/column_left');
		$data['footer'] = $this->load->controller('common/footer');

		$this->response->setOutput($this->load->view('module/boss_facecomments.tpl', $data));
	}
	
	private function validate() {
		if (!$this->user->hasPermission('modify', 'module/boss_facecomments')) {
			$this->error['warning'] = $this->language->get('error_permission');
		}
		
		if (utf8_strlen($this->request->post['boss_facecomments_app_id']) < 3) {
			$this->error['app_id'] = $this->language->get('error_app_id');
		}

		if (utf8_strlen($this->request->post['boss_facecomments_num_posts']) < 1 || !is_numeric($this->request->post['boss_facecomments_num_posts']) || $this->request->post['boss_facecomments_num_posts'] < 1 ) {
			$this->error['num_posts'] = $this->language->get('error_num_posts');
		}
		
		return !$this->error;	
	}	
}