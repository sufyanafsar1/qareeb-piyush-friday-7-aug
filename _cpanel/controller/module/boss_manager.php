<?php
class ControllerModuleBossManager extends Controller {
	private $error = array(); 

	public function index() {   
		$this->language->load('module/boss_manager');

		$this->document->setTitle($this->language->get('heading_title'));

		$this->load->model('setting/setting');
		$this->load->model('tool/image');
		
		$this->document->addStyle('view/stylesheet/bossthemes/boss_manager.css');
		if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validate()) {
			
			$this->model_setting_setting->editSetting('boss_manager', $this->request->post);		
			$this->saveXML($this->request->post['xml'],isset($this->request->post['custom_color'])?$this->request->post['custom_color']:'');
			$this->saveXMLFont($this->request->post['xml_font'],isset($this->request->post['custom_font'])?$this->request->post['custom_font']:'');
			$this->session->data['success'] = $this->language->get('text_success');

			$this->response->redirect($this->url->link('module/boss_manager', 'token=' . $this->session->data['token'], 'SSL'));
		}

		$data['heading_title'] = $this->language->get('heading_title');
		
		$data['text_edit'] = $this->language->get('text_edit');

		$data['text_enabled'] = $this->language->get('text_enabled');
		$data['text_disabled'] = $this->language->get('text_disabled');
		$data['text_content_top'] = $this->language->get('text_content_top');
		$data['text_content_bottom'] = $this->language->get('text_content_bottom');		
		$data['text_column_left'] = $this->language->get('text_column_left');
		$data['text_column_right'] = $this->language->get('text_column_right');

		$data['entry_layout'] = $this->language->get('entry_layout');
		$data['entry_position'] = $this->language->get('entry_position');
		$data['entry_status'] = $this->language->get('entry_status');
		$data['entry_sort_order'] = $this->language->get('entry_sort_order');

		$data['button_save'] = $this->language->get('button_save');
		$data['button_cancel'] = $this->language->get('button_cancel');
		$data['button_add_module'] = $this->language->get('button_add_module');
		$data['button_remove'] = $this->language->get('button_remove');
		
		$data['arrstatus'] = array(
			"0" => $this->language->get('text_disabled'),
			"1" => $this->language->get('text_enabled')
		);
		
		$data['arrshow'] = array(
			"use_tab" => $this->language->get('text_tab'),
			"use_accordion" => $this->language->get('text_accordion')
		);
		$data['arrview'] = array(
			"grid" => $this->language->get('text_grid'),
			"list" => $this->language->get('text_list'),
			"both_list" => $this->language->get('text_both_list'),
			"both_grid" => $this->language->get('text_both_grid')
		);
		$data['arrusemenu'] = array(
			"default" => $this->language->get('text_default'),
			"megamenu" => $this->language->get('text_megamenu'),			
		);
		$data['arrperrow'] = array(
			"1" => 1,
			"2" => 2,			
			"3" => 3,			
			"4" => 4,			
			"5" => 5,			
			"6" => 6,			
		);
		$data['arrstyle'] = array(
			"1" => 'Homepage default',						
		);
		$data['token'] = $this->session->data['token'];
		if (isset($this->session->data['success'])) {
			$data['success'] = $this->session->data['success'];

			unset($this->session->data['success']);
		} else {
			$data['success'] = '';
		}

		if (isset($this->error['warning'])) {
			$data['error_warning'] = $this->error['warning'];
		} else {
			$data['error_warning'] = '';
		}

		$data['breadcrumbs'] = array();

		$data['breadcrumbs'][] = array(
			'text'      => $this->language->get('text_home'),
			'href'      => $this->url->link('common/dashboard', 'token=' . $this->session->data['token'], 'SSL'),
			'separator' => false
		);

		$data['breadcrumbs'][] = array(
			'text'      => $this->language->get('text_module'),
			'href'      => $this->url->link('extension/module', 'token=' . $this->session->data['token'], 'SSL'),
			'separator' => ' :: '
		);

		$data['breadcrumbs'][] = array(
			'text'      => $this->language->get('heading_title'),
			'href'      => $this->url->link('module/boss_manager', 'token=' . $this->session->data['token'], 'SSL'),
			'separator' => ' :: '
		);

		$data['action'] = $this->url->link('module/boss_manager', 'token=' . $this->session->data['token'], 'SSL');

		$data['cancel'] = $this->url->link('extension/module', 'token=' . $this->session->data['token'], 'SSL');

		$data['boss_manager'] = array();
		
		$data['option'] = array();
		$data['layout'] = array();
		
		$data['footer_about'] = array();
		
		
		
		$data['placeholder'] = $this->model_tool_image->resize('no_image.png', 100, 100);
		
		
		$data['header_mobile'] = array();
		if (isset($this->request->post['boss_manager_header_mobile'])) {
			$data['header_mobile'] = $this->request->post['boss_manager_header_mobile'];
		} elseif ($this->config->get('boss_manager_header_mobile')) { 
			$data['header_mobile'] = $this->config->get('boss_manager_header_mobile');
		}
		
		if (isset($this->request->post['boss_manager_footer_about'])) {
			$data['footer_about'] = $this->request->post['boss_manager_footer_about'];
		} elseif ($this->config->get('boss_manager_footer_about')) { 
			$data['footer_about'] = $this->config->get('boss_manager_footer_about');
		}
		
		$footer_about = $data['footer_about'];
		
		if (isset($footer_about['image_link']) && file_exists(DIR_IMAGE . $footer_about['image_link'])) {
			$data['about_image'] = $this->model_tool_image->resize($footer_about['image_link'], 100, 100);
		} else {
			$data['about_image'] = $this->model_tool_image->resize('no_image.png', 100, 100);
		}
		$data['footer_social'] = array();
		
		if (isset($this->request->post['boss_manager_footer_social'])) {
			$data['footer_social'] = $this->request->post['boss_manager_footer_social'];
		} elseif ($this->config->get('boss_manager_footer_social')) { 
			$data['footer_social'] = $this->config->get('boss_manager_footer_social');
		}	
		
		
		$data['footer_powered'] = array();
		
		if (isset($this->request->post['boss_manager_footer_powered'])) {
			$data['footer_powered'] = $this->request->post['boss_manager_footer_powered'];
		} elseif ($this->config->get('boss_manager_footer_powered')) { 
			$data['footer_powered'] = $this->config->get('boss_manager_footer_powered');
		}
		
		$boss_manager = array();

		if (isset($this->request->post['boss_manager'])) {
			$boss_manager = $this->request->post['boss_manager'];
		} elseif ($this->config->get('boss_manager')) { 
			$boss_manager = $this->config->get('boss_manager');
		}

		$data['boss_manager'] = $boss_manager;
		
		if(!empty($boss_manager)){
			$data['option'] = $boss_manager['option'];
			$data['status'] = $boss_manager['status'];
			$data['layout'] = $boss_manager['layout'];
			$data['other'] = $boss_manager['other'];
			$data['color'] = $boss_manager['color'];
		}
		
		$this->load->model('localisation/language');
		
		$data['languages'] = $this->model_localisation_language->getLanguages();
		
		$data['header'] = $this->load->controller('common/header');
		$data['column_left'] = $this->load->controller('common/column_left');
		$data['footer'] = $this->load->controller('common/footer');

		$this->response->setOutput($this->load->view('module/boss_manager.tpl', $data));

	}
	protected function saveXML($groups,$customs) {
		//echo "<pre>"; print_r($customs); echo "</pre>";die();
		$xml = new DOMDocument('1.0','UTF-8');

		//create item_setting
		$root = $xml->createElement('items_setting');
		$xml->appendChild($root);	
		
		foreach($groups as $group){
			//create groups
			$groups_xml = $xml->createElement('groups');
			$root->appendChild($groups_xml);
			
			//create title		
			$title = $xml->createElement('title');
			$groups_xml->appendChild($title);
			//add value
			$title->appendChild($xml->createTextNode($group['title'][0]));
			if(isset($group['text'])){
				for($i=0; $i< count($group['text']) ; $i++){ 
					//create item
					$item = $xml->createElement('item');
					$groups_xml->appendChild($item);		
					
					//create text		
					$text = $xml->createElement('text');
					$item->appendChild($text);
					//add value
					$text->appendChild($xml->createTextNode($group['text'][$i]));
					
					//create name		
					$name = $xml->createElement('name');
					$item->appendChild($name);
					//add value
					$name->appendChild($xml->createTextNode($group['name'][$i]));
					
					//create class		
					$class = $xml->createElement('class');
					$item->appendChild($class);
					//add value
					$class->appendChild($xml->createTextNode($group['class'][$i]));
					
					//create value		
					$value = $xml->createElement('value');
					$item->appendChild($value);
					//add value
					$value->appendChild($xml->createTextNode($group['value'][$i]));
					
					//create style		
					$style = $xml->createElement('style');
					$item->appendChild($style);
					//add value
					$style->appendChild($xml->createTextNode($group['style'][$i]));
				}
			}
		}
		
		//create customs
		$customs_xml = $xml->createElement('customs');
		$root->appendChild($customs_xml);
		if(!empty($customs)){
			foreach($customs as $custom){ 		
				//create item
				$item = $xml->createElement('item');
				$customs_xml->appendChild($item);		
				
				//create text		
				$text = $xml->createElement('text');
				$item->appendChild($text);
				//add value
				$text->appendChild($xml->createTextNode($custom['text']));
				
				//create class		
				$class = $xml->createElement('class');
				$item->appendChild($class);
				//add value
				$class->appendChild($xml->createTextNode($custom['class']));
				
				//create value		
				$value = $xml->createElement('value');
				$item->appendChild($value);
				//add value
				$value->appendChild($xml->createTextNode($custom['value']));
				
				//create important		
				$important = $xml->createElement('important');
				$item->appendChild($important);
				//add value
				$important->appendChild($xml->createTextNode($custom['important']));
				
				//create style		
				$style = $xml->createElement('style');
				$item->appendChild($style);
				//add value
				$style->appendChild($xml->createTextNode($custom['style']));
			}
		}
		//nice output
		$xml->formatOutput = true;
		$xml->save("../config_xml/color_setting.xml"); 
	}
	protected function saveXMLFont($groups,$customs) {
		//echo "<pre>"; print_r($customs); echo "</pre>";die();
		$xml = new DOMDocument('1.0','UTF-8');

		//create item_setting
		$root = $xml->createElement('items_setting');
		$xml->appendChild($root);	
		
		foreach($groups as $group){
			//create groups
			$groups_xml = $xml->createElement('groups');
			$root->appendChild($groups_xml);
			
			//create title		
			$title = $xml->createElement('title');
			$groups_xml->appendChild($title);
			//add value
			$title->appendChild($xml->createTextNode($group['title'][0]));
			if(isset($group['text'])){
				for($i=0; $i< count($group['text']) ; $i++){ 
					//create item
					$item = $xml->createElement('item');
					$groups_xml->appendChild($item);		
					
					//create text		
					$text = $xml->createElement('text');
					$item->appendChild($text);
					//add value
					$text->appendChild($xml->createTextNode($group['text'][$i]));
					
					//create style		
					$style = $xml->createElement('style');
					$item->appendChild($style);
					//add value
					$style->appendChild($xml->createTextNode($group['style'][$i]));
					
					//create size		
					$size = $xml->createElement('size');
					$item->appendChild($size);
					//add value
					$size->appendChild($xml->createTextNode($group['size'][$i]));
					
					//create weight		
					$weight = $xml->createElement('weight');
					$item->appendChild($weight);
					//add value
					$weight->appendChild($xml->createTextNode($group['weight'][$i]));
					
					//create transform		
					$transform = $xml->createElement('transform');
					$item->appendChild($transform);
					//add value
					$transform->appendChild($xml->createTextNode($group['transform'][$i]));
					
					//create class_name		
					$class_name = $xml->createElement('class_name');
					$item->appendChild($class_name);
					//add value
					$class_name->appendChild($xml->createTextNode($group['class_name'][$i]));
				}
			}
		}
		
		//create customs
		$customs_xml = $xml->createElement('customs');
		$root->appendChild($customs_xml);
		if(!empty($customs)){
			foreach($customs as $custom){ 		
				//create item
				$item = $xml->createElement('item');
				$customs_xml->appendChild($item);		
				
				//create text		
				$text = $xml->createElement('text');
				$item->appendChild($text);
				//add value
				$text->appendChild($xml->createTextNode($custom['text']));
				
				//create style		
				$style = $xml->createElement('style');
				$item->appendChild($style);
				//add value
				$style->appendChild($xml->createTextNode($custom['style']));
				
				//create size		
				$size = $xml->createElement('size');
				$item->appendChild($size);
				//add value
				$size->appendChild($xml->createTextNode($custom['size']));
				
				//create weight		
				$weight = $xml->createElement('weight');
				$item->appendChild($weight);
				//add value
				$weight->appendChild($xml->createTextNode($custom['weight']));
				
				//create transform		
				$transform = $xml->createElement('transform');
				$item->appendChild($transform);
				//add value
				$transform->appendChild($xml->createTextNode($custom['transform']));
				
				//create class_name		
				$class_name = $xml->createElement('class_name');
				$item->appendChild($class_name);
				//add value
				$class_name->appendChild($xml->createTextNode($custom['class_name']));
			}
		}
		//nice output
		$xml->formatOutput = true;
		$xml->save("../config_xml/font_setting.xml"); 
	}
	public function changeTemplate(){
		
		$this->document->addStyle('view/stylesheet/bossthemes/boss_manager.css');
		if (isset($this->request->get['value']) && !empty($this->request->get['value'])) {
			$value = $this->request->get['value'];			
		} else {
			$value = 1;
		}
		
		if($value==1){
			$data['objXML'] = simplexml_load_file("../config_xml/theme_color_1.xml");
		}else if($value==2){
			$data['objXML'] = simplexml_load_file("../config_xml/theme_color_2.xml");
		}else if($value==3){
			$data['objXML'] = simplexml_load_file("../config_xml/theme_color_3.xml");
		}
	
		$json = array();			
	
		$json['output'] = $this->load->view('module/boss_changetemplate.tpl', $data);
		$this->response->addHeader('Content-Type: application/json');
		$this->response->setOutput(json_encode($json));
	}

	protected function validate() {
		if (!$this->user->hasPermission('modify', 'module/boss_manager')) {
			$this->error['warning'] = $this->language->get('error_permission');
		}

		if (!$this->error) {
			return true;
		} else {
			return false;
		}	
	}
	
	public function install() 
	{
		$this->load->model('setting/setting');
		$this->load->model('localisation/language');			
		$languages = $this->model_localisation_language->getLanguages();
		
		$header_mobile = array();
		$about_us_title = array();
		$about_us_content = array();
		$follow_us_title = array();
		$navigation_powered_content = array();
		
		foreach($languages as $language){
			$header_mobile{$language['language_id']} = '&lt;div class=&quot;logged-link&quot;&gt;
				&lt;a href=&quot;index.php?route=account/login&quot;&gt;&lt;i class=&quot;fa fa-user&quot;&gt;&lt;/i&gt;&lt;span&gt;Sign up&lt;/span&gt;&lt;/a&gt;
				&lt;a href=&quot;index.php?route=account/register&quot;&gt;&lt;i class=&quot;fa fa-plus&quot;&gt;&lt;/i&gt;&lt;span&gt;Create account&lt;/span&gt;&lt;/a&gt;
			&lt;/div&gt;';
			$about_us_title{$language['language_id']} = '';
			$about_us_content{$language['language_id']} = '';
			
			$follow_us_title{$language['language_id']} = 'Follow us';	
			$navigation_powered_content{$language['language_id']} = '&lt;div class=&quot;nav_footer&quot;&gt;
&lt;ul&gt;
&lt;li&gt;&lt;a title=&quot;About Us&quot; href=&quot;#&quot;&gt;About Us&lt;/a&gt;&lt;/li&gt;
&lt;li&gt;&lt;a title=&quot;Blog&quot; href=&quot;#&quot;&gt;Blog&lt;/a&gt;&lt;/li&gt;
&lt;li&gt;&lt;a title=&quot;Contact&quot; href=&quot;#&quot;&gt;Contact&lt;/a&gt;&lt;/li&gt;
&lt;li&gt;&lt;a title=&quot;Faq&quot; href=&quot;#&quot;&gt;Faq&lt;/a&gt;&lt;/li&gt;
&lt;li&gt;&lt;a title=&quot;Wishlist&quot; href=&quot;#&quot;&gt;Wishlist&lt;/a&gt;&lt;/li&gt;
&lt;li&gt;&lt;a title=&quot;My Account&quot; href=&quot;#&quot;&gt;My Account&lt;/a&gt;&lt;/li&gt;
&lt;li&gt;&lt;a title=&quot;Login&quot; href=&quot;#&quot;&gt;Login&lt;/a&gt;&lt;/li&gt;
&lt;li&gt;&lt;a title=&quot;Eng&quot; href=&quot;#&quot;&gt;Eng&lt;/a&gt;&lt;/li&gt;
&lt;/ul&gt;
&lt;/div&gt;
&lt;div id=&quot;powered&quot;&gt;
&lt;p&gt;&copy; 2014 Copyright by Claudine. All rights Reserved.&lt;span&gt;Opencart Themes by &lt;a href=&quot;http://www.bossthemes.com&quot;&gt;Bossthemes.com.&lt;/a&gt;&lt;/span&gt;&lt;/p&gt;
&lt;/div&gt;';
		}
		$boss_manager = array(
			'boss_manager' => array ( 
				'status' => 1,
				'option' => array(
					'bt_scroll_top' => 1,
					'sticky_menu' => 0,
					'use_menu' => 'megamenu',
					'animation' => 1,				
					'loading' => 1,				
				),
				'layout' => array(
					'mode_css' => 'wide',
					'box_width' => 1200,
					'h_mode_css' => 'inherit',
					'h_box_width' => 1180,
					'f_mode_css' => 'inherit',
					'f_box_width' => 1180,
				),
				'header_link' => array(
					'language' => 1,
					'currency' => 1,
					'phone' => 0,
					'my_account' => 1,
					'wishlist' => 1,
					'shopping_cart' => 1,
					'checkout' => 1,
					'logo' => 1,
					'search' => 1,
					'cart_mini' => 1,
				),
				'footer_link' => array(
					'information' => 1,
					'contact_us' => 1,
					'return' => 1,
					'site_map' => 1,
					'brands' => 1,
					'gift_vouchers' => 1,
					'affiliates' => 1,
					'specials' => 1,
					'my_account' => 1,
					'order_history' => 1,
					'wishlist' => 1,
					'newsletter' => 1,					
				),
				'color' => 1,
				'other' => array(
					'pro_tab' => 'use_tab',
					'category_image' => 0,
					'category_info' => 1,
					'refine_search' => 1,
					'category_icon' => 1,
					'view_pro' => 'both_grid',
					'perrow' => 3,					
				),
			),
			
			'boss_manager_header_mobile' => array(
				'status' => 1,
				'content' => $header_mobile,
			),
			'boss_manager_footer_about' => array(
				'status' => 1,
				'image_status' => 1,
				'image_link' => 'catalog/bt_claudine/logo_footer.png',
				'about_title' => $about_us_title,
				'about_content' => $about_us_content,	
			),
			'boss_manager_footer_social' => array(
				'status' => 1,
				'title' => $follow_us_title,
				'face_url' => '#',
				'face_status' => 1,
				'twitter_url' => '#',	
				'twitter_status' => 1,
				'googleplus_url' => '#',	
				'googleplus_status' => 1,
				'pinterest_url' => '#',
				'pinterest_status' => 1,
				'youtube_url' => '#',
				'youtube_status' => 1,
				'in_url' => '#',
				'in_status' => 1,
				'instagram_url' => '#',
				'instagram_status' => 1,				
				'rss_url' => '#',	
				'rss_status' => 1,
								
			),
			'boss_manager_footer_powered' => $navigation_powered_content,
		);
		
		$this->model_setting_setting->editSetting('boss_manager', $boss_manager);		
	}
	private function deleteDataModule($code) {
		$this->load->model('extension/module');
		$this->load->model('setting/setting');
		// delete the module
		$this->model_extension_module->deleteModulesByCode($code);		
		$this->model_setting_setting->deleteSetting($code);
   	}
	private function uninstallModule($code) {			
    	$this->db->query("DELETE FROM " . DB_PREFIX . "extension WHERE code = '" . $this->db->escape($code) . "'");    	
   	}
	private function installModule($code) {	
		$this->load->model('user/user_group');		
		$this->uninstallModule($code);		
		$this->db->query("INSERT INTO " . DB_PREFIX . "extension SET type	= 'module', code = '" . $this->db->escape($code) . "'");	
		$this->model_user_user_group->addPermission($this->user->getId(), 'access', 'module/'.$code);
		
   	}
	private function checkModule($code) {					
		$result = $this->db->query("Select *from " . DB_PREFIX . "extension where code = '" . $this->db->escape($code) . "'");
		if($result->num_rows)
			return true;
		return false;
   	}
	private function getIdLayout($layout_name) {
		$query = $this->db->query("SELECT DISTINCT * FROM " . DB_PREFIX . "layout WHERE LOWER(name) = LOWER('".$layout_name."')");
		return (int)$query->row['layout_id'];
	}
	public function addSampleData(){
		$module_code = $this->request->get['module_code'];
		$this->load->model('localisation/language');
		$languages = $this->model_localisation_language->getLanguages();
		$json = array();
		$json['error'] = '';
		switch ($module_code) {
			case 'boss_alphabet':
				$code = 'boss_alphabet';
				$this->deleteDataModule($code);
				$this->installModule($code);
				$title = array();
				foreach($languages as $language){
					$title{$language['language_id']} = 'Quick Select';
				}
				//create sample data and add module
				$sample_data = array(
					'name' => 'Alphabet',
					'code' => $code,
					'status' => 1,
					'boss_alphabet_module' => array(
						'title' => $title,										
					),
				);
				$this->model_extension_module->addModule($code, $sample_data);
				//get module_id
				$module_id = $this->db->getLastId();
				
				$layouts = array(
					0 =>  array(
						'layout_id' => $this->getIdLayout('category'), // categpry page		
						'position' => 'column_left',
						'sort_order' => 3,
					),
					1 =>  array(
						'layout_id' => $this->getIdLayout('product'), // product page		
						'position' => 'column_left',
						'sort_order' => 3,
					),
					2 =>  array(
						'layout_id' => $this->getIdLayout('manufacturer'), // manufacture page		
						'position' => 'column_left',
						'sort_order' => 3,
					),					
				);
				foreach($layouts as $layout){
					//add layout		
					$layout_module = array(
						'code'  => $code.'.'.$module_id,
						'position'  => $layout['position'],				
						'sort_order'  => $layout['sort_order'],
					);			
					$layout_id = $layout['layout_id']; 
					$this->db->query("INSERT INTO " . DB_PREFIX . "layout_module SET layout_id = '" . (int)$layout_id . "', code = '" . $this->db->escape($layout_module['code']) . "', position = '" . $this->db->escape($layout_module['position']) . "', sort_order = '" . (int)$layout_module['sort_order'] . "'");
				}
			break;
			case 'boss_carousel':
				$code = 'boss_carousel';
				$this->deleteDataModule($code);
				$this->installModule($code);
				$banner_image_description = array();
				$title = array();
				foreach($languages as $language){
					$banner_image_description[$language['language_id']]['title'] = 'Boss Carousel';
					$title{$language['language_id']} = 'Brand & Client';
				}
				$data_banner = array(
					'name' => 'Boss Carousel',
					'status' => 1,
					'banner_image' => array(
						0 => array(
							'banner_image_description' => $banner_image_description,
							'link' => '#',
							'image' => 'catalog/bt_claudine/brand5.png',
							'sort_order' => 1,
						),
						1 => array(
							'banner_image_description' => $banner_image_description,
							'link' => '#',
							'image' => 'catalog/bt_claudine/brand1.png',
							'sort_order' => 2,
						),
						2 => array(
							'banner_image_description' => $banner_image_description,
							'link' => '#',
							'image' => 'catalog/bt_claudine/brand2.png',
							'sort_order' => 3,
						),
						3 => array(
							'banner_image_description' => $banner_image_description,
							'link' => '#',
							'image' => 'catalog/bt_claudine/brand3.png',
							'sort_order' => 4,
						),
						4 => array(
							'banner_image_description' => $banner_image_description,
							'link' => '#',
							'image' => 'catalog/bt_claudine/brand4.png',
							'sort_order' => 5,
						),
						5 => array(
							'banner_image_description' => $banner_image_description,
							'link' => '#',
							'image' => 'catalog/bt_claudine/brand6.png',
							'sort_order' => 6,
						),
						6 => array(
							'banner_image_description' => $banner_image_description,
							'link' => '#',
							'image' => 'catalog/bt_claudine/brand7.png',
							'sort_order' => 7,
						),
						7 => array(
							'banner_image_description' => $banner_image_description,
							'link' => '#',
							'image' => 'catalog/bt_claudine/brand8.png',
							'sort_order' => 8,
						),
						
					),
					
				);
				$this->load->model('design/banner');
				$banner_id = $this->model_design_banner->addBanner($data_banner);
			
				//create sample data and add module
				$sample_data = array(
					'name' => 'Boss Carousel',
					'code' => $code,
					'status' => 1,
					'boss_carousel_module' => array(
						'title' => $title,
						'banner_id' => $banner_id,
						'limit' => 8,
						'width' => 160,
						'height' => 40,
					),									
				);
				$this->model_extension_module->addModule($code, $sample_data);
				//get module_id
				$module_id = $this->db->getLastId();
				
				$layouts = array(
					0 =>  array(
						'layout_id' => $this->getIdLayout('home'), // home page		
						'position' => 'content_top',
						'sort_order' => 6,
					),
									
				);
				foreach($layouts as $layout){
					//add layout		
					$layout_module = array(
						'code'  => $code.'.'.$module_id,
						'position'  => $layout['position'],				
						'sort_order'  => $layout['sort_order'],
					);			
					$layout_id = $layout['layout_id']; 
					$this->db->query("INSERT INTO " . DB_PREFIX . "layout_module SET layout_id = '" . (int)$layout_id . "', code = '" . $this->db->escape($layout_module['code']) . "', position = '" . $this->db->escape($layout_module['position']) . "', sort_order = '" . (int)$layout_module['sort_order'] . "'");
				}
			break;
			case 'boss_category':
				$code = 'boss_category';
				$this->deleteDataModule($code);
				$this->installModule($code);
				$title = array();
				foreach($languages as $language){
					$title{$language['language_id']} = 'Categories';
				}
				//create sample data and add module
				$sample_data = array(
					'name' => 'Category',
					'code' => $code,
					'status' => 1,
					'title' => $title,
					'type' => 0,
					'column' => 2,
					'width' => 380,					
				);
				$this->model_extension_module->addModule($code, $sample_data);
				//get module_id
				$module_id = $this->db->getLastId();
				
				$layouts = array(
					0 =>  array(
						'layout_id' => $this->getIdLayout('category'), // categpry page		
						'position' => 'column_left',
						'sort_order' => 1,
					),
					1 =>  array(
						'layout_id' => $this->getIdLayout('product'), // product page		
						'position' => 'column_left',
						'sort_order' => 1,
					),
					2 =>  array(
						'layout_id' => $this->getIdLayout('manufacturer'), // manufacture page		
						'position' => 'column_left',
						'sort_order' => 1,
					),					
				);
				foreach($layouts as $layout){
					//add layout		
					$layout_module = array(
						'code'  => $code.'.'.$module_id,
						'position'  => $layout['position'],				
						'sort_order'  => $layout['sort_order'],
					);			
					$layout_id = $layout['layout_id']; 
					$this->db->query("INSERT INTO " . DB_PREFIX . "layout_module SET layout_id = '" . (int)$layout_id . "', code = '" . $this->db->escape($layout_module['code']) . "', position = '" . $this->db->escape($layout_module['position']) . "', sort_order = '" . (int)$layout_module['sort_order'] . "'");
				}
			break;
			case 'boss_featured':
				$code = 'boss_featured';
				$this->deleteDataModule($code);
				$this->installModule($code);
				$title1 = array();
				$title2 = array();
				$title3 = array();
				$title4 = array();
				foreach($languages as $language){
					$title1{$language['language_id']} = 'Featured Products';				
					$title2{$language['language_id']} = 'Top Best Sellers';
					$title3{$language['language_id']} = 'Best Seller';
					$title4{$language['language_id']} = 'New Arrivals';
					
				}
				$contents =  array(
					0 => array(
						'name' => 'Featured Product - Home',
						'status' => 1,
						'title' => $title1,
						'type_product' => 'popular',
						'image_width' => 220,
						'image_height' => 220,
						'limit_character' => 30,
						'limit' => 4,
						'show_slider' => 0,
						'num_row' => 1,						
						'auto_scroll' => 0,						
						'per_row' => 2,
						'nav_type' => 0,
						'show_pro_large' => 1,
						'product_id' => 42,
						'img_width' => 370,
						'img_height' => 370,
						'addition_image' => 1,
						'img_add_width' => 83,
						'img_add_height' => 83,
											
						'layout_id' => $this->getIdLayout('home'), // home page		
						'position' => 'content_top',
						'sort_order' => 3,
					),
					1 => array(
						'name' => 'Left1',
						'status' => 1,
						'title' => $title2,
						'type_product' => 'best_seller',
						'image_width' => 68,
						'image_height' => 68,
						'limit_character' => 20,
						'limit' => 10,
						'show_slider' => 1,
						'num_row' => 1,						
						'auto_scroll' => 1,						
						'per_row' => 5,
						'nav_type' => 0,
						'show_pro_large' => 0,
						'product_id' => '',
						'img_width' => 370,
						'img_height' => 370,
						'addition_image' => 0,
						'img_add_width' => 83,
						'img_add_height' => 83,
											
						'layout_id' => $this->getIdLayout('home'), // home page		
						'position' => 'column_left',
						'sort_order' => 1,
					),
					2 => array(
						'name' => 'Left2',
						'status' => 1,
						'title' => $title3,
						'type_product' => 'popular',
						'image_width' => 68,
						'image_height' => 68,
						'limit_character' => 20,
						'limit' => 6,
						'show_slider' => 1,
						'num_row' => 1,						
						'per_row' => 3,
						'auto_scroll' => 0,
						'nav_type' => 0,
						'show_pro_large' => 1,
						'product_id' => 42,
						'img_width' => 230,
						'img_height' => 230,
						'addition_image' => 0,
						'img_add_width' => 83,
						'img_add_height' => 83,
											
						'layout_id' => $this->getIdLayout('home'), // home page		
						'position' => 'column_left',
						'sort_order' => 3,
					),
					3 => array(
						'name' => 'New Arrivals',
						'status' => 1,
						'title' => $title4,
						'type_product' => 'latest',
						'image_width' => 270,
						'image_height' => 270,
						'limit_character' => 30,
						'limit' => 6,
						'show_slider' => 0,
						'num_row' => 1,						
						'per_row' => 3,
						'auto_scroll' => 0,
						'nav_type' => 0,
						'show_pro_large' => 0,
						'product_id' => '',
						'img_width' => 370,
						'img_height' => 370,
						'addition_image' => 0,
						'img_add_width' => 83,
						'img_add_height' => 83,
											
						'layout_id' => $this->getIdLayout('home'), // home page		
						'position' => 'content_top',
						'sort_order' => 1,
					),
					
				);
				foreach($contents as $key => $content){
					//create sample data and add module
					
					$this->model_extension_module->addModule($code, $content);
					//get module_id
					$module_id = $this->db->getLastId();
					$data_module = $content;
					$data_module['module_id'] = $module_id;
					$this->model_extension_module->editModule($module_id, $data_module);
					//add layout		
					$layout_module = array(
						'code'  => $code.'.'.$module_id,
						'position'  => $content['position'],				
						'sort_order'  => $content['sort_order'],
					);			
					$layout_id = $content['layout_id']; 
					$this->db->query("INSERT INTO " . DB_PREFIX . "layout_module SET layout_id = '" . (int)$layout_id . "', code = '" . $this->db->escape($layout_module['code']) . "', position = '" . $this->db->escape($layout_module['position']) . "', sort_order = '" . (int)$layout_module['sort_order'] . "'");
				}
			break;
			case 'boss_filterproduct':
				$code = 'boss_filterproduct';
				$this->deleteDataModule($code);
				$this->installModule($code);
				$title1 = array();
				$title2 = array();
				$title3 = array();
				$title4 = array();
				$title5 = array();
				foreach($languages as $language){
					$title1{$language['language_id']} = 'All';
					$title2{$language['language_id']} = 'Casio';
					$title3{$language['language_id']} = 'Rolex';
					$title4{$language['language_id']} = 'Seiko';
					$title5{$language['language_id']} = 'Top Brands';
				}
				//create sample data and add module
				$sample_data = array(
					'name' => 'Top Brands',
					'code' => $code,
					'status' => 1,
					'boss_filterproduct_module' => array(
						'tabs' => array(
							0 => array(
								'title' => $title1,
								'type_product' => 'latest',
							),
							1 => array(
								'title' => $title2,
								'type_product' => 'popular',
							),
							2 => array(
								'title' => $title3,
								'type_product' => 'special',
							),
							3 => array(
								'title' => $title4,
								'type_product' => 'best_seller',
							),
						),					
						'title' => $title5,
						'image_width' => 195,
						'image_height' => 195,
						'limit' => 4,
						'use_scrolling_panel' => 0,
						'scroll' => 4,
						'use_tab' => 1,
					),
				);
				$this->model_extension_module->addModule($code, $sample_data);
				//get module_id
				$module_id = $this->db->getLastId();
				
				$layouts = array(
					0 =>  array(
						'layout_id' => $this->getIdLayout('home'), // home page		
						'position' => 'content_top',
						'sort_order' => 5,
					),									
				);
				foreach($layouts as $layout){
					//add layout		
					$layout_module = array(
						'code'  => $code.'.'.$module_id,
						'position'  => $layout['position'],				
						'sort_order'  => $layout['sort_order'],
					);			
					$layout_id = $layout['layout_id']; 
					$this->db->query("INSERT INTO " . DB_PREFIX . "layout_module SET layout_id = '" . (int)$layout_id . "', code = '" . $this->db->escape($layout_module['code']) . "', position = '" . $this->db->escape($layout_module['position']) . "', sort_order = '" . (int)$layout_module['sort_order'] . "'");
				}
			break;
			case 'boss_flickr':
				$code = 'boss_flickr';
				$this->deleteDataModule($code);
				$this->installModule($code);				
				$title = array();
				foreach($languages as $language){
					$title{$language['language_id']} = 'Flickr';
				}
				//create sample data and add module
				$sample_data = array(
					'name' => 'Boss Flickr',
					'code' => $code,
					'status' => 1,
					'boss_flickr_module' => array(
						'api_key' => '313c1e9d1c1819731bc46f9182e6325f',
						'user_id' => '131017800@N05',
						'title' => $title,
						'limit' => 6,						
						'image_size' => 1,						
					),
				);
				$this->model_extension_module->addModule($code, $sample_data);
				//get module_id
				$module_id = $this->db->getLastId();
				
				$layouts = array(
					0 =>  array(
						'layout_id' => $this->getIdLayout('home'), // home page		
						'position' => 'column_left',
						'sort_order' => 6,
					),										
				);
				foreach($layouts as $layout){
					//add layout		
					$layout_module = array(
						'code'  => $code.'.'.$module_id,
						'position'  => $layout['position'],				
						'sort_order'  => $layout['sort_order'],
					);			
					$layout_id = $layout['layout_id']; 
					$this->db->query("INSERT INTO " . DB_PREFIX . "layout_module SET layout_id = '" . (int)$layout_id . "', code = '" . $this->db->escape($layout_module['code']) . "', position = '" . $this->db->escape($layout_module['position']) . "', sort_order = '" . (int)$layout_module['sort_order'] . "'");
				}
			break;
			case 'boss_gallery':
				$code = 'boss_gallery';
				$this->deleteDataModule($code);
				$this->installModule($code);				
				$title = array();
				foreach($languages as $language){
					$title{$language['language_id']} = 'Flickr';
				}
				//create sample data and add module
				$sample_data = array(
					'name' => 'Gallery',
					'code' => $code,
					'status' => 1,
					'boss_gallery_title' => $title,
					'boss_gallery_image' => array(
						0 => array(
							'gallery_image_description' => $title,
							'link' => '#',
							'image' => 'catalog/bt_claudine/flirck1.jpg',
							'class' => '',
						),
						1 => array(
							'gallery_image_description' => $title,
							'link' => '#',
							'image' => 'catalog/bt_claudine/flirck2.jpg',
							'class' => '',
						),
						2 => array(
							'gallery_image_description' => $title,
							'link' => '#',
							'image' => 'catalog/bt_claudine/flirck3.jpg',
							'class' => '',
						),
						3 => array(
							'gallery_image_description' => $title,
							'link' => '#',
							'image' => 'catalog/bt_claudine/flirck4.jpg',
							'class' => '',
						),
						4 => array(
							'gallery_image_description' => $title,
							'link' => '#',
							'image' => 'catalog/bt_claudine/flirck5.jpg',
							'class' => '',
						),
						5 => array(
							'gallery_image_description' => $title,
							'link' => '#',
							'image' => 'catalog/bt_claudine/flirck6.jpg',
							'class' => '',
						),
					),					
				);
				$this->model_extension_module->addModule($code, $sample_data);
				//get module_id
				$module_id = $this->db->getLastId();
				
				$layouts = array(
					0 =>  array(
						'layout_id' => $this->getIdLayout('home'), // home page		
						'position' => 'column_left',
						'sort_order' => 6,
					),										
				);
				foreach($layouts as $layout){
					//add layout		
					$layout_module = array(
						'code'  => $code.'.'.$module_id,
						'position'  => $layout['position'],				
						'sort_order'  => $layout['sort_order'],
					);			
					$layout_id = $layout['layout_id']; 
					$this->db->query("INSERT INTO " . DB_PREFIX . "layout_module SET layout_id = '" . (int)$layout_id . "', code = '" . $this->db->escape($layout_module['code']) . "', position = '" . $this->db->escape($layout_module['position']) . "', sort_order = '" . (int)$layout_module['sort_order'] . "'");
				}
			break;
			case 'boss_manufacturer':
				$code = 'boss_manufacturer';
				$this->deleteDataModule($code);
				$this->installModule($code);
				$title = array();
				foreach($languages as $language){					
					$title{$language['language_id']} = 'Manufacturer';
				}
				//create sample data and add module
				$sample_data = array(
					'name' => 'Manufacturer',
					'code' => $code,
					'status' => 1,
					'module_description' => array(
						'title' => $title,
					),								
					
				);
				$this->model_extension_module->addModule($code, $sample_data);
				//get module_id
				$module_id = $this->db->getLastId();
				
				$layouts = array(
					0 =>  array(
						'layout_id' => $this->getIdLayout('category'), // categpry page		
						'position' => 'column_left',
						'sort_order' => 4,
					),
					1 =>  array(
						'layout_id' => $this->getIdLayout('product'), // product page		
						'position' => 'column_left',
						'sort_order' => 4,
					),
					2 =>  array(
						'layout_id' => $this->getIdLayout('manufacturer'), // manufacture page		
						'position' => 'column_left',
						'sort_order' => 4,
					),					
				);
				foreach($layouts as $layout){
					//add layout		
					$layout_module = array(
						'code'  => $code.'.'.$module_id,
						'position'  => $layout['position'],				
						'sort_order'  => $layout['sort_order'],
					);			
					$layout_id = $layout['layout_id']; 
					$this->db->query("INSERT INTO " . DB_PREFIX . "layout_module SET layout_id = '" . (int)$layout_id . "', code = '" . $this->db->escape($layout_module['code']) . "', position = '" . $this->db->escape($layout_module['position']) . "', sort_order = '" . (int)$layout_module['sort_order'] . "'");
				}
			break;
			case 'boss_menucategory':
				$code = 'boss_menucategory';
				$this->deleteDataModule($code);
				$this->installModule($code);
				$title = array();
				$title1 = array();
				$title2 = array();
				$title3 = array();
				$title4 = array();
				$title5 = array();
				$more_category = array();
				foreach($languages as $language){
					$more_category{$language['language_id']} = 'More Category';
					$title{$language['language_id']} = "Categories";
					$title1{$language['language_id']} = "Men's Watches";
					$title2{$language['language_id']} = "Women’s Watches";
					$title3{$language['language_id']} = 'Leather Watches';
					$title4{$language['language_id']} = 'Gold & Siver';
					$title5{$language['language_id']} = 'Steel  Watches';
				}
				//create sample data and add module
				$sample_data = array(
					'name' => 'Menu-Category',
					'code' => $code,
					'status' => 1,					
					'title' => $title,										
					'alway_show' => 10,					
					'menu_fixed' => 1,					
					'boss_menucategory_config' => array(
						'0cs4pqmk9yj02j4i' => array(
							'title' => $title1,
							'icon' => 'catalog/bt_claudine/icon_category_01.png',
							'category_id' => 20,
							'column' => 2,
							'sub_width' => 240,
							'bgimage' => '',
							'status' => 1,
							'sort_order' => 1,
						),
						'hfr0v91xkw7' => array(
							'title' => $title2,
							'icon' => 'catalog/bt_claudine/icon_category_03.png',
							'category_id' => 20,
							'column' => 2,
							'sub_width' => 240,
							'bgimage' => '',
							'status' => 1,
							'sort_order' => 2,
						),
						'zqnkzwrlr3e' => array(
							'title' => $title3,
							'icon' => 'catalog/bt_claudine/icon_category_05.png',
							'category_id' => 20,
							'column' => 2,
							'sub_width' => 240,
							'bgimage' => '',
							'status' => 1,
							'sort_order' => 3,
						),
						'iq7cx9ze07p' => array(
							'title' => $title4,
							'icon' => 'catalog/bt_claudine/icon_category_07.png',
							'category_id' => 20,
							'column' => 2,
							'sub_width' => 240,
							'bgimage' => '',
							'status' => 1,
							'sort_order' => 4,
						),
						'0o0mqr7ntvh' => array(
							'title' => $title5,
							'icon' => 'catalog/bt_claudine/icon_category_11.png',
							'category_id' => 20,
							'column' => 2,
							'sub_width' => 240,
							'bgimage' => '',
							'status' => 1,
							'sort_order' => 5,
						),
					),
					
				);
				$this->model_extension_module->addModule($code, $sample_data);
				//get module_id
				$module_id = $this->db->getLastId();
				//edit module_id
				$sample_data_edit = $sample_data;
				$sample_data_edit['module_id'] = $module_id;				
				$this->model_extension_module->editModule($module_id, $sample_data_edit);
				
				$layouts = array(
					0 =>  array(
						'layout_id' => 9999, // all page		
						'position' => 'btslideshow',
						'sort_order' => 1,
					),
										
				);
				foreach($layouts as $layout){
					//add layout		
					$layout_module = array(
						'code'  => $code.'.'.$module_id,
						'position'  => $layout['position'],				
						'sort_order'  => $layout['sort_order'],
					);			
					$layout_id = $layout['layout_id']; 
					$this->db->query("INSERT INTO " . DB_PREFIX . "layout_module SET layout_id = '" . (int)$layout_id . "', code = '" . $this->db->escape($layout_module['code']) . "', position = '" . $this->db->escape($layout_module['position']) . "', sort_order = '" . (int)$layout_module['sort_order'] . "'");
				}
			break;
			case 'boss_quickshop':
				$code = 'boss_quickshop';
				$this->deleteDataModule($code);
				$this->installModule($code);
				$title = array();
				foreach($languages as $language){
					$title{$language['language_id']} = 'Quick Shop';
				}
				$contents = array(
					0 => array(
						'position' => 'content_top',
						'sort_order' => 0,
						'layout_id' => $this->getIdLayout('home'),  // home page
					),
					1 => array(
						'position' => 'content_top',
						'sort_order' => 0,
						'layout_id' => $this->getIdLayout('category'),   //category page
					),
					2 => array(
						'position' => 'content_top',
						'sort_order' => 0,
						'layout_id' => $this->getIdLayout('product'),   //product page
					),
				);
				
				//create sample data and add module
				$sample_data = array(
					'name' => 'Boss Quickshop',
					'code' => $code,
					'status' => 1,
					'boss_quickshop_module' => array(
						'title' => $title,
						'array_class_selected' => '#column-left .bt-item-large .product-thumb, #content .product-thumb,.product-layout&gt;div,#product_related &gt; li,.product-grid li,.content_tabs .box-product &gt; div',
						'width' => 900,				
					),
				);
				$this->model_extension_module->addModule($code, $sample_data);
				//get module_id
				$module_id = $this->db->getLastId();
				
				foreach($contents as $content){
					//add layout		
					$layout_module = array(
						'code'  => $code.'.'.$module_id,
						'position'  => $content['position'],				
						'sort_order'  => $content['sort_order'],
					);			
					$layout_id = $content['layout_id']; 
					$this->db->query("INSERT INTO " . DB_PREFIX . "layout_module SET layout_id = '" . (int)$layout_id . "', code = '" . $this->db->escape($layout_module['code']) . "', position = '" . $this->db->escape($layout_module['position']) . "', sort_order = '" . (int)$layout_module['sort_order'] . "'");
				}
			break;
			case 'boss_tagcloud':
				$code = 'boss_tagcloud';
				$this->deleteDataModule($code);
				$this->installModule($code);
				$title = array();
				foreach($languages as $language){
					$boss_tagcloud_module[$language['language_id']]['title'] = 'Tags';
				}
				//create sample data and add module
				$sample_data = array(
					'name' => 'Tag Cloud',
					'code' => $code,
					'status' => 1,
					'boss_tagcloud_module' => $boss_tagcloud_module,
					'boss_tagcloud_module_limit' => 20,										
					'boss_tagcloud_module_min_size' => 9,
					'boss_tagcloud_module_max_size' => 25,
					'boss_tagcloud_module_font_weight' => 'normal',						
					
				);
				$this->model_extension_module->addModule($code, $sample_data);
				//get module_id
				$module_id = $this->db->getLastId();
				
				$layouts = array(
					0 =>  array(
						'layout_id' => $this->getIdLayout('category'), // categpry page		
						'position' => 'column_left',
						'sort_order' => 6,
					),
					1 =>  array(
						'layout_id' => $this->getIdLayout('product'), // product page		
						'position' => 'column_left',
						'sort_order' => 6,
					),
					2 =>  array(
						'layout_id' => $this->getIdLayout('manufacturer'), // manufacture page		
						'position' => 'column_left',
						'sort_order' => 6,
					),					
				);
				foreach($layouts as $layout){
					//add layout		
					$layout_module = array(
						'code'  => $code.'.'.$module_id,
						'position'  => $layout['position'],				
						'sort_order'  => $layout['sort_order'],
					);			
					$layout_id = $layout['layout_id']; 
					$this->db->query("INSERT INTO " . DB_PREFIX . "layout_module SET layout_id = '" . (int)$layout_id . "', code = '" . $this->db->escape($layout_module['code']) . "', position = '" . $this->db->escape($layout_module['position']) . "', sort_order = '" . (int)$layout_module['sort_order'] . "'");
				}
			break;
			case 'boss_testimonial':
				$code = 'boss_testimonial';
				$this->deleteDataModule($code);
				$this->installModule($code);
				$this->load->model('catalog/boss_testimonial');
				$this->load->model('user/user_group');
				
				$this->model_user_user_group->addPermission($this->user->getId(), 'access', 'catalog/boss_testimonial');
				$this->model_user_user_group->addPermission($this->user->getId(), 'modify', 'catalog/boss_testimonial');
				$this->model_user_user_group->addPermission($this->user->getId(), 'access', 'catalog/boss_testimonial_setting');
				$this->model_user_user_group->addPermission($this->user->getId(), 'modify', 'catalog/boss_testimonial_setting');
				
				$data = array(
					'testimonial_admin_approved'             =>1,
					'testimonial_default_rating'             =>3,
					'testimonial_all_page_limit'             =>20,
				);
				
				$this->load->model('setting/setting');        
				$this->model_setting_setting->editSetting('testimonial', $data);
				$this->model_catalog_boss_testimonial->createDatabaseTables();
				$title = array();
				foreach($languages as $language){
					$title{$language['language_id']} = 'Testimonials';
				}
				//create sample data and add module
				$sample_data = array(
					'name' => 'Home Testimonial Left',
					'code' => $code,
					'status' => 1,
					'boss_testimonial_module' => array(
						'title' => $title,										
						'limit' => 3,										
						'limit_character' => 150,										
						'random' => 0,
						'auto_scroll' => 1,
						'show_name' => 1,						
						'show_subject' => 0,						
						'show_message' => 1,						
						'show_city' => 0,						
						'show_rating' => 0,						
						'show_date' => 1,						
						'show_image' => 1,						
						'show_all' => 0,						
						'show_write' => 0,						
					),
				);
				$this->model_extension_module->addModule($code, $sample_data);
				//get module_id
				$module_id = $this->db->getLastId();
				
				$layouts = array(
					0 =>  array(
						'layout_id' => $this->getIdLayout('home'), // home page		
						'position' => 'column_left',
						'sort_order' => 4,
					),										
				);
				foreach($layouts as $layout){
					//add layout		
					$layout_module = array(
						'code'  => $code.'.'.$module_id,
						'position'  => $layout['position'],				
						'sort_order'  => $layout['sort_order'],
					);			
					$layout_id = $layout['layout_id']; 
					$this->db->query("INSERT INTO " . DB_PREFIX . "layout_module SET layout_id = '" . (int)$layout_id . "', code = '" . $this->db->escape($layout_module['code']) . "', position = '" . $this->db->escape($layout_module['position']) . "', sort_order = '" . (int)$layout_module['sort_order'] . "'");
				}
			break;
			
			case 'boss_zoom':
				$code = 'boss_zoom';
				$this->deleteDataModule($code);
				$this->installModule($code);
				$title = array();
				foreach($languages as $language){
					$title{$language['language_id']} = 'Boss Zoom';
				}
				//create sample data and add module
				$sample_data = array(
					'name' => 'Boss Zoom',
					'code' => $code,
					'status' => 1,
					'boss_zoom_module' => array(
						'thumb_image_width' => 480,
						'thumb_image_height' => 480,
						'addition_image_width' => 90,
						'addition_image_height' => 90,
						'zoom_image_width' => 700,
						'zoom_image_height' => 700,
						'zoom_area_width' => 480,
						'zoom_area_height' => 480,
						'position_zoom_area' => 'inside',
						'adjustX' => 0,
						'adjustY' => 0,
						'title_image' => 1,
						'title_opacity' => 0.5,
						'tint' => '#FFFFFF',
						'tintOpacity' => 0.5,
						'softfocus' => 1,
						'lensOpacity' => 0.7,
						'smoothMove' => 3,
					),
				);
				$this->model_extension_module->addModule($code, $sample_data);
				//get module_id
				$module_id = $this->db->getLastId();
				
				$layouts = array(					
					0 =>  array(
						'layout_id' => $this->getIdLayout('product'), // product page		
						'position' => 'content_top',
						'sort_order' => 0,
					),
									
				);
				foreach($layouts as $layout){
					//add layout		
					$layout_module = array(
						'code'  => $code.'.'.$module_id,
						'position'  => $layout['position'],				
						'sort_order'  => $layout['sort_order'],
					);			
					$layout_id = $layout['layout_id']; 
					$this->db->query("INSERT INTO " . DB_PREFIX . "layout_module SET layout_id = '" . (int)$layout_id . "', code = '" . $this->db->escape($layout_module['code']) . "', position = '" . $this->db->escape($layout_module['position']) . "', sort_order = '" . (int)$layout_module['sort_order'] . "'");
				}
			break;
			case 'html':
				$code = 'html';
				$this->deleteDataModule($code);
				$this->installModule($code);
				$module_description1 = array();
				$module_description2 = array();
				$module_description3 = array();
				$module_description4 = array();
				$module_description5 = array();
				$module_description6 = array();
				$module_description7 = array();
				
				foreach($languages as $language){
					$module_description1[$language['language_id']]['title'] = '';
					$module_description2[$language['language_id']]['title'] = '';
					$module_description3[$language['language_id']]['title'] = '';
					$module_description4[$language['language_id']]['title'] = '';
					$module_description5[$language['language_id']]['title'] = '';
					$module_description6[$language['language_id']]['title'] = '';
					$module_description7[$language['language_id']]['title'] = '';
					
					$module_description1[$language['language_id']]['description'] = '&lt;div data-delay=&quot;400&quot; data-animate=&quot;fadeInUp&quot; class=&quot;block-cate-page fadeInUp animated&quot;&gt;&lt;a title=&quot;banner category&quot; href=&quot;#&quot;&gt;&lt;img src=&quot;http://demo.bossthemes.com/claudine_watches/image/catalog/bt_claudine/banner-category.jpg&quot; alt=&quot;banner-category&quot;&gt;
	&lt;/a&gt;&lt;/div&gt;';
					$module_description2[$language['language_id']]['description'] = '&lt;div class=&quot;block-cate-page block-home-1&quot;&gt;
&lt;a href=&quot;#&quot;&gt;
&lt;img src=&quot;http://demo.bossthemes.com/claudine_watches/image/catalog/bt_claudine/block1.jpg&quot; title=&quot;banner&quot; alt=&quot;banner&quot;&gt;
&lt;/a&gt;
&lt;/div&gt;';
					$module_description3[$language['language_id']]['description'] = '&lt;div class=&quot;block-home-2&quot;&gt;
&lt;div class=&quot;block-cate-page&quot;&gt;
&lt;div class=&quot;col-sm-6 col-xs-12&quot;&gt;&lt;a href=&quot;#&quot;&gt;&lt;img src=&quot;http://demo.bossthemes.com/claudine_watches/image/catalog/bt_claudine/block2.jpg&quot; title=&quot;banner&quot; alt=&quot;banner&quot;&gt;&lt;/a&gt;&lt;/div&gt;&lt;div class=&quot;col-sm-6 col-xs-12&quot;&gt;&lt;a href=&quot;#&quot;&gt;&lt;img src=&quot;http://demo.bossthemes.com/claudine_watches/image/catalog/bt_claudine/block3.jpg&quot; title=&quot;banner&quot; alt=&quot;banner&quot;&gt;&lt;/a&gt;&lt;/div&gt;&lt;/div&gt;
&lt;/div&gt;';
					$module_description4[$language['language_id']]['description'] = '&lt;div class=&quot;banner-column&quot;&gt;&lt;a title=&quot;banner&quot; href=&quot;#&quot;&gt;
&lt;img title=&quot;banner&quot; alt=&quot;banner&quot; src=&quot;http://demo.bossthemes.com/claudine_watches/image/catalog/bt_claudine/block_column.jpg&quot;&gt;&lt;/a&gt;&lt;/div&gt;';
					$module_description5[$language['language_id']]['description'] = '&lt;div class=&quot;shipping_return_support&quot;&gt;
&lt;div class=&quot;b_shipping col-sm-4 col-xs-12&quot;&gt;
&lt;a title=&quot;Free Shipping All Order&quot; href=&quot;#&quot;&gt;&lt;span&gt;&lt;i class=&quot;fa fa-truck&quot;&gt;&lt;/i&gt; Free Shipping All Order&lt;/span&gt;&lt;/a&gt;
&lt;/div&gt;
&lt;div class=&quot;b_return col-sm-4 col-xs-12&quot;&gt;
&lt;a title=&quot;30 days return service&quot; href=&quot;#&quot;&gt;&lt;span&gt;&lt;i class=&quot;fa fa-paper-plane&quot;&gt;&lt;/i&gt;30 days return service&lt;/span&gt;&lt;/a&gt;
&lt;/div&gt;
&lt;div class=&quot;b_support col-sm-4 col-xs-12&quot;&gt;
&lt;a title=&quot;Customer support service&quot; href=&quot;#&quot;&gt;&lt;span&gt;&lt;i class=&quot;fa fa-life-ring&quot;&gt;&lt;/i&gt;Customer support service&lt;/span&gt;&lt;/a&gt;
&lt;/div&gt;
&lt;/div&gt;';
					$module_description6[$language['language_id']]['description'] = '&lt;div class=&quot;banner-column&quot;&gt;&lt;a title=&quot;banner&quot; href=&quot;#&quot;&gt;
&lt;img title=&quot;banner&quot; alt=&quot;banner&quot; src=&quot;http://demo.bossthemes.com/claudine_watches/image/catalog/bt_claudine/saveup.png&quot;&gt;&lt;/a&gt;&lt;/div&gt;';
					$module_description7[$language['language_id']]['description'] = '&lt;div class=&quot;bt-video&quot; style=&quot;height:500px&quot;&gt;&lt;div class=&quot;bt_video_text not-animated&quot; data-animate=&quot;flipInY&quot; data-delay=&quot;200&quot;&gt;&lt;span&gt;Amazing video liveshow&lt;/span&gt;&lt;p&gt;Hight fashion 14/15&lt;/p&gt;
&lt;/div&gt;
&lt;div id=&quot;b_parallax&quot; class=&quot;bt_video_banner bt-show&quot; style=&quot;background-image:url(http://demo.bossthemes.com/claudine_watches/image/catalog/bt_claudine/bkg_video.jpg); &quot;&gt;&lt;/div&gt;
&lt;video id=&quot;bt_video&quot; class=&quot;video-js vjs-default-skin&quot; controls=&quot;&quot; preload=&quot;auto&quot; data-setup=&quot;{}&quot; &gt;    &lt;source src=&quot;http://demo.bossthemes.com/claudine_watches/image/catalog/bt_claudine/home_session.mp4&quot; type=&quot;video/mp4&quot;&gt;      &lt;/video&gt;&lt;script type=&quot;text/javascript&quot; src=&quot;catalog/view/javascript/bossthemes/video/video.js&quot;&gt;&lt;/script&gt;&lt;/div&gt;';
					
				}
				$contents = array(
					0 => array(
						'name' => 'Banner Category',
						'module_description' => $module_description1,
						'position' => 'content_top',
						'sort_order' => 1,
						'layout_id' => $this->getIdLayout('category'),   //category page
					),
					1 => array(
						'name' => 'Block Home 1',
						'module_description' => $module_description2,
						'layout' => array(
							0 => array(
								'position' => 'content_top',
								'sort_order' => 2,
								'layout_id' => $this->getIdLayout('home'),  // home page
							),
							
						),
						
					),				
					
					2 => array(
						'name' => 'Block Home 2',
						'module_description' => $module_description3,
						'position' => 'content_top',
						'sort_order' => 4,
						'layout_id' => $this->getIdLayout('home'),   //home page
					),
					3 => array(
						'name' => 'Column Left',
						'module_description' => $module_description4,
						'layout' => array(
							0 => array(
								'position' => 'column_left',
								'sort_order' => 7,
								'layout_id' => $this->getIdLayout('category'),  // category page
							),
							1 => array(
								'position' => 'column_left',
								'sort_order' => 7,
								'layout_id' => $this->getIdLayout('product'),  // Product page
							),
							2 => array(
								'position' => 'column_left',
								'sort_order' => 7,
								'layout_id' => $this->getIdLayout('manufacturer'),  // Manufacturer page
							),
							
						),
					),
					4 => array(
						'name' => 'Free shipping',
						'module_description' => $module_description5,
						'position' => 'btslideshow',
						'sort_order' => 2,
						'layout_id' => 9999,   //all page
					),
					5 => array(
						'name' => 'Save Up',
						'module_description' => $module_description6,
						'position' => 'column_left',
						'sort_order' => 5,
						'layout_id' => $this->getIdLayout('home'),   //home page
					),
					6 => array(
						'name' => 'Video',
						'module_description' => $module_description7,
						'position' => 'content_bottom',
						'sort_order' => 2,
						'layout_id' => $this->getIdLayout('home'),   //home page
					),
					
					
				);
				foreach($contents as $content){
					//create sample data and add module
					$sample_data = array(
						'name' => $content['name'],
						'code' => $code,
						'status' => 1,
						'module_description' => $content['module_description'],
					);
					$this->model_extension_module->addModule($code, $sample_data);
					//get module_id
					$module_id = $this->db->getLastId();
				
					if(isset($content['layout'])){
						foreach($content['layout'] as $layout){
							//add layout		
							$layout_module = array(
								'code'  => $code.'.'.$module_id,
								'position'  => $layout['position'],				
								'sort_order'  => $layout['sort_order'],
							);			
							$layout_id = $layout['layout_id']; 
							$this->db->query("INSERT INTO " . DB_PREFIX . "layout_module SET layout_id = '" . (int)$layout_id . "', code = '" . $this->db->escape($layout_module['code']) . "', position = '" . $this->db->escape($layout_module['position']) . "', sort_order = '" . (int)$layout_module['sort_order'] . "'");
						}
					}else{
						//add layout		
							$layout_module = array(
								'code'  => $code.'.'.$module_id,
								'position'  => $content['position'],				
								'sort_order'  => $content['sort_order'],
							);			
							$layout_id = $content['layout_id']; 
							$this->db->query("INSERT INTO " . DB_PREFIX . "layout_module SET layout_id = '" . (int)$layout_id . "', code = '" . $this->db->escape($layout_module['code']) . "', position = '" . $this->db->escape($layout_module['position']) . "', sort_order = '" . (int)$layout_module['sort_order'] . "'");
					}
				}
			break;
			case 'newslettersubscribe':
				$code = 'newslettersubscribe';
				$this->deleteDataModule($code);
				$this->installModule($code);
				$this->load->model('user/user_group');				
				$this->model_user_user_group->addPermission($this->user->getId(), 'access', 'sale/newssubscribers');
				$this->model_user_user_group->addPermission($this->user->getId(), 'modify', 'sale/newssubscribers');
				$title = array();
				$sub_title = array();
				foreach($languages as $language){
					$title{$language['language_id']} = 'Newletters';
					$sub_title{$language['language_id']} = 'Subscribe to receive our news everyday!';
				}
				//create sample data and add module
				$sample_data = array(
					'name' => 'News Letter',
					'code' => $code,
					'status' => 1,
					'title' => $title,
					'sub_title' => $sub_title,
					'option_unsubscribe' => 0,
					'newslettersubscribe_mail_status' => 0,
					'newslettersubscribe_registered' => 1,
					
				);
				$this->model_extension_module->addModule($code, $sample_data);
				//get module_id
				$module_id = $this->db->getLastId();
				
				$layouts = array(					
					0 =>  array(
						'layout_id' => 9999, // all page		
						'position' => 'btfooter',
						'sort_order' => 1,
					),
									
				);
				foreach($layouts as $layout){
					//add layout		
					$layout_module = array(
						'code'  => $code.'.'.$module_id,
						'position'  => $layout['position'],				
						'sort_order'  => $layout['sort_order'],
					);			
					$layout_id = $layout['layout_id']; 
					$this->db->query("INSERT INTO " . DB_PREFIX . "layout_module SET layout_id = '" . (int)$layout_id . "', code = '" . $this->db->escape($layout_module['code']) . "', position = '" . $this->db->escape($layout_module['position']) . "', sort_order = '" . (int)$layout_module['sort_order'] . "'");
				}
			break;
			case 'boss_newmegamenu':
				$code = 'boss_newmegamenu';
				$this->deleteDataModule($code);
				$this->installModule($code);
				
				//create sample data and add module
				$sample_data = array(
					'name' => 'Megamenu',
					'code' => $code,
					'status' => 1,
					'boss_newmegamenu_module' => array(
						'menu_width' => 1170,
						'num_column' => 6,
						'module_id' => '',
						),
				);
				$this->model_extension_module->addModule($code, $sample_data);
				//get module_id
				$module_id = $this->db->getLastId();
				//edit module_id
				$sample_data_edit = array(
					'name' => 'Megamenu',
					'code' => $code,
					'status' => 1,
					'boss_newmegamenu_module' => array(
						'menu_width' => 1170,
						'num_column' => 6,
						'module_id' => $module_id,
						),
				);
				$this->model_extension_module->editModule($module_id, $sample_data_edit);
				
				$layouts = array(					
					0 =>  array(
						'layout_id' => 9999, // all page		
						'position' => 'btmainmenu',
						'sort_order' => 0,
					),
									
				);
				foreach($layouts as $layout){
					//add layout		
					$layout_module = array(
						'code'  => $code.'.'.$module_id,
						'position'  => $layout['position'],				
						'sort_order'  => $layout['sort_order'],
					);			
					$layout_id = $layout['layout_id']; 
					$this->db->query("INSERT INTO " . DB_PREFIX . "layout_module SET layout_id = '" . (int)$layout_id . "', code = '" . $this->db->escape($layout_module['code']) . "', position = '" . $this->db->escape($layout_module['position']) . "', sort_order = '" . (int)$layout_module['sort_order'] . "'");
				}
				
				
				$this->load->model('bossthemes/boss_newmegamenu');
				$this->model_bossthemes_boss_newmegamenu->createdb();
				
				$this->db->query("update " . DB_PREFIX . "megamenu SET module_id = '" . (int)$module_id . "'");
			break;
			case 'boss_revolutionslider':
					$code = 'boss_revolutionslider';
					$this->deleteDataModule($code);
					$this->installModule($code);
					
					//create sample data and add module
					$sample_data = array(
						'name' => 'Slideshow',
						'code' => $code,
						'status' => 1,
						'slider_id' => 1,
					);
					$this->model_extension_module->addModule($code, $sample_data);
					//get module_id
					$module_id = $this->db->getLastId();
					$layouts = array(					
						0 =>  array(
							'layout_id' => $this->getIdLayout('home'), // home page		
							'position' => 'btslideshow',
							'sort_order' => 3,
						),
										
					);
					foreach($layouts as $layout){
						//add layout		
						$layout_module = array(
							'code'  => $code.'.'.$module_id,
							'position'  => $layout['position'],				
							'sort_order'  => $layout['sort_order'],
						);			
						$layout_id = $layout['layout_id']; 
						$this->db->query("INSERT INTO " . DB_PREFIX . "layout_module SET layout_id = '" . (int)$layout_id . "', code = '" . $this->db->escape($layout_module['code']) . "', position = '" . $this->db->escape($layout_module['position']) . "', sort_order = '" . (int)$layout_module['sort_order'] . "'");
					}
					
					$this->load->model('bossthemes/boss_revolutionslider');
			
					$this->model_bossthemes_boss_revolutionslider->createdb();
					
				
			break;
			case 'bossblog':
				$code = 'bossblog';
				$this->deleteDataModule($code);
				$this->installModule($code);
				$this->load->model('user/user_group');
		
				$this->model_user_user_group->addPermission($this->user->getId(), 'access', 'bossblog/category');
				$this->model_user_user_group->addPermission($this->user->getId(), 'modify', 'bossblog/category');
				
				$this->model_user_user_group->addPermission($this->user->getId(), 'access', 'bossblog/articles');
				$this->model_user_user_group->addPermission($this->user->getId(), 'modify', 'bossblog/articles');
				
				$this->model_user_user_group->addPermission($this->user->getId(), 'access', 'bossblog/comment');
				$this->model_user_user_group->addPermission($this->user->getId(), 'modify', 'bossblog/comment');
				
				$this->model_user_user_group->addPermission($this->user->getId(), 'access', 'bossblog/setting');
				$this->model_user_user_group->addPermission($this->user->getId(), 'modify', 'bossblog/setting');
				$this->load->model('setting/setting');        
				$this->model_setting_setting->deleteSetting('config_bossblog');
				$this->load->model('bossblog/bossblog');					
				$this->model_bossblog_bossblog->dropBlog();
				
				$this->load->model('bossblog/category');					
				$this->model_bossblog_category->checkBlogCategory();
				
				$this->load->model('bossblog/comment');					
				$this->model_bossblog_comment->checkBlogComment();
				
				$this->load->model('bossblog/articles');			
				$this->model_bossblog_articles->checkBlogArticle();
				
				$data = array(
		
					'config_bossblog_name'                  =>'Blog',
					'config_bossblog_limit'             =>5,
					'config_bossblog_admin_limit'           =>5,
					'config_bossblog_comment_status'             =>1,
					'config_bossblog_approval_status'            =>0,
					'config_bossblog_image_category_width' =>880,
					'config_bossblog_image_category_height' =>260,
					'config_bossblog_image_article_width'   =>270,
					'config_bossblog_image_article_height'  =>200,
					'config_bossblog_image_related_width'   =>500,
					'config_bossblog_image_related_height'  =>370,                    
				);
				
				$this->model_setting_setting->editSetting('config_bossblog', $data);
				$special= array ('name' => 'Boss Blog', 'layout_route' => array(
					0 => array ('store_id' => 0, 'route' => 'bossblog/%')) );
        
				$this->addLayout($special);	
			break;	
			case 'blogcategory':
				$code = 'blogcategory';
				$b_check = $this->checkModule('bossblog');
				if(!$b_check){
					$json['error'] = '<i class="fa fa-check-circle"></i> Error! You must install the Boss - Blog module before.';
				}else{				
					$this->deleteDataModule($code);
					$this->installModule($code);
					
					//create sample data and add module
					$sample_data = array(
						'name' => 'Blog Category',
						'code' => $code,
						'status' => 1,
						'count' => 1
					);
					$this->model_extension_module->addModule($code, $sample_data);
					//get module_id
					$module_id = $this->db->getLastId();
					
					$layouts = array(					
						0 =>  array(
							'layout_id' => $this->getIdLayout('Boss Blog'), // product page		
							'position' => 'column_left',
							'sort_order' => 1,
						),
										
					);
					foreach($layouts as $layout){
						//add layout		
						$layout_module = array(
							'code'  => $code.'.'.$module_id,
							'position'  => $layout['position'],				
							'sort_order'  => $layout['sort_order'],
						);			
						$layout_id = $layout['layout_id']; 
						$this->db->query("INSERT INTO " . DB_PREFIX . "layout_module SET layout_id = '" . (int)$layout_id . "', code = '" . $this->db->escape($layout_module['code']) . "', position = '" . $this->db->escape($layout_module['position']) . "', sort_order = '" . (int)$layout_module['sort_order'] . "'");
					}
				}
			break;
			case 'blogrecentcomment':
				$code = 'blogrecentcomment';
				$b_check = $this->checkModule('bossblog');
				if(!$b_check){
					$json['error'] = '<i class="fa fa-check-circle"></i> Error! You must install the Boss - Blog module before.';
				}else{				
					$this->deleteDataModule($code);
					$this->installModule($code);
					$title = array();
					foreach($languages as $language){
						$title{$language['language_id']} = 'Blog Recent Comment';
					}
					//create sample data and add module
					$sample_data = array(
						'name' => 'Blog Recent Comment',
						'code' => $code,
						'status' => 1,
						'title' => $title,
						'limit' => 4,
					);
					$this->model_extension_module->addModule($code, $sample_data);
					//get module_id
					$module_id = $this->db->getLastId();
					
					$layouts = array(					
						0 =>  array(
							'layout_id' => $this->getIdLayout('Boss Blog'), // product page		
							'position' => 'column_left',
							'sort_order' => 4,
						),
										
					);
					foreach($layouts as $layout){
						//add layout		
						$layout_module = array(
							'code'  => $code.'.'.$module_id,
							'position'  => $layout['position'],				
							'sort_order'  => $layout['sort_order'],
						);			
						$layout_id = $layout['layout_id']; 
						$this->db->query("INSERT INTO " . DB_PREFIX . "layout_module SET layout_id = '" . (int)$layout_id . "', code = '" . $this->db->escape($layout_module['code']) . "', position = '" . $this->db->escape($layout_module['position']) . "', sort_order = '" . (int)$layout_module['sort_order'] . "'");
					}
				}
			break;
			case 'blogrecentpost':
				$code = 'blogrecentpost';
				$b_check = $this->checkModule('bossblog');
				if(!$b_check){
					$json['error'] = '<i class="fa fa-check-circle"></i> Error! You must install the Boss - Blog module before.';
				}else{				
					$this->deleteDataModule($code);
					$this->installModule($code);
					$title = array();
					foreach($languages as $language){
						$title{$language['language_id']} = 'Blog Recent Post';
					}
					//create sample data and add module
					$sample_data = array(
						'name' => 'Blog Recent Post',
						'code' => $code,
						'status' => 1,
						'blogrecentpost_module' => array(
							'title' => $title,
							'limit' => 3,
						),						
					);
					$this->model_extension_module->addModule($code, $sample_data);
					//get module_id
					$module_id = $this->db->getLastId();
					
					$layouts = array(					
						0 =>  array(
							'layout_id' => $this->getIdLayout('Boss Blog'), // product page		
							'position' => 'column_left',
							'sort_order' => 3,
						),
										
					);
					foreach($layouts as $layout){
						//add layout		
						$layout_module = array(
							'code'  => $code.'.'.$module_id,
							'position'  => $layout['position'],				
							'sort_order'  => $layout['sort_order'],
						);			
						$layout_id = $layout['layout_id']; 
						$this->db->query("INSERT INTO " . DB_PREFIX . "layout_module SET layout_id = '" . (int)$layout_id . "', code = '" . $this->db->escape($layout_module['code']) . "', position = '" . $this->db->escape($layout_module['position']) . "', sort_order = '" . (int)$layout_module['sort_order'] . "'");
					}
				}
			break;
			case 'blogsearch':
				$code = 'blogsearch';
				$b_check = $this->checkModule('bossblog');
				if(!$b_check){
					$json['error'] = '<i class="fa fa-check-circle"></i> Error! You must install the Boss - Blog module before.';
				}else{				
					$this->deleteDataModule($code);
					$this->installModule($code);
					
					//create sample data and add module
					$sample_data = array(
						'name' => 'Blog Search',
						'code' => $code,
						'status' => 1,
												
					);
					$this->model_extension_module->addModule($code, $sample_data);
					//get module_id
					$module_id = $this->db->getLastId();
					
					$layouts = array(					
						0 =>  array(
							'layout_id' => $this->getIdLayout('Boss Blog'), // product page		
							'position' => 'column_left',
							'sort_order' => 2,
						),
										
					);
					foreach($layouts as $layout){
						//add layout		
						$layout_module = array(
							'code'  => $code.'.'.$module_id,
							'position'  => $layout['position'],				
							'sort_order'  => $layout['sort_order'],
						);			
						$layout_id = $layout['layout_id']; 
						$this->db->query("INSERT INTO " . DB_PREFIX . "layout_module SET layout_id = '" . (int)$layout_id . "', code = '" . $this->db->escape($layout_module['code']) . "', position = '" . $this->db->escape($layout_module['position']) . "', sort_order = '" . (int)$layout_module['sort_order'] . "'");
					}
				}
			break;
			case 'blogtagcloud':
				$code = 'blogtagcloud';
				$b_check = $this->checkModule('bossblog');
				if(!$b_check){
					$json['error'] = '<i class="fa fa-check-circle"></i> Error! You must install the Boss - Blog module before.';
				}else{				
					$this->deleteDataModule($code);
					$this->installModule($code);
					$title = array();
					foreach($languages as $language){
						$title{$language['language_id']} = 'Blog Tag Cloud';
					}
					//create sample data and add module
					$sample_data = array(
						'name' => 'Blog Tag Cloud',
						'code' => $code,
						'status' => 1,
						'blogtagcloud_module' => array(
							'title' => $title,
							'limit' => 3,
							'min_font_size' => 9,
							'max_font_size' => 25,
							'font_weight' => 'normal',
						),						
					);
					$this->model_extension_module->addModule($code, $sample_data);
					//get module_id
					$module_id = $this->db->getLastId();
					
					$layouts = array(					
						0 =>  array(
							'layout_id' => $this->getIdLayout('Boss Blog'), // product page		
							'position' => 'column_left',
							'sort_order' => 5,
						),
										
					);
					foreach($layouts as $layout){
						//add layout		
						$layout_module = array(
							'code'  => $code.'.'.$module_id,
							'position'  => $layout['position'],				
							'sort_order'  => $layout['sort_order'],
						);			
						$layout_id = $layout['layout_id']; 
						$this->db->query("INSERT INTO " . DB_PREFIX . "layout_module SET layout_id = '" . (int)$layout_id . "', code = '" . $this->db->escape($layout_module['code']) . "', position = '" . $this->db->escape($layout_module['position']) . "', sort_order = '" . (int)$layout_module['sort_order'] . "'");
					}
				}
			break;
			case 'boss_blogfeatured':
				$code = 'boss_blogfeatured';
				$b_check = $this->checkModule('bossblog');
				if(!$b_check){
					$json['error'] = '<i class="fa fa-check-circle"></i> Error! You must install the Boss - Blog module before.';
				}else{				
					$this->deleteDataModule($code);
					$this->installModule($code);
					$title = array();
					foreach($languages as $language){
						$title{$language['language_id']} = 'Latest News';
					}
					//create sample data and add module
					$sample_data = array(
						'name' => 'Blog Featured',
						'code' => $code,
						'status' => 1,
						'filter_blog' => 'latest',						
						'title' => $title,
						'useslider' => 0,
						'limit' => 4,
						'limit_article' => 20,
						'limit_des' => 90,
						'image_width' => 500,
						'image_height' => 370,										
					);
					$this->model_extension_module->addModule($code, $sample_data);
					//get module_id
					$module_id = $this->db->getLastId();
					
					$layouts = array(					
						0 =>  array(
							'layout_id' => $this->getIdLayout('home'), // product page		
							'position' => 'content_bottom',
							'sort_order' => 1,
						),
										
					);
					foreach($layouts as $layout){
						//add layout		
						$layout_module = array(
							'code'  => $code.'.'.$module_id,
							'position'  => $layout['position'],				
							'sort_order'  => $layout['sort_order'],
						);			
						$layout_id = $layout['layout_id']; 
						$this->db->query("INSERT INTO " . DB_PREFIX . "layout_module SET layout_id = '" . (int)$layout_id . "', code = '" . $this->db->escape($layout_module['code']) . "', position = '" . $this->db->escape($layout_module['position']) . "', sort_order = '" . (int)$layout_module['sort_order'] . "'");
					}
				}
			break;	
		}					
	
		$json['output'] = '<i class="fa fa-check-circle"></i> Install module success! go to module page to see the changes';
		$this->response->addHeader('Content-Type: application/json');
		$this->response->setOutput(json_encode($json));
	}
	public function addLayout($data) {
		$name=$this->db->escape($data['name']);
		$this->deleteLayout($name);
		$this->db->query("INSERT INTO " . DB_PREFIX . "layout SET name = '" . $this->db->escape($data['name']) . "'");
		
		$layout_id = $this->db->getLastId();
		
		if (isset($data['layout_route'])) {
			foreach ($data['layout_route'] as $layout_route) {
				$this->db->query("INSERT INTO " . DB_PREFIX . "layout_route SET layout_id = '" . (int)$layout_id . "', store_id = '" . (int)$layout_route['store_id'] . "', route = '" . $this->db->escape($layout_route['route']) . "'");
			}	
		}
	}
	private function deleteLayout($layout_name) {
    	$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "layout WHERE name = '".$layout_name."'");
    	if($query->num_rows){
    		$layout_id = $query->row['layout_id'];
    		$this->db->query("DELETE FROM " . DB_PREFIX . "layout WHERE layout_id = '" . (int)$layout_id . "'");
    		$this->db->query("DELETE FROM " . DB_PREFIX . "layout_route WHERE layout_id = '" . (int)$layout_id . "'");
    		$this->db->query("DELETE FROM " . DB_PREFIX . "category_to_layout WHERE layout_id = '" . (int)$layout_id . "'");
    		$this->db->query("DELETE FROM " . DB_PREFIX . "product_to_layout WHERE layout_id = '" . (int)$layout_id . "'");
    		$this->db->query("DELETE FROM " . DB_PREFIX . "information_to_layout WHERE layout_id = '" . (int)$layout_id . "'");	
    		}
   	}
}
?>