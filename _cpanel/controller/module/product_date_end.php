<?php
require_once(substr_replace(DIR_SYSTEM, '', -7) . 'vendor/equotix/product_date_end/equotix.php');
class ControllerModuleProductDateEnd extends Equotix {
	protected $version = '1.1.2';
	protected $code = 'product_date_end';
	protected $extension = 'Product Date End';
	protected $extension_id = '53';
	protected $purchase_url = 'product-date-end';
	protected $purchase_id = '20703';
	protected $error = array();
	
	public function index() {   
		$this->language->load('module/product_date_end');

		$this->document->setTitle(strip_tags($this->language->get('heading_title')));
		
		if (isset($this->session->data['success'])) {
			$data['success'] = $this->session->data['success'];
			
			unset($this->session->data['success']);
		} else {
			$data['success'] = '';
		}
		
		$data['heading_title'] = $this->language->get('heading_title');
		
		$data['text_edit'] = $this->language->get('text_edit');
		$data['text_congratulations'] = $this->language->get('text_congratulations');
		
		$data['tab_general'] = $this->language->get('tab_general');
		
		$data['button_cancel'] = $this->language->get('button_cancel');

  		$data['breadcrumbs'] = array();

   		$data['breadcrumbs'][] = array(
       		'text'      => $this->language->get('text_home'),
			'href'      => $this->url->link('common/home', 'token=' . $this->session->data['token'], 'SSL')
   		);

   		$data['breadcrumbs'][] = array(
       		'text'      => $this->language->get('text_module'),
			'href'      => $this->url->link('extension/module', 'token=' . $this->session->data['token'], 'SSL')
   		);
		
   		$data['breadcrumbs'][] = array(
       		'text'      => $this->language->get('heading_title'),
			'href'      => $this->url->link('module/product_date_end', 'token=' . $this->session->data['token'], 'SSL')
   		);
		
		$data['cancel'] = $this->url->link('extension/module', 'token=' . $this->session->data['token'], 'SSL');
		
		$data['header'] = $this->load->controller('common/header');
		$data['column_left'] = $this->load->controller('common/column_left');
		$data['footer'] = $this->load->controller('common/footer');

		$this->generateOutput('module/product_date_end.tpl', $data);
	}
	
	public function install() {
		$this->language->load('module/product_date_end');

		$path = substr_replace(DIR_SYSTEM, '', -7);

		if (file_exists($path . 'vqmod/xml/product_date_end.xml_')) {
			rename($path . 'vqmod/xml/product_date_end.xml_', $path . 'vqmod/xml/product_date_end.xml');
			
			$this->session->data['success'] = $this->language->get('text_success');
		}
		
		$query = $this->db->query("SHOW COLUMNS FROM " . DB_PREFIX . "product");
		
		$exists = false;
		
		foreach ($query->rows as $result) {
			if ($result['Field'] == 'date_end') {
				$exists = true;
				break;
			}
		}
		if (!$exists) {
			$this->db->query("ALTER TABLE " . DB_PREFIX . "product ADD date_end date NOT NULL");
		}

		$this->response->redirect($this->url->link('module/product_date_end', 'token=' . $this->session->data['token'], 'SSL'));
	}
	
	public function uninstall() {
		$this->language->load('module/product_date_end');

		$path = substr_replace(DIR_SYSTEM, '', -7);

		if (file_exists($path . 'vqmod/xml/product_date_end.xml')) {
			rename($path . 'vqmod/xml/product_date_end.xml', $path . 'vqmod/xml/product_date_end.xml_');

			$this->session->data['success'] = $this->language->get('text_success');
		}
		
		$query = $this->db->query("SHOW COLUMNS FROM " . DB_PREFIX . "product");
		
		$exists = false;
		
		foreach ($query->rows as $result) {
			if ($result['Field'] == 'date_end') {
				$exists = true;
				break;
			}
		}
		
		if ($exists) {
			$this->db->query("ALTER TABLE " . DB_PREFIX . "product DROP date_end");
		}
	}
}