<?php
class ControllerModuleSeller extends Controller {
	private $error = array();

	public function index() {
		$this->load->language('module/seller');

		$this->document->setTitle($this->language->get('heading_title'));

		$this->load->model('setting/setting');

		if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validate()) {
			$this->model_setting_setting->editSetting('seller', $this->request->post);
			
			$this->db->query("UPDATE " . DB_PREFIX . "product SET  approve = '1'");
			$this->db->query("UPDATE " . DB_PREFIX . "category SET  approve = '1'");
			$this->db->query("UPDATE " . DB_PREFIX . "option SET  approve = '1'");
			$this->db->query("UPDATE " . DB_PREFIX . "attribute SET  approve = '1'");
			   

			$this->session->data['success'] = $this->language->get('text_success');

			$this->response->redirect($this->url->link('extension/module', 'token=' . $this->session->data['token'], 'SSL'));
		}

		$data['heading_title'] = $this->language->get('heading_title');

		$data['text_edit'] = $this->language->get('text_edit');
		$data['text_enabled'] = $this->language->get('text_enabled');
		$data['text_disabled'] = $this->language->get('text_disabled');
		
		$data['entry_product'] = $this->language->get('entry_product');
		$data['entry_limit'] = $this->language->get('entry_limit');
		$data['entry_image'] = $this->language->get('entry_image');
		$data['entry_width'] = $this->language->get('entry_width');
		$data['entry_height'] = $this->language->get('entry_height');
		$data['entry_status'] = $this->language->get('entry_status');

		$data['help_product'] = $this->language->get('help_product');

		$data['button_save'] = $this->language->get('button_save');
		$data['button_cancel'] = $this->language->get('button_cancel');
		$data['button_module_add'] = $this->language->get('button_module_add');
		$data['button_remove'] = $this->language->get('button_remove');

		if (isset($this->error['warning'])) {
			$data['error_warning'] = $this->error['warning'];
		} else {
			$data['error_warning'] = '';
		}

		if (isset($this->error['image'])) {
			$data['error_image'] = $this->error['image'];
		} else {
			$data['error_image'] = array();
		}

		$data['breadcrumbs'] = array();

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_home'),
			'href' => $this->url->link('common/dashboard', 'token=' . $this->session->data['token'], 'SSL')
		);

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_module'),
			'href' => $this->url->link('extension/module', 'token=' . $this->session->data['token'], 'SSL')
		);

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('heading_title'),
			'href' => $this->url->link('module/seller', 'token=' . $this->session->data['token'], 'SSL')
		);

		$data['action'] = $this->url->link('module/seller', 'token=' . $this->session->data['token'], 'SSL');

		$data['cancel'] = $this->url->link('extension/module', 'token=' . $this->session->data['token'], 'SSL');

		$data['token'] = $this->session->data['token'];

		if (isset($this->request->post['seller_product'])) {
			$data['seller_product'] = $this->request->post['seller_product'];
		} else {
			$data['seller_product'] = $this->config->get('seller_product');
		}

		$this->load->model('sale/seller');

		if (isset($this->request->post['seller_product'])) {
			$sellers = explode(',', $this->request->post['seller_product']);
		} else {
			$sellers = explode(',', $this->config->get('seller_product'));
		}

		if (isset($this->request->post['seller_width'])) {
			$data['seller_width'] = $this->request->post['seller_width'];
		} else {
			$data['seller_width'] = $this->config->get('seller_width');
		}

		if (isset($this->request->post['seller_height'])) {
			$data['seller_height'] = $this->request->post['seller_height'];
		} else {
			$data['seller_height'] = $this->config->get('seller_height');
		}


		$data['sellers'] = array();

		foreach ($sellers as $seller_id) {
			$seller_info = $this->model_sale_seller->getseller($seller_id);

			if ($seller_info) {
			$name = $seller_info['firstname'].' '.$seller_info['lastname'];

				$data['sellers'][] = array(
					'seller_id' => $seller_info['seller_id'],
					'name'       => $name
				);
			}
		}
		
		if (isset($this->request->post['seller_status'])) {
			$data['seller_status'] = $this->request->post['seller_status'];
		} else {
			$data['seller_status'] = $this->config->get('seller_status');
		}
			
		$data['header'] = $this->load->controller('common/header');
		$data['column_left'] = $this->load->controller('common/column_left');
		$data['footer'] = $this->load->controller('common/footer');

		$this->response->setOutput($this->load->view('module/seller.tpl', $data));
	}

	protected function validate() {
		if (!$this->user->hasPermission('modify', 'module/seller')) {
			$this->error['warning'] = $this->language->get('error_permission');
		}
		return !$this->error;
	}
}
