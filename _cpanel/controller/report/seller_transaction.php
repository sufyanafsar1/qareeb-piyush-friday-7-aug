<?php
class ControllerReportSellerTransaction extends Controller { 

public function download() {
		$this->load->language( 'tool/export_import' );
		
		$this->document->setTitle($this->language->get('heading_title'));
		
		$this->load->model( 'tool/export_import' );
		
		   @ini_set("memory_limit", "128M");
           
		   @set_time_limit(1800);

          if (isset($this->request->get['filter_date_start'])) {
			$filter_date_start = $this->request->get['filter_date_start'];
		} else {
			$filter_date_start = '';
		}

		if (isset($this->request->get['filter_date_end'])) {
			$filter_date_end = $this->request->get['filter_date_end'];
		} else {
			$filter_date_end = '';
		}
		
		if (isset($this->request->get['filter_seller_group'])) {
			$filter_seller_group = $this->request->get['filter_seller_group'];
		} else {
			$filter_seller_group = 0;
		}
		
		if (isset($this->request->get['filter_paid_status'])) {
			$filter_paid_status = $this->request->get['filter_paid_status'];
		} else {
			$filter_paid_status = 0;
		}
		
		if (isset($this->request->get['filter_order_status_id'])) {
			$filter_order_status_id = $this->request->get['filter_order_status_id'];
		} else {
			$filter_order_status_id = 0;
		}	
		
          
		  $data1 = array(
			'filter_date_start'	     => $filter_date_start, 
			'filter_date_end'	     => $filter_date_end, 
			'filter_seller_group'    => $filter_seller_group,
			'filter_paid_status'     => $filter_paid_status,
			'filter_order_status_id' => $filter_order_status_id
		);
		
		
           $this->model_tool_export_import->download($data1,'a', null, null, null, null);

		
	}
	
	public function download1() {
		$this->load->language( 'tool/export_import' );
		
		$this->document->setTitle($this->language->get('heading_title'));
		
		$this->load->model( 'tool/export_import' );
		
		   @ini_set("memory_limit", "128M");
           
		   @set_time_limit(1800);

        
		
		
           $this->model_tool_export_import->download1($data1=array(),'a', null, null, null, null);

		
	}

	public function index() {  
		$this->load->language('report/seller_transaction');

		$this->document->setTitle($this->language->get('heading_title'));
		
		$this->load->model('report/seller_transaction');
		
		
		if (isset($this->request->get['filter_date_start'])) {
			$filter_date_start = $this->request->get['filter_date_start'];
		} else {
			$filter_date_start = '';
		}

		if (isset($this->request->get['filter_date_end'])) {
			$filter_date_end = $this->request->get['filter_date_end'];
		} else {
			$filter_date_end = '';
		}
		
		if (isset($this->request->get['filter_seller_group'])) {
			$filter_seller_group = $this->request->get['filter_seller_group'];
		} else {
			$filter_seller_group = 0;
		}
		
		if (isset($this->request->get['filter_paid_status'])) {
			$filter_paid_status = $this->request->get['filter_paid_status'];
		} else {
			$filter_paid_status = 0;
		}
		
		if (isset($this->request->get['filter_order_status_id'])) {
			$filter_order_status_id = $this->request->get['filter_order_status_id'];
		} else {
			$filter_order_status_id = 0;
		}	
		$filter_order_status_id = implode(",",$this->config->get('config_complete_status'));
		if (isset($this->request->get['page'])) {
			$page = $this->request->get['page'];
		} else {
			$page = 1;
		}
		
		$url = '';
						
		if (isset($this->request->get['filter_date_start'])) {
			$url .= '&filter_date_start=' . $this->request->get['filter_date_start'];
		}
		
		if (isset($this->request->get['filter_date_end'])) {
			$url .= '&filter_date_end=' . $this->request->get['filter_date_end'];
		}
		
		if (isset($this->request->get['filter_seller_group'])) {
			$url .= '&filter_seller_group=' . $this->request->get['filter_seller_group'];
		}
		
		if (isset($this->request->get['filter_paid_status'])) {
			$url .= '&filter_paid_status=' . $this->request->get['filter_paid_status'];
		}
		
		if (isset($this->request->get['filter_order_status_id'])) {
			$url .= '&filter_order_status_id=' . $this->request->get['filter_order_status_id'];
		}
								
		if (isset($this->request->get['page'])) {
			$url .= '&page=' . $this->request->get['page'];
		}

   		$data['breadcrumbs'] = array();

   		$data['breadcrumbs'][] = array(
       		'text'      => $this->language->get('text_home'),
			'href'      => $this->url->link('common/home', 'token=' . $this->session->data['token'], 'SSL'),       		
      		'separator' => false
   		);

   		$data['breadcrumbs'][] = array(
       		'text'      => $this->language->get('heading_title'),
			'href'      => $this->url->link('report/seller_transaction', 'token=' . $this->session->data['token'] . $url, 'SSL'),
      		'separator' => ' :: '
   		);
		
		$data['export_import'] = $this->url->link('report/seller_transaction/download', 'token=' . $this->session->data['token'] . $url, 'SSL');
	
	
	  $data['export_import1'] = $this->url->link('report/seller_transaction/download1', 'token=' . $this->session->data['token'], 'SSL');
	
		
		$data['isseller'] = false;
		$seller_access = $this->model_report_seller_transaction->getSellersName1();
		$seller_access = implode(",",$seller_access);
		$this->load->model('report/seller_transaction');
		
		$data['orders']    = array();
		$data['incomes']   = array();
		$data1 = array(
			'filter_date_start'	     => $filter_date_start, 
			'filter_date_end'	     => $filter_date_end, 
			'filter_seller_group'    => $filter_seller_group,
			'filter_paid_status'     => $filter_paid_status,
			'filter_order_status_id' => $filter_order_status_id,
			'start'                  => ($page - 1) * $this->config->get('config_admin_limit'),
			'limit'                  => $this->config->get('config_admin_limit')
		);
		
		$order_total = $this->model_report_seller_transaction->getSellerTotalOrders($data1,$seller_access);
		$results = $this->model_report_seller_transaction->getSellerOrders($data1,$seller_access);
		
		$balances = $this->model_report_seller_transaction->getSellerTotalAmount($data1,$seller_access);
		
		
		
		$data['sellers_name'] = $this->model_report_seller_transaction->getSellersName($data1,$seller_access);
		
		$payment_histories = $this->model_report_seller_transaction->getPaymentHistory(0,$seller_access);

		$this->load->model('localisation/order_status');
		$data['order_statuses'] = $this->model_localisation_order_status->getOrderStatuses();
		
		$data['storerevenue']		= 0;
		$data['storecommision']	= 0;
		$data['sellersbalance']	= 0;
		foreach ($balances AS $balance) {
			$data1 = array(
				'filter_paid_status'     => 0,
				
					'filter_paid_status'     => 0,
				'filter_date_start'	     => $filter_date_start, 
			   'filter_date_end'	     => $filter_date_end, 
				'filter_order_status_id' => $filter_order_status_id
			);

			$orders = $this->model_report_seller_transaction->getSellersOrders($data1,$balance['seller_id']);
			$sellerproducts = array();
			foreach ($orders as $order) {
				$customerinfo = $this->model_report_seller_transaction->getcustomerinfo($order['order_id']);
				$sellerproducts[] = array(
					'product_id'	=> $order['product_id'],
					'date' 			=> date($this->language->get('date_format_short'), strtotime($order['date'])),
					'order_id'  	=> $order['order_id'],
					'order_status'  => $order['order_status'],
					'product_name'  => $order['product_name'],
					'price'         => $this->currency->format(($order['price']-$order['commission']), $this->config->get('config_currency')),
					'quantity'  	=> $order['quantity'],
					'com_ratio'		=> ' <br/>(-' .$order['commission']. '% commission)',
					'commission'  	=> $this->currency->format($order['commission'], $this->config->get('config_currency')),
					'total'			=> $this->currency->format($order['total'], $this->config->get('config_currency')),
					'amount'  		=> $this->currency->format($order['amount'], $this->config->get('config_currency')),
					'paid_status'   => $order['paid_status'],
					'customerinfo'  => $customerinfo
				);
			}
			if(!isset($balance['seller_id'])){
				$balance['seller_id'] = "";
			}
			if(!isset($balance['paypal_email'])){
				$balance['paypal_email'] = "";
			}
			if(!isset($balance['company'])){
				$balance['company'] = "";
			}
			
			
			$data['storerevenue']   += $balance['seller_amount'];
			$data['storecommision'] += $balance['commission'];
			$data['sellersbalance'] += $balance['gross_amount'];
			$data['incomes'][] = array (
				'seller_id'			=> $balance['seller_id'],
				'order_product_id'			=> $balance['order_product_id'],
				'paypal_email'		=> $balance['paypal_email'],
				'company'			=> $balance['company'],
				
				'quantity'			=> $balance['quantity'],
				'paid_amount'		=> $balance['seller_amount'],
				'sellerproducts'    => $sellerproducts,
				'amounttopay1'=> $balance['seller_amount'],
				'seller_amount'  	=> $this->currency->format($balance['seller_amount'], $this->config->get('config_currency')),
				'commission'  		=> $this->currency->format($balance['commission'], $this->config->get('config_currency')),
				'gross_amount'  	=> $this->currency->format($balance['gross_amount'], $this->config->get('config_currency')),
				'href'      => $this->url->link('report/seller_transaction/insert', 'order_product_ids='.$balance['order_product_id'].'
				&seller_id='.$balance['seller_id'].'&amount='.$balance['seller_amount'].'&order_id='.$balance['order_id'].'&token=' . $this->session->data['token']. $url, 'SSL')
			
			);
			
			
			$data['lc'] = $this->config->get('config_admin_language');

			$data['return'] = $this->url->link('report/seller_transaction', 'token=' . $this->session->data['token'] . $url, 'SSL');

			$data['notify_url'] = $this->url->link('report/seller_transaction/callback', 'token=' . $this->session->data['token'] . $url, 'SSL');

			$data['cancelURL'] = $this->url->link('report/seller_transaction', 'token=' . $this->session->data['token'] . $url, 'SSL');
			
			
			$data['currency'] =  $this->config->get('config_currency');
			
			$data['paymentaction'] = 'Payment';
			
			$data['custom'] = $balance['seller_id'];
		}

		$data['oldincomes']   = array();
		$data1 = array(
			'filter_date_start'	     => $filter_date_start, 
			'filter_date_end'	     => $filter_date_end, 
			'filter_seller_group'    => $filter_seller_group,
			'filter_paid_status'     => 1,
			'filter_order_status_id' => $filter_order_status_id,
			'start'                  => ($page - 1) * $this->config->get('config_admin_limit'),
			'limit'                  => $this->config->get('config_admin_limit')
		);
		
		$order_total = $this->model_report_seller_transaction->getSellerTotalOrders($data1,$seller_access);
		$results = $this->model_report_seller_transaction->getSellerOrders($data1,$seller_access);
		
		$balances = $this->model_report_seller_transaction->getSellerTotalAmount($data1,$seller_access);
		$data['sellers_name'] = $this->model_report_seller_transaction->getSellersName($data1,$seller_access);
		
		$payment_histories = $this->model_report_seller_transaction->getPaymentHistory(0,$seller_access);

		$this->load->model('localisation/order_status');
		$data['order_statuses'] = $this->model_localisation_order_status->getOrderStatuses();
		
		
		$paidbalances = $this->model_report_seller_transaction->getSellerpaidTotalAmount12($data,$seller_access);
		
		$paid_total = count($paidbalances);
		
		
		
		foreach ($paidbalances AS $pbalance) {
			$data12 = array(
				'filter_paid_status'     => 0,
				'filter_order_status_id' => $filter_order_status_id
			);

			$orders = $this->model_report_seller_transaction->newgetSellersOrders($pbalance['seller_id'],$pbalance['order_product_id']);
			$sellerproducts = array();
			
			
			foreach ($orders as $order) {
				$customerinfo = $this->model_report_seller_transaction->getcustomerinfo($order['order_id']);
				$sellerproducts[] = array(
					'product_id'	=> $order['product_id'],
					'date' 			=> date($this->language->get('date_format_short'), strtotime($order['date_added'])),
					'order_id'  	=> $order['order_id'],
					
					'product_name'  => $order['name'],
					
					'price'         => $this->currency->format(($order['price']-$order['commission']), $this->config->get('config_currency')),
					'quantity'  	=> $order['quantity'],
					'com_ratio'		=> ' <br/>(-' .$order['commission']. '% commission)',
					'commission'  	=> $this->currency->format($order['commission'], $this->config->get('config_currency')),
					'total'			=> $this->currency->format($order['total'], $this->config->get('config_currency'))
					
				);
			}
			if(!isset($pbalance['seller_id'])){
				$pbalance['seller_id'] = "";
			}
			if(!isset($pbalance['paypal_email'])){
				$pbalance['paypal_email'] = "";
			}
			if(!isset($pbalance['company'])){
				$pbalance['company'] = "";
			}

			$data['oldincomes'][] = array (
				'seller_id'			=> $pbalance['seller_id'],
				
				'order_product_id'			=> $pbalance['order_product_id'],
				
				'paypal_email'		=> $pbalance['paypal_email'],
				'company'			=> $pbalance['company'],
				'quantity'			=> $pbalance['quantity'],
				
				'payment_date'			=> $pbalance['payment_date'],
				
				'payment_description'			=> $pbalance['payment_description'],
				
				'paid_amount'		=> $pbalance['seller_amount'],
				'sellerproducts'    => $sellerproducts,
				'seller_amount'  	=> $this->currency->format($pbalance['seller_amount'], $this->config->get('config_currency')),
				'commision'  		=> '',
				'gross_amount'  	=> $this->currency->format($pbalance['gross_amount'], $this->config->get('config_currency'))
			);
		}
		
		$data['heading_title'] = $this->language->get('heading_title');
		
		$data['text_no_results'] = $this->language->get('text_no_results');
		$data['text_all_status'] = $this->language->get('text_all_status');
		$data['text_all_sellers'] = $this->language->get('text_all_sellers');
		$data['text_gross_incomes'] = $this->language->get('text_gross_incomes');
		$data['text_commision'] = $this->language->get('text_commision');
		$data['text_seller_earning'] = $this->language->get('text_seller_earning');
		$data['text_payment_history'] = $this->language->get('text_payment_history');
		$data['text_seller_payment_history'] = $this->language->get('text_seller_payment_history');
		$data['text_yes'] = $this->language->get('text_yes');
		$data['text_no'] = $this->language->get('text_no');
		$data['text_wait'] = $this->language->get('text_wait');
		
		$data['column_buyer_id'] = $this->language->get('column_buyer_id');
		$data['column_buyer_name'] = $this->language->get('column_buyer_name');
		$data['column_buy_date'] = $this->language->get('column_buy_date');
		$data['column_date_added'] = $this->language->get('column_date_added');
		$data['column_order_id'] = $this->language->get('column_order_id');
    	$data['column_product_name'] = $this->language->get('column_product_name');
		$data['column_unit_price'] = $this->language->get('column_unit_price');
		$data['column_quantity'] = $this->language->get('column_quantity');
		$data['column_commision'] = $this->language->get('column_commision');
		$data['column_amount'] = $this->language->get('column_amount');
		$data['column_total'] = $this->language->get('column_total');
		$data['column_transaction_status'] = $this->language->get('column_transaction_status');
		$data['column_paid_status'] = $this->language->get('column_paid_status');
		$data['column_seller_name'] = $this->language->get('column_seller_name');
		$data['column_seller_id'] = $this->language->get('column_seller_id');
		$data['column_action'] = $this->language->get('column_action');
		$data['column_payment_amount'] = $this->language->get('column_payment_amount');
		$data['column_payment_date'] = $this->language->get('column_payment_date');
		$data['column_order_product'] = $this->language->get('column_order_product');
		
		$data['entry_date_start'] = $this->language->get('entry_date_start');
		$data['entry_date_end'] = $this->language->get('entry_date_end');
		$data['entry_group'] = $this->language->get('entry_group');	
		$data['entry_order_status'] = $this->language->get('entry_order_status');
		$data['entry_status'] = $this->language->get('entry_status');
		
		$data['button_Paypal'] = $this->language->get('button_Paypal');
		$data['button_addPayment'] = $this->language->get('button_addPayment');
		$data['button_filter'] = $this->language->get('button_filter');
		
		$data['token'] = $this->session->data['token'];
		
		$this->load->model('localisation/order_status');
		
		$data['order_statuses'] = $this->model_localisation_order_status->getOrderStatuses();

		$url = '';
						
		if (isset($this->request->get['filter_date_start'])) {
			$url .= '&filter_date_start=' . $this->request->get['filter_date_start'];
		}
		
		if (isset($this->request->get['filter_date_end'])) {
			$url .= '&filter_date_end=' . $this->request->get['filter_date_end'];
		}
		
		if (isset($this->request->get['filter_seller_group'])) {
			$url .= '&filter_seller_group=' . $this->request->get['filter_seller_group'];
		}
		
		if (isset($this->request->get['filter_paid_status'])) {
			$url .= '&filter_paid_status=' . $this->request->get['filter_paid_status'];
		}
		
		if (isset($this->request->get['filter_order_status_id'])) {
			$url .= '&filter_order_status_id=' . $this->request->get['filter_order_status_id'];
		}
				
		$pagination = new Pagination();
		$pagination->total = $paid_total;
		$pagination->page = $page;
		$pagination->limit = $this->config->get('config_admin_limit');
		$pagination->text = $this->language->get('text_pagination');
		$pagination->url = $this->url->link('report/seller_transaction', 'token=' . $this->session->data['token'] . $url . '&page={page}', 'SSL');
			
		$data['pagination'] = $pagination->render();	

        $data['results'] = sprintf($this->language->get('text_pagination'), ($paid_total) ? (($page - 1) * $this->config->get('config_limit_admin')) + 1 : 0, ((($page - 1) * $this->config->get('config_limit_admin')) > ($paid_total - $this->config->get('config_limit_admin'))) ? $paid_total : ((($page - 1) * $this->config->get('config_limit_admin')) + $this->config->get('config_limit_admin')), $paid_total, ceil($paid_total / $this->config->get('config_limit_admin')));
		

		$data['filter_date_start'] = $filter_date_start;
		$data['filter_date_end'] = $filter_date_end;	
		$data['filter_seller_group'] = $filter_seller_group;	
		$data['filter_paid_status'] = $filter_paid_status;		
		$data['filter_order_status_id'] = $filter_order_status_id;
		
		$data['addPayment'] = $this->url->link('report/seller_transaction/insert', 'token=' . $this->session->data['token'] . $url, 'SSL');
				 
		$data['header'] = $this->load->controller('common/header');
		$data['column_left'] = $this->load->controller('common/column_left');
		$data['footer'] = $this->load->controller('common/footer');

		$this->response->setOutput($this->load->view('report/seller_transaction.tpl', $data));
		
		
	}
	
	public function insert() {
    	$this->load->language('report/seller_transaction');

    	$this->document->setTitle('Pay to seller'); 
		$data['heading_title'] = 'Pay to seller';
		$this->load->model('report/seller_transaction');
		$this->load->model('sale/seller');
		if(isset($this->session->data['token'])){
			$data['token'] = $this->session->data['token'];
		}else{
			$data['token'] = '';
		}
		$url = '';
		
	
		
							
		if (isset($this->request->get['filter_date_start'])) {
			$url .= '&filter_date_start=' . $this->request->get['filter_date_start'];
		}
		
		if (isset($this->request->get['filter_date_end'])) {
			$url .= '&filter_date_end=' . $this->request->get['filter_date_end'];
		}
		
		if (isset($this->request->get['amount'])) {
			$url .= '&amount=' . $this->request->get['amount'];
		}
		
		if (isset($this->request->get['seller_id'])) {
			$url .= '&seller_id=' . $this->request->get['seller_id'];
		}
		
		if (isset($this->request->get['order_id'])) {
			$url .= '&order_id=' . $this->request->get['order_id'];
		}
		
		if (isset($this->request->get['order_product_ids'])) {
			$url .= '&order_product_ids=' . $this->request->get['order_product_ids'];
		}
		
		if (isset($this->request->get['filter_paid_status'])) {
			$url .= '&filter_paid_status=' . $this->request->get['filter_paid_status'];
		}
		
		if (isset($this->request->get['filter_order_status_id'])) {
			$url .= '&filter_order_status_id=' . $this->request->get['filter_order_status_id'];
		}
		
			
		
if (isset($this->request->get['filter_date_start'])) {
			$filter_date_start = $this->request->get['filter_date_start'];
		} else {
			$filter_date_start = '';
		}

		if (isset($this->request->get['filter_date_end'])) {
			$filter_date_end = $this->request->get['filter_date_end'];
		} else {
			$filter_date_end = '';
		}
		
		if(isset($this->request->get['order_product_ids'])){
			$data['order_product_ids'] = $this->request->get['order_product_ids'];
		}else{
			$data['order_product_ids'] = '';
		}
		
		
		
		if(isset($this->request->get['amount'])){
			$data['amount'] = $this->request->get['amount'];
		}else{
			$data['amount'] = 0;
		}
		
		if(isset($this->request->get['order_id'])){
			$data['order_id'] = $this->request->get['order_id'];
		}else{
			$data['order_id'] = "";
		}
		
		
	
		
		

		$data['insert'] = $this->url->link('report/seller_transaction/insert', 'token=' . $this->session->data['token'] . $url, 'SSL');
		
    	if (($this->request->server['REQUEST_METHOD'] == 'POST')) {
	
			$selleramt = $this->request->post['amount'];
			
			$seller_info = $this->model_sale_seller->getSeller($this->request->post['seller_id']);
			
			
			if($selleramt > 0){
			
			if (isset($this->request->post['order_product_ids'])) {
			
			$vars = $this->request->post['order_product_ids'];
			
			 $ids = explode(',',$vars);
			 
			 $c = COUNT($ids);
			 
			 if($c > 1){
			 
			 $nids =$ids;
			 
			 }else{
			 
			 $nids[] =$vars;
			 
			 }
			 
			 
		}
		
		
		
		
		
			$this->model_report_seller_transaction->addPaymentToSellerId($selleramt,$this->request->post,$seller_info,$nids);
			
		}
			$this->response->redirect($this->url->link('report/seller_transaction', 'token=' . $this->session->data['token'], 'SSL'));
    	}
	
		if(isset($this->request->get['seller_id'])){
    		$data['seller_id'] = $this->request->get['seller_id'];
		}else{
			$data['seller_id'] = 0;
		}
		$seller_info = $this->model_sale_seller->getSeller($data['seller_id']);	
		$data['bank_name']	= "";
		$data['account_number']= "";
		$data['account_name']	= "";
		$data['branch']		= "";
		$data['ifsccode']		= "";
		$data['telephone']	= "";
		$data['business_name']= "";
		$data['vat_number']	= "";

		if($seller_info){
			$data['bank_name']	= $seller_info['bank_name'];
			$data['account_number']=$seller_info['account_number'];
			$data['account_name']	= $seller_info['account_name'];
			$data['branch']		= $seller_info['branch'];
			$data['ifsccode']		= $seller_info['ifsccode'];
			$data['telephone']	= $seller_info['telephone'];
			
			$data['name']= $seller_info['firstname']." ".$seller_info['lastname'];
			
		}
		
		$data['header'] = $this->load->controller('common/header');
		$data['column_left'] = $this->load->controller('common/column_left');
		$data['footer'] = $this->load->controller('common/footer');

		$this->response->setOutput($this->load->view('report/seller_payment.tpl', $data));
		
		
		
		
		}
	
	public function callback() {	
	
	
	
		if (isset($this->request->post['custom'])) {
		
		    $sid = explode('#',$this->request->post['custom']);
			$order_product_id = explode(',',$sid['0']);
        	$seller_id = $sid['1'];
			
		} else {
		   $order_product_id =array();
		
			$seller_id = 0;
			
		}
		
		
		
		
		$this->load->model('report/seller_transaction');
		
		if($order_product_id){
		
			foreach ($order_product_id as $key => $value) {
			
			$this->model_report_seller_transaction->updateorderproduct($seller_id,$value);
			
			}
		
		}	

       	
		
        $url = "";		
		
		$this->model_report_seller_transaction->updatetransaction($seller_id,$this->request->post['mc_gross']);
		
		$this->response->redirect($this->url->link('report/seller_transaction', 'token=' . $this->session->data['token'], 'SSL'));
				
			
	}
	
	public function removeHistory() {
		$this->language->load('report/seller_transaction');

		$this->load->model('report/seller_transaction');

		$json = array();

		if (!$this->user->hasPermission('modify', 'report/seller_transaction')) {
      		$json['error'] = $this->language->get('error_permission');
    	} else {
		
			if (isset($this->request->get['payment_id'])) {
				$this->model_report_seller_transaction->removeHistory($this->request->get['payment_id']);
			}
		}

		$this->response->setOutput(json_encode($json));
  	}
}
?>