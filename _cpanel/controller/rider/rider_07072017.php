<?php
class ControllerRiderRider extends Controller {
    
    public function index(){
     
       	$this->load->language('rider/rider');

		$this->document->setTitle($this->language->get('heading_title'));
		

		$this->getList();
        
    }
    
    public function getAreas()
    {
        $this->load->language('rider/rider');
		$this->document->setTitle($this->language->get('heading_area_title'));
		$this->getAreaList();
        
    }
    
    public function add() {
		$this->load->language('rider/rider');

		$this->document->setTitle($this->language->get('heading_title'));
		$this->getForm();

	}
     public function edit() {
      
		$this->load->language('rider/rider');

		$this->document->setTitle($this->language->get('heading_title'));
		$this->getForm();

	}
    public function addArea() {
		$this->load->language('rider/rider');

		$this->document->setTitle($this->language->get('heading_area_title'));
		$this->getAreaForm();

	}
    
    
    public function getForm() {
	$this->load->model('rider/rider');

		$data['heading_title'] = $this->language->get('heading_title');

		$data['text_form'] = !isset($this->request->get['rider_id']) ? $this->language->get('text_add') : $this->language->get('text_edit');
		$data['text_no_results'] = $this->language->get('text_no_results');
		$data['text_default'] = $this->language->get('text_default');
		$data['text_select'] = $this->language->get('text_select');
		$data['text_none'] = $this->language->get('text_none');
		$data['text_loading'] = $this->language->get('text_loading');
		$data['text_ip_add'] = sprintf($this->language->get('text_ip_add'), $this->request->server['REMOTE_ADDR']);
		$data['text_product'] = $this->language->get('text_product');
		$data['text_voucher'] = $this->language->get('text_voucher');
		$data['text_rider_detail'] = $this->language->get('text_rider_detail');

		
		$data['entry_firstname'] = $this->language->get('entry_firstname');
		$data['entry_password'] = $this->language->get('entry_password');
        $data['entry_confirm_password'] = $this->language->get('entry_confirm_password');
		$data['entry_email'] = $this->language->get('entry_email');
		$data['entry_telephone'] = $this->language->get('entry_telephone');
        $data['entry_license'] = $this->language->get('entry_license');
        $data['entry_address'] = $this->language->get('entry_address');
        $data['entry_city'] = $this->language->get('entry_city');
        $data['entry_picture'] = $this->language->get('entry_picture');
        $data['entry_area'] = $this->language->get('entry_area');
        $data['entry_country'] = $this->language->get('entry_country');
		

		$data['column_product'] = $this->language->get('column_product');
		$data['column_model'] = $this->language->get('column_model');
		$data['column_quantity'] = $this->language->get('column_quantity');
		$data['column_price'] = $this->language->get('column_price');
		$data['column_total'] = $this->language->get('column_total');
		$data['column_action'] = $this->language->get('column_action');

		$data['button_save'] = $this->language->get('button_save');
		$data['button_cancel'] = $this->language->get('button_cancel');
		$data['button_continue'] = $this->language->get('button_continue');
		$data['button_back'] = $this->language->get('button_back');
		$data['button_refresh'] = $this->language->get('button_refresh');
		$data['button_product_add'] = $this->language->get('button_product_add');
		$data['button_voucher_add'] = $this->language->get('button_voucher_add');
		$data['button_apply'] = $this->language->get('button_apply');
		$data['button_upload'] = $this->language->get('button_upload');
		$data['button_remove'] = $this->language->get('button_remove');
		$data['button_ip_add'] = $this->language->get('button_ip_add');

		$data['tab_area'] = $this->language->get('tab_area');
		$data['tab_rider'] = $this->language->get('tab_rider');
		$data['tab_availability'] = $this->language->get('tab_availability');
		
		$data['token'] = $this->session->data['token'];

		
         if (isset($this->request->get['rider_id'])) {
			$rider_info = $this->model_rider_rider->getRiderInfo($this->request->get['rider_id']);
           
		}
        
        if (!empty($rider_info)) {
         
             //Rider Info
            $data['rider_id'] = $rider_info['rider_id'];
            $data['email'] = $rider_info['email'];
            $data['name'] =  $rider_info['name'];
            $data['password'] = $rider_info['password'];
            $data['contact_no'] = $rider_info['contact_no'];
            $data['license'] = $rider_info['license'];
            $data['address'] = $rider_info['address'];
           
            
            //Area
            
            $data['area_name'] = $rider_info['area_name'];
            
            //Availability
            $data['m'] = $rider_info['monday'];
            $data['mti'] = $this->getRequiredTime($rider_info['mon_time_in']);
            $data['mto'] = $this->getRequiredTime($rider_info['mon_time_out']);
            
            $data['t'] = $rider_info['tuesday'] ==1;
            $data['tti'] = $this->getRequiredTime($rider_info['tue_time_in']);
            $data['tto'] = $this->getRequiredTime($rider_info['tue_time_out']);
            
            $data['w'] = $rider_info['wednesday'];
            $data['wti'] = $this->getRequiredTime($rider_info['wed_time_in']);
            $data['wto'] = $this->getRequiredTime($rider_info['wed_time_out']);
            
            $data['thu'] = $rider_info['thursday'] ;
            $data['thti'] = $this->getRequiredTime($rider_info['thu_time_in']);
            $data['thto'] = $this->getRequiredTime($rider_info['thu_time_out']);
            
            $data['f'] = $rider_info['friday'];
            $data['fti'] = $this->getRequiredTime($rider_info['fri_time_in']);
            $data['fto'] = $this->getRequiredTime($rider_info['fri_time_out']);
            
            $data['sat'] = $rider_info['saturday'] ;
            $data['satti'] = $this->getRequiredTime($rider_info['sat_time_in']);
            $data['satto'] = $this->getRequiredTime($rider_info['sat_time_out']);
            
            $data['sun'] = $rider_info['sunday'];
            $data['sunti'] = $this->getRequiredTime($rider_info['sun_time_in']);
            $data['sunto'] = $this->getRequiredTime($rider_info['sun_time_out']);
            }
        else{
            //Rider Info
            $data['rider_id'] = 0;
            $data['email'] = '';
            $data['name'] = '';
            $data['password'] = '';
            $data['contact_no'] = '';
            $data['license'] = '';
            $data['address'] = '';
           
            
            
            //Area
            
            $data['area_name'] = '';
            
            //Availability
            $data['m'] = 0;
            $data['mti'] = '9:00 AM';
            $data['mto'] = '8:00 PM';
            
            $data['t'] = 0;
            $data['tti'] = '9:00 AM';
            $data['tto'] = '8:00 PM';
            
            $data['w'] = 0;
            $data['wti'] = '9:00 AM';
            $data['wto'] = '8:00 PM';
            
            $data['thu'] = 0;
            $data['thti'] = '9:00 AM';
            $data['thto'] = '8:00 PM';
            
            $data['f'] = 0;
            $data['fti'] = '9:00 AM';
            $data['fto'] = '8:00 PM';
            
            $data['sat'] = 0;
            $data['satti'] = '9:00 AM';
            $data['satto'] = '8:00 PM';
            
            $data['sun'] = 0;
            $data['sunti'] = '9:00 AM';
            $data['sunto'] = '8:00 PM';
        }


		$data['breadcrumbs'] = array();

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_home'),
			'href' => $this->url->link('common/dashboard', 'token=' . $this->session->data['token'], 'SSL')
		);

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('heading_title'),
			'href' => $this->url->link('rider/rider', 'token=' . $this->session->data['token'] . $url, 'SSL')
		);

		$data['cancel'] = $this->url->link('rider/rider', 'token=' . $this->session->data['token'] . $url, 'SSL');


		$data['header'] = $this->load->controller('common/header');
		$data['column_left'] = $this->load->controller('common/column_left');
		$data['footer'] = $this->load->controller('common/footer');

		$this->response->setOutput($this->load->view('rider/rider_form.tpl', $data));
	}
      public function getAreaForm() {
	

		$data['heading_title'] = $this->language->get('heading_title');

		$data['text_form'] = !isset($this->request->get['area_id']) ? $this->language->get('text_add') : $this->language->get('text_edit');
		$data['text_no_results'] = $this->language->get('text_no_results');
		$data['text_default'] = $this->language->get('text_default');
		$data['text_select'] = $this->language->get('text_select');
		$data['text_none'] = $this->language->get('text_none');
		$data['text_loading'] = $this->language->get('text_loading');
		$data['text_ip_add'] = sprintf($this->language->get('text_ip_add'), $this->request->server['REMOTE_ADDR']);
		$data['text_product'] = $this->language->get('text_product');
		$data['text_voucher'] = $this->language->get('text_voucher');
		$data['text_rider_detail'] = $this->language->get('text_rider_detail');

		
	
        $data['entry_city'] = $this->language->get('entry_city');       
        $data['entry_area'] = $this->language->get('entry_area');

		

	

		$data['button_save'] = $this->language->get('button_save');
		$data['button_cancel'] = $this->language->get('button_cancel');
		$data['button_continue'] = $this->language->get('button_continue');
		$data['button_back'] = $this->language->get('button_back');
		$data['button_refresh'] = $this->language->get('button_refresh');	
		$data['button_ip_add'] = $this->language->get('button_ip_add');	
		
		$data['token'] = $this->session->data['token'];

		

	

		$data['breadcrumbs'] = array();

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_home'),
			'href' => $this->url->link('common/dashboard', 'token=' . $this->session->data['token'], 'SSL')
		);

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('heading_title'),
			'href' => $this->url->link('rider/rider/area_list', 'token=' . $this->session->data['token'] . $url, 'SSL')
		);

		$data['cancel'] = $this->url->link('rider/rider', 'token=' . $this->session->data['token'] . $url, 'SSL');
        $data['action'] = $this->url->link('rider/rider/addArea', 'token=' . $this->session->data['token'] . $url, 'SSL');


		$data['header'] = $this->load->controller('common/header');
		$data['column_left'] = $this->load->controller('common/column_left');
		$data['footer'] = $this->load->controller('common/footer');

		$this->response->setOutput($this->load->view('rider/area_form.tpl', $data));
	}
    
    
    	protected function getList() {
    	  
		//die('here');

		
		$data['breadcrumbs'] = array();

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_home'),
			'href' => $this->url->link('common/dashboard', 'token=' . $this->session->data['token'], 'SSL')
		);

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('heading_title'),
			'href' => $this->url->link('rider/rider_list', 'token=' . $this->session->data['token'] . $url, 'SSL')
		);

		$data['add'] = $this->url->link('rider/rider/add', 'token=' . $this->session->data['token'] . $url, 'SSL');
		$data['delete'] = $this->url->link('rider/rider/delete', 'token=' . $this->session->data['token'] . $url, 'SSL');

		$data['riders'] = array();

	

		$rider_total = 0; 

	
        
        $ch = curl_init('https://careeb.com/index.php?route=rider/MainAPI/GetUsersByStatus&is_active=1');
        //$ch = curl_init('http://localhost/careeb_live/index.php?route=rider/MainAPI/GetUsersByStatus&is_active=1');
        
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
              
        //execute post
        $resultJson = curl_exec($ch);
        $results = json_decode($resultJson, true);
        
        $data['riders'] = $results;
        
	    $rider_total    =  count($results);

		$data['heading_title'] = $this->language->get('heading_title');
        
        $data['text_list'] = $this->language->get('text_list');
		$data['text_enabled'] = $this->language->get('text_enabled');
		$data['text_disabled'] = $this->language->get('text_disabled');
		$data['text_yes'] = $this->language->get('text_yes');
		$data['text_no'] = $this->language->get('text_no');
		$data['text_default'] = $this->language->get('text_default');
		$data['text_no_results'] = $this->language->get('text_no_results');
		$data['text_confirm'] = $this->language->get('text_confirm');

		$data['column_name'] = $this->language->get('column_name');
		$data['column_email'] = $this->language->get('column_email');
		$data['column_customer_group'] = $this->language->get('column_customer_group');
		$data['column_status'] = $this->language->get('column_status');
		$data['column_approved'] = $this->language->get('column_approved');
		$data['column_ip'] = $this->language->get('column_ip');
		$data['column_date_added'] = $this->language->get('column_date_added');
		$data['column_action'] = $this->language->get('column_action');

		$data['entry_name'] = $this->language->get('entry_name');
		$data['entry_email'] = $this->language->get('entry_email');
		$data['entry_customer_group'] = $this->language->get('entry_customer_group');
		$data['entry_status'] = $this->language->get('entry_status');
		$data['entry_approved'] = $this->language->get('entry_approved');
		$data['entry_ip'] = $this->language->get('entry_ip');
		$data['entry_date_added'] = $this->language->get('entry_date_added');

		$data['button_approve'] = $this->language->get('button_approve');
		$data['button_add'] = $this->language->get('button_add');
		$data['button_edit'] = $this->language->get('button_edit');
		$data['button_delete'] = $this->language->get('button_delete');
		$data['button_filter'] = $this->language->get('button_filter');
		$data['button_login'] = $this->language->get('button_login');
		$data['button_unlock'] = $this->language->get('button_unlock');

		$data['token'] = $this->session->data['token'];


		$pagination = new Pagination();
		$pagination->total = $rider_total;
		$pagination->page = $page;
		$pagination->limit = $this->config->get('config_limit_admin');
		$pagination->url = $this->url->link('rider/rider', 'token=' . $this->session->data['token'] . $url . '&page={page}', 'SSL');

		$data['pagination'] = $pagination->render();

		$data['results'] = sprintf($this->language->get('text_pagination'), ($rider_total) ? (($page - 1) * $this->config->get('config_limit_admin')) + 1 : 0, ((($page - 1) * $this->config->get('config_limit_admin')) > ($rider_total - $this->config->get('config_limit_admin'))) ? $rider_total : ((($page - 1) * $this->config->get('config_limit_admin')) + $this->config->get('config_limit_admin')), $rider_total, ceil($rider_total / $this->config->get('config_limit_admin')));



		$data['header'] = $this->load->controller('common/header');
		$data['column_left'] = $this->load->controller('common/column_left');
		$data['footer'] = $this->load->controller('common/footer');

		$this->response->setOutput($this->load->view('rider/rider_list.tpl', $data));
	}
    
    protected function getAreaList() {
    	  
		//die('here');

		
		$data['breadcrumbs'] = array();

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_home'),
			'href' => $this->url->link('common/dashboard', 'token=' . $this->session->data['token'], 'SSL')
		);

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('heading_area_title'),
			'href' => $this->url->link('rider/area_list', 'token=' . $this->session->data['token'] . $url, 'SSL')
		);

		$data['add'] = $this->url->link('rider/rider/addArea', 'token=' . $this->session->data['token'] . $url, 'SSL');
		$data['delete'] = $this->url->link('rider/rider/deleteArea', 'token=' . $this->session->data['token'] . $url, 'SSL');

		$data['areas'] = array();

	

		$areas_total = 0; 

	
        
        //$ch = curl_init('https://careeb.com/index.php?route=rider/MainAPI/GetUsersByStatus&is_active=1');
        $ch = curl_init('http://localhost/careeb_live/index.php?route=rider/MainAPI/getAreas');
        
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
              
        //execute post
        $resultJson = curl_exec($ch);
        $results = json_decode($resultJson, true);
        
        $data['areas'] = $results;
        
	    $areas_total    =  count($results);

		$data['heading_title'] = $this->language->get('heading_area_title');
        
        $data['text_list'] = $this->language->get('text_area_list');
		$data['text_enabled'] = $this->language->get('text_enabled');
		$data['text_disabled'] = $this->language->get('text_disabled');
		$data['text_yes'] = $this->language->get('text_yes');
		$data['text_no'] = $this->language->get('text_no');
		$data['text_default'] = $this->language->get('text_default');
		$data['text_no_results'] = $this->language->get('text_no_results');
		$data['text_confirm'] = $this->language->get('text_confirm');

		$data['column_name'] = $this->language->get('column_name');
		$data['column_email'] = $this->language->get('column_email');
		$data['column_customer_group'] = $this->language->get('column_customer_group');
		$data['column_status'] = $this->language->get('column_status');
		$data['column_approved'] = $this->language->get('column_approved');
		$data['column_ip'] = $this->language->get('column_ip');
		$data['column_date_added'] = $this->language->get('column_date_added');
		$data['column_action'] = $this->language->get('column_action');

		$data['entry_name'] = $this->language->get('entry_name');
		$data['entry_email'] = $this->language->get('entry_email');
		$data['entry_customer_group'] = $this->language->get('entry_customer_group');
		$data['entry_status'] = $this->language->get('entry_status');
		$data['entry_approved'] = $this->language->get('entry_approved');
		$data['entry_ip'] = $this->language->get('entry_ip');
		$data['entry_date_added'] = $this->language->get('entry_date_added');

		$data['button_approve'] = $this->language->get('button_approve');
		$data['button_add'] = $this->language->get('button_add');
		$data['button_edit'] = $this->language->get('button_edit');
		$data['button_delete'] = $this->language->get('button_delete');
		$data['button_filter'] = $this->language->get('button_filter');
		$data['button_login'] = $this->language->get('button_login');
		$data['button_unlock'] = $this->language->get('button_unlock');

		$data['token'] = $this->session->data['token'];


		$pagination = new Pagination();
		$pagination->total = $areas_total;
		$pagination->page = $page;
		$pagination->limit = $this->config->get('config_limit_admin');
		$pagination->url = $this->url->link('rider/rider/getAreas', 'token=' . $this->session->data['token'] . $url . '&page={page}', 'SSL');

		$data['pagination'] = $pagination->render();

		$data['results'] = sprintf($this->language->get('text_pagination'), ($rider_total) ? (($page - 1) * $this->config->get('config_limit_admin')) + 1 : 0, ((($page - 1) * $this->config->get('config_limit_admin')) > ($rider_total - $this->config->get('config_limit_admin'))) ? $rider_total : ((($page - 1) * $this->config->get('config_limit_admin')) + $this->config->get('config_limit_admin')), $rider_total, ceil($rider_total / $this->config->get('config_limit_admin')));



		$data['header'] = $this->load->controller('common/header');
		$data['column_left'] = $this->load->controller('common/column_left');
		$data['footer'] = $this->load->controller('common/footer');


		$this->response->setOutput($this->load->view('rider/area_list.tpl', $data));
	}
    
    public function getRequiredTime($datetime){
        
      return   date("h:i A",strtotime($datetime));
    }
    
}