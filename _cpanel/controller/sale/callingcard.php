<?php
class ControllerSaleCallingcard extends Controller {
	private $error = array();
	

	public function index() {
		
		
		$cardvalue= $this->request->post['calling_card'];
		$email_value= $this->request->post['calling_card_email'];
		$product_value= $this->request->post['mail_product_id'];
		
		
		if ($email_value && $cardvalue && $product_value) {
				
				
				$subject = "Recharge Card";
				$message  = '<html dir="ltr" lang="en">' . "\n";
					$message .= '  <head>' . "\n";
					$message .= '    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">' . "\n";
					$message .= '  </head>' . "\n";
					$message .= '  <body> ' . "\n";
					$message .= '  <p>Dear Valued Customer </p>' . "\n\n";
					$message .= '  <p>Thank you for using qareeb.com </p>' . "\n\n";
					$message .= '  <p> Your <a href="#">'.$product_value.'</a> Recharge Card Number is '.$cardvalue.' </p>' . "\n\n";
					$message .= '  <p>In case having issue or Question please communicate with us at support@qareeb.com </p>';				
				
				$message .= '</body></html>' . "\n";

				$mail = new Mail();
				$mail->protocol = $this->config->get('config_mail_protocol');
				$mail->parameter = $this->config->get('config_mail_parameter');
				$mail->smtp_hostname = $this->config->get('config_mail_smtp_hostname');
				$mail->smtp_username = $this->config->get('config_mail_smtp_username');
				$mail->smtp_password = html_entity_decode($this->config->get('config_mail_smtp_password'), ENT_QUOTES, 'UTF-8');
				$mail->smtp_port = $this->config->get('config_mail_smtp_port');
				$mail->smtp_timeout = $this->config->get('config_mail_smtp_timeout');

				$mail->setTo($email_value);
				$mail->setFrom($this->config->get('config_email'));
				$mail->setSender(html_entity_decode($this->config->get('config_name'), ENT_QUOTES, 'UTF-8'));
				$mail->setSubject(html_entity_decode($subject, ENT_QUOTES, 'UTF-8'));
				$mail->setHtml($message);
				$mail->send();
			}

		
  
        $filter_data = array();

		

			$filter_data = array(
				
				'success' =>"Calling Card Successfully Sent."
				
			);

			

		
		
		
		$this->response->addHeader('Content-Type: application/json');
		$this->response->setOutput(json_encode($filter_data));
	
	}
} 