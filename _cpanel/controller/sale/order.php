<?php
class ControllerSaleOrder extends Controller
{

	private $error = array();

	public function index()
	{
		
		$this->load->language('sale/order');

		$this->document->setTitle($this->language->get('heading_title'));
		
		$this->load->model('sale/order');

		$this->getList();
	}

	public function add()
	{

		$this->load->language('sale/order');

		$this->document->setTitle($this->language->get('heading_title'));

		$this->load->model('sale/order');

		$this->getForm();
	}

	public function edit()
	{
		$this->load->language('sale/order');

		$this->document->setTitle($this->language->get('heading_title'));

		$this->load->model('sale/order');

		$this->getForm();
	}

	protected function getList()
	{

		if (isset($this->request->get['filter_order_id'])) {
			$filter_order_id = $this->request->get['filter_order_id'];
		} else {
			$filter_order_id = null;
		}

		if (isset($this->request->get['filter_customer'])) {
			$filter_customer = $this->request->get['filter_customer'];
		} else {
			$filter_customer = null;
		}

		if (isset($this->request->get['filter_city'])) {
			$filter_city = $this->request->get['filter_city'];
		} else {
			$filter_city = null;
		}

		if (isset($this->request->get['filter_order_status'])) {
			$filter_order_status = $this->request->get['filter_order_status'];
		} else {
			$filter_order_status = null;
		}

		if (isset($this->request->get['filter_customer_type'])) {
			$filter_customer_type = $this->request->get['filter_customer_type'];
		} else {
			$filter_customer_type = null;
		}

		if (isset($this->request->get['filter_total'])) {
			$filter_total = $this->request->get['filter_total'];
		} else {
			$filter_total = null;
		}

		/* if (isset($this->request->get['filter_date_added'])) {
			$filter_date_added = $this->request->get['filter_date_added'];
		} else {
			$filter_date_added = null;
		} */

		if (isset($this->request->get['filter_date_modified'])) {
			$filter_date_modified = $this->request->get['filter_date_modified'];
		} else {
			$filter_date_modified = null;
		}

		//add filter
		if (isset($this->request->get['filter_rider'])) {
			$filter_rider = $this->request->get['filter_rider'];
		} else {
			$filter_rider = null;
		}
		if (isset($this->request->get['filter_month'])) {
			$filter_month = $this->request->get['filter_month'];
		} else {
			$filter_month = '';
		}
		if (isset($this->request->get['filter_year'])) {
			$filter_year = $this->request->get['filter_year'];
		} else {
			$filter_year = null;
		}
		if (isset($this->request->get['filter_date_start'])) {
			$filter_date_start = $this->request->get['filter_date_start'];
		} else {
			//$filter_date_start = date('Y-m-d', strtotime(date('Y') . '-' . date('m') . '-01'));
			$filter_date_start = '';
		}
		if (isset($this->request->get['filter_date_end'])) {
			$filter_date_end = $this->request->get['filter_date_end'];
		} else {
			$filter_date_end = '';
		}

		if (isset($this->request->get['filter_group'])) {
			$filter_group = $this->request->get['filter_group'];
			$data['filter_group'] = $this->request->get['filter_group'];
		} else {
			$filter_group = '';
		}
		//end filter

		if (isset($this->request->get['filter_seller_name'])) {
			$filter_seller_name = $this->request->get['filter_seller_name'];
		} else {
			$filter_seller_name = null;
		}

		if (isset($this->request->get['sort'])) {
			$sort = $this->request->get['sort'];
		} else {
			$sort = 'o.order_id';
		}

		if (isset($this->request->get['order'])) {
			$order = $this->request->get['order'];
		} else {
			$order = 'DESC';
		}

		if (isset($this->request->get['page'])) {
			$page = $this->request->get['page'];
		} else {
			$page = 1;
		}

		$this->load->model('catalog/product');

		$sellers = $this->model_catalog_product->getSellers();

		$data['sellers'] = $sellers;

		$url = '';

		if (isset($this->request->get['filter_order_id'])) {
			$url .= '&filter_order_id=' . $this->request->get['filter_order_id'];
		}

		if (isset($this->request->get['filter_customer'])) {
			$url .= '&filter_customer=' . urlencode(html_entity_decode($this->request->get['filter_customer'], ENT_QUOTES, 'UTF-8'));
		}

		if (isset($this->request->get['filter_city'])) {
			$url .= '&filter_city=' . urlencode(html_entity_decode($this->request->get['filter_city'], ENT_QUOTES, 'UTF-8'));
		}

		if (isset($this->request->get['filter_order_status'])) {
			$url .= '&filter_order_status=' . $this->request->get['filter_order_status'];
		}

		if (isset($this->request->get['filter_customer_type'])) {
			$url .= '&filter_customer_type=' . $this->request->get['filter_customer_type'];
		}

		if (isset($this->request->get['filter_total'])) {
			$url .= '&filter_total=' . $this->request->get['filter_total'];
		}

		if (isset($this->request->get['filter_date_added'])) {
			$url .= '&filter_date_added=' . $this->request->get['filter_date_added'];
		}

		if (isset($this->request->get['filter_date_modified'])) {
			$url .= '&filter_date_modified=' . $this->request->get['filter_date_modified'];
		}

		if (isset($this->request->get['filter_seller_name'])) {
			$url .= '&filter_seller_name=' . $this->request->get['filter_seller_name'];
		}

		if (isset($this->request->get['sort'])) {
			$url .= '&sort=' . $this->request->get['sort'];
		}

		if (isset($this->request->get['order'])) {
			$url .= '&order=' . $this->request->get['order'];
		}

		if (isset($this->request->get['page'])) {
			$url .= '&page=' . $this->request->get['page'];
		}
		if (isset($this->request->get['filter_rider'])) {
			$url .= '&filter_rider=' . urlencode(html_entity_decode($this->request->get['filter_rider'], ENT_QUOTES, 'UTF-8'));
		}
		if (isset($this->request->get['filter_month'])) {
			$url .= '&filter_month=' . $this->request->get['filter_month'];
		}
		if (isset($this->request->get['filter_year'])) {
			$url .= '&filter_year=' . $this->request->get['filter_year'];
		}
		if (isset($this->request->get['filter_date_start'])) {
			$url .= '&filter_date_start=' . $this->request->get['filter_date_start'];
		}
		if (isset($this->request->get['filter_date_end'])) {
			$url .= '&filter_date_end=' . $this->request->get['filter_date_end'];
		}
		if (isset($this->request->get['order'])) {
			$url .= '&order=' . $this->request->get['order'];
		}

		if (isset($this->request->get['filter_group'])) {
			$url .= '&filter_group=' . $this->request->get['filter_group'];
		}

		$data['groups'] = array();

		$data['groups'][] = array(
			'text'  => 'Years',
			'value' => 'year',
		);

		$data['groups'][] = array(
			'text'  => 'Months',
			'value' => 'month',
		);

		$data['groups'][] = array(
			'text'  => 'Weeks',
			'value' => 'week',
		);

		$data['groups'][] = array(
			'text'  => 'Days',
			'value' => 'day',
		);

		$data['breadcrumbs'] = array();

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_home'),
			'href' => $this->url->link('common/dashboard', 'token=' . $this->session->data['token'], 'SSL')
		);

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('heading_title'),
			'href' => $this->url->link('sale/order', 'token=' . $this->session->data['token'] . $url, 'SSL')
		);

		$data['invoice'] = $this->url->link('sale/order/invoice', 'token=' . $this->session->data['token'], 'SSL');
		$data['shipping'] = $this->url->link('sale/order/shipping', 'token=' . $this->session->data['token'], 'SSL');
		$data['add'] = $this->url->link('sale/order/add', 'token=' . $this->session->data['token'], 'SSL');

		$data['orders'] = array();

		$filter_data = array(
			'filter_order_id'      => $filter_order_id,
			'filter_customer'	   => $filter_customer,
			'filter_city'	       => $filter_city,
			'filter_order_status'  => $filter_order_status,
			'filter_customer_type' => $filter_customer_type,
			'filter_total'         => $filter_total,
			'filter_date_added'    => $filter_date_added,
			'filter_date_modified' => $filter_date_modified,
			'filter_rider'	       => $filter_rider,
			'filter_date_start'	   => $filter_date_start,
			'filter_date_end'	   => $filter_date_end,
			'filter_month'	       => $filter_month,
			'filter_year'	       => $filter_year,
			'filter_group'         => $filter_group,
			'filter_seller_name'   => $filter_seller_name,
			'sort'                 => $sort,
			'order'                => $order,
			'start'                => ($page - 1) * $this->config->get('config_limit_admin'),
			'limit'                => $this->config->get('config_limit_admin')
		);
		/* echo"<pre>";
		print_r($filter_data);
		exit; */
		$order_total = $this->model_sale_order->getTotalOrders($filter_data);

		$results = $this->model_sale_order->getOrders($filter_data);

		foreach ($results as $result) {

			//product  subscript
			$not_found_product_query = $this->db->query("SELECT sum(total) as total FROM `oc_order_product` WHERE `order_id` = " . $result['order_id'] . " AND `product_status` = 2")->row;


			$order_info = $this->model_sale_order->getOrder($result['order_id']);

			$Total1 = $this->model_sale_order->getOrderTotals1($result['order_id']);
			$Total2 = $this->model_sale_order->getOrderTotals2($result['order_id']);


			$sellername_firstname = $this->model_sale_order->getsellername($result['order_id']);
			/* echo"<pre>";
			print_r($sellername[0]['firstname']);
			exit; */
			$sellername = $sellername_firstname[0]['firstname'];
			$date_added = date("Y-m-d", strtotime($result['date_added']));
			$date = '2018-11-02';

			$beforDate = 0;
			$afterDate = 0;
			if ($result['date_added'] != '0000-00-00 00:00:00') {
				if ($date_added <= $date) {
					$Final_Total = ($Total1 + $Total2);
				} else {
					$Final_Total = $Total1;
				}
			} else {
				$Final_Total = ($Total1 + $Total2);
			}

			$Final_Total -= $not_found_product_query['total'];
			$data['orders'][] = array(
				'rider_name'     => $this->getRiderNameByOrderId($result['order_id']),
				'payment_method' => $order_info['payment_method'],
				'shipping_city'  => $order_info['shipping_city'],
				'order_id'       => $result['order_id'],
				'customer'       => $result['customer'],

				'seller'       => $this->model_sale_order->getOrderSellersnew($result['order_id'], $filter_seller),
				'sellerstatus' => $this->model_sale_order->getOrderSellerStatusnew($result['order_id'], $filter_seller_status_id),

				'status'        => $result['status'],
				'sellername'    => $sellername,
				'total'         => $this->currency->format($Final_Total, $result['currency_code'], $result['currency_value']),
				'date_added'    => date($this->language->get('datetime_format'), strtotime($result['date_added'])),
				'date_modified' => date($this->language->get('datetime_format'), strtotime($result['date_modified'])),
				'shipping_code' => $result['shipping_code'],
				'sequence'      => $result['sequence'],
				'view'          => $this->url->link('sale/order/info', 'token=' . $this->session->data['token'] . '&order_id=' . $result['order_id'] . $url, 'SSL'),
				'edit'          => $this->url->link('sale/order/edit', 'token=' . $this->session->data['token'] . '&order_id=' . $result['order_id'] . $url, 'SSL'),
			);
		}

		$data['heading_title'] = $this->language->get('heading_title');

		$data['text_list']       = $this->language->get('text_list');
		$data['text_no_results'] = $this->language->get('text_no_results');
		$data['text_confirm']    = $this->language->get('text_confirm');
		$data['text_missing']    = $this->language->get('text_missing');
		$data['text_loading']    = $this->language->get('text_loading');

		$data['column_order_id']      = $this->language->get('column_order_id');
		$data['column_customer']      = $this->language->get('column_customer');
		$data['column_city']          = $this->language->get('column_city');
		$data['column_status']        = $this->language->get('column_status');
		$data['column_total']         = $this->language->get('column_total');
		$data['column_date_added']    = $this->language->get('column_date_added');
		$data['column_date_modified'] = $this->language->get('column_date_modified');
		$data['column_action']        = $this->language->get('column_action');

		$data['entry_return_id']     = $this->language->get('entry_return_id');
		$data['entry_order_id']      = $this->language->get('entry_order_id');
		$data['entry_customer']      = $this->language->get('entry_customer');
		$data['entry_city']          = $this->language->get('entry_city');
		$data['entry_order_status']  = $this->language->get('entry_order_status');
		$data['entry_total']         = $this->language->get('entry_total');
		$data['entry_date_added']    = $this->language->get('entry_date_added');
		$data['entry_date_modified'] = $this->language->get('entry_date_modified');

		$data['button_invoice_print']  = $this->language->get('button_invoice_print');
		$data['button_shipping_print'] = $this->language->get('button_shipping_print');
		$data['button_add']            = $this->language->get('button_add');
		$data['button_edit']           = $this->language->get('button_edit');
		$data['button_delete']         = $this->language->get('button_delete');
		$data['button_filter']         = $this->language->get('button_filter');
		$data['button_view']           = $this->language->get('button_view');
		$data['button_ip_add']         = $this->language->get('button_ip_add');
		$data['entry_rider']           = 'Rider Name';
		$data['entry_date_start']      = 'Start Date';
		$data['entry_date_end']        = 'End Date';
		$data['entry_group']           = 'Entry Group';
		$data['entry_month']           = 'Entry Month';
		$data['entry_year']            = 'Entry Year';

		$data['token'] = $this->session->data['token'];

		if (isset($this->request->post['selected'])) {
			$data['selected'] = (array) $this->request->post['selected'];
		} else {
			$data['selected'] = array();
		}

		$url = '';

		if (isset($this->request->get['filter_order_id'])) {
			$url .= '&filter_order_id=' . $this->request->get['filter_order_id'];
		}

		if (isset($this->request->get['filter_customer'])) {
			$url .= '&filter_customer=' . urlencode(html_entity_decode($this->request->get['filter_customer'], ENT_QUOTES, 'UTF-8'));
		}

		if (isset($this->request->get['filter_city'])) {
			$url .= '&filter_customer=' . urlencode(html_entity_decode($this->request->get['filter_city'], ENT_QUOTES, 'UTF-8'));
		}

		if (isset($this->request->get['filter_order_status'])) {
			$url .= '&filter_order_status=' . $this->request->get['filter_order_status'];
		}

		if (isset($this->request->get['filter_customer_type'])) {
			$url .= '&filter_customer_type=' . $this->request->get['filter_customer_type'];
		}

		if (isset($this->request->get['filter_total'])) {
			$url .= '&filter_total=' . $this->request->get['filter_total'];
		}

		if (isset($this->request->get['filter_date_added'])) {
			$url .= '&filter_date_added=' . $this->request->get['filter_date_added'];
		}

		if (isset($this->request->get['filter_date_modified'])) {
			$url .= '&filter_date_modified=' . $this->request->get['filter_date_modified'];
		}

		if (isset($this->request->get['filter_seller_name'])) {
			$url .= '&filter_seller_name=' . $this->request->get['filter_seller_name'];
		}

		if (isset($this->request->get['filter_rider'])) {
			$url .= '&filter_rider=' . urlencode(html_entity_decode($this->request->get['filter_rider'], ENT_QUOTES, 'UTF-8'));
		}
		if (isset($this->request->get['filter_month'])) {
			$url .= '&filter_month=' . $this->request->get['filter_month'];
		}
		if (isset($this->request->get['filter_year'])) {
			$url .= '&filter_year=' . $this->request->get['filter_year'];
		}
		if (isset($this->request->get['filter_date_start'])) {
			$url .= '&filter_date_start=' . $this->request->get['filter_date_start'];
		}
		if (isset($this->request->get['filter_date_end'])) {
			$url .= '&filter_date_end=' . $this->request->get['filter_date_end'];
		}
		if (isset($this->request->get['filter_group'])) {
			$url .= '&filter_group=' . $this->request->get['filter_group'];
		}

		if ($order == 'ASC') {
			$url .= '&order=DESC';
		} else {
			$url .= '&order=ASC';
		}

		if (isset($this->request->get['page'])) {
			$url .= '&page=' . $this->request->get['page'];
		}

		$data['sort_order']         = $this->url->link('sale/order', 'token=' . $this->session->data['token'] . '&sort=o.order_id' . $url, 'SSL');
		$data['sort_customer']      = $this->url->link('sale/order', 'token=' . $this->session->data['token'] . '&sort=customer' . $url, 'SSL');
		$data['sort_city']          = $this->url->link('sale/order', 'token=' . $this->session->data['token'] . '&sort=city' . $url, 'SSL');
		$data['sort_status']        = $this->url->link('sale/order', 'token=' . $this->session->data['token'] . '&sort=status' . $url, 'SSL');
		$data['sort_total']         = $this->url->link('sale/order', 'token=' . $this->session->data['token'] . '&sort=o.total' . $url, 'SSL');
		$data['sort_date_added']    = $this->url->link('sale/order', 'token=' . $this->session->data['token'] . '&sort=o.date_added' . $url, 'SSL');
		$data['sort_date_modified'] = $this->url->link('sale/order', 'token=' . $this->session->data['token'] . '&sort=o.date_modified' . $url, 'SSL');

		$url = '';

		if (isset($this->request->get['filter_order_id'])) {
			$url .= '&filter_order_id=' . $this->request->get['filter_order_id'];
		}

		if (isset($this->request->get['filter_customer'])) {
			$url .= '&filter_customer=' . urlencode(html_entity_decode($this->request->get['filter_customer'], ENT_QUOTES, 'UTF-8'));
		}

		if (isset($this->request->get['filter_city'])) {
			$url .= '&filter_city=' . urlencode(html_entity_decode($this->request->get['filter_city'], ENT_QUOTES, 'UTF-8'));
		}

		if (isset($this->request->get['filter_order_status'])) {
			$url .= '&filter_order_status=' . $this->request->get['filter_order_status'];
		}

		if (isset($this->request->get['filter_customer_type'])) {
			$url .= '&filter_customer_type=' . $this->request->get['filter_customer_type'];
		}

		if (isset($this->request->get['filter_total'])) {
			$url .= '&filter_total=' . $this->request->get['filter_total'];
		}

		if (isset($this->request->get['filter_date_added'])) {
			$url .= '&filter_date_added=' . $this->request->get['filter_date_added'];
		}

		if (isset($this->request->get['filter_date_modified'])) {
			$url .= '&filter_date_modified=' . $this->request->get['filter_date_modified'];
		}

		if (isset($this->request->get['filter_seller_name'])) {
			$url .= '&filter_seller_name=' . $this->request->get['filter_seller_name'];
		}

		if (isset($this->request->get['filter_rider'])) {
			$url .= '&filter_rider=' . urlencode(html_entity_decode($this->request->get['filter_rider'], ENT_QUOTES, 'UTF-8'));
		}
		if (isset($this->request->get['filter_month'])) {
			$url .= '&filter_month=' . $this->request->get['filter_month'];
		}
		if (isset($this->request->get['filter_year'])) {
			$url .= '&filter_year=' . $this->request->get['filter_year'];
		}
		if (isset($this->request->get['filter_date_start'])) {
			$url .= '&filter_date_start=' . $this->request->get['filter_date_start'];
		}
		if (isset($this->request->get['filter_date_end'])) {
			$url .= '&filter_date_end=' . $this->request->get['filter_date_end'];
		}
		if (isset($this->request->get['filter_group'])) {
			$url .= '&filter_group=' . $this->request->get['filter_group'];
		}

		if (isset($this->request->get['sort'])) {
			$url .= '&sort=' . $this->request->get['sort'];
		}

		if (isset($this->request->get['order'])) {
			$url .= '&order=' . $this->request->get['order'];
		}

		$pagination        = new Pagination();
		$pagination->total = $order_total;
		$pagination->page  = $page;
		$pagination->limit = $this->config->get('config_limit_admin');
		$pagination->url   = $this->url->link('sale/order', 'token=' . $this->session->data['token'] . $url . '&page={page}', 'SSL');

		$data['pagination'] = $pagination->render();

		$data['results'] = sprintf($this->language->get('text_pagination'), ($order_total) ? (($page - 1) * $this->config->get('config_limit_admin')) + 1 : 0, ((($page - 1) * $this->config->get('config_limit_admin')) > ($order_total - $this->config->get('config_limit_admin'))) ? $order_total : ((($page - 1) * $this->config->get('config_limit_admin')) + $this->config->get('config_limit_admin')), $order_total, ceil($order_total / $this->config->get('config_limit_admin')));

		$data['filter_order_id']      = $filter_order_id;
		$data['filter_customer']      = $filter_customer;
		$data['filter_city']          = $filter_city;
		$data['filter_order_status']  = $filter_order_status;
		$data['filter_customer_type'] = $filter_customer_type;
		$data['filter_total']         = $filter_total;
		$data['filter_date_added']    = $filter_date_added;
		$data['filter_date_modified'] = $filter_date_modified;
		$data['filter_seller_name']   = $filter_seller_name;
		$data['filter_rider']         = $filter_rider;
		$data['filter_date_start']    = $filter_date_start;
		$data['filter_date_end']      = $filter_date_end;
		$data['filter_month']         = $filter_month;
		$data['filter_year']          = $filter_year;

		$this->load->model('localisation/order_status');

		$data['order_statuses'] = $this->model_localisation_order_status->getOrderStatuses();

		$data['sort'] = $sort;
		$data['order'] = $order;

		$data['store'] = HTTPS_CATALOG;

		// API login
		$this->load->model('user/api');

		$api_info = $this->model_user_api->getApi($this->config->get('config_api_id'));

		if ($api_info) {
			$data['api_id'] = $api_info['api_id'];
			$data['api_key'] = $api_info['key'];
			$data['api_ip'] = $this->request->server['REMOTE_ADDR'];
		} else {
			$data['api_id'] = '';
			$data['api_key'] = '';
			$data['api_ip'] = '';
		}
		// month 
		$months = array();
		/* 	$month = array();
		for ($i = 0; $i < 12; $i++) {
			$timestamp = mktime(0, 0, 0, date('n') - $i, 1);
			$months[date('n', $timestamp)] = date('F', $timestamp);
		}
		foreach($months as $k => $va) {
			$month[$k] =$va;
			if($k == 1){
				break;
			}
		} */
		for ($i = 0; $i < 24; $i++) {
			$months = date("F - Y", strtotime(date('Y-m-01') . " -$i months"));
			$months_value = date("Y-m", strtotime(date('Y-m-01') . " -$i months"));
			$month[$months_value] = $months;
		}
		$data['button_export'] = $this->language->get('button_export');
		$data['export']        = $this->url->link('sale/order/export', 'token=' . $this->session->data['token'] . $url, 'SSL');
		$data['month']         = $month;
		      $years           = array_combine(range(date("Y"), 2018), range(date("Y"), 2018));
		$data['years']         = $years;

		//$data['riderOrders'] = $this->getOrderList();

		$data['header']               = $this->load->controller('common/header');
		$data['column_left']          = $this->load->controller('common/column_left');
		$data['footer']               = $this->load->controller('common/footer');
		$data['column_seller']        = 'City';
		$data['column_seller_status'] = 'Status';

		/* echo"<pre>";
		print_r($data);
		exit; */
		$this->response->setOutput($this->load->view('sale/order_list.tpl', $data));
	}

	public function export()
	{

		if (isset($this->request->get['filter_order_id'])) {
			$filter_order_id = $this->request->get['filter_order_id'];
		} else {
			$filter_order_id = null;
		}

		if (isset($this->request->get['filter_customer'])) {
			$filter_customer = $this->request->get['filter_customer'];
		} else {
			$filter_customer = null;
		}

		if (isset($this->request->get['filter_rider'])) {
			$filter_rider = $this->request->get['filter_rider'];
		} else {
			$filter_rider = null;
		}

		if (isset($this->request->get['filter_city'])) {
			$filter_city = $this->request->get['filter_city'];
		} else {
			$filter_city = null;
		}

		if (isset($this->request->get['filter_order_status'])) {
			$filter_order_status = $this->request->get['filter_order_status'];
		} else {
			$filter_order_status = null;
		}

		if (isset($this->request->get['filter_customer_type'])) {
			$filter_customer_type = $this->request->get['filter_customer_type'];
		} else {
			$filter_customer_type = null;
		}

		if (isset($this->request->get['filter_total'])) {
			$filter_total = $this->request->get['filter_total'];
		} else {
			$filter_total = null;
		}

		if (isset($this->request->get['filter_date_added'])) {
			$filter_date_added = $this->request->get['filter_date_added'];
		} else {
			$filter_date_added = null;
		}

		if (isset($this->request->get['filter_date_modified'])) {
			$filter_date_modified = $this->request->get['filter_date_modified'];
		} else {
			$filter_date_modified = null;
		}
		if (isset($this->request->get['filter_month'])) {
			$filter_month = $this->request->get['filter_month'];
		} else {
			$filter_month = '';
		}
		if (isset($this->request->get['filter_year'])) {
			$filter_year = $this->request->get['filter_year'];
		} else {
			$filter_year = null;
		}
		if (isset($this->request->get['filter_date_start'])) {
			$filter_date_start = $this->request->get['filter_date_start'];
		} else {
			//$filter_date_start = date('Y-m-d', strtotime(date('Y') . '-' . date('m') . '-01'));
			$filter_date_start = '';
		}
		if (isset($this->request->get['filter_date_end'])) {
			$filter_date_end = $this->request->get['filter_date_end'];
		} else {
			$filter_date_end = '';
		}

		if (isset($this->request->get['filter_group'])) {
			$filter_group = $this->request->get['filter_group'];
			$data['filter_group'] = $this->request->get['filter_group'];
		} else {
			$filter_group = 'week';
		}

		if (isset($this->request->get['filter_date_added'])) {
			$filter_group = $this->request->get['filter_date_added'];
			$data['filter_date_added'] = $this->request->get['filter_date_added'];
		} else {
			$filter_date_added = '';
		}

		if (isset($this->request->get['filter_seller_name'])) {
			$filter_seller_name = $this->request->get['filter_seller_name'];
		} else {
			$filter_seller_name = null;
		}

		if (isset($this->request->get['sort'])) {
			$sort = $this->request->get['sort'];
		} else {
			$sort = 'o.order_id';
		}

		if (isset($this->request->get['order'])) {
			$order = $this->request->get['order'];
		} else {
			$order = 'DESC';
		}

		$filter_data = array(
			'filter_order_id'      => $filter_order_id,
			'filter_customer'	   => $filter_customer,
			'filter_rider'	       => $filter_rider,
			'filter_city'	       => $filter_city,
			'filter_order_status'  => $filter_order_status,
			'filter_customer_type'  => $filter_customer_type,
			'filter_total'         => $filter_total,
			'filter_date_added'    => $filter_date_added,
			'filter_date_modified' => $filter_date_modified,
			'filter_date_start'	   => $filter_date_start,
			'filter_date_end'	   => $filter_date_end,
			'filter_month'	       => $filter_month,
			'filter_year'	       => $filter_year,
			'filter_group'         => $filter_group,
			'filter_seller_name'   => $filter_seller_name,
			'sort'                 => $sort,
			'order'                => $order
		);

		$url = '';
		if (isset($this->request->get['filter_order_id'])) {
			$url .= '&filter_order_id=' . $this->request->get['filter_order_id'];
		}

		if (isset($this->request->get['filter_customer'])) {
			$url .= '&filter_customer=' . urlencode(html_entity_decode($this->request->get['filter_customer'], ENT_QUOTES, 'UTF-8'));
		}

		if (isset($this->request->get['filter_rider'])) {
			$url .= '&filter_rider=' . urlencode(html_entity_decode($this->request->get['filter_rider'], ENT_QUOTES, 'UTF-8'));
		}

		if (isset($this->request->get['filter_city'])) {
			$url .= '&filter_city=' . urlencode(html_entity_decode($this->request->get['filter_city'], ENT_QUOTES, 'UTF-8'));
		}

		if (isset($this->request->get['filter_order_status'])) {
			$url .= '&filter_order_status=' . $this->request->get['filter_order_status'];
		}

		if (isset($this->request->get['filter_customer_type'])) {
			$url .= '&filter_customer_type=' . $this->request->get['filter_customer_type'];
		}

		if (isset($this->request->get['filter_total'])) {
			$url .= '&filter_total=' . $this->request->get['filter_total'];
		}

		if (isset($this->request->get['filter_date_added'])) {
			$url .= '&filter_date_added=' . $this->request->get['filter_date_added'];
		}

		if (isset($this->request->get['filter_date_start'])) {
			$url .= '&filter_date_start=' . $this->request->get['filter_date_start'];
		}

		if (isset($this->request->get['filter_date_end'])) {
			$url .= '&filter_date_end=' . $this->request->get['filter_date_end'];
		}

		if (isset($this->request->get['filter_group'])) {
			$url .= '&filter_group=' . $this->request->get['filter_group'];
		}

		if (isset($this->request->get['filter_date_modified'])) {
			$url .= '&filter_date_modified=' . $this->request->get['filter_date_modified'];
		}

		if (isset($this->request->get['filter_seller_name'])) {
			$url .= '&filter_seller_name=' . $this->request->get['filter_seller_name'];
		}

		if (isset($this->request->get['sort'])) {
			$url .= '&sort=' . $this->request->get['sort'];
		}

		if (isset($this->request->get['order'])) {
			$url .= '&order=' . $this->request->get['order'];
		}

		if (isset($this->request->get['filter_month'])) {
			$url .= '&filter_month=' . $this->request->get['filter_month'];
		}
		if (isset($this->request->get['filter_year'])) {
			$url .= '&filter_year=' . $this->request->get['filter_year'];
		}
		if (isset($this->request->get['filter_date_start'])) {
			$url .= '&filter_date_start=' . $this->request->get['filter_date_start'];
		}
		if (isset($this->request->get['filter_date_end'])) {
			$url .= '&filter_date_end=' . $this->request->get['filter_date_end'];
		}
		if (isset($this->request->get['order'])) {
			$url .= '&order=' . $this->request->get['order'];
		}

		if (isset($this->request->get['filter_group'])) {
			$url .= '&filter_group=' . $this->request->get['filter_group'];
		}

		try {

			$this->load->model('sale/order');

			$products_list = array();

			$data = array();

			$results = $this->model_sale_order->getorderdataexport($filter_data);

			foreach ($results as $result) {
				$order_info = $this->model_sale_order->getOrder($result['order_id']);

				$Total1 = $this->model_sale_order->getOrderTotals1($result['order_id']);
				$Total2 = $this->model_sale_order->getOrderTotals2($result['order_id']);

				$sellername_firstname = $this->model_sale_order->getsellername($result['order_id']);
				$sellername = $sellername_firstname[0]['firstname'];
				$date_added = date("Y-m-d", strtotime($result['date_added']));
				$date = '2018-11-02';

				$beforDate = 0;
				$afterDate = 0;
				if ($result['date_added'] != '0000-00-00 00:00:00') {
					if ($date_added <= $date) {
						$Final_Total = ($Total1 + $Total2);
					} else {
						$Final_Total = $Total1;
					}
				} else {
					$Final_Total = ($Total1 + $Total2);
				}

				//$a = $this->model_sale_order->getOrderSellerStatusnew(6539,$filter_seller_status_id);

				$data['orders'][] = array(
					'order_id'      => $result['order_id'],
					'customer'      => $result['customer'],
					'seller'      => $this->model_sale_order->getOrderSellersnew($result['order_id'], $filter_seller),
					//'sellerstatus' =>$this->model_sale_order->getOrderSellerStatusnew($result['order_id'],$filter_seller_status_id),
					'total'         => $this->currency->format($Final_Total, $result['currency_code'], $result['currency_value']),
					'date_added'    => date($this->language->get('datetime_format'), strtotime($result['date_added'])),
					'rider_name'    => str_replace('"', '', $this->getRiderNameByOrderId($result['order_id'])),
					'payment_method' => $order_info['payment_method'],
					'shipping_city' => $order_info['shipping_city'],
					'status'        => $result['status']
					//'sellername'    => $sellername,


					//'date_modified' => date($this->language->get('datetime_format'), strtotime($result['date_modified'])),
					//'shipping_code' => $result['shipping_code'],
					//'sequence' => $result['sequence']
				);
			}
			$products = array();

			$products_column = array();

			$products_column = array('Order Id', 'Customer Name', 'seller', 'Total', 'Date Added', 'Rider Name', 'Payment Method', 'City', 'Status');

			$products[0] =   $products_column;

			foreach ($data['orders'] as $products_row) {
				$products[] =   $products_row;
			}
			require_once(DIR_SYSTEM . 'library/excel_xml.php');
			$xls = new Excel_XML('UTF-8', false, 'Products Purchased Report');

			$xls->addArray($products);

			$xls->generateXML('order_report_' . date('Y-m-d _ H:i:s'));

		} catch(Exception $e) {
			echo 'Message: ' .$e->getMessage();
	 }
	}

	public function getForm()
	{

		$this->load->model('customer/customer');

		$data['heading_title'] = $this->language->get('heading_title');

		$data['text_form'] = !isset($this->request->get['order_id']) ? $this->language->get('text_add') : $this->language->get('text_edit');
		$data['text_no_results'] = $this->language->get('text_no_results');
		$data['text_default'] = $this->language->get('text_default');
		$data['text_select'] = $this->language->get('text_select');
		$data['text_none'] = $this->language->get('text_none');
		$data['text_loading'] = $this->language->get('text_loading');
		$data['text_ip_add'] = sprintf($this->language->get('text_ip_add'), $this->request->server['REMOTE_ADDR']);
		$data['text_product'] = $this->language->get('text_product');
		$data['text_voucher'] = $this->language->get('text_voucher');
		$data['text_order_detail'] = $this->language->get('text_order_detail');

		$data['entry_store'] = $this->language->get('entry_store');
		$data['entry_customer'] = $this->language->get('entry_customer');
		$data['entry_customer_group'] = $this->language->get('entry_customer_group');
		$data['entry_firstname'] = $this->language->get('entry_firstname');
		$data['entry_lastname'] = $this->language->get('entry_lastname');
		$data['entry_email'] = $this->language->get('entry_email');
		$data['entry_telephone'] = $this->language->get('entry_telephone');
		$data['entry_fax'] = $this->language->get('entry_fax');
		$data['entry_comment'] = $this->language->get('entry_comment');
		$data['entry_affiliate'] = $this->language->get('entry_affiliate');
		$data['entry_address'] = $this->language->get('entry_address');
		$data['entry_company'] = $this->language->get('entry_company');
		$data['entry_address_1'] = $this->language->get('entry_address_1');
		$data['entry_address_2'] = $this->language->get('entry_address_2');
		$data['entry_city'] = $this->language->get('entry_city');
		$data['entry_postcode'] = $this->language->get('entry_postcode');
		$data['entry_zone'] = $this->language->get('entry_zone');
		$data['entry_zone_code'] = $this->language->get('entry_zone_code');
		$data['entry_country'] = $this->language->get('entry_country');
		$data['entry_product'] = $this->language->get('entry_product');
		$data['choose_city'] = $this->language->get('choose_city');
		$data['choose_area'] = $this->language->get('choose_area');
		$data['choose_store'] = $this->language->get('choose_store');


		$data['entry_option'] = $this->language->get('entry_option');
		$data['entry_quantity'] = $this->language->get('entry_quantity');
		$data['entry_to_name'] = $this->language->get('entry_to_name');
		$data['entry_to_email'] = $this->language->get('entry_to_email');
		$data['entry_from_name'] = $this->language->get('entry_from_name');
		$data['entry_from_email'] = $this->language->get('entry_from_email');
		$data['entry_theme'] = $this->language->get('entry_theme');
		$data['entry_message'] = $this->language->get('entry_message');
		$data['entry_amount'] = $this->language->get('entry_amount');
		$data['entry_currency'] = $this->language->get('entry_currency');
		$data['entry_shipping_method'] = $this->language->get('entry_shipping_method');
		$data['entry_payment_method'] = $this->language->get('entry_payment_method');
		$data['entry_coupon'] = $this->language->get('entry_coupon');
		$data['entry_voucher'] = $this->language->get('entry_voucher');
		$data['entry_reward'] = $this->language->get('entry_reward');
		$data['entry_order_status'] = $this->language->get('entry_order_status');

		$data['column_product'] = $this->language->get('column_product');
		$data['column_model'] = $this->language->get('column_model');
		$data['column_quantity'] = $this->language->get('column_quantity');
		$data['column_price'] = $this->language->get('column_price');
		$data['column_total'] = $this->language->get('column_total');
		$data['column_action'] = $this->language->get('column_action');

		$data['button_save'] = $this->language->get('button_save');
		$data['button_cancel'] = $this->language->get('button_cancel');
		$data['button_continue'] = $this->language->get('button_continue');
		$data['button_back'] = $this->language->get('button_back');
		$data['button_refresh'] = $this->language->get('button_refresh');
		$data['button_product_add'] = $this->language->get('button_product_add');
		$data['button_voucher_add'] = $this->language->get('button_voucher_add');
		$data['button_apply'] = $this->language->get('button_apply');
		$data['button_upload'] = $this->language->get('button_upload');
		$data['button_remove'] = $this->language->get('button_remove');
		$data['button_ip_add'] = $this->language->get('button_ip_add');

		$data['tab_order'] = $this->language->get('tab_order');
		$data['tab_customer'] = $this->language->get('tab_customer');
		$data['tab_payment'] = $this->language->get('tab_payment');
		$data['tab_shipping'] = $this->language->get('tab_shipping');
		$data['tab_product'] = $this->language->get('tab_product');
		$data['tab_voucher'] = $this->language->get('tab_voucher');
		$data['tab_total'] = $this->language->get('tab_total');

		$data['token'] = $this->session->data['token'];

		$url = '';




		if (isset($this->request->get['filter_order_id'])) {
			$url .= '&filter_order_id=' . $this->request->get['filter_order_id'];
		}

		if (isset($this->request->get['filter_customer'])) {
			$url .= '&filter_customer=' . urlencode(html_entity_decode($this->request->get['filter_customer'], ENT_QUOTES, 'UTF-8'));
		}

		if (isset($this->request->get['filter_order_status'])) {
			$url .= '&filter_order_status=' . $this->request->get['filter_order_status'];
		}

		if (isset($this->request->get['filter_total'])) {
			$url .= '&filter_total=' . $this->request->get['filter_total'];
		}

		if (isset($this->request->get['filter_date_added'])) {
			$url .= '&filter_date_added=' . $this->request->get['filter_date_added'];
		}

		if (isset($this->request->get['filter_date_modified'])) {
			$url .= '&filter_date_modified=' . $this->request->get['filter_date_modified'];
		}

		if (isset($this->request->get['filter_seller_name'])) {
			$url .= '&filter_seller_name=' . $this->request->get['filter_seller_name'];
		}

		if (isset($this->request->get['sort'])) {
			$url .= '&sort=' . $this->request->get['sort'];
		}

		if (isset($this->request->get['order'])) {
			$url .= '&order=' . $this->request->get['order'];
		}

		if (isset($this->request->get['page'])) {
			$url .= '&page=' . $this->request->get['page'];
		}

		$data['breadcrumbs'] = array();

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_home'),
			'href' => $this->url->link('common/dashboard', 'token=' . $this->session->data['token'], 'SSL')
		);

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('heading_title'),
			'href' => $this->url->link('sale/order', 'token=' . $this->session->data['token'] . $url, 'SSL')
		);

		$data['cancel'] = $this->url->link('sale/order', 'token=' . $this->session->data['token'] . $url, 'SSL');

		if (isset($this->request->get['order_id'])) {
			$order_info = $this->model_sale_order->getOrder($this->request->get['order_id']);
		}

		if (!empty($order_info)) {

			$data['order_id'] = $this->request->get['order_id'];
			$data['store_id'] = $order_info['store_id'];

			$data['customer'] = $order_info['customer'];
			$data['customer_id'] = $order_info['customer_id'];
			$data['customer_group_id'] = $order_info['customer_group_id'];
			$data['firstname'] = $order_info['firstname'];
			$data['lastname'] = $order_info['lastname'];
			$data['email'] = $order_info['email'];
			$data['telephone'] = $order_info['telephone'];
			$data['fax'] = $order_info['fax'];
			$data['account_custom_field'] = $order_info['custom_field'];

			$this->load->model('customer/customer');

			$data['addresses'] = $this->model_customer_customer->getAddresses($order_info['customer_id']);

			$data['payment_firstname'] = $order_info['payment_firstname'];
			$data['payment_lastname'] = $order_info['payment_lastname'];
			$data['payment_company'] = $order_info['payment_company'];
			$data['payment_address_1'] = $order_info['payment_address_1'];
			$data['payment_address_2'] = $order_info['payment_address_2'];
			$data['payment_city'] = $order_info['payment_city'];
			$data['payment_postcode'] = $order_info['payment_postcode'];
			$data['payment_country_id'] = $order_info['payment_country_id'];
			$data['payment_zone_id'] = $order_info['payment_zone_id'];
			$data['payment_custom_field'] = $order_info['payment_custom_field'];
			$data['payment_method'] = $order_info['payment_method'];
			$data['payment_code'] = $order_info['payment_code'];

			$data['shipping_firstname'] = $order_info['shipping_firstname'];
			$data['shipping_lastname'] = $order_info['shipping_lastname'];
			$data['shipping_company'] = $order_info['shipping_company'];
			$data['shipping_address_1'] = $order_info['shipping_address_1'];
			$data['shipping_address_2'] = $order_info['shipping_address_2'];
			$data['shipping_city'] = $order_info['shipping_cityshipping_city'];

			$data['shipping_postcode'] = $order_info['shipping_postcode'];
			$data['shipping_country_id'] = $order_info['shipping_country_id'];
			$data['shipping_zone_id'] = $order_info['shipping_zone_id'];
			$data['shipping_custom_field'] = $order_info['shipping_custom_field'];
			$data['shipping_method'] = $order_info['shipping_method'];
			$data['shipping_code'] = $order_info['shipping_code'];

			// Products
			$data['order_products'] = array();

			$products = $this->model_sale_order->getOrderProducts($this->request->get['order_id']);

			foreach ($products as $product) {

				$data['seller_id'] = $product['seller_id'];


				$data['order_products'][] = array(
					'product_id' => $product['product_id'],
					'name'       => $product['name'],
					'model'      => $product['model'],
					'option'     => $this->model_sale_order->getOrderOptions($this->request->get['order_id'], $product['order_product_id']),
					'quantity'   => $product['quantity'],
					'price'      => $product['price'],
					'total'      => $product['total'],
					'reward'     => $product['reward']
				);
			}

			// Vouchers
			$data['order_vouchers'] = $this->model_sale_order->getOrderVouchers($this->request->get['order_id']);

			$data['coupon'] = '';
			$data['voucher'] = '';
			$data['reward'] = '';

			$data['order_totals'] = array();

			$order_totals = $this->model_sale_order->getOrderTotals($this->request->get['order_id']);

			foreach ($order_totals as $order_total) {
				// If coupon, voucher or reward points
				$start = strpos($order_total['title'], '(') + 1;
				$end = strrpos($order_total['title'], ')');

				if ($start && $end) {
					$data[$order_total['code']] = substr($order_total['title'], $start, $end - $start);
				}
			}

			$data['order_status_id'] = $order_info['order_status_id'];
			$data['comment'] = $order_info['comment'];
			$data['affiliate_id'] = $order_info['affiliate_id'];
			$data['affiliate'] = $order_info['affiliate_firstname'] . ' ' . $order_info['affiliate_lastname'];
			$data['currency_code'] = $order_info['currency_code'];
		} else {
			$data['seller_id'] = 0;
			$data['order_id'] = 0;
			$data['store_id'] = '';
			$data['customer'] = '';
			$data['customer_id'] = '';
			$data['customer_group_id'] = $this->config->get('config_customer_group_id');
			$data['firstname'] = '';
			$data['lastname'] = '';
			$data['email'] = '';
			$data['telephone'] = '';
			$data['fax'] = '';
			$data['customer_custom_field'] = array();

			$data['addresses'] = array();

			$data['payment_firstname'] = '';
			$data['payment_lastname'] = '';
			$data['payment_company'] = '';
			$data['payment_address_1'] = '';
			$data['payment_address_2'] = '';
			$data['payment_city'] = '';
			$data['payment_postcode'] = '';
			$data['payment_country_id'] = '';
			$data['payment_zone_id'] = '';
			$data['payment_custom_field'] = array();
			$data['payment_method'] = '';
			$data['payment_code'] = '';

			$data['shipping_firstname'] = '';
			$data['shipping_lastname'] = '';
			$data['shipping_company'] = '';
			$data['shipping_address_1'] = '';
			$data['shipping_address_2'] = '';
			$data['shipping_city'] = '';
			$data['shipping_postcode'] = '';
			$data['shipping_country_id'] = '';
			$data['shipping_zone_id'] = '';
			$data['shipping_custom_field'] = array();
			$data['shipping_method'] = '';
			$data['shipping_code'] = '';

			$data['order_products'] = array();
			$data['order_vouchers'] = array();
			$data['order_totals'] = array();

			$data['order_status_id'] = $this->config->get('config_order_status_id');
			$data['comment'] = '';
			$data['affiliate_id'] = '';
			$data['affiliate'] = '';
			$data['currency_code'] = $this->config->get('config_currency');

			$data['coupon'] = '';
			$data['voucher'] = '';
			$data['reward'] = '';
		}

		// Stores
		$this->load->model('setting/store');

		$data['stores'] = array();

		$data['stores'][] = array(
			'store_id' => 0,
			'name'     => $this->language->get('text_default'),
			'href'     => HTTPS_CATALOG
		);

		$results = $this->model_setting_store->getStores();

		foreach ($results as $result) {
			$data['stores'][] = array(
				'store_id' => $result['store_id'],
				'name'     => $result['name'],
				'href'     => $result['url']
			);
		}

		// Customer Groups
		$this->load->model('customer/customer_group');

		$data['customer_groups'] = $this->model_customer_customer_group->getCustomerGroups();

		// Custom Fields
		$this->load->model('customer/custom_field');

		$data['custom_fields'] = array();

		$filter_data = array(
			'sort'  => 'cf.sort_order',
			'order' => 'ASC'
		);

		$custom_fields = $this->model_customer_custom_field->getCustomFields($filter_data);

		foreach ($custom_fields as $custom_field) {
			$data['custom_fields'][] = array(
				'custom_field_id'    => $custom_field['custom_field_id'],
				'custom_field_value' => $this->model_customer_custom_field->getCustomFieldValues($custom_field['custom_field_id']),
				'name'               => $custom_field['name'],
				'value'              => $custom_field['value'],
				'type'               => $custom_field['type'],
				'location'           => $custom_field['location'],
				'sort_order'         => $custom_field['sort_order']
			);
		}

		$this->load->model('localisation/order_status');

		$data['order_statuses'] = $this->model_localisation_order_status->getOrderStatuses();

		$this->load->model('localisation/country');

		$data['countries'] = $this->model_localisation_country->getCountries();

		$this->load->model('localisation/currency');

		$data['currencies'] = $this->model_localisation_currency->getCurrencies();

		$data['voucher_min'] = $this->config->get('config_voucher_min');

		$this->load->model('sale/voucher_theme');

		$data['voucher_themes'] = $this->model_sale_voucher_theme->getVoucherThemes();

		// API login
		$this->load->model('user/api');

		$api_info = $this->model_user_api->getApi($this->config->get('config_api_id'));

		if ($api_info) {
			$data['api_id'] = $api_info['api_id'];
			$data['api_key'] = $api_info['key'];
			$data['api_ip'] = $this->request->server['REMOTE_ADDR'];
		} else {
			$data['api_id'] = '';
			$data['api_key'] = '';
			$data['api_ip'] = '';
		}

		$data['header'] = $this->load->controller('common/header');
		$data['column_left'] = $this->load->controller('common/column_left');
		$data['footer'] = $this->load->controller('common/footer');

		$data['city'] = $this->model_sale_order->getCity();

		//$deliveryChargesArr = $this->db->query("select oc_sellers.delivery_charges, oc_sellers.seller_id, oc_sellers.firstname, oc_sellers.lastname from oc_order_seller_shipping_fee join oc_sellers on oc_sellers.seller_id = oc_order_seller_shipping_fee.seller_id where oc_order_seller_shipping_fee.order_id = '$order_id'")->rows;
		/* echo"<pre>";
			print_r($data);
			exit; */
		$this->response->setOutput($this->load->view('sale/order_form.tpl', $data));
	}
	public function getcity($filter_city)
	{
		$this->load->model('sale/order');

		$json = array();
		if (isset($this->request->get['filter_city'])) {
			$filter_city = $this->request->get['filter_city'];
		} else {
			$filter_city = '';
		}

		$this->load->model('customer/customer');

		$filter_data = array(
			'filter_city'  => $filter_city
		);
		$results = $this->model_sale_order->getcitys($filter_data);

		foreach ($results as $result) {
			$json[] = array(
				//'name'              => strip_tags(html_entity_decode($result['name'], ENT_QUOTES, 'UTF-8')),
				'name'              => $result['shipping_city']
			);
		}

		$sort_order = array();

		foreach ($json as $key => $value) {
			$sort_order[$key] = $value['name'];
		}

		array_multisort($sort_order, SORT_ASC, $json);

		$this->response->addHeader('Content-Type: application/json');
		$this->response->setOutput(json_encode($json));
	}
	public function getarea()
	{
		$this->load->model('sale/order');
		$data = $this->model_sale_order->getarea($_POST['city_id']);
		echo '<option value="">Select Area</option>';

		if (!empty($data)) {
			foreach ($data as $info) {
				echo '<option value="' . $info['area_id'] . '">' . $info['name'] . '</option>';
			}
		} else {
			echo '<option value="">Area not available</option>';
		}
	}

	public function getseller()
	{
		$this->load->model('sale/order');
		$data = $this->model_sale_order->getseller($_POST['area_id']);
		echo '<option value="">Select Store</option>';
		if (!empty($data)) {
			foreach ($data as $info) {
				echo '<option value="' . $info['seller_id'] . '">' . $info['firstname'] . '</option>';
			}
		} else {
			echo '<option value="">Store not available</option>';
		}
	}

	public function info()
	{
		
		$this->load->model('sale/order');
		
		if (isset($this->request->get['order_id'])) {
			$order_id = $this->request->get['order_id'];
		} else {
			$order_id = 0;
		}

		$order_info = $this->model_sale_order->getOrder($order_id);
		$delivery_time = $this->model_sale_order->getOrderDeliveryTime($order_id);
		// echo '<pre>';
		// print_r($order_info);
		// echo '</pre>';
		// exit;
		if ($delivery_time) {
			$data['delivery_time'] = $delivery_time['delivery_time'] . " (Asia/Riyadh)";
		} else {
			$data['delivery_time'] = 'Not Available';
		}

		$data['riderName'] = $this->getRiderNameByOrderId($order_id);
		

		if (!empty($order_info)) {
			// echo 'FILE NAME: ' . __FILE__ . '<br> LINE NO: ' . __LINE__;
			// exit;

			$this->load->language('sale/order');

			$this->document->setTitle($this->language->get('heading_title'));

			$data['heading_title']              = $this->language->get('heading_title');
			$data['text_ip_add']                = sprintf($this->language->get('text_ip_add'), $this->request->server['REMOTE_ADDR']);
			$data['text_order_detail']          = $this->language->get('text_order_detail');
			$data['text_customer_detail']       = $this->language->get('text_customer_detail');
			$data['text_option']                = $this->language->get('text_option');
			$data['text_delivery_time']         = "Delivery Time";
			$data['text_store']                 = $this->language->get('text_store');
			$data['text_date_added']            = $this->language->get('text_date_added');
			$data['text_payment_method']        = $this->language->get('text_payment_method');
			$data['text_shipping_method']       = $this->language->get('text_shipping_method');
			$data['text_customer']              = $this->language->get('text_customer');
			$data['text_customer_group']        = $this->language->get('text_customer_group');
			$data['text_email']                 = $this->language->get('text_email');
			$data['text_telephone']             = $this->language->get('text_telephone');
			$data['text_invoice']               = $this->language->get('text_invoice');
			$data['text_reward']                = $this->language->get('text_reward');
			$data['text_affiliate']             = $this->language->get('text_affiliate');
			$data['text_order']                 = sprintf($this->language->get('text_order'), $this->request->get['order_id']);
			$data['text_payment_address']       = $this->language->get('text_payment_address');
			$data['text_shipping_address']      = $this->language->get('text_shipping_address');
			$data['text_comment']               = $this->language->get('text_comment');
			$data['text_account_custom_field']  = $this->language->get('text_account_custom_field');
			$data['text_payment_custom_field']  = $this->language->get('text_payment_custom_field');
			$data['text_shipping_custom_field'] = $this->language->get('text_shipping_custom_field');
			$data['text_browser']               = $this->language->get('text_browser');
			$data['text_ip']                    = $this->language->get('text_ip');
			$data['text_forwarded_ip']          = $this->language->get('text_forwarded_ip');
			$data['text_user_agent']            = $this->language->get('text_user_agent');
			$data['text_accept_language']       = $this->language->get('text_accept_language');
			$data['text_history']               = $this->language->get('text_history');
			$data['text_history_add']           = $this->language->get('text_history_add');
			$data['text_loading']               = $this->language->get('text_loading');
			$data['column_product']             = $this->language->get('column_product');
			$data['column_model']               = $this->language->get('column_model');
			$data['column_quantity']            = $this->language->get('column_quantity');
			$data['column_price']               = $this->language->get('column_price');
			$data['column_total']               = $this->language->get('column_total');
			$data['entry_order_status']         = $this->language->get('entry_order_status');
			$data['entry_notify']               = $this->language->get('entry_notify');
			$data['entry_override']             = $this->language->get('entry_override');
			$data['entry_comment']              = $this->language->get('entry_comment');
			$data['help_override']              = $this->language->get('help_override');
			$data['button_invoice_print']       = $this->language->get('button_invoice_print');
			$data['button_shipping_print']      = $this->language->get('button_shipping_print');
			$data['button_edit']                = $this->language->get('button_edit');
			$data['button_cancel']              = $this->language->get('button_cancel');
			$data['button_generate']            = $this->language->get('button_generate');
			$data['button_reward_add']          = $this->language->get('button_reward_add');
			$data['button_reward_remove']       = $this->language->get('button_reward_remove');
			$data['button_commission_add']      = $this->language->get('button_commission_add');
			$data['button_commission_remove']   = $this->language->get('button_commission_remove');
			$data['button_history_add']         = $this->language->get('button_history_add');
			$data['button_ip_add']              = $this->language->get('button_ip_add');
			$data['tab_history']                = $this->language->get('tab_history');
			$data['tab_additional']             = $this->language->get('tab_additional');
			$data['token']                      = $this->session->data['token'];

			$url = '';
			
			if (isset($this->request->get['filter_order_id'])) {
				$url .= '&filter_order_id=' . $this->request->get['filter_order_id'];
			}

			if (isset($this->request->get['filter_customer'])) {
				$url .= '&filter_customer=' . urlencode(html_entity_decode($this->request->get['filter_customer'], ENT_QUOTES, 'UTF-8'));
			}

			if (isset($this->request->get['filter_order_status'])) {
				$url .= '&filter_order_status=' . $this->request->get['filter_order_status'];
			}

			if (isset($this->request->get['filter_total'])) {
				$url .= '&filter_total=' . $this->request->get['filter_total'];
			}

			if (isset($this->request->get['filter_date_added'])) {
				$url .= '&filter_date_added=' . $this->request->get['filter_date_added'];
			}

			if (isset($this->request->get['filter_date_modified'])) {
				$url .= '&filter_date_modified=' . $this->request->get['filter_date_modified'];
			}

			if (isset($this->request->get['filter_seller_name'])) {
				$url .= '&filter_seller_name=' . $this->request->get['filter_seller_name'];
			}

			if (isset($this->request->get['sort'])) {
				$url .= '&sort=' . $this->request->get['sort'];
			}

			if (isset($this->request->get['order'])) {
				$url .= '&order=' . $this->request->get['order'];
			}

			if (isset($this->request->get['page'])) {
				$url .= '&page=' . $this->request->get['page'];
			}
			
			$data['breadcrumbs'] = array();

			$data['breadcrumbs'][] = array(
				'text' => $this->language->get('text_home'),
				'href' => $this->url->link('common/dashboard', 'token=' . $this->session->data['token'], 'SSL')
			);

			$data['breadcrumbs'][] = array(
				'text' => $this->language->get('heading_title'),
				'href' => $this->url->link('sale/order', 'token=' . $this->session->data['token'] . $url, 'SSL')
			);

			$data['shipping']       = $this->url->link('sale/order/shipping', 'token=' . $this->session->data['token'] . '&order_id=' . (int) $this->request->get['order_id'], 'SSL');
			$data['invoice']        = $this->url->link('sale/order/invoice', 'token=' . $this->session->data['token'] . '&order_id=' . (int) $this->request->get['order_id'], 'SSL');
			$data['invoice_mobile'] = $this->url->link('sale/order/invoice_mobile', 'token=' . $this->session->data['token'] . '&order_id=' . (int) $this->request->get['order_id'], 'SSL');
			$data['edit']           = $this->url->link('sale/order/edit', 'token=' . $this->session->data['token'] . '&order_id=' . (int) $this->request->get['order_id'], 'SSL');
			$data['cancel']         = $this->url->link('sale/order', 'token=' . $this->session->data['token'] . $url, 'SSL');
			$data['order_id']       = $this->request->get['order_id'];
			$data['store_name']     = $order_info['store_name'];
			$data['store_url']      = $order_info['store_url'];
			
			if (isset($_SERVER['HTTPS'])) {
				$data['store_url'] = str_replace('http://', 'https://', $data['store_url']);
			}

			if ($order_info['invoice_no']) {
				$data['invoice_no'] = $order_info['invoice_prefix'] . $order_info['invoice_no'];
			} else {
				$data['invoice_no'] = '';
			}

			$data['date_added'] = $order_info['date_added'];
			$data['firstname']  = $order_info['firstname'];
			$data['lastname']   = $order_info['lastname'];
			$data['storename']  = $order_info['storename'];

			if ($order_info['customer_id']) {
				$data['customer'] = $this->url->link('customer/customer/edit', 'token=' . $this->session->data['token'] . '&customer_id=' . $order_info['customer_id'], 'SSL');
			} else {
				$data['customer'] = '';
			}
			$this->load->model('customer/customer_group');
			
			$customer_group_info = $this->model_customer_customer_group->getCustomerGroup($order_info['customer_group_id']);
			

			if ($customer_group_info) {
				$data['customer_group'] = $customer_group_info['name'];
			} else {
				$data['customer_group'] = '';
			}

			$data['email']           = $order_info['email'];
			$data['telephone']       = $order_info['telephone'];
			$data['shipping_method'] = $order_info['shipping_method'];
			$data['payment_method']  = $order_info['payment_method'];
			
			
			// Payment Address
			if ($order_info['payment_address_format']) {
				$format = $order_info['payment_address_format'];
			} else {
				$format = '{firstname} {lastname}' . "\n" . '{company}' . "\n" . '{address_1}' . "\n" . '{address_2}' . "\n" . '{city} {postcode}' . "\n" . '{zone}' . "\n" . '{country}';
			}
			
			$find = array(
				'{firstname}',
				'{lastname}',
				'{company}',
				'{address_1}',
				'{address_2}',
				'{city}',
				'{postcode}',
				'{zone}',
				'{zone_code}',
				'{country}'
			);

			$replace = array(
				'firstname' => $order_info['payment_firstname'],
				'lastname'  => $order_info['payment_lastname'],
				'company'   => $order_info['payment_company'],
				'address_1' => $order_info['payment_address_1'],
				'address_2' => $order_info['payment_address_2'],
				'city'      => $order_info['payment_city'],
				'postcode'  => $order_info['payment_postcode'],
				'zone'      => $order_info['payment_zone'],
				'zone_code' => $order_info['payment_zone_code'],
				'country'   => $order_info['payment_country']
			);

			$data['payment_address'] = str_replace(array("\r\n", "\r", "\n"), '<br />', preg_replace(array("/\s\s+/", "/\r\r+/", "/\n\n+/"), '<br />', trim(str_replace($find, $replace, $format))));

			


			// Shipping Address
			if (isset($order_info['shipping_address_1'])) {
				$googleaddress = $order_info['shipping_address_1'];
			}
			if (isset($order_info['shipping_address_2'])) {
				$googleaddress .= ' ' . $order_info['shipping_address_2'];
			}
			if (isset($order_info['shipping_address_2'])) {
				$googleaddress .= ' ' . $order_info['shipping_city'];
			}

			if ($order_info['shipping_address_format']) {
				$format = $order_info['shipping_address_format'];
			} else {
				//$format = '{firstname} {lastname}' . "\n" . '{company}' . "\n" . '{address_1}' . "\n" . '{address_2}' . "\n" . '{city} {postcode}' . "\n" . '{zone}' . "\n" . '{country}';
				$format = '{firstname} {lastname}' . "\n" . '{company}' . "\n" . '<a href="https://maps.google.com/?q=' . $googleaddress . '" target="_blank">{address_1}</a>' . "\n" . '{address_2}' . "\n" . '{city} {postcode}' . "\n" . '{country}';
			}
			
			$find = array(
				'{firstname}',
				'{lastname}',
				'{company}',
				'{address_1}',
				'{address_2}',
				'{city}',
				'{postcode}',
				'{zone}',
				'{zone_code}',
				'{country}'
			);

			$replace = array(
				'firstname' => $order_info['shipping_firstname'],
				'lastname'  => $order_info['shipping_lastname'],
				'company'   => $order_info['shipping_company'],
				'address_1' => $order_info['shipping_address_1'],
				'address_2' => $order_info['shipping_address_2'],
				'city'      => $order_info['shipping_city'],
				'postcode'  => $order_info['shipping_postcode'],
				'zone'      => $order_info['shipping_zone'],
				'zone_code' => $order_info['shipping_zone_code'],
				'country'   => $order_info['shipping_country']
			);
			
			$data['shipping_address'] = str_replace(array("\r\n", "\r", "\n"), '<br />', preg_replace(array("/\s\s+/", "/\r\r+/", "/\n\n+/"), '<br />', trim(str_replace($find, $replace, $format))));
			$data['city']             = $order_info['shipping_city'];
			$data['total']            = $order_info['total'];
			$data['ship_add']         = $order_info['shipping_address_1'];
			// Uploaded files
			$this->load->model('tool/upload');

			$data['products'] = array();
			


			$products = $this->model_sale_order->getOrderProducts($this->request->get['order_id']);
			$price_status_totals = 0;
			
			foreach ($products as $product) {

				$option_data = array();

				$options = $this->model_sale_order->getOrderOptions($this->request->get['order_id'], $product['order_product_id']);

				foreach ($options as $option) {
					if ($option['type'] != 'file') {
						$option_data[] = array(
							'name'  => $option['name'],
							'value' => $option['value'],
							'type'  => $option['type']
						);
					} else {
						$upload_info = $this->model_tool_upload->getUploadByCode($option['value']);

						if ($upload_info) {
							$option_data[] = array(
								'name'  => $option['name'],
								'value' => $upload_info['name'],
								'type'  => $option['type'],
								'href'  => $this->url->link('tool/upload/download', 'token=' . $this->session->data['token'] . '&code=' . $upload_info['code'], 'SSL')
							);
						}
					}
				}

				if ($product['product_status'] == 1) {
					$product_status_name = 'Found';
				} elseif ($product['product_status'] == 2) {
					$product_status_name = 'Not Found';
					$not_found_total = $not_found_total + $product['total'];
				} else {
					$product_status_name = 'Processing';
				}
				$this->load->model('catalog/product');

				//	$product['name'] = $this->model_catalog_product->getProductNamenew($product['product_id']);	


				$data['products1'][] = array(
					'order_product_id' 		=> $product['order_product_id'],
					'product_id'       		=> $product['product_id'],
					'product_status'   		=> $product['product_status'],
					'product_status_name'	=> $product_status_name,
					'name'    	 	  		=> $product['name'],
					'name'    	 	  		=> $product['name'],
					'model'    		  		=> $product['model'],
					'option'   		  		=> $option_data,
					'quantity'		  		=> $product['quantity'],
					'price'    		  		=> $this->currency->format($product['price'] + ($this->config->get('config_tax') ? $product['tax'] : 0), $order_info['currency_code'], $order_info['currency_value']),
					'total'    		  		=> $this->currency->format($product['total'] + ($this->config->get('config_tax') ? ($product['tax'] * $product['quantity']) : 0), $order_info['currency_code'], $order_info['currency_value']),
					'href'     		   		=> $this->url->link('catalog/product/edit', 'token=' . $this->session->data['token'] . '&product_id=' . $product['product_id'], 'SSL')

				);
				if ($product['product_status'] == 2) {
					$total_status = number_format((float) $product['total'] + ($this->config->get('config_tax') ? ($product['tax'] * $product['quantity']) : 0), 2, '.', '');
					$price_status_totals += $total_status;
				}
			}
			
			$data['vouchers'] = array();

			$vouchers = $this->model_sale_order->getOrderVouchers($this->request->get['order_id']);

			foreach ($vouchers as $voucher) {
				$data['vouchers'][] = array(
					'description' => $voucher['description'],
					'amount'      => $this->currency->format($voucher['amount'], $order_info['currency_code'], $order_info['currency_value']),
					'href'        => $this->url->link('sale/voucher/edit', 'token=' . $this->session->data['token'] . '&voucher_id=' . $voucher['voucher_id'], 'SSL')
				);
			}

			$data['totals'] = array();

			$totals = $this->model_sale_order->getOrderTotals($this->request->get['order_id']);
			
			$order_id = $this->request->get['order_id'];
			/* echo $a = "
							select oc_sellers.delivery_charges, oc_sellers.seller_id, oc_sellers.firstname, oc_sellers.lastname 
							from oc_order_seller_shipping_fee 
							join oc_sellers on oc_sellers.seller_id = oc_order_seller_shipping_fee.seller_id 
							where oc_order_seller_shipping_fee.order_id = '$order_id'" ;
							 exit; */

			$deliveryChargesArr = $this->db->query("
			select oc_order_seller_shipping_fee.shipping_charges as delivery_charges, oc_sellers.seller_id,  oc_sellers.free_delivery_limit, oc_sellers.firstname, oc_sellers.lastname 
			from oc_order_seller_shipping_fee 
			join oc_sellers on oc_sellers.seller_id = oc_order_seller_shipping_fee.seller_id 
			where oc_order_seller_shipping_fee.order_id = '$order_id'")->rows;

			if (empty($deliveryChargesArr)) {
				$deliveryChargesArr = $this->db->query("SELECT delivery_charges,free_delivery_limit FROM `oc_sellers` WHERE `seller_id` = " . $order_info['store'])->rows;;
			}
			
			//var_dump($productIdStr);exit;

			$date_added = date("Y-m-d", strtotime($order_info['date_added']));
			$date = '2018-11-02';

			$beforDate = 0;
			$afterDate = 0;
			if ($order_info['date_added'] != '0000-00-00 00:00:00') {
				if ($date_added <= $date) {
					$beforDate = 1;
				} else {
					$afterDate = 1;
				}
			} else {
				$beforDate = 1;
			}
			foreach ($totals as $total) {
				if ($total['code'] == 'sub_total') {
					$order_sub_total = $total['value'];
				} elseif ($total['code'] == 'total') {
					$order_total = $total['value'];
				}
			}
			$vattotals_deliveryCharges = 0;
			

			foreach ($totals as $key => $total) {

				if ($total['code'] == 'vat') {
					$vattotals_deliveryCharges = 1;
					$deliveryCharges = 0;

					if (!empty($deliveryChargesArr)) {
						foreach ($deliveryChargesArr as $deliveryChargesRow) {

							if ($deliveryChargesRow['free_delivery_limit'] > $data_sub_total or $deliveryChargesRow['free_delivery_limit'] == 0) {
								$deliveryCharges = $deliveryChargesRow['delivery_charges'];
								$json['totals'][] = array(
									'title' => 'Delivery Charges',
									'text'  => $this->currency->format($deliveryChargesRow['delivery_charges']),
								);
							} else {
								$json['totals'][] = array(
									'title' => 'Delivery Charges',
									'text'  => $this->currency->format(0),
								);
							}
						}
					} else {
						if ($deliveryCharges1->row['free_delivery_limit'] > $data_sub_total or $deliveryCharges1->row['free_delivery_limit'] == 0) {
							$json['totals'][] = array(
								'title' => 'Delivery Charges',
								'text'  => $this->currency->format($deliveryCharges1->row['delivery_charges']),
							);
						} else {
							$json['totals'][] = array(
								'title' => 'Delivery Charges',
								'text'  => $this->currency->format(0),
							);
						}
					}

					foreach ($deliveryChargesArr as $deliveryChargesRow) {

						if ($order_total != $order_sub_total) {
							$deliveryCharges = $deliveryChargesRow['delivery_charges'];
							$data['totals'][] = array(
								'title' => 'Delivery Charges',
								'text'  => $this->currency->format($deliveryCharges),
							);
						} else {
							$deliveryCharges = $deliveryChargesRow['delivery_charges'];
							$data['totals'][] = array(
								'title' => 'Delivery Charges',
								'text'  => $this->currency->format(0),
							);
						}

						//$total['value'] += $deliveryCharges;
					}


					// echo"<pre>";
					// print_r($data['totals']);
					// exit;


				}


				if ($beforDate == 1) {
					if ($total['code'] == 'total') {
						$deliveryCharges = 0;

						foreach ($deliveryChargesArr as $deliveryChargesRow) {
							if ($order_total != $order_sub_total) {
								$deliveryCharges = $deliveryChargesRow['delivery_charges'];

								$total['value'] += $deliveryCharges;
							} else {
							}
						}
					}
				}
				if ($total['code'] == 'sub_total') {
					$total['value'] = $total['value'] - $price_status_totals;
				}

				if ($total['code'] == 'total') {
					$total['value'] = $total['value'] - $price_status_totals;
					$total_data  = $total['value'] - $price_status_totals;
				}
				
				if($total['code'] == ''){
					$total['code'] ='';
				}
				$data['totals'][] = array(
					'title' => $total['title'],
					'code'  => $total['code'],
					'text'  => $this->currency->format($total['value'], $order_info['currency_code'], $order_info['currency_value']),
				);
			}

			
			if ($order_total != $order_sub_total) {
				if ($vattotals_deliveryCharges == 0) {
					$deliveryChargeskey = count($data['totals']) - 1;
					$totallast = $data['totals'][$deliveryChargeskey];

					$data['totals'][$deliveryChargeskey] = array(
						'title' => 'Delivery Charges',
						'text'  => $this->currency->format($deliveryChargesArr[0]['delivery_charges']),
					);
					$data['totals'][$deliveryChargeskey + 1] = $totallast;
				}
			}
			
			
			$this->load->model('customer/customer');

			$data['reward'] = $order_info['reward'];

			$data['reward_total'] = $this->model_customer_customer->getTotalCustomerRewardsByOrderId($this->request->get['order_id']);

			$data['affiliate_firstname'] = $order_info['affiliate_firstname'];
			$data['affiliate_lastname'] = $order_info['affiliate_lastname'];
			
			if ($order_info['affiliate_id']) {
				$data['affiliate'] = $this->url->link('marketing/affiliate/edit', 'token=' . $this->session->data['token'] . '&affiliate_id=' . $order_info['affiliate_id'], 'SSL');
			} else {
				$data['affiliate'] = '';
			}

			$data['commission'] = $this->currency->format($order_info['commission'], $order_info['currency_code'], $order_info['currency_value']);
			
			$this->load->model('marketing/affiliate');

			$data['commission_total'] = $this->model_marketing_affiliate->getTotalTransactionsByOrderId($this->request->get['order_id']);

			$this->load->model('localisation/order_status');

			$order_status_info = $this->model_localisation_order_status->getOrderStatus($order_info['order_status_id']);
			
			if ($order_status_info) {
				$data['order_status'] = $order_status_info['name'];
			} else {
				$data['order_status'] = '';
			}

			$data['order_statuses'] = $this->model_localisation_order_status->getOrderStatuses();

			$data['order_status_id'] = $order_info['order_status_id'];

			$data['account_custom_field'] = $order_info['custom_field'];
			
			// Custom Fields
			$this->load->model('customer/custom_field');

			$data['account_custom_fields'] = array();

			$filter_data = array(
				'sort'  => 'cf.sort_order',
				'order' => 'ASC',
			);

			$custom_fields = $this->model_customer_custom_field->getCustomFields($filter_data);
			
			foreach ($custom_fields as $custom_field) {
				if ($custom_field['location'] == 'account' && isset($order_info['custom_field'][$custom_field['custom_field_id']])) {
					if ($custom_field['type'] == 'select' || $custom_field['type'] == 'radio') {
						$custom_field_value_info = $this->model_customer_custom_field->getCustomFieldValue($order_info['custom_field'][$custom_field['custom_field_id']]);

						if ($custom_field_value_info) {
							$data['account_custom_fields'][] = array(
								'name'  => $custom_field['name'],
								'value' => $custom_field_value_info['name']
							);
						}
					}

					if ($custom_field['type'] == 'checkbox' && is_array($order_info['custom_field'][$custom_field['custom_field_id']])) {
						foreach ($order_info['custom_field'][$custom_field['custom_field_id']] as $custom_field_value_id) {
							$custom_field_value_info = $this->model_customer_custom_field->getCustomFieldValue($custom_field_value_id);

							if ($custom_field_value_info) {
								$data['account_custom_fields'][] = array(
									'name'  => $custom_field['name'],
									'value' => $custom_field_value_info['name']
								);
							}
						}
					}

					if ($custom_field['type'] == 'text' || $custom_field['type'] == 'textarea' || $custom_field['type'] == 'file' || $custom_field['type'] == 'date' || $custom_field['type'] == 'datetime' || $custom_field['type'] == 'time') {
						$data['account_custom_fields'][] = array(
							'name'  => $custom_field['name'],
							'value' => $order_info['custom_field'][$custom_field['custom_field_id']]
						);
					}

					if ($custom_field['type'] == 'file') {
						$upload_info = $this->model_tool_upload->getUploadByCode($order_info['custom_field'][$custom_field['custom_field_id']]);

						if ($upload_info) {
							$data['account_custom_fields'][] = array(
								'name'  => $custom_field['name'],
								'value' => $upload_info['name']
							);
						}
					}
				}
			}
			
			// Custom fields
			$data['payment_custom_fields'] = array();

			foreach ($custom_fields as $custom_field) {
				if ($custom_field['location'] == 'address' && isset($order_info['payment_custom_field'][$custom_field['custom_field_id']])) {
					if ($custom_field['type'] == 'select' || $custom_field['type'] == 'radio') {
						$custom_field_value_info = $this->model_customer_custom_field->getCustomFieldValue($order_info['payment_custom_field'][$custom_field['custom_field_id']]);

						if ($custom_field_value_info) {
							$data['payment_custom_fields'][] = array(
								'name'  => $custom_field['name'],
								'value' => $custom_field_value_info['name'],
								'sort_order' => $custom_field['sort_order']
							);
						}
					}

					if ($custom_field['type'] == 'checkbox' && is_array($order_info['payment_custom_field'][$custom_field['custom_field_id']])) {
						foreach ($order_info['payment_custom_field'][$custom_field['custom_field_id']] as $custom_field_value_id) {
							$custom_field_value_info = $this->model_customer_custom_field->getCustomFieldValue($custom_field_value_id);

							if ($custom_field_value_info) {
								$data['payment_custom_fields'][] = array(
									'name'  => $custom_field['name'],
									'value' => $custom_field_value_info['name'],
									'sort_order' => $custom_field['sort_order']
								);
							}
						}
					}

					if ($custom_field['type'] == 'text' || $custom_field['type'] == 'textarea' || $custom_field['type'] == 'file' || $custom_field['type'] == 'date' || $custom_field['type'] == 'datetime' || $custom_field['type'] == 'time') {
						$data['payment_custom_fields'][] = array(
							'name'  => $custom_field['name'],
							'value' => $order_info['payment_custom_field'][$custom_field['custom_field_id']],
							'sort_order' => $custom_field['sort_order']
						);
					}

					if ($custom_field['type'] == 'file') {
						$upload_info = $this->model_tool_upload->getUploadByCode($order_info['payment_custom_field'][$custom_field['custom_field_id']]);

						if ($upload_info) {
							$data['payment_custom_fields'][] = array(
								'name'  => $custom_field['name'],
								'value' => $upload_info['name'],
								'sort_order' => $custom_field['sort_order']
							);
						}
					}
				}
			}
			
			// Shipping
			$data['shipping_custom_fields'] = array();
			
			foreach ($custom_fields as $custom_field) {
				if ($custom_field['location'] == 'address' && isset($order_info['shipping_custom_field'][$custom_field['custom_field_id']])) {
					if ($custom_field['type'] == 'select' || $custom_field['type'] == 'radio') {
						$custom_field_value_info = $this->model_customer_custom_field->getCustomFieldValue($order_info['shipping_custom_field'][$custom_field['custom_field_id']]);

						if ($custom_field_value_info) {
							$data['shipping_custom_fields'][] = array(
								'name'  => $custom_field['name'],
								'value' => $custom_field_value_info['name'],
								'sort_order' => $custom_field['sort_order']
							);
						}
					}

					if ($custom_field['type'] == 'checkbox' && is_array($order_info['shipping_custom_field'][$custom_field['custom_field_id']])) {
						foreach ($order_info['shipping_custom_field'][$custom_field['custom_field_id']] as $custom_field_value_id) {
							$custom_field_value_info = $this->model_customer_custom_field->getCustomFieldValue($custom_field_value_id);

							if ($custom_field_value_info) {
								$data['shipping_custom_fields'][] = array(
									'name'  => $custom_field['name'],
									'value' => $custom_field_value_info['name'],
									'sort_order' => $custom_field['sort_order']
								);
							}
						}
					}

					if ($custom_field['type'] == 'text' || $custom_field['type'] == 'textarea' || $custom_field['type'] == 'file' || $custom_field['type'] == 'date' || $custom_field['type'] == 'datetime' || $custom_field['type'] == 'time') {
						$data['shipping_custom_fields'][] = array(
							'name'  => $custom_field['name'],
							'value' => $order_info['shipping_custom_field'][$custom_field['custom_field_id']],
							'sort_order' => $custom_field['sort_order']
						);
					}

					if ($custom_field['type'] == 'file') {
						$upload_info = $this->model_tool_upload->getUploadByCode($order_info['shipping_custom_field'][$custom_field['custom_field_id']]);

						if ($upload_info) {
							$data['shipping_custom_fields'][] = array(
								'name'  => $custom_field['name'],
								'value' => $upload_info['name'],
								'sort_order' => $custom_field['sort_order']
							);
						}
					}
				}
			}
			
			$data['ip'] = $order_info['ip'];
			$data['forwarded_ip'] = $order_info['forwarded_ip'];
			$data['user_agent'] = $order_info['user_agent'];
			$data['accept_language'] = $order_info['accept_language'];

			// Additional Tabs
			$data['tabs'] = array();

			$this->load->model('extension/extension');
			
			//if(($order_info['payment_code'] != 'credit_or_debit_card') || ($order_info['payment_code'] != 'credit_or_debit_cart')){
			if (($order_info['payment_code'] != 'credit_or_debit_card') && ($order_info['payment_code'] != 'credit_or_debit_cart')) {
				
				if($order_info['payment_code'] != 'Apple_pay') {
					$content = $this->load->controller('payment/' . $order_info['payment_code'] . '/order');
				}
				
				if ($content) {
					$this->load->language('payment/' . $order_info['payment_code']);

					$data['tabs'][] = array(
						'code'    => $order_info['payment_code'],
						'title'   => $this->language->get('heading_title'),
						'content' => $content
					);
				}
				$extensions = $this->model_extension_extension->getInstalled('fraud');
				
				foreach ($extensions as $extension) {
					if ($this->config->get($extension . '_status')) {
						$this->load->language('fraud/' . $extension);

						$content = $this->load->controller('fraud/' . $extension . '/order');

						if ($content) {
							$data['tabs'][] = array(
								'code'    => $extension,
								'title'   => $this->language->get('heading_title'),
								'content' => $content
							);
						}
					}
				}
			}
			
			// vat 
				foreach($data['totals'] as $key=>$totals_data){
					
					if($totals_data['code']	== 'vat'){
						// echo $total_data;
						$vat = (float)$this->config->get('config_vat');
						// $vat = 15;
						if(isset($vat) && $vat > 0){
						   // $this->load->model('total/vat');
							$value= $vat_total = $total_data * $vat / ($vat+100);
							$data['totals'][$key]['text'] = 'SR '.round($value,2);
						}			
					}

				}
		
			// API login
			$this->load->model('user/api');

			$api_info = $this->model_user_api->getApi($this->config->get('config_api_id'));

			if ($api_info) {
				$data['api_id'] = $api_info['api_id'];
				$data['api_key'] = $api_info['key'];
				$data['api_ip'] = $this->request->server['REMOTE_ADDR'];
			} else {
				$data['api_id'] = '';
				$data['api_key'] = '';
				$data['api_ip'] = '';
			}
			
			$data['header'] = $this->load->controller('common/header');
			$data['column_left'] = $this->load->controller('common/column_left');
			$data['footer'] = $this->load->controller('common/footer');


			$this->response->setOutput($this->load->view('sale/order_info.tpl', $data));
		} else {
			// echo 'FILE NAME: ' . __FILE__ . '<br> LINE NO: ' . __LINE__;
			// exit;
			$this->load->language('error/not_found');

			$this->document->setTitle($this->language->get('heading_title'));

			$data['heading_title'] = $this->language->get('heading_title');

			$data['text_not_found'] = $this->language->get('text_not_found');

			$data['breadcrumbs'] = array();

			$data['breadcrumbs'][] = array(
				'text' => $this->language->get('text_home'),
				'href' => $this->url->link('common/dashboard', 'token=' . $this->session->data['token'], 'SSL')
			);

			$data['breadcrumbs'][] = array(
				'text' => $this->language->get('heading_title'),
				'href' => $this->url->link('error/not_found', 'token=' . $this->session->data['token'], 'SSL')
			);
			
			$data['header']      = $this->load->controller('common/header');
			$data['column_left'] = $this->load->controller('common/column_left');
			$data['footer']      = $this->load->controller('common/footer');

			$this->response->setOutput($this->load->view('error/not_found.tpl', $data));
		}
	}

	public function createInvoiceNo()
	{
		$this->load->language('sale/order');

		$json = array();

		if (!$this->user->hasPermission('modify', 'sale/order')) {
			$json['error'] = $this->language->get('error_permission');
		} elseif (isset($this->request->get['order_id'])) {
			if (isset($this->request->get['order_id'])) {
				$order_id = $this->request->get['order_id'];
			} else {
				$order_id = 0;
			}

			$this->load->model('sale/order');

			$invoice_no = $this->model_sale_order->createInvoiceNo($order_id);

			if ($invoice_no) {
				$json['invoice_no'] = $invoice_no;
			} else {
				$json['error'] = $this->language->get('error_action');
			}
		}

		$this->response->addHeader('Content-Type: application/json');
		$this->response->setOutput(json_encode($json));
	}

	public function addReward()
	{
		$this->load->language('sale/order');

		$json = array();

		if (!$this->user->hasPermission('modify', 'sale/order')) {
			$json['error'] = $this->language->get('error_permission');
		} else {
			if (isset($this->request->get['order_id'])) {
				$order_id = $this->request->get['order_id'];
			} else {
				$order_id = 0;
			}

			$this->load->model('sale/order');

			$order_info = $this->model_sale_order->getOrder($order_id);

			if ($order_info && $order_info['customer_id'] && ($order_info['reward'] > 0)) {
				$this->load->model('customer/customer');

				$reward_total = $this->model_customer_customer->getTotalCustomerRewardsByOrderId($order_id);

				if (!$reward_total) {
					$this->model_customer_customer->addReward($order_info['customer_id'], $this->language->get('text_order_id') . ' #' . $order_id, $order_info['reward'], $order_id);
				}
			}

			$json['success'] = $this->language->get('text_reward_added');
		}

		$this->response->addHeader('Content-Type: application/json');
		$this->response->setOutput(json_encode($json));
	}

	public function removeReward()
	{
		$this->load->language('sale/order');

		$json = array();

		if (!$this->user->hasPermission('modify', 'sale/order')) {
			$json['error'] = $this->language->get('error_permission');
		} else {
			if (isset($this->request->get['order_id'])) {
				$order_id = $this->request->get['order_id'];
			} else {
				$order_id = 0;
			}

			$this->load->model('sale/order');

			$order_info = $this->model_sale_order->getOrder($order_id);

			if ($order_info) {
				$this->load->model('customer/customer');

				$this->model_customer_customer->deleteReward($order_id);
			}

			$json['success'] = $this->language->get('text_reward_removed');
		}

		$this->response->addHeader('Content-Type: application/json');
		$this->response->setOutput(json_encode($json));
	}

	public function addCommission()
	{
		$this->load->language('sale/order');

		$json = array();

		if (!$this->user->hasPermission('modify', 'sale/order')) {
			$json['error'] = $this->language->get('error_permission');
		} else {
			if (isset($this->request->get['order_id'])) {
				$order_id = $this->request->get['order_id'];
			} else {
				$order_id = 0;
			}

			$this->load->model('sale/order');

			$order_info = $this->model_sale_order->getOrder($order_id);

			if ($order_info) {
				$this->load->model('marketing/affiliate');

				$affiliate_total = $this->model_marketing_affiliate->getTotalTransactionsByOrderId($order_id);

				if (!$affiliate_total) {
					$this->model_marketing_affiliate->addTransaction($order_info['affiliate_id'], $this->language->get('text_order_id') . ' #' . $order_id, $order_info['commission'], $order_id);
				}
			}

			$json['success'] = $this->language->get('text_commission_added');
		}

		$this->response->addHeader('Content-Type: application/json');
		$this->response->setOutput(json_encode($json));
	}

	public function removeCommission()
	{
		$this->load->language('sale/order');

		$json = array();

		if (!$this->user->hasPermission('modify', 'sale/order')) {
			$json['error'] = $this->language->get('error_permission');
		} else {
			if (isset($this->request->get['order_id'])) {
				$order_id = $this->request->get['order_id'];
			} else {
				$order_id = 0;
			}

			$this->load->model('sale/order');

			$order_info = $this->model_sale_order->getOrder($order_id);

			if ($order_info) {
				$this->load->model('marketing/affiliate');

				$this->model_marketing_affiliate->deleteTransaction($order_id);
			}

			$json['success'] = $this->language->get('text_commission_removed');
		}

		$this->response->addHeader('Content-Type: application/json');
		$this->response->setOutput(json_encode($json));
	}

	public function history()
	{
		$this->load->language('sale/order');

		$data['text_no_results'] = $this->language->get('text_no_results');

		$data['column_date_added'] = $this->language->get('column_date_added');
		$data['column_status'] = $this->language->get('column_status');
		$data['column_notify'] = $this->language->get('column_notify');
		$data['column_comment'] = $this->language->get('column_comment');

		if (isset($this->request->get['page'])) {
			$page = $this->request->get['page'];
		} else {
			$page = 1;
		}

		$data['histories'] = array();

		$this->load->model('sale/order');

		$results = $this->model_sale_order->getOrderHistories($this->request->get['order_id'], ($page - 1) * 10, 10);

		foreach ($results as $result) {
			$data['histories'][] = array(
				'notify'     => $result['notify'] ? $this->language->get('text_yes') : $this->language->get('text_no'),
				'status'     => $result['status'],
				'comment'    => nl2br($result['comment']),
				'date_added' => date($this->language->get('datetime_format'), strtotime($result['date_added']))
			);
		}

		$history_total = $this->model_sale_order->getTotalOrderHistories($this->request->get['order_id']);

		$pagination = new Pagination();
		$pagination->total = $history_total;
		$pagination->page = $page;
		$pagination->limit = 10;
		$pagination->url = $this->url->link('sale/order/history', 'token=' . $this->session->data['token'] . '&order_id=' . $this->request->get['order_id'] . '&page={page}', 'SSL');

		$data['pagination'] = $pagination->render();

		$data['results'] = sprintf($this->language->get('text_pagination'), ($history_total) ? (($page - 1) * 10) + 1 : 0, ((($page - 1) * 10) > ($history_total - 10)) ? $history_total : ((($page - 1) * 10) + 10), $history_total, ceil($history_total / 10));

		$this->response->setOutput($this->load->view('sale/order_history.tpl', $data));
	}

	public function invoice_mobile()
	{

		$this->load->language('sale/order');

		$data['title'] = $this->language->get('text_invoice');

		if ($this->request->server['HTTPS']) {
			$data['base'] = HTTPS_SERVER;
		} else {
			$data['base'] = HTTP_SERVER;
		}

		$data['direction'] = $this->language->get('direction');
		$data['lang'] = $this->language->get('code');

		$data['text_invoice'] = $this->language->get('text_invoice');
		$data['text_order_detail'] = $this->language->get('text_order_detail');
		$data['text_order_id'] = $this->language->get('text_order_id');
		$data['text_invoice_no'] = $this->language->get('text_invoice_no');
		$data['text_invoice_date'] = $this->language->get('text_invoice_date');
		$data['text_date_added'] = $this->language->get('text_date_added');
		$data['text_telephone'] = $this->language->get('text_telephone');
		$data['text_fax'] = $this->language->get('text_fax');
		$data['text_email'] = $this->language->get('text_email');
		$data['text_website'] = $this->language->get('text_website');
		$data['text_payment_address'] = $this->language->get('text_payment_address');
		$data['text_shipping_address'] = $this->language->get('text_shipping_address');
		$data['text_payment_method'] = $this->language->get('text_payment_method');
		$data['text_shipping_method'] = $this->language->get('text_shipping_method');
		$data['text_comment'] = $this->language->get('text_comment');

		$data['column_product'] = $this->language->get('column_product');
		$data['column_model'] = $this->language->get('column_upc');
		$data['column_quantity'] = $this->language->get('column_quantity');
		$data['column_price'] = $this->language->get('column_price');
		$data['column_total'] = $this->language->get('column_total');
		$data['column_telephone'] = $this->language->get('text_telephone');
		$data['column_store'] = $this->language->get('column_store');

		$this->load->model('sale/order');
		$this->load->model('catalog/product');
		$this->load->model('setting/setting');



		$data['orders'] = array();
		/* echo"<pre>";
		print_r($store);
		exit; */
		$orders = array();

		if (isset($this->request->post['selected'])) {
			$orders = $this->request->post['selected'];
		} elseif (isset($this->request->get['order_id'])) {
			$orders[] = $this->request->get['order_id'];
		}

		foreach ($orders as $order_id) {
			$store = $this->db->query("SELECT username  FROM oc_sellers WHERE seller_id = (SELECT seller_id FROM oc_order WHERE order_id = " . $order_id . ")");

			$data['store'] = $store->row['username'];

			$order_info1 = $this->model_sale_order->getOrder($order_id);

			if ($order_info1) {
				$order_info = $order_info1;
				$store_info = $this->model_setting_setting->getSetting('config', $order_info['store_id']);

				if ($store_info) {
					$store_address = $store_info['config_address'];
					$store_email = $store_info['config_email'];
					$store_telephone = $store_info['config_telephone'];
					$store_fax = $store_info['config_fax'];
				} else {
					$store_address = $this->config->get('config_address');
					$store_email = $this->config->get('config_email');
					$store_telephone = $this->config->get('config_telephone');
					$store_fax = $this->config->get('config_fax');
				}

				if ($order_info['invoice_no']) {
					$invoice_no = $order_info['invoice_prefix'] . $order_info['invoice_no'];
				} else {
					$invoice_no = '';
				}

				if ($order_info['payment_address_format']) {
					$format = $order_info['payment_address_format'];
				} else {
					$format = '{firstname} {lastname}' . "\n" . '{company}' . "\n" . '{address_1}' . "\n" . '{address_2}' . "\n" . '{city} {postcode}' . "\n" . '{zone}' . "\n" . '{country}';
				}

				$find = array(
					'{firstname}',
					'{lastname}',
					'{company}',
					'{address_1}',
					'{address_2}',
					'{city}',
					'{postcode}',
					'{zone}',
					'{zone_code}',
					'{country}'
				);

				$replace = array(
					'firstname' => $order_info['payment_firstname'],
					'lastname'  => $order_info['payment_lastname'],
					'company'   => $order_info['payment_company'],
					'address_1' => $order_info['payment_address_1'],
					'address_2' => $order_info['payment_address_2'],
					'city'      => $order_info['payment_city'],
					'postcode'  => $order_info['payment_postcode'],
					'zone'      => $order_info['payment_zone'],
					'zone_code' => $order_info['payment_zone_code'],
					'country'   => $order_info['payment_country']
				);

				$payment_address = str_replace(array("\r\n", "\r", "\n"), '<br />', preg_replace(array("/\s\s+/", "/\r\r+/", "/\n\n+/"), '<br />', trim(str_replace($find, $replace, $format))));

				if ($order_info['shipping_address_format']) {
					$format = $order_info['shipping_address_format'];
				} else {
					$format = '{firstname} {lastname}' . "\n" . '{company}' . "\n" . '{address_1}' . "\n" . '{address_2}' . "\n" . '{city} {postcode}' . "\n" . '{zone}' . "\n" . '{country}';
				}

				$find = array(
					'{firstname}',
					'{lastname}',
					'{company}',
					'{address_1}',
					'{address_2}',
					'{city}',
					'{postcode}',
					'{zone}',
					'{zone_code}',
					'{country}'
				);

				$store_name = $this->db->query("SELECT username  FROM oc_sellers WHERE seller_id = (SELECT seller_id FROM oc_order WHERE order_id = " . $order_id . ")");
				$replace = array(
					'firstname' => $order_info['shipping_firstname'],
					'lastname'  => $order_info['shipping_lastname'],
					'company'   => $order_info['shipping_company'],
					'address_1' => $order_info['shipping_address_1'],
					'address_2' => $order_info['shipping_address_2'],
					'city'      => $order_info['shipping_city'],
					'postcode'  => $order_info['shipping_postcode'],
					'zone'      => $order_info['shipping_zone'],
					'zone_code' => $order_info['shipping_zone_code'],
					'country'   => $order_info['shipping_country'],
					'telephone'   => $order_info['telephone'],
					'store'     => $store_name
				);

				$shipping_address =  $order_info['shipping_firstname'] . " " . $order_info['shipping_lastname'] . $order_info['storename'] . "<br/>" . $order_info['shipping_address_1'] . " , " . $order_info['shipping_address_2'] . " <br/> " . $order_info['shipping_city'] . " , " . $order_info['shipping_zone'] . " , " . $order_info['shipping_country'];

				//$shipping_address = str_replace(array("\r\n", "\r", "\n"), '<br />', preg_replace(array("/\s\s+/", "/\r\r+/", "/\n\n+/"), '<br />', trim(str_replace($find, $replace, $format))));

				$this->load->model('tool/upload');

				$product_data = array();

				$products = $this->model_sale_order->getOrderProducts($order_id);

				$price_status_totals = 0;

				foreach ($products as $product) {
					$option_data = array();

					$options = $this->model_sale_order->getOrderOptions($order_id, $product['order_product_id']);
					$product_name = $this->model_catalog_product->getProductenglishName($product['product_id']);
					foreach ($options as $option) {
						if ($option['type'] != 'file') {
							$value = $option['value'];
						} else {
							$upload_info = $this->model_tool_upload->getUploadByCode($option['value']);

							if ($upload_info) {
								$value = $upload_info['name'];
							} else {
								$value = '';
							}
						}

						$option_data[] = array(
							'name'  => $option['name'],
							'value' => $value
						);
					}

					$product_upc = $this->db->query("SELECT upc  FROM `oc_product` WHERE `product_id` =" . $product['product_id'] . "");
					$total_products = $product['total'] + ($this->config->get('config_tax') ? ($product['tax'] * $product['quantity']) : 0);
					$total_products = 0;

					$product_data[] = array(
						'name'     => $product_name,
						//'model'    => $product['model'],
						'model'    => $product_upc->row['upc'],
						'product_status'  => $product['product_status'],
						'option'   => $option_data,
						'quantity' => $product['quantity'],
						//'price'    => $this->currency->format($product['price'] + ($this->config->get('config_tax') ? $product['tax'] : 0), $order_info['currency_code'], $order_info['currency_value']),
						'price'    =>  number_format((float) $product['price'] + ($this->config->get('config_tax') ? $product['tax'] : 0), 2, '.', ''),
						//'total'    => $this->currency->format($product['total'] + ($this->config->get('config_tax') ? ($product['tax'] * $product['quantity']) : 0), $order_info['currency_code'], $order_info['currency_value'])
						'total'    => number_format((float) $product['total'] + ($this->config->get('config_tax') ? ($product['tax'] * $product['quantity']) : 0), 2, '.', '')
					);
					if ($product['product_status'] == 2) {
						$total_status = number_format((float) $product['total'] + ($this->config->get('config_tax') ? ($product['tax'] * $product['quantity']) : 0), 2, '.', '');
						$price_status_totals += $total_status;
					}
				}

				$voucher_data = array();

				$vouchers = $this->model_sale_order->getOrderVouchers($order_id);

				foreach ($vouchers as $voucher) {
					$voucher_data[] = array(
						'description' => $voucher['description'],
						'amount'      => $this->currency->format($voucher['amount'], $order_info['currency_code'], $order_info['currency_value'])
					);
				}


				$total_data = array();

				$totals = $this->model_sale_order->getOrderTotals($order_id);
				//oc_sellers.delivery_charges
				$deliveryChargesArr = $this->db->query("
							select oc_order_seller_shipping_fee.shipping_charges as delivery_charges, oc_sellers.seller_id, oc_sellers.firstname, oc_sellers.lastname 
							from oc_order_seller_shipping_fee 
							join oc_sellers on oc_sellers.seller_id = oc_order_seller_shipping_fee.seller_id 
							where oc_order_seller_shipping_fee.order_id = '" . $order_id . "'")->rows;

				//var_dump($productIdStr);exit;

				$date_added = date("Y-m-d", strtotime($order_info['date_added']));
				$date = '2018-11-02';

				$beforDate = 0;
				$afterDate = 0;
				if ($order_info['date_added'] != '0000-00-00 00:00:00') {
					if ($date_added <= $date) {
						$beforDate = 1;
					} else {
						$afterDate = 1;
					}
				} else {
					$beforDate = 1;
				}

				foreach ($totals as $total) {
					if ($total['code'] == 'sub_total') {
						$order_sub_total = $total['value'];
					} elseif ($total['code'] == 'total') {
						$order_total = $total['value'];
					}
				}

				foreach ($totals as $total) {

					if ($total['code'] == 'vat') {
						$deliveryCharges = 0;
						foreach ($deliveryChargesArr as $deliveryChargesRow) {
							if ($order_total != $order_sub_total) {
								$deliveryCharges = $deliveryChargesRow['delivery_charges'];
								$total_data[] = array(
									'title' => $deliveryChargesRow['firstname'] . ' ' . $deliveryChargesRow['lastname'] . ' Shipping Fee',
									'text'  => $this->currency->format($deliveryCharges),
								);
							} else {
								$total_data[] = array(
									'title' => $deliveryChargesRow['firstname'] . ' ' . $deliveryChargesRow['lastname'] . ' Shipping Fee',
									'text'  => $this->currency->format(0),
								);
							}
							//$total['value'] += $deliveryCharges;
						}
					}

					if ($total['code'] == 'sub_total') {
						$total['value'] = $total['value'] - $price_status_totals;
					}

					if ($beforDate == 1) {
						if ($total['code'] == 'total') {
							$deliveryCharges = 0;
							foreach ($deliveryChargesArr as $deliveryChargesRow) {
								if ($order_total != $order_sub_total) {
									$deliveryCharges = $deliveryChargesRow['delivery_charges'];

									$total['value'] += $deliveryCharges;
								}
							}
							//$total['value'] = $total['value'] - $price_status_totals;

						}
					} else {
						$total['value'] = $total['value'] - $price_status_totals;
					}


					$total_data[] = array(
						'title' => $total['title'],
						'text'  => $this->currency->format($total['value'], $order_info['currency_code'], $order_info['currency_value']),
					);
				}
				// echo'<pre>';
				// print_r($data);
				// exit;

				$delivery_time = $this->model_sale_order->getOrderDeliveryTime($order_id);
				if ($delivery_time) {
					$delivery_time = $delivery_time['delivery_time'] . " (Asia/Riyadh)";
				} else {
					$delivery_time = 'Not Available';
				}


				$data['orders'][] = array(
					'order_id'	         => $order_id,
					'invoice_no'         => $invoice_no,
					'customer'         	 => $order_info1['customer'],
					'date_added'         => date($this->language->get('date_format_short'), strtotime($order_info['date_added'])),
					'store_name'         => $order_info['store_name'],
					'store_url'          => rtrim($order_info['store_url'], '/'),
					'store_address'      => nl2br($store_address),
					'store_email'        => $store_email,
					'store_telephone'    => $store_telephone,
					'store_fax'          => $store_fax,
					'email'              => $order_info['email'],
					'telephone'          => $order_info['telephone'],
					'shipping_address'   => $shipping_address,
					'shipping_method'    => $order_info['shipping_method'],
					'payment_address'    => $payment_address,
					'payment_method'     => $order_info['payment_method'],
					'product'            => $product_data,
					'voucher'            => $voucher_data,
					'total'              => $total_data,
					'store'         => $store_name,
					'comment'            => nl2br($order_info['comment']),
					'delivery_time'            => $delivery_time
				);
			}
		}
		// echo'<pre>';
		// print_r($data);
		// exit;
		$this->response->setOutput($this->load->view('sale/order_invoice_mobile.tpl', $data));
	}

	public function invoice()
	{

		$this->load->language('sale/order');

		$data['title'] = $this->language->get('text_invoice');

		if ($this->request->server['HTTPS']) {
			$data['base'] = HTTPS_SERVER;
		} else {
			$data['base'] = HTTP_SERVER;
		}

		$data['direction'] = $this->language->get('direction');
		$data['lang'] = $this->language->get('code');

		$data['text_invoice'] = $this->language->get('text_invoice');
		$data['text_order_detail'] = $this->language->get('text_order_detail');
		$data['text_order_id'] = $this->language->get('text_order_id');
		$data['text_invoice_no'] = $this->language->get('text_invoice_no');
		$data['text_invoice_date'] = $this->language->get('text_invoice_date');
		$data['text_date_added'] = $this->language->get('text_date_added');
		$data['text_telephone'] = $this->language->get('text_telephone');
		$data['text_fax'] = $this->language->get('text_fax');
		$data['text_email'] = $this->language->get('text_email');
		$data['text_website'] = $this->language->get('text_website');
		$data['text_payment_address'] = $this->language->get('text_payment_address');
		$data['text_shipping_address'] = $this->language->get('text_shipping_address');
		$data['text_payment_method'] = $this->language->get('text_payment_method');
		$data['text_shipping_method'] = $this->language->get('text_shipping_method');
		$data['text_comment'] = $this->language->get('text_comment');

		$data['column_product'] = $this->language->get('column_product');
		$data['column_model'] = $this->language->get('column_upc');
		$data['column_quantity'] = $this->language->get('column_quantity');
		$data['column_price'] = $this->language->get('column_price');
		$data['column_total'] = $this->language->get('column_total');
		$data['column_telephone'] = $this->language->get('text_telephone');
		$data['column_store'] = $this->language->get('column_store');

		$this->load->model('sale/order');
		$this->load->model('catalog/product');
		$this->load->model('setting/setting');



		$data['orders'] = array();
		/* echo"<pre>";
		print_r($store);
		exit; */
		$orders = array();

		if (isset($this->request->post['selected'])) {
			$orders = $this->request->post['selected'];
		} elseif (isset($this->request->get['order_id'])) {
			$orders[] = $this->request->get['order_id'];
		}

		foreach ($orders as $order_id) {
			$store = $this->db->query("SELECT username  FROM oc_sellers WHERE seller_id = (SELECT seller_id FROM oc_order WHERE order_id = " . $order_id . ")");

			$data['store'] = $store->row['username'];

			$order_info1 = $this->model_sale_order->getOrder($order_id);

			if ($order_info1) {
				$order_info = $order_info1;
				$store_info = $this->model_setting_setting->getSetting('config', $order_info['store_id']);

				if ($store_info) {
					$store_address = $store_info['config_address'];
					$store_email = $store_info['config_email'];
					$store_telephone = $store_info['config_telephone'];
					$store_fax = $store_info['config_fax'];
				} else {
					$store_address = $this->config->get('config_address');
					$store_email = $this->config->get('config_email');
					$store_telephone = $this->config->get('config_telephone');
					$store_fax = $this->config->get('config_fax');
				}

				if ($order_info['invoice_no']) {
					$invoice_no = $order_info['invoice_prefix'] . $order_info['invoice_no'];
				} else {
					$invoice_no = '';
				}

				if ($order_info['payment_address_format']) {
					$format = $order_info['payment_address_format'];
				} else {
					$format = '{firstname} {lastname}' . "\n" . '{company}' . "\n" . '{address_1}' . "\n" . '{address_2}' . "\n" . '{city} {postcode}' . "\n" . '{zone}' . "\n" . '{country}';
				}

				$find = array(
					'{firstname}',
					'{lastname}',
					'{company}',
					'{address_1}',
					'{address_2}',
					'{city}',
					'{postcode}',
					'{zone}',
					'{zone_code}',
					'{country}'
				);

				$replace = array(
					'firstname' => $order_info['payment_firstname'],
					'lastname'  => $order_info['payment_lastname'],
					'company'   => $order_info['payment_company'],
					'address_1' => $order_info['payment_address_1'],
					'address_2' => $order_info['payment_address_2'],
					'city'      => $order_info['payment_city'],
					'postcode'  => $order_info['payment_postcode'],
					'zone'      => $order_info['payment_zone'],
					'zone_code' => $order_info['payment_zone_code'],
					'country'   => $order_info['payment_country']
				);

				$payment_address = str_replace(array("\r\n", "\r", "\n"), '<br />', preg_replace(array("/\s\s+/", "/\r\r+/", "/\n\n+/"), '<br />', trim(str_replace($find, $replace, $format))));

				if ($order_info['shipping_address_format']) {
					$format = $order_info['shipping_address_format'];
				} else {
					$format = '{firstname} {lastname}' . "\n" . '{company}' . "\n" . '{address_1}' . "\n" . '{address_2}' . "\n" . '{city} {postcode}' . "\n" . '{zone}' . "\n" . '{country}';
				}

				$find = array(
					'{firstname}',
					'{lastname}',
					'{company}',
					'{address_1}',
					'{address_2}',
					'{city}',
					'{postcode}',
					'{zone}',
					'{zone_code}',
					'{country}'
				);

				$store_name = $this->db->query("SELECT username  FROM oc_sellers WHERE seller_id = (SELECT seller_id FROM oc_order WHERE order_id = " . $order_id . ")");
				$replace = array(
					'firstname' => $order_info['shipping_firstname'],
					'lastname'  => $order_info['shipping_lastname'],
					'company'   => $order_info['shipping_company'],
					'address_1' => $order_info['shipping_address_1'],
					'address_2' => $order_info['shipping_address_2'],
					'city'      => $order_info['shipping_city'],
					'postcode'  => $order_info['shipping_postcode'],
					'zone'      => $order_info['shipping_zone'],
					'zone_code' => $order_info['shipping_zone_code'],
					'country'   => $order_info['shipping_country'],
					'telephone'   => $order_info['telephone'],
					'store'     => $store_name
				);

				$shipping_address =  $order_info['shipping_firstname'] . " " . $order_info['shipping_lastname'] . $order_info['storename'] . "<br/>" . $order_info['shipping_address_1'] . " , " . $order_info['shipping_address_2'] . " <br/> " . $order_info['shipping_city'] . " , " . $order_info['shipping_zone'] . " , " . $order_info['shipping_country'];

				//$shipping_address = str_replace(array("\r\n", "\r", "\n"), '<br />', preg_replace(array("/\s\s+/", "/\r\r+/", "/\n\n+/"), '<br />', trim(str_replace($find, $replace, $format))));

				$this->load->model('tool/upload');

				$product_data = array();

				$products = $this->model_sale_order->getOrderProducts($order_id);
				$price_status_totals = 0;
				foreach ($products as $product) {
					$option_data = array();

					$options = $this->model_sale_order->getOrderOptions($order_id, $product['order_product_id']);
					$product_name = $this->model_catalog_product->getProductNamenew($product['product_id']);
					foreach ($options as $option) {
						if ($option['type'] != 'file') {
							$value = $option['value'];
						} else {
							$upload_info = $this->model_tool_upload->getUploadByCode($option['value']);

							if ($upload_info) {
								$value = $upload_info['name'];
							} else {
								$value = '';
							}
						}

						$option_data[] = array(
							'name'  => $option['name'],
							'value' => $value
						);
					}

					$product_upc = $this->db->query("SELECT upc  FROM `oc_product` WHERE `product_id` =" . $product['product_id'] . "");

					$product_data[] = array(
						'name'     => $product_name,
						//'model'    => $product['model'],
						'product_status'   		=> $product['product_status'],
						'model'    => $product_upc->row['upc'],
						'option'   => $option_data,
						'quantity' => $product['quantity'],
						'price'    => $this->currency->format($product['price'] + ($this->config->get('config_tax') ? $product['tax'] : 0), $order_info['currency_code'], $order_info['currency_value']),
						'total'    => $this->currency->format($product['total'] + ($this->config->get('config_tax') ? ($product['tax'] * $product['quantity']) : 0), $order_info['currency_code'], $order_info['currency_value'])
					);
					if ($product['product_status'] == 2) {
						$total_status = number_format((float) $product['total'] + ($this->config->get('config_tax') ? ($product['tax'] * $product['quantity']) : 0), 2, '.', '');
						$price_status_totals += $total_status;
					}
				}

				$voucher_data = array();

				$vouchers = $this->model_sale_order->getOrderVouchers($order_id);

				foreach ($vouchers as $voucher) {
					$voucher_data[] = array(
						'description' => $voucher['description'],
						'amount'      => $this->currency->format($voucher['amount'], $order_info['currency_code'], $order_info['currency_value'])
					);
				}


				$total_data = array();

				$totals = $this->model_sale_order->getOrderTotals($order_id);

				$deliveryChargesArr = $this->db->query("
							select oc_order_seller_shipping_fee.shipping_charges as delivery_charges, oc_sellers.seller_id, oc_sellers.firstname, oc_sellers.lastname 
							from oc_order_seller_shipping_fee 
							join oc_sellers on oc_sellers.seller_id = oc_order_seller_shipping_fee.seller_id 
							where oc_order_seller_shipping_fee.order_id = '" . $order_id . "'")->rows;

				//var_dump($productIdStr);exit;

				$date_added = date("Y-m-d", strtotime($order_info['date_added']));
				$date = '2018-11-02';

				$beforDate = 0;
				$afterDate = 0;
				if ($order_info['date_added'] != '0000-00-00 00:00:00') {
					if ($date_added <= $date) {
						$beforDate = 1;
					} else {
						$afterDate = 1;
					}
				} else {
					$beforDate = 1;
				}

				foreach ($totals as $total) {
					if ($total['code'] == 'sub_total') {
						$order_sub_total = $total['value'];
					} elseif ($total['code'] == 'total') {
						$order_total = $total['value'];
					}
				}

				foreach ($totals as $total) {

					if ($total['code'] == 'sub_total') {
						$total['value'] = $total['value'] - $price_status_totals;
					}
					if ($total['code'] == 'vat') {
						$deliveryCharges = 0;
						foreach ($deliveryChargesArr as $deliveryChargesRow) {
							if ($order_total != $order_sub_total) {
								$deliveryCharges = $deliveryChargesRow['delivery_charges'];
								$total_data[] = array(
									'title' => $deliveryChargesRow['firstname'] . ' ' . $deliveryChargesRow['lastname'] . ' Shipping Fee',
									'text'  => $this->currency->format($deliveryCharges),
								);
							} else {
								$total_data[] = array(
									'title' => $deliveryChargesRow['firstname'] . ' ' . $deliveryChargesRow['lastname'] . ' Shipping Fee',
									'text'  => $this->currency->format(0),
								);
							}
							//$total['value'] += $deliveryCharges;
						}
					}

					if ($beforDate == 1) {
						if ($total['code'] == 'total') {
							$deliveryCharges = 0;
							foreach ($deliveryChargesArr as $deliveryChargesRow) {
								if ($order_total != $order_sub_total) {
									$deliveryCharges = $deliveryChargesRow['delivery_charges'];

									$total['value'] += $deliveryCharges;
								}
							}
						}
					}
					if ($total['code'] == 'total') {
						$total['value'] = $total['value'] - $price_status_totals;
					}


					$total_data[] = array(
						'title' => $total['title'],
						'text'  => $this->currency->format($total['value'], $order_info['currency_code'], $order_info['currency_value']),
					);
				}

				$delivery_time = $this->model_sale_order->getOrderDeliveryTime($order_id);
				if ($delivery_time) {
					$delivery_time = $delivery_time['delivery_time'] . " (Asia/Riyadh)";
				} else {
					$delivery_time = 'Not Available';
				}


				$data['orders'][] = array(
					'order_id'	         => $order_id,
					'invoice_no'         => $invoice_no,
					'date_added'         => date($this->language->get('date_format_short'), strtotime($order_info['date_added'])),
					'store_name'         => $order_info['store_name'],
					'store_url'          => rtrim($order_info['store_url'], '/'),
					'store_address'      => nl2br($store_address),
					'store_email'        => $store_email,
					'store_telephone'    => $store_telephone,
					'store_fax'          => $store_fax,
					'email'              => $order_info['email'],
					'telephone'          => $order_info['telephone'],
					'shipping_address'   => $shipping_address,
					'shipping_method'    => $order_info['shipping_method'],
					'payment_address'    => $payment_address,
					'payment_method'     => $order_info['payment_method'],
					'product'            => $product_data,
					'voucher'            => $voucher_data,
					'total'              => $total_data,
					'store'         => $store_name,
					'comment'            => nl2br($order_info['comment']),
					'delivery_time'            => $delivery_time
				);
			}
		}

		$this->response->setOutput($this->load->view('sale/order_invoice.tpl', $data));
	}

	public function shipping()
	{

		$this->load->language('sale/order');

		$data['title'] = $this->language->get('text_shipping');

		if ($this->request->server['HTTPS']) {
			$data['base'] = HTTPS_SERVER;
		} else {
			$data['base'] = HTTP_SERVER;
		}

		$data['direction'] = $this->language->get('direction');
		$data['lang'] = $this->language->get('code');

		$data['text_shipping'] = $this->language->get('text_shipping');
		$data['text_picklist'] = $this->language->get('text_picklist');
		$data['text_order_no'] = $this->language->get('text_order_no');
		$data['text_order_detail'] = $this->language->get('text_order_detail');
		$data['text_order_id'] = $this->language->get('text_order_id');
		$data['text_invoice_no'] = $this->language->get('text_invoice_no');
		$data['text_invoice_date'] = $this->language->get('text_invoice_date');
		$data['text_date_added'] = $this->language->get('text_date_added');
		$data['text_telephone'] = $this->language->get('text_telephone');
		$data['text_fax'] = $this->language->get('text_fax');
		$data['text_email'] = $this->language->get('text_email');
		$data['text_website'] = $this->language->get('text_website');
		$data['text_contact'] = $this->language->get('text_contact');
		$data['text_payment_address'] = $this->language->get('text_payment_address');
		$data['text_delivery_address'] = $this->language->get('text_delivery_address');
		$data['text_shipping_method'] = $this->language->get('text_shipping_method');
		$data['text_sku'] = $this->language->get('text_sku');
		$data['text_upc'] = $this->language->get('text_upc');
		$data['text_ean'] = $this->language->get('text_ean');
		$data['text_jan'] = $this->language->get('text_jan');
		$data['text_isbn'] = $this->language->get('text_isbn');
		$data['text_mpn'] = $this->language->get('text_mpn');
		$data['text_comment'] = $this->language->get('text_comment');

		$data['column_location'] = $this->language->get('column_location');
		$data['column_reference'] = $this->language->get('column_reference');
		$data['column_product'] = $this->language->get('column_product');
		$data['column_weight'] = $this->language->get('column_weight');
		$data['column_model'] = $this->language->get('column_model');
		$data['column_quantity'] = $this->language->get('column_quantity');
		$data['column_price'] = $this->language->get('column_price');
		$data['column_category'] =  $this->language->get('column_category');

		$this->load->model('sale/order');

		$this->load->model('catalog/product');

		$this->load->model('setting/setting');

		$data['orders'] = array();

		$orders = array();

		if (isset($this->request->post['selected'])) {
			$orders = $this->request->post['selected'];
		} elseif (isset($this->request->get['order_id'])) {
			$orders[] = $this->request->get['order_id'];
		}

		foreach ($orders as $order_id) {
			$order_info = $this->model_sale_order->getOrder($order_id);

			// Make sure there is a shipping method
			if ($order_info && $order_info['shipping_code']) {
				$store_info = $this->model_setting_setting->getSetting('config', $order_info['store_id']);

				if ($store_info) {
					$store_address = $store_info['config_address'];
					$store_email = $store_info['config_email'];
					$store_telephone = $store_info['config_telephone'];
					$store_fax = $store_info['config_fax'];
				} else {
					$store_address = $this->config->get('config_address');
					$store_email = $this->config->get('config_email');
					$store_telephone = $this->config->get('config_telephone');
					$store_fax = $this->config->get('config_fax');
				}

				if ($order_info['invoice_no']) {
					$invoice_no = $order_info['invoice_prefix'] . $order_info['invoice_no'];
				} else {
					$invoice_no = '';
				}

				if ($order_info['shipping_address_format']) {
					$format = $order_info['shipping_address_format'];
				} else {
					$format = '{firstname} {lastname}' . "\n" . '{company}' . "\n" . '{address_1}' . "\n" . '{address_2}' . "\n" . '{city} {postcode}' . "\n" . '{zone}' . "\n" . '{country}';
				}

				$find = array(
					'{firstname}',
					'{lastname}',
					'{company}',
					'{address_1}',
					'{address_2}',
					'{city}',
					'{postcode}',
					'{zone}',
					'{zone_code}',
					'{country}'
				);

				$replace = array(
					'firstname' => $order_info['shipping_firstname'],
					'lastname'  => $order_info['shipping_lastname'],
					'company'   => $order_info['shipping_company'],
					'address_1' => $order_info['shipping_address_1'],
					'address_2' => $order_info['shipping_address_2'],
					'city'      => $order_info['shipping_city'],
					'postcode'  => $order_info['shipping_postcode'],
					'zone'      => $order_info['shipping_zone'],
					'zone_code' => $order_info['shipping_zone_code'],
					'country'   => $order_info['shipping_country']
				);
				$shipping_address =  $order_info['shipping_firstname'] . " " . $order_info['shipping_lastname'] . "<br/>" . $order_info['shipping_address_1'] . " , " . $order_info['shipping_address_2'] . " <br/> " . $order_info['shipping_city'] . " , " . $order_info['shipping_zone'] . " , " . $order_info['shipping_country'];
				//$shipping_address = str_replace(array("\r\n", "\r", "\n"), '<br />', preg_replace(array("/\s\s+/", "/\r\r+/", "/\n\n+/"), '<br />', trim(str_replace($find, $replace, $format))));

				$this->load->model('tool/upload');

				$product_data = array();

				$products = $this->model_sale_order->getOrderProducts($order_id);


				foreach ($products as $product) {
					$option_weight = '';

					$product_info = $this->model_catalog_product->getProduct($product['product_id']);
					$product_name = $this->model_catalog_product->getProductNamenew($product['product_id']);
					$product_categories = $this->model_catalog_product->getProductCategoriesInfo($product['product_id']);


					if ($product_info) {
						$option_data = array();

						$options = $this->model_sale_order->getOrderOptions($order_id, $product['order_product_id']);

						foreach ($options as $option) {
							$option_value_info = $this->model_catalog_product->getProductOptionValue($order_id, $product['order_product_id']);

							if ($option['type'] != 'file') {
								$value = $option['value'];
							} else {
								$upload_info = $this->model_tool_upload->getUploadByCode($option['value']);

								if ($upload_info) {
									$value = $upload_info['name'];
								} else {
									$value = '';
								}
							}

							$option_data[] = array(
								'name'  => $option['name'],
								'value' => $value
							);

							$product_option_value_info = $this->model_catalog_product->getProductOptionValue($product['product_id'], $option['product_option_value_id']);

							if ($product_option_value_info) {
								if ($product_option_value_info['weight_prefix'] == '+') {
									$option_weight += $product_option_value_info['weight'];
								} elseif ($product_option_value_info['weight_prefix'] == '-') {
									$option_weight -= $product_option_value_info['weight'];
								}
							}
						}
						//'name'     => $product_info['name'],
						$product_data[] = array(
							'name'     => $product_name,
							'model'    => $product_info['model'],
							'option'   => $option_data,
							'quantity' => $product['quantity'],
							'product_status'   		=> $product['product_status'],
							'price'    => $this->currency->format($product['price'] + ($this->config->get('config_tax') ? $product['tax'] : 0)),
							'category' => $product_categories,
							'location' => $product_info['location'],
							'sku'      => $product_info['sku'],
							'upc'      => $product_info['upc'],
							'ean'      => $product_info['ean'],
							'jan'      => $product_info['jan'],
							'isbn'     => $product_info['isbn'],
							'mpn'      => $product_info['mpn'],
							'weight'   => $this->weight->format(($product_info['weight'] + $option_weight) * $product['quantity'], $product_info['weight_class_id'], $this->language->get('decimal_point'), $this->language->get('thousand_point'))
						);
					}
				}

				$delivery_time = $this->model_sale_order->getOrderDeliveryTime($order_id);
				if ($delivery_time) {
					$delivery_time = $delivery_time['delivery_time'] . " (Asia/Riyadh)";
				} else {
					$delivery_time = 'Not Available';
				}
				//                             $delivery_time = 'Not Available'; 

				$data['orders'][] = array(
					'order_id'	       => $order_id,
					'invoice_no'       => $invoice_no,
					'date_added'       => date($this->language->get('date_format_short'), strtotime($order_info['date_added'])),
					'store_name'       => $order_info['store_name'],
					'store_url'        => rtrim($order_info['store_url'], '/'),
					'store_address'    => nl2br($store_address),
					'store_email'      => $store_email,
					'store_telephone'  => $store_telephone,
					'store_fax'        => $store_fax,
					'email'            => $order_info['email'],
					'telephone'        => $order_info['telephone'],
					'shipping_address' => $shipping_address,
					'shipping_method'  => $order_info['shipping_method'],
					'product'          => $product_data,
					'comment'          => nl2br($order_info['comment']),
					'delivery_time'          => $delivery_time
				);
			}
		}

		$this->response->setOutput($this->load->view('sale/order_shipping.tpl', $data));
	}
	public function getOrderList()
	{

		$ch = curl_init('http://149.56.18.34:8443/riderapi/order/GetOrderByAssignAndAcceptStatus');
		curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "GET");
		// curl_setopt($ch, CURLOPT_POSTFIELDS, $data_string);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($ch, CURLOPT_TIMEOUT, 5);
		curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 5);

		//execute post
		$result = curl_exec($ch);
		//close connection
		curl_close($ch);

		$orderRider =  (array) json_decode($result, true);

		return $orderRider;
	}

	function getRiderNameByOrderId($orderId)
	{
		$this->load->model('rider/rider');

		$riderName = $this->model_rider_rider->getRiderNameByOrderId($orderId);

		return json_encode($riderName);
	}
}
