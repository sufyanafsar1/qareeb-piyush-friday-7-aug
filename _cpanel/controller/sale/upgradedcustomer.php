<?php    
class ControllerSaleUpgradedcustomer extends Controller { 
	private $error = array();
  
  	public function index() {
		$this->language->load('sale/upgradedcustomer');
		 
		$this->document->setTitle($this->language->get('heading_title'));
		
		$this->load->model('sale/upgradedcustomer');
		$this->load->model('catalog/commission');
    	$this->getList();
  	}
  
   
    
  	protected function getList() {
		if (isset($this->request->get['filter_name'])) {
			$filter_name = $this->request->get['filter_name'];
		} else {
			$filter_name = null;
		}
		
		if (isset($this->request->get['filter_upgrade_date'])) {
			$filter_upgrade_date = $this->request->get['filter_upgrade_date'];
		} else {
			$filter_upgrade_date = null;
		}		
		
		if (isset($this->request->get['sort'])) {
			$sort = $this->request->get['sort'];
		} else {
			$sort = 'name'; 
		}
		
		if (isset($this->request->get['order'])) {
			$order = $this->request->get['order'];
		} else {
			$order = 'ASC';
		}
		
		if (isset($this->request->get['page'])) {
			$page = $this->request->get['page'];
		} else {
			$page = 1;
		}
						
		$url = '';

		if (isset($this->request->get['filter_name'])) {
			$url .= '&filter_name=' . urlencode(html_entity_decode($this->request->get['filter_name'], ENT_QUOTES, 'UTF-8'));
		}
					
		if (isset($this->request->get['filter_upgrade_date'])) {
			$url .= '&filter_upgrade_date=' . $this->request->get['filter_upgrade_date'];
		}
						
		if (isset($this->request->get['sort'])) {
			$url .= '&sort=' . $this->request->get['sort'];
		}

		if (isset($this->request->get['order'])) {
			$url .= '&order=' . $this->request->get['order'];
		}
		
		if (isset($this->request->get['page'])) {
			$url .= '&page=' . $this->request->get['page'];
		}

  		$data['breadcrumbs'] = array();

   		$data['breadcrumbs'][] = array(
       		'text'      => $this->language->get('text_home'),
			'href'      => $this->url->link('common/home', 'token=' . $this->session->data['token'], 'SSL'),
      		'separator' => false
   		);

   		$data['breadcrumbs'][] = array(
       		'text'      => $this->language->get('heading_title'),
			'href'      => $this->url->link('sale/upgradedcustomer', 'token=' . $this->session->data['token'] . $url, 'SSL'),
      		'separator' => ' :: '
   		);
		
		
		$data['upgradedcustomers'] = array();

		$data1 = array(
			'filter_name'              => $filter_name, 
			'filter_upgrade_date'        => $filter_upgrade_date,
			'sort'                     => $sort,
			'order'                    => $order,
			'start'                    => ($page - 1) * $this->config->get('config_admin_limit'),
			'limit'                    => $this->config->get('config_admin_limit')
		);
		
		$upgradedcustomer_total = $this->model_sale_upgradedcustomer->getTotalUpgradedcustomers($data1);
	
		$results = $this->model_sale_upgradedcustomer->getUpgradedcustomers($data1);
 
    	foreach ($results as $result) {
			$action = array();
			$group = "";
			$oldgroup = $this->model_catalog_commission->getCommission($result['old_commission_id']);
			if($oldgroup){
				$group = $oldgroup['commission_name'];
			}
			$data['upgradedcustomers'][] = array(
				'seller_id'    => $result['seller_id'],
				'name'           => $result['name'],
				'email'          => $result['email'],
				'amount'          => $result['amount'],
				'commission_id'          => $result['commission'],
				'old_commission_id'          => $group,
				'upgrade_date'     => date($this->language->get('date_format_short'), strtotime($result['upgrade_date'])),
				'expiry_date'     => date($this->language->get('date_format_short'), strtotime($result['expiration_date'])),
				'action'         => $action
			);
		}	
					
		$data['heading_title'] = $this->language->get('heading_title');
		$data['text_no_results'] = $this->language->get('text_no_results');
		$data['column_name'] = $this->language->get('column_name');
		$data['column_email'] = $this->language->get('column_email');
		$data['column_customer_old_group'] = $this->language->get('column_customer_old_group');
		$data['column_customer_new_group'] = $this->language->get('column_customer_new_group');
		$data['column_charges'] = $this->language->get('column_charges');
		$data['column_upgraded_date'] = $this->language->get('column_upgraded_date');
		$data['column_expiration_date'] = $this->language->get('column_expiration_date');
		$data['column_action'] = $this->language->get('column_action');		
		
		$data['button_filter'] = $this->language->get('button_filter');

		$data['token'] = $this->session->data['token'];

		if (isset($this->error['warning'])) {
			$data['error_warning'] = $this->error['warning'];
		} else {
			$data['error_warning'] = '';
		}
		
		if (isset($this->session->data['success'])) {
			$data['success'] = $this->session->data['success'];
		
			unset($this->session->data['success']);
		} else {
			$data['success'] = '';
		}
		
		$url = '';

		if (isset($this->request->get['filter_name'])) {
			$url .= '&filter_name=' . urlencode(html_entity_decode($this->request->get['filter_name'], ENT_QUOTES, 'UTF-8'));
		}
		
		if (isset($this->request->get['filter_email'])) {
			$url .= '&filter_email=' . urlencode(html_entity_decode($this->request->get['filter_email'], ENT_QUOTES, 'UTF-8'));
		}
			
		if (isset($this->request->get['filter_upgrade_date'])) {
			$url .= '&filter_upgrade_date=' . $this->request->get['filter_upgrade_date'];
		}
			
		if ($order == 'ASC') {
			$url .= '&order=DESC';
		} else {
			$url .= '&order=ASC';
		}

		if (isset($this->request->get['page'])) {
			$url .= '&page=' . $this->request->get['page'];
		}
		
		$data['sort_name'] = $this->url->link('sale/upgradedcustomer', 'token=' . $this->session->data['token'] . '&sort=name' . $url, 'SSL');
		
		$data['sort_upgrade_date'] = $this->url->link('sale/upgradedcustomer', 'token=' . $this->session->data['token'] . '&sort=u.upgrade_date' . $url, 'SSL');
		
		$url = '';

		if (isset($this->request->get['filter_name'])) {
			$url .= '&filter_name=' . urlencode(html_entity_decode($this->request->get['filter_name'], ENT_QUOTES, 'UTF-8'));
		}
		
		if (isset($this->request->get['filter_email'])) {
			$url .= '&filter_email=' . urlencode(html_entity_decode($this->request->get['filter_email'], ENT_QUOTES, 'UTF-8'));
		}
			
		if (isset($this->request->get['filter_upgrade_date'])) {
			$url .= '&filter_upgrade_date=' . $this->request->get['filter_upgrade_date'];
		}
			
		if (isset($this->request->get['sort'])) {
			$url .= '&sort=' . $this->request->get['sort'];
		}
												
		if (isset($this->request->get['order'])) {
			$url .= '&order=' . $this->request->get['order'];
		}

		$pagination = new Pagination();
		$pagination->total = $upgradedcustomer_total;
		$pagination->page = $page;
		$pagination->limit = $this->config->get('config_admin_limit');
		$pagination->text = $this->language->get('text_pagination');
		$pagination->url = $this->url->link('sale/upgradedcustomer', 'token=' . $this->session->data['token'] . $url . '&page={page}', 'SSL');
			
		$data['pagination'] = $pagination->render();
		
		$data['results'] = sprintf($this->language->get('text_pagination'), ($upgradedcustomer_total) ? (($page - 1) * $this->config->get('config_limit_admin')) + 1 : 0, ((($page - 1) * $this->config->get('config_limit_admin')) > ($upgradedcustomer_total - $this->config->get('config_limit_admin'))) ? $upgradedcustomer_total : ((($page - 1) * $this->config->get('config_limit_admin')) + $this->config->get('config_limit_admin')), $upgradedcustomer_total, ceil($upgradedcustomer_total / $this->config->get('config_limit_admin')));


		$data['filter_name'] = $filter_name;
		
		$data['filter_upgrade_date'] = $filter_upgrade_date;
			
		$data['sort'] = $sort;
		$data['order'] = $order;
		
		$data['header'] = $this->load->controller('common/header');
		$data['column_left'] = $this->load->controller('common/column_left');
		$data['footer'] = $this->load->controller('common/footer');

		$this->response->setOutput($this->load->view('sale/upgradedcustomer_list.tpl', $data));
  	}
}
?>