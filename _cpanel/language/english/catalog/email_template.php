<?php
// Heading
$_['heading_title']          = 'Email Templates';

// Text
$_['text_success']           = 'Success: You have modified email templates!';
$_['text_list']              = 'Email Templates List';
$_['text_add']               = 'Add Email Templates';
$_['text_edit']              = 'Edit Email Templates';
$_['text_default']           = 'Default';

// Column
$_['column_name']            = 'Email Templates Name';
$_['column_sort_order']      = 'Sort Order';
$_['column_action']          = 'Action';

// Entry
$_['entry_name']             = 'Email Templates Name';
$_['entry_description']      = 'Description';
$_['entry_subject'] 	     = 'Subject';
$_['entry_status']           = 'Status';


// Error
$_['error_warning']          = 'Warning: Please check the form carefully for errors!';
$_['error_permission']       = 'Warning: You do not have permission to modify email templates!';
$_['error_name']             = 'Email Template Name must be between 2 and 255 characters!';
$_['error_subject']      	 = 'Subject must be between 2 and 255 characters!';
