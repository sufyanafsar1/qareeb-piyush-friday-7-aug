<?php
// Heading
$_['heading_title']     = 'Barcode Requests';

// Column
$_['column_product']   = 'Product Name';
$_['column_upc_current']   = 'Current Bar Code';
$_['column_upc_new']   = 'New Bar Code';
$_['column_status']     = 'Status';
$_['column_total']      = 'Total';
$_['column_date_added'] = 'Date Added';
$_['column_action']     = 'Action';