<?php
// Heading
$_['heading_title']          = 'Areas';

// Text
$_['text_success']           = 'Success: You have modified areas!';
$_['text_list']              = 'Area List';
$_['text_add']               = 'Add area';
$_['text_edit']              = 'Edit area';

// Column
$_['column_name']            = 'Area Name';
$_['column_code']            = 'Area Code';
$_['column_name_ar']            = 'Arabic Name';
$_['column_city']         = 'City';
$_['column_action']          = 'Action';

// Entry
$_['entry_name']             = 'Area Name';
$_['entry_code']             = 'Area Code';
$_['entry_name_ar']             = 'Arabic Name';
$_['entry_city']          = 'City';
$_['entry_status']           = 'Status';

// Error
$_['error_permission']       = 'Warning: You do not have permission to modify areas!';
$_['error_name']             = 'Area Name must be between 3 and 128 characters!';
$_['error_default']          = 'Warning: This area cannot be deleted as it is currently assigned as the default store area!';
$_['error_store']            = 'Warning: This area cannot be deleted as it is currently assigned to %s stores!';
$_['error_address']          = 'Warning: This area cannot be deleted as it is currently assigned to %s address book entries!';
$_['error_affiliate']        = 'Warning: This area cannot be deleted as it is currently assigned to %s affiliates!';
