<?php
// Heading
$_['heading_title']          = 'Cities';

// Text
$_['text_success']           = 'Success: You have modified city!';
$_['text_list']              = 'City List';
$_['text_add']               = 'Add City';
$_['text_edit']              = 'Edit City';

// Column
$_['column_name']            = 'City Name';
$_['column_code']            = 'City Code';
$_['column_name_ar']            = 'Arabic Name';
$_['column_action']          = 'Action';

// Entry
$_['entry_name']             = 'City Name';
$_['entry_code']             = 'City Code';
$_['entry_name_ar']             = 'Arabic Name';
$_['entry_status']           = 'Status';

// Error
$_['error_permission']       = 'Warning: You do not have permission to modify cities!';
$_['error_name']             = 'City Name must be between 3 and 128 characters!';
$_['error_default']          = 'Warning: This city cannot be deleted as it is currently assigned as the default store city!';
$_['error_store']            = 'Warning: This city cannot be deleted as it is currently assigned to %s stores!';
$_['error_address']          = 'Warning: This city cannot be deleted as it is currently assigned to %s address book entries!';
$_['error_affiliate']        = 'Warning: This city cannot be deleted as it is currently assigned to %s affiliates!';
