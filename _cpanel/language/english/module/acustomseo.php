<?php
// Heading
$_['heading_title']    = 'A Custom Seo';

$_['text_module']      = 'Modules';
$_['text_success']     = 'Success: You have modified A Custom Seo module!';
$_['text_add']         = 'Add Seo url';
$_['text_edit']        = 'Edit Seo url';
$_['text_list']		   = 'Seo url List';

// Entry
$_['entry_url']        = 'Url';
$_['entry_seo_url']    = 'Seo url';
$_['entry_keyword']    = 'Keyword';

//Column
$_['column_url'] 	   = 'Url';
$_['column_seo_url']   = 'Seo url';
$_['column_action']	   = 'Action';

//Help
$_['help_url']		  = 'Ex. product_id=xx or category_id=xx or manufacturer_id=xx or information_id=4';

// Error
$_['error_permission']  	  = 'Warning: You do not have permission to modify A Custom Seo module!';
$_['error_url']      		  = 'Url required!';
$_['error_keyword']     	  = 'SEO url required!';
$_['error_keyword_exits']     = 'SEO url already in use!';