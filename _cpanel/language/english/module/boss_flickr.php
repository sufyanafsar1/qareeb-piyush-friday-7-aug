<?php
// Heading
$_['heading_title']       = 'Boss Flickr';

// Text
$_['text_module']         = 'Modules';
$_['text_success']        = 'Success: You have modified module Boss Flickr!';
$_['text_enabled']    	  = 'Enable';
$_['text_disabled']       = 'Disable';

// Entry
$_['entry_name']       = 'Module Name';
$_['entry_api_key']       = 'API Key';
$_['entry_user_id']       = 'User Id';
$_['entry_image_size']       = 'Image Size';
$_['entry_limit']         = 'Limit:';
$_['entry_status']        = 'Status:';
$_['entry_title']        = 'Title:';

// Error
$_['error_permission']   = 'Warning: You do not have permission to modify module Boss Flickr!';
$_['error_name']       = 'Module Name must be between 3 and 64 characters!';
$_['error_limit']      = 'Limit required!';
$_['error_api_key']      = 'Api Key required!';
$_['error_user_id']      = 'User Id required!';

?>