<?php
// Heading
$_['heading_title']    = 'Everything you Need Form';

$_['text_module']      = 'Modules';
$_['text_success']     = 'Success: You have modified Contact Form module!';
$_['text_edit']        = 'Edit Contact Form Module';

// Entry
$_['entry_status']     = 'Status';

// Error
$_['error_permission'] = 'Warning: You do not have permission to modify Contact Form module!';