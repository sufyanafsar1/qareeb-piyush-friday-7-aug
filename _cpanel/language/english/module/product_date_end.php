<?php
// Heading
$_['heading_title']       				   = '<span style="color:#0000ff">Product Date End</span>';

// Text
$_['text_module']         				   = 'Modules';
$_['text_success']        				   = 'Success: You have modified module Product Date End!';
$_['text_edit']        					   = 'Edit Module Product Date End';
$_['text_congratulations']				   = 'Congratulations! Extension has been installed!';

// Tab
$_['tab_general']		  				   = 'General';