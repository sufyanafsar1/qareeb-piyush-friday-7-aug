<?php
// Heading
$_['heading_title']       = 'Admin Activity Report';

// Text
$_['text_list']           = 'Admin Activity List';
$_['text_address_add']    = '<a href="user_id=%d">%s</a> added a new address.';
$_['text_address_edit']   = '<a href="user_id=%d">%s</a> updated their address.';
$_['text_add_user'] = '<a href="user_id=%d">%s</a> added new User.';
$_['text_delete'] = '<a href="user_id=%d">%s</a> deleted <a >%s</a>  Successfully.';
$_['text_edit']           = '<a href="user_id=%d">%s</a>  account details.';
$_['text_forgotten']      = '<a href="user_id=%d">%s</a> requested a new password.';
$_['text_login']          = '<a href="user_id=%d">%s</a> logged in.';
$_['text_password']       = '<a href="user_id=%d">%s</a> updated their account password.';
$_['text_register']       = '<a href="user_id=%d">%s</a> registered for an account.';
$_['text_return_account'] = '<a href="user_id=%d">%s</a> submitted a product return.';
$_['text_return_guest']   = '%s submitted a product return.';
$_['text_order_account']  = '<a href="user_id=%d">%s</a> created a <a href="order_id=%d">new order</a>.';
$_['text_order_guest']    = '%s created a <a href="order_id=%d">new order</a>.';

// Column
$_['column_user']     = 'Admin';
$_['column_comment']      = 'Comment';
$_['column_ip']           = 'IP';
$_['column_date_added']   = 'Date Added';

// Entry
$_['entry_user']      = 'Admin';
$_['entry_ip']            = 'IP';
$_['entry_date_start']    = 'Date Start';
$_['entry_date_end']      = 'Date End';