<?php
// Heading
$_['heading_title']              = 'Riders';
$_['heading_area_title']         = 'Rider Areas';

// Text
$_['text_list']                  = 'Rider List';
$_['text_area_list']                  = 'Areas List';
$_['text_add']                   = 'Add Rider';
$_['text_edit']                  = 'Edit Rider';
$_['text_rider_detail']          = 'Rider Details';

$_['text_email']                 = 'E-Mail';
$_['text_telephone']             = 'Telephone';


$_['text_ip']                    = 'IP Address';


// Column
$_['column_order_id']            = 'Order ID';
$_['column_customer']            = 'Customer';
$_['column_status']              = 'Status';
$_['column_date_added']          = 'Date Added';
$_['column_date_modified']       = 'Date Modified';
$_['column_total']               = 'Total';
$_['column_product']             = 'Product';
$_['column_model']               = 'Model';
$_['column_quantity']            = 'Quantity';
$_['column_price']               = 'Unit Price';
$_['column_comment']             = 'Comment';
$_['column_notify']              = 'Customer Notified';
$_['column_location']            = 'Location';
$_['column_reference']           = 'Reference';
$_['column_action']              = 'Action';
$_['column_weight']              = 'Product weight';
$_['column_category']            = 'Category Name';



// Entry

$_['entry_firstname']               = 'Name';
$_['entry_password']                = 'Password';
$_['entry_confirm_password']        = 'Confirm Password';
$_['entry_email']                   = 'E-Mail';
$_['entry_telephone']               = 'Mobile No.';
$_['entry_license']                 = 'Licenese No.';
$_['entry_address']                 = 'Address';
$_['entry_picture']                 = 'Picture';
$_['entry_area']                    = 'Area Name';
$_['entry_city']                    = 'City';
$_['entry_country']                 = 'Country';


// Help
$_['help_override']              = 'If the customers order is being blocked from changing the order status due to a anti-fraud extension enable override.';

// Error
$_['error_warning']              = 'Warning: Please check the form carefully for errors!';
$_['error_permission']           = 'Warning: You do not have permission to modify orders!';
$_['error_action']               = 'Warning: Could not complete this action!';
$_['error_filetype']			 = 'Invalid file type!';
