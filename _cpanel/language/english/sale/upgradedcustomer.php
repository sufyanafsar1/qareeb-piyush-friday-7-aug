<?php
// Heading
$_['heading_title']         = 'Upgraded Sellers History';
$_['column_name']           = 'Seller Name';
$_['column_email']          = 'E-Mail';
$_['column_customer_old_group'] = 'Old Plan';
$_['column_customer_new_group'] = 'New Plan';
$_['column_charges'] = 'Charges';
$_['column_upgraded_date']  = 'Upgraded Date';
$_['column_expiration_date']  = 'Expiration Date';
$_['column_action']         = 'Action';

?>