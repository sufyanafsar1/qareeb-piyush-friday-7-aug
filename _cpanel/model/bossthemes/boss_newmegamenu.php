<?php
class ModelBossthemesBossNewMegamenu extends Model { 
	public function createdb(){
		$sql = " SHOW TABLES LIKE '".DB_PREFIX."megamenu'";
		$query = $this->db->query( $sql );
		if( count($query->rows) > 0 ){
			
			$sql="delete from `".DB_PREFIX."megamenu`";
			$query = $this->db->query( $sql );
		}
		$sql = " SHOW TABLES LIKE '".DB_PREFIX."megamenu_column'";
		$query = $this->db->query( $sql );
		if( count($query->rows) > 0 ){
			
			$sql="delete from `".DB_PREFIX."megamenu_column`";
			$query = $this->db->query( $sql );
		}
		$sql = " SHOW TABLES LIKE '".DB_PREFIX."megamenu_description'";
		$query = $this->db->query( $sql );
		if( count($query->rows) > 0 ){
			
			$sql="delete from `".DB_PREFIX."megamenu_description`";
			$query = $this->db->query( $sql );
		}
		$sql = " SHOW TABLES LIKE '".DB_PREFIX."megamenu_row'";
		$query = $this->db->query( $sql );
		if( count($query->rows) > 0 ){
			
			$sql="delete from `".DB_PREFIX."megamenu_row`";
			$query = $this->db->query( $sql );
		}
		$sql = array();
		$sql[]  = "CREATE TABLE IF NOT EXISTS `".DB_PREFIX."megamenu` (
			  `menu_id` int(11) NOT NULL AUTO_INCREMENT,
			  `status` tinyint(1) NOT NULL DEFAULT '0',
			  `sort_order` int(3) NOT NULL DEFAULT '0',
			  `url` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
			  `icon` varchar(225) COLLATE utf8_unicode_ci DEFAULT NULL,
			  `label_color` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
			  `num_column` int(2) unsigned DEFAULT '1',
			  `icon_class` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
			  `icon_class_status` tinyint(1) NOT NULL DEFAULT '1',
			  `module_id` int(11) NOT NULL,
			  PRIMARY KEY (`menu_id`)
			) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=45 ;";
		$sql[] = "CREATE TABLE IF NOT EXISTS `".DB_PREFIX."megamenu_column` (
			`column_id` int(11) NOT NULL AUTO_INCREMENT,
			`row_id` int(11) NOT NULL,
			`sort_order` int(3) NOT NULL DEFAULT '0',
			`status` tinyint(1) NOT NULL,
			`num_column` int(2) unsigned NOT NULL DEFAULT '1',
			`type` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
			`params` text COLLATE utf8_unicode_ci,
			PRIMARY KEY (`column_id`,`row_id`)
			) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=31 ;";
		
		$sql[] = "CREATE TABLE IF NOT EXISTS `".DB_PREFIX."megamenu_description` (
			`menu_id` int(11) NOT NULL,
		  `language_id` int(11) NOT NULL,
		  `title` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
		  `label` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
		  PRIMARY KEY (`menu_id`,`language_id`)
		) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;";

		$sql[] = "CREATE TABLE IF NOT EXISTS `".DB_PREFIX."megamenu_row` (
			`row_id` int(11) NOT NULL AUTO_INCREMENT,
		  `menu_id` int(11) NOT NULL,
		  `sort_order` int(3) NOT NULL DEFAULT '0',
		  `status` tinyint(1) NOT NULL,
		  `height` decimal(10,0) DEFAULT NULL,
		  `bg_color` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
		  `bg_image` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
		  PRIMARY KEY (`row_id`,`menu_id`)
		) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=16 ;";
		
		$sql[] = "INSERT INTO `".DB_PREFIX."megamenu` (`menu_id`, `status`, `sort_order`, `url`, `icon`, `label_color`, `num_column`, `icon_class`, `icon_class_status`, `module_id`) VALUES
(24, 1, 1, 'index.php?route=common/home', '', '', 1, 'glyphicon glyphicon-star', 0, 80),
(25, 1, 4, 'index.php?route=bossblog/bossblog', '', '', 6, '', 0, 80),
(26, 1, 3, 'index.php?route=product/product&amp;product_id=42', '', '', 1, '', 0, 80),
(28, 1, 5, 'index.php?route=product/category&amp;path=20', '', '', 6, '', 0, 80),
(29, 1, 6, 'index.php?route=product/category&amp;path=20', '', '', 4, '', 0, 80),
(31, 1, 2, 'index.php?route=product/category&amp;path=20', '', '', 4, '', 0, 80);";
		$sql[] = "INSERT INTO `".DB_PREFIX."megamenu_row` (`row_id`, `menu_id`, `sort_order`, `status`, `height`, `bg_color`, `bg_image`) VALUES
(16, 31, 1, 1, 0, '', ''),
(17, 26, 0, 1, 0, '', ''),
(18, 27, 0, 1, 0, '', ''),
(19, 28, 0, 1, 0, '', 'catalog/bt_claudine/bkg_menu1.jpg'),
(20, 29, 0, 1, 0, '', ''),
(21, 30, 0, 1, 0, '', ''),
(22, 46, 1, 1, 0, '', ''),
(23, 24, 1, 1, 0, '', ''),
(24, 25, 1, 1, 0, '', '');";
		$param1 =  $this->db->escape('{"category_id":"20","product_img_w":"","product_img_h":"","manufacturer_img_w":"","manufacturer_img_h":"","manufacturer_name":"0","content_text":{"1":"\\t\\t\\t\\t\\t\\t","2":"\\t\\t\\t\\t\\t\\t"}}');
		$param2 =  $this->db->escape('{"category_id":"33","product_img_w":"200","product_img_h":"200","product_ids":["28","41","48","42"],"manufacturer_img_w":"","manufacturer_img_h":"","manufacturer_name":"0","content_text":{"1":"&lt;ul&gt;\\n\\n&lt;li&gt;&lt;a href=&quot;index.php?route=product\\/product&amp;amp;product_id=28&quot; title=&quot;Simple Product&quot;&gt;Simple Product&lt;\\/a&gt;&lt;\\/li&gt;\\n\\n&lt;li&gt;&lt;a href=&quot;index.php?route=product\\/product&amp;amp;product_id=41&quot; title=&quot;Discount Product&quot;&gt;Discount Product&lt;\\/a&gt;&lt;\\/li&gt;\\n\\n&lt;li&gt;&lt;a href=&quot;index.php?route=product\\/product&amp;amp;product_id=40&quot; title=&quot;Special Product&quot;&gt;Special Product&lt;\\/a&gt;&lt;\\/li&gt;\\n\\n&lt;li&gt;&lt;a href=&quot;index.php?route=product\\/product&amp;amp;product_id=43&quot; title=&quot;Reviews Product&quot;&gt;Reviews Product&lt;\\/a&gt;&lt;\\/li&gt;\\n\\n&lt;li&gt;&lt;a href=&quot;index.php?route=product\\/product&amp;amp;product_id=42&quot; title=&quot;Options Product&quot;&gt;Options Product&lt;\\/a&gt;&lt;\\/li&gt;\\n\\n&lt;\\/ul&gt;","2":"&lt;ul&gt;\\n\\n&lt;li&gt;&lt;a href=&quot;index.php?route=product\\/product&amp;amp;product_id=28&quot; title=&quot;Simple Product&quot;&gt;Simple Product&lt;\\/a&gt;&lt;\\/li&gt;\\n\\n&lt;li&gt;&lt;a href=&quot;index.php?route=product\\/product&amp;amp;product_id=41&quot; title=&quot;Discount Product&quot;&gt;Discount Product&lt;\\/a&gt;&lt;\\/li&gt;\\n\\n&lt;li&gt;&lt;a href=&quot;index.php?route=product\\/product&amp;amp;product_id=40&quot; title=&quot;Special Product&quot;&gt;Special Product&lt;\\/a&gt;&lt;\\/li&gt;\\n\\n&lt;li&gt;&lt;a href=&quot;index.php?route=product\\/product&amp;amp;product_id=43&quot; title=&quot;Reviews Product&quot;&gt;Reviews Product&lt;\\/a&gt;&lt;\\/li&gt;\\n\\n&lt;li&gt;&lt;a href=&quot;index.php?route=product\\/product&amp;amp;product_id=42&quot; title=&quot;Options Product&quot;&gt;Options Product&lt;\\/a&gt;&lt;\\/li&gt;\\n\\n&lt;\\/ul&gt;"},"sub_title":{"1":"","2":""}}');
		$param3 =  $this->db->escape('{"category_id":"24","product_img_w":"","product_img_h":"","manufacturer_img_w":"","manufacturer_img_h":"","manufacturer_name":"0","content_text":{"1":"","2":""}}');
		$param4 =  $this->db->escape('{"category_id":"0","product_img_w":"","product_img_h":"","manufacturer_img_w":"","manufacturer_img_h":"","manufacturer_name":"0","content_text":{"1":"&lt;img src=&quot;image\\/catalog\\/bt_claudine\\/featured1.png&quot; title=&quot;Layout Composer&quot; alt=&quot;Layout Composer&quot;&gt;\\n&lt;div class=&quot;item-detail&quot;&gt;\\n\\t&lt;ul&gt;\\n\\n\\n\\t\\t&lt;li&gt;Drag &amp;amp; Drop adding modules to layouts&lt;\\/li&gt;\\n\\n\\n\\t\\t&lt;li&gt;Drag &amp;amp; Drop sort modules in layouts&lt;\\/li&gt;\\n\\n\\n\\t\\t&lt;li&gt;Quick edit module&lt;\\/li&gt;\\n\\n\\n\\t\\t&lt;li&gt;Easy adding and configure new module&lt;\\/li&gt;\\n\\n\\n\\t\\t&lt;li&gt;Easy access to all Page layouts&lt;\\/li&gt;\\n\\n\\n\\n\\n\\t&lt;\\/ul&gt;\\n\\n\\n&lt;a target=&quot;_blank&quot; href=&quot;https:\\/\\/www.youtube.com\\/watch?v=JphBnmiBjOI&amp;amp;feature&quot; title=&quot;Watch Video&quot;&gt;Watch Video\\t\\t\\t\\t\\t\\t&lt;\\/a&gt;\\n&lt;\\/div&gt;","2":"&lt;img src=&quot;image\\/catalog\\/bt_claudine\\/featured1.png&quot; title=&quot;Layout Composer&quot; alt=&quot;Layout Composer&quot;&gt;\\n&lt;div class=&quot;item-detail&quot;&gt;\\n\\t&lt;ul&gt;\\n\\n\\t\\t&lt;li&gt;Drag &amp;amp; Drop adding modules to layouts&lt;\\/li&gt;\\n\\n\\t\\t&lt;li&gt;Drag &amp;amp; Drop sort modules in layouts&lt;\\/li&gt;\\n\\n\\t\\t&lt;li&gt;Quick edit module&lt;\\/li&gt;\\n\\n\\t\\t&lt;li&gt;Easy adding and configure new module&lt;\\/li&gt;\\n\\n\\t\\t&lt;li&gt;Easy access to all Page layouts&lt;\\/li&gt;\\n\\n&lt;li&gt;&lt;a target=&quot;_blank&quot; href=&quot;https:\\/\\/www.youtube.com\\/watch?v=JphBnmiBjOI&amp;amp;feature&quot; title=&quot;Watch Video&quot;&gt;Watch Video\\t\\t\\t\\t\\t\\t&lt;\\/a&gt;&lt;\\/li&gt;\\n\\n\\t&lt;\\/ul&gt;\\n\\n&lt;\\/div&gt;\\t\\t\\t\\t\\t\\t"},"sub_title":{"1":"LAYOUT COMPOSER","2":"LAYOUT COMPOSER"}}');
		$param5 =  $this->db->escape('{"category_id":"0","product_img_w":"","product_img_h":"","manufacturer_img_w":"150","manufacturer_img_h":"81","manufacturer_name":"0","manufacturer_id":["5","7","9","8","10","6"],"content_text":{"1":"\\t\\t\\t\\t\\t\\t","2":"\\t\\t\\t\\t\\t\\t"},"sub_title":{"1":"","2":""}}');
		$param6 =  $this->db->escape('{"category_id":"0","product_img_w":"170","product_img_h":"170","product_ids":["42"],"manufacturer_img_w":"","manufacturer_img_h":"","manufacturer_name":"0","content_text":{"1":"&lt;div class=&quot;html_column2&quot;&gt;\\n&lt;img src=&quot;http:\\/\\/demo.bossthemes.com\\/claudine_watches\\/image\\/catalog\\/bt_claudine\\/logo.png&quot; title=&quot;Logo&quot; alt=&quot;Logo&quot; style=&quot;margin-top:22px&quot;&gt;\\n&lt;p&gt;Metallic round sunglasses powder blue loafer Prada Saffiano button up print.  Floral bandeau envelope clutch dove grey Jil Sander Vasari dress. &lt;\\/p&gt;\\n\\n\\n\\n&lt;p&gt;Leggings Lanvin Jil Sander Vasari texture collarless grunge leather tote. La marini\\u00e8re Acne luxe dungaree knitwear.&lt;\\/p&gt;\\n\\n\\n\\n&lt;\\/div&gt;","2":"&lt;div class=&quot;html_column2&quot;&gt;\\n&lt;img src=&quot;http:\\/\\/demo.bossthemes.com\\/claudine_watches\\/image\\/catalog\\/bt_claudine\\/logo.png&quot; title=&quot;Logo&quot; alt=&quot;Logo&quot; style=&quot;margin-top:22px&quot;&gt;\\n&lt;p&gt;Metallic round sunglasses powder blue loafer Prada Saffiano button up print.  Floral bandeau envelope clutch dove grey Jil Sander Vasari dress. &lt;\\/p&gt;\\n\\n\\n\\n&lt;p&gt;Leggings Lanvin Jil Sander Vasari texture collarless grunge leather tote. La marini\\u00e8re Acne luxe dungaree knitwear.&lt;\\/p&gt;\\n\\n\\n\\n&lt;\\/div&gt;"},"sub_title":{"1":"","2":""}}');
		$param7 =  $this->db->escape('{"category_id":"0","product_img_w":"200","product_img_h":"200","product_ids":["33"],"manufacturer_img_w":"","manufacturer_img_h":"","manufacturer_name":"0","content_text":{"1":"\\t\\t\\t\\t\\t\\t\\t\\t\\t\\t\\t\\t\\t\\t\\t\\t\\t\\t\\t\\t\\t\\t\\t\\t\\t\\t\\t\\t\\t\\t\\t\\t\\t\\t\\t\\t","2":"\\t\\t\\t\\t\\t\\t\\t\\t\\t\\t\\t\\t\\t\\t\\t\\t\\t\\t\\t\\t\\t\\t\\t\\t\\t\\t\\t\\t\\t\\t\\t\\t\\t\\t\\t\\t"},"sub_title":{"1":"New Arrival","2":"New Arrival"}}');
		$param8 =  $this->db->escape('{"category_id":"25","product_img_w":"170","product_img_h":"170","product_ids":["44"],"manufacturer_img_w":"","manufacturer_img_h":"","manufacturer_name":"0","content_text":{"1":"\\t\\t\\t\\t\\t\\t\\t\\t\\t\\t\\t\\t\\t\\t\\t\\t\\t\\t","2":"\\t\\t\\t\\t\\t\\t\\t\\t\\t\\t\\t\\t\\t\\t\\t\\t\\t\\t"},"sub_title":{"1":"Category","2":"Category"}}');
		$param9 =  $this->db->escape('{"category_id":"0","product_img_w":"170","product_img_h":"170","product_ids":["29"],"manufacturer_img_w":"","manufacturer_img_h":"","manufacturer_name":"0","content_text":{"1":"&lt;div class=&quot;html_column1&quot;&gt;\\n&lt;ul&gt;\\n\\n&lt;li&gt;&lt;a href=&quot;#&quot; title=&quot;Product Details&quot;&gt;Product Details&lt;\\/a&gt;&lt;\\/li&gt;\\n\\n&lt;li&gt;&lt;a href=&quot;#&quot; title=&quot;Shopping Cart&quot;&gt;Shopping Cart&lt;\\/a&gt;&lt;\\/li&gt;\\n\\n&lt;li&gt;&lt;a href=&quot;#&quot; title=&quot;Checkout&quot;&gt;Checkout&lt;\\/a&gt;&lt;\\/li&gt;\\n\\n&lt;li&gt;&lt;a href=&quot;#&quot; title=&quot;Login \\/ Register&quot;&gt;Login \\/ Register&lt;\\/a&gt;&lt;\\/li&gt;\\n\\n&lt;li&gt;&lt;a href=&quot;#&quot; title=&quot;My Account&quot;&gt;My Account&lt;\\/a&gt;&lt;\\/li&gt;\\n\\n&lt;\\/ul&gt;\\n\\n&lt;\\/div&gt;","2":"&lt;div class=&quot;html_column1&quot;&gt;\\n&lt;ul&gt;\\n\\n&lt;li&gt;&lt;a href=&quot;#&quot; title=&quot;Product Details&quot;&gt;Product Details&lt;\\/a&gt;&lt;\\/li&gt;\\n\\n&lt;li&gt;&lt;a href=&quot;#&quot; title=&quot;Shopping Cart&quot;&gt;Shopping Cart&lt;\\/a&gt;&lt;\\/li&gt;\\n\\n&lt;li&gt;&lt;a href=&quot;#&quot; title=&quot;Checkout&quot;&gt;Checkout&lt;\\/a&gt;&lt;\\/li&gt;\\n\\n&lt;li&gt;&lt;a href=&quot;#&quot; title=&quot;Login \\/ Register&quot;&gt;Login \\/ Register&lt;\\/a&gt;&lt;\\/li&gt;\\n\\n&lt;li&gt;&lt;a href=&quot;#&quot; title=&quot;My Account&quot;&gt;My Account&lt;\\/a&gt;&lt;\\/li&gt;\\n\\n&lt;\\/ul&gt;\\n\\n&lt;\\/div&gt;"},"sub_title":{"1":"Pages","2":"Pages"}}');
		$param10 =  $this->db->escape('{"category_id":"0","product_img_w":"200","product_img_h":"200","product_ids":["42","40","36","48","29"],"manufacturer_img_w":"","manufacturer_img_h":"","manufacturer_name":"0","content_text":{"1":"\\t\\t\\t\\t\\t\\t\\t\\t\\t\\t\\t\\t\\t\\t\\t\\t\\t\\t","2":"\\t\\t\\t\\t\\t\\t\\t\\t\\t\\t\\t\\t\\t\\t\\t\\t\\t\\t"},"sub_title":{"1":"New Arrival","2":"New Arrival"}}');
		$param11 =  $this->db->escape('{"category_id":"0","product_img_w":"","product_img_h":"","manufacturer_img_w":"150","manufacturer_img_h":"81","manufacturer_name":"0","manufacturer_id":["5","7","9","8","10","6"],"content_text":{"1":"&lt;div class=&quot;html_column1&quot;&gt;\\n&lt;ul&gt;\\n\\n&lt;li&gt;&lt;a target=&quot;_blank&quot; title=&quot;Default Store&quot; href=&quot;http:\\/\\/demo.bossthemes.com\\/claudine\\/&quot;&gt;Default Store&lt;\\/a&gt;&lt;\\/li&gt;\\n\\n&lt;li&gt;&lt;a target=&quot;_blank&quot; title=&quot;Watches Store&quot; href=&quot;http:\\/\\/demo.bossthemes.com\\/claudine_watches\\/&quot;&gt;Watches Store&lt;\\/a&gt;&lt;\\/li&gt;\\n\\n&lt;\\/ul&gt;\\n\\n&lt;\\/div&gt;","2":"&lt;div class=&quot;html_column1&quot;&gt;\\n&lt;ul&gt;\\n\\n&lt;li&gt;&lt;a target=&quot;_blank&quot; title=&quot;Default Store&quot; href=&quot;http:\\/\\/demo.bossthemes.com\\/claudine\\/&quot;&gt;Default Store&lt;\\/a&gt;&lt;\\/li&gt;\\n\\n&lt;li&gt;&lt;a target=&quot;_blank&quot; title=&quot;Watches Store&quot; href=&quot;http:\\/\\/demo.bossthemes.com\\/claudine_watches\\/&quot;&gt;Watches Store&lt;\\/a&gt;&lt;\\/li&gt;\\n\\n&lt;\\/ul&gt;\\n\\n&lt;\\/div&gt;"},"sub_title":{"1":"","2":""}}');
		$param12 =  $this->db->escape('{"category_id":"0","product_img_w":"200","product_img_h":"200","product_ids":["42","41","48"],"manufacturer_img_w":"","manufacturer_img_h":"","manufacturer_name":"0","content_text":{"1":"\\t\\t\\t\\t\\t\\t\\t\\t\\t\\t\\t\\t\\t\\t\\t\\t\\t\\t\\t\\t\\t\\t\\t\\t\\t\\t\\t\\t\\t\\t\\t\\t\\t\\t\\t\\t","2":"\\t\\t\\t\\t\\t\\t\\t\\t\\t\\t\\t\\t\\t\\t\\t\\t\\t\\t\\t\\t\\t\\t\\t\\t\\t\\t\\t\\t\\t\\t\\t\\t\\t\\t\\t\\t"},"sub_title":{"1":"New Arrival","2":"New Arrival"}}');
		$param13 =  $this->db->escape('{"category_id":"0","product_img_w":"","product_img_h":"","manufacturer_img_w":"","manufacturer_img_h":"","manufacturer_name":"0","content_text":{"1":"&lt;iframe src=&quot;https:\\/\\/www.youtube.com\\/embed\\/DneXA_IhQHs&quot; allowfullscreen=&quot;true&quot; frameborder=&quot;0&quot;&gt;&lt;\\/iframe&gt;\\t\\t\\t\\t\\t\\t","2":"&lt;iframe src=&quot;https:\\/\\/www.youtube.com\\/embed\\/DneXA_IhQHs&quot; allowfullscreen=&quot;true&quot; frameborder=&quot;0&quot;&gt;&lt;\\/iframe&gt;\\t\\t\\t\\t\\t\\t"},"sub_title":{"1":"Video","2":"Video"}}');
		$param14 =  $this->db->escape('{"category_id":"59","product_img_w":"","product_img_h":"","manufacturer_img_w":"","manufacturer_img_h":"","manufacturer_name":"0","content_text":{"1":"\\t\\t\\t\\t\\t\\t\\t\\t\\t\\t\\t\\t\\t\\t\\t\\t\\t\\t","2":"\\t\\t\\t\\t\\t\\t\\t\\t\\t\\t\\t\\t\\t\\t\\t\\t\\t\\t"},"sub_title":{"1":"","2":""}}');
		$param15 =  $this->db->escape('{"category_id":"57","product_img_w":"","product_img_h":"","manufacturer_img_w":"","manufacturer_img_h":"","manufacturer_name":"0","content_text":{"1":"\\t\\t\\t\\t\\t\\t\\t\\t\\t\\t\\t\\t\\t\\t\\t\\t\\t\\t","2":"\\t\\t\\t\\t\\t\\t\\t\\t\\t\\t\\t\\t\\t\\t\\t\\t\\t\\t"},"sub_title":{"1":"","2":""}}');
		$param16 =  $this->db->escape('{"category_id":"60","product_img_w":"","product_img_h":"","manufacturer_img_w":"","manufacturer_img_h":"","manufacturer_name":"0","content_text":{"1":"\\t\\t\\t\\t\\t\\t\\t\\t\\t\\t\\t\\t\\t\\t\\t\\t\\t\\t","2":"\\t\\t\\t\\t\\t\\t\\t\\t\\t\\t\\t\\t\\t\\t\\t\\t\\t\\t"},"sub_title":{"1":"","2":""}}');
		$param17 =  $this->db->escape('{"category_id":"0","product_img_w":"","product_img_h":"","manufacturer_img_w":"","manufacturer_img_h":"","manufacturer_name":"0","content_text":{"1":"&lt;img src=&quot;image\\/catalog\\/bt_claudine\\/featured2.png&quot; title=&quot;Boss - Featured Product&quot; alt=&quot;Boss - Featured Product&quot;&gt;\\n&lt;div class=&quot;item-detail&quot;&gt;\\n\\t\\t\\t\\t\\t\\t\\t&lt;ul&gt;\\n\\n\\t\\t\\t\\t\\t\\t\\t\\t&lt;li&gt;Displaying Products in the frontend&lt;\\/li&gt;\\n\\n\\t\\t\\t\\t\\t\\t\\t\\t&lt;li&gt;Supporting jquery responsive slider&lt;\\/li&gt;\\n\\n\\t\\t\\t\\t\\t\\t\\t\\t&lt;li&gt;&lt;span style=&quot;font-size: 13.1999998092651px; line-height: 18.8571434020996px;&quot;&gt;Supporting&lt;\\/span&gt;: products\\/row, number rows, display pagination,  show product large, show additional image&lt;\\/li&gt;\\n\\n\\t\\t\\t\\t\\t\\t\\t\\t&lt;li&gt;\\tOver 50 variations&lt;\\/li&gt;\\n\\n\\t\\t\\t\\t\\t\\t\\t&lt;\\/ul&gt;\\n\\n\\t\\t\\t\\t\\t\\t&lt;\\/div&gt;","2":"&lt;img src=&quot;image\\/catalog\\/bt_claudine\\/featured2.png&quot; title=&quot;Boss - Featured Product&quot; alt=&quot;Boss - Featured Product&quot;&gt;\\n&lt;div class=&quot;item-detail&quot;&gt;\\n\\t\\t\\t\\t\\t\\t\\t&lt;ul&gt;\\n\\n\\t\\t\\t\\t\\t\\t\\t\\t&lt;li&gt;Displaying Products in the frontend&lt;\\/li&gt;\\n\\n\\t\\t\\t\\t\\t\\t\\t\\t&lt;li&gt;Supporting jquery responsive slider&lt;\\/li&gt;\\n\\n\\t\\t\\t\\t\\t\\t\\t\\t&lt;li&gt;&lt;span style=&quot;font-size: 13.1999998092651px; line-height: 18.8571434020996px;&quot;&gt;Supporting&lt;\\/span&gt;: products\\/row, number rows, display pagination,  show product large, show additional image&lt;\\/li&gt;\\n\\n\\t\\t\\t\\t\\t\\t\\t\\t&lt;li&gt;\\tOver 50 variations&lt;\\/li&gt;\\n\\n\\t\\t\\t\\t\\t\\t\\t&lt;\\/ul&gt;\\n\\n\\t\\t\\t\\t\\t\\t&lt;\\/div&gt;"},"sub_title":{"1":"Featured Product","2":"Featured Product"}}');
		$param18 =  $this->db->escape('{"category_id":"0","product_img_w":"","product_img_h":"","manufacturer_img_w":"","manufacturer_img_h":"","manufacturer_name":"0","content_text":{"1":"&lt;img src=&quot;image\\/catalog\\/bt_claudine\\/featured3.png&quot; title=&quot;Boss - Filter Product&quot; alt=&quot;Boss - Filter Product&quot;&gt;&lt;div class=&quot;item-detail&quot;&gt;\\t\\t\\t\\t\\t\\t\\t&lt;ul&gt;\\n\\t\\t\\t\\t\\t\\t\\t\\t&lt;li&gt;\\tDisplaying Products in the frontend.&lt;\\/li&gt;\\n\\t\\t\\t\\t\\t\\t\\t\\t&lt;li&gt;\\tSupporting jquery responsive slider and jquery tabs effect.&lt;\\/li&gt;\\n\\t\\t\\t\\t\\t\\t\\t\\t&lt;li&gt;&lt;span style=&quot;font-size: 13.1999998092651px; line-height: 18.8571434020996px;&quot;&gt;Supporting&amp;nbsp;&lt;\\/span&gt;: products\\/row, number rows.&lt;\\/li&gt;\\n\\t\\t\\t\\t\\t\\t\\t\\t&lt;li&gt;\\tOver 20 variations&lt;\\/li&gt;\\n\\t\\t\\t\\t\\t\\t\\t&lt;\\/ul&gt;\\n\\t\\t\\t\\t\\t\\t&lt;\\/div&gt;","2":"&lt;img src=&quot;image\\/catalog\\/bt_claudine\\/featured3.png&quot; title=&quot;Boss - Filter Product&quot; alt=&quot;Boss - Filter Product&quot;&gt;&lt;div class=&quot;item-detail&quot;&gt;\\t\\t\\t\\t\\t\\t\\t&lt;ul&gt;\\n\\t\\t\\t\\t\\t\\t\\t\\t&lt;li&gt;\\tDisplaying Products in the frontend.&lt;\\/li&gt;\\n\\t\\t\\t\\t\\t\\t\\t\\t&lt;li&gt;\\tSupporting jquery responsive slider and jquery tabs effect.&lt;\\/li&gt;\\n\\t\\t\\t\\t\\t\\t\\t\\t&lt;li&gt;&lt;span style=&quot;font-size: 13.1999998092651px; line-height: 18.8571434020996px;&quot;&gt;Supporting&amp;nbsp;&lt;\\/span&gt;: products\\/row, number rows.&lt;\\/li&gt;\\n\\t\\t\\t\\t\\t\\t\\t\\t&lt;li&gt;\\tOver 20 variations&lt;\\/li&gt;\\n\\t\\t\\t\\t\\t\\t\\t&lt;\\/ul&gt;\\n\\t\\t\\t\\t\\t\\t&lt;\\/div&gt;"},"sub_title":{"1":"Filter Product","2":"Filter Product"}}');
		$param19 =  $this->db->escape('{"category_id":"0","product_img_w":"","product_img_h":"","manufacturer_img_w":"","manufacturer_img_h":"","manufacturer_name":"0","content_text":{"1":"&lt;img src=&quot;image\\/catalog\\/bt_claudine\\/featured4.png&quot; title=&quot;Boss - MenuCategory&quot; alt=&quot;Boss - MenuCategory&quot;&gt;&lt;div class=&quot;item-detail&quot;&gt;\\t\\t\\t\\t\\t\\t\\t&lt;ul&gt;\\n\\t\\t\\t\\t\\t\\t\\t\\t&lt;li&gt;\\tDisplaying categories in the frontend&lt;\\/li&gt;\\n\\t\\t\\t\\t\\t\\t\\t\\t&lt;li&gt;\\tSupporting responsive&lt;\\/li&gt;\\n\\t\\t\\t\\t\\t\\t\\t\\t&lt;li&gt;\\tStrong configuration in admin: title, Icon, background of dropdown, width of dropdown.&lt;\\/li&gt;\\n\\t\\t\\t\\t\\t\\t\\t&lt;\\/ul&gt;\\n\\t\\t\\t\\t\\t\\t&lt;\\/div&gt;","2":"&lt;img src=&quot;image\\/catalog\\/bt_claudine\\/featured4.png&quot; title=&quot;Boss - MenuCategory&quot; alt=&quot;Boss - MenuCategory&quot;&gt;&lt;div class=&quot;item-detail&quot;&gt;\\t\\t\\t\\t\\t\\t\\t&lt;ul&gt;\\n\\t\\t\\t\\t\\t\\t\\t\\t&lt;li&gt;\\tDisplaying categories in the frontend&lt;\\/li&gt;\\n\\t\\t\\t\\t\\t\\t\\t\\t&lt;li&gt;\\tSupporting responsive&lt;\\/li&gt;\\n\\t\\t\\t\\t\\t\\t\\t\\t&lt;li&gt;\\tStrong configuration in admin: title, Icon, background of dropdown, width of dropdown.&lt;\\/li&gt;\\n\\t\\t\\t\\t\\t\\t\\t&lt;\\/ul&gt;\\n\\t\\t\\t\\t\\t\\t&lt;\\/div&gt;"},"sub_title":{"1":"MenuCategory","2":"MenuCategory"}}');
		$sql[] = "INSERT INTO `".DB_PREFIX."megamenu_column` (`column_id`, `row_id`, `sort_order`, `status`, `num_column`, `type`, `params`) VALUES
(31, 16, 0, 1, 1, 'category', '".$param1."'),
(33, 17, 1, 1, 1, 'html', '".$param2."'),
(34, 18, 1, 1, 1, 'category', '".$param3."'),
(36, 20, 1, 1, 1, 'html', '".$param4."'),
(41, 21, 1, 1, 6, 'manufacturer', '".$param5."'),
(42, 19, 1, 1, 2, 'html', '".$param6."'),
(43, 19, 2, 1, 1, 'product', '".$param7."'),
(44, 19, 3, 1, 1, 'category', '".$param8."'),
(45, 19, 4, 1, 1, 'html', '".$param9."'),
(46, 22, 1, 1, 5, 'product', '".$param10."'),
(47, 23, 1, 1, 1, 'html', '".$param11."'),
(48, 24, 0, 1, 3, 'product', '".$param12."'),
(50, 24, 0, 1, 3, 'html', '".$param13."'),
(51, 16, 2, 1, 1, 'category', '".$param14."'),
(52, 16, 3, 1, 1, 'category', '".$param15."'),
(53, 16, 4, 1, 1, 'category', '".$param16."'),
(54, 20, 2, 1, 1, 'html', '".$param17."'),
(55, 20, 3, 1, 1, 'html', '".$param18."'),
(56, 20, 4, 1, 1, 'html', '".$param19."');";		
		$sql[] = "INSERT INTO `".DB_PREFIX."megamenu_description` (`menu_id`, `language_id`, `title`, `label`) VALUES
(24, 2, 'Home', ''),
(25, 1, 'Blog', ''),
(26, 1, 'Products', ''),
(24, 1, 'Home', ''),
(28, 2, 'Pages', ''),
(29, 2, 'Features', ''),
(29, 1, 'Features', ''),
(31, 1, 'Categories', ''),
(28, 1, 'Pages', ''),
(25, 2, 'Blog', ''),
(31, 2, 'Categories', ''),
(26, 2, 'Products', '');";
		
		foreach( $sql as $q ){
			$query = $this->db->query($q);
		}
	}
	public function addMenu($module_id,$data){
		
		$this->db->query("INSERT INTO " . DB_PREFIX . "megamenu SET status = '" . (int)$data['status'] . "', sort_order = '" . (int)$data['sort_order'] . "',num_column = '" . (int)$data['num_column'] . "', url = '" . $this->db->escape($data['url']) . "',icon = '" . $this->db->escape($data['icon']) . "',label_color = '" . $this->db->escape($data['label_color']) . "', icon_class_status = '" . (int)$data['icon_class_status'] . "', icon_class = '" . $this->db->escape($data['icon_class']) . "',module_id = '" . (int)$module_id . "'");
		
		$menu_id = $this->db->getLastId();

		foreach ($data['menudes'] as $language_id => $value) {
			$this->db->query("INSERT INTO " . DB_PREFIX . "megamenu_description SET menu_id = '" . (int)$menu_id . "', language_id = '" . (int)$language_id . "', title = '" . $this->db->escape($value['title']) . "',label = '" . $this->db->escape($value['label']) . "'");
		}
	}
	
	public function addMenuRow($menu_id,$data){
		
		$this->db->query("INSERT INTO " . DB_PREFIX . "megamenu_row SET menu_id = '" . (int)$menu_id . "', status = '" . (int)$data['status'] . "', sort_order = '" . (int)$data['sort_order'] . "', height = '" . (float)$data['height'] . "', bg_color = '" . $this->db->escape($data['bg_color']) . "', bg_image = '" . $this->db->escape($data['bg_image']) . "'");
	}
	
	public function addMenuColumn($row_id,$data){
		
		$this->db->query("INSERT INTO " . DB_PREFIX . "megamenu_column SET row_id = '" . (int)$row_id . "', status = '" . (int)$data['status'] . "', sort_order = '" . (int)$data['sort_order'] . "', num_column = '" . (int)$data['num_column'] . "', type = '" . $this->db->escape($data['type']) . "', params = '" . $this->db->escape(json_encode($data['params'])) . "'");
	}
	
	public function editMenu($menu_id,$data) {
		$this->db->query("UPDATE " . DB_PREFIX . "megamenu SET status = '" . (int)$data['status'] . "', sort_order = '" . (int)$data['sort_order'] . "',url = '" . $this->db->escape($data['url']) . "', icon_class_status = '" . (int)$data['icon_class_status'] . "', icon_class = '" . $this->db->escape($data['icon_class']) . "', num_column = '" . (int)$data['num_column'] . "', icon = '" . $this->db->escape($data['icon']) . "',label_color = '" . $this->db->escape($data['label_color']) . "' WHERE menu_id = '" . (int)$menu_id . "'");
		
		$this->db->query("DELETE FROM " . DB_PREFIX . "megamenu_description WHERE menu_id = '" . (int)$menu_id . "'");

		foreach ($data['menudes'] as $language_id => $value) {
			$this->db->query("INSERT INTO " . DB_PREFIX . "megamenu_description SET menu_id = '" . (int)$menu_id . "', language_id = '" . (int)$language_id . "', title = '" . $this->db->escape($value['title']) . "',label = '" . $this->db->escape($value['label']) . "'");
		}
	}
	
	public function editMenuRow($row_id,$data) {
		$this->db->query("UPDATE " . DB_PREFIX . "megamenu_row SET menu_id = '" . (int)$data['menu_id'] . "', status = '" . (int)$data['status'] . "', sort_order = '" . (int)$data['sort_order'] . "', height = '" . (float)$data['height'] . "', bg_color = '" . $this->db->escape($data['bg_color']) . "', bg_image = '" . $this->db->escape($data['bg_image']) . "' WHERE row_id = '" . (int)$row_id . "'");
	}
	
	public function editMenuColumn($column_id,$data) {
		$this->db->query("UPDATE " . DB_PREFIX . "megamenu_column SET status = '" . (int)$data['status'] . "', sort_order = '" . (int)$data['sort_order'] . "', num_column = '" . (int)$data['num_column'] . "', type = '" . $this->db->escape($data['type']) . "', params = '" . $this->db->escape(json_encode($data['params'])) . "' WHERE column_id = '" . (int)$column_id . "'");
	}
	
	public function deleteMenu($menu_id) {
		$this->db->query("DELETE FROM " . DB_PREFIX . "megamenu WHERE menu_id = '" . (int)$menu_id . "'");
		$this->db->query("DELETE FROM " . DB_PREFIX . "megamenu_description WHERE menu_id = '" . (int)$menu_id . "'");
	}
	
	public function deleteMenuRow($row_id) {
		$this->db->query("DELETE FROM " . DB_PREFIX . "megamenu_row WHERE row_id = '" . (int)$row_id . "'");
	}
	public function deleteMenuColumn($column_id) {
		$this->db->query("DELETE FROM " . DB_PREFIX . "megamenu_column WHERE column_id = '" . (int)$column_id . "'");
	}
	
	public function getMenus(){
		$sql = "SELECT * FROM " . DB_PREFIX . "megamenu m LEFT JOIN " . DB_PREFIX . "megamenu_description md ON (m.menu_id = md.menu_id)";
		
		$sql .= " WHERE md.language_id = '" . (int)$this->config->get('config_language_id') . "'";
		
		$sql .= " GROUP BY m.menu_id";
		
		$sql .= " ORDER BY m.sort_order";
		
		$query = $this->db->query($sql);

		return $query->rows;
		
	}
	public function getMenusByModuleId($module_id){
		$sql = "SELECT * FROM " . DB_PREFIX . "megamenu m LEFT JOIN " . DB_PREFIX . "megamenu_description md ON (m.menu_id = md.menu_id)";
		
		$sql .= " WHERE m.module_id = '" . (int)$module_id . "' and md.language_id = '" . (int)$this->config->get('config_language_id') . "'";
		
		$sql .= " GROUP BY m.menu_id";
		
		$sql .= " ORDER BY m.sort_order";
		
		$query = $this->db->query($sql);

		return $query->rows;
		
	}
	
	public function getMenuRows($menu_id){
		$sql = "SELECT * FROM " . DB_PREFIX . "megamenu_row mr WHERE mr.menu_id = '" . (int)$menu_id . "'";
		
		$sql .= " ORDER BY mr.sort_order";
		
		$query = $this->db->query($sql);

		return $query->rows;
		
	}
	
	public function getMenuColumns($row_id){
		$sql = "SELECT * FROM " . DB_PREFIX . "megamenu_column mc WHERE mc.row_id = '" . (int)$row_id . "'";
		
		$sql .= " ORDER BY mc.sort_order";
		
		$query = $this->db->query($sql);

		return $query->rows;
		
	}
	
	public function getMenu($menu_id){
		$query = $this->db->query("SELECT DISTINCT * FROM " . DB_PREFIX . "megamenu m LEFT JOIN " . DB_PREFIX . "megamenu_description md ON (m.menu_id = md.menu_id) WHERE m.menu_id = '" . $menu_id . "' AND md.language_id = '" . (int)$this->config->get('config_language_id') . "'");

		return $query->row;
		
	}
	
	public function getMenuRow($row_id){
		$query = $this->db->query("SELECT DISTINCT * FROM " . DB_PREFIX . "megamenu_row mr WHERE mr.row_id = '" . (int)$row_id . "'");

		return $query->row;
		
	}
	
	public function getMenuColumn($column_id){
		$query = $this->db->query("SELECT DISTINCT * FROM " . DB_PREFIX . "megamenu_column mc WHERE mc.column_id = '" . (int)$column_id . "'");

		return $query->row;
		
	}
	
	public function getMenuDescription($menu_id){
		$query = $this->db->query("SELECT DISTINCT * FROM " . DB_PREFIX . "megamenu_description md WHERE md.menu_id = '" . $menu_id . "'");

		foreach ($query->rows as $result) {
			$menu_description_data[$result['language_id']] = array(
				'title'             => $result['title'],
				'label'             => $result['label']
			);
		}

		return $menu_description_data;
		
	}
	
	public function updateSortMenu($data){
		$count = 1;
		foreach ($data as $menu_id) {
			$query = "UPDATE " . DB_PREFIX . "megamenu SET sort_order = " . $count . " WHERE menu_id = " . $menu_id;
			$this->db->query($query);
			$count ++;	
		}
	}
	public function updateSortRow($menu_id,$data){
		$count = 1;
		foreach ($data as $row_id) {
			$this->db->query("UPDATE " . DB_PREFIX . "megamenu_row SET sort_order = " . $count . " WHERE menu_id = " . $menu_id." AND row_id = ".$row_id);
			$count ++;	
		}
	}
	public function updateSortColumn($row_id,$data){
		$count = 1;
		foreach ($data as $column_id) {
			$this->db->query("UPDATE " . DB_PREFIX . "megamenu_column SET sort_order = " . $count . " WHERE row_id = " . $row_id." AND column_id = ".$column_id);
			$count ++;	
		}
	}
	
	public function install(){
		$sql = " SHOW TABLES LIKE '".DB_PREFIX."megamenu'";
		$query = $this->db->query( $sql );
		if( count($query->rows) > 0 ){
			
			$sql="delete from `".DB_PREFIX."megamenu`";
			$query = $this->db->query( $sql );
			$sql = array();			
			$sql[]  = "CREATE TABLE IF NOT EXISTS `".DB_PREFIX."megamenu` (
			  `menu_id` int(11) NOT NULL AUTO_INCREMENT,
			  `status` tinyint(1) NOT NULL DEFAULT '0',
			  `sort_order` int(3) NOT NULL DEFAULT '0',
			  `url` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
			  `icon` varchar(225) COLLATE utf8_unicode_ci DEFAULT NULL,
			  `label_color` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
			  `num_column` int(2) unsigned DEFAULT '1',
			  `icon_class` varchar(255) CHARACTER SET utf8_unicode_ci DEFAULT NULL,
			  `icon_class_status` tinyint(1) NOT NULL DEFAULT '1',
			  `module_id` int(11) NOT NULL,
			  PRIMARY KEY (`menu_id`)
			) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=45 ;";
			$sql[] = "CREATECREATE TABLE IF NOT EXISTS `".DB_PREFIX."megamenu_description` (
			  `menu_id` int(11) NOT NULL,
			  `language_id` int(11) NOT NULL,
			  `title` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
			  `label` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
			  PRIMARY KEY (`menu_id`,`language_id`)
			) ENGINE=MyISAM DEFAULT CHARSET=utf16 COLLATE=utf16_unicode_ci;";
			$sql[] = "CREATE TABLE IF NOT EXISTS `".DB_PREFIX."megamenu_row` (
				  `row_id` int(11) NOT NULL AUTO_INCREMENT,
				  `menu_id` int(11) NOT NULL,
				  `sort_order` int(3) NOT NULL DEFAULT '0',
				  `status` tinyint(1) NOT NULL,
				  `height` decimal(10,0) DEFAULT NULL,
				  `bg_color` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
				  `bg_image` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
				  PRIMARY KEY (`row_id`,`menu_id`)
				) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=18 ;";
			$sql[] = "CREATE TABLE IF NOT EXISTS `".DB_PREFIX."megamenu_column` (
				  `column_id` int(11) NOT NULL AUTO_INCREMENT,
				  `row_id` int(11) NOT NULL,
				  `sort_order` int(3) NOT NULL DEFAULT '0',
				  `status` tinyint(1) NOT NULL,
				  `num_column` int(2) unsigned NOT NULL DEFAULT '1',
				  `type` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
				  `params` text COLLATE utf8_unicode_ci,
				  PRIMARY KEY (`column_id`,`row_id`)
				) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=32 ;";
			foreach( $sql as $q ){
				$query = $this->db->query( $q );
			}
		}else{ 
			$sql = array();
			$sql[]  = "CREATE TABLE IF NOT EXISTS `".DB_PREFIX."megamenu` (
			  `menu_id` int(11) NOT NULL AUTO_INCREMENT,
			  `status` tinyint(1) NOT NULL DEFAULT '0',
			  `sort_order` int(3) NOT NULL DEFAULT '0',
			  `url` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
			  `icon` varchar(225) COLLATE utf8_unicode_ci DEFAULT NULL,
			  `label_color` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
			  `num_column` int(2) unsigned DEFAULT '1',
			  `icon_class` varchar(255) CHARACTER SET utf32 COLLATE utf32_unicode_ci DEFAULT NULL,
			  `icon_class_status` tinyint(1) NOT NULL DEFAULT '1',
			  `module_id` int(11) NOT NULL,
			  PRIMARY KEY (`menu_id`)
			) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=45 ;";
			$sql[] = "CREATECREATE TABLE IF NOT EXISTS `".DB_PREFIX."megamenu_description` (
			  `menu_id` int(11) NOT NULL,
			  `language_id` int(11) NOT NULL,
			  `title` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
			  `label` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
			  PRIMARY KEY (`menu_id`,`language_id`)
			) ENGINE=MyISAM DEFAULT CHARSET=utf16 COLLATE=utf16_unicode_ci;";
			$sql[] = "CREATE TABLE IF NOT EXISTS `".DB_PREFIX."megamenu_row` (
				  `row_id` int(11) NOT NULL AUTO_INCREMENT,
				  `menu_id` int(11) NOT NULL,
				  `sort_order` int(3) NOT NULL DEFAULT '0',
				  `status` tinyint(1) NOT NULL,
				  `height` decimal(10,0) DEFAULT NULL,
				  `bg_color` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
				  `bg_image` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
				  PRIMARY KEY (`row_id`,`menu_id`)
				) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=18 ;";
			$sql[] = "CREATE TABLE IF NOT EXISTS `".DB_PREFIX."megamenu_column` (
				  `column_id` int(11) NOT NULL AUTO_INCREMENT,
				  `row_id` int(11) NOT NULL,
				  `sort_order` int(3) NOT NULL DEFAULT '0',
				  `status` tinyint(1) NOT NULL,
				  `num_column` int(2) unsigned NOT NULL DEFAULT '1',
				  `type` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
				  `params` text COLLATE utf8_unicode_ci,
				  PRIMARY KEY (`column_id`,`row_id`)
				) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=32 ;";

			foreach( $sql as $q ){
				$query = $this->db->query( $q );
			}
		}	
	}
}

?>