<?php
class ModelBossthemesBossRevolutionSlider extends Model { 

	public function createdb(){
	
		$sql = " SHOW TABLES LIKE '".DB_PREFIX."btslider'";
		$query = $this->db->query( $sql );
		if( count($query->rows) > 0 ){
			
			$sql="DELETE FROM `".DB_PREFIX."btslider`";
			$query = $this->db->query( $sql );
		}
		
		$sql = " SHOW TABLES LIKE '".DB_PREFIX."btslider_slide'";
		$query = $this->db->query( $sql );
		if( count($query->rows) > 0 ){
			
			$sql="DELETE FROM `".DB_PREFIX."btslider_slide`";
			$query = $this->db->query( $sql );
		}
		
		$sql = array();
		$sql[]  = "CREATE TABLE IF NOT EXISTS `".DB_PREFIX."btslider` (
		  `id` int(11) NOT NULL AUTO_INCREMENT,
		  `setting` text COLLATE utf8_unicode_ci,
		  PRIMARY KEY (`id`)
		) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=24 ;";
		
		$sql[] = "CREATE TABLE IF NOT EXISTS `".DB_PREFIX."btslider_slide` (
			`id` int(11) NOT NULL AUTO_INCREMENT,
			`slider_id` int(11) NOT NULL,
			`status` tinyint(4) NOT NULL DEFAULT '0',
			`sort_order` int(3) NOT NULL DEFAULT '0',
			`slideset` text COLLATE utf8_unicode_ci,
			`caption` text COLLATE utf8_unicode_ci,
			PRIMARY KEY (`id`)
		) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=17 ;";
		
		$setting =  $this->db->escape('{"slider_type":"custom","slider_width":"878","slider_height":"540","delay":"5000","startWithSlide":"0","stopslider":"off","stopafterloops":"-1","stopatslide":"-1","touchenabled":"on","onhoverstop":"on","timeline":"on","timerlineposition":"top","shadow":"0","navigationtype":"bullet","navigationarrow":"solo","navigationstyle":"preview2","navigationhalign":"center","navigationvalign":"bottom","navigationhoffset":"0","navigationvoffset":"15","soloarrowlefthalign":"left","soloarrowleftvalign":"center","soloarrowlefthoffset":"20","soloarrowleftvoffset":"0","soloarrowrighthalign":"right","soloarrowrightvalign":"center","soloarrowrighthoffset":"20","soloarrowrightvoffset":"0","timehidethumbnail":"200","thumbnailwidth":"100","thumbnailheight":"50","thumbamount":"5","hidecapptionatlimit":"2","hideallcapptionatlimit":"2","hideslideratlimit":"2"}');
		
		$slideset1 =  $this->db->escape('{"url":"#","enablelink":"1","type_background":"image_bg","background":"catalog/banner/slider1.jpg","transitions":"slotslide-horizontal","slotamount":"11","masterspeed":"500","delay":"5000","target":"_blank","kenburns":"off","enablefullvideo":"1"}');
		$caption1 =  $this->db->escape('[{"text_caption":{"1":"luxury timeless","2":"luxury timeless"},"datax":"170","type_caption":"text","datay":"178","class_css":"big_white","dataspeed":"500","datastart":"1000","dataend":"5000","dataafterspeed":"300","incom_animation":"lft","outgo_animation":"ltb","easing":"easeOutCirc","endeasing":"easeInOutBounce"},{"image_caption":"catalog/bt_claudine/text_slider1.png","datax":"250","type_caption":"image","datay":"105","class_css":"big_white","dataspeed":"500","datastart":"800","dataend":"5000","dataafterspeed":"300","incom_animation":"randomrotate","outgo_animation":"randomrotateout","easing":"easeInExpo","endeasing":"easeInQuint"},{"text_caption":{"1":"Print crop midi collarless dress sneaker tortoise-shell sunglasses slip dress. ","2":"Print crop midi collarless dress sneaker tortoise-shell sunglasses slip dress. "},"datax":"200","type_caption":"text","datay":"270","class_css":"medium_white","dataspeed":"500","datastart":"1500","dataend":"5000","dataafterspeed":"300","incom_animation":"sft","outgo_animation":"stt","easing":"easeOutBack","endeasing":"easeOutBack"},{"text_caption":{"1":"Navy blue bandeau la mariniere Paris Hermes chambray plaited","2":"Navy blue bandeau la mariniere Paris Hermes chambray plaited"},"datax":"242","type_caption":"text","datay":"300","class_css":"medium_white","dataspeed":"500","datastart":"1800","dataend":"5000","dataafterspeed":"300","incom_animation":"sft","outgo_animation":"stt","easing":"easeOutBack","endeasing":"easeOutBack"},{"text_caption":{"1":"&lt;a class=&quot;btn&quot;&gt;PURCHASE THIS THEME&lt;/a&gt;","2":"&lt;a class=&quot;btn&quot;&gt;PURCHASE THIS THEME&lt;/a&gt;"},"datax":"333","type_caption":"text","datay":"354","class_css":"medium_text","dataspeed":"500","datastart":"1500","dataend":"5000","dataafterspeed":"500","incom_animation":"sft","outgo_animation":"stt","easing":"easeOutBack","endeasing":"easeOutBack"}]');
		$slideset2 =  $this->db->escape('{"url":"#","enablelink":"1","type_background":"image_bg","background":"catalog/banner/slider2.jpg","transitions":"curtain-3","slotamount":"10","masterspeed":"500","delay":"5000","target":"_blank","kenburns":"off","enablefullvideo":"0"}');
		$caption2 =  $this->db->escape('[{"text_caption":{"1":"time to live","2":"time to live"},"datax":"100","type_caption":"text","datay":"180","class_css":"big_white","dataspeed":"500","datastart":"1200","dataend":"5000","dataafterspeed":"300","incom_animation":"lfr","outgo_animation":"ltr","easing":"easeOutBack","endeasing":"easeOutBack"},{"text_caption":{"1":"Print crop midi collarless dress sneaker tortoise-shell sunglasses slip dress. ","2":"Print crop midi collarless dress sneaker tortoise-shell sunglasses slip dress. "},"datax":"80","type_caption":"text","datay":"280","class_css":"medium_white","dataspeed":"500","datastart":"1500","dataend":"5000","dataafterspeed":"300","incom_animation":"lfl","outgo_animation":"ltl","easing":"easeOutBack","endeasing":"easeOutBack"},{"text_caption":{"1":"&lt;a class=&quot;btn&quot;&gt;PURCHASE THIS THEME&lt;/a&gt;","2":"&lt;a class=&quot;btn&quot;&gt;PURCHASE THIS THEME&lt;/a&gt;"},"datax":"180","type_caption":"text","datay":"380","class_css":"medium_text","dataspeed":"500","datastart":"2000","dataend":"5000","dataafterspeed":"500","incom_animation":"randomrotate","outgo_animation":"stt","easing":"easeOutBack","endeasing":"easeOutBack"},{"text_caption":{"1":"Navy blue bandeau la mariniere Paris Hermes chambray plaited","2":"Navy blue bandeau la mariniere Paris Hermes chambray plaited"},"datax":"100","type_caption":"text","datay":"310","class_css":"medium_white","dataspeed":"500","datastart":"1800","dataend":"5000","dataafterspeed":"300","incom_animation":"lfr","outgo_animation":"ltr","easing":"easeOutBack","endeasing":"easeOutBack"},{"image_caption":"catalog/bt_claudine/text_slider2.png","datax":"100","type_caption":"image","datay":"110","class_css":"big_white","dataspeed":"500","datastart":"800","dataend":"5000","dataafterspeed":"300","incom_animation":"randomrotate","outgo_animation":"ltl","easing":"easeOutBack","endeasing":"easeOutBack"}]');
		$slideset3 =  $this->db->escape('{"url":"#","enablelink":"1","type_background":"image_bg","background":"catalog/banner/slider3.jpg","transitions":"curtain-2","slotamount":"7","masterspeed":"500","delay":"5000","target":"_blank","kenburns":"off","enablefullvideo":"0"}');
		$caption3 =  $this->db->escape('[{"text_caption":{"1":"mens watches","2":"mens watches"},"datax":"80","type_caption":"text","datay":"180","class_css":"big_white","dataspeed":"500","datastart":"1200","dataend":"5000","dataafterspeed":"300","incom_animation":"fade","outgo_animation":"ltr","easing":"easeOutBack","endeasing":"easeOutBack"},{"text_caption":{"1":"Print crop midi collarless dress sneaker tortoise-shell sunglasses slip dress. ","2":"Print crop midi collarless dress sneaker tortoise-shell sunglasses slip dress. "},"datax":"80","type_caption":"text","datay":"280","class_css":"medium_white","dataspeed":"500","datastart":"1500","dataend":"5000","dataafterspeed":"300","incom_animation":"lfl","outgo_animation":"ltl","easing":"easeOutBack","endeasing":"easeOutBack"},{"text_caption":{"1":"Navy blue bandeau la mariniere Paris Hermes chambray plaited","2":"Navy blue bandeau la mariniere Paris Hermes chambray plaited"},"datax":"110","type_caption":"text","datay":"310","class_css":"medium_white","dataspeed":"300","datastart":"1700","dataend":"5000","dataafterspeed":"500","incom_animation":"lfr","outgo_animation":"ltr","easing":"easeOutBack","endeasing":"easeOutBack"},{"image_caption":"catalog/bt_claudine/text_slider3.png","datax":"180","type_caption":"image","datay":"100","class_css":"big_white","dataspeed":"300","datastart":"800","dataend":"5000","dataafterspeed":"300","incom_animation":"randomrotate","outgo_animation":"ltr","easing":"easeOutBack","endeasing":"easeOutBack"},{"text_caption":{"1":"&lt;a class=&quot;btn&quot;&gt;PURCHASE THIS THEME&lt;/a&gt;","2":"&lt;a class=&quot;btn&quot;&gt;PURCHASE THIS THEME&lt;/a&gt;"},"datax":"200","type_caption":"text","datay":"380","class_css":"medium_text","dataspeed":"500","datastart":"1500","dataend":"5000","dataafterspeed":"500","incom_animation":"sft","outgo_animation":"stt","easing":"easeOutBack","endeasing":"easeOutBack"}]');
		$sql[] = "INSERT INTO `".DB_PREFIX."btslider` (`id`, `setting`) VALUES
(1, '".$setting."');";
		$sql[] = "INSERT INTO `".DB_PREFIX."btslider_slide` (`id`, `slider_id`, `status`, `sort_order`, `slideset`, `caption`) VALUES
(13, 1, 1, 1, '".$slideset1."', '".$caption1."'),
(16, 1, 1, 2, '".$slideset2."', '".$caption2."'),
(17, 1, 1, 3, '".$slideset3."', '".$caption3."');";
		foreach( $sql as $q ){
			$query = $this->db->query( $q );
		}
	}

	public function addSlide($data){
		
		$this->db->query("INSERT INTO " . DB_PREFIX . "btslider_slide SET slider_id = '" . (int)$data['slider_id'] . "', status = '" . (int)$data['status'] . "',slideset = '" . $data['slideset'] . "',caption = '" . $data['caption'] . "', sort_order = '" . (int)$data['sort_order'] . "'");
	}
	
	public function addSlide_New($slider_id,$data){
		
		$this->db->query("INSERT INTO " . DB_PREFIX . "btslider_slide SET slider_id = '" . (int)$slider_id . "', status = '" . (int)$data['status'] . "',slideset = '" . json_encode($data['slideset']) . "',caption = '" . $this->db->escape(json_encode($data['caption'])) . "', sort_order = '" . (int)$data['sort_order'] . "'");
	}
	
	public function editSlide($slide_id,$slider_id,$data){
		$this->db->query("UPDATE " . DB_PREFIX . "btslider_slide SET slider_id = '" . (int)$slider_id . "', slideset = '" . json_encode($data['slideset']) . "',caption = '" . $this->db->escape(json_encode($data['caption'])) . "', status = '" . (int)$data['status'] . "', sort_order = '" . (int)$data['sort_order'] . "' WHERE id = '" . (int)$slide_id . "'");
	}
	
	public function addSlider($data){
		
		$this->db->query("INSERT INTO " . DB_PREFIX . "btslider SET setting = '" . json_encode($data['setting']) . "'");
		
		$slider_id = $this->db->getLastId();
		
		return $slider_id;
	}
	
	public function getLastId(){
		$sql = "SELECT * FROM " . DB_PREFIX . "btslider s";
		
		$query = $this->db->query($sql);
		
		$slider_id = $this->db->getLastId();
		
		return $slider_id;
	}
	
	public function editSlider($slider_id,$data){
		
		$this->db->query("UPDATE " . DB_PREFIX . "btslider SET setting = '" . json_encode($data['setting']) . "' WHERE id = '" . (int)$slider_id . "'");
	}
	
	public function getModules($group, $store_id = 0){
		$data = array(); 
		
		$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "setting WHERE store_id = '" . (int)$store_id . "' AND `group` = '" . $this->db->escape($group) . "'");
		
		foreach ($query->rows as $result) {
			if (!$result['serialized']) {
				$data[$result['key']] = $result['value'];
			} else {
				$data[$result['key']] = unserialize($result['value']);
			}
		}

		return $data;
	}
	
	public function getSliders(){
		$sql = "SELECT * FROM " . DB_PREFIX . "btslider s";
		
		$sql .= " GROUP BY s.id";
		
		$query = $this->db->query($sql);

		return $query->rows;
	}
	
	public function getSlider($slider_id){
		$sql = "SELECT * FROM " . DB_PREFIX . "btslider s WHERE s.id = '" . (int)$slider_id . "'";
		
		$sql .= " GROUP BY s.id";
		
		$query = $this->db->query($sql);

		return $query->row;
	}
	
	public function getSlide($slide_id){
		$sql = "SELECT * FROM " . DB_PREFIX . "btslider_slide ss WHERE ss.id = '" . (int)$slide_id . "'";
		
		$query = $this->db->query($sql);

		return $query->row;
	}
	
	public function getSlides($slider_id){
		$sql = "SELECT * FROM " . DB_PREFIX . "btslider_slide ss WHERE ss.slider_id = '" . (int)$slider_id . "'";
		
		$sql .= " GROUP BY ss.id";
		
		$sql .= " ORDER BY ss.sort_order";
		
		$query = $this->db->query($sql);

		return $query->rows;
	}
	
	public function copySlide($slide_id) {
		$query = $this->db->query("SELECT DISTINCT * FROM " . DB_PREFIX . "btslider_slide WHERE id = '" . (int)$slide_id . "'");

		if ($query->num_rows) {
			$data = array();

			$data = $query->row;
			$this->addSlide($data);
		}
	}
	
	public function deleteSlider($slider_id){
		$this->db->query("DELETE FROM " . DB_PREFIX . "btslider WHERE id = '" . (int)$slider_id . "'");
	}
	
	public function deleteSlide($slide_id){
		$this->db->query("DELETE FROM " . DB_PREFIX . "btslider_slide WHERE id = '" . (int)$slide_id . "'");
	}
	
	public function updateSortSlide($data){
		$count = 1;
		foreach ($data as $slide_id) {
			$query = "UPDATE " . DB_PREFIX . "btslider_slide SET sort_order = " . $count . " WHERE id = " . $slide_id;
			$this->db->query($query);
			$count ++;	
		}
	}
	
	public function getTotalslidesBySliderId($slider_id) {
		$query = $this->db->query("SELECT COUNT(*) AS total FROM " . DB_PREFIX . "btslider_slide WHERE slider_id = '" . (int)$slider_id . "'");

		return $query->row['total'];
	}
}
?>