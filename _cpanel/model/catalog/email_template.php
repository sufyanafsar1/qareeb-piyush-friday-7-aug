<?php
class ModelCatalogEmailTemplate extends Model {
	public function addTemplate($data) {
		
		$this->db->query("INSERT INTO " . DB_PREFIX . "email_template SET name = '" . $this->db->escape($data['name']) . "', `description` = '" . $this->db->escape($data['description']) . "', subject = '" . $this->db->escape($data['subject']) . "', status = '" . (int)$data['status'] . "', date_modified = NOW(), date_added = NOW()");

		$template_id = $this->db->getLastId();

		return $template_id;
	}

	public function editTemplate($template_id, $data) {
		
		$this->db->query("UPDATE " . DB_PREFIX . "email_template SET name = '" . $this->db->escape($data['name']) . "', `description` = '" . $this->db->escape($data['description']) . "', subject = '" . $this->db->escape($data['subject']) . "', status = '" . (int)$data['status'] . "', date_modified = NOW() WHERE template_id = '" . (int)$template_id . "'");
		
	}

	public function getTemplate($template_id) {
		$query = $this->db->query("SELECT DISTINCT * FROM " . DB_PREFIX . "email_template c WHERE c.template_id = '" . (int)$template_id . "'");

		return $query->row;
	}

	public function getTemplates($data = array()) {
		$sql = "SELECT * FROM " . DB_PREFIX . "email_template";

		if (isset($data['start']) || isset($data['limit'])) {
			if ($data['start'] < 0) {
				$data['start'] = 0;
			}

			if ($data['limit'] < 1) {
				$data['limit'] = 20;
			}

			$sql .= " LIMIT " . (int)$data['start'] . "," . (int)$data['limit'];
		}

		$query = $this->db->query($sql);

		return $query->rows;
	}

	public function getTotalTemplates() {
		$query = $this->db->query("SELECT COUNT(*) AS total FROM " . DB_PREFIX . "email_template");

		return $query->row['total'];
	}	
}
