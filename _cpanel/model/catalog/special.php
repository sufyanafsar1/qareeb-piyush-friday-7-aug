<?php
class ModelCatalogSpecial extends Model {

	public function getProduct($product_id) {
		$query = $this->db->query("SELECT DISTINCT *, (SELECT keyword FROM " . DB_PREFIX . "url_alias WHERE query = 'product_id=" . (int)$product_id . "') AS keyword FROM " . DB_PREFIX . "product p LEFT JOIN " . DB_PREFIX . "product_description pd ON (p.product_id = pd.product_id) WHERE p.product_id = '" . (int)$product_id . "' AND pd.language_id = '" . (int)$this->config->get('config_language_id') . "'");

		return $query->row;
	}

	public function getProducts($data = array()) {
		$sql = "SELECT pd.name, p.product_id, p.image, p.model, p.status, sp.price as special, p.price, s.firstname, sp.date_start, sp.date_end, p.upc
				FROM " . DB_PREFIX . "product_special sp 
				JOIN " . DB_PREFIX ."product p ON (sp.product_id = p.product_id) 
				JOIN " . DB_PREFIX . "product_description pd ON (p.product_id = pd.product_id) 
				JOIN " . DB_PREFIX . "sellers s ON (sp.seller_id = s.seller_id)
				WHERE pd.language_id = '" . (int)$this->config->get('config_language_id') . "'";

		if (!empty($data['filter_name'])) {
			$sql .= " AND pd.name LIKE '%" . $this->db->escape($data['filter_name']) . "%'";
		}

		if (!empty($data['filter_model'])) {
			$sql .= " AND p.model LIKE '" . $this->db->escape($data['filter_model']) . "%'";
		}

		if (isset($data['filter_status']) && !is_null($data['filter_status'])) {
			$sql .= " AND p.status = '" . (int)$data['filter_status'] . "'";
		}

		if (!empty($data['filter_upc'])) {
			$sql .= " AND p.upc LIKE '" . $this->db->escape($data['filter_upc']) . "%'";
		}
		
		if (isset($data['filter_seller_name']) && !is_null($data['filter_seller_name'])) {
			$sql .= " AND sp.seller_id = '" . (int)$data['filter_seller_name'] . "'";
		}

		$sql .= " GROUP BY p.product_id";

		$sort_data = array(
			'pd.name',
			'p.model',
			'p.price',
			'p.quantity',
			'p.status',
			'p.sort_order'
		);

		if (isset($data['sort']) && in_array($data['sort'], $sort_data)) {
			$sql .= " ORDER BY " . $data['sort'];
		} else {
			$sql .= " ORDER BY pd.name";
		}

		if (isset($data['order']) && ($data['order'] == 'DESC')) {
			$sql .= " DESC";
		} else {
			$sql .= " ASC";
		}

		if (isset($data['start']) || isset($data['limit'])) {
			if ($data['start'] < 0) {
				$data['start'] = 0;
			}

			if ($data['limit'] < 1) {
				$data['limit'] = 20;
			}

			$sql .= " LIMIT " . (int)$data['start'] . "," . (int)$data['limit'];
		}
		$query = $this->db->query($sql);

		return $query->rows;
	}
	
	public function getProductsAuto($data = array()) {
		$sql = "SELECT pd.name, p.product_id, p.image, p.model, p.status, sp.price as special, p.price, s.firstname 
				FROM " . DB_PREFIX . "product_special sp 
				JOIN " . DB_PREFIX ."product p ON (sp.product_id = p.product_id) 
				JOIN " . DB_PREFIX . "product_description pd ON (p.product_id = pd.product_id) 
				JOIN " . DB_PREFIX . "sellers s ON (sp.seller_id = s.seller_id)
				WHERE pd.language_id = '" . (int)$this->config->get('config_language_id') . "'";

		if (!empty($data['filter_name'])) {
			$sql .= " AND pd.name LIKE '%" . $this->db->escape($data['filter_name']) . "%'";
		}

		if (!empty($data['filter_model'])) {
			$sql .= " AND p.model LIKE '" . $this->db->escape($data['filter_model']) . "%'";
		}

		if (isset($data['filter_status']) && !is_null($data['filter_status'])) {
			$sql .= " AND p.status = '" . (int)$data['filter_status'] . "'";
		}

		if (!empty($data['filter_upc'])) {
			$sql .= " AND p.upc LIKE '" . $this->db->escape($data['filter_upc']) . "%'";
		}
		
		if (isset($data['filter_seller_name']) && !is_null($data['filter_seller_name'])) {
			$sql .= " AND sp.seller_id = '" . (int)$data['filter_seller_name'] . "'";
		}

		$sql .= " GROUP BY p.product_id";

		$sort_data = array(
			'pd.name',
			'p.model',
			'p.price',
			'p.quantity',
			'p.status',
			'p.sort_order'
		);

		if (isset($data['sort']) && in_array($data['sort'], $sort_data)) {
			$sql .= " ORDER BY " . $data['sort'];
		} else {
			$sql .= " ORDER BY pd.name";
		}

		if (isset($data['order']) && ($data['order'] == 'DESC')) {
			$sql .= " DESC";
		} else {
			$sql .= " ASC";
		}

		if (isset($data['start']) || isset($data['limit'])) {
			if ($data['start'] < 0) {
				$data['start'] = 0;
			}

			if ($data['limit'] < 1) {
				$data['limit'] = 20;
			}

			$sql .= " LIMIT " . (int)$data['start'] . "," . (int)$data['limit'];
		}
		$query = $this->db->query($sql);

		return $query->rows;
	}
	
	public function getProductSpecials($product_id) {
		$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "product_special WHERE product_id = '" . (int)$product_id . "' ORDER BY priority, price");

		return $query->rows;
	}
	
	public function getTotalProducts($data = array()) {
		
		$sql = "SELECT COUNT(DISTINCT sp.product_id) AS total FROM " . DB_PREFIX . "product_special sp JOIN " . DB_PREFIX ."product p ON (sp.product_id = p.product_id) LEFT JOIN " . DB_PREFIX . "product_description pd ON (p.product_id = pd.product_id)";

		$sql .= " WHERE pd.language_id = '" . (int)$this->config->get('config_language_id') . "'";

		if (!empty($data['filter_name'])) {
			$sql .= " AND pd.name LIKE '%" . $this->db->escape($data['filter_name']) . "%'";
		}

		if (!empty($data['filter_model'])) {
			$sql .= " AND p.model LIKE '" . $this->db->escape($data['filter_model']) . "%'";
		}

		if (isset($data['filter_status']) && !is_null($data['filter_status'])) {
			$sql .= " AND p.status = '" . (int)$data['filter_status'] . "'";
		}

		if (!empty($data['filter_upc'])) {
			$sql .= " AND p.upc LIKE '" . $this->db->escape($data['filter_upc']) . "%'";
		}
		
		if (isset($data['filter_seller_name']) && !is_null($data['filter_seller_name'])) {
			$sql .= " AND sp.seller_id = '" . (int)$data['filter_seller_name'] . "'";
		}

		$query = $this->db->query($sql);

		return $query->row['total'];
	}
	
	public function getSellers() {	
		$sellers_data = array();
		
		$query = $this->db->query("SELECT distinct(v.seller_id), CONCAT(v.firstname, ' ',v.lastname) AS name  FROM " . DB_PREFIX . "sellers as v," . DB_PREFIX . "seller as p where p.seller_id = v.seller_id ORDER BY v.firstname");
		
		$sellers = $query->rows;
		if(count($sellers)>0){
			foreach($sellers as $seller){
				$sellers_data[$seller['seller_id']] = $seller['name'];
			}
		}
		return $sellers_data;
	}
	
	public function getProductSellers1($product_id) {
		$query = $this->db->query("SELECT s.firstname, s.lastname, sp.price, sp.date_start, sp.date_end FROM " . DB_PREFIX . "product_special as sp," . DB_PREFIX . "sellers as s WHERE sp.product_id = '" . (int)$product_id . "' AND s.seller_id=sp.seller_id");	
		
		$data = array();
		if ( count( $query->rows ) > 0 ) {
			foreach ( $query->rows as $rows ) {
				$data[] = $rows;
			}
		}
		return $data;
	}

}
