<?php
class ModelLocalisationArea extends Model {
	public function addArea($data) {
		$this->db->query("INSERT INTO " . DB_PREFIX . "area SET status = '" . (int)$data['status'] . "', name = '" . $this->db->escape($data['name']) . "', name_ar = '" . $this->db->escape($data['name_ar']) . "', city_id = '" . (int)$data['city_id'] . "'");

		$this->cache->delete('area');
	}

	public function editArea($area_id, $data) {
		$this->db->query("UPDATE " . DB_PREFIX . "area SET status = '" . (int)$data['status'] . "', name = '" . $this->db->escape($data['name']) . "', name_ar = '" . $this->db->escape($data['name_ar']) . "', city_id = '" . (int)$data['city_id'] . "' WHERE area_id = '" . (int)$area_id . "'");

		$this->cache->delete('area');
	}

	public function deleteArea($area_id) {
		$this->db->query("DELETE FROM " . DB_PREFIX . "area WHERE area_id = '" . (int)$area_id . "'");

		$this->cache->delete('area');
	}

	public function getArea($area_id) {	  
		$query = $this->db->query("SELECT DISTINCT * FROM " . DB_PREFIX . "area WHERE area_id = '" . (int)$area_id . "'");

		return $query->row;
	}

	public function getAreas($data = array()) {
		$sql = "SELECT *, a.name, a.name_ar, c.name AS city FROM " . DB_PREFIX . "area a LEFT JOIN " . DB_PREFIX . "city c ON (a.city_id = c.city_id)";
       
        if (!empty($data['filter_name'])) {
			$sql .= " WHERE a.name LIKE '" . $this->db->escape($data['filter_name']) . "%'";
		}

		$sort_data = array(
			'c.name',
			'a.name',
			'a.name_ar'
		);

		if (isset($data['sort']) && in_array($data['sort'], $sort_data)) {
			$sql .= " ORDER BY " . $data['sort'];
		} else {
			$sql .= " ORDER BY c.name";
		}

		if (isset($data['order']) && ($data['order'] == 'DESC')) {
			$sql .= " DESC";
		} else {
			$sql .= " ASC";
		}

		if (isset($data['start']) || isset($data['limit'])) {
			if ($data['start'] < 0) {
				$data['start'] = 0;
			}

			if ($data['limit'] < 1) {
				$data['limit'] = 20;
			}

			$sql .= " LIMIT " . (int)$data['start'] . "," . (int)$data['limit'];
		}
        

		$query = $this->db->query($sql);

		return $query->rows;
	}
    
    
    
    public function getAreasByArrayId($data = array()) {
		$comma_separated = implode(",", $data);
     
        $sql = "SELECT *, a.name, a.name_ar, c.name AS city FROM " . DB_PREFIX . "area a LEFT JOIN " . DB_PREFIX . "city c ON (a.city_id = c.city_id)";
	
    	if($comma_separated !="")
        {
           $sql .= " WHERE area_id IN(".$comma_separated.")"; 
        }
    
		$query = $this->db->query($sql);

		return $query->rows;
	}

	public function getAreasByCityId($city_id) {
		$area_data = $this->cache->get('area.' . (int)$city_id);

		if (!$area_data) {
			$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "area WHERE city_id = '" . (int)$city_id . "' AND status = '1' ORDER BY name");

			$area_data = $query->rows;

			$this->cache->set('area.' . (int)$city_id, $area_data);
		}

		return $area_data;
	}

	public function getTotalAreas() {
		$query = $this->db->query("SELECT COUNT(*) AS total FROM " . DB_PREFIX . "area");

		return $query->row['total'];
	}

	public function getTotalAreasByCityId($city_id) {
		$query = $this->db->query("SELECT COUNT(*) AS total FROM " . DB_PREFIX . "area WHERE city_id = '" . (int)$city_id . "'");

		return $query->row['total'];
	}
}