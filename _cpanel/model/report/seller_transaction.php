<?php
class ModelReportSellerTransaction extends Model {


public function getPaymentInfos() {
		$sql = "SELECT v.username AS name, vp.payment_info AS details, vp.payment_amount, vp.payment_date,v.seller_id FROM `" . DB_PREFIX . "seller_payment` vp LEFT JOIN `" . DB_PREFIX . "sellers` v ON (vp.seller_id = v.seller_id) ";
		$sql .= " ORDER BY vp.payment_date DESC LIMIT 10";		
		$query = $this->db->query($sql);		
		return $query->rows;
	}
	
	public function addPaymentToSellerId($amt,$data,$seller_info,$nids) {
	
	
	
			$this->db->query("INSERT INTO " . DB_PREFIX . "seller_payment SET seller_id = '" . (int)$data['seller_id'] . "', 
			payment_info = '" . $this->db->escape($data['description']) . "', payment_amount = '".(float)$amt."', 
			payment_status = '".(int)$this->config->get('config_complete_status_id')."', payment_date = Now()");
			
			$this->db->query("INSERT INTO " . DB_PREFIX . "seller_transaction SET seller_id = '" . (int)$data['seller_id'] . "',
			order_id = '" . (int)$data['order_id'] . "',
			description = '" . $this->db->escape($data['description']) . "', amount = '-" . (float)$amt. "',
			transaction_status ='".(int)$this->config->get('config_complete_status_id')."', date_added = NOW()");
						



		foreach ($nids AS $details) {
				if ($details) {
				
					$this->db->query("UPDATE " . DB_PREFIX . "order_product op SET 
					seller_paid_status = '5',
					payment_description = '" . $this->db->escape($data['description']) . "',
					payment_date = Now()
					WHERE op.seller_id = '" . (int)$data['seller_id']. "' AND 
					op.order_product_id = '".(int)$details."'");



					
				}
			}
		

		
		$store_name = $this->config->get('config_name');	
		
			
			$this->language->load('mail/customer');
			
			$message  = "You have received ".$this->currency->format($amt, $this->config->get('config_currency'))." credit in your bank"."\n\n";
			$message  .= "Comment:\n\n";
			$message  .= $data['description'];
			$message  .= "\n\n";
			
			$mail = new Mail();
			$mail->protocol = $this->config->get('config_mail_protocol');
			$mail->parameter = $this->config->get('config_mail_parameter');
			$mail->smtp_hostname = $this->config->get('config_mail_smtp_host');
			$mail->smtp_username = $this->config->get('config_mail_smtp_username');
			$mail->smtp_password = html_entity_decode($this->config->get('config_mail_smtp_password'), ENT_QUOTES, 'UTF-8');
			$mail->smtp_port = $this->config->get('config_mail_smtp_port');
			$mail->smtp_timeout = $this->config->get('config_mail_smtp_timeout');	
			

			
			$mail->setTo($seller_info['email']);
			$mail->setFrom($this->config->get('config_email'));
			$mail->setSender($store_name);
			$mail->setSubject(html_entity_decode(sprintf($this->language->get('text_transaction_subject'), $this->config->get('config_name')), ENT_QUOTES, 'UTF-8'));
	    	$mail->setText($message);
			$mail->send();
			
			return true;
	}

	public function getSellerOrders($data = array(),$seller_access) {
		if(empty($seller_access)){$seller_access=0;}
		$sql = "SELECT op.product_id AS product_id, o.date_added AS date, o.order_id AS order_id, o.order_status_id AS order_status, pd.name AS product_name, op.price AS price,op.quantity AS quantity, op.commission AS commission,op.seller_total AS amount, op.total AS total, op.seller_paid_status AS paid_status FROM `" . DB_PREFIX . "order` o LEFT JOIN `" . DB_PREFIX . "order_product` op ON (o.order_id = op.order_id) LEFT JOIN `" . DB_PREFIX . "product_description` pd ON (op.product_id = pd.product_id)";
		
		if (!empty($data['filter_order_status_id'])) {
			$sql .= " WHERE o.order_status_id = '" . (int)$data['filter_order_status_id'] . "'";
		} else {
			$sql .= " WHERE o.order_status_id > '0'";
		}
		
		if (!empty($data['filter_seller_group'])) {
			$sql .= " AND op.seller_id IN (" . (int)$data['filter_seller_group'] . ")";
		} elseif ($seller_access) {
			$sql .= " AND op.seller_id IN (" . $seller_access . ")";
		}

		if (!empty($data['filter_paid_status'])) {
			$sql .= " AND op.seller_paid_status = '" . (int)$data['filter_paid_status'] . "'";
		} elseif (!is_null($data['filter_paid_status']) && $data['filter_seller_group']) {
			$sql .= " AND op.seller_paid_status = '" . (int)$data['filter_paid_status'] . "'";
		} else {
			$sql .= " AND op.seller_paid_status = '0'";
		}
		
		if (!empty($data['filter_date_start'])) {
			$sql .= " AND DATE(o.date_added) >= '" . $this->db->escape($data['filter_date_start']) . "'";
		}

		if (!empty($data['filter_date_end'])) {
			$sql .= " AND DATE(o.date_added) <= '" . $this->db->escape($data['filter_date_end']) . "'";
		}
		
		$sql .= " ORDER BY o.order_id DESC";
		
		if (isset($data['start']) || isset($data['limit'])) {
			if ($data['start'] < 0) {
				$data['start'] = 0;
			}			

			if ($data['limit'] < 1) {
				$data['limit'] = 20;
			}	
			
			$sql .= " LIMIT " . (int)$data['start'] . "," . (int)$data['limit'];
		}
		
		$query = $this->db->query($sql);

		return $query->rows;
	}	
	
	public function getSellersOrders($data = array(),$seller_access) {
	
	
		if(empty($seller_access)){$seller_access=0;}
		$sql = "SELECT op.product_id AS product_id, o.date_added AS date, o.order_id AS order_id, o.order_status_id AS order_status, pd.name AS product_name, op.price AS price,op.quantity AS quantity, op.commission AS commission,op.seller_total AS amount, op.total AS total,op.commissionper, op.seller_paid_status AS paid_status FROM `" . DB_PREFIX . "order` o LEFT JOIN `" . DB_PREFIX . "order_product` op ON (o.order_id = op.order_id) LEFT JOIN `" . DB_PREFIX . "product_description` pd ON (op.product_id = pd.product_id)";
		
		if (!empty($data['filter_order_status_id'])) {
			$sql .= " WHERE o.order_status_id = '" . (int)$data['filter_order_status_id'] . "'";
		} else {
			$sql .= " WHERE o.order_status_id > '0'";
		}
		
		if (!empty($data['filter_date_start'])) {
			$sql .= " AND DATE(o.date_added) >= '" . $this->db->escape($data['filter_date_start']) . "'";
		}

		if (!empty($data['filter_date_end'])) {
			$sql .= " AND DATE(o.date_added) <= '" . $this->db->escape($data['filter_date_end']) . "'";
		}

		$sql .= " AND op.seller_id IN (" . $seller_access . ")";
	
		if (!empty($data['filter_paid_status'])) {
			$sql .= " AND op.seller_paid_status = '" . (int)$data['filter_paid_status'] . "'";
		} elseif (!is_null($data['filter_paid_status'])) {
			$sql .= " AND op.seller_paid_status = '" . (int)$data['filter_paid_status'] . "'";
		} else {
			$sql .= " AND op.seller_paid_status = '0'";
		}
		
		
		
		$sql .= " ORDER BY o.order_id DESC";
		
		
		
		$query = $this->db->query($sql);

		return $query->rows;
	}

	
	public function newgetSellersOrders($seller_access,$order_product_id) {
	
	
		if(empty($seller_access)){$seller_access=0;}
		$sql = "SELECT * FROM `" . DB_PREFIX . "order` o LEFT JOIN `" . DB_PREFIX . "order_product` op ON (o.order_id = op.order_id)";
		
		
		
		$sql .= " WHERE o.order_status_id > '0' AND op.seller_paid_status = '5'
		AND op.seller_id IN (" . $seller_access . ") AND op.order_product_id = '" . (int)$order_product_id . "'";
		
		
		$query = $this->db->query($sql);

		return $query->rows;
	}
	
	public function getSellerTotalOrders12($data = array(),$seller_access) {
		if(empty($seller_access)){$seller_access=0;}
		$sql = "SELECT COUNT(*) AS total 
		FROM `" . DB_PREFIX . "order` o LEFT JOIN `" . DB_PREFIX . "order_product` op ON (o.order_id = op.order_id)
		LEFT JOIN `" . DB_PREFIX . "sellers` vds ON (op.seller_id = vds.seller_id)";
		
		if (!empty($data['filter_order_status_id'])) {
			$sql .= " WHERE o.order_status_id = '" . (int)$data['filter_order_status_id'] . "'";
		} else {
			$sql .= " WHERE o.order_status_id > '0'";
		}
		
		
		$sql .= " AND op.seller_id IN (" . $seller_access . ")";
	
		$sql .= " AND op.seller_paid_status = '5'";
		
		
		$sql .= " group by op.seller_id";
		
		$query = $this->db->query($sql);
		
		return $query->row['total'];
	}
	
	
	public function getSellerTotalOrders($data = array(),$seller_access) {
		if(empty($seller_access)){$seller_access=0;}
		$sql = "SELECT COUNT(*) AS total FROM `" . DB_PREFIX . "order` o 
		LEFT JOIN `" . DB_PREFIX . "order_product` op ON (o.order_id = op.order_id) 
		LEFT JOIN `" . DB_PREFIX . "product_description` pd ON (op.product_id = pd.product_id)";
		
		if (!empty($data['filter_order_status_id'])) {
			$sql .= " WHERE o.order_status_id = '" . (int)$data['filter_order_status_id'] . "'";
		} else {
			$sql .= " WHERE o.order_status_id > '0'";
		}
		
		if (!empty($data['filter_seller_group'])) {
			$sql .= " AND op.seller_id IN ('" . (int)$data['filter_seller_group'] . "')";
		} elseif ($seller_access) {
			$sql .= " AND op.seller_id IN (" . $seller_access . ")";
		}
		
		if (!empty($data['filter_paid_status'])) {
			$sql .= " AND op.seller_paid_status = '" . (int)$data['filter_paid_status'] . "'";
		} elseif (!is_null($data['filter_paid_status']) && $data['filter_seller_group']) {
			$sql .= " AND op.seller_paid_status = '" . (int)$data['filter_paid_status'] . "'";
		} else {
			$sql .= " AND op.seller_paid_status = '0'";
		}
		
		if (!empty($data['filter_date_start'])) {
			$sql .= " AND DATE(o.date_added) >= '" . $this->db->escape($data['filter_date_start']) . "'";
		}

		if (!empty($data['filter_date_end'])) {
			$sql .= " AND DATE(o.date_added) <= '" . $this->db->escape($data['filter_date_end']) . "'";
		}
		
		$query = $this->db->query($sql);
		
		return $query->row['total'];
	}
	
	
	public function updateorderproduct($seller_id,$opid) {


  			
		$query = $this->db->query("SELECT *	FROM `" . DB_PREFIX . "order` o 
		LEFT JOIN `" . DB_PREFIX . "order_product` op ON (o.order_id = op.order_id) 
		where op.order_product_id = '" . (int)$opid . "' AND op.seller_id ='" . (int)$seller_id . "' AND 
		op.seller_paid_status = '0' AND o.order_id = op.order_id");
	
			
        if ($query->rows) {
			foreach ($query->rows AS $data) {

			$this->db->query("UPDATE " . DB_PREFIX . "order_product  SET seller_paid_status = '5',
			payment_description = 'PayPal Payment',
			payment_date = Now()
			WHERE order_id = '" . (int)$data['order_id'] . "'"); 			
		     
			
		
           }

         }

		 
			return 1;
		
	}
	
	public function updatetransaction($seller_id,$amt) {
	
		if ($seller_id) {
			$query = $this->db->query("SELECT * FROM `" . DB_PREFIX . "seller_transaction`  WHERE seller_id = '" . (int)$seller_id . "'");
			
			$this->db->query("INSERT INTO " . DB_PREFIX . "seller_payment SET seller_id = '" . (int)$seller_id . "', 
			payment_info = 'Paypal Payment', payment_amount = '" . (float)$amt . "', payment_status = '5', payment_date = Now()");
			
			if ($query->rows) {
			foreach ($query->rows AS $data) {
						
							  
				 	$this->db->query("UPDATE " . DB_PREFIX . "seller_transaction  SET transaction_status = '5' WHERE 
					seller_transaction_id = '" . (int)$data['seller_transaction_id'] . "'");      
				   
		           }	
			}
			
			
			
			return 1;
		}
	}
	
	public function getSellerTotalAmount($data = array(),$seller_access) {
		if(empty($seller_access)){$seller_access=0;}
		$sql = "SELECT SUM(op.seller_total) AS seller_amount, SUM(op.commission) AS commission, SUM(op.total)
		AS gross_amount,SUM(op.quantity) AS quantity, op.seller_id AS seller_id, CONCAT(vds.firstname, ' ', vds.lastname) AS company,
		vds.paypal_email AS paypal_email,GROUP_CONCAT(op.order_product_id) AS order_product_id,o.order_id FROM `" . DB_PREFIX . "order` o LEFT JOIN `" . DB_PREFIX . "order_product` op ON (o.order_id = op.order_id) LEFT JOIN `" . DB_PREFIX . "sellers` vds ON (op.seller_id = vds.seller_id)";
		if (!empty($data['filter_order_status_id'])) {
			$sql .= " WHERE o.order_status_id = '" . (int)$data['filter_order_status_id'] . "'";
		} else {
			$sql .= " WHERE o.order_status_id > '0'";
		}
		
		$sql .= " AND op.seller_id IN (" . $seller_access . ")";
		
		if (!empty($data['filter_paid_status'])) {
			$sql .= " AND op.seller_paid_status = '" . (int)$data['filter_paid_status'] . "'";
		} elseif (!is_null($data['filter_paid_status']) && $data['filter_seller_group']) {
			$sql .= " AND op.seller_paid_status = '" . (int)$data['filter_paid_status'] . "'";
		} else {
			$sql .= " AND op.seller_paid_status = '0'";
		}
		
		if (!empty($data['filter_date_start'])) {
			$sql .= " AND DATE(o.date_added) >= '" . $this->db->escape($data['filter_date_start']) . "'";
		}

		if (!empty($data['filter_date_end'])) {
			$sql .= " AND DATE(o.date_added) <= '" . $this->db->escape($data['filter_date_end']) . "'";
		}

		$sql .= " group by op.seller_id";
		$query = $this->db->query($sql);
		return $query->rows;
	}
	
	
	public function getSellerpaidTotalAmount12($data = array(),$seller_access) {
		if(empty($seller_access)){$seller_access=0;}
		$sql = "SELECT op.seller_total AS seller_amount,op.commission AS commission, op.total
		AS gross_amount,op.quantity AS quantity, op.seller_id AS seller_id, CONCAT(vds.firstname, ' ', vds.lastname) AS company,
		vds.paypal_email AS paypal_email,op.*
		FROM `" . DB_PREFIX . "order` o LEFT JOIN `" . DB_PREFIX . "order_product` op ON (o.order_id = op.order_id)
		LEFT JOIN `" . DB_PREFIX . "sellers` vds ON (op.seller_id = vds.seller_id)";
		if (!empty($data['filter_order_status_id'])) {
			$sql .= " WHERE o.order_status_id = '" . (int)$data['filter_order_status_id'] . "'";
		} else {
			$sql .= " WHERE o.order_status_id > '0'";
		}
		
		$sql .= " AND op.seller_id IN (" . $seller_access . ")";
		
		
			$sql .= " AND op.seller_paid_status = '5'";
		
		
		if (!empty($data['filter_date_start'])) {
			$sql .= " AND DATE(o.date_added) >= '" . $this->db->escape($data['filter_date_start']) . "'";
		}

		if (!empty($data['filter_date_end'])) {
			$sql .= " AND DATE(o.date_added) <= '" . $this->db->escape($data['filter_date_end']) . "'";
		}

		
		if (isset($data['start']) || isset($data['limit'])) {
			if ($data['start'] < 0) {
				$data['start'] = 0;
			}

			if ($data['limit'] < 1) {
				$data['limit'] = 20;
			}

			$sql .= " LIMIT " . (int)$data['start'] . "," . (int)$data['limit'];
		}
		
		
		$query = $this->db->query($sql);
		return $query->rows;
	}
	
	public function getSellersName1() {
		$sql = "SELECT distinct(seller_id) FROM `" . DB_PREFIX . "sellers` v";
		$query = $this->db->query($sql);
		$sellers = array();
		if(count($query->rows)>0){
			foreach($query->rows as $seller){
				$sellers[] = $seller['seller_id'];
			}
		}
		return $sellers;
	}

	public function getSellersName($data = array(),$seller_access) {
		if(empty($seller_access)){$seller_access=0;}
		$sql = "SELECT distinct(c.seller_id) as seller_id, CONCAT(c.firstname, ' ', c.lastname) AS name FROM `" . DB_PREFIX . "sellers` c , `" . DB_PREFIX . "seller` s where s.seller_id = c.seller_id";

		$query = $this->db->query($sql);
		
		return $query->rows;
	}
	
	public function getcustomerinfo($orderid) {
		$sql = "SELECT * from `" . DB_PREFIX . "order` where order_id = '" . (int)$orderid . "'";
		$query = $this->db->query($sql);
		return $query->row;
	}

	public function addPaymentToSeller($payments,$order_details) {
		foreach ($payments AS $data) {
			$this->db->query("INSERT INTO " . DB_PREFIX . "seller_payment SET seller_id = '" . (int)$data['seller_id'] . "', payment_info = '" . $order_details . "', payment_amount = '" . (float)$data['paid_amount'] . "', payment_status = '5', payment_date = Now()");
			$this->db->query("INSERT INTO " . DB_PREFIX . "customer_transaction SET customer_id = '" . (int)$data['seller_id'] . "', description = 'PayPal Payment', amount = '-" . (float)$data['paid_amount'] . "',transaction_status =5 , date_added = NOW()");
		}
		
		foreach (unserialize($order_details) AS $details) {
			if ($details['order_id']) {
				$this->db->query("UPDATE " . DB_PREFIX . "order_product op SET seller_paid_status = '1' WHERE op.order_id = '" . (int)$details['order_id'] . "' AND op.product_id = '" . (int)$details['product_id'] . "'");
			}
		}
		
	}
	
	public function getPaymentHistory($data = array(),$seller_access) {
		if(empty($seller_access)){$seller_access=0;}
		$sql = "SELECT CONCAT(v.firstname, ' ', v.lastname) AS name, vp.payment_id AS payment_id, vp.payment_info AS details, vp.payment_amount, vp.payment_date FROM `" . DB_PREFIX . "seller_payment` vp LEFT JOIN `" . DB_PREFIX . "customer` v ON (vp.seller_id = v.customer_id) ";

		if  ($seller_access) {
			$sql .= " WHERE v.customer_id IN (" . $seller_access . ")";
		}
		
		$sql .= " ORDER BY vp.payment_date DESC LIMIT 10";
		
		$query = $this->db->query($sql);
		
		return $query->rows;
	}
	
	public function removeHistory($payment_id) {
		if ($payment_id) {
			$query = $this->db->query("SELECT * FROM `" . DB_PREFIX . "vendor_payment` vp WHERE vp.payment_id = '" . (int)$payment_id . "'");
			
			if ($query->row['payment_info']) {
				foreach (unserialize($query->row['payment_info']) AS $payment_details) {
					$this->db->query("UPDATE " . DB_PREFIX . "order_product op SET vendor_paid_status = '0' WHERE op.order_id = '" . (int)$payment_details['order_id'] . "' AND op.product_id = '" . (int)$payment_details['product_id'] . "'");
				}
				$this->db->query("DELETE FROM " . DB_PREFIX . "vendor_payment WHERE payment_id = '" . (int)$payment_id . "'");
			}
		}
	}
	
	public function editProduct($product_id, $data) {
		$this->db->query("UPDATE " . DB_PREFIX . "product SET model = '" . $this->db->escape($data['model']) . "', sku = '" . $this->db->escape($data['sku']) . "', upc = '" . $this->db->escape($data['upc']) . "', location = '" . $this->db->escape($data['location']) . "', quantity = '" . (int)$data['quantity'] . "', minimum = '" . (int)$data['minimum'] . "', subtract = '" . (int)$data['subtract'] . "', stock_status_id = '" . (int)$data['stock_status_id'] . "', date_available = '" . $this->db->escape($data['date_available']) . "', manufacturer_id = '" . (int)$data['manufacturer_id'] . "', shipping = '" . (int)$data['shipping'] . "', price = '" . (float)$data['price'] . "', points = '" . (int)$data['points'] . "', weight = '" . (float)$data['weight'] . "', weight_class_id = '" . (int)$data['weight_class_id'] . "', length = '" . (float)$data['length'] . "', width = '" . (float)$data['width'] . "', height = '" . (float)$data['height'] . "', length_class_id = '" . (int)$data['length_class_id'] . "', status = '" . (int)$data['status'] . "', tax_class_id = '" . $this->db->escape($data['tax_class_id']) . "', sort_order = '" . (int)$data['sort_order'] . "', date_modified = NOW() WHERE product_id = '" . (int)$product_id . "'");
	}
	
}
?>