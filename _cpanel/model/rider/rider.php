<?php
class ModelRiderRider extends Model {
	


	public function getRiderInfo($rider_id) {
	  
      $sqlQuery = "SELECT * FROM `ri_riderinfo` ri 
                    LEFT JOIN ri_riderinfoareas ria ON ri.rider_id = ria.rider_id 
                    LEFT JOIN ri_areas ra ON ria.area_id = ra.area_id 
                    LEFT JOIN ri_rideravailability rav ON ri.rider_id = rav.rider_id 
                    WHERE ri.rider_id ='".$rider_id."'";
		$query = $this->db->query($sqlQuery);

		return $query->row;
	}
    
    public function getTotalActiveRiders($data = array())
    {
        /* $query = $this->db->query("SELECT COUNT(*) AS total FROM `ri_riderinfo` ri 
                    LEFT JOIN ri_riderinfoareas ria ON ri.rider_id = ria.rider_id 
                    LEFT JOIN ri_areas ra ON ria.area_id = ra.area_id WHERE ri.is_active = 1 "); */
					
		$sql = "SELECT COUNT(*) AS total  FROM `ri_riderinfo` ri 
                    LEFT JOIN ri_riderinfoareas ria ON ri.rider_id = ria.rider_id 
                    LEFT JOIN ri_areas ra ON ria.area_id = ra.area_id WHERE ri.is_active = 1 ";
		if (!empty($data['filter_name'])) {
			$sql .= " AND LCASE(ri.name) LIKE '%" . $this->db->escape(utf8_strtolower($data['filter_name'])) . "%'";
		}
		
		if (!empty($data['filter_email'])) {
			$sql .= " AND LCASE(ri.email) LIKE '%" . $this->db->escape(utf8_strtolower($data['filter_email'])) . "%'";
		}
		$query = $this->db->query($sql);
		
		return $query->row['total'];
    }
	
	public function getTotalRidersOrder($data = array())
    {
        
		$sql = "SELECT COUNT(*) AS total  FROM `ri_riderorder` ri JOIN oc_order o ON ri.order_fromDB_id = o.order_id WHERE ri.rider_id = '".$data['rider_id']."'";
		
		$query = $this->db->query($sql);
		
		return $query->row['total'];
    }
	
	public function getRidersOrder($data = array())
    {
        
        $sql = "SELECT ri.* FROM `ri_riderorder` ri JOIN oc_order o ON ri.order_fromDB_id = o.order_id WHERE ri.rider_id = '".$data['rider_id']."' ORDER BY o.order_id DESC";
        			
    	if (isset($data['start']) || isset($data['limit'])) {
    		if ($data['start'] < 0) {
    			$data['start'] = 0;
    		}
    
    		if ($data['limit'] < 1) {
    			$data['limit'] = 20;
    		}

			$sql .= " LIMIT " . (int)$data['start'] . "," . (int)$data['limit'];
		}
        
        $query = $this->db->query($sql);

		return $query->rows;
    }
    
    public function getActiveRiders($data = array())
    {
        
        $sql = "SELECT ri.*,ra.area_name  FROM `ri_riderinfo` ri 
                    LEFT JOIN ri_riderinfoareas ria ON ri.rider_id = ria.rider_id 
                    LEFT JOIN ri_areas ra ON ria.area_id = ra.area_id WHERE ri.is_active = 1 ";
        		
		if (!empty($data['filter_name'])) {
			$sql .= " AND LCASE(ri.name) LIKE '%" . $this->db->escape(utf8_strtolower($data['filter_name'])) . "%'";
		}
		
		if (!empty($data['filter_email'])) {
			$sql .= " AND LCASE(ri.email) LIKE '%" . $this->db->escape(utf8_strtolower($data['filter_email'])) . "%'";
		}
		
    	if (isset($data['start']) || isset($data['limit'])) {
    		if ($data['start'] < 0) {
    			$data['start'] = 0;
    		}
    
    		if ($data['limit'] < 1) {
    			$data['limit'] = 20;
    		}

		 $sql .= " LIMIT " . (int)$data['start'] . "," . (int)$data['limit'];
		}
        
        $query = $this->db->query($sql);

		return $query->rows;
    }
    
    
    public function getRiderNameByOrderId($orderId){
         $sql = "SELECT ri.name FROM ri_riderorder ro JOIN ri_riderinfo ri on ro.rider_id = ri.rider_id WHERE ro.order_fromDB_id='".$orderId."' ORDER BY ro.Order_id";
        $query = $this->db->query($sql);
        
		if ($query->num_rows) {
            return $query->row['name'];
        } else {
            return '';
        }       
    }
    
  

}
