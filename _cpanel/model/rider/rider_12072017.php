<?php
class ModelRiderRider extends Model {
	


	public function getRiderInfo($rider_id) {
	  
      $sqlQuery = "SELECT * FROM `ri_riderinfo` ri 
                    INNER JOIN ri_riderinfoareas ria ON ri.rider_id = ria.rider_id 
                    INNER JOIN ri_areas ra ON ria.area_id = ra.area_id 
                    INNER JOIN ri_rideravailability rav ON ri.rider_id = rav.rider_id 
                    WHERE ri.rider_id ='".$rider_id."'";
		$query = $this->db->query($sqlQuery);

		return $query->row;
	}
    
    public function getTotalActiveRiders()
    {
        $query = $this->db->query("SELECT COUNT(*) AS total FROM `ri_riderinfo` ri 
                    INNER JOIN ri_riderinfoareas ria ON ri.rider_id = ria.rider_id 
                    INNER JOIN ri_areas ra ON ria.area_id = ra.area_id WHERE ri.is_active = 1 ");

		return $query->row['total'];
    }
    
    public function getActiveRiders($data = array())
    {
        
        $sql = "SELECT ri.*,ra.area_name  FROM `ri_riderinfo` ri 
                    INNER JOIN ri_riderinfoareas ria ON ri.rider_id = ria.rider_id 
                    INNER JOIN ri_areas ra ON ria.area_id = ra.area_id WHERE ri.is_active = 1 ";
        
    	if (isset($data['start']) || isset($data['limit'])) {
    		if ($data['start'] < 0) {
    			$data['start'] = 0;
    		}
    
    		if ($data['limit'] < 1) {
    			$data['limit'] = 20;
    		}

		 $sql .= " LIMIT " . (int)$data['start'] . "," . (int)$data['limit'];
		}
        
        $query = $this->db->query($sql);

		return $query->rows;
    }
    
  

}
