<?php
class ModelSaleUpgradedcustomer extends Model{		
	public function getUpgradedcustomers($data = array()) {
		$sql = "SELECT *,u.expiry_date as expiration_date, CONCAT(c.firstname, ' ', c.lastname) 
		AS name, cgd.commission_name AS commission FROM " . DB_PREFIX . "upgraded_sellers u
		LEFT JOIN " . DB_PREFIX . "sellers c ON (u.seller_id = c.seller_id) 
		LEFT JOIN " . DB_PREFIX . "commission cgd ON 
		(u.commission_id = cgd.commission_id)";

		$implode = array();
		
		if (!empty($data['filter_name'])) {
			$implode[] = "CONCAT(c.firstname, ' ', c.lastname) LIKE '%" . $this->db->escape($data['filter_name']) . "%'";
		}
				
		if (!empty($data['filter_upgrade_date'])) {
			$implode[] = "DATE(u.upgrade_date) = DATE('" . $this->db->escape($data['filter_upgrade_date']) . "')";
		}
		
		if ($implode) {
			$sql .= " AND " . implode(" AND ", $implode);
		}
		
		$sort_data = array(
			'name',
			'u.upgrade_date'
		);	
			
		if (isset($data['sort']) && in_array($data['sort'], $sort_data)) {
			$sql .= " ORDER BY " . $data['sort'];	
		} else {
			$sql .= " ORDER BY upgrade_date";	
		}
			
		if (isset($data['order']) && ($data['order'] == 'DESC')) {
			$sql .= " DESC";
		} else {
			$sql .= " ASC";
		}
		
		if (isset($data['start']) || isset($data['limit'])) {
			if ($data['start'] < 0) {
				$data['start'] = 0;
			}			

			if ($data['limit'] < 1) {
				$data['limit'] = 20;
			}	
			
			$sql .= " LIMIT " . (int)$data['start'] . "," . (int)$data['limit'];
		}		
		
		$query = $this->db->query($sql);
		
		return $query->rows;	
	}
	
			
	public function getTotalUpgradedcustomers($data = array()) {
      	$sql = "SELECT COUNT(*) AS total FROM " . DB_PREFIX . "upgraded_sellers as u
		LEFT JOIN " . DB_PREFIX . "sellers c ON (u.seller_id = c.seller_id)";
		$implode = array();
		if (!empty($data['filter_name'])) {
			$implode[] = "CONCAT(c.firstname, ' ', c.lastname) LIKE '%" . $this->db->escape($data['filter_name']) . "%'";
		}		
		if (!empty($data['filter_upgrade_date'])) {
			$implode[] = "DATE(upgrade_date) = DATE('" . $this->db->escape($data['filter_upgrade_date']) . "')";
		}
		if ($implode) {
			$sql .= " WHERE " . implode(" AND ", $implode);
		}		
		$query = $this->db->query($sql);		
		return $query->row['total'];
	}
}
?>