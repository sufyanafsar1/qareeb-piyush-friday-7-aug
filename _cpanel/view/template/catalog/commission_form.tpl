<?php echo $header; ?><?php echo $column_left; ?>
<div id="content">
  <div class="page-header">
    <div class="container-fluid">
      <div class="pull-right">
        <button type="submit" form="form-review" data-toggle="tooltip" title="<?php echo $button_save; ?>" class="btn btn-primary"><i class="fa fa-save"></i></button>
        <a href="<?php echo $cancel; ?>" data-toggle="tooltip" title="<?php echo $button_cancel; ?>" class="btn btn-default"><i class="fa fa-reply"></i></a></div>
      <h1><?php echo $heading_title; ?></h1>
      <ul class="breadcrumb">
        <?php foreach ($breadcrumbs as $breadcrumb) { ?>
        <li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
        <?php } ?>
      </ul>
    </div>
  </div>
  <div class="container-fluid">
    <?php if ($error_warning) { ?>
    <div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> <?php echo $error_warning; ?>
      <button type="button" class="close" data-dismiss="alert">&times;</button>
    </div>
    <?php } ?>
    <div class="panel panel-default">
      <div class="panel-heading">
        <h3 class="panel-title"><i class="fa fa-pencil"></i> <?php echo $heading_title; ?></h3>
      </div>
      <div class="panel-body">
        <form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data" id="form-review" class="form-horizontal">
		
		 <ul class="nav nav-tabs">
            <li class="active"><a href="#tab-general" data-toggle="tab"><?php echo $tab_general; ?></a></li>
			
			
			<li><a href="#tab-plan" data-toggle="tab"><?php echo $tab_plan; ?></a></li>
			
          </ul>
		  
		   <div class="tab-content">
		   
		  
	         <div id="tab-plan"  class="tab-pane">
		  
				  <div class="form-group">
					<label class="col-sm-2 control-label" for="input-author"><?php echo $entry_amount; ?></label>
					<div class="col-sm-10">
					  <input type="text" name="amount" value="<?php echo $amount; ?>" placeholder="<?php echo $entry_amount; ?>" 
					  id="input-author" class="form-control" />
					 
					</div>
				  </div>
				  
				  <div class="form-group">
					<label class="col-sm-2 control-label" for="input-author"><?php echo $entry_duration; ?></label>
					<div class="col-sm-10">
					  per<input type="text" name="per" value="<?php echo $per; ?>" placeholder="per" 
					  id="input-author" />
						<select name="duration_id" >
						<?php foreach ($durations as $duration) { ?>
						<?php if ($duration['duration_id'] == $duration_id) { ?>
						<option value="<?php echo $duration['duration_id']; ?>" selected="selected"><?php echo $duration['duration_name']; ?></option>
						<?php } else { ?>
						<option value="<?php echo $duration['duration_id']; ?>"><?php echo $duration['duration_name']; ?></option>
						<?php } ?>
						<?php } ?>
						</select>
					</div>
				  </div>
				
		  
		  </div>
		  
		  
		  
            <div class="tab-pane active" id="tab-general">
		  
          <div class="form-group required">
            <label class="col-sm-2 control-label" for="input-author"><?php echo $entry_name; ?></label>
            <div class="col-sm-10">
              <input type="text" name="commission_name" value="<?php echo $commission_name; ?>" placeholder="<?php echo $entry_name; ?>" 
			  id="input-author" class="form-control" />
              <?php if ($error_commission_name) { ?>
              <div class="text-danger"><?php echo $error_commission_name; ?></div>
              <?php } ?>
            </div>
          </div>
          <div class="form-group required">
            <label class="col-sm-2 control-label" for="input-product">Default Commission rate:</label>
            <div class="col-sm-10">
			
			<input name="commission" value="<?php echo $commission; ?>" size="25" class="form-control"/>
            <?php if ($error_commission) { ?>
            <div class="text-danger"><?php echo $error_commission; ?></div>
            <?php } ?>
			
			
            
            </div>
          </div>
          <div class="form-group required">
            <label class="col-sm-2 control-label" for="input-text"><?php echo $entry_product_limit; ?></label>
            <div class="col-sm-10">
              <input name="product_limit" value="<?php echo $product_limit; ?>" size="25" class="form-control"/>
            </div>
          </div>
          <div class="form-group required">
            <label class="col-sm-2 control-label" for="input-name"><?php echo $entry_sort_order; ?></label>
            <div class="col-sm-10">
             <input name="sort_order" value="<?php echo $sort_order; ?>" size="1" class="form-control"/>
            </div>
          </div>
          <div class="form-group">
            <label class="col-sm-2 control-label" for="input-status"><?php echo $entry_commission; ?></label>
          
          </div>
		  
		  <?php foreach($categories as $catcommission){ ?>
		  <div class="form-group">
		   <label class="col-sm-2 control-label" for="input-name"><?php echo $catcommission['name'];?></label>
                 <div class="col-sm-10">
             <input name="commission_rate[<?php echo $catcommission['category_id'];?>]" value="<?php if(isset($commission_rate[$catcommission['category_id']])){ echo $commission_rate[$catcommission['category_id']];}?>" size="2" />%
		  
            </div>
			
          </div>
		  <?php } ?>
		  
		  </div>
		  
		  
		
		  
		  
		  </div>
        </form>
		  <script type="text/javascript"><!--

$('#description').summernote({height: 300});

//--></script>
      </div>
    </div>
  </div>
 </div>
<?php echo $footer; ?>