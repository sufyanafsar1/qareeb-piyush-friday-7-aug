<?php echo $header; ?><?php echo $column_left; ?>
<div id="content">
  <div class="page-header">
    <div class="container-fluid">
     <div class="pull-right"><a href="<?php echo $insert; ?>" data-toggle="tooltip" title="<?php echo $button_insert; ?>" class="btn btn-primary"><i class="fa fa-plus"></i></a>
        <button type="button" data-toggle="tooltip" title="<?php echo $button_delete; ?>" class="btn btn-danger" onclick="confirm('<?php echo $text_confirm; ?>') ? $('#form-review').submit() : false;"><i class="fa fa-trash-o"></i></button>
      </div>
      <h1><?php echo $heading_title; ?></h1>
      <ul class="breadcrumb">
        <?php foreach ($breadcrumbs as $breadcrumb) { ?>
        <li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
        <?php } ?>
      </ul>
    </div>
  </div>
  <div class="container-fluid">
    <?php if ($error_warning) { ?>
    <div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> <?php echo $error_warning; ?>
      <button type="button" class="close" data-dismiss="alert">&times;</button>
    </div>
    <?php } ?>
    <?php if ($success) { ?>
    <div class="alert alert-success"><i class="fa fa-check-circle"></i> <?php echo $success; ?>
      <button type="button" class="close" data-dismiss="alert">&times;</button>
    </div>
    <?php } ?>
    <div class="panel panel-default">
      <div class="panel-heading">
        <h3 class="panel-title"><i class="fa fa-list"></i> <?php echo $heading_title; ?></h3>
      </div>
      <div class="panel-body">
        
        <form action="<?php echo $delete; ?>" method="post" enctype="multipart/form-data" id="form-review">
          <div class="table-responsive">
            <table class="table table-bordered table-hover">
             <thead>
          <tr>
            <td width="1" style="text-align: text-center;"><input type="checkbox" onclick="$('input[name*=\'selected\']').attr('checked', this.checked);" /></td>
			<td class="left"><?php if ($sort == 'commission_name') { ?>
              <a href="<?php echo $sort_commission_name; ?>" class="<?php echo strtolower($order); ?>"><?php echo $column_name; ?></a>
              <?php } else { ?>
              <a href="<?php echo $sort_commission_name; ?>"><?php echo $column_name; ?></a>
              <?php } ?></td>
            
			<td class="left"><?php if ($sort == 'commission') { ?>  
			  <a href="<?php echo $sort_commission; ?>" class="<?php echo strtolower($order); ?>"><?php echo $column_commission; ?></a>
              <?php } else { ?>
              <a href="<?php echo $sort_commission; ?>"><?php echo $column_commission; ?></a>
              <?php } ?></td>
			<td class="text-right"><?php echo $column_total_agents; ?></td>
			
			<td class="text-right"><?php echo $column_product_limit; ?></td>
			
			
            <td class="text-right"><?php if ($sort == 'sort_order') { ?>
              <a href="<?php echo $sort_sort_order; ?>" class="<?php echo strtolower($order); ?>"><?php echo $column_sort_order; ?></a>
              <?php } else { ?>
              <a href="<?php echo $sort_sort_order; ?>"><?php echo $column_sort_order; ?></a>
            <?php } ?></td>
            <td class="text-right"><?php echo $column_action; ?></td>
          </tr>
        </thead>
        <tbody>
          <?php if ($commissions) { ?>
          <?php foreach ($commissions as $commission) { ?>
          <tr>
          
			  
			   <td class="text-center"><?php if (in_array($commission['commission_id'], $selected)) { ?>
                    <input type="checkbox" name="selected[]" value="<?php echo $commission['commission_id']; ?>" checked="checked" />
                    <?php } else { ?>
                    <input type="checkbox" name="selected[]" value="<?php echo $commission['commission_id']; ?>" />
                    <?php } ?></td>
			<td class="left"><?php echo $commission['commission_name']; ?></td>
			<td class="left"><?php echo $commission['commission']; ?></td>
			<td class="text-right"><?php echo $commission['total_agents']; ?></td>
			
			<td class="text-right"><?php echo $commission['product_limit']; ?></td>
			
			<td class="text-right"><?php echo $commission['sort_order']; ?></td>
             <td class="text-right"><a href="<?php echo $commission['edit']; ?>" data-toggle="tooltip" title="<?php echo $button_edit; ?>" class="btn btn-primary"><i class="fa fa-pencil"></i></a></td>
                </tr>
          <?php } ?>
          <?php } else { ?>
          <tr>
            <td class="text-center" colspan="7"><?php echo $text_no_results; ?></td>
          </tr>
          <?php } ?>
        </tbody>
            </table>
          </div>
        </form>
        <div class="row">
          <div class="col-sm-6 text-left"><?php echo $pagination; ?></div>
          <div class="col-sm-6 text-text-right"><?php echo $results; ?></div>
        </div>
      </div>
    </div>
  </div>
  
 </div>
<?php echo $footer; ?>