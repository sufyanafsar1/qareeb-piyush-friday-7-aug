<?php echo $header; ?><?php echo $column_left; ?>
<div id="content">
  <div class="page-header">
    <div class="container-fluid">
      <div class="pull-right">
        <button type="submit" form="form-review" data-toggle="tooltip" title="<?php echo $button_save; ?>" class="btn btn-primary"><i class="fa fa-save"></i></button>
        <a href="<?php echo $cancel; ?>" data-toggle="tooltip" title="<?php echo $button_cancel; ?>" class="btn btn-default"><i class="fa fa-reply"></i></a></div>
      <h1><?php echo $heading_title; ?></h1>
      <ul class="breadcrumb">
        <?php foreach ($breadcrumbs as $breadcrumb) { ?>
        <li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
        <?php } ?>
      </ul>
    </div>
  </div>
  <div class="container-fluid">
    <?php if ($error_warning) { ?>
    <div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> <?php echo $error_warning; ?>
      <button type="button" class="close" data-dismiss="alert">&times;</button>
    </div>
    <?php } ?>
    <div class="panel panel-default">
      <div class="panel-heading">
        <h3 class="panel-title"><i class="fa fa-pencil"></i> <?php echo $heading_title; ?></h3>
      </div>
      <div class="panel-body">
        <form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data" id="form-review" class="form-horizontal">
          <div class="form-group required">
            <label class="col-sm-2 control-label" for="input-author"><?php echo $entry_author; ?></label>
            <div class="col-sm-10">
              <input type="text" name="author" value="<?php echo $author; ?>" placeholder="<?php echo $entry_author; ?>" id="input-author" class="form-control" />
              <?php if ($error_author) { ?>
              <div class="text-danger"><?php echo $error_author; ?></div>
              <?php } ?>
            </div>
          </div>
          
          <div class="form-group required">
            <label class="col-sm-2 control-label" for="input-text"><?php echo $entry_text; ?></label>
            <div class="col-sm-10">
              <textarea name="text" cols="60" rows="8" placeholder="<?php echo $entry_text; ?>" id="input-text" class="form-control"><?php echo $text; ?></textarea>
              <?php if ($error_text) { ?>
              <dspan class="text-danger">
              <?php echo $error_text; ?></span>
              <?php } ?>
            </div>
          </div>
		  <style>
			.accountDiv ul{margin:0;padding:0;}
			.accountDiv li{cursor:pointer;list-style-type: none;display: inline-block;color: #F0F0F0;text-shadow: 0 0 1px #666666;font-size:20px;}
			.accountDiv .highlight, .accountDiv .selected {color:#F4B30A;text-shadow: 0 0 1px #F48F0A;}
		</style>
		  <div class="form-group required accountDiv">
					
			<label class="col-sm-2 control-label" style="padding-top: 0px;" for="input-name"><?php echo $entry_rating; ?></label>
									
			<div class="col-sm-10">	
				<div id="tutorial-rating">
					<input type="hidden" name="rating" id="rating" value="<?php echo $rating; ?>" />
					<ul onMouseOut="resetRating('rating');">
					  <?php  
						$i=0;
						for($i=1;$i<=5;$i++) { 
							$selected = "";
							if(!empty($rating) && $i<=$rating) {
								$selected = "selected";
							}											
						?>
							<li class='<?php echo $selected; ?>' onmouseover="highlightStar(this,'rating');" onmouseout="removeHighlight('rating');" onClick="addRating(this,'rating');">&#9733;</li>  
					  <?php }  ?>
					<ul>
					<?php if ($error_rating) { ?>
						<span class="text-danger"><?php echo $error_rating; ?></span>
					<?php } ?>
				</div>								
			</div>						
		</div>
		
		<div class="form-group required accountDiv">
					
			<label class="col-sm-2 control-label" style="padding-top: 0px;" for="input-name">Rate Rider:</label>
									
			<div class="col-sm-10">	
				<div id="tutorial-rate_rider">
					<input type="hidden" name="rate_rider" id="rate_rider" value="<?php echo $rate_rider; ?>" />
					<ul onMouseOut="resetRating('rate_rider');">
					  <?php  
						$i=0;
						for($i=1;$i<=5;$i++) { 
							$selected = "";
							if(!empty($rate_rider) && $i<=$rate_rider) {
								$selected = "selected";
							}											
						?>
							<li class='<?php echo $selected; ?>' onmouseover="highlightStar(this,'rate_rider');" onmouseout="removeHighlight('rate_rider');" onClick="addRating(this,'rate_rider');">&#9733;</li>  
					  <?php }  ?>
					<ul>
					<?php if ($error_rate_rider) { ?>
						<span class="text-danger"><?php echo $error_rate_rider; ?></span>
					<?php } ?>
				</div>								
			</div>						
		</div>
		
		<div class="form-group required accountDiv">
					
			<label class="col-sm-2 control-label" style="padding-top: 0px;" for="input-name">Overall Rating:</label>
									
			<div class="col-sm-10">	
				<div id="tutorial-overall_rating">
					<input type="hidden" name="overall_rating" id="overall_rating" value="<?php echo $overall_rating; ?>" />
					<ul onMouseOut="resetRating('overall_rating');">
					  <?php  
						$i=0;
						for($i=1;$i<=5;$i++) { 
							$selected = "";
							if(!empty($overall_rating) && $i<=$overall_rating) {
								$selected = "selected";
							}											
						?>
							<li class='<?php echo $selected; ?>' onmouseover="highlightStar(this,'overall_rating');" onmouseout="removeHighlight('overall_rating');" onClick="addRating(this,'overall_rating');">&#9733;</li>  
					  <?php }  ?>
					<ul>
					<?php if ($error_rate_rider) { ?>
						<span class="text-danger"><?php echo $error_rate_rider; ?></span>
					<?php } ?>
				</div>								
			</div>						
		</div>
				          
          <div class="form-group">
            <label class="col-sm-2 control-label" for="input-status"><?php echo $entry_status; ?></label>
            <div class="col-sm-10">
              <select name="status" id="input-status" class="form-control">
                <?php if ($status) { ?>
                <option value="1" selected="selected"><?php echo $text_enabled; ?></option>
                <option value="0"><?php echo $text_disabled; ?></option>
                <?php } else { ?>
                <option value="1"><?php echo $text_enabled; ?></option>
                <option value="0" selected="selected"><?php echo $text_disabled; ?></option>
                <?php } ?>
              </select>
            </div>
          </div>
        </form>
      </div>
    </div>
  </div>
  </div>
  <script>
function highlightStar(obj,id) {
	removeHighlight(id);		
	$('.accountDiv #tutorial-'+id+' li').each(function(index) {
		$(this).addClass('highlight');
		if(index == $('.accountDiv #tutorial-'+id+' li').index(obj)) {
			return false;	
		}
	});
}

function removeHighlight(id) {
	$('.accountDiv #tutorial-'+id+' li').removeClass('selected');
	$('.accountDiv #tutorial-'+id+' li').removeClass('highlight');
}

function addRating(obj,id) {
	$('.accountDiv #tutorial-'+id+' li').each(function(index) {
		$(this).addClass('selected');
		$('#tutorial-'+id+' #'+id).val((index+1));
		if(index == $('.accountDiv #tutorial-'+id+' li').index(obj)) {
			return false;	
		}
	});	
}

function resetRating(id) {
	if($('#tutorial-'+id+' #'+id).val() != 0) {
		$('.accountDiv #tutorial-'+id+' li').each(function(index) {
			$(this).addClass('selected');
			if((index+1) == $('#tutorial-'+id+' #'+id).val()) {
				return false;	
			}
		});
	}
} 
</script>
<?php echo $footer; ?>