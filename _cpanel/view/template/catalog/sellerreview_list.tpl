<?php echo $header; ?><?php echo $column_left; ?>
<div id="content">
  <div class="page-header">
    <div class="container-fluid">
     <div class="pull-right">
	
        <button type="button" data-toggle="tooltip" title="<?php echo $button_delete; ?>" class="btn btn-danger" onclick="confirm('Are you sure?') ? $('#form-review').submit() : false;"><i class="fa fa-trash-o"></i></button>
      </div>
      <h1><?php echo $heading_title; ?></h1>
      <ul class="breadcrumb">
        <?php foreach ($breadcrumbs as $breadcrumb) { ?>
        <li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
        <?php } ?>
      </ul>
    </div>
  </div>
  <style>
.accountDiv ul{margin:0;padding:0;}
.accountDiv li{cursor:pointer;list-style-type: none;display: inline-block;color: #F0F0F0;text-shadow: 0 0 1px #666666;font-size:20px;}
.accountDiv .highlight, .accountDiv .selected {color:#F4B30A;text-shadow: 0 0 1px #F48F0A;}
</style>
  <div class="container-fluid">
    <?php if ($error_warning) { ?>
    <div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> <?php echo $error_warning; ?>
      <button type="button" class="close" data-dismiss="alert">&times;</button>
    </div>
    <?php } ?>
    <?php if ($success) { ?>
    <div class="alert alert-success"><i class="fa fa-check-circle"></i> <?php echo $success; ?>
      <button type="button" class="close" data-dismiss="alert">&times;</button>
    </div>
    <?php } ?>
    <div class="panel panel-default">
      <div class="panel-heading">
        <h3 class="panel-title"><i class="fa fa-list"></i> <?php echo $heading_title; ?></h3>
      </div>
    
        <form action="<?php echo $delete; ?>" method="post" enctype="multipart/form-data" id="form-review">
          <div class="table-responsive">
            <table class="table table-bordered table-hover">
             <thead>
            <tr>
              <td width="1" style="text-align: center;"><input type="checkbox" onclick="$('input[name*=\'selected\']').attr('checked', this.checked);" /></td>
              <td class="text-left"><?php if ($sort == 'pd.name') { ?>
                <a href="<?php echo $sort_product; ?>" class="<?php echo strtolower($order); ?>"><?php echo $column_product; ?></a>
                <?php } else { ?>
                <a href="<?php echo $sort_product; ?>"><?php echo $column_product; ?></a>
                <?php } ?></td>
              <td class="text-left"><?php if ($sort == 'r.author') { ?>
                <a href="<?php echo $sort_author; ?>" class="<?php echo strtolower($order); ?>"><?php echo $column_author; ?></a>
                <?php } else { ?>
                <a href="<?php echo $sort_author; ?>"><?php echo $column_author; ?></a>
                <?php } ?></td>
              <td class="text-right"><?php if ($sort == 'r.rating') { ?>
                <a href="<?php echo $sort_rating; ?>" class="<?php echo strtolower($order); ?>">Rate of service</a>
                <?php } else { ?>
                <a href="<?php echo $sort_rating; ?>">Rate of service</a>
                <?php } ?></td>
				<td class="text-right">Rate of Rider</td>
				<td class="text-right">Overall Rating</td>
              <td class="text-left"><?php if ($sort == 'r.status') { ?>
                <a href="<?php echo $sort_status; ?>" class="<?php echo strtolower($order); ?>"><?php echo $column_status; ?></a>
                <?php } else { ?>
                <a href="<?php echo $sort_status; ?>"><?php echo $column_status; ?></a>
                <?php } ?></td>
              <td class="text-left"><?php if ($sort == 'r.date_added') { ?>
                <a href="<?php echo $sort_date_added; ?>" class="<?php echo strtolower($order); ?>"><?php echo $column_date_added; ?></a>
                <?php } else { ?>
                <a href="<?php echo $sort_date_added; ?>"><?php echo $column_date_added; ?></a>
                <?php } ?></td>
              <td class="text-right"><?php echo $column_action; ?></td>
            </tr>
          </thead>
          <tbody>
            <?php if ($reviews) { ?>
            <?php foreach ($reviews as $review) { ?>
            <tr>
              <td style="text-align:center;"><?php if ($review['selected']) { ?>
                <input type="checkbox" name="selected[]" value="<?php echo $review['review_id']; ?>" checked="checked" />
                <?php } else { ?>
                <input type="checkbox" name="selected[]" value="<?php echo $review['review_id']; ?>" />
                <?php } ?></td>
              <td class="text-left"><?php echo $review['name']; ?></td>
              <td class="text-left"><?php echo $review['author']; ?></td>
              <td class="text-right accountDiv">
				<div id="tutorial-rating">					
					<ul>
					  <?php  
						$i=0;
						for($i=1;$i<=5;$i++) { 
							$selected = "";
							if(!empty($review['rating']) && $i<=$review['rating']) {
								$selected = "selected";
							}											
						?>
							<li class='<?php echo $selected; ?>'>&#9733;</li>  
					  <?php }  ?>
					<ul>					
				</div>
				<?php //echo $review['rating']; ?>
				</td>
			 <td class="text-right accountDiv">			
				<div id="tutorial-rate_rider">					
					<ul>
					  <?php  
						$i=0;
						for($i=1;$i<=5;$i++) { 
							$selected = "";
							if(!empty($review['rate_rider']) && $i<=$review['rate_rider']) {
								$selected = "selected";
							}											
						?>
							<li class='<?php echo $selected; ?>'>&#9733;</li>  
					  <?php }  ?>
					<ul>					
				</div>
				<?php //echo $review['rate_rider']; ?>
				</td>
			 <td class="text-right accountDiv">			
				<div id="tutorial-overall_rating">					
					<ul>
					  <?php  
						$i=0;
						for($i=1;$i<=5;$i++) { 
							$selected = "";
							if(!empty($review['overall_rating']) && $i<=$review['overall_rating']) {
								$selected = "selected";
							}											
						?>
							<li class='<?php echo $selected; ?>'>&#9733;</li>  
					  <?php }  ?>
					<ul>					
				</div>
			 <?php //echo $review['overall_rating']; ?>
			 </td>
			  
              <td class="text-left"><?php echo $review['status']; ?></td>
              <td class="text-left"><?php echo $review['date_added']; ?></td>
              <td class="text-right"><?php foreach ($review['action'] as $action) { ?>
                [ <a href="<?php echo $action['href']; ?>"><?php echo $action['text']; ?></a> ]
                <?php } ?></td>
            </tr>
            <?php } ?>
            <?php } else { ?>
            <tr>
              <td class="center" colspan="7"><?php echo $text_no_results; ?></td>
            </tr>
            <?php } ?>
          </tbody>
		  
            </table>
          </div>
        </form>
        <div class="row">
          <div class="col-sm-6 text-left"><?php echo $pagination; ?></div>
          <div class="col-sm-6 text-right"><?php echo $results; ?></div>
        </div>
      </div>
    </div>
  </div>
  </div>	
<?php echo $footer; ?>