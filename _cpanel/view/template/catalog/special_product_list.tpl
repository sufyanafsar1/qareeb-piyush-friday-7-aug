<?php echo $header; ?><?php echo $column_left; ?>
<div id="content">  
  <div class="container-fluid">
       <div class="panel panel-default">
      <div class="panel-heading">
        <h3 class="panel-title"><i class="fa fa-list"></i> <?php echo $text_list; ?></h3>
      </div>
      <div class="panel-body">
        <div class="well">
          <div class="row">
            <div class="col-sm-4">
              <div class="form-group">
                <label class="control-label" for="input-name"><?php echo $entry_name; ?></label>
                <input type="text" name="filter_name" value="<?php echo $filter_name; ?>" placeholder="<?php echo $entry_name; ?>" id="input-name" class="form-control" />
              </div>             
            </div>
            <div class="col-sm-4">            
			  <div class="form-group">
                <label class="control-label" for="input-upc"><?php echo $entry_upc; ?></label>
                <input type="text" name="filter_upc" value="<?php echo $filter_upc; ?>" placeholder="<?php echo $entry_upc; ?>" id="input-upc" class="form-control" />
              </div>
            </div>
			
			<div class="col-sm-4">            
				<div class="form-group">
					<label class="control-label" for="input-status">Seller</label>
					<select name="filter_seller_name" id="input-status" class="form-control">
						<option value=""></option>
						<?php if (!is_null($filter_seller_name) && !$filter_seller_name) { ?>
						<?php } else { ?>
						<?php } ?>
						<?php foreach ($sellers as $key=>$seller) { ?>
							<?php if ($filter_seller_name == $key) { ?>
								<option selected value="<?php echo $key; ?>"><?php echo $seller; ?></option>
							<?php } else { ?>
								<option value="<?php echo $key; ?>"><?php echo $seller; ?></option>
							<?php } ?>
						<?php } ?>
					</select>
				</div>
             
				<button type="button" id="button-filter" class="btn btn-primary pull-right"><i class="fa fa-search"></i> <?php echo $button_filter; ?></button>
            </div>
          </div>
        </div>
        <form action="<?php echo $delete; ?>" method="post" enctype="multipart/form-data" id="form-product">
          <div class="table-responsive">
            <table class="table table-bordered table-hover">
              <thead>
                <tr>
                  <td style="width: 1px;" class="text-center"><input type="checkbox" onclick="$('input[name*=\'selected\']').prop('checked', this.checked);" /></td>
				 <?php /*  <td class="text-left">Product Id</td> */ ?>
                  <td class="text-center"><?php echo $column_image; ?></td>
                  <td class="text-left" style="width: 25%;"><?php if ($sort == 'pd.name') { ?>
                    <a href="<?php echo $sort_name; ?>" class="<?php echo strtolower($order); ?>"><?php echo $column_name; ?></a>
                    <?php } else { ?>
                    <a href="<?php echo $sort_name; ?>"><?php echo $column_name; ?></a>
                    <?php } ?></td>                 
					<td class="text-left">UPC</td>
                  <td class="text-left"><?php if ($sort == 'p.price') { ?>
                    <a href="<?php echo $sort_price; ?>" class="<?php echo strtolower($order); ?>"><?php echo $column_price; ?></a>
                    <?php } else { ?>
                    <a href="<?php echo $sort_price; ?>"><?php echo $column_price; ?></a>
                    <?php } ?></td>                
                  <td class="text-left"><?php if ($sort == 'p.status') { ?>
                    <a href="<?php echo $sort_status; ?>" class="<?php echo strtolower($order); ?>"><?php echo $column_status; ?></a>
                    <?php } else { ?>
                    <a href="<?php echo $sort_status; ?>"><?php echo $column_status; ?></a>
                    <?php } ?></td> 
					<td class="text-left">Seller</td>		
					<td class="text-left">Start Date</td>
					<td class="text-left">End Date</td>		
                </tr>
              </thead>
              <tbody>
                <?php if ($products) { ?>
                <?php foreach ($products as $product) { ?>
                <tr>
                  <td class="text-center"><?php if (in_array($product['product_id'], $selected)) { ?>
                    <input type="checkbox" name="selected[]" value="<?php echo $product['product_id']; ?>" checked="checked" />
                    <?php } else { ?>
                    <input type="checkbox" name="selected[]" value="<?php echo $product['product_id']; ?>" />
                    <?php } ?></td>
					<?php /* <td class="text-left"><?php echo $product['product_id']; ?></td> */ ?>
                  <td class="text-center"><?php if ($product['image']) { ?>
                    <img src="<?php echo $product['image']; ?>" alt="<?php echo properName($product['name']); ?>" class="img-thumbnail" />
                    <?php } else { ?>
                    <span class="img-thumbnail list"><i class="fa fa-camera fa-2x"></i></span>
                    <?php } ?></td>
                  <td class="text-left"><?php echo properName($product['name']); ?></td>                  
				  <td class="text-left"><?php echo $product['upc']; ?></td>
                  <td class="text-left">
					<?php if(!empty($product['seller_name'])){ ?>
						<ul  style="padding-left: 15px;">
						<?php foreach ($product['seller_name'] as $seller) { ?>
							<li><?php echo $seller['price']; ?></li>
						<?php } ?>
						</ul>
					<?php } ?>
				</td>               
                  <td class="text-left"><?php echo $product['status']; ?></td> 
					<td class="text-left">
						<?php if(!empty($product['seller_name'])){ ?>
							<ul  style="padding-left: 15px;">
							<?php foreach ($product['seller_name'] as $seller) { ?>
								<li><?php echo $seller['firstname'] . $seller['lastname']; ?></li>
							<?php } ?>
							</ul>
						<?php } ?>
					</td>
					<td class="text-left">						
						<?php if(!empty($product['seller_name'])){ ?>
							<ul style="padding-left: 15px;">
							<?php foreach ($product['seller_name'] as $seller) { ?>
								<li><?php echo $seller['date_start']; ?></li>
							<?php } ?>
							</ul>
						<?php } ?>
					</td>
					<td class="text-left">
						<?php if(!empty($product['seller_name'])){ ?>
							<ul  style="padding-left: 15px;">
							<?php foreach ($product['seller_name'] as $seller) { ?>
								<li><?php echo $seller['date_end']; ?></li>
							<?php } ?>
							</ul>
						<?php } ?>						
					</td>
                </tr>
                <?php } ?>
                <?php } else { ?>
                <tr>
                  <td class="text-center" colspan="9"><?php echo $text_no_results; ?></td>
                </tr>
                <?php } ?>
              </tbody>
            </table>
          </div>
        </form>
        <div class="row">
          <div class="col-sm-6 text-left"><?php echo $pagination; ?></div>
          <div class="col-sm-6 text-right"><?php echo $results; ?></div>
        </div>
      </div>
    </div>
  </div>
  <script type="text/javascript"><!--
$('#button-filter').on('click', function() {
	var url = 'index.php?route=catalog/special&token=<?php echo $token; ?>';
	
	var filter_seller_name = $('select[name=\'filter_seller_name\']').val();
	
	if (filter_seller_name) {
		url += '&filter_seller_name=' + encodeURIComponent(filter_seller_name);
	}

	var filter_name = $('input[name=\'filter_name\']').val();
	if (filter_name) {
		url += '&filter_name=' + encodeURIComponent(filter_name);
	}

	var filter_upc = $('input[name=\'filter_upc\']').val();
	if (filter_upc) {
		url += '&filter_upc=' + encodeURIComponent(filter_upc);
	}

	location = url;
});
//--></script>
  <script type="text/javascript"><!--
$('input[name=\'filter_name\']').autocomplete({
	'source': function(request, response) {
		$.ajax({
			url: 'index.php?route=catalog/special/autocomplete&token=<?php echo $token; ?>&filter_name=' +  encodeURIComponent(request),
			dataType: 'json',
			success: function(json) {
				response($.map(json, function(item) {
					return {
						label: item['name'],
						value: item['product_id']
					}
				}));
			}
		});
	},
	'select': function(item) {
		$('input[name=\'filter_name\']').val(item['label']);
	}
});
//--></script></div>
<?php echo $footer; ?>