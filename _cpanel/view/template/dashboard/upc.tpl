<div class="panel panel-default">
  <div class="panel-heading">
    <h3 class="panel-title"><i class="fa fa-barcode"></i> <?php echo $heading_title; ?></h3>
  </div>
  <div class="table-responsive">
    <table class="table">
      <thead>
        <tr>
          <td ><?php echo $column_product; ?></td>
          <td><?php echo $column_upc_current; ?></td>
          <td><?php echo $column_upc_new; ?></td>
          <td><?php echo $column_status; ?></td>
          <td><?php echo $column_date_added; ?></td>

          <td class="text-right"><?php echo $column_action; ?></td>
        </tr>
      </thead>
      <tbody>
        <?php if ($products) { ?>
        <?php foreach ($products as $product) {
   $status = 'Pending';
     $cBarCode = (!empty($product['upc_current'])) ? $product['upc_current'] : '-';
        ?>
        <tr>
          <td ><?php echo $product['name']; ?></td>
          <td><?php echo $cBarCode; ?></td>
          <td><?php echo $product['upc_new']; ?></td>
          <td><?php echo $status; ?></td>
          <td><?php echo $product['date_added']; ?></td>
          <td class="text-right">
            <button type="button" onclick= "acceptBarCode(<?php echo $product['upc_id'] ?>)" data-loading-text="Loading..." data-toggle="tooltip" title="" class="btn btn-success" data-original-title="Approve"><i class="fa fa-thumbs-o-up"></i></button>
            <button type="button" onclick= "rejectBarCode(<?php echo $product['upc_id'] ?>)" data-loading-text="Loading..." data-toggle="tooltip" title="" class="btn btn-danger" data-original-title="Disapprove"><i class="fa fa-thumbs-o-down"></i></button>
          </td>
        </tr>
        <?php } ?>
        <?php } else { ?>
        <tr>
          <td class="text-center" colspan="6"><?php echo $text_no_results; ?></td>
        </tr>
        <?php } ?>
      </tbody>
    </table>
  </div>
</div>

<script type="text/javascript">

    function acceptBarCode(upcId)
    {

        var req = {
            id: upcId
        };

        $.ajax({
            url:'index.php?route=dashboard/upc/acceptBarCode&token=<?php echo $token; ?>',
            type: 'POST',
            accepts: 'application/json',
            data: req,
            async: true,
            dataType: 'json',
            success: function (response) {

                if (!(response == null || response.toString() == "")) {

                    if(response=='success'){
                        alert("Status updated successfully!");
                    }else{
                        alert("Status update failed!");
                    }

                    location.reload();
                }
            },
            error: function (response) {
                alert("Error Occured");
            }
        });
    }

    function rejectBarCode(upcId)
    {

        var req = {
            id: upcId
        };

        $.ajax({
            url:'index.php?route=dashboard/upc/rejectBarCode&token=<?php echo $token; ?>',
            type: 'POST',
            accepts: 'application/json',
            data: req,
            async: true,
            dataType: 'json',
            success: function (response) {

                if(response=='success'){
                    alert("Status updated successfully!");
                }else{
                    alert("Status update failed!");
                }
                location.reload();
            },
            error: function (response) {
                alert("Error Occured");
            }
        });
    }

</script>