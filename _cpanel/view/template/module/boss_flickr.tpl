<?php echo $header; ?><?php echo $column_left; ?>
<div id="content">
<div class="page-header">
    <div class="container-fluid">
      <div class="pull-right">
        <button type="submit" form="form-boss-flickr" data-toggle="tooltip" title="<?php echo $button_save; ?>" class="btn btn-primary"><i class="fa fa-save"></i></button>
        <a href="<?php echo $cancel; ?>" data-toggle="tooltip" title="<?php echo $button_cancel; ?>" class="btn btn-default"><i class="fa fa-reply"></i></a></div>
      <h1><?php echo $heading_title; ?></h1>
      <ul class="breadcrumb">
        <?php foreach ($breadcrumbs as $breadcrumb) { ?>
        <li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
        <?php } ?>
      </ul>
    </div>
  </div>
 <div class="container-fluid">
  <?php if ($error_warning) { ?>
    <div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> <?php echo $error_warning; ?>
      <button type="button" class="close" data-dismiss="alert">&times;</button>
    </div>
    <?php } ?>
<div class="panel panel-default">
    <div class="panel-heading">
		<h3 class="panel-title"><i class="fa fa-pencil"></i> <?php echo $heading_title; ?></h3> 
	</div>
  <div class="panel-body">
    <form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data" id="form-boss-flickr" class="form-horizontal">
		<div class="form-group require">
            <label class="col-sm-2 control-label" for="input-name"><?php echo $entry_name; ?></label>
            <div class="col-sm-10">
              <input type="text" name="name" value="<?php echo $name; ?>" placeholder="<?php echo $entry_name; ?>" id="input-name" class="form-control" />
              <?php if ($error_name) { ?>
              <div class="text-danger"><?php echo $error_name; ?></div>
              <?php } ?>
            </div>
		</div>
		<div class="form-group">
			<label class="col-sm-2 control-label"><?php echo $entry_api_key; ?></label>
			<div class="col-sm-10">
				<input type="text" name="boss_flickr_module[api_key]" value="<?php echo isset($modules['api_key'])?$modules['api_key']:'313c1e9d1c1819731bc46f9182e6325f'; ?>" class="form-control" />
				<?php if ($error_api_key) { ?>
					<div class="text-danger"><?php echo $error_api_key; ?></div>
				<?php } ?>
			</div>
		</div>
		<div class="form-group">
			<label class="col-sm-2 control-label"><?php echo $entry_user_id; ?></label>
			<div class="col-sm-10">
				<input type="text" name="boss_flickr_module[user_id]" value="<?php echo isset($modules['user_id'])?$modules['user_id']:'131017800@N05'; ?>" class="form-control" />
				<?php if ($error_user_id) { ?>
					<div class="text-danger"><?php echo $error_user_id; ?></div>
				<?php } ?>
			</div>
		</div>
		<div class="form-group">
			<label class="col-sm-2 control-label"><?php echo $entry_title; ?></label>
			<div class="col-sm-10">
				<?php foreach ($languages as $language) { ?>
					<div class="form-group">
						<div class="col-sm-11">
							<input name="boss_flickr_module[title][<?php echo $language['language_id']; ?>]" value="<?php echo isset($modules['title'][$language['language_id']]) ? $modules['title'][$language['language_id']] : ''; ?>" class="form-control" />
						</div>
						<div class="col-sm-1">
							<img src="view/image/flags/<?php echo $language['image']; ?>" title="<?php echo $language['name']; ?>" />
						</div>
					</div>
				<?php } ?>
			</div>
		</div>
		<div class="form-group">
			<label class="col-sm-2 control-label"><?php echo $entry_limit; ?></label>
			<div class="col-sm-10">
				<input type="text" name="boss_flickr_module[limit]" value="<?php echo isset($modules['limit'])?$modules['limit']:6; ?>" class="form-control" />
				<?php if ($error_limit) { ?>
					<div class="text-danger"><?php echo $error_limit; ?></div>
				<?php } ?>
			</div>
		</div>
		<div class="form-group">
            <label class="col-sm-2 control-label"><?php echo $entry_image_size; ?></label>
            <div class="col-sm-10">
              <select name="boss_flickr_module[image_size]" class="form-control">
                
                <option value="1" <?php if (isset($modules['image_size']) && $modules['image_size']==1) { echo 'selected="selected"'; } ?>>75x75</option>
                <option value="2" <?php if (isset($modules['image_size']) && $modules['image_size']==2) { echo 'selected="selected"'; } ?>>150x150</option>
                <option value="3" <?php if (isset($modules['image_size']) && $modules['image_size']==3) { echo 'selected="selected"'; } ?>>100x53</option>
                <option value="4" <?php if (isset($modules['image_size']) && $modules['image_size']==4) { echo 'selected="selected"'; } ?>>150x80</option>
                
              </select>
            </div>
        </div>
		<div class="form-group">
            <label class="col-sm-2 control-label" for="input-status"><?php echo $entry_status; ?></label>
            <div class="col-sm-10">
              <select name="status" id="input-status" class="form-control">
                <?php if ($status) { ?>
                <option value="1" selected="selected"><?php echo $text_enabled; ?></option>
                <option value="0"><?php echo $text_disabled; ?></option>
                <?php } else { ?>
                <option value="1"><?php echo $text_enabled; ?></option>
                <option value="0" selected="selected"><?php echo $text_disabled; ?></option>
                <?php } ?>
              </select>
            </div>
        </div> 					
    </form>
  </div>
</div>
</div>
<?php echo $footer; ?>