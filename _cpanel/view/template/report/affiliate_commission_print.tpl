<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8" />
<title><?php echo $heading_title; ?></title>
<link href="view/javascript/bootstrap/css/bootstrap.css" rel="stylesheet" media="all" />
<script type="text/javascript" src="view/javascript/jquery/jquery-2.1.1.min.js"></script>
<script type="text/javascript" src="view/javascript/bootstrap/js/bootstrap.min.js"></script>
<link href="view/javascript/font-awesome/css/font-awesome.min.css" type="text/css" rel="stylesheet" />
<link type="text/css" href="view/stylesheet/stylesheet.css" rel="stylesheet" media="all" />
<link rel="icon" type="image/png" href="https://www.qareeb.com/image/catalog/logo/favicon.png">
</head>
    <body onload="printpage()">
<div class="container">
<div style="page-break-after: always;">
<h1><?php echo $heading_title; ?></h1>    
        <div class="table-responsive">
          <table class="table table-bordered"> 
		  <thead>
		  <tr class="heading_title">
			<td class="left"><?php echo $column_affiliate; ?></td>
			<td class="left"><?php echo $column_email; ?></td>
			<td class="left"><?php echo $column_status; ?></td>
			<td class="right"><?php echo $column_commission; ?></td>
			<td class="right"><?php echo $column_orders; ?></td>
			<td class="right"><?php echo $column_total; ?></td>
			
		  </tr>   
		</thead>
          <?php if ($affiliates) { ?>
          <?php foreach ($affiliates as $affiliate) { ?>
          <tr>
            <td class="left"><?php echo $affiliate['affiliate']; ?></td>
            <td class="left"><?php echo $affiliate['email']; ?></td>
            <td class="left"><?php echo $affiliate['status']; ?></td>
            <td class="right"><?php echo $affiliate['commission']; ?></td>
            <td class="right"><?php echo $affiliate['orders']; ?></td>
            <td class="right"><?php echo $affiliate['total']; ?></td>
         </tr>
          <?php } ?>
          <?php } else { ?>
          <tr>
            <td class="text-center" colspan="6"><?php echo $text_no_results; ?></td>
          </tr>
          <?php } ?>
      </table>
    </div>
</div>
    </div>
</body>	  
<script language="Javascript1.2">
  <!--
  function printpage() {
  window.print();
  }
  //-->
</script>
