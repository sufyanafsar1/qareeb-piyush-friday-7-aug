<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8" />
<title><?php echo $heading_title; ?></title>
<link href="view/javascript/bootstrap/css/bootstrap.css" rel="stylesheet" media="all" />
<script type="text/javascript" src="view/javascript/jquery/jquery-2.1.1.min.js"></script>
<script type="text/javascript" src="view/javascript/bootstrap/js/bootstrap.min.js"></script>
<link href="view/javascript/font-awesome/css/font-awesome.min.css" type="text/css" rel="stylesheet" />
<link type="text/css" href="view/stylesheet/stylesheet.css" rel="stylesheet" media="all" />
<link rel="icon" type="image/png" href="https://www.qareeb.com/image/catalog/logo/favicon.png">

</head>
<body onload="printpage()">
<div class="container">
<div style="page-break-after: always;">
<h1><?php echo $heading_title; ?></h1>    
			<div class="table-responsive">
			  <table class="table table-bordered"> 
			  <thead>
				  <tr>
					<td class="text-left"><?php echo $column_comment; ?></td>
					<td class="text-left"><?php echo $column_ip; ?></td>
					<td class="text-left"><?php echo $column_date_added; ?></td>
				  </tr>
				</thead>
				<tbody>
				  <?php if ($activities) { ?>
				  <?php foreach ($activities as $activity) { ?>
				  <tr>
					<td class="text-left"><?php echo $activity['comment']; ?></td>
					<td class="text-left"><?php echo $activity['ip']; ?></td>
					<td class="text-left"><?php echo $activity['date_added']; ?></td>
				  </tr>
				  <?php } ?>
				  <?php } else { ?>
				  <tr>
					<td class="text-center" colspan="4"><?php echo $text_no_results; ?></td>
				  </tr>
				  <?php } ?>
				</tbody>
			  </table>
			</div>
        
      </div>
    </div>

</body>
</html>
  <script language="Javascript1.2">
  <!--
  function printpage() {

  window.print();
  }
  //-->
</script>