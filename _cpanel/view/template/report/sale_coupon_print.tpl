<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8" />
<title><?php echo $heading_title; ?></title>
<link href="view/javascript/bootstrap/css/bootstrap.css" rel="stylesheet" media="all" />
<script type="text/javascript" src="view/javascript/jquery/jquery-2.1.1.min.js"></script>
<script type="text/javascript" src="view/javascript/bootstrap/js/bootstrap.min.js"></script>
<link href="view/javascript/font-awesome/css/font-awesome.min.css" type="text/css" rel="stylesheet" />
<link type="text/css" href="view/stylesheet/stylesheet.css" rel="stylesheet" media="all" />
<link rel="icon" type="image/png" href="https://www.qareeb.com/image/catalog/logo/favicon.png">

</head>
<body onload="printpage()">
<div class="container">
<div style="page-break-after: always;">
<h1><?php echo $heading_title; ?></h1>    
        <div class="table-responsive">
          <table class="table table-bordered"> 
		  <thead>
          <tr class="heading">
            <td class="left"><?php echo $column_name; ?></td>
            <td class="left"><?php echo $column_code; ?></td>
            <td class="right"><?php echo $column_orders; ?></td>
            <td class="right"><?php echo $column_total; ?></td>
            <td class="right"><?php echo $column_action; ?></td>
		  </thead>
          </tr>        
          <?php if ($coupons) { ?>
          <?php foreach ($coupons as $coupon) { ?>
          <tr>
            <td class="left"><?php echo $coupon['name']; ?></td>
            <td class="left"><?php echo $coupon['code']; ?></td>
            <td class="right"><?php echo $coupon['orders']; ?></td>
            <td class="right"><?php echo $coupon['total']; ?></td>
            <td class="right"><?php foreach ($coupon['action'] as $action) { ?>
              [ <a href="<?php echo $action['href']; ?>"><?php echo $action['text']; ?></a> ]
              <?php } ?></td>            
          </tr>
          <?php } ?>
          <?php } else { ?>
          <tr>
            <td class="text-center" colspan="6"><?php echo $text_no_results; ?></td>
          </tr>
          <?php } ?>
      </table>
    </div>
</div>
    </div>
</body>
</html>
<script language="Javascript1.2">
  <!--
  function printpage() {
  window.print();
  }
  //-->
</script>
