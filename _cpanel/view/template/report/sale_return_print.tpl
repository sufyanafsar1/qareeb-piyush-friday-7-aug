<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8" />
<title><?php echo $heading_title; ?></title>
<link href="view/javascript/bootstrap/css/bootstrap.css" rel="stylesheet" media="all" />
<script type="text/javascript" src="view/javascript/jquery/jquery-2.1.1.min.js"></script>
<script type="text/javascript" src="view/javascript/bootstrap/js/bootstrap.min.js"></script>
<link href="view/javascript/font-awesome/css/font-awesome.min.css" type="text/css" rel="stylesheet" />
<link type="text/css" href="view/stylesheet/stylesheet.css" rel="stylesheet" media="all" />
<link rel="icon" type="image/png" href="https://www.qareeb.com/image/catalog/logo/favicon.png">

</head>
    <body onload="printpage()">
<div class="container">
<div style="page-break-after: always;">
<h1><?php echo $heading_title; ?></h1>    
        <div class="table-responsive">
          <table class="table table-bordered"> 
		  <thead>      
          <tr class="heading">
            <td class="left"><?php echo $column_date_start; ?></td>
            <td class="left"><?php echo $column_date_end; ?></td>
            <td class="right"><?php echo $column_returns; ?></td>
          </tr>     
		 </thead>
          <?php if ($returns) { ?>
          <?php foreach ($returns as $return) { ?>
          <tr>
            <td class="left"><?php echo $return['date_start']; ?></td>
            <td class="left"><?php echo $return['date_end']; ?></td>
            <td class="right"><?php echo $return['returns']; ?></td>
          </tr>
          <?php } ?>
          <?php } else { ?>
          <tr>
            <td class="text-center" colspan="3"><?php echo $text_no_results; ?></td>
          </tr>
          <?php } ?>        
      </table>
    </div>
</div>
    </div>
</body>
<script language="Javascript1.2">
  <!--
  function printpage() {
  window.print();
  }
  //-->
</script>
