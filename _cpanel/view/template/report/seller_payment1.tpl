<?php echo $header; ?><?php echo $column_left; ?>
<div id="content">
  <div class="page-header">
    <div class="container-fluid">
      <h1><?php echo $heading_title; ?></h1>
	  
	  <div class="pull-right">
   
        <a data-toggle="tooltip" title="Confirm" class="btn btn-primary" onclick="confirm('Are you sure you wants to confirm payment?') ? $('#form').submit() : false;"><i class="fa fa-save">
		</i></a>
		 <a href="<?php echo $cancel; ?>" data-toggle="tooltip" title="Cancel" class="btn btn-default"><i class="fa fa-reply"></i></a>
		
		</div>
    
	
       
    </div>
  </div>
  <div class="container-fluid">
    <div class="panel panel-default">
      <div class="panel-heading">
        <h3 class="panel-title"><i class="fa fa-bar-chart"></i> <?php echo $heading_title; ?></h3>
      </div>
      
        <div class="table-responsive">
		<form method="post" enctype="multipart/form-data" action="<?php echo $insert;?>" id="form">
          <table class="table table-bordered">
		  
		  <tr>
		<td class="text-left" colspan="2">
		Bank Details of 
		<?php if(!empty($business_name)){echo $business_name;}else{echo $name;}?><br/>
		Payee Name: <?php echo $account_name;?><br/>
		Bank Name: <?php echo $bank_name;?><br/>
		Account Number: <?php echo $account_number;?><br/>
		Branch: <?php echo $branch;?><br/>
		IFSC CODE: <?php echo $ifsccode;?><br/>
		Telephone: <?php echo $telephone;?><br/>
		</td>
          </tr>
          <tr>
		<td class="text-left">Note:</td>
		<td><textarea name="description" cols="40" rows="5"></textarea>
		<?php if ($error_description) { ?>
                  <div class="text-danger"><?php echo $error_description; ?></div>
                  <?php } ?>
		
		</td>
		<input type="hidden" name="seller_id" value="<?php echo $seller_id;?>" />
		<input type="hidden" name="seller_transaction_ids" value="<?php echo $seller_transaction_ids;?>" />
		</td>
		<tr>
		<td class="text-left">Amount to pay:</td>
		<td>
		<input type="text" name="seller_amount" value="<?php echo $seller_amount;?>" />
		<?php if ($error_seller_amount) { ?>
                  <div class="text-danger"><?php echo $error_seller_amount; ?></div>
                  <?php } ?>
		</td>
		
		
          </tr>
		  
         </table>
		 </form>
        </div>
       

  <script type="text/javascript"><!--

$('#description').summernote({height: 300});

//--></script>
      </div>
    </div>
  </div>
  
 </div>
<?php echo $footer; ?>