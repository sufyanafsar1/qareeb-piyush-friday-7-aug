<?php echo $header; ?><?php echo $column_left; ?>
<div id="content">
  <div class="page-header">
    <div class="container-fluid">
	
	
	 
      <h1><?php echo $heading_title; ?></h1>
      <ul class="breadcrumb">
        <?php foreach ($breadcrumbs as $breadcrumb) { ?>
        <li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
        <?php } ?>
      </ul>
    </div>
  </div>
  <div class="container-fluid">
    <div class="panel panel-default">
      <div class="panel-heading">
	  
	   
	
        <h3 class="panel-title"><i class="fa fa-bar-chart"></i> <?php echo $heading_title; ?></h3>
		   
      </div>
	  
	       
	  
	  <div class="panel-body">
        <div class="well">
          <div class="row">
            <div class="col-sm-6">
              <div class="form-group">
                <label class="control-label" for="input-date-start"><?php echo $entry_date_start; ?></label>
                <div class="input-group date">
                  <input type="text" name="filter_date_start" value="<?php echo $filter_date_start; ?>" placeholder="<?php echo $entry_date_start; ?>" data-date-format="YYYY-MM-DD" id="input-date-start" class="form-control" />
                  <span class="input-group-btn">
                  <button type="button" class="btn btn-default"><i class="fa fa-calendar"></i></button>
                  </span></div>
              </div>
              <div class="form-group">
                <label class="control-label" for="input-date-end"><?php echo $entry_date_end; ?></label>
                <div class="input-group date">
                  <input type="text" name="filter_date_end" value="<?php echo $filter_date_end; ?>" placeholder="<?php echo $entry_date_end; ?>" data-date-format="YYYY-MM-DD" id="input-date-end" class="form-control" />
                  <span class="input-group-btn">
                  <button type="button" class="btn btn-default"><i class="fa fa-calendar"></i></button>
                  </span></div>
              </div>
            </div>
           
              <button type="button" id="button-filter" class="btn btn-primary pull-right"><i class="fa fa-search"></i> <?php echo $button_filter; ?></button>
            
			</div>
          </div>
        </div>
		
      
        <div class="table-responsive">
          <table class="table table-bordered">
            <thead>
              <tr>
                <td class="text-left"><?php echo $column_seller_id; ?></td>
                <td class="text-left"><?php echo $column_seller_name; ?></td>
                <td class="text-right"><?php echo $column_quantity; ?></td>
                <td class="text-right"><?php echo $column_total; ?></td>
                <td class="text-right"><?php echo $column_commision; ?></td>
                <td class="text-right"><?php echo $column_amount; ?></td>
              <td class="text-right"><?php echo $column_action; ?></td>
             
			 </tr>
            </thead>
            <tbody>
              <?php if ($incomes) { ?>
              <?php foreach ($incomes as $income) { ?>
				<tr>
				<td class="text-left"><a style="cursor:pointer;" onclick="return showhidediv(<?php echo $income['seller_id'];?>);" id="showhide<?php echo $income['seller_id'];?>">Show</a> <?php echo $income['seller_id']; ?></td>
				<td class="text-left"><?php echo $income['company']; ?></td>
				<td class="text-right"><?php echo $income['quantity']; ?></td>
				<td class="text-right"><?php echo $income['gross_amount']; ?></td>
				<td class="text-right"><?php echo $income['commission']; ?></td>
				<td class="text-right"><?php echo $income['seller_amount']; ?></td>
				<td class="text-right">
					
					
			<?php $amt = number_format((float)$income['amounttopay1'], 2, '.', '');?>
			
			<?php if($amt > 0){ ?>
		
		<?php $customs = $income['order_product_id'].'#'.$income['seller_id']; ?>	
	
	    
		<form action="https://www.sandbox.paypal.com/cgi-bin/webscr" method="post" id="PayPalForm<?php echo $income['seller_id']; ?>" 
		name="PayPalForm<?php echo $income['seller_id']; ?>"  target="_top">
		
		<input type="hidden" name="cmd" value="_xclick">
		<input type="hidden" name="business" value="<?php echo $income['paypal_email']; ?>">
		<input type="hidden" name="amount" value="<?php echo $amt; ?>" id="amount<?php echo $income['seller_id']; ?>">
		<input type="hidden" name="item_name" value="<?php echo $income['company']; ?>">
		<input type="hidden" name="item_number" value="1">
		<input type="hidden" name="custom" value="<?php echo $customs; ?>" id="custom<?php echo $income['seller_id']; ?>">
		<input type="hidden" name="currency_code" value="<?php echo $currency;?>">
		<input type="hidden" name="cancel_return" value="<?php echo $cancelURL;?>">
		<input type="hidden" name="return" value="<?php echo $notify_url; ?>">
		<input type="hidden" name="notify_url" value="<?php echo $notify_url; ?>"> 
		<input type="button" value="Pay Seller" onclick="paypal<?php echo $income['seller_id']; ?>();" class="btn btn-primary"/> 
		</form>

		
		
	
<br/>

			<a href="<?php echo $income['href'];?>" class="btn btn-primary"><?php echo $button_addPayment; ?></a>
			
  <script>
  function paypal<?php echo $income['seller_id']; ?>(){
  
  document.PayPalForm<?php echo $income['seller_id']; ?>.submit();
    
	
	}
   </script>
   
   <?php } ?>
				</td>



				</tr>
				
				<tr>
		<td colspan="8">
		<table class="table table-bordered" id="<?php echo $income['seller_id'];?>" style="display:none;">
        <thead>
          <tr>
            <td class="text-right"><?php echo $column_order_id; ?></td>
            <td class="text-left"><?php echo $column_product_name; ?></td>
			<td class="text-left"><?php echo $column_date_added; ?></td>
			<td class="text-left"><?php echo $column_transaction_status; ?></td>
            <td class="text-right"><?php echo $column_unit_price; ?></td>
            <td class="text-right"><?php echo $column_quantity; ?></td>
			<td class="text-right"><?php echo $column_total; ?></td>
			<td class="text-right"><?php echo $column_commision; ?></td>
    		<td class="text-right"><?php echo $column_amount; ?></td>
          </tr>
        </thead>
        <tbody>
          <?php if ($income['sellerproducts']) { ?>
          <?php foreach ($income['sellerproducts'] as $order) { ?>
          <tr>
		    <td class="text-right"><a onclick="return showhideinnerdiv(<?php echo $income['seller_id'].$order['order_id'].$order['product_id'];?>);" id="showhideinner<?php echo $income['seller_id'].$order['order_id'].$order['product_id'];?>">Show</a> <?php echo $order['order_id']; ?></td>
			<td class="text-left"><?php echo $order['product_name']; ?></td>
            <td class="text-left"><?php echo $order['date']; ?></td>
            <td class="text-left">
			<?php foreach ($order_statuses as $order_status) { ?>
			<?php if ($order_status['order_status_id'] == $order['order_status']) { ?>
				<?php echo $order_status['name']; ?>
			<?php } ?>
			<?php } ?></td>
            <td class="text-right"><?php echo $order['price']; ?></td>
            <td class="text-right"><?php echo $order['quantity']; ?></td>
			<td class="text-right"><?php echo $order['total']; ?> <?php echo $order['com_ratio']; ?></td>
			<td class="text-right"><?php echo $order['commission']; ?></td>
			<td class="text-right"><?php echo $order['amount']; ?></td>
          </tr>
	  <tr>
		<td colspan="9">
		<table class="table table-bordered" style="display:none;" style="cursor:pointer;" id="<?php echo $income['seller_id'].$order['order_id'].$order['product_id'];?>" >
		<thead>
		  <tr>
		    <td class="text-left"><?php echo $column_buyer_id; ?></td>
		    <td class="text-left"><?php echo $column_buyer_name; ?></td>
		    <td class="text-left"><?php echo $column_buy_date; ?></td>
		    <td colspan="6"></td>
		  </tr>
		</thead>
		<tbody>
		<?php 
		if($order['customerinfo']) {?>
		  <tr>
			<td><?php echo $order['customerinfo']['customer_id']; ?></td>
			<td class="text-left"><?php echo $order['customerinfo']['firstname']." ".$order['customerinfo']['lastname']; ?></td>
			<td class="text-left"><?php echo $order['customerinfo']['date_added']; ?></td>
		  </tr>
		 <?php } ?>
		</table>
		</td>
	 </tr>
          <?php } ?>
          <?php } else { ?>
          <tr>
            <td class="center" colspan="10"><?php echo $text_no_results; ?></td>
          </tr>
          <?php } ?>
        </tbody>
      </table>
      </td>
      </tr>
              <?php } ?>
              <?php } else { ?>
              <tr>
                <td class="text-center" colspan="7"><?php echo $text_no_results; ?></td>
              </tr>
              <?php } ?>
            </tbody>
          </table>
		  
		  <br/>
		  <br/>
		  
		   <!--div class="pagination"><?php echo $pagination; ?></div-->
		   
		     
		

       
      </div>
    </div>
  </div>
  
  
 			 
		
   
   
		  <div class="container-fluid">
		  
		
		  
		
      <h1><?php echo $text_payment_history; ?></h1>
   
   
		  
        <div class="panel panel-default">
		
		<div class="panel-heading">
		
	
		
		</div>
		
		
		 <form method="post" enctype="multipart/form-data" id="form">

        <div class="table-responsive">
		
	  <table class="table table-bordered">
	  <thead>
          <tr>
		<td class="text-left"><?php echo $column_seller_id; ?></td>
		<td class="text-left"><?php echo $column_seller_name; ?></td>
		
		<td class="text-left">Payment Date</td>
		<td class="text-left">Payment Description</td>
		
		<td class="text-left"><?php echo $column_amount; ?></td>
          </tr>
        </thead>
<?php foreach ($oldincomes AS $income) { ?>
        <tr>
		<td><a style="cursor:pointer;" onclick="return showhidedivo(<?php echo $income['order_product_id'];?>);" 
				id="showhideo<?php echo $income['order_product_id'];?>"> Show</a> <?php echo $income['seller_id']; ?></td>
		<td><?php echo $income['company']; ?></td>
		<td><?php echo $income['payment_date']; ?></td>
		<td><?php echo $income['payment_description']; ?></td>
		<td><?php echo $income['seller_amount']; ?></td>
		</tr>
		
		
		<tr>
		<td colspan="8">
		<table class="table table-bordered" id="o<?php echo $income['order_product_id'];?>" style="display:none;">
        <thead>
          <tr>
            <td class="text-right"><?php echo $column_order_id; ?></td>
            <td class="text-left"><?php echo $column_product_name; ?></td>
			<td class="text-left"><?php echo $column_date_added; ?></td>
            
			<td class="text-right"><?php echo $column_total; ?></td>
			<td class="text-right"><?php echo $column_commision; ?></td>
    		<td class="text-right"><?php echo $column_amount; ?></td>
          </tr>
        </thead>
        <tbody>
          <?php if ($income['sellerproducts']) { ?>
          <?php foreach ($income['sellerproducts'] as $order) { ?>
          <tr>
		    <td class="text-right"><?php echo $order['order_id']; ?></td>
			<td class="text-left"><?php echo $order['product_name']; ?></td>
            <td class="text-left"><?php echo $order['date']; ?></td>
           
          
			<td class="text-right"><?php echo $order['total']; ?> <?php echo $order['com_ratio']; ?></td>
			<td class="text-right"><?php echo $order['commission']; ?></td>
			<td class="text-right"><?php echo $order['price']; ?></td>
          </tr>
		  <?php } ?>
          <?php } ?>
		 </tbody>
		 
		 </table>
		 </td>
		 </tr>
		  
		  
			<?php } ?>

      </table>
	  
	  </form>
	  
	  
	
        </div>
		
		 <div class="row">
          <div class="col-sm-6 text-left"><?php echo $pagination; ?></div>
          <div class="col-sm-6 text-right"><?php echo $results; ?></div>
        </div>
		
		
		
		</div>
		
		
        </div>
		
		
  
  <script type="text/javascript"><!--
function showhidediv(tableid){
	var showhide = $('#'+tableid).css('display');
	if(showhide == 'none'){
		$('#showhide'+tableid).html('Hide');
		$('#'+tableid).show(300);
	}
	else{
		$('#showhide'+tableid).html('Show');
		$('#'+tableid).hide(300);
	}
	return false;
}


function showhideinnerdiv(tableid){
	var showhide = $('#'+tableid).css('display');
	if(showhide == 'none'){
		$('#showhideinner'+tableid).html('Hide');
		$('#'+tableid).show(300);
	}
	else{
		$('#showhideinner'+tableid).html('Show');
		$('#'+tableid).hide(300);
	}
	return false;
}

function showhidedivo(tableid){
	var showhide = $('#o'+tableid).css('display');
	if(showhide == 'none'){
		$('#showhideo'+tableid).html('Hide');
		$('#o'+tableid).show(300);
	}
	else{
		$('#showhideo'+tableid).html('Show');
		$('#o'+tableid).hide(300);
	}
	return false;
}

function showhideinnerdivo(tableid){
	var showhide = $('#o'+tableid).css('display');
	if(showhide == 'none'){
		$('#showhideinnero'+tableid).html('Hide');
		$('#o'+tableid).show(300);
	}
	else{
		$('#showhideinnero'+tableid).html('Show');
		$('#o'+tableid).hide(300);
	}
	return false;
}

function filter() {
	url = 'index.php?route=report/seller_transaction&token=<?php echo $token; ?>';
	
	var filter_date_start = $('input[name=\'filter_date_start\']').attr('value');
	
	if (filter_date_start) {
		url += '&filter_date_start=' + encodeURIComponent(filter_date_start);
	}

	var filter_date_end = $('input[name=\'filter_date_end\']').attr('value');
	
	if (filter_date_end) {
		url += '&filter_date_end=' + encodeURIComponent(filter_date_end);
	}
		
	var filter_seller_group = $('select[name=\'filter_seller_group\']').attr('value');
	
	if (filter_seller_group) {
		url += '&filter_seller_group=' + encodeURIComponent(filter_seller_group);
	}
	
	var filter_paid_status = $('select[name=\'filter_paid_status\']').attr('value');
	
	if (filter_paid_status) {
		url += '&filter_paid_status=' + encodeURIComponent(filter_paid_status);
	}
	
	var filter_order_status_id = $('select[name=\'filter_order_status_id\']').attr('value');
	
	if (filter_order_status_id) {
		url += '&filter_order_status_id=' + encodeURIComponent(filter_order_status_id);
	}	

	location = url;
}
//--></script> 
<script type="text/javascript"><!--
function removeHistory(id) {
	$.ajax({
		url: 'index.php?route=report/seller_transaction/removeHistory&token=<?php echo $token; ?>&payment_id=' + id,
		dataType: 'json',
		beforeSend: function() {
			$('.success, .warning').remove();
			$('#history').before('<div class="attention"><img src="view/image/loading_1.gif" alt="" /> <?php echo $text_wait; ?></div>');
		},
		
		complete: function() {
			$('.attention').remove();
		},
		
		success: function(data) {
			$('#history_' + id).remove();
		}
			
	});
}
//--></script>


 <script type="text/javascript"><!--
$('#button-filter').on('click', function() {
	url = 'index.php?route=report/seller_transaction&token=<?php echo $token; ?>';
	
	var filter_date_start = $('input[name=\'filter_date_start\']').val();
	
	if (filter_date_start) {
		url += '&filter_date_start=' + encodeURIComponent(filter_date_start);
	}

	var filter_date_end = $('input[name=\'filter_date_end\']').val();
	
	if (filter_date_end) {
		url += '&filter_date_end=' + encodeURIComponent(filter_date_end);
	}
	
	

	location = url;
});
//--></script> 
  <script type="text/javascript"><!--
$('.date').datetimepicker({
	pickTime: false
});
//--></script>

 </div>
<?php echo $footer; ?>