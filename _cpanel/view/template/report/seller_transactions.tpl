<?php echo $header; ?><?php echo $column_left; ?>

<div id="content">
  <div class="page-header">
    <div class="container-fluid">
      <h1><?php echo $heading_title; ?></h1>
      <ul class="breadcrumb">
        <?php foreach ($breadcrumbs as $breadcrumb) { ?>
        <li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
        <?php } ?>
      </ul>
    </div>
  </div>
  <div class="container-fluid">
    <div class="panel panel-default">
      <div class="panel-heading">
        <h3 class="panel-title"><i class="fa fa-bar-chart"></i> <?php echo $heading_title; ?></h3>
      </div>
        <div class="table-responsive">
          <table class="table table-bordered">
            <thead>
              <tr>
                <td class="text-left"><?php echo $column_seller_id; ?></td>
                <td class="text-left"><?php echo $column_seller_name; ?></td>
                <td class="text-right"><?php echo $column_total; ?> Sale</td>
                <td class="text-right">Total <?php echo $column_commision; ?></td>
                <td class="text-right">Seller Cut</td>
              <td class="text-left"><?php echo $column_action; ?></td>
			 </tr>
            </thead>
            <tbody>
              <?php if ($incomes) { ?>
              <?php foreach ($incomes as $income) { 
	      $amt = number_format((float)$income['amounttopay1'], 2, '.', '');
	      if($amt > 0){  ?>
				<tr>
				<td class="text-left"><?php echo $income['seller_id']; ?></td>
				<td class="text-left"><?php echo $income['company']; ?></td>
				<td class="text-right"><?php echo $income['gross_amount']; ?></td>
				<td class="text-right"><?php echo $income['commission']; ?></td>
				<td class="text-right"><?php echo $income['seller_amount']; ?></td>
				<td class="text-left">
			<?php ?>
			
			<a href="<?php echo $income['href'];?>" target="_blank" class="btn btn-primary"><?php echo $button_addPayment; ?></a>
<a href="<?php echo $income['transaction'];?>" target="_blank" class="btn btn-primary">View Transactions</a>
				</td>
				
				</tr>
				
              <?php }}}else{ ?>
              <tr>
                <td class="text-center" colspan="7"><?php echo $text_no_results; ?></td>
              </tr>
              <?php } ?>
            </tbody>
          </table>
		  
		  <br/>
		  <br/>
		   <!--div class="pagination"><?php echo $pagination; ?></div-->
      </div>
    </div>
  </div>	 
        </div>
 </div>
<?php echo $footer; ?>