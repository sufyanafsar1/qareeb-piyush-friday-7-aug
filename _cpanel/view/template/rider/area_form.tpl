<?php echo $header; ?><?php echo $column_left; ?>
<div id="content">
  <div class="page-header">
    <div class="container-fluid">
      <div class="pull-right">
        <button type="button" id="btnArea" form="form-user" data-toggle="tooltip" title="<?php echo $button_save; ?>" class="btn btn-primary"><i class="fa fa-save"></i></button>
        <a href="<?php echo $cancel; ?>" data-toggle="tooltip" title="<?php echo $button_cancel; ?>" class="btn btn-default"><i class="fa fa-reply"></i></a></div>
      <h1><?php echo $heading_title; ?></h1>
      <ul class="breadcrumb">
        <?php foreach ($breadcrumbs as $breadcrumb) { ?>
        <li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
        <?php } ?>
      </ul>
    </div>
  </div>
  <div class="container-fluid">
    <?php if ($error_warning) { ?>
    <div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> <?php echo $error_warning; ?>
      <button type="button" class="close" data-dismiss="alert">&times;</button>
    </div>
    <?php } ?>
    <div class="panel panel-default">
      <div class="panel-heading">
        <h3 class="panel-title"><i class="fa fa-pencil"></i> <?php echo $text_form; ?></h3>
      </div>
      <div class="panel-body">
        <form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data" id="form-user" class="form-horizontal">
          <div class="form-group required">
            <label class="col-sm-2 control-label" for="input-username"><?php echo $entry_area; ?></label>
            <div class="col-sm-10">
              <input type="text" name="area" value="" placeholder="" id="input-area" class="form-control" />
              <?php if ($error_area) { ?>
              <div class="text-danger"><?php echo $error_area; ?></div>
              <?php } ?>
            </div>
          </div>
    
          <div class="form-group required">
            <label class="col-sm-2 control-label" for="input-firstname"><?php echo $entry_city; ?></label>
            <div class="col-sm-10">
              <input type="text" name="city" value="" placeholder="" id="input-city" class="form-control" />
              <?php if ($error_city) { ?>
              <div class="text-danger"><?php echo $error_city; ?></div>
              <?php } ?>
            </div>
          </div>
     
        </form>
      </div>
    </div>
  </div>
</div>

<script>
var baseUrl = "https://www.qareeb.com/index.php?route=rider/MainAPI/";
 //var baseUrl = "http://localhost/careebnew/index.php?route=rider/MainAPI/";
function validateField(fieldVal)
 {
    if(fieldVal !="")
    {
        return true;
    }
    else{
        return false;
    }
 }
$('#btnArea').on('click', function() {
   

	  
	     var rider_city = $("#input-city").val();
          var rider_area = $("#input-area").val(); 
        
	    if(!validateField(rider_area))
	    {
	         alert("Please enter valid Area"); 
	         return false; 
	    }
	    else if(!validateField(rider_city))
	    {
	      alert("Please enter valid City");
	      return false;
	    }
	    
        
        var req = {
                area: rider_area,
                city: rider_city
            };
            
	 	$.ajax({
                url: baseUrl + 'addArea',
                type: 'POST',              
                accepts: 'application/json',
                data: req,
                async: true,                
                success: function (response) {
                 
                    if (response == "success") {
                      window.location ="index.php?route=rider/rider/getAreas&token=<?php echo $token; ?>";
                        }
                },
                error: function (response) {
                    alert(response);
                }
            });
});

</script>


<?php echo $footer; ?> 