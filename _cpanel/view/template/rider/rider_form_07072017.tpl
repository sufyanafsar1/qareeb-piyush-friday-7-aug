<link rel="stylesheet" type="text/css" href="./jquery.datetimepicker.css"/>
<script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAUWE7HDZphNC3nd0VVVITuPiBYVDN7Upk&libraries=places"></script>
<?php echo $header; ?><?php echo $column_left; ?>
<div id="content">
  <div class="page-header">
    <div class="container-fluid">
      <div class="pull-right"><a href="<?php echo $cancel; ?>" class="btn btn-default"><i class="fa fa-reply"></i> <?php echo $button_cancel; ?></a></div>
      <h1><?php echo $heading_title; ?></h1>
      <ul class="breadcrumb">
        <?php foreach ($breadcrumbs as $breadcrumb) { ?>
        <li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
        <?php } ?>
      </ul>
    </div>
  </div>
  <div class="container-fluid">
    <div class="panel panel-default">
      <div class="panel-heading">
        <h3 class="panel-title"><i class="fa fa-pencil"></i> <?php echo $text_form; ?></h3>
      </div>
      <div class="panel-body">
        <form class="form-horizontal">
          <ul id="order" class="nav nav-tabs nav-justified">
            <li class="disabled active"><a href="#tab-rider" data-toggle="tab">1. <?php echo $tab_rider; ?></a></li>
            <li class="disabled"><a href="#tab-area" data-toggle="tab">2. <?php echo $tab_area; ?></a></li>
            <li class="disabled"><a href="#tab-availability" data-toggle="tab">3. <?php echo $tab_availability; ?></a></li>
          </ul>
          <div class="tab-content">
            <div class="tab-pane active" id="tab-rider">
                 <div class="form-group required">
                <label class="col-sm-2 control-label" for="input-email"><?php echo $entry_email; ?></label>
                <div class="col-sm-10">
                  <input type="text" name="email" value="<?php echo $email; ?>" id="input-email" class="form-control" />
                  <input type="hidden" name="rider_id" value="<?php echo $rider_id; ?>" id="rider_id" class="form-control" />
                </div>
              </div>
              <div class="form-group required">
                <label class="col-sm-2 control-label" for="input-firstname"><?php echo $entry_firstname; ?></label>
                <div class="col-sm-10">
                  <input type="text" name="name" value="<?php echo $name ?>" id="input-name" class="form-control" />
                </div>
              </div>
              <div class="form-group required">
                <label class="col-sm-2 control-label" for="input-password"><?php echo $entry_password; ?></label>
                <div class="col-sm-10">
                  <input type="password" name="password" value="<?php echo $password ?>" id="input-password" class="form-control" />
                </div>
              </div>      
              <div class="form-group required">
                <label class="col-sm-2 control-label" for="input-confirm_password"><?php echo $entry_confirm_password; ?></label>
                <div class="col-sm-10">
                  <input type="password" name="cPassword" value="" id="input-cPassword" class="form-control" />
                </div>
              </div>           
          
              <div class="form-group required">
                <label class="col-sm-2 control-label" for="input-contact"><?php echo $entry_telephone; ?></label>
                <div class="col-sm-10">
                  <input type="text" name="contact_no" value="<?php echo $contact_no ?>" id="input-telephone" class="form-control" />
                </div>
              </div>
              <div class="form-group">
                <label class="col-sm-2 control-label" for="input-license"><?php echo $entry_license; ?></label>
                <div class="col-sm-10">
                  <input type="text" name="license" value="<?php echo $license ?>" id="input-license" class="form-control" />
                </div>
              </div>
              <div class="form-group">
                <label class="col-sm-2 control-label" for="input-address"><?php echo $entry_address; ?></label>
                <div class="col-sm-10">
                  <input type="text" name="address" value="<?php echo $address ?>" id="input-address" class="form-control" />
                </div>
              </div>
           
             <!-- <div class="form-group">
                <label class="col-sm-2 control-label" for="input-city"><?php echo $entry_picture; ?></label>
                <div class="col-sm-10">
                  <input type="file" name="rider_pic" value="" id="input-picture" />
                </div>
              </div> -->
  
  
              <div class="text-right">
                <button type="button" id="button-rider" data-loading-text="<?php echo $text_loading; ?>" class="btn btn-primary"><i class="fa fa-arrow-right"></i> <?php echo $button_continue; ?></button>
              </div>
            </div>
            <div class="tab-pane" id="tab-area">
            
               <!-- <div class="text-right"> 
                <label for="locationTextField">Search Location</label>
                 <input id="locationTextField1111" type="text" size="50" />
                </div> -->
                <br /><br />
             <div class="form-group required">
                <label class="col-sm-2 control-label" for="input-area"><?php echo $entry_area; ?></label>
                <div class="col-sm-10">
                 <!-- <input type="text" name="area" value="" id="input-area" class="form-control" />-->
                   <input id="locationTextField" value="<?php echo $area_name; ?>" type="text" name="area"  class="form-control"/>
                </div>
              </div>
             <!-- <div class="form-group required">
                <label class="col-sm-2 control-label" for="input-cityname"><?php echo $entry_city; ?></label>
                <div class="col-sm-10">
                  <input type="text" name="cityName" value="" id="input-cityName" class="form-control" />
                </div>
              </div>
              <div class="form-group required">
                <label class="col-sm-2 control-label" for="input-country"><?php echo $entry_country; ?></label>
                <div class="col-sm-10">
                  <input type="text" name="country" value="" id="input-country" class="form-control" />
                </div>
              </div> -->
              <br />
              <div class="row">
                <div class="col-sm-6 text-left">
                  <button type="button" onclick="$('a[href=\'#tab-rider\']').tab('show');" class="btn btn-default"><i class="fa fa-arrow-left"></i> <?php echo $button_back; ?></button>
                </div>
                <div class="col-sm-6 text-right">
                  <button type="button" id="button-area" class="btn btn-primary"><i class="fa fa-arrow-right"></i> <?php echo $button_continue; ?></button>
                </div>
              </div>
            </div>
                        
            <div class="tab-pane" id="tab-availability">
            
              <div style="margin-left:150px;">
              <div class="well" style="margin-right:300px;">
              <div class="row">
             <div class="col-sm-6">
                 <div class="form-group">
                 <div class="input-wide">
                 <?php $monday = ($m ==1) ? "checked" : ""; ?>
                 <input type="checkbox" name="monday_checkBox" id="input_monday_checkBox" class="form-control" <?php echo $monday; ?>  />
                 </div>
                <label class="control-label" for="input-date-start">Monday Time In</label>
                <div>
                  <input type="text" name="monday_date_start" value="<?php echo $mti; ?>" placeholder="<?php echo $mti; ?>"  id="datetimepicker_mask" class="form-control" />
                </div>
              </div>
              
               <div class="form-group">
                <label class="control-label" for="input-date-start">Monday Time Out</label>
                <div>
                  <input type="text" name="monday_date_end" value="<?php echo $mto; ?>" placeholder="<?php echo $mto; ?>"  id="datetimepicker_mask1" class="form-control" />
                </div>
              </div>
              </div>
              </div>
              </div>
              </div>
              
              <div style="margin-left:150px;">
              <div class="well" style="margin-right:300px;">
              <div class="row">
             <div class="col-sm-6">
             
              <div class="form-group">
              <div class="input-wide">
              <?php $tuesday = ($t ==1) ? "checked" : ""; ?>
                 <input type="checkbox" name="tuesday_checkBox" id="input_tuesday_checkBox" class="form-control" <?php echo $tuesday; ?> />
                 </div>
                <label class="control-label" for="input-date-start">Tuesday Time In</label>
                <div>
                  <input type="text" name="tuesday_date_start" value="<?php echo $tti; ?>" placeholder="<?php echo $tti; ?>"  id="datetimepicker_mask2" class="form-control" />
                </div>
              </div>
              
              
              <div class="form-group">
                <label class="control-label" for="input-date-start">Tuesday Time Out</label>
                <div>
                  <input type="text" name="tuesday_date_end" value="<?php echo $tto; ?>" placeholder="<?php echo $tto; ?>"  id="datetimepicker_mask3" class="form-control" />
                </div>
              </div>
              
              </div></div></div></div>
              <div style="margin-left:150px;">
              <div class="well" style="margin-right:300px;">
              <div class="row">
             <div class="col-sm-6">
                <div class="form-group">
                <div class="input-wide">
                <?php $wednesday = ($w ==1) ? "checked" : ""; ?>
                 <input type="checkbox" name="wednesday_checkBox" id="input_wednesday_checkBox" class="form-control"  <?php echo $wednesday; ?> />
                 </div>
                <label class="control-label" for="input-date-start">Wednesday Time In</label>
                <div>
                  <input type="text" name="wednesday_date_start" value="<?php echo $wti; ?>" placeholder="<?php echo $wti; ?>" id="datetimepicker_mask4" class="form-control" />
                </div>
              </div>
              
                <div class="form-group">
                <label class="control-label" for="input-date-start">Wednesday Time Out</label>
                <div>
                  <input type="text" name="wednesday_date_end" value="<?php echo $wto; ?>" placeholder="<?php echo $wto; ?>" id="datetimepicker_mask5" class="form-control" />
                </div>
              </div>
                  </div></div></div></div>
                  <div style="margin-left:150px;">
                  <div class="well" style="margin-right:300px;">
              <div class="row">
             <div class="col-sm-6">
               <div class="form-group">
               <div class="input-wide">
               <?php $thursday = ($thu ==1) ? "checked" : ""; ?>
                 <input type="checkbox" name="Thursday_checkBox" id="input_thursday_checkBox" class="form-control" <?php echo $thursday; ?> />
                 </div>
                <label class="control-label" for="input-date-start">Thursday Time In</label>
                <div>
                  <input type="text" name="thursday_date_start" value="<?php echo $thti; ?>" placeholder="<?php echo $thti; ?>" id="datetimepicker_mask6" class="form-control" />
                </div>
              </div>
              
                <div class="form-group">
                <label class="control-label" for="input-date-start">Thursday Time Out</label>
                <div>
                  <input type="text" name="thursday_date_end" value="<?php echo $thto; ?>" placeholder="<?php echo $thto; ?>" id="datetimepicker_mask7" class="form-control" />
                </div>
              </div>
              
              </div></div></div></div>
              <div style="margin-left:150px;">
              <div class="well" style="margin-right:300px;">
              <div class="row">
             <div class="col-sm-6">
               <div class="form-group">
               <div class="input-wide">
               <?php $friday = ($f ==1) ? "checked" : ""; ?>
                 <input type="checkbox" name="friday_checkBox" id="input_friday_checkBox" class="form-control" <?php echo $friday; ?> />
                 </div>
                <label class="control-label" for="input-date-start">Friday Time In</label>
                <div>
                  <input type="text" name="friday_date_start" value="<?php echo $fti; ?>" placeholder="<?php echo $fti; ?>" id="datetimepicker_mask8" class="form-control" />
                </div>
              </div>
              
                <div class="form-group">
                <label class="control-label" for="input-date-start">Friday Time Out</label>
                <div>
                  <input type="text" name="friday_date_end" value="<?php echo $fto; ?>" placeholder="<?php echo $fto; ?>" id="datetimepicker_mask9" class="form-control" />
                </div>
              </div>
              </div></div></div></div>
              <div style="margin-left:150px;">
              <div class="well" style="margin-right:300px;">
              <div class="row">
             <div class="col-sm-6">
                <div class="form-group">
                <div class="input-wide">
                 <?php $satuarday = ($sat ==1) ? "checked" : ""; ?>
                 <input type="checkbox" name="satuarday_checkBox" id="input_satuarday_checkBox" class="form-control" <?php echo $satuarday; ?> />
                 </div>
                <label class="control-label" for="input-date-start">Satuarday Time In</label>
                <div>
                  <input type="text" name="satuarday_date_start" value="<?php echo $satti; ?>" placeholder="<?php echo $satti; ?>" id="datetimepicker_mask10" class="form-control" />
                </div>
              </div>
              
              <div class="form-group">
                <label class="control-label" for="input-date-start">Satuarday Time Out</label>
                <div>
                  <input type="text" name="satuarday_date_end" value="<?php echo $satto; ?>" placeholder="<?php echo $satto; ?>" id="datetimepicker_mask11" class="form-control" />
                </div>
              </div>
              
              </div></div></div></div>
              <div style="margin-left:150px;">
              <div class="well" style="margin-right:300px;">
              <div class="row">
             <div class="col-sm-6">
                <div class="form-group">
                <div class="input-wide">
                <?php $sunday = ($sun ==1) ? "checked" : ""; ?>
                 <input type="checkbox" name="sunday_checkBox" id="input_sunday_checkBox" class="form-control" <?php echo $sunday; ?> />
                 </div>
                <label class="control-label" for="input-date-start">Sunday Time In</label>
                <div>
                  <input type="text" name="sunday_date_start" value="<?php echo $sunti; ?>" placeholder="<?php echo $sunti; ?>" id="datetimepicker_mask12" class="form-control" />
                </div>
              </div>
              
              <div class="form-group">
                <label class="control-label" for="input-date-start">Sunday Time Out</label>
                <div>
                  <input type="text" name="sunday_date_end" value="<?php echo $sunto; ?>" placeholder="<?php echo $sunto; ?>" id="datetimepicker_mask13" class="form-control" />
                </div>
              </div>
              </div></div></div></div>
              <br/>
              <div class="row">
                <div class="col-sm-6 text-left">
                  <button type="button" onclick="$('a[href=\'#tab-area\']').tab('show');"  class="btn btn-default"><i class="fa fa-arrow-left"></i> <?php echo $button_back; ?></button>
                </div>
                <div class="col-sm-6 text-right">
                  <button type="button" id="button-availability" class="btn btn-primary"><i class="fa fa-arrow-right"></i> <?php echo $button_continue; ?></button>
                </div>
              </div>
            </div>
          </div>
        </form>
      </div>
    </div>
  </div>
 

  <script type="text/javascript"><!--


            function init() {
                var input = document.getElementById('locationTextField');
                var autocomplete = new google.maps.places.Autocomplete(input);
            }

            google.maps.event.addDomListener(window, 'load', init);
    


$('#datetimepicker_format').datetimepicker({value:'2015/04/15 05:03', format: $("#datetimepicker_format_value").val()});
console.log($('#datetimepicker_format').datetimepicker('getValue'));

$("#datetimepicker_format_change").on("click", function(e){
	$("#datetimepicker_format").data('xdsoft_datetimepicker').setOptions({format: $("#datetimepicker_format_value").val()});
});
$("#datetimepicker_format_locale").on("change", function(e){
	$.datetimepicker.setLocale($(e.currentTarget).val());
});

$('#datetimepicker').datetimepicker({
dayOfWeekStart : 1,
lang:'en',
disabledDates:['1986/01/08','1986/01/09','1986/01/10'],
startDate:	'1986/01/05'
});
$('#datetimepicker').datetimepicker({value:'2015/04/15 05:03',step:10});

$('.some_class').datetimepicker();

$('#default_datetimepicker').datetimepicker({
	formatTime:'H:i',
	formatDate:'d.m.Y',
	//defaultDate:'8.12.1986', // it's my birthday
	defaultDate:'+03.01.1970', // it's my birthday
	defaultTime:'10:00',
	timepickerScrollbar:false
});

$('#datetimepicker10').datetimepicker({
	step:5,
	inline:true
});
$('#datetimepicker_mask').datetimepicker({
	pickDate: false
    
});
$('#datetimepicker_mask1').datetimepicker({
pickDate: false
});
$('#datetimepicker_mask2').datetimepicker({
	pickDate: false
});
$('#datetimepicker_mask3').datetimepicker({
	pickDate: false
});
$('#datetimepicker_mask4').datetimepicker({
	pickDate: false
});
$('#datetimepicker_mask5').datetimepicker({
pickDate: false
});
$('#datetimepicker_mask6').datetimepicker({
	pickDate: false
});
$('#datetimepicker_mask7').datetimepicker({
	pickDate: false
});
$('#datetimepicker_mask8').datetimepicker({
	pickDate: false
});
$('#datetimepicker_mask9').datetimepicker({
	pickDate: false
});
$('#datetimepicker_mask10').datetimepicker({
	pickDate: false
});
$('#datetimepicker_mask11').datetimepicker({
	pickDate: false
});
$('#datetimepicker_mask12').datetimepicker({
	pickDate: false
});
$('#datetimepicker_mask13').datetimepicker({
	pickDate: false
});

$('#datetimepicker1').datetimepicker({
	datepicker:false,
	format:'H:i',
	step:5
});
$('#datetimepicker2').datetimepicker({
	yearOffset:222,
	lang:'ch',
	timepicker:false,
	format:'d/m/Y',
	formatDate:'Y/m/d',
	minDate:'-1970/01/02', // yesterday is minimum date
	maxDate:'+1970/01/02' // and tommorow is maximum date calendar
});
$('#datetimepicker3').datetimepicker({
	inline:true
});
$('#datetimepicker4').datetimepicker();
$('#open').click(function(){
	$('#datetimepicker4').datetimepicker('show');
});
$('#close').click(function(){
	$('#datetimepicker4').datetimepicker('hide');
});
$('#reset').click(function(){
	$('#datetimepicker4').datetimepicker('reset');
});
$('#datetimepicker5').datetimepicker({
	datepicker:false,
	allowTimes:['12:00','13:00','15:00','17:00','17:05','17:20','19:00','20:00'],
	step:5
});
$('#datetimepicker6').datetimepicker();
$('#destroy').click(function(){
	if( $('#datetimepicker6').data('xdsoft_datetimepicker') ){
		$('#datetimepicker6').datetimepicker('destroy');
		this.value = 'create';
	}else{
		$('#datetimepicker6').datetimepicker();
		this.value = 'destroy';
	}
});
var logic = function( currentDateTime ){
	if (currentDateTime && currentDateTime.getDay() == 6){
		this.setOptions({
			minTime:'11:00'
		});
	}else
		this.setOptions({
			minTime:'8:00'
		});
};
$('#datetimepicker7').datetimepicker({
	onChangeDateTime:logic,
	onShow:logic
});
$('#datetimepicker8').datetimepicker({
	onGenerate:function( ct ){
		$(this).find('.xdsoft_date')
			.toggleClass('xdsoft_disabled');
	},
	minDate:'-1970/01/2',
	maxDate:'+1970/01/2',
	timepicker:false
});
$('#datetimepicker9').datetimepicker({
	onGenerate:function( ct ){
		$(this).find('.xdsoft_date.xdsoft_weekend')
			.addClass('xdsoft_disabled');
	},
	weekends:['01.01.2014','02.01.2014','03.01.2014','04.01.2014','05.01.2014','06.01.2014'],
	timepicker:false
});
var dateToDisable = new Date();
	dateToDisable.setDate(dateToDisable.getDate() + 2);
$('#datetimepicker11').datetimepicker({
	beforeShowDay: function(date) {
		if (date.getMonth() == dateToDisable.getMonth() && date.getDate() == dateToDisable.getDate()) {
			return [false, ""]
		}

		return [true, ""];
	}
});
$('#datetimepicker12').datetimepicker({
	beforeShowDay: function(date) {
		if (date.getMonth() == dateToDisable.getMonth() && date.getDate() == dateToDisable.getDate()) {
			return [true, "custom-date-style"];
		}

		return [true, ""];
	}
});
$('#datetimepicker_dark').datetimepicker({theme:'dark'})



function validateField(fieldVal)
 {
    if(fieldVal !="")
    {
        return true;
    }
    else{
        return false;
    }
 }
 var riderId;
 var areaId;
 var baseUrl = "https://careeb.com/index.php?route=rider/MainAPI/";
 //var baseUrl = "http://localhost/careeb_live/index.php?route=rider/MainAPI/";
 
 getAreasList();
 function getAreasList()
{
    
   
    	 	$.ajax({
                url: baseUrl + 'getAreas',
                type: 'Get',                
                dataType: 'json',
                success: function (response) {
                    var areaDropDown = "<select name='area' id='riderArea' class='form-control'>";
                   $.each(response, function(index) { 
                    
                   areaDropDown +="<option value='"+ response[index].area_id +"'>"+ response[index].area_name +","+ response[index].city +" </option>";
              
                
                });
                areaDropDown += "</select>";  
                
              $("#areaDDL").html(areaDropDown);
                },
                error: function (response) {
                    alert(response);
                }
            });

   
}
$('#button-rider').on('click', function() {

	     var rider_email = $("#input-email").val();
	     var rider_name = $("#input-name").val();
	     var rider_password = $("#input-password").val();
	     var cPassword = $("#input-cPassword").val();
	     var rider_mobile = $("#input-telephone").val();
	     var rider_license = $("#input-license").val(); 
	     var rider_address = $("#input-address").val(); 
	     var rider_city = $("#input-city").val(); 
	     
        
	    if(!validateField(rider_email))
	    {
	         alert("Please enter valid email"); 
	         return false; 
	    }
	    else if(!validateField(rider_name))
	    {
	      alert("Please enter valid name");
	      return false;
	    }
	    else if(!validateField(rider_password))
	    {
	      alert("Please enter valid password");
	      return false;
	    }
	    else if(!validateField(cPassword))
	    {
	      alert("Please enter valid confirm password");
	      return false;
	    }
	    else if(!validateField(rider_mobile))
	    {
	      alert("Please enter valid mobile");
	      return false;
	    }
	   
	    else if(!validateField(rider_city))
	    {
	      alert("Please enter valid city");
	      return false;
	    }
	    else if(rider_password != cPassword)
	    {
	     alert("Both passwords do not match");
	      return false;
	    }
        
        
        var req = {
                email: rider_email,
                name: rider_name,
                password: rider_password,
                contact_no: rider_mobile,
                address: rider_address,
                license: rider_license,
                is_active: 1,
                rider_status: 1,
                is_online: 0

            };
            
	 	$.ajax({
                url: baseUrl,
                type: 'POST',              
                accepts: 'application/json',
                data: req,
                async: true,
                dataType: 'json',
                success: function (response) {
                 
                    if (!(response == null)) {
                      
                        riderId =response.userID;
                        if(riderId > 0)
                        {
                        $('a[href=\'#tab-area\']').tab('show'); 
                        }
                        }
                },
                error: function (response) {
                    alert(response);
                }
            });
});


$('#button-area').on('click', function() {
      //  var areaId = $("#riderArea option:selected").val();
       //url: baseUrl + 'addRiderArea&rider_id='+riderId+'&area_id='+areaId,
      var area = $("#locationTextField").val();	   
   
       
       if(riderId ==undefined)
       {
        riderId = $("#rider_id").val();
       }
     
       
        $.ajax({
            
                url: baseUrl + 'addAreaV2&area='+area,
                type: 'GET',
                accepts: 'application/json',
                async: true,
                success: function (response) {
                   
        if (parseInt(response) > 0) {
                        
            $.ajax({
            
                url: baseUrl + 'addRiderArea&rider_id='+riderId+'&area_id='+parseInt(response),
                type: 'GET',
                accepts: 'application/json',
                async: true,
                success: function (result) {
                    
                    if (result ="success") {
                        
                        $('a[href=\'#tab-availability\']').tab('show'); 
                        }
                },
                error: function (result) {
                    alert(result);
                }
            });
                        }
                },
                error: function (response) {
                    alert(response);
                }
            });
       
       

	
});

$('#button-availability').on('click', function() {
        
       
       if(riderId ==undefined)
       {
        riderId = $("#rider_id").val();
       }
   
        var monTimeIn = $("#datetimepicker_mask").val();
  	    var monTimeOut = $("#datetimepicker_mask1").val();
  	    var tueTimeIn = $("#datetimepicker_mask2").val();
  	    var tueTimeOut = $("#datetimepicker_mask3").val();
  	    var wedTimeIn = $("#datetimepicker_mask4").val();
  	    var wedTimeOut = $("#datetimepicker_mask5").val();
  	    var thuTimeIn = $("#datetimepicker_mask6").val();
  	    var thuTimeOut = $("#datetimepicker_mask7").val();
  	    var friTimeIn = $("#datetimepicker_mask8").val();
  	    var friTimeOut = $("#datetimepicker_mask9").val();
  	    var satTimeIn = $("#datetimepicker_mask10").val();
  	    var satTimeOut = $("#datetimepicker_mask11").val();
  	    var sunTimeIn = $("#datetimepicker_mask12").val();
  	    var sunTimeOut = $("#datetimepicker_mask13").val();
        var mondayCheckBox= document.getElementById("input_monday_checkBox").checked;
        var tuesdayCheckBox= document.getElementById("input_tuesday_checkBox").checked;
        var wednesdayCheckBox= document.getElementById("input_wednesday_checkBox").checked;
        var thursdayCheckBox= document.getElementById("input_thursday_checkBox").checked;
        var fridayCheckBox= document.getElementById("input_friday_checkBox").checked;
        var satuardayCheckBox= document.getElementById("input_satuarday_checkBox").checked;
        var sundayCheckBox= document.getElementById("input_sunday_checkBox").checked;
        
        
        var parm = {
                rider_id: riderId,
                saturday: satuardayCheckBox,
                sat_time_in: satTimeIn,
                sat_time_out: satTimeOut,
                sunday: sundayCheckBox,
                sun_time_in: sunTimeIn,
                sun_time_out: sunTimeOut,
                monday: mondayCheckBox,
                mon_time_in: monTimeIn,
                mon_time_out: monTimeOut,
                tuesday: tuesdayCheckBox,
                tue_time_in: tueTimeIn,
                tue_time_out: tueTimeOut,
                wednesday: wednesdayCheckBox,
                wed_time_in: wedTimeIn,
                wed_time_out: wedTimeOut,
                thursday: thursdayCheckBox,
                thu_time_in: thuTimeIn,
                thu_time_out: thuTimeOut,
                friday: fridayCheckBox,
                fri_time_in: friTimeIn,
                fri_time_out: friTimeOut,

            };
            
          $.ajax({
                url: baseUrl + 'updateRiderAvailability',
                type: 'POST',               
                accepts: 'application/json',
                data:parm,
                async: true,
                success: function (response) {                   
                    if (response ="success") {                       
                       window.location ="index.php?route=rider/rider&token=<?php echo $token; ?>";
                        }
                },
                error: function (response) {
                    alert(response);                    
                }
            });
	
});



$('.date').datetimepicker({
	pickTime: false
});

$('.datetime').datetimepicker({
	pickDate: true,
	pickTime: true
});

$('.time').datetimepicker({
	pickDate: false
});
//--></script>
</div>
<?php echo $footer; ?>
