<?php echo $header; ?><?php echo $column_left; ?>
<div id="content">
  <div class="page-header">
    <div class="container-fluid">
      <div class="pull-right">
        
        
        <a href="<?php echo $add; ?>" data-toggle="tooltip" title="<?php echo $button_add; ?>" class="btn btn-primary"><i class="fa fa-plus"></i></a></div>
      <h1><?php echo $heading_title; ?></h1>
      <ul class="breadcrumb">
        <?php foreach ($breadcrumbs as $breadcrumb) { ?>
        <li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
        <?php } ?>
      </ul>
    </div>
  </div>
  <div class="container-fluid">
    <div class="panel panel-default">
      <div class="panel-heading">
        <h3 class="panel-title"><i class="fa fa-list"></i> <?php echo $text_list; ?></h3>
      </div>
      <div class="panel-body">

        <form method="post" enctype="multipart/form-data" target="_blank" id="form-order">
          <div class="table-responsive">
            <table class="table table-bordered table-hover">
              <thead>
                <tr>
                <td class="text-right">Name</td>
                <td class="text-left">email</td>
                <td class="text-right">Contact #</td>
                <td class="text-right">Area</td>                
                <td class="text-right">License #</td>
                <td class="text-right">Address </td>
                <td class="text-right"><?php echo $column_action; ?></td> 
                </tr>
              </thead>
              <tbody>
                <?php  if ($riders) { ?>
                <?php foreach($riders as $rider) { ?>
                <tr>
                  
                  <td class="text-right"><?php echo $rider['name']; ?></td>
                  <td class="text-left"><?php echo $rider['email']; ?></td>                 
                  <td class="text-right"><?php echo $rider['contact_no']; ?></td>
                  <td class="text-right"><?php echo $rider['area_name']; ?></td>                  
                  <td class="text-left"><?php echo $rider['license']; ?></td>
                  <td class="text-left"><?php echo $rider['address']; ?></td>
                  <td class="text-right" >
                  <a href="https://careeb.com/admin/index.php?route=rider/rider/edit&amp;token=<?php echo $token; ?>&amp;rider_id=<?php echo $rider['rider_id'] ?>" data-toggle="tooltip" title="" class="btn btn-primary" data-original-title="Edit"><i class="fa fa-pencil"></i></a>
                  <button type="button" onclick= "deleteRider(<?php echo $rider['rider_id'] ?>)" data-loading-text="Loading..." data-toggle="tooltip" title="" class="btn btn-danger" data-original-title="Delete"><i class="fa fa-trash-o"></i></button></td> 
                </tr>
               <?php } ?>
                <?php } else { ?>
                <tr>
                  <td class="text-center" colspan="8"><?php echo $text_no_results; ?></td>
                </tr>
                <?php } ?>
              </tbody>
            </table>
          </div>
        </form>
        <div class="row">
          <div class="col-sm-6 text-left"><?php echo $pagination; ?></div>
          <div class="col-sm-6 text-right"><?php echo $results; ?></div>
        </div>
      </div>
    </div>
  </div>

  <script src="view/javascript/jquery/datetimepicker/bootstrap-datetimepicker.min.js" type="text/javascript"></script>
  <link href="view/javascript/jquery/datetimepicker/bootstrap-datetimepicker.min.css" type="text/css" rel="stylesheet" media="screen" />
  <script type="text/javascript"><!--
$('.date').datetimepicker({
	pickTime: false
});

var baseUrl = "https://careeb.com/index.php?route=rider/MainAPI/";
 //var baseUrl = "http://localhost/careeb_live/index.php?route=rider/MainAPI/";
function deleteRider(riderId)
{
    
    var req = {
                UserID: riderId,
                status: 0
            };
    
     $.ajax({
                url: baseUrl + 'deleteRider',
                type: 'GET',                
                accepts: 'application/json',
                data: req,
                async: true,
                dataType: 'json',
                success: function (response) {
                   
                    if (!(response == null || response.toString() == "")) {
                         alert("Rider deleted successfully!");
                         location.reload();
                        }
                },
                error: function (response) {
                    alert("Error Occured");
                }
            });
}
//--></script></div>
<?php echo $footer; ?>
