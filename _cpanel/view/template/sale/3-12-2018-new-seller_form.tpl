<style>
html,
body,
#googleMap,.googleMap {
  height: 50%;
  width: 100%;
  margin: 0px;
  padding: 0px
}
.gm-fullscreen-control{ display:none;}
.controls {
        margin-top: 16px;
        border: 1px solid transparent;
        border-radius: 2px 0 0 2px;
        box-sizing: border-box;
        -moz-box-sizing: border-box;
        height: 32px;
        outline: none;
        box-shadow: 0 2px 6px rgba(0, 0, 0, 0.3);
      }

      #pac-input {
        background-color: #fff;
        font-family: Roboto;
        font-size: 15px;
        font-weight: 300;
        margin-left: 12px;
        padding: 0 11px 0 13px;
        text-overflow: ellipsis;
        width: 400px;
      }

      #pac-input:focus {
        border-color: #4d90fe;
      }

      .pac-container {
        font-family: Roboto;
      }

      #type-selector {
        color: #fff;
        background-color: #4d90fe;
        padding: 5px 11px 0px 11px;
      }

      #type-selector label {
        font-family: Roboto;
        font-size: 13px;
        font-weight: 300;
      }

    </style>


<?php echo $header; ?><?php echo $column_left; ?>
<div id="content">
  <div class="page-header">
    <div class="container-fluid">
      <div class="pull-right">
        <button type="submit" form="form-customer" data-toggle="tooltip" title="<?php echo $button_save; ?>" class="btn btn-primary"><i class="fa fa-save"></i></button>
        <a href="<?php echo $cancel; ?>" data-toggle="tooltip" title="<?php echo $button_cancel; ?>" class="btn btn-default"><i class="fa fa-reply"></i></a></div>
      <h1><?php echo $heading_title; ?></h1>
      <ul class="breadcrumb">
        <?php foreach ($breadcrumbs as $breadcrumb) { ?>
        <li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
        <?php } ?>
      </ul>
    </div>
  </div>
  <div class="container-fluid">
    <?php if ($error_warning) { ?>
    <div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> <?php echo $error_warning; ?>
      <button type="button" class="close" data-dismiss="alert">&times;</button>
    </div>
    <?php } ?>
    <div class="panel panel-default">
      <div class="panel-heading">
        <h3 class="panel-title"><i class="fa fa-pencil"></i> <?php echo $heading_title; ?></h3>
      </div>
      <div class="panel-body">
        <form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data" id="form-customer" class="form-horizontal">
          <ul class="nav nav-tabs">
            <li class="active"><a href="#tab-general" data-toggle="tab"><?php echo $tab_general; ?></a></li>
            
            <li><a href="#tab-time" data-toggle="tab">Delivery Settings</a></li>
            
            <li><a href="#tab-history" data-toggle="tab">Payment History</a></li>
           
          </ul>
          <div class="tab-content">
            <div class="tab-pane active" id="tab-general">
              <div class="row">
                <div class="col-sm-2">
                  <ul class="nav nav-pills nav-stacked" id="address">
                    <li class="active"><a href="#tab-customer" data-toggle="tab"><?php echo $tab_general; ?></a></li>
                    <?php $address_row = 1; ?>
                    <?php foreach ($addresses as $address) { ?>
                    <li><a href="#tab-address<?php echo $address_row; ?>" data-toggle="tab"><i class="fa fa-minus-circle" onclick="$('#address a:first').tab('show'); $('#address a[href=\'#tab-address<?php echo $address_row; ?>\']').parent().remove(); $('#tab-address<?php echo $address_row; ?>').remove();"></i> <?php echo $tab_address . ' ' . $address_row; ?></a></li>
                    <?php $address_row++; ?>
                    <?php } ?>
                    <li id="address-add"><a onclick="addAddress();"><i class="fa fa-plus-circle"></i> <?php echo $button_add_address; ?></a></li>
				  
				 </ul>
                </div>
                <div class="col-sm-10">
                  <div class="tab-content">
                    <div class="tab-pane active" id="tab-customer">
                      
                      <div class="form-group required">
                        <label class="col-sm-2 control-label" for="input-firstname"><?php echo $entry_firstname; ?></label>
                        <div class="col-sm-10">
                          <input type="text" name="firstname" value="<?php echo $firstname; ?>" placeholder="<?php echo $entry_firstname; ?>" id="input-firstname" class="form-control" />
                          <?php if ($error_firstname) { ?>
                          <div class="text-danger"><?php echo $error_firstname; ?></div>
                          <?php } ?>
                        </div>
                      </div>
                      <div class="form-group required">
                        <label class="col-sm-2 control-label" for="input-lastname"><?php echo $entry_lastname; ?></label>
                        <div class="col-sm-10">
                          <input type="text" name="lastname" value="<?php echo $lastname; ?>" placeholder="<?php echo $entry_lastname; ?>" id="input-lastname" class="form-control" />
                          <?php if ($error_lastname) { ?>
                          <div class="text-danger"><?php echo $error_lastname; ?></div>
                          <?php } ?>
                        </div>
                      </div>
                      <div class="form-group required">
                        <label class="col-sm-2 control-label" for="input-email"><?php echo $entry_email; ?></label>
                        <div class="col-sm-10">
                          <input type="text" name="email" value="<?php echo $email; ?>" placeholder="<?php echo $entry_email; ?>" id="input-email" class="form-control" />
                          <?php if ($error_email) { ?>
                          <div class="text-danger"><?php echo $error_email; ?></div>
                          <?php  } ?>
                        </div>
                      </div>
                      <div class="form-group required">
                        <label class="col-sm-2 control-label" for="input-telephone"><?php echo $entry_telephone; ?></label>
                        <div class="col-sm-10">
                          <input type="text" name="telephone" value="<?php echo $telephone; ?>" placeholder="<?php echo $entry_telephone; ?>" id="input-telephone" class="form-control" />
                          <?php if ($error_telephone) { ?>
                          <div class="text-danger"><?php echo $error_telephone; ?></div>
                          <?php  } ?>
                        </div>
                      </div>
                      <div class="form-group" style="display:none">
                        <label class="col-sm-2 control-label" for="input-fax"><?php echo $entry_fax; ?></label>
                        <div class="col-sm-10">
                          <input type="text" name="fax" value="<?php echo $fax; ?>" placeholder="<?php echo $entry_fax; ?>" id="input-fax" class="form-control" />
                        </div>
                      </div>
                    
                      <div class="form-group required">
                        <label class="col-sm-2 control-label" for="input-password"><?php echo $entry_password; ?></label>
                        <div class="col-sm-10">
                          <input type="password" name="password" value="<?php echo $password; ?>" placeholder="<?php echo $entry_password; ?>" id="input-password" class="form-control" autocomplete="off" />
                          <?php if ($error_password) { ?>
                          <div class="text-danger"><?php echo $error_password; ?></div>
                          <?php  } ?>
                        </div>
                      </div>
                      
                      <div class="form-group required">
                        <label class="col-sm-2 control-label" for="input-newsletter"><?php echo $entry_username; ?></label>
                        <div class="col-sm-10">
                         <input type="text" name="username" placeholder="<?php echo $entry_username; ?>" value="<?php echo $username; ?>" class="form-control"/>
                  <?php if ($error_username12) { ?>
                  <div class="text-danger"><?php echo $error_username12; ?></div>
                  <?php } ?>
                        </div>
                      </div>
					  
					  
				<div class="form-group">
                        <label class="col-sm-2 control-label" for="input-tin_no">Tin No/Vat no.</label>
                        <div class="col-sm-10">
                         <input type="text" name="tin_no" placeholder="tin_no" value="<?php echo $tin_no; ?>" class="form-control"/>
                  
                        </div>
                      </div>	  
					  
					  
					  
					  
					  
					  
					  
					  
					  
					  
                      <div class="form-group">
                        <label class="col-sm-2 control-label" for="input-status"><?php echo $entry_status; ?></label>
                        <div class="col-sm-10">
                          <select name="status" id="input-status" class="form-control">
                            <?php if ($status) { ?>
                            <option value="1" selected="selected"><?php echo $text_enabled; ?></option>
                            <option value="0"><?php echo $text_disabled; ?></option>
                            <?php } else { ?>
                            <option value="1"><?php echo $text_enabled; ?></option>
                            <option value="0" selected="selected"><?php echo $text_disabled; ?></option>
                            <?php } ?>
                          </select>
                        </div>
                      </div>
					  
					   <div class="form-group required">
                        <label class="col-sm-2 control-label" for="input-comm"><?php echo $entry_commission; ?></label>
                        <div class="col-sm-10">
                         <select name="commission_id" id="input-comm" class="form-control">
                    <option value=""><?php echo $text_select; ?></option>
                    <?php foreach ($commissions as $commission) { ?>
                    <?php if ($commission['commission_id'] == $commission_id) { ?>
                    <option value="<?php echo $commission['commission_id']; ?>" selected="selected"><?php echo $commission['commission_name']; ?></option>
                    <?php } else { ?>
                    <option value="<?php echo $commission['commission_id']; ?>"><?php echo $commission['commission_name']; ?></option>
                    <?php } ?>
                    <?php } ?>
                  </select>
                        </div>
                      </div>
					<?php if($seller_id > 0) {?>
					  <div class="form-group">
                        <label class="col-sm-2 control-label" for="input-image"><?php echo $entry_image; ?></label>
                        <div class="col-sm-10">       
								<div id="option-1" class="option">
								<img src="<?php echo $thumb; ?>" alt="" id="thumb" width="100" height="100"/><br/>
								<a id="image" class="btn btn-primary">Upload</a>
								<input type="hidden" name="image"  value="<?php echo $image; ?>"  />
								<div id="option-2"></div>
								</div>
                        </div>
                      </div>
<script type="text/javascript" src="view/javascript/jquery/ajaxupload.js"></script>
<script type="text/javascript"><!--
					new AjaxUpload('#image', {
					action: 'index.php?route=sale/seller/upload&token=<?php echo $token; ?>&seller_id=' + <?php echo $seller_id; ?>,
					name: 'file',
					autoSubmit: true,
					responseType: 'json',
					onSubmit: function(file, extension) {
					$('#image').after('<img src="view/image/loading.gif" class="loading" style="padding-left: 5px;" />');
					},
					onComplete: function(file, json) {
					$('.error').remove();

					if (json.success) {
					alert(json.success);			
					$("input[name='image']").attr('value', json.foldername+json.file);
					$('#thumb').attr('src','../image/'+json.foldername+json.file);
					}

					if (json.error) {
					$('#option-2').html('<span class="text-danger">' + json.error + '</span>');
					}

					$('.loading').remove();	
					}
					});
					//--></script>

                                            <div class="form-group">
                        <label class="col-sm-2 control-label" for="input-banner">banner</label>
                        <div class="col-sm-10">       
								<div id="option-1" class="option">
								<img src="<?php echo $bannerthumb; ?>" alt="" id="bannerthumb" width="100" height="100"/><br/>
								<a id="banner" class="btn btn-primary">Upload</a>
								<input type="hidden" name="banner"  value="<?php echo $banner; ?>"  />
								<div id="option-22"></div>
								</div>
                        </div>
                      </div>
<script type="text/javascript"><!--
					new AjaxUpload('#banner', {
					action: 'index.php?route=sale/seller/upload&token=<?php echo $token; ?>&seller_id=' + <?php echo $seller_id; ?>,
					name: 'file',
					autoSubmit: true,
					responseType: 'json',
					onSubmit: function(file, extension) {
					$('#banner').after('<img src="view/image/loading.gif" class="loading" style="padding-left: 5px;" />');
					},
					onComplete: function(file, json) {
					$('.error').remove();

					if (json.success) {
					alert(json.success);			
					$("input[name='banner']").attr('value', json.foldername+json.file);
					$('#bannerthumb').attr('src','../image/'+json.foldername+json.file);
					}

					if (json.error) {
					$('#option-22').html('<span class="text-danger">' + json.error + '</span>');
					}

					$('.loading').remove();	
					}
					});
					//--></script>   
    
    
    
					  <?php } ?>




					  
					  <div class="form-group">
                        <label class="col-sm-2 control-label" for="input-status"><?php echo $entry_aboutus; ?></label>
                        <div class="col-sm-10">
                          <textarea name="aboutus" id="aboutus"><?php echo $aboutus; ?></textarea>
			
            
                        </div>
                      </div>
                      
                      
					  
                    </div>
      
                <!----- Address Form ------------>
                <?php $address_row = 1; ?>
                <?php foreach ($addresses as $address) {  ?>
                    <div  class="tab-pane" id="tab-address<?php echo $address_row; ?>">
                      <input type="hidden" name="address[<?php echo $address_row; ?>][address_id]" value="<?php echo  $address['address_id']; ?>" />
                     <input id="pac-input-die" class="controls" type="text" placeholder="Search Box"/>
	                 <div class="googleMap"  id="googleMap<?php echo $address_row; ?>"></div> <br/>
                      <div class="form-group required" style="display:none;">
                        <label class="col-sm-2 control-label" for="input-firstname<?php echo $address_row; ?>"><?php echo $entry_firstname; ?></label>
                        <div class="col-sm-10">
						
                          <input type="text" name="address[<?php echo $address_row; ?>][firstname]" value="<?php echo $address['firstname']; ?>"
						  placeholder="<?php echo $entry_firstname; ?>" id="input-firstname<?php echo $address_row; ?>" class="form-control" />
                          <?php if (isset($error_address_firstname[$address_row])) { ?>
                  <span class="text-danger"><?php echo $error_address_firstname[$address_row]; ?></span>
                  <?php } ?>
                        </div>
                      </div>
                      <div class="form-group required" style="display:none;">
                        <label class="col-sm-2 control-label" for="input-lastname<?php echo $address_row; ?>"><?php echo $entry_lastname; ?></label>
                        <div class="col-sm-10">
                          <input type="text" name="address[<?php echo $address_row; ?>][lastname]"  class="form-control"
						  
						   placeholder="<?php echo $entry_lastname; ?>"  value="<?php echo $address['lastname']; ?>" />
                  <?php if (isset($error_address_lastname[$address_row])) { ?>
                  <span class="text-danger"><?php echo $error_address_lastname[$address_row]; ?></span>
                  <?php } ?>
                        </div>
                      </div>
                      
                        
                      <div class="form-group required"  style="display:none;">
                            <label class="col-sm-2 control-label" for="input-lat<?php echo $address_row; ?>">Latitude</label>
                            <div class="col-sm-10">
                                <input type="text" name="address[<?php echo $address_row; ?>][latitude]" id="input-lat<?php echo $address_row; ?>"  class="form-control" placeholder="Latitude" value="<?php echo $address['latitude']; ?>" />
                                  
                            </div>
                        </div> 
                       	  <div class="form-group required"   style="display:none;">
                            <label class="col-sm-2 control-label" for="input-long<?php echo $address_row; ?>">Longitude</label>
                            <div class="col-sm-10">
                                <input type="text" name="address[<?php echo $address_row; ?>][longitude]" id="input-long<?php echo $address_row; ?>"  class="form-control" placeholder="Longitude" value="<?php echo $address['longitude']; ?>" />
                                 
                            </div>
                        </div>  
                        
                        
                      <div class="form-group required">
                        <label class="col-sm-2 control-label" for="input-address-1<?php echo $address_row; ?>"><?php echo $entry_address_1; ?></label>
                        <div class="col-sm-10">
                         <input readonly="" type="text" name="address[<?php echo $address_row; ?>][address_1]" id="input-address-1<?php echo $address_row; ?>"  class="form-control" placeholder="<?php echo $entry_address_1; ?>" value="<?php echo $address['address_1']; ?>" />
                         <?php if (isset($error_address[$address_row]['address_1'])) { ?>
                          <div class="text-danger"><?php echo $error_address[$address_row]['address_1']; ?></div>
                          <?php } ?>
                        </div>
                      </div>
          
                        <div class="form-group required">
                            <label class="col-sm-2 control-label" for="input-area<?php echo $address_row; ?>">Area</label>
                            <div class="col-sm-10">
                                <input type="text" onkeyup="autoArea('address[<?php echo $address_row; ?>][area]','input-area<?php echo $address_row; ?>')"  name="address[<?php echo $address_row; ?>][area]" id="input-area<?php echo $address_row; ?>"  class="form-control" placeholder="Area" value="<?php echo $address['area']; ?>" />
                          <?php if (isset($error_address[$address_row]['area'])) { ?>
                          <div class="text-danger"><?php echo $error_address[$address_row]['area']; ?></div>
                          <?php } ?>
                            </div>
                        </div>
                              
                          <div class="form-group required">
                            <label class="col-sm-2 control-label" for="input-city<?php echo $address_row; ?>">City</label>
                            <div class="col-sm-10">
                                <input type="text" name="address[<?php echo $address_row; ?>][city]" id="input-city<?php echo $address_row; ?>"   class="form-control" placeholder="City" value="<?php echo $address['city']; ?>" />
                           <?php if (isset($error_address[$address_row]['city'])) { ?>
                          <div class="text-danger"><?php echo $error_address[$address_row]['city']; ?></div>
                          <?php } ?>
                            </div>
                        </div> 
                      
                      
                      
                      <div class="form-group">
                        <label class="col-sm-2 control-label" for="input-company<?php echo $address_row; ?>"><?php echo $entry_company; ?></label>
                        <div class="col-sm-10">
                         <input type="text" name="address[<?php echo $address_row; ?>][company]"  class="form-control" placeholder="<?php echo $entry_company; ?>" value="<?php echo $address['company']; ?>" />
						</div>
                      </div>
					  
					   <div class="form-group">
                        <label class="col-sm-2 control-label" for="input-company<?php echo $address_row; ?>"><?php echo $entry_company_id; ?></label>
                        <div class="col-sm-10">
                         <input type="text" name="address[<?php echo $address_row; ?>][company_id]"  class="form-control" placeholder="<?php echo $entry_company_id; ?>" value="<?php echo $address['company_id']; ?>" />
						</div>
                      </div>
					  
					  <div class="form-group">
                        <label class="col-sm-2 control-label" for="input-company<?php echo $address_row; ?>"><?php echo $entry_tax_id; ?></label>
                        <div class="col-sm-10">
                        <input type="text" name="address[<?php echo $address_row; ?>][tax_id]"  class="form-control"  placeholder="<?php echo $entry_tax_id; ?>" value="<?php echo $address['tax_id']; ?>" />
                  <?php if (isset($error_address_tax_id[$address_row])) { ?>
                  <span class="text-danger"><?php echo $error_address_tax_id[$address_row]; ?></span>
                  <?php } ?>
						</div>
                      </div>
					  
					  
                      <div class="form-group required">
                        <label class="col-sm-2 control-label" for="input-postcode<?php echo $address_row; ?>"><?php echo $entry_postcode; ?></label>
                        <div class="col-sm-10">
                         <input type="text" name="address[<?php echo $address_row; ?>][postcode]" placeholder="<?php echo $entry_postcode; ?>"  class="form-control" value="<?php echo $address['postcode']; ?>" />
						</div>
                      </div>
                    
                      <div class="form-group">
                        <label class="col-sm-2 control-label"><?php echo $entry_default; ?></label>
                        <div class="col-sm-10">
                          <label class="radio">
                           <?php if ($address_address_id == $address['address_id']){ ?>
                  <input type="radio" name="address[<?php echo $address_row; ?>][default]" value="<?php echo $address_row; ?>" checked="checked" /></td>
                <?php } else { ?>
                <input type="radio" name="address[<?php echo $address_row; ?>][default]" value="<?php echo $address_row; ?>" />
                  </td>
                <?php } ?>
                          </label>
                        </div>
                      </div>
                    </div>
                     <?php $address_row++; ?>
                    
                    <?php } ?>
                <!----- Address Form End ----------->
					
					
			
                   
                  </div>
				  
				  
				  
                </div>
              </div>
            </div>
			
			
			
					<div class="tab-pane" id="tab-history">

					<div class="form-group">
					<label class="col-sm-2 control-label" for="input-comment"></label>
					 <div class="col-sm-10">
              <?php if ($paypalorcheque == 1) { ?>
            <input type="radio" name="paypalorcheque" value="1" checked="checked" />
            Paypal
            <input type="radio" name="paypalorcheque" value="0" />
            Cheque
	    <input type="radio" name="paypalorcheque" value="2" />
            Bank Transfer
            <?php }elseif ($paypalorcheque == 0) { ?>
            <input type="radio" name="paypalorcheque" value="1" />
            Paypal
            <input type="radio" name="paypalorcheque" value="0" checked="checked" />
            Cheque
	    <input type="radio" name="paypalorcheque" value="2" />
            Bank Transfer
            <?php } else { ?>
            <input type="radio" name="paypalorcheque" value="1" />
            Paypal
            <input type="radio" name="paypalorcheque" value="0" />
            Cheque
	    <input type="radio" name="paypalorcheque" value="2" checked="checked" />
            Bank Transfer
            <?php } ?>
			
            </div>
			
			
			
					</div>
					
					
					 <div class="form-group required" id="paypal-tr">
            <label class="col-sm-2 control-label" for="input-paypal"><?php echo $entry_paypalemail; ?></label>
            <div class="col-sm-10">
              <input type="text" name="paypal_email" value="<?php echo $paypal_email; ?>" placeholder="<?php echo $entry_paypalemail; ?>" 
			  id="input-paypal" class="form-control" />
              <?php if ($error_paypalemail) { ?>
              <div class="text-danger"><?php echo $error_paypalemail; ?></div>
              <?php } ?>
            </div>
          </div>
		  
		  
		   <div class="form-group required" id="bankname-tr">
            <label class="col-sm-2 control-label" for="input-bank"><?php echo $entry_bankname; ?></label>
            <div class="col-sm-10">
              <input type="text" name="bank_name" value="<?php echo $bank_name; ?>" placeholder="<?php echo $entry_bankname; ?>" 
			  id="input-bank" class="form-control" />
               <?php if ($error_bankname) { ?>
              <div class="text-danger"><?php echo $error_bankname; ?></div>
              <?php } ?>
            </div>
          </div>
		  
		  
		   <div class="form-group required" id="accountnumber-tr">
            <label class="col-sm-2 control-label" for="input-accountnumber"><?php echo $entry_accountnumber; ?></label>
            <div class="col-sm-10">
              <input type="text" name="account_number" value="<?php echo $account_number; ?>" placeholder="<?php echo $entry_accountnumber; ?>" 
			  id="input-accountnumber" class="form-control" />
               <?php if ($error_accountnumber) { ?>
              <div class="text-danger"><?php echo $error_accountnumber; ?></div>
              <?php } ?>
            </div>
          </div>
		  
		  
		   <div class="form-group required" id="accountname-tr">
            <label class="col-sm-2 control-label" for="input-accountname"><?php echo $entry_accountname; ?></label>
            <div class="col-sm-10">
              <input type="text" name="account_name" value="<?php echo $account_name; ?>" placeholder="<?php echo $entry_accountname; ?>" 
			  id="input-accountname" class="form-control" />
               <?php if ($error_accountname) { ?>
              <div class="text-danger"><?php echo $error_accountname; ?></div>
              <?php } ?>
            </div>
          </div>
		  
		  <div class="form-group required" id="branch-tr">
            <label class="col-sm-2 control-label" for="input-branch"><?php echo $entry_branch; ?></label>
            <div class="col-sm-10">
              <input type="text" name="branch" value="<?php echo $branch; ?>" placeholder="<?php echo $entry_branch; ?>" 
			  id="input-branch" class="form-control" />
               <?php if ($error_branch) { ?>
              <div class="text-danger"><?php echo $error_branch; ?></div>
              <?php } ?>
            </div>
          </div>
		  
		  
		   <div class="form-group required" id="ifsccode-tr">
            <label class="col-sm-2 control-label" for="input-ifsccode"><?php echo $entry_ifsccode; ?></label>
            <div class="col-sm-10">
              <input type="text" name="ifsccode" value="<?php echo $ifsccode; ?>" placeholder="<?php echo $entry_ifsccode; ?>" 
			  id="input-ifsccode" class="form-control" />
               <?php if ($error_ifsccode) { ?>
              <div class="text-danger"><?php echo $error_ifsccode; ?></div>
              <?php } ?>
            </div>
          </div>
		  
		  
		 
		 
		   <div class="form-group required" id="cheque-tr">
            <label class="col-sm-2 control-label" for="input-cheque"><?php echo $entry_cheque; ?></label>
            <div class="col-sm-10">
              <input type="text" name="cheque" value="<?php echo $cheque; ?>" placeholder="<?php echo $entry_cheque; ?>" 
			  id="input-cheque" class="form-control" />
               <?php if ($error_cheque) { ?>
              <div class="text-danger"><?php echo $error_cheque; ?></div>
              <?php } ?>
            </div>
          </div>
		  
					</div>
              
              <div class="tab-pane" id="tab-time">
              <hr>
<h3>Time Settings</h3>
<hr>

<div class="row">
    <div class="form-group required">
        <div class="col-md-5 text-center">
            <div class="col-md-6"></div>
            <div class="col-md-6 text-center">
                <strong>Delivery Start Time</strong>
            </div>
        </div>
        <div class="col-md-5 text-center">
            <div class="col-md-6"></div>
            <div class="col-md-6 text-center">
                <strong>Delivery End Time</strong>
            </div>
        </div>
        <div class="col-md-2">
        
        </div>

    </div>
</div>

<div class="row">
    <div class="form-group required">
        <div class="col-md-5">
            <label class="col-sm-6 control-label" for="input-mon_start_time">Monday</label>
            <div class="col-sm-6">
                <input type="time" name="mon_start_time" value="<?php echo $mon_start_time; ?>" id="input-mon_start_time" class="form-control" />
                <?php if ($error_mon_start_time) { ?>
                    <div class="text-danger"><?php echo $error_mon_start_time; ?></div>
                <?php } ?>
            </div>
        </div>
        <div class="col-md-5">
            <label class="col-sm-6 control-label" for="input-mon_end_time">Monday</label>
            <div class="col-sm-6">
                <input type="time" name="mon_end_time" value="<?php echo $mon_end_time; ?>" id="input-mon_end_time" class="form-control" />
                <?php if ($error_mon_end_time) { ?>
                    <div class="text-danger"><?php echo $error_mon_end_time; ?></div>
                <?php } ?>
            </div>
        </div>
        <div class="col-md-2">
            <input type="radio" name="mon_openclose" value="1" <?php  echo (($mon_start_time != '00:00:00' || $mon_end_time != '00:00:00') ? 'checked="checked"' : ''); ?> /> Open
            <input type="radio" name="mon_openclose" value="0" onclick="resetForm('mon')" <?php  echo (($mon_start_time == '00:00:00' && $mon_end_time == '00:00:00') ? 'checked="checked"' : ''); ?> /> Close
        </div>

    </div>
</div>
<div class="row">
    <div class="form-group required">
        <div class="col-md-5">
            <label class="col-sm-6 control-label" for="input-tue_start_time">Tuesday</label>
            <div class="col-sm-6">
                <input type="time" name="tue_start_time" value="<?php echo $tue_start_time; ?>" id="input-tue_start_time" class="form-control" />
                <?php if ($error_tue_start_time) { ?>
                    <div class="text-danger"><?php echo $error_tue_start_time; ?></div>
                <?php } ?>
            </div>
        </div>
        <div class="col-md-5">
            <label class="col-sm-6 control-label" for="input-tue_end_time">Tuesday</label>
            <div class="col-sm-6">
                <input type="time" name="tue_end_time" value="<?php echo $tue_end_time; ?>" id="input-tue_end_time" class="form-control" />
                <?php if ($error_tue_end_time) { ?>
                    <div class="text-danger"><?php echo $error_tue_end_time; ?></div>
                <?php } ?>
            </div>
        </div>
        <div class="col-md-2">
            <input type="radio" name="tue_openclose" value="1" <?php  echo (($tue_start_time != '00:00:00' || $tue_end_time != '00:00:00') ? 'checked="checked"' : ''); ?> /> Open
            <input type="radio" name="tue_openclose" value="0" onclick="resetForm('tue')" <?php  echo (($tue_start_time == '00:00:00' && $tue_end_time == '00:00:00') ? 'checked="checked"' : ''); ?> /> Close
        </div>

    </div>
</div>
<div class="row">
    <div class="form-group required">
        <div class="col-md-5">
            <label class="col-sm-6 control-label" for="input-wed_start_time">Wednesday</label>
            <div class="col-sm-6">
                <input type="time" name="wed_start_time" value="<?php echo $wed_start_time; ?>" id="input-wed_start_time" class="form-control" />
                <?php if ($error_wed_start_time) { ?>
                    <div class="text-danger"><?php echo $error_wed_start_time; ?></div>
                <?php } ?>
            </div>
        </div>
        <div class="col-md-5">
            <label class="col-sm-6 control-label" for="input-wed_end_time">Wednesday</label>
            <div class="col-sm-6">
                <input type="time" name="wed_end_time" value="<?php echo $wed_end_time; ?>" id="input-wed_end_time" class="form-control" />
                <?php if ($error_wed_end_time) { ?>
                    <div class="text-danger"><?php echo $error_wed_end_time; ?></div>
                <?php } ?>
            </div>
        </div>
        <div class="col-md-2">
            <input type="radio" name="wed_openclose" value="1" <?php  echo (($wed_start_time != '00:00:00' || $wed_end_time != '00:00:00') ? 'checked="checked"' : ''); ?> /> Open
            <input type="radio" name="wed_openclose" value="0" onclick="resetForm('wed')" <?php  echo (($wed_start_time == '00:00:00' && $wed_end_time == '00:00:00') ? 'checked="checked"' : ''); ?> /> Close
        </div>

    </div>
</div>
<div class="row">
    <div class="form-group required">
        <div class="col-md-5">
            <label class="col-sm-6 control-label" for="input-thu_start_time">Thursday</label>
            <div class="col-sm-6">
                <input type="time" name="thu_start_time" value="<?php echo $thu_start_time; ?>" id="input-thu_start_time" class="form-control" />
                <?php if ($error_thu_start_time) { ?>
                    <div class="text-danger"><?php echo $error_thu_start_time; ?></div>
                <?php } ?>
            </div>
        </div>
        <div class="col-md-5">
            <label class="col-sm-6 control-label" for="input-thu_end_time">Thursday</label>
            <div class="col-sm-6">
                <input type="time" name="thu_end_time" value="<?php echo $thu_end_time; ?>" id="input-thu_end_time" class="form-control" />
                <?php if ($error_thu_end_time) { ?>
                    <div class="text-danger"><?php echo $error_thu_end_time; ?></div>
                <?php } ?>
            </div>
        </div>
        <div class="col-md-2">
            <input type="radio" name="thu_openclose" value="1" <?php  echo (($thu_start_time != '00:00:00' || $thu_end_time != '00:00:00') ? 'checked="checked"' : ''); ?> /> Open
            <input type="radio" name="thu_openclose" value="0" onclick="resetForm('thu')" <?php  echo (($thu_start_time == '00:00:00' && $thu_end_time == '00:00:00') ? 'checked="checked"' : ''); ?> /> Close
        </div>

    </div>
</div>
<div class="row">
    <div class="form-group required">
        <div class="col-md-5">
            <label class="col-sm-6 control-label" for="input-fri_start_time">Friday</label>
            <div class="col-sm-6">
                <input type="time" name="fri_start_time" value="<?php echo $fri_start_time; ?>" id="input-fri_start_time" class="form-control" />
                <?php if ($error_fri_start_time) { ?>
                    <div class="text-danger"><?php echo $error_fri_start_time; ?></div>
                <?php } ?>
            </div>
        </div>
        <div class="col-md-5">
            <label class="col-sm-6 control-label" for="input-fri_end_time">Friday</label>
            <div class="col-sm-6">
                <input type="time" name="fri_end_time" value="<?php echo $fri_end_time; ?>" id="input-fri_end_time" class="form-control" />
                <?php if ($error_fri_end_time) { ?>
                    <div class="text-danger"><?php echo $error_fri_end_time; ?></div>
                <?php } ?>
            </div>
        </div>
        <div class="col-md-2">
            <input type="radio" name="fri_openclose" value="1" <?php  echo (($fri_start_time != '00:00:00' || $fri_end_time != '00:00:00') ? 'checked="checked"' : ''); ?> /> Open
            <input type="radio" name="fri_openclose" value="0" onclick="resetForm('fri')" <?php  echo (($fri_start_time == '00:00:00' && $fri_end_time == '00:00:00') ? 'checked="checked"' : ''); ?> /> Close
        </div>

    </div>
</div>
<div class="row">
    <div class="form-group required">
        <div class="col-md-5">
            <label class="col-sm-6 control-label" for="input-sat_start_time">Saturday</label>
            <div class="col-sm-6">
                <input type="time" name="sat_start_time" value="<?php echo $sat_start_time; ?>" id="input-sat_start_time" class="form-control" />
                <?php if ($error_sat_start_time) { ?>
                    <div class="text-danger"><?php echo $error_sat_start_time; ?></div>
                <?php } ?>
            </div>
        </div>
        <div class="col-md-5">
            <label class="col-sm-6 control-label" for="input-sat_end_time">Saturday</label>
            <div class="col-sm-6">
                <input type="time" name="sat_end_time" value="<?php echo $sat_end_time; ?>" id="input-sat_end_time" class="form-control" />
                <?php if ($error_sat_end_time) { ?>
                    <div class="text-danger"><?php echo $error_sat_end_time; ?></div>
                <?php } ?>
            </div>
        </div>
        <div class="col-md-2">
            <input type="radio" name="sat_openclose" value="1" <?php  echo (($sat_start_time != '00:00:00' || $sat_end_time != '00:00:00') ? 'checked="checked"' : ''); ?> /> Open
            <input type="radio" name="sat_openclose" value="0" onclick="resetForm('sat')" <?php  echo (($sat_start_time == '00:00:00' && $sat_end_time == '00:00:00') ? 'checked="checked"' : ''); ?> /> Close
        </div>

    </div>
</div>
<div class="row">
    <div class="form-group required">
        <div class="col-md-5">
            <label class="col-sm-6 control-label" for="input-sun_start_time">Sunday</label>
            <div class="col-sm-6">
                <input type="time" name="sun_start_time" value="<?php echo $sun_start_time; ?>" id="input-sun_start_time" class="form-control" />
                <?php if ($error_sun_start_time) { ?>
                    <div class="text-danger"><?php echo $error_sun_start_time; ?></div>
                <?php } ?>
            </div>
        </div>
        <div class="col-md-5">
            <label class="col-sm-6 control-label" for="input-sun_end_time">Sunday</label>
            <div class="col-sm-6">
                <input type="time" name="sun_end_time" value="<?php echo $sun_end_time; ?>" id="input-sun_end_time" class="form-control" />
                <?php if ($error_sun_end_time) { ?>
                    <div class="text-danger"><?php echo $error_sun_end_time; ?></div>
                <?php } ?>
            </div>
        </div>
        <div class="col-md-2">
            <input type="radio" name="sun_openclose" value="1"  <?php  echo (($sun_start_time != '00:00:00' || $sun_end_time != '00:00:00') ? 'checked="checked"' : ''); ?>/> Open
            <input type="radio" name="sun_openclose" value="0" onclick="resetForm('sun')" <?php  echo (($sun_start_time == '00:00:00' && $sun_end_time == '00:00:00') ? 'checked="checked"' : ''); ?>/> Close
        </div>

    </div>
</div>


<h3>Other Settings</h3>
<hr>
<div class="row">
    <div class="form-group required">
        <div class="col-md-6">
            <label class="col-sm-6 control-label" for="input-delivery_charges">Delivery Charges</label>
            <div class="col-sm-6">
                <input type="number" name="delivery_charges" value="<?php echo $delivery_charges; ?>" id="input-delivery_charges" class="form-control" />
                <?php if ($error_delivery_charges) { ?>
                    <div class="text-danger"><?php echo $error_delivery_charges; ?></div>
                <?php } ?>
            </div>
        </div>
    </div>            
</div>
<div class="row">
    <div class="form-group required">
        <div class="col-md-6">
            <label class="col-sm-6 control-label" for="input-delivery_timegap">Delivery Time Gap</label>
            <div class="col-sm-6">
                <input type="text" name="delivery_timegap" value="<?php echo $delivery_timegap; ?>" id="input-delivery_timegap" class="form-control" />
                <?php if ($error_delivery_timegap) { ?>
                    <div class="text-danger"><?php echo $error_delivery_timegap; ?></div>
                <?php } ?>
            </div>
        </div>
    </div>            
</div>
<div class="row">
    <div class="form-group required">
        <div class="col-md-6">
            <label class="col-sm-6 control-label" for="input-package_ready">Package Ready Time</label>
            <div class="col-sm-6">
                <input type="text" name="package_ready" value="<?php echo $package_ready; ?>" id="input-package_ready" class="form-control" />
                <?php if ($error_package_ready) { ?>
                    <div class="text-danger"><?php echo $error_package_ready; ?></div>
                <?php } ?>
            </div>
        </div>
    </div>            
</div>

<div class="row">
    <div class="form-group required">
        <div class="col-md-6">
            <label class="col-sm-6 control-label" for="input-minimum-order">Minimum Order Limit</label>
            <div class="col-sm-6">
                <input type="number" name="minimum_order" value="<?php echo $minimum_order; ?>" id="input-minimum-order" class="form-control" />
                <?php if ($error_minimum_order) { ?>
                    <div class="text-danger"><?php echo $error_minimum_order; ?></div>
                <?php } ?>
            </div>
        </div>
    </div>            
</div>
	  
        </form>
      </div>
    </div>
  </div>
 
 
 <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBZ4-CaBdvKPNqamRcb4yzRDn3vYVuJgE0&libraries=places" ></script>
<script>

$(document).ready(function(){

   var address_row_total = <?php echo $address_row; ?>;
   
   for(j=1; j < address_row_total; j++ )
   {
        var lat1    =  parseFloat($("#input-lat"+j).val());
        var long1   =  parseFloat($("#input-long"+j).val());
        var rowId1   =   j +1;
        initializeV2(rowId1,lat1,long1);
        
        
   }
   
});


function startFromZero(arr) {
    var newArr = [];
    var count = 0;

    for (var i in arr) {
        newArr[count++] = arr[i];
    }

    return newArr;
}

    var map;
    var myCenter = new google.maps.LatLng(31.5546, 74.3572);
    var markers = [];
    var infowindow;
    var myCenter_new;

function initializeV2(rowId,lati,longi) {
    debugger;
    var lat = parseFloat(lati);
    var lng = parseFloat(longi);
    
    myCenter_new = new google.maps.LatLng(lat, lng);
    var mapId = 1;
    if(rowId <= 1)
    {
        mapId = 1
    }
    else{
        mapId = parseInt(rowId) - parseInt(1);
    }

         var mapProp = {
            center: myCenter_new,
            zoom: 14,
            mapTypeId: google.maps.MapTypeId.ROADMAP
          };
          
         map = new google.maps.Map(document.getElementById("googleMap"+mapId), mapProp);
         var geocoder = new google.maps.Geocoder;
          google.maps.event.addListener(map, 'click', function(event) {
            placeMarker(event.latLng,mapId,geocoder);
          });

          // Create the search box and link it to the UI element.
          var input = /** @type {HTMLInputElement} */(
              document.getElementById('pac-input'));
          map.controls[google.maps.ControlPosition.TOP_LEFT].push(input);
  


          var searchBox = new google.maps.places.SearchBox(
            /** @type {HTMLInputElement} */(input));
        
          // [START region_getplaces]
          // Listen for the event fired when the user selects an item from the
          // pick list. Retrieve the matching places for that item.
          google.maps.event.addListener(searchBox, 'places_changed', function() {
            var places = searchBox.getPlaces();
        
            if (places.length == 0) {
              return;
            }    
     
   
    
       for (i = 0; i < markers.length; i++) {
        var obj = markers[i];
           if(obj['marker'+mapId])
           {
                marker;
                marker = obj['marker'+mapId]
                marker.setMap(null); 
                delete markers[i];    
          
           }
           markers= startFromZero(markers);         
        
        }

    // For each place, get the icon, place name, and location.

    var bounds = new google.maps.LatLngBounds();
    for (var i = 0, place; place = places[i]; i++) {
      var image = {
        url: place.icon,
        size: new google.maps.Size(71, 71),
        origin: new google.maps.Point(0, 0),
        anchor: new google.maps.Point(17, 34),
        scaledSize: new google.maps.Size(25, 25)
      };

      // Create a marker for each place.
       var marker = new google.maps.Marker({
        map: map,        
        title: place.name,
        position: place.geometry.location
      });
     
      //Add marker to array
      var obj = {};  
      obj["marker"+mapId] = marker;
      markers.push(obj);       
    
    $('#input-lat'+mapId).val(place.geometry.location.lat());
    $('#input-long'+mapId).val(place.geometry.location.lng());
    geocodeLatLng(place.geometry.location,geocoder,mapId);
      bounds.extend(place.geometry.location);
    }

    map.fitBounds(bounds);
    map.setZoom(14);
  });
  // [END region_getplaces]

  // Bias the SearchBox results towards places that are within the bounds of the
  // current map's viewport.
  google.maps.event.addListener(map, 'bounds_changed', function() {
    var bounds = map.getBounds();
    searchBox.setBounds(bounds);
  });
  
  
  // Add Marker 
  placeMarker(myCenter_new,mapId,geocoder);

}


function initialize(rowId) {    
    var mapId = 1;
    if(rowId <= 1)
    {
        mapId = 1
    }
    else{
        mapId = parseInt(rowId) - parseInt(1);
    }

         var mapProp = {
            center: myCenter,
            zoom: 14,
            mapTypeId: google.maps.MapTypeId.ROADMAP
          };
          
         map = new google.maps.Map(document.getElementById("googleMap"+mapId), mapProp);
         var geocoder = new google.maps.Geocoder;
          google.maps.event.addListener(map, 'click', function(event) {
            placeMarker(event.latLng,mapId,geocoder);
          });

          // Create the search box and link it to the UI element.
          var input = /** @type {HTMLInputElement} */(
              document.getElementById('pac-input'));
          map.controls[google.maps.ControlPosition.TOP_LEFT].push(input);
  


          var searchBox = new google.maps.places.SearchBox(
            /** @type {HTMLInputElement} */(input));
        
          // [START region_getplaces]
          // Listen for the event fired when the user selects an item from the
          // pick list. Retrieve the matching places for that item.
          google.maps.event.addListener(searchBox, 'places_changed', function() {
            var places = searchBox.getPlaces();
        
            if (places.length == 0) {
              return;
            }    
     
   
    
       for (i = 0; i < markers.length; i++) {
        var obj = markers[i];
           if(obj['marker'+mapId])
           {
                marker;
                marker = obj['marker'+mapId]
                marker.setMap(null); 
                delete markers[i];    
          
           }
           markers= startFromZero(markers);         
        
        }

    // For each place, get the icon, place name, and location.

    var bounds = new google.maps.LatLngBounds();
    for (var i = 0, place; place = places[i]; i++) {
      var image = {
        url: place.icon,
        size: new google.maps.Size(71, 71),
        origin: new google.maps.Point(0, 0),
        anchor: new google.maps.Point(17, 34),
        scaledSize: new google.maps.Size(25, 25)
      };

      // Create a marker for each place.
       var marker = new google.maps.Marker({
        map: map,        
        title: place.name,
        position: place.geometry.location
      });
     
      //Add marker to array
      var obj = {};  
      obj["marker"+mapId] = marker;
      markers.push(obj);       
    
    $('#input-lat'+mapId).val(place.geometry.location.lat());
    $('#input-long'+mapId).val(place.geometry.location.lng());
    geocodeLatLng(place.geometry.location,geocoder,mapId);
      bounds.extend(place.geometry.location);
    }

    map.fitBounds(bounds);
    map.setZoom(14);
  });
  // [END region_getplaces]

  // Bias the SearchBox results towards places that are within the bounds of the
  // current map's viewport.
  google.maps.event.addListener(map, 'bounds_changed', function() {
    var bounds = map.getBounds();
    searchBox.setBounds(bounds);
  });
  
  
}
function placeMarker(location,mapId,geocoder) {
debugger;
    for (i = 0; i < markers.length; i++) {
        var obj = markers[i];
           if(obj['marker'+mapId])
           {
                marker;
                marker = obj['marker'+mapId]
                marker.setMap(null); 
                delete markers[i];        
         
           }
            markers= startFromZero(markers);          
        
        }
  
        var marker = new google.maps.Marker({
          position: location,
          map: map,
        }); 
         map.panTo(location);
        var obj = {};
        obj["marker"+mapId] = marker;
        markers.push(obj);   
  
   $('#input-lat'+mapId).val(location.lat);
   $('#input-long'+mapId).val(location.lng); 
   geocodeLatLng(location,geocoder,mapId) 
}

  function geocodeLatLng(location,geocoder,mapId) {

        geocoder.geocode({'location': location}, function(results, status) {
          if (status === 'OK') {
            if (results[0]) {
                 var city = "";
                 var area = "";
               
                  for(var i=0, len=results[0].address_components.length; i<len; i++) {                        
                    	var ac = results[0].address_components[i];
                    	if(ac.types.indexOf("locality") >= 0) city = ac.long_name;
                        if(ac.types.indexOf("route") >= 0) area = ac.long_name;
                    	
                    }
             $("#input-address-1"+mapId).val(results[0].formatted_address);
             $("#input-area"+mapId).val(area);
             $("#input-city"+mapId).val(city);

            } else {
              window.alert('No results found');
            }
          } else {
            window.alert('Geocoder failed due to: ' + status);
          }
        });
      }

  </script>
 
 
   <script type="text/javascript"><!--

$('#aboutus').summernote({height: 300});

//--></script>
  
  <script type="text/javascript"><!--
var address_row = <?php echo $address_row; ?>;

function addAddress_OLD() {
	html  = '<div class="tab-pane" id="tab-address' + address_row + '">';
	html += '  <input type="hidden" name="address[' + address_row + '][address_id]" value="" />';

	

	html += '  <div class="form-group">';
	html += '    <label class="col-sm-2 control-label" for="input-company' + address_row + '"><?php echo $entry_company; ?></label>';
	html += '    <div class="col-sm-10"><input type="text" name="address[' + address_row + '][company]" value="" placeholder="<?php echo $entry_company; ?>" id="input-company' + address_row + '" class="form-control" /></div>';
	html += '  </div>';

	html += '  <div class="form-group required">';
	html += '    <label class="col-sm-2 control-label" for="input-address-1' + address_row + '"><?php echo $entry_address_1; ?></label>';
	html += '    <div class="col-sm-10"><input type="text" name="address[' + address_row + '][address_1]" value="" placeholder="<?php echo $entry_address_1; ?>" id="input-address-1' + address_row + '" class="form-control" /></div>';
	html += '  </div>';

	html += '  <div class="form-group">';
	html += '    <label class="col-sm-2 control-label" for="input-address-2' + address_row + '"><?php echo $entry_address_2; ?></label>';
	html += '    <div class="col-sm-10"><input type="text" name="address[' + address_row + '][address_2]" value="" placeholder="<?php echo $entry_address_2; ?>" id="input-address-2' + address_row + '" class="form-control" /></div>';
	html += '  </div>';

	html += '  <div class="form-group required">';
	html += '    <label class="col-sm-2 control-label" for="input-city' + address_row + '"><?php echo $entry_city; ?></label>';
	html += '    <div class="col-sm-10"><input type="text" name="address[' + address_row + '][city]" value="" placeholder="<?php echo $entry_city; ?>" id="input-city' + address_row + '" class="form-control" /></div>';
	html += '  </div>';

	html += '  <div class="form-group required">';
	html += '    <label class="col-sm-2 control-label" for="input-postcode' + address_row + '"><?php echo $entry_postcode; ?></label>';
	html += '    <div class="col-sm-10"><input type="text" name="address[' + address_row + '][postcode]" value="" placeholder="<?php echo $entry_postcode; ?>" id="input-postcode' + address_row + '" class="form-control" /></div>';
	html += '  </div>';

	html += '  <div class="form-group required">';
	html += '    <label class="col-sm-2 control-label" for="input-country' + address_row + '"><?php echo $entry_country; ?></label>';
	html += '    <div class="col-sm-10"><select name="address[' + address_row + '][country_id]" id="input-country' + address_row + '" onchange="country(this, \'' + address_row + '\', \'0\');" class="form-control">';
    html += '         <option value=""><?php echo $text_select; ?></option>';
    <?php foreach ($countries as $country) { ?>
    html += '         <option value="<?php echo $country['country_id']; ?>"><?php echo addslashes($country['name']); ?></option>';
    <?php } ?>
    html += '      </select></div>';
	html += '  </div>';

	html += '  <div class="form-group required">';
	html += '    <label class="col-sm-2 control-label" for="input-zone' + address_row + '"><?php echo $entry_zone; ?></label>';
	html += '    <div class="col-sm-10"><select name="address[' + address_row + '][zone_id]" id="input-zone' + address_row + '" class="form-control"><option value=""><?php echo $text_none; ?></option></select></div>';
	html += '  </div>';

	

	html += '  <div class="form-group">';
	html += '    <label class="col-sm-2 control-label"><?php echo $entry_default; ?></label>';
	html += '    <div class="col-sm-10"><label class="radio"><input type="radio" name="address[' + address_row + '][default]" value="1" /></label></div>';
	html += '  </div>';

    html += '</div>';

	$('#tab-general .tab-content').prepend(html);

	
	$('select[name=\'address[' + address_row + '][country_id]\']').trigger('change');

	$('#address-add').before('<li><a href="#tab-address' + address_row + '" data-toggle="tab"><i class="fa fa-minus-circle" onclick="$(\'#address a:first\').tab(\'show\'); $(\'a[href=\\\'#tab-address' + address_row + '\\\']\').parent().remove(); $(\'#tab-address' + address_row + '\').remove();"></i> <?php echo $tab_address; ?> ' + address_row + '</a></li>');

	$('#address a[href=\'#tab-address' + address_row + '\']').tab('show');

	$('.date').datetimepicker({
		pickTime: false
	});
	
	$('.datetime').datetimepicker({
		pickDate: true,
		pickTime: true
	});
	
	$('.time').datetimepicker({
		pickDate: false
	});	

	address_row++;
}


function addAddress() {
	html  = '<div class="tab-pane" id="tab-address' + address_row + '">';
	html += '  <input type="hidden" name="address[' + address_row + '][address_id]" value="" />';
	html += '<input id="pac-input" class="controls" type="text" placeholder="Search Box">';
	html += '<div class="googleMap" id="googleMap'+ address_row +'"></div> <br/>';
    
    html += '  <div class="form-group" style="display:none;">';
	html += '    <label class="col-sm-2 control-label" for="input-lat' + address_row + '">Latitude</label>';
	html += '    <div class="col-sm-10"><input type="text" name="address[' + address_row + '][latitude]" value="" placeholder="Latitude" id="input-lat' + address_row + '" class="form-control" /></div>';
	html += '  </div>';
    
    html += '  <div class="form-group"  style="display:none;">';
	html += '    <label class="col-sm-2 control-label" for="input-long' + address_row + '">Longitude</label>';
	html += '    <div class="col-sm-10"><input type="text" name="address[' + address_row + '][longitude]" value="" placeholder="Longitude" id="input-long' + address_row + '" class="form-control" /></div>';
	html += '  </div>';  
    
    html += '  <div class="form-group required">';
	html += '    <label class="col-sm-2 control-label" for="input-address-1' + address_row + '"><?php echo $entry_address_1; ?></label>';
	html += '    <div class="col-sm-10"><input type="text" name="address[' + address_row + '][address_1]" value="" placeholder="<?php echo $entry_address_1; ?>" id="input-address-1' + address_row + '" class="form-control" /></div>';
	html += '  </div>'; 
    
    html += '  <div class="form-group required">';
	html += '    <label class="col-sm-2 control-label" for="input-address-1' + address_row + '">Area</label>';
	html += '    <div class="col-sm-10"><input type="text" onkeyup="autoArea(\'address[' + address_row + '][area]\',\'input-area' + address_row + '\')" name="address[' + address_row + '][area]" value="" placeholder="Area" id="input-area' + address_row + '" class="form-control" /><input type="hidden" name="input-area' + address_row + '" value="" /></div>';
	html += '  </div>'; 
     
    html += '  <div class="form-group required">';
	html += '    <label class="col-sm-2 control-label" for="input-address-1' + address_row + '">City</label>';
	html += '    <div class="col-sm-10"><input type="text" name="address[' + address_row + '][city]" value="" placeholder="City" id="input-city' + address_row + '" class="form-control" /></div>';
	html += '  </div>';    

	html += '  <div class="form-group">';
	html += '    <label class="col-sm-2 control-label" for="input-company' + address_row + '"><?php echo $entry_company; ?></label>';
	html += '    <div class="col-sm-10"><input type="text" name="address[' + address_row + '][company]" value="" placeholder="<?php echo $entry_company; ?>" id="input-company' + address_row + '" class="form-control" /></div>';
	html += '  </div>';
    
   	html += '  <div class="form-group">';
    html += '  <label class="col-sm-2 control-label" for="input-company' + address_row + '"><?php echo $entry_company_id; ?></label>';
    html += '  <div class="col-sm-10"><input type="text" name="address[' + address_row + '][company_id]"  class="form-control" placeholder="<?php echo $entry_company_id; ?>" value="" /></div>';
    html += '  </div>';
    
    html += '  <div class="form-group">';
    html += '  <label class="col-sm-2 control-label" for="input-company' + address_row + '"><?php echo $entry_tax_id; ?></label>';
    html += '  <div class="col-sm-10"><input type="text" name="address[' + address_row + '][tax_id]"  class="form-control" placeholder="<?php echo $entry_tax_id; ?>" value="" /></div>';
    html += '  </div>';
    


	html += '  <div class="form-group required">';
	html += '    <label class="col-sm-2 control-label" for="input-postcode' + address_row + '"><?php echo $entry_postcode; ?></label>';
	html += '    <div class="col-sm-10"><input type="text" name="address[' + address_row + '][postcode]" value="" placeholder="<?php echo $entry_postcode; ?>" id="input-postcode' + address_row + '" class="form-control" /></div>';
	html += '  </div>';

	html += '  <div class="form-group">';
	html += '    <label class="col-sm-2 control-label"><?php echo $entry_default; ?></label>';
	html += '    <div class="col-sm-10"><label class="radio"><input type="radio" name="address[' + address_row + '][default]" value="1" /></label></div>';
	html += '  </div>';

    html += '</div>';

	$('#tab-general .tab-content').prepend(html);

	
	$('select[name=\'address[' + address_row + '][country_id]\']').trigger('change');

	$('#address-add').before('<li><a href="#tab-address' + address_row + '" data-toggle="tab"><i class="fa fa-minus-circle" onclick="$(\'#address a:first\').tab(\'show\'); $(\'a[href=\\\'#tab-address' + address_row + '\\\']\').parent().remove(); $(\'#tab-address' + address_row + '\').remove();"></i> <?php echo $tab_address; ?> ' + address_row + '</a></li>');

	$('#address a[href=\'#tab-address' + address_row + '\']').tab('show');

	$('.date').datetimepicker({
		pickTime: false
	});
	
	$('.datetime').datetimepicker({
		pickDate: true,
		pickTime: true
	});
	
	$('.time').datetimepicker({
		pickDate: false
	});
    	
    
	address_row++;
    initialize(address_row);
    
}
//--></script> 
  <script type="text/javascript"><!--
function country(element, index, zone_id) {
  if (element.value != '') {
		$.ajax({
			url: 'index.php?route=localisation/country/country&token=<?php echo $token; ?>&country_id=' + element.value,
			dataType: 'json',
			beforeSend: function() {
				$('select[name=\'address[' + index + '][country_id]\']').after(' <i class="fa fa-circle-o-notch fa-spin"></i>');
			},
			complete: function() {
				$('.fa-spin').remove();
			},
			success: function(json) {
				if (json['postcode_required'] == '1') {
					$('input[name=\'address[' + index + '][postcode]\']').parent().addClass('required');
				} else {
					$('input[name=\'address[' + index + '][postcode]\']').parent().parent().removeClass('required');
				}

				html = '<option value=""><?php echo $text_select; ?></option>';

				if (json['zone']) {
					for (i = 0; i < json['zone'].length; i++) {
						html += '<option value="' + json['zone'][i]['zone_id'] + '"';

						if (json['zone'][i]['zone_id'] == zone_id) {
							html += ' selected="selected"';
						}

						html += '>' + json['zone'][i]['name'] + '</option>';
					}
				} else {
					html += '<option value="0"><?php echo $text_none; ?></option>';
				}

				$('select[name=\'address[' + index + '][zone_id]\']').html(html);
			},
			error: function(xhr, ajaxOptions, thrownError) {
				alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
			}
		});
	}
}

$('select[name$=\'[country_id]\']').trigger('change');
//--></script> 

  <script type="text/javascript"><!--
  function autoArea(fieldName,renderedId){
    
        $('input[name=\''+fieldName+'\']').autocomplete({
    	'source': function(request, response) {
    		$.ajax({
    			url: 'index.php?route=localisation/area/autocomplete&token=<?php echo $token; ?>&filter_name=' +  encodeURIComponent(request),
    			dataType: 'json',
    			success: function(json) {
    				json.unshift({
    					area_id: 0,
    					name: '<?php echo $text_none; ?>'
    				});
    
    				response($.map(json, function(item) {
    					return {
    						label: item['name'],
    						value: item['area_id']
    					}
    				}));
    			}
    		});
    	},
    	'select': function(item) {
    		$('input[name=\''+fieldName+'\']').val(item['label']);
    		$('input[name=\''+renderedId+'\']').val(item['value']);
    	}
    });
    
  }

//--></script> 
<script type="text/javascript"><!--
  function autoCity(fieldName,renderedId){
    
        $('input[name=\''+fieldName+'\']').autocomplete({
    	'source': function(request, response) {
    		$.ajax({
    			url: 'index.php?route=localisation/area/autocomplete&token=<?php echo $token; ?>&filter_name=' +  encodeURIComponent(request),
    			dataType: 'json',
    			success: function(json) {
    				json.unshift({
    					area_id: 0,
    					name: '<?php echo $text_none; ?>'
    				});
    
    				response($.map(json, function(item) {
    					return {
    						label: item['name'],
    						value: item['area_id']
    					}
    				}));
    			}
    		});
    	},
    	'select': function(item) {
    		$('input[name=\''+fieldName+'\']').val(item['label']);
    		$('input[name=\''+renderedId+'\']').val(item['value']);
    	}
    });
    
  }

//--></script> 


  <script type="text/javascript"><!--
function city(element, index, area_id) {
  if (element.value != '') {
		$.ajax({
			url: 'index.php?route=localisation/area/areas&token=<?php echo $token; ?>&city_id=' + element.value,
			dataType: 'json',
			beforeSend: function() {
				$('select[name=\'address[' + index + '][city_id]\']').after(' <i class="fa fa-circle-o-notch fa-spin"></i>');
			},
			complete: function() {
				$('.fa-spin').remove();
			},
			success: function(json) {
				

				html = '<option value=""><?php echo $text_select; ?></option>';

				if (json) {
					for (i = 0; i < json.length; i++) {
						html += '<option value="' + json[i]['area_id'] + '"';

						if (json[i]['area_id'] == area_id) {
							html += ' selected="selected"';
						}

						html += '>' + json[i]['name'] + '</option>';
					}
				} else {
					html += '<option value="0"><?php echo $text_none; ?></option>';
				}

				$('select[name=\'address[' + index + '][area_id]\']').html(html);
			},
			error: function(xhr, ajaxOptions, thrownError) {
				alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
			}
		});
	}
}

$('select[name$=\'[city_id]\']').trigger('change');
//--></script> 
  
<script type="text/javascript"><!--

$('input[name=\'paypalorcheque\']').on('change', function() {
	var value = $(this).val();
	if(value == 1){
		$('#paypal-tr').show();
		$('#cheque-tr').hide();
		$('#bankname-tr').hide();
		$('#accountnumber-tr').hide();
		$('#accountname-tr').hide();
		$('#branch-tr').hide();
		$('#ifsccode-tr').hide();
	}else if(value == 2){
		$('#paypal-tr').hide();
		$('#cheque-tr').hide();
		$('#bankname-tr').show();
		$('#accountnumber-tr').show();
		$('#accountname-tr').show();
		$('#branch-tr').show();
		$('#ifsccode-tr').show();
	}
	else{
		$('#paypal-tr').hide();
		$('#cheque-tr').show();
		$('#bankname-tr').hide();
		$('#accountnumber-tr').hide();
		$('#accountname-tr').hide();
		$('#branch-tr').hide();
		$('#ifsccode-tr').hide();
	}
});

$('input[name=\'paypalorcheque\']:checked').trigger('change');
//--></script>
        <script>
function resetForm(id) {
var filedidStart = 'input-'+id+'_start_time';
var filedidEnd = 'input-'+id+'_end_time';


$('#' + filedidStart).val('');
$('#' + filedidEnd).val('');

//    $('#' + filedidStart).val(function() {
//        return this.defaultValue;
//    });
//    $('#' + filedidEnd).val(function() {//
//        return this.defaultValue;
//    });
}
        </script>

</div>
<?php echo $footer; ?>