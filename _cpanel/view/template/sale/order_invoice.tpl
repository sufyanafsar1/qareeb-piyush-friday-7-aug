<!DOCTYPE html>
<html dir="<?php echo $direction; ?>" lang="<?php echo $lang; ?>">
<head>
<meta charset="UTF-8" />
<title><?php echo $title; ?></title>
<base href="<?php echo $base; ?>" />
<link rel="icon" type="image/png" href="https://www.qareeb.com/image/catalog/logo/favicon.png">
<link href="view/javascript/bootstrap/css/bootstrap.css" rel="stylesheet" media="all" />
<script type="text/javascript" src="view/javascript/jquery/jquery-2.1.1.min.js"></script>
<script type="text/javascript" src="view/javascript/bootstrap/js/bootstrap.min.js"></script>
<link href="view/javascript/font-awesome/css/font-awesome.min.css" type="text/css" rel="stylesheet" />
<link type="text/css" href="view/stylesheet/stylesheet.css" rel="stylesheet" media="all" />
</head>
<style>
.table > thead > tr > th, .table > tbody > tr > th, .table > tfoot > tr > th, .table > thead > tr > td, .table > tbody > tr > td, .table > tfoot > tr > td {
    padding: 5px;
}
</style>
<body style="font-size: 10px;">
<div class="container invoice-page">
  <?php foreach ($orders as $order) { ?>
  <div style="page-break-after: always; min-height:80%">
  <div class="row">
         
          <table class="table">
          <tbody>
        <tr>
          <td class="custom-td custom-td-new" style="width: 70%;"> 
		    <img src="../image/catalog/logo/QareebLogo.svg" style="max-width: 19%;" title="Qareeb.com " class="img-responsive" alt="Qareeb.com "></td>
          <td class="custom-td custom-td-new invoice-no" style="width: 30%;"> <h3><?php echo $text_invoice; ?> #<?php echo $order['order_id']; ?></h3></td>
        </tr>
        </tbody>
          </table>
       </div>
   
    <table class="table table-bordered">
      <thead>
        <tr>
          <td colspan="2"><?php echo $text_order_detail; ?> <span style="float: right;"><b>Store Name: </b><?php echo $store; ?><span></td>
        </tr>
      </thead>
      <tbody>
        <tr>
          <td style="width: 50%;">
            <b><?php echo $text_shipping_address; ?></b>
            <address  style="margin-bottom:2px;">
            <?php echo $order['shipping_address']; ?>
            </address>
			 <b><?php echo $column_telephone; ?>: </b>
			 <?php echo $order['telephone']; ?>
            
           </td>
          <td style="width: 50%;"><b><?php echo $text_date_added; ?></b> <?php echo $order['date_added']; ?><br />
            <?php if ($order['invoice_no']) { ?>
            <b><?php echo $text_invoice_no; ?></b> <?php echo $order['invoice_no']; ?><br />
            <?php } ?>
           
            <b><?php echo $text_payment_method; ?></b> <?php echo $order['payment_method']; ?><br />
           
            <?php if ($order['delivery_time']) { ?>
            <b><?php echo "Delivery Time"; ?></b> <?php echo $order['delivery_time'] ; ?><br />
            <?php } ?>
          
          </td>
        </tr>
      </tbody>
    </table>

    <table class="table table-bordered">
      <thead>
        <tr>
          <td><b><?php echo $column_model; ?></b></td>
		  <td><b><?php echo $column_product; ?></b></td>
          <td class="text-right"><b><?php echo $column_quantity; ?></b></td>
          <td class="text-right"><b><?php echo $column_price; ?></b></td>
          <td class="text-right"><b><?php echo $column_total; ?></b></td>
        </tr>
      </thead>
      <tbody>
        <?php foreach ($order['product'] as $product) { ?>
        <tr <?php if($product['product_status'] == 2){echo "class='product_not_found_info'";} ?> >
		<td><?php echo $product['model']; ?></td>
          <td><?php echo $product['name']; ?>
            <?php foreach ($product['option'] as $option) { ?>
            <br />
            &nbsp;<small> - <?php echo $option['name']; ?>: <?php echo $option['value']; ?></small>
            <?php } ?></td>
          
          <td class="text-right"><?php echo $product['quantity']; ?></td>
          <td class="text-right"><?php echo $product['price']; ?></td>
          <td class="text-right"><?php echo $product['total']; ?></td>
        </tr>
        <?php } ?>
        <?php foreach ($order['voucher'] as $voucher) { ?>
        <tr>
          <td><?php echo $voucher['description']; ?></td>
          <td></td>
          <td class="text-right">1</td>
          <td class="text-right"><?php echo $voucher['amount']; ?></td>
          <td class="text-right"><?php echo $voucher['amount']; ?></td>
        </tr>
        <?php } ?>
        <?php foreach ($order['total'] as $total) { ?>
        <tr>
          <td class="text-right" colspan="4"><b><?php echo $total['title']; ?></b></td>
          <td class="text-right"><?php echo $total['text']; ?></td>
        </tr>
        <?php } ?>
      </tbody>
    </table>
    <?php if ($order['comment']) { ?>
    <table class="table table-bordered">
      <thead>
        <tr>
          <td><b><?php echo $text_comment; ?></b></td>
        </tr>
      </thead>
      <tbody>
        <tr>
          <td><?php echo $order['comment']; ?></td>
        </tr>
      </tbody>
    </table>
    <?php } ?>
      <div class="row">
         
          <table class="table">
          <tbody>
        <tr>
          <td class="custom-td custom-td-new" style="width: 50%;"> <b>Qareeb Saudi Trading company</b></td>
          <td class="custom-td" style="width: 25%;"> <img src="view/image/phone.png"/> &nbsp; <?php echo $order['store_telephone']; ?></td>
          <td class="custom-td" style="width: 25%;"> <img src="view/image/email.png"/>&nbsp;<?php echo $order['store_email']; ?></td>
        </tr>
        </tbody>
          </table>
       </div>
  </div>
  <?php } ?>
</div>
</body>
</html>