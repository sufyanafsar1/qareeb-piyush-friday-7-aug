<!DOCTYPE html>
<html dir="<?php echo $direction; ?>" lang="<?php echo $lang; ?>">
<head>
<meta charset="UTF-8" />
<title><?php echo $title; ?></title>
<base href="<?php echo $base; ?>" />
<link rel="icon" type="image/png" href="https://www.qareeb.com/image/catalog/logo/favicon.png">
<link href="view/javascript/bootstrap/css/bootstrap.css" rel="stylesheet" media="all" />
<script type="text/javascript" src="view/javascript/jquery/jquery-2.1.1.min.js"></script>
<script type="text/javascript" src="view/javascript/bootstrap/js/bootstrap.min.js"></script>
<link href="view/javascript/font-awesome/css/font-awesome.min.css" type="text/css" rel="stylesheet" />
<link type="text/css" href="view/stylesheet/stylesheet.css" rel="stylesheet" media="all" />
</head>
<style>
.table-bordered > thead > tr > th, .table-bordered > tbody > tr > th, .table-bordered > tfoot > tr > th, .table-bordered > thead > tr > td, .table-bordered > tbody > tr > td, .table-bordered > tfoot > tr > td {
    border: 1px solid black !important;
}
</style>
<body style="font-size: 11px;color:black;padding: 10px 0px;">
<div class="container invoice-page" style="width: 100% !important;">
  <?php foreach ($orders as $order) { ?>
  <div style="page-break-after: always; min-height:80%">
  <div class="row">
         
          <table style="display:none;" class="table">
          <tbody>
        <tr>
          <td class="custom-td custom-td-new" style="width: 70%;"> 
		    <img src="../image/catalog/logo/logo_mobile.jpeg" style="max-width: 19%;" title="Qareeb.com " class="img-responsive" alt="Qareeb.com "></td>
          <td class="custom-td custom-td-new invoice-no" style="width: 30%;"> <h3><?php echo $text_invoice; ?> #<?php echo $order['order_id']; ?></h3></td>
        </tr>
        </tbody>
          </table>
       </div>
   
    <table style="border: none;" class="table table-bordered">
      <thead>
        <tr>
          <td style="border: none; width: 33%;"><span><b>Store Name: </b><?php echo $order['store_name']; ?><br/><b>Order Id: </b> <?php echo $order['order_id']; ?> <br/><b>Date: </b> <?php echo $order['date_added']; ?><span></td>
		
		  <td style="border: none; width:34%; padding: 10px 11% 10px 11%;" class="custom-td custom-td-new" style=""> 
		  <img src="../image/catalog/logo/logo_mobile.jpeg" style="" title="Qareeb.com " class="img-responsive" alt="Qareeb.com "></td>
		
		 <td style="border: none; width: 33%; text-align: right;"><?php  ?> <span style=""><b>Telephone: </b><?php echo $order['telephone'];?><br/> <b>Customer: </b><?php echo$order['customer'];   ?><span></td>
        </tr>
      </thead>
      <tbody>
        <tr style="display:none">
          <td style="width: 50%;">
            <b><?php echo $text_shipping_address; ?></b>
            <address  style="margin-bottom:2px;">
            <?php echo $order['shipping_address']; ?>
            </address>
			 <b><?php echo $column_telephone; ?>: </b>
			 <?php echo $order['telephone']; ?>
            
           </td>
          <td style="width: 50%;"><b><?php echo $text_date_added; ?></b> <?php echo $order['date_added']; ?><br />
            <?php if ($order['invoice_no']) { ?>
            <b><?php echo $text_invoice_no; ?></b> <?php echo $order['invoice_no']; ?><br />
            <?php } ?>
           
            <b><?php echo $text_payment_method; ?></b> <?php echo $order['payment_method']; ?><br />
           
            <?php if ($order['delivery_time']) { ?>
            <b><?php echo "Delivery Time"; ?></b> <?php echo $order['delivery_time'] ; ?><br />
            <?php } ?>
          
          </td>
        </tr>
      </tbody>
    </table>

    <table class="table table-bordered">
      <thead>
        <tr>
          
          <td><b><?php echo $column_total; ?></b></td>
          <td><b><?php echo $column_price; ?></b></td>
          <td><b><?php echo $column_quantity; ?></b></td>
		  <td class="text-right"><b><?php echo $column_product; ?></b></td>
        </tr>
      </thead>
      <tbody>
        <?php foreach ($order['product'] as $product) { ?>
        <tr <?php if($product['product_status'] == 2){echo "class='product_not_found_info'";} ?> >
		
          <td><?php echo $product['total']; ?></td>
          <td><?php echo $product['price']; ?></td>
          <td><?php echo $product['quantity']; ?></td>
          <td  class="text-right"><?php echo $product['name']; ?>
            <?php foreach ($product['option'] as $option) { ?>
            <br />
            &nbsp;<small> - <?php echo $option['name']; ?>: <?php echo $option['value']; ?></small>
            <?php } ?></td>
          
        </tr>
        <?php } ?>
        <?php foreach ($order['voucher'] as $voucher) { ?>
        <tr>
          <td><?php echo $voucher['description']; ?></td>
          <td></td>
          <td class="text-right">1</td>
          <td class="text-right"><?php echo $voucher['amount']; ?></td>
          <td class="text-right"><?php echo $voucher['amount']; ?></td>
        </tr>
        <?php } ?>
        <?php foreach ($order['total'] as $total) { ?>
        <tr>
          <td class="text-right" colspan="3"><b><?php echo $total['title']; ?></b></td>
          <td class="text-right"><?php echo $total['text']; ?></td>
        </tr>
        <?php } ?>
      </tbody>
    </table>
    <?php if ($order['comment']) { ?>
    <table class="table table-bordered">
      <thead>
        <tr>
          <td><b><?php echo $text_comment; ?></b></td>
        </tr>
      </thead>
      <tbody>
        <tr>
          <td><?php echo $order['comment']; ?></td>
        </tr>
      </tbody>
    </table>
    <?php } ?>
      <div class="row">
         
          <table class="table">
          <tbody>
        <tr>
          <td class="custom-td custom-td-new" style="width: 50%;"> <b>Qareeb Saudi Trading company</b></td>
          <td class="custom-td" style="width: 25%;"> <img src="view/image/phone.png"/> &nbsp; <?php echo $order['store_telephone']; ?></td>
          <td class="custom-td" style="width: 25%; text-align: right;"> <img src="view/image/email.png"/>&nbsp;<?php echo $order['store_email']; ?></td>
        </tr>
        </tbody>
          </table>
       </div>
  </div>
  <?php } ?>
</div>
</body>
</html>