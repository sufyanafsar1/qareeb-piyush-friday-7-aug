
<!DOCTYPE html>
<html dir="<?php echo $direction; ?>" lang="<?php echo $lang; ?>">
<head>
<meta charset="UTF-8" />
<title><?php echo $title; ?></title>
<base href="<?php echo $base; ?>" />
<link rel="icon" type="image/png" href="https://www.qareeb.com/image/catalog/logo/favicon.png">
<link href="view/javascript/bootstrap/css/bootstrap.css" rel="stylesheet" media="all" />
<script type="text/javascript" src="view/javascript/jquery/jquery-2.1.1.min.js"></script>
<script type="text/javascript" src="view/javascript/bootstrap/js/bootstrap.min.js"></script>
<link href="view/javascript/font-awesome/css/font-awesome.min.css" type="text/css" rel="stylesheet" />
<link type="text/css" href="view/stylesheet/stylesheet.css" rel="stylesheet" media="all" />
</head>
<body>
<div class="container shipping-page">
  <?php foreach ($orders as $order) { ?>
  <div style="page-break-after: always;">
    
      <div class="row">
       
         <table class="table">
          <tbody>
        <tr>
          <td class="custom-td custom-td-new" style="width: 65%;"> <img src="view/image/logo.png"/></td>
          <td class="custom-td custom-td-new invoice-no" style="width: 35%;"> <h3><?php echo $text_order_no; ?> #<?php echo $order['order_id']; ?></h3></td>
        </tr>
        </tbody>
          </table>
       </div>
    <table class="table table-bordered">
      <thead>
        <tr>
          <td colspan="2"><?php echo $text_order_detail; ?></td>
        </tr>
      </thead>
      <tbody>
        <tr>
          <td>
           <b><?php echo $text_delivery_address; ?></b> <br/>
           <?php echo $order['shipping_address']; ?> <br/><br/>
           
           
           </td>
          <td style="width: 50%;"><b><?php echo $text_date_added; ?></b> <?php echo $order['date_added']; ?><br />
          
          
            <?php if ($order['invoice_no']) { ?>
            <b><?php echo $text_invoice_no; ?></b> <?php echo $order['invoice_no']; ?><br />
            <?php } ?>
         
         <?php if ($order['delivery_time']) { ?>
            <b><?php echo "Delivery Time"; ?></b> <?php echo $order['delivery_time'];  ?><br />
            <?php } ?>
         
         
          <b><?php echo $text_contact; ?>:</b> <br/> 
           <img src="view/image/mobile.png"/> &nbsp; <?php echo $order['telephone']; ?><br/>
            <img src="view/image/mail.png"/> &nbsp; <?php echo $order['email']; ?>
            <br/>
            </td>
        </tr>
      </tbody>
    </table>

    <table class="table table-bordered">
      <thead>
        <tr>
          <td><b><?php echo $column_location; ?></b></td>
          <td><b><?php echo $column_reference; ?></b></td>
          <td><b><?php echo $column_product; ?></b></td>
           <td><b><?php echo $column_category; ?></b></td>
          <td><b><?php echo $column_weight; ?></b></td>
          <td><b><?php echo $column_model; ?></b></td>
          <td class="text-right"><b><?php echo $column_quantity; ?></b></td>
          <td class="text-right"><b><?php echo $column_price; ?></b></td>
        </tr>
      </thead>
      <tbody>
        <?php foreach ($order['product'] as $product) { ?>
        <tr <?php if($product['product_status'] == 2){echo "class='product_not_found_info'";} ?>  >
          <td><?php echo $product['location']; ?></td>
          <td><?php if ($product['sku']) { ?>
            <?php echo $text_sku; ?> <?php echo $product['sku']; ?><br />
            <?php } ?>
            <?php if ($product['upc']) { ?>
            <?php echo $text_upc; ?> <?php echo $product['upc']; ?><br />
            <?php } ?>
            <?php if ($product['ean']) { ?>
            <?php echo $text_ean; ?> <?php echo $product['ean']; ?><br />
            <?php } ?>
            <?php if ($product['jan']) { ?>
            <?php echo $text_jan; ?> <?php echo $product['jan']; ?><br />
            <?php } ?>
            <?php if ($product['isbn']) { ?>
            <?php echo $text_isbn; ?> <?php echo $product['isbn']; ?><br />
            <?php } ?>
            <?php if ($product['mpn']) { ?>
            <?php echo $text_mpn; ?><?php echo $product['mpn']; ?><br />
            <?php } ?></td>
          <td><?php echo $product['name']; ?>
            <?php foreach ($product['option'] as $option) { ?>
            <br />
            &nbsp;<small> - <?php echo $option['name']; ?>: <?php echo $option['value']; ?></small>
            <?php } ?></td>
              <td>
            <?php foreach ($product['category'] as $category) { ?>
            <br />
            &nbsp;<?php echo $category['name']; ?>
            <?php } ?></td>
          <td><?php echo $product['weight']; ?></td>
          <td><?php echo $product['model']; ?></td>
          <td class="text-right"><?php echo $product['quantity']; ?></td>
          <td class="text-right"><?php echo $product['price']; ?></td>
        </tr>
        <?php } ?>
      </tbody>
    </table>
    <?php if ($order['comment']) { ?>
    <table class="table table-bordered">
      <thead>
        <tr>
          <td><b><?php echo $text_comment; ?></b></td>
        </tr>
      </thead>
      <tbody>
        <tr>
          <td><?php echo $order['comment']; ?></td>
        </tr>
      </tbody>
    </table>
    <?php } ?>
       <div class="row">
         
          <table class="table">
          <tbody>
        <tr>
          <td class="custom-td custom-td-new" style="width: 50%;"> <b>Qareeb Online Grocery</b></td>
          <td class="custom-td" style="width: 25%;"> <img src="view/image/phone.png"/> &nbsp; <?php echo $order['store_telephone']; ?></td>
          <td class="custom-td" style="width: 25%;"> <img src="view/image/email.png"/>&nbsp;<?php echo $order['store_email']; ?></td>
        </tr>
        </tbody>
          </table>
       </div>
  </div>
  <?php } ?>
</div>
</body>
</html>