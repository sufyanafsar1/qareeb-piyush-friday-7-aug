<?php
class ControllerAccountInviteOthersapi extends Controller {
	

	public function index() {


		$userName=$this->request->get['userName'];
     
        $emails=      explode(",",$this->request->get['emails']);
        
        foreach ($emails as $email) 
        {
    $mail = new Mail();
					$mail->protocol = $this->config->get('config_mail_protocol');
					$mail->parameter = $this->config->get('config_mail_parameter');
					$mail->smtp_hostname = $this->config->get('config_mail_smtp_hostname');
					$mail->smtp_username = $this->config->get('config_mail_smtp_username');
					$mail->smtp_password = html_entity_decode($this->config->get('config_mail_smtp_password'), ENT_QUOTES, 'UTF-8');
					$mail->smtp_port = $this->config->get('config_mail_smtp_port');
					$mail->smtp_timeout = $this->config->get('config_mail_smtp_timeout');

					$mail->setTo($email);
					$mail->setFrom($this->config->get('config_email'));
					$mail->setSender(html_entity_decode("qareeb.com Qareeb APP", ENT_QUOTES, 'UTF-8'));
					$mail->setSubject(html_entity_decode("qareeb.com Qareeb APP" , ENT_QUOTES, 'UTF-8'));
					$mail->setHtml(" <!DOCTYPE html><html><title>qareeb.com</title><body>
                    <h3>". $userName." has invited you to visit qareeb.com</h3>
                    <p>Hi , No need to wait in lines anymore, now we can buy grocery online at Qareeb.</p>
                    <p>Regards,</p>
                    <p>".$userName."</p>
                    <p>
                     <a href='http://qareeb.com'>Qareeb Web</a> 
                     <a href='https://play.google.com/store/apps/details?id=com.rs.careebstore'>Qareeb Android</a> 
                     <a href='https://itunes.apple.com/us/app/careeb/id1200691958?ls=1&mt=8'>Qareeb IOS</a> 
                    </p>
                    </body>
                    </html> ");
					$mail->setText(" ");
					$mail->send();
     
     }
		
			$data['message'] = 'success';
			//$data['customer_info'] = $this->model_account_customer->getCustomerByEmail($this->request->post['email']);			
		
		print_r(json_encode($data));
	}

}