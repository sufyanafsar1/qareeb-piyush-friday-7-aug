<?php
class ControllerAccountAccount extends Controller
{
	public function index()
	{

		if (!$this->customer->isLogged()) {
			//$this->session->data['redirect'] = $this->url->link('account/account', '', 'SSL');

			$this->response->redirect($this->url->link('account/register', '', 'SSL'));
		}

		$this->load->language('account/account');

		$this->document->setTitle($this->language->get('heading_title'));

		$data['breadcrumbs'] = array();

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_home'),
			'href' => $this->url->link('common/home')
		);

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_account'),
			'href' => $this->url->link('account/account', '', 'SSL')
		);

		if (isset($this->session->data['success'])) {
			$data['success'] = $this->session->data['success'];

			unset($this->session->data['success']);
		} else {
			$data['success'] = '';
		}

		$data['heading_title'] = $this->language->get('heading_title');

		$data['text_my_account'] = $this->language->get('text_my_account');
		$data['text_my_orders'] = $this->language->get('text_my_orders');
		$data['text_my_newsletter'] = $this->language->get('text_my_newsletter');
		$data['text_edit'] = $this->language->get('text_edit');
		$data['text_password'] = $this->language->get('text_password');
		$data['text_address'] = $this->language->get('text_address');
		$data['text_wishlist'] = $this->language->get('text_wishlist');
		$data['text_order'] = $this->language->get('text_order');
		$data['text_download'] = $this->language->get('text_download');
		$data['text_reward'] = $this->language->get('text_reward');
		$data['text_return'] = $this->language->get('text_return');
		$data['text_transaction'] = $this->language->get('text_transaction');
		$data['text_newsletter'] = $this->language->get('text_newsletter');
		$data['text_recurring'] = $this->language->get('text_recurring');
		$data['text_invite'] =  $this->language->get('text_invite');
		$data['edit'] = $this->url->link('account/edit', '', 'SSL');
		$data['password'] = $this->url->link('account/password', '', 'SSL');
		$data['address'] = $this->url->link('account/address', '', 'SSL');
		$data['wishlist'] = $this->url->link('account/wishlist');

		$data['order'] = $this->url->link('account/order', '', 'SSL');
		$data['download'] = $this->url->link('account/download', '', 'SSL');
		$data['return'] = $this->url->link('account/return', '', 'SSL');
		$data['transaction'] = $this->url->link('account/transaction', '', 'SSL');
		$data['newsletter'] = $this->url->link('account/newsletter', '', 'SSL');
		$data['recurring'] = $this->url->link('account/recurring', '', 'SSL');

		if ($this->config->get('reward_status')) {
			$data['reward'] = $this->url->link('account/reward', '', 'SSL');
		} else {
			$data['reward'] = '';
		}

		//	$data['column_left'] = $this->load->controller('common/column_left');
		//	$data['column_right'] = $this->load->controller('common/column_right');
		//	$data['content_top'] = $this->load->controller('common/content_top');
		//	$data['content_bottom'] = $this->load->controller('common/content_bottom');
		$data['footer'] = $this->load->controller('common/footer');
		$data['header'] = $this->load->controller('common/header');

		if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/template/account/account.tpl')) {
			$this->response->setOutput($this->load->view($this->config->get('config_template') . '/template/account/account.tpl', $data));
		} else {
			$this->response->setOutput($this->load->view('default/template/account/account.tpl', $data));
		}
	}

	public function country()
	{
		$json = array();

		$this->load->model('localisation/country');

		$country_info = $this->model_localisation_country->getCountry($this->request->get['country_id']);

		if ($country_info) {
			$this->load->model('localisation/zone');

			$json = array(
				'country_id'        => $country_info['country_id'],
				'name'              => $country_info['name'],
				'iso_code_2'        => $country_info['iso_code_2'],
				'iso_code_3'        => $country_info['iso_code_3'],
				'address_format'    => $country_info['address_format'],
				'postcode_required' => $country_info['postcode_required'],
				'zone'              => $this->model_localisation_zone->getZonesByCountryId($this->request->get['country_id']),
				'status'            => $country_info['status']
			);
		}

		$this->response->addHeader('Content-Type: application/json');
		$this->response->setOutput(json_encode($json));
	}

	public function sendInvite()
	{

		$inviteEmail = $this->request->post['inviteEmail'];
		$email_array = explode(',', $inviteEmail);
		$store_name = $this->config->get('config_name');

		$c_name = $this->customer->getFirstName() . ' ' . $this->customer->getLastName();
		$subject =  $c_name . ' has invited you to visit Careeb.com ';

		foreach ($email_array as $key => $value) {
			if ($value) {
				if (filter_var($value, FILTER_VALIDATE_EMAIL)) {
					$message  = '<html dir="ltr" lang="en">' . "\n";
					$message .= '  <head>' . "\n";
					$message .= '    <title>' . $this->request->post['subject'] . '</title>' . "\n";
					$message .= '    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">' . "\n";
					$message .= '  </head>' . "\n";
					$message .= '  <body><p><h3> ' . $c_name . ' has invited you to visit "qareeb.com" </h3></p><p> Hi , No need to wait in lines anymore, now we can buy grocery online at "qareeb.com"</p><p>Regards,</p><p>' . $c_name . ' </p><p><a rel="nofollow" href="http://qareeb.com" target="_blank">Qareeb Web </a>&nbsp;&nbsp;<a rel="nofollow" href="https://play.google.com/store/apps/details?id=com.rs.careebstore" target="_blank">Qareeb Android </a>&nbsp;&nbsp;<a rel="nofollow" href="https://itunes.apple.com/us/app/careeb/id1200691958?ls=1&mt=8" target="_blank"> Qareeb IOS </a> </p></body>';
					$message .= '</html>' . "\n";

					$careeb_web = '<a rel="nofollow" href="http://qareeb.com" target="_blank">Qareeb Web </a>';

					$careeb_android = '<a rel="nofollow" href="https://play.google.com/store/apps/details?id=com.rs.careebstore" target="_blank">Qareeb Android </a>';

					$careeb_ios = '<a rel="nofollow" href="https://itunes.apple.com/us/app/careeb/id1200691958?ls=1&mt=8" target="_blank"> Qareeb IOS </a>';

					$this->load->model('catalog/product');

					$template = $this->model_catalog_product->getEmailTemplate('invite-your-friend');

					$tags = array("[CUSTOMER_NAME]", "[CAREEB_WEB]", "[CAREEB_ANDROID]", "[CAREEB_IOS]", "[SUBJECT]");
					$tagsValues = array($c_name, $careeb_web, $careeb_android, $careeb_ios, $template['subject']);

					//get subject
					$subject = html_entity_decode($template['subject'], ENT_QUOTES, 'UTF-8');
					$subject = str_replace($tags, $tagsValues, $subject);
					$subject = html_entity_decode($subject, ENT_QUOTES);

					//get message body
					$msg = html_entity_decode($template['description'], ENT_QUOTES, 'UTF-8');
					$msg = str_replace($tags, $tagsValues, $msg);
					$msg = html_entity_decode($msg, ENT_QUOTES);

					$header = html_entity_decode($this->config->get('config_mail_header'), ENT_QUOTES, 'UTF-8');
					$header = str_replace($tags, $tagsValues, $header);
					$message = html_entity_decode($header, ENT_QUOTES);
					$message .= $msg;
					$message .= html_entity_decode($this->config->get('config_mail_footer'), ENT_QUOTES, 'UTF-8');


					$mail = new Mail();
					$mail->protocol = $this->config->get('config_mail_protocol');
					$mail->parameter = $this->config->get('config_mail_parameter');
					$mail->smtp_hostname = $this->config->get('config_mail_smtp_hostname');
					$mail->smtp_username = $this->config->get('config_mail_smtp_username');
					$mail->smtp_password = html_entity_decode($this->config->get('config_mail_smtp_password'), ENT_QUOTES, 'UTF-8');
					$mail->smtp_port = $this->config->get('config_mail_smtp_port');
					$mail->smtp_timeout = $this->config->get('config_mail_smtp_timeout');

					$mail->setTo($value);
					$mail->setFrom($this->config->get('config_email'));
					$mail->setSender(html_entity_decode($store_name, ENT_QUOTES, 'UTF-8'));
					$mail->setSubject(html_entity_decode($subject, ENT_QUOTES, 'UTF-8'));
					$mail->setHtml($message);
					$mail->send();
				} else {
					$json['error'][] = $value;
				}
			}
		}

		if (isset($json['error']) && count($json['error'])) {
			$this->response->addHeader('Content-Type: application/json');
			$this->response->setOutput(json_encode($json));
		} else {
			$json['success'] = 'Invitation has been sent successfully';
			$this->response->addHeader('Content-Type: application/json');
			$this->response->setOutput(json_encode($json));
		}
	}
}
