<?php
class ControllerAccountChangePasswordapi extends Controller {
	private $error = array();

	public function index() {
		$this->load->language('account/password');
		$this->load->model('account/customer');
		
		if ($this->validate()) {
			
			$this->model_account_customer->editPasswordByUserId($this->request->post['user_id'], $this->request->post['password']);
			
			// Add to activity log
			$customer_info = $this->model_account_customer->getCustomerByUserId($this->request->post['user_id']);
			
			if ($customer_info) {
			
				$this->load->model('account/activity');

				$activity_data = array(
					'customer_id' => $this->request->post['user_id'],
					'name'        => $customer_info['firstname'] . ' ' . $customer_info['lastname']
				);

				$this->model_account_activity->addActivity('password', $activity_data);
			}			
			$data['message'] = 'success';	

		}else{
			$data['message'] = $this->error['warning'];	
		}
		print_r(json_encode($data));
	}

	protected function validate() {
		
		if (!isset($this->request->post['user_id'])) {
			$this->error['warning'] = $this->language->get('error_userid');
		} elseif (!$this->model_account_customer->getTotalCustomersByUserId($this->request->post['user_id'])) {
			$this->error['warning'] = $this->language->get('error_userid');
		} elseif (!$this->model_account_customer->getCustomersPasswordCheck($this->request->post['user_id'], $this->request->post['old_password'])) {
			$this->error['warning'] = $this->language->get('error_oldpassword');
		} elseif ((utf8_strlen($this->request->post['password']) < 4) || (utf8_strlen($this->request->post['password']) > 20)) {
			$this->error['warning'] = $this->language->get('error_password');
		}

		return !$this->error;
	}
}