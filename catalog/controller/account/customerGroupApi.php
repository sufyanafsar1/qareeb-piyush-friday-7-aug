<?php
class ControllerAccountCustomerGroupApi extends Controller {
	private $error = array();

	public function index() {
	
		$this->load->model('account/customer_group');
      
		 $responce;
          $customerGroupData = $this->model_account_customer_group->getCustomerGroups();	
		if(isset($customerGroupData) && count($customerGroupData) > 0)
          {
			  $responce["customer_group"]=   $customerGroupData;
		   
          }	else
		      {
			   $responce["error"]="customer group not found";
		      }

		print_r(json_encode($responce));
	}

	
}