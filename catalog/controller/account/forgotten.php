<?php
class ControllerAccountForgotten extends Controller {
	private $error = array();

	public function index() {
	  
		if ($this->customer->isLogged()) {		 
			$this->response->redirect($this->url->link('account/account', '', 'SSL'));
		}
  
		$this->language->load('account/forgotten');

		$this->document->setTitle($this->language->get('heading_title'));

		$this->load->model('account/customer');
		
		if (($this->request->server['REQUEST_METHOD'] == 'POST')) {
			
			if (isset($this->request->post['mobile'] ) && $this->request->post['mobile'] != null ) {
				
				if ($this->validate()) {
					
					$this->load->language('mail/forgotten');

					$password = createPassword();
		            $customer_info = $this->model_account_customer->getCustomerByTelephone($this->request->post['mobile']);
		           
		            if($customer_info)
		            {
		                $this->model_account_customer->editPassword($customer_info['email'], $password);
		                $message = sprintf($this->language->get('text_subject'), html_entity_decode($this->config->get('config_name'), ENT_QUOTES, 'UTF-8')) . "\n";
		                $message .= $this->language->get('text_password') ." ". $password. "\n";
		                             
		                //$smsData =  tetSMS($this->request->post['mobile'],$message);
		                 $smsData =  sendSMS($this->request->post['mobile'],$message);
		                    
		               	$this->load->model('account/activity');

		                // Add to activity log
						$activity_data = array(
							'customer_id' => $customer_info['customer_id'],
							'name'        => $customer_info['firstname'] . ' ' . $customer_info['lastname']
						);

						$this->model_account_activity->addActivity('forgotten', $activity_data);
		                
		            }
					
					$this->session->data['success'] = $this->language->get('text_success');
		          
					//$this->response->redirect($this->url->link('account/login', '', 'SSL'));
		            $this->response->redirect($this->url->link('account/forgotten', '', 'SSL'));
		        }
			}
			elseif (isset($this->request->post['email'] ) && $this->request->post['email'] != null) {
				
				if ($this->validateOld()) {
					
					$this->load->language('mail/forgotten');

					$password = createPassword();
		            $customer_info = $this->model_account_customer->getCustomerByEmail($this->request->post['email']);
					
		            if($customer_info)
		            {			
						$code = $password;

						$this->model_account_customer->editCode($this->request->post['email'], $code);
						
						$email = $this->request->post['email'];
			
		                $this->model_account_customer->editPassword($customer_info['email'], $password);
		                $message = sprintf($this->language->get('text_subject'), html_entity_decode($this->config->get('config_name'), ENT_QUOTES, 'UTF-8')) . "\n";
						
		                $c_name = $customer_info['firstname'] . ' ' . $customer_info['lastname'];
									
						
		                $reset_link = $this->url->link('account/reset_pwd_link', 'code='.$code.'&email='. $email, 'SSL');
						
						//$reset_link .= '<a href="'.$resetlink.'" style="display:block; width:160px; height:25px; background:#614A99; padding:14px 5px 6px 6px; text-align:center; border-radius:5px; color:white; font-weight:bold;">Reset Your Password</a>';

						$ip_address = $this->request->server['REMOTE_ADDR'];
						
						$this->load->model('catalog/product');	

						$template = $this->model_catalog_product->getEmailTemplate('customer-forgot-passwod-email');	

						$tags = array("[CUSTOMER_NAME]", "[RESET_LINK]", "[IP_ADDRESS]", "[SUBJECT]");
						$tagsValues = array($c_name, $reset_link, $ip_address, $template['subject']);
						
						//get subject
						$subject = html_entity_decode($template['subject'], ENT_QUOTES, 'UTF-8');
						$subject = str_replace($tags, $tagsValues, $subject);
						$subject = html_entity_decode($subject, ENT_QUOTES);
												
						//get message body
						$msg = html_entity_decode($template['description'], ENT_QUOTES, 'UTF-8');
						$msg = str_replace($tags, $tagsValues, $msg);
						$msg = html_entity_decode($msg, ENT_QUOTES);
								
						$header = html_entity_decode($this->config->get('config_mail_header'), ENT_QUOTES, 'UTF-8');
						$header = str_replace($tags, $tagsValues, $header);
						$message = html_entity_decode($header, ENT_QUOTES);
						$message .= $msg;
						$message .= html_entity_decode($this->config->get('config_mail_footer'), ENT_QUOTES, 'UTF-8');
						
												
		                $mail = new Mail();
						$mail->protocol = $this->config->get('config_mail_protocol');
						$mail->parameter = $this->config->get('config_mail_parameter');
						$mail->smtp_hostname = $this->config->get('config_mail_smtp_hostname');
						$mail->smtp_username = $this->config->get('config_mail_smtp_username');
						$mail->smtp_password = html_entity_decode($this->config->get('config_mail_smtp_password'), ENT_QUOTES, 'UTF-8');
						$mail->smtp_port = $this->config->get('config_mail_smtp_port');
						$mail->smtp_timeout = $this->config->get('config_mail_smtp_timeout');
						$mail->setTo($this->request->post['email']);
						$mail->setFrom($this->config->get('config_email'));
						$mail->setSender(html_entity_decode($this->config->get('config_name'), ENT_QUOTES, 'UTF-8'));
						$mail->setSubject(html_entity_decode($subject, ENT_QUOTES, 'UTF-8'));
						// $mail->setText(html_entity_decode($message, ENT_QUOTES, 'UTF-8'));
						$mail->setHtml($message);
						$mail->send();

		               	$this->load->model('account/activity');

		                // Add to activity log
						$activity_data = array(
							'customer_id' => $customer_info['customer_id'],
							'name'        => $customer_info['firstname'] . ' ' . $customer_info['lastname']
						);

						$this->model_account_activity->addActivity('forgotten', $activity_data);
		                
		            }
					
					$this->session->data['success'] = $this->language->get('text_success_email');
		          
					//$this->response->redirect($this->url->link('account/login', '', 'SSL'));
		            $this->response->redirect($this->url->link('account/forgotten', '', 'SSL'));
		        }
			}elseif (empty($this->request->post['email']) && empty($this->request->post['mobile'])) {
			$this->error['warning'] = $this->language->get('error_mobile_email');}
		}
		if(isset($this->session->data['success'])){
			$data['message_success'] = $this->session->data['success'];
		} else {
			$data['message_success'] = '';
		}
        $data['mobile_format'] = $this->language->get('entry_mobile_format');
		$data['breadcrumbs'] = array();

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_home'),
			'href' => $this->url->link('common/home')
		);

		

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_forgotten'),
			'href' => $this->url->link('account/forgotten', '', 'SSL')
		);

		$data['heading_title'] = $this->language->get('heading_title');

		$data['text_your_email'] = $this->language->get('text_your_email');
		$data['text_email'] = $this->language->get('text_email');

		$data['entry_email'] = $this->language->get('entry_email');
        
        $data['text_your_mobile'] = $this->language->get('text_your_mobile');
		$data['text_mobile'] = $this->language->get('text_mobile');

		$data['entry_mobile'] = $this->language->get('entry_mobile');

		$data['button_continue'] = $this->language->get('button_continue');
		$data['button_back'] = $this->language->get('button_back');

		if (isset($this->error['warning'])) {
			$data['error_warning'] = $this->error['warning'];
		} else {
			$data['error_warning'] = '';
		}

		$data['action'] = $this->url->link('account/forgotten', '', 'SSL');

		//$data['back'] = $this->url->link('account/login', '', 'SSL');
        $data['back'] = $this->url->link('common/home', '', 'SSL');

		$data['column_left'] = $this->load->controller('common/column_left');
		$data['column_right'] = $this->load->controller('common/column_right');
		$data['content_top'] = $this->load->controller('common/content_top');
		$data['content_bottom'] = $this->load->controller('common/content_bottom');
		$data['footer'] = $this->load->controller('common/footer');
       
  
		$data['header'] = $this->load->controller('common/header');

		if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/template/account/forgotten.tpl')) {
			$this->response->setOutput($this->load->view($this->config->get('config_template') . '/template/account/forgotten.tpl', $data));
		} else {
			$this->response->setOutput($this->load->view('default/template/account/forgotten.tpl', $data));
		}
	}

	protected function validate() {
		if (!isset($this->request->post['mobile']) || empty($this->request->post['mobile'])) {
			$this->error['warning'] = $this->language->get('error_mobile');
		} elseif (!$this->model_account_customer->getTotalCustomersByTelephone($this->request->post['mobile'])) {
			$this->error['warning'] = $this->language->get('error_mobile');
		}

		$customer_info = $this->model_account_customer->getCustomerByTelephone($this->request->post['mobile']);

		if ($customer_info && !$customer_info['approved']) {
		    $this->error['warning'] = $this->language->get('error_approved');
		}
        
		return !$this->error;
	}
    protected function validateOld() {
		if (!isset($this->request->post['email'])) {
			$this->error['warning'] = $this->language->get('error_email');
		} elseif (!$this->model_account_customer->getTotalCustomersByEmail($this->request->post['email'])) {
			$this->error['warning'] = $this->language->get('error_email');
		}

		$customer_info = $this->model_account_customer->getCustomerByEmail($this->request->post['email']);

		if ($customer_info && !$customer_info['approved']) {
		    $this->error['warning'] = $this->language->get('error_approved');
		}

		return !$this->error;
	}
}
