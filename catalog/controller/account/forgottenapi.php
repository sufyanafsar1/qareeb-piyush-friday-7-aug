<?php
class ControllerAccountForgottenapi extends Controller
{
	private $error = array();

	public function index()
	{
		
		$this->load->language('account/forgotten');
		$this->load->model('account/customer');

		if ($this->validate()) {
			$this->load->language('mail/forgotten');

			//$password = substr(sha1(uniqid(mt_rand(), true)), 0, 10);
			$password = createPassword();
			$this->model_account_customer->editPassword($this->request->post['email'], $password);

			$customer_info = $this->model_account_customer->getCustomerByEmail($this->request->post['email']);

			$c_name = $customer_info['firstname'] . ' ' . $customer_info['lastname'];

			$ip_address = $this->request->server['REMOTE_ADDR'];

			$this->load->model('catalog/product');

			$template = $this->model_catalog_product->getEmailTemplate('customer-forgot-passwod-api-email');

			$tags = array("[CUSTOMER_NAME]", "[PASSWORD]", "[IP_ADDRESS]", "[SUBJECT]");
			$tagsValues = array($c_name, $password, $ip_address, $template['subject']);

			//get subject
			$subject = html_entity_decode($template['subject'], ENT_QUOTES, 'UTF-8');
			$subject = str_replace($tags, $tagsValues, $subject);
			$subject = html_entity_decode($subject, ENT_QUOTES);

			//get message body
			$msg = html_entity_decode($template['description'], ENT_QUOTES, 'UTF-8');
			$msg = str_replace($tags, $tagsValues, $msg);
			$msg = html_entity_decode($msg, ENT_QUOTES);

			$header = html_entity_decode($this->config->get('config_mail_header'), ENT_QUOTES, 'UTF-8');
			$header = str_replace($tags, $tagsValues, $header);
			$message = html_entity_decode($header, ENT_QUOTES);
			$message .= $msg;
			$message .= html_entity_decode($this->config->get('config_mail_footer'), ENT_QUOTES, 'UTF-8');

			$mail = new Mail();
			$mail->protocol = $this->config->get('config_mail_protocol');
			$mail->parameter = $this->config->get('config_mail_parameter');
			$mail->smtp_hostname = $this->config->get('config_mail_smtp_hostname');
			$mail->smtp_username = $this->config->get('config_mail_smtp_username');
			$mail->smtp_password = html_entity_decode($this->config->get('config_mail_smtp_password'), ENT_QUOTES, 'UTF-8');
			$mail->smtp_port = $this->config->get('config_mail_smtp_port');
			$mail->smtp_timeout = $this->config->get('config_mail_smtp_timeout');
			$mail->setTo($this->request->post['email']);
			$mail->setFrom($this->config->get('config_email'));
			$mail->setSender(html_entity_decode($this->config->get('config_name'), ENT_QUOTES, 'UTF-8'));
			$mail->setSubject($subject);
			$mail->setHtml($message);
			$mail->send();

			// Add to activity log
			$customer_info = $this->model_account_customer->getCustomerByEmail($this->request->post['email']);

			if ($customer_info) {
				$this->load->model('account/activity');

				$activity_data = array(
					'customer_id' => $customer_info['customer_id'],
					'name'        => $customer_info['firstname'] . ' ' . $customer_info['lastname']
				);

				$this->model_account_activity->addActivity('forgotten', $activity_data);
			}
			$data['message'] = 'success';
		} else {
			$data['message'] = $this->error['warning'];
		}
		print_r(json_encode($data));
	}

	protected function validate()
	{
		if (!isset($this->request->post['email'])) {
			$this->error['warning'] = $this->language->get('error_email');
		} elseif (!$this->model_account_customer->getTotalCustomersByEmail($this->request->post['email'])) {
			$this->error['warning'] = $this->language->get('error_email');
		}

		return !$this->error;
	}
}
