<?php
class ControllerAccountiosEditapi extends Controller {
	private $error = array();

	public function index() {
		$this->load->language('account/edit');
		$this->load->model('account/customer');

		if (isset($this->request->get['customer_id'])) {
			$customer_info = $this->model_account_customer->getCustomer($this->request->get['customer_id']);
			print_r(json_encode($customer_info));
		}
		
	}
	
	public function update() {
		$this->load->language('account/edit');
		$this->load->model('account/customer');
		
		if ($this->validate()) {
			//$this->model_account_customer->editCustomer($this->request->post);
			$currentInfo = $this->model_account_customer->getCustomerByEmail($this->request->post['email']);
			$this->event->trigger('pre.customer.edit', $data);

			$customer_id = $currentInfo['customer_id'];
	
			$this->db->query("UPDATE " . DB_PREFIX . "customer SET firstname = '" . $this->db->escape($this->request->post['firstname']) . "', lastname = '" . $this->db->escape($this->request->post['lastname']) . "', email = '" . $this->db->escape($this->request->post['email']) . "', telephone = '" . $this->db->escape($this->request->post['telephone']) . "', fax = '" . $this->db->escape($this->request->post['fax']) . "' WHERE customer_id = '" . (int)$customer_id . "'");
	
			$this->event->trigger('post.customer.edit', $customer_id);

			// Add to activity log
			$this->load->model('account/activity');
			
			$activity_data = array(
				'customer_id' => $currentInfo['customer_id'],
				'name'        => $currentInfo['firstname'] . ' ' . $currentInfo['lastname']
			);

			$this->model_account_activity->addActivity('edit', $activity_data);
			$data['message'] = 'success';
		}else{
			$data['message'] = 'fail';
			if(isset($this->error['firstname'])){
				$data['message'] = $this->error['firstname'];
			}else if(isset($this->error['lastname'])){
				$data['message'] = $this->error['lastname'];
			}else if(isset($this->error['email'])){
				$data['message'] = $this->error['email'];
			}else if(isset($this->error['warning'])){
				$data['message'] = $this->error['warning'];
			}else if(isset($this->error['telephone'])){
				$data['message'] = $this->error['telephone'];
			}	
		}
		print_r(json_encode($data));
	}
	
	public function update_profile() {
		$this->load->language('account/edit');
		$this->load->model('account/customer');
		if ($this->validate_profile()) {
			$this->db->query("UPDATE " . DB_PREFIX . "customer SET firstname = '" . $this->db->escape($this->request->post['firstname']) . "', lastname = '" . $this->db->escape($this->request->post['lastname']) . "' WHERE customer_id = '" . (int)$this->request->post['customer_id'] . "'");
			$this->db->query("UPDATE " . DB_PREFIX . "address SET firstname = '" . $this->db->escape($this->request->post['firstname']) . "', lastname = '" . $this->db->escape($this->request->post['lastname']) . "', address_1 = '" . $this->db->escape($this->request->post['address']) . "', city = '" . $this->db->escape($this->request->post['city']) . "' WHERE customer_id = '" . (int)$this->request->post['customer_id'] . "' AND address_id = '" . (int)$this->request->post['address_id'] . "'");

			// Add to activity log
			$this->load->model('account/activity');
			
			$activity_data = array(
				'customer_id' => $this->request->post['customer_id'],
				'name'        => $this->request->post['firstname'] . ' ' . $this->request->post['lastname']
			);

			$this->model_account_activity->addActivity('edit', $activity_data);
			$data['status'] = 'success';
		}else{
			$data['status'] = 'fail';
			if(isset($this->error['firstname'])){
				$data['msg'] = $this->error['firstname'];
			}else if(isset($this->error['lastname'])){
				$data['msg'] = $this->error['lastname'];
			}else if(isset($this->error['warning'])){
				$data['msg'] = $this->error['warning'];
			}else if(isset($this->error['address'])){
				$data['msg'] = $this->error['address'];
			}else if(isset($this->error['city'])){
				$data['msg'] = $this->error['city'];
			}	
		}
		$this->response->setOutput(json_encode($data));
	}
	
		protected function validate_profile() {
		if ((utf8_strlen(trim($this->request->post['firstname'])) < 1) || (utf8_strlen(trim($this->request->post['firstname'])) > 32)) {
			$this->error['firstname'] = $this->language->get('error_firstname');
		}

		if ((utf8_strlen(trim($this->request->post['lastname'])) < 1) || (utf8_strlen(trim($this->request->post['lastname'])) > 32)) {
			$this->error['lastname'] = $this->language->get('error_lastname');
		}
		
		$currentInfo = $this->model_account_customer->getCustomer($this->request->post['customer_id']);
		
		if (!count($currentInfo)) {
			$this->error['warning'] = $this->language->get('error_not_exists');
		}

		if (utf8_strlen($this->request->post['address']) < 3) {
			$this->error['address'] = $this->language->get('error_address');
		}

		if ((utf8_strlen($this->request->post['city']) < 1) || (utf8_strlen($this->request->post['city']) > 32)) {
			$this->error['city'] = $this->language->get('error_city');
		}

		return !$this->error;
	}
	

	protected function validate() {
		if ((utf8_strlen(trim($this->request->post['firstname'])) < 1) || (utf8_strlen(trim($this->request->post['firstname'])) > 32)) {
			$this->error['firstname'] = $this->language->get('error_firstname');
		}

		if ((utf8_strlen(trim($this->request->post['lastname'])) < 1) || (utf8_strlen(trim($this->request->post['lastname'])) > 32)) {
			$this->error['lastname'] = $this->language->get('error_lastname');
		}

		if ((utf8_strlen($this->request->post['email']) > 96) || !preg_match('/^[^\@]+@.*.[a-z]{2,15}$/i', $this->request->post['email'])) {
			$this->error['email'] = $this->language->get('error_email');
		}
		
		$currentInfo = $this->model_account_customer->getCustomerByEmail($this->request->post['email']);
		
		if (($currentInfo['email'] != $this->request->post['email']) && $this->model_account_customer->getTotalCustomersByEmail($this->request->post['email'])) {
			$this->error['warning'] = $this->language->get('error_exists');
		}

		if ((utf8_strlen($this->request->post['telephone']) < 3) || (utf8_strlen($this->request->post['telephone']) > 32)) {
			$this->error['telephone'] = $this->language->get('error_telephone');
		}

		return !$this->error;
	}
	
	public function get_profile(){
		$query = $this->db->query("SELECT adds.address_id, cus.firstname, cus.lastname, cus.email, cus.telephone, adds.city, adds.address_1 as address FROM " . DB_PREFIX . "customer AS cus LEFT JOIN " . DB_PREFIX . "address AS adds ON cus.customer_id = adds.customer_id WHERE cus.customer_id = '" . (int)$this->request->post['customer_id'] . "'");
		if($query->row){
		    $this->response->setOutput(json_encode($query->row));
		}else{
		    $this->response->setOutput(json_encode(array('msg' => 'No record found.')));
		}
		
	}
}