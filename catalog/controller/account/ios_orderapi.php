<?php
class ControllerAccountIosOrderApi extends Controller {
	private $error = array();

	public function index() {
		
		$data['orders'] = array();


		$language_id =  $this->request->get['language_id'];
	   
		$this->load->model('account/order');

		$query = $this->db->query("SELECT o.order_id, o.firstname, o.lastname, o.order_status_id, os.name as status, o.date_added, o.total, o.currency_code, o.currency_value FROM `" . DB_PREFIX . "order` o LEFT JOIN " . DB_PREFIX . "order_status os ON (o.order_status_id = os.order_status_id) WHERE o.customer_id = '" . (int)$this->request->get['customer_id'] . "' AND o.order_status_id > '0' AND o.store_id = '" . (int)$this->config->get('config_store_id') . "' AND os.language_id = '" . (int)$this->config->get('config_language_id') . "' ORDER BY o.order_id DESC");

		$results = $query->rows;

		foreach ($results as $result) {
			$product_total = $this->model_account_order->getTotalOrderProductsByOrderId($result['order_id']);
			$voucher_total = $this->model_account_order->getTotalOrderVouchersByOrderId($result['order_id']);
			
			$Total1 = $this->model_account_order->getOrderTotals1($result['order_id']);
			$Total2 = $this->model_account_order->getOrderTotals2($result['order_id']);
			
			$date_added = date("Y-m-d", strtotime($result['date_added']));				
			$date = '2018-11-02';
	
			$beforDate = 0;
			$afterDate = 0;
			if($result['date_added'] != '0000-00-00 00:00:00'){
				if($date_added <= $date){
					$Final_Total = ($Total1 + $Total2);				
				} else {
					$Final_Total = $Total1;		
				}				
			} else {
				$Final_Total = ($Total1 + $Total2);
			}
			
			$seller = $this->model_account_order->getSellerOrderProducts($result['order_id']);
		   
			$sellerName  = (isset($language_id) && $language_id==3) ? $seller[0]['lastname'] : $seller[0]['firstname'];
			
			$delivery_date_time = '';
			$orderDeliveryRes = $this->db->query("SELECT times FROM " . DB_PREFIX . "delivery_time WHERE order_id = '" . (int)$result['order_id'] . "'");
			$delivery_date_time =  $orderDeliveryRes->row['times'];
			if($delivery_date_time!=''){
				$tmpArr = explode(" - ",$delivery_date_time);
				//print_r($tmpArr);
				$delivery_date_time = date('H:i',strtotime(trim($tmpArr[0])));
				$delivery_date_time .= ' '.date('H:i',strtotime(trim($tmpArr[1])));
				
			}
			
		   
			$data['orders'][] = array(
				'order_id'   => $result['order_id'],
				'name'       => $result['firstname'] . ' ' . $result['lastname'],
				'status'     => $result['status'],
				'order_status_id'     => $result['order_status_id'],
				'delivery_date_time'     => $delivery_date_time,
				'date_added' => date($this->language->get('date_format_short'), strtotime($result['date_added'])),
				'products'   => ($product_total + $voucher_total),
				'total'      => $Final_Total,				
				'seller_name'  => $sellerName,
				'seller_id'  => $seller[0]['seller_id']
			);
		}
		print_r(json_encode($data['orders']));		
	}

	public function info() {
		$this->load->language('account/order');
		$this->load->model('account/order');
		$this->load->model('slot/timing');
		
		$order_id = (int)$this->request->get['order_id'];
		$customer_id = (int)$this->request->get['customer_id'];
		$order_info = $this->model_account_order->getOrderMobile($order_id,$customer_id);
		$language_id =  $this->request->get['language_id'];

		if (count($order_info)>0) {
			if ($order_info['invoice_no']) {
				$datainfo['invoice_no'] = $order_info['invoice_prefix'] . $order_info['invoice_no'];
			} else {
				$datainfo['invoice_no'] = '';
			}

			$datainfo['order_id'] = $this->request->get['order_id'];
			$datainfo['date_added'] = date($this->language->get('date_format_short'), strtotime($order_info['date_added']));
            $datainfo['delivery_date_time'] =  $this->model_slot_timing->getDeliveryTime($order_id);
			if ($order_info['payment_address_format']) {
				$format = $order_info['payment_address_format'];
			} else {
				$format = '{firstname} {lastname}' . "\n" . '{company}' . "\n" . '{address_1}' . "\n" . '{address_2}' . "\n" . '{city} {postcode}' . "\n" . '{zone}' . "\n" . '{country}';
			}

			$find = array(
				'{firstname}',
				'{lastname}',
				'{company}',
				'{address_1}',
				'{address_2}',
				'{city}',
				'{postcode}',
				'{zone}',
				'{zone_code}',
				'{country}'
			);

			$replace = array(
				'firstname' => $order_info['payment_firstname'],
				'lastname'  => $order_info['payment_lastname'],
				'company'   => $order_info['payment_company'],
				'address_1' => $order_info['payment_address_1'],
				'address_2' => $order_info['payment_address_2'],
				'city'      => $order_info['payment_city'],
				'postcode'  => $order_info['payment_postcode'],
				'zone'      => $order_info['payment_zone'],
				'zone_code' => $order_info['payment_zone_code'],
				'country'   => $order_info['payment_country']
			);

			$datainfo['payment_address'] = str_replace(array("\r\n", "\r", "\n"), '<br />', preg_replace(array("/\s\s+/", "/\r\r+/", "/\n\n+/"), '<br />', trim(str_replace($find, $replace, $format))));;

			$datainfo['payment_method'] = $order_info['payment_method']; 

			if ($order_info['shipping_address_format']) {
				$format = $order_info['shipping_address_format'];
			} else {
				$format = '{firstname} {lastname}' . "\n" . '{company}' . "\n" . '{address_1}' . "\n" . '{address_2}' . "\n" . '{city} {postcode}' . "\n" . '{zone}' . "\n" . '{country}';
			}

			$find = array(
				'{firstname}',
				'{lastname}',
				'{company}',
				'{address_1}',
				'{address_2}',
				'{city}',
				'{postcode}',
				'{zone}',
				'{zone_code}',
				'{country}'
			);

			$replace = array(
				'firstname' => $order_info['shipping_firstname'],
				'lastname'  => $order_info['shipping_lastname'],
				'company'   => $order_info['shipping_company'],
				'address_1' => $order_info['shipping_address_1'],
				'address_2' => $order_info['shipping_address_2'],
				'city'      => $order_info['shipping_city'],
				'postcode'  => $order_info['shipping_postcode'],
				'zone'      => $order_info['shipping_zone'],
				'zone_code' => $order_info['shipping_zone_code'],
				'country'   => $order_info['shipping_country']
			);

			$datainfo['shipping_address'] = str_replace(array("\r\n", "\r", "\n"), '<br />', preg_replace(array("/\s\s+/", "/\r\r+/", "/\n\n+/"), '<br />', trim(str_replace($find, $replace, $format))));

			$datainfo['shipping_method'] = $order_info['shipping_method'];

			$this->load->model('catalog/product');
			$this->load->model('tool/upload');

			// Products
			$datainfo['products'] = array();

			$products = $this->model_account_order->getOrderProducts($this->request->get['order_id']);

			foreach ($products as $product) {
				$option_data = array();

				$options = $this->model_account_order->getOrderOptions($this->request->get['order_id'], $product['order_product_id']);

				foreach ($options as $option) {
					if ($option['type'] != 'file') {
						$value = $option['value'];
					} else {
						$upload_info = $this->model_tool_upload->getUploadByCode($option['value']);

						if ($upload_info) {
							$value = $upload_info['name'];
						} else {
							$value = '';
						}
					}

					$option_data[] = array(
						'name'  => $option['name'],
						'value' => (utf8_strlen($value) > 20 ? utf8_substr($value, 0, 20) . '..' : $value)
					);
				}

				$product_info = $this->model_catalog_product->getProductBySellerIdAPI($product['product_id'],$product['seller_id'],$language_id) ; //$this->model_catalog_product->getProduct($product['product_id']);
                $sellerName  = (isset($language_id) && $language_id==3) ? $product['lastname'] : $product['firstname'];
				
				
				
				$datainfo['products'][] = array(
				    'id'=> $product['product_id'],
					'name'     => $product_info['name'],
					'model'    => $product['model'],
					'option'   => $option_data,
					'quantity' => $product['quantity'],
					'price'    => number_format($product['price'] + ($this->config->get('config_tax') ? $product['tax'] : 0),2),
					'total'    => $this->currency->format($product['total'] + ($this->config->get('config_tax') ? ($product['tax'] * $product['quantity']) : 0), $order_info['currency_code'], $order_info['currency_value']),
					'reorder'  => "",
					'return'   => "",
                    'seller_name'  => $sellerName,
                    'seller_id'  => $product['seller_id']
				);
			}

			// Voucher
			$datainfo['vouchers'] = array();

			$vouchers = $this->model_account_order->getOrderVouchers($this->request->get['order_id']);

			foreach ($vouchers as $voucher) {
				$datainfo['vouchers'][] = array(
					'description' => $voucher['description'],
					'amount'      => $this->currency->format($voucher['amount'], $order_info['currency_code'], $order_info['currency_value'])
				);
			}

			// Totals
			$datainfo['totals'] = array();

			$totals = $this->model_account_order->getOrderTotals($this->request->get['order_id']);
			
			$deliveryChargesArr = $this->db->query("
							select oc_sellers.delivery_charges, oc_sellers.seller_id, oc_sellers.firstname, oc_sellers.lastname 
							from oc_order_seller_shipping_fee 
							join oc_sellers on oc_sellers.seller_id = oc_order_seller_shipping_fee.seller_id 
							where oc_order_seller_shipping_fee.order_id = '".$this->request->get['order_id']."'")->rows;

							
							
			foreach ($totals as $total) {
				
				if( $total['code'] == 'total' ){
					$deliveryCharges = 0;
					foreach( $deliveryChargesArr as $deliveryChargesRow ){
						
						$deliveryCharges = $deliveryChargesRow['delivery_charges'];
						$data['totals'][] = array(
								'title' => 'Delivery Charges',
								'text'  => $this->currency->format( $deliveryCharges ),
						);
						$total['value'] += $deliveryCharges;
					}
					
				}
				
				if( $total['code'] == 'vat' ){
					
					//$total['title'] = 'Value Added Tax (5%25)';
					$total['title'] =  str_replace("Value Added Tax","VAT",$total['title']);

					
				}
				
				$datainfo['totals'][] = array(
					'title' => $total['title'],
					'text'  => number_format($total['value'],2),
				);
			}
			
			$datainfo['comment'] = nl2br($order_info['comment']);

			// History
			$datainfo['histories'] = array();

			$results = $this->model_account_order->getOrderHistories($this->request->get['order_id'],$language_id);

			foreach ($results as $result) {
				$datainfo['histories'][] = array(
					'date_added' => date($this->language->get('date_format_short'), strtotime($result['date_added'])),
					'status'     => $result['status'],
					'comment'    => $result['notify'] ? nl2br($result['comment']) : ''
				);
			}
			
		
			//header('Content-Type: application/json');
			echo json_encode($datainfo);
		}
	}
	
	
	public function validateOrderProduct(){

        $this->load->model('store/store');
        $this->load->model('catalog/product');
        $this->load->model('saccount/seller');
        $this->load->model('tool/image');

        //Parameters

        $product_id = (int)$this->request->get['product_id'];
        $seller_req = (int)$this->request->get['seller_req'];
        $is_area = (int)$this->request->get['is_area'];

        $data = array();
        $data['storeExist'] = false; $data['productExist'] = false;

        if($is_area==1) //GEt AreaId
        {
            $area_id = (int)$this->request->get['area_id'];
            $storesArray = $this->model_store_store->getStoresByAreaId($area_id);
        }else if($is_area==0){

                $lat = $this->request->get['lat'];
                $lng = $this->request->get['lng'];
                $radiusKm = $this->request->get['radius'];

            $storesArray = $this->model_store_store->getSellerByRadius($radiusKm, $lat, $lng);
	    }



        //If Stores Exist
        if($storesArray){
            foreach($storesArray as $store){
                $storesId[] = $store['seller_id'];
            }
             if(in_array($seller_req,$storesId)){
                 $data['storeExist'] = true;

                 $data['seller'] = $this->model_saccount_seller->getSeller($seller_req);
                 $data['seller']['image'] = $this->model_tool_image->resize($data['seller']['image'], 720, 406);
                  $data['seller']['banner'] = $this->model_tool_image->resize($data['seller']['banner'], 2880, 966);
                 $sellerProduct = $this->model_catalog_product->getProductBySellerIdAPI($product_id,$seller_req,1);
                 $data['productExist'] = ($sellerProduct) ? true : false;
             }

        }

        print_r(json_encode($data));
    }

}