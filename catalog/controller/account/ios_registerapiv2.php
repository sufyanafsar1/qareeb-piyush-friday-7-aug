<?php
class ControllerAccountiosRegisterapiv2 extends Controller {
	private $error = array();

	public function index() {
		$this->load->language('account/register');
		$this->load->model('account/customer');

		if ($this->validateRegister()) {
	
			 $data  = $this->request->post;
			
			 $data["password"] = createPassword();
			
			$customer_id = $this->model_account_customer->addCustomerTemp($data);
			$message = $this->language->get('text_otp') ." ". $data["password"] ." ". $this->language->get('text_otp_continue') ." Qareeb.com ";
			$smsData =  sendSMS($this->request->post['telephone'],$message);
			
					
		
			
			$data['message'] = 'success';
			$data['sentPassword'] = $data["password"];
			//$data['mobile'] = $this->request->post['telephone'];
		}else{
			
         
			if(isset($this->error['firstname'])){
				$data['message'] = $this->error['firstname'];
			}else if(isset($this->error['lastname'])){
				$data['message'] = $this->error['lastname'];
			}else if(isset($this->error['email'])){
				$data['message'] = $this->error['email'];
			}else if(isset($this->error['warning_mobile'])){
				$data['message'] = $this->error['warning_mobile'];
			}else if(isset($this->error['password'])){
				$data['message'] = $this->error['password'];
			}else if(isset($this->error['confirm'])){
				$data['message'] = $this->error['confirm'];
			}else if(isset($this->error['telephone'])){
				$data['message'] = $this->error['telephone'];
			}else if(isset($this->error['address_1'])){
				$data['message'] = $this->error['address_1'];
			}else if(isset($this->error['warning'])){
				$data['message'] = $this->error['warning'];
			}
			
			
		}
		
		print_r(json_encode($data));
	}
	
	
	

	public function validate() {
		if ((utf8_strlen(trim($this->request->post['firstname'])) < 1) || (utf8_strlen(trim($this->request->post['firstname'])) > 32)) {
			$this->error['firstname'] = $this->language->get('error_firstname');
		}

		if ((utf8_strlen(trim($this->request->post['lastname'])) < 1) || (utf8_strlen(trim($this->request->post['lastname'])) > 32)) {
			$this->error['lastname'] = $this->language->get('error_lastname');
		}

		if ((utf8_strlen($this->request->post['email']) > 96) || !preg_match('/^[^\@]+@.*.[a-z]{2,15}$/i', $this->request->post['email'])) {
			$this->error['email'] = $this->language->get('error_email');
		}

		if ($this->model_account_customer->getTotalCustomersByEmail($this->request->post['email'])) {
			$this->error['warning'] = $this->language->get('error_exists');
		}		


		return !$this->error;
	}
	
	private function validateRegister() {
	    
		if ((utf8_strlen(trim($this->request->post['firstname'])) < 1) || (utf8_strlen(trim($this->request->post['firstname'])) > 32)) {
			$this->error['firstname'] = $this->language->get('error_firstname');
		}

	if ((utf8_strlen(trim($this->request->post['lastname'])) < 1) || (utf8_strlen(trim($this->request->post['lastname'])) > 32)) {
			$this->error['lastname'] = $this->language->get('error_lastname');
		}

		if ((utf8_strlen($this->request->post['email']) > 96) || !preg_match('/^[^\@]+@.*.[a-z]{2,15}$/i', $this->request->post['email'])) {
			$this->error['email'] = $this->language->get('error_email');
		}

		if ($this->model_account_customer->getTotalCustomersByEmail($this->request->post['email'])) {
			$this->error['warning'] = $this->language->get('error_exists');
		}
        

		if ((utf8_strlen($this->request->post['telephone']) < 3) || (utf8_strlen($this->request->post['telephone']) > 32)) {
			$this->error['telephone'] = $this->language->get('error_telephone');
		}
		
		if(!preg_match('/^(9665)(5|0|3|6|4|9|1|8|7)([0-9]{7})$/', $this->request->post['telephone']))
        {
           $this->error['telephone'] = $this->language->get('error_telephone');
        }
        if ($this->model_account_customer->getTotalCustomersByTelephone($this->request->post['telephone'])) {
			$this->error['warning_mobile'] = $this->language->get('error_exists_mobile');
		}

		if ((utf8_strlen(trim($this->request->post['address_1'])) < 3) || (utf8_strlen(trim($this->request->post['address_1'])) > 128)) {
			$this->error['address_1'] = $this->language->get('error_address_1');
		}
      
		return !$this->error;
	}
	
	public function  updateUserMobileId(){
		$this->load->model('account/customer');
		$userMobileData  = $this->request->post;
		$this->model_account_customer->updateUserMobileId($userMobileData);

		$data['message'] = 'success';
         print_r(json_encode($data));

	}
	
	
}