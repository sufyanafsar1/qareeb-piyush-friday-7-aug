<?php
class ControllerAccountiosRegisterapiv3 extends Controller {
	private $error = array();

	public function index() {
		
		$this->load->language('account/register');
		$this->load->model('account/customer');

		if ($this->validateRegister()) {

			 $data_post  = $this->request->post;
			
			 $data_post["password"] = $this->request->post['password'];
			
			$customer_id = $this->model_account_customer->addCustomerTemp($data_post);
			//$message = $this->language->get('text_otp') ." ". $data_post["password"] ." ". $this->language->get('text_otp_continue') ." Qareeb.com ";
			//$smsData =  sendSMS($this->request->post['telephone'],$message);
			
			
			$customerdata = $this->model_account_customer->getTempCustomer($this->request->post['telephone'],$this->request->post['password']);	
			if(isset($customerdata) && count($customerdata) > 0)
			  {
				$customer_id = $this->model_account_customer->addCustomerIos($customerdata);
				if(isset($customer_id) && $customer_id > 0) {  

				$data['success'] = true;
                
				$query1 = $this->db->query("SELECT * from oc_customer where customer_id='" . $customer_id . "'");
 
				$data['customer']= $query1->row;
   
                $this->model_account_customer->deleteTempCustomer($this->request->post['telephone'],$this->request->post['password']); 
				
				$this->load->language('mail/forgotten');
			
				$link = $this->url->link('account/forgotten', 'SSL');
				
				$c_name = $customerdata['firstname'] . ' ' . $customerdata['lastname'];
							
				$click_here_link .= '<a href="'.$link.'" >click here</a>';
				
				$this->load->model('catalog/product');	

				$template = $this->model_catalog_product->getEmailTemplate('customer-register-email');	

				$tags = array("[CUSTOMER_NAME]", "[CUSTOMER_EMAIL]", "[CUSTOMER_PASSWORD]", "[CLICK_HERE_LINK]");
				$tagsValues = array($c_name, $customerdata['email'], $customerdata['password'], $click_here_link);
				
				//get subject
				$subject = html_entity_decode($template['subject'], ENT_QUOTES, 'UTF-8');
				$subject = str_replace($tags, $tagsValues, $subject);
				$subject = html_entity_decode($subject, ENT_QUOTES);
					
				//get message body
				$msg = html_entity_decode($template['description'], ENT_QUOTES, 'UTF-8');
				$msg = str_replace($tags, $tagsValues, $msg);
				$msg = html_entity_decode($msg, ENT_QUOTES);
						
				$header = html_entity_decode($this->config->get('config_mail_header'), ENT_QUOTES, 'UTF-8');
				$header = str_replace($tags, $tagsValues, $header);
				$message = html_entity_decode($header, ENT_QUOTES);
				$message .= $msg;
				$message .= html_entity_decode($this->config->get('config_mail_footer'), ENT_QUOTES, 'UTF-8');
				// echo $message;
				// exit;
				$mail = new Mail();
				$mail->protocol = $this->config->get('config_mail_protocol');
				$mail->parameter = $this->config->get('config_mail_parameter');
				$mail->smtp_hostname = $this->config->get('config_mail_smtp_hostname');
				$mail->smtp_username = $this->config->get('config_mail_smtp_username');
				$mail->smtp_password = html_entity_decode($this->config->get('config_mail_smtp_password'), ENT_QUOTES, 'UTF-8');
				$mail->smtp_port = $this->config->get('config_mail_smtp_port');
				$mail->smtp_timeout = $this->config->get('config_mail_smtp_timeout');
				$mail->setTo($customerdata['email']);
				$mail->setFrom($this->config->get('config_email'));
				$mail->setSender(html_entity_decode($this->config->get('config_name'), ENT_QUOTES, 'UTF-8'));
				$mail->setSubject(html_entity_decode($subject, ENT_QUOTES, 'UTF-8'));
				// $mail->setText(html_entity_decode($message, ENT_QUOTES, 'UTF-8'));
				$mail->setHtml($message);
				$mail->send();				
				
					// $data['success'] = true;
					 // $query1 = $this->db->query("SELECT * from oc_customer where customer_id='" . $customer_id . "'");
	 
					// $data['customer']= $query1->row;
	   
					// $this->model_account_customer->deleteTempCustomer($this->request->post['mobile'],$this->request->post['password']); 
					// $link = $this->url->link('account/forgotten', 'SSL');
					// $c_name = $customerdata['firstname'] . ' ' . $customerdata['lastname'];
					// $subject = "Thank you for registering at Qareeb.com";
					// $message  = '<html dir="ltr" lang="en">' . "\n";
					// $message .= '  <head>' . "\n";
					// $message .= '    <title> Subject </title>' . "\n";
					// $message .= '    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">' . "\n";
					// $message .= '  </head>' . "\n";
					// $message .= '  <body> ' . "\n";
					// $message .= '  <p>Dear '.$c_name.' </p>' . "</p><br>";
					// $message .= '  <p>Welcome https://www.qareeb.com Thank you for registering with us.' . "</p><br>";
					// $message .= '  <p>Your login information is as follows: ' . "</p>";
					// $message .= '  <p>Username : '.$customerdata['email']. "</p>";
					// $message .= '  <p>Password: ' .$customerdata['password']. "</p><br><br>";
					// $message .= '  <p>To login, please access here type the username given above.' . "</p><br>";
					// $message .= '  <p>If you wish to reset your password please <a href="' . $link . '">click here</a>' . "</p><br>";
					// $message .= '  <p>We request you to make note of this information and keep safe place for future reference.' . "</p><br>";
					// $message .= '  <p>For any questions or queries, please contact Customer Support at support@qareeb.com' . "</p>";
					

					// $message .= '</body></html>' . "\n";
					// $mail = new Mail();
					// $mail->protocol = $this->config->get('config_mail_protocol');
					// $mail->parameter = $this->config->get('config_mail_parameter');
					// $mail->smtp_hostname = $this->config->get('config_mail_smtp_hostname');
					// $mail->smtp_username = $this->config->get('config_mail_smtp_username');
					// $mail->smtp_password = html_entity_decode($this->config->get('config_mail_smtp_password'), ENT_QUOTES, 'UTF-8');
					// $mail->smtp_port = $this->config->get('config_mail_smtp_port');
					// $mail->smtp_timeout = $this->config->get('config_mail_smtp_timeout');
					// $mail->setTo($customerdata['email']);
					// $mail->setFrom($this->config->get('config_email'));
					// $mail->setSender(html_entity_decode($this->config->get('config_name'), ENT_QUOTES, 'UTF-8'));
					// $mail->setSubject(html_entity_decode($subject, ENT_QUOTES, 'UTF-8'));
					//$mail->setText(html_entity_decode($message, ENT_QUOTES, 'UTF-8'));
					// $mail->setHtml($message);
					// $mail->send(); 
				 } else{
					$responce["error"]="customer can not be added";
				 }
			  }	else{
			   $responce["error"]="customer not found";
		      }	
		
			
			//$data['message'] = 'success';
			//$data['sentPassword'] = $data["password"];
			//$data['mobile'] = $this->request->post['telephone'];
		}else{
			
         
			if(isset($this->error['firstname'])){
				$data['message'] = $this->error['firstname'];
			}else if(isset($this->error['lastname'])){
				$data['message'] = $this->error['lastname'];
			}else if(isset($this->error['email'])){
				$data['message'] = $this->error['email'];
			}else if(isset($this->error['warning_mobile'])){
				$data['message'] = $this->error['warning_mobile'];
			}else if(isset($this->error['password'])){
				$data['message'] = $this->error['password'];
			}else if(isset($this->error['confirm'])){
				$data['message'] = $this->error['confirm'];
			}else if(isset($this->error['telephone'])){
				$data['message'] = $this->error['telephone'];
			}else if(isset($this->error['address_1'])){
				$data['message'] = $this->error['address_1'];
			}else if(isset($this->error['warning'])){
				$data['message'] = $this->error['warning'];
			} 
			// else if (isset($this->error['region_id'])) {
			// 	$data['message'] = $this->error['region_id'];
			// } else if (isset($this->error['city_id'])) {
			// 	$data['message'] = $this->error['city_id'];
			// }
			
			
		}
		
		print_r(json_encode($data));
	}
	
	
	

	public function validate() {
		if ((utf8_strlen(trim($this->request->post['firstname'])) < 1) || (utf8_strlen(trim($this->request->post['firstname'])) > 32)) {
			$this->error['firstname'] = $this->language->get('error_firstname');
		}

		if ((utf8_strlen(trim($this->request->post['lastname'])) < 1) || (utf8_strlen(trim($this->request->post['lastname'])) > 32)) {
			$this->error['lastname'] = $this->language->get('error_lastname');
		}

		if ((utf8_strlen($this->request->post['email']) > 96) || !preg_match('/^[^\@]+@.*.[a-z]{2,15}$/i', $this->request->post['email'])) {
			$this->error['email'] = $this->language->get('error_email');
		}

		if ($this->model_account_customer->getTotalCustomersByEmail($this->request->post['email'])) {
			$this->error['warning'] = $this->language->get('error_exists');
		}		


		return !$this->error;
	}
	
	private function validateRegister() {
		
		if ((utf8_strlen(html_entity_decode($this->request->post['password'], ENT_QUOTES, 'UTF-8')) < 4) || (utf8_strlen(html_entity_decode($this->request->post['password'], ENT_QUOTES, 'UTF-8')) > 40)) {
			$this->error['password'] = $this->language->get('error_password');
		}

		if ($this->request->post['confirm'] != $this->request->post['password']) {
			$this->error['confirm'] = $this->language->get('error_confirm');
		}
	    
		if ((utf8_strlen(trim($this->request->post['firstname'])) < 1) || (utf8_strlen(trim($this->request->post['firstname'])) > 32)) {
			$this->error['firstname'] = $this->language->get('error_firstname');
		}

		if ((utf8_strlen(trim($this->request->post['lastname'])) < 1) || (utf8_strlen(trim($this->request->post['lastname'])) > 32)) {
			$this->error['lastname'] = $this->language->get('error_lastname');
		}

		if ((utf8_strlen($this->request->post['email']) > 96) || !preg_match('/^[^\@]+@.*.[a-z]{2,15}$/i', $this->request->post['email'])) {
			$this->error['email'] = $this->language->get('error_email');
		}

		if ($this->model_account_customer->getTotalCustomersByEmail($this->request->post['email'])) {
			$this->error['warning'] = $this->language->get('error_exists');
		}
        

		if ((utf8_strlen($this->request->post['telephone']) < 3) || (utf8_strlen($this->request->post['telephone']) > 32)) {
			$this->error['telephone'] = $this->language->get('error_telephone');
		}
		
		if(!preg_match('/^(9665)(5|0|3|6|4|9|1|8|7)([0-9]{7})$/', $this->request->post['telephone']))
        {
           $this->error['telephone'] = $this->language->get('error_telephone');
        }
        if ($this->model_account_customer->getTotalCustomersByTelephone($this->request->post['telephone'])) {
			$this->error['warning_mobile'] = $this->language->get('error_exists_mobile');
		}

		if ((utf8_strlen(trim($this->request->post['address_1'])) < 3) || (utf8_strlen(trim($this->request->post['address_1'])) > 128)) {
			$this->error['address_1'] = $this->language->get('error_address_1');
		}

		// if ($this->request->post['region_id'] == '') {
		// 	$this->error['region_id'] = 'Region field is required!';
		// }

		// if ($this->request->post['city_id'] == '') {
		// 	$this->error['city_id'] = 'City field is required!';
		// }
      
		return !$this->error;
	}
	
	public function  updateUserMobileId(){
		$this->load->model('account/customer');
		$userMobileData  = $this->request->post;
		$this->model_account_customer->updateUserMobileId($userMobileData);

		$data['message'] = 'success';
         print_r(json_encode($data));

	}
	
	
}