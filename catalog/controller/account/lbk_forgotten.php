<?php
class ControllerAccountForgotten extends Controller {
	private $error = array();

	public function index() {
	  
		if ($this->customer->isLogged()) {
		 
			$this->response->redirect($this->url->link('account/account', '', 'SSL'));
		}
  

		$this->language->load('account/forgotten');

		$this->document->setTitle($this->language->get('heading_title'));

		$this->load->model('account/customer');
		if (($this->request->server['REQUEST_METHOD'] == 'POST')) {
			if (isset($this->request->post['mobile'] ) && $this->request->post['mobile'] != null ) {
				if ($this->validate()) {
					$this->load->language('mail/forgotten');

					$password = createPassword();
		            $customer_info = $this->model_account_customer->getCustomerByTelephone($this->request->post['mobile']);
		           
		            if($customer_info)
		            {
		                $this->model_account_customer->editPassword($customer_info['email'], $password);
		                $message = sprintf($this->language->get('text_subject'), html_entity_decode($this->config->get('config_name'), ENT_QUOTES, 'UTF-8')) . "\n";
		                $message .= $this->language->get('text_password') ." ". $password. "\n";
		                             
		                //$smsData =  tetSMS($this->request->post['mobile'],$message);
		                 $smsData =  sendSMS($this->request->post['mobile'],$message);
		                    
		               	$this->load->model('account/activity');

		                // Add to activity log
						$activity_data = array(
							'customer_id' => $customer_info['customer_id'],
							'name'        => $customer_info['firstname'] . ' ' . $customer_info['lastname']
						);

						$this->model_account_activity->addActivity('forgotten', $activity_data);
		                
		            }
					
					$this->session->data['success'] = $this->language->get('text_success');
		          
					//$this->response->redirect($this->url->link('account/login', '', 'SSL'));
		            $this->response->redirect($this->url->link('account/forgotten', '', 'SSL'));
		        }
			}
			elseif (isset($this->request->post['email'] ) && $this->request->post['email'] != null) {
				if ($this->validateOld()) {
					$this->load->language('mail/forgotten');

					$password = createPassword();
		            $customer_info = $this->model_account_customer->getCustomerByEmail($this->request->post['email']);
		            if($customer_info)
		            {
						
			
						$code = $password;

						$this->model_account_customer->editCode($this->request->post['email'], $code);
						$email = $this->request->post['email'];
			
		                $this->model_account_customer->editPassword($customer_info['email'], $password);
		                $message = sprintf($this->language->get('text_subject'), html_entity_decode($this->config->get('config_name'), ENT_QUOTES, 'UTF-8')) . "\n";
		                $c_name = $customer_info['firstname'] . ' ' . $customer_info['lastname'];
						$logo_link= $this->config->get('config_url'). 'image/catalog/salatty_icons/careeb_small_logo.png';
		                $reset_link =$this->url->link('account/reset_pwd_link', 'code=' . $code .'&email=' . $email, 'SSL');

		                $subject = sprintf($this->language->get('text_subject'), html_entity_decode($this->config->get('config_name'), ENT_QUOTES, 'UTF-8'));
						$message  = sprintf($this->language->get('text_greeting'), html_entity_decode($this->config->get('config_name'), ENT_QUOTES, 'UTF-8')) . "\n\n";
						// $message .= $this->language->get('text_change') .$password. "\n\n";
						// // $message .= $this->url->link('account/reset', 'code=' . $password, true) . "\n\n";
						// $message .= sprintf($this->language->get('text_ip'), $this->request->server['REMOTE_ADDR']) . "\n\n";
						

						$message  = '<html dir="ltr" lang="en">' . "\n";
     					$message .= '  <head>' . "\n";
    					$message .= '    <title>' . $this->request->post['subject'] . '</title>' . "\n";
     					$message .= '    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">' . "\n";
     					$message .= '  </head>' . "\n";
     					$message .= '  <body><img style="margin-left:85px" src="' . $logo_link . '" />' . "\n";
						$message .= '  <p>Dear '.$c_name.' </p>' . "\n\n\n";
						
						$message .= '  <p>A new password was requested for careeb.com customer account. </p>' . "\n";						
						$message .= '  <p>To reset your password click on the link below:</p>'. "\n";
						
						//$message .= '  <p>We got a request to reset your careeb.com password, </p>' . "\n";
						//$message .= '  <p>please use your new Password  '.$password.' </p>' . "\n\n\n";						
						//$message .= '  <p>If you did not request this Password simply ignore this message </p>' . "\n";
						
						$message .= '<a style="display: block;width: 160px;height: 25px;background: #8bc42f;padding: 14px 5px 6px 6px;text-align: center;border-radius: 5px;color: white;font-weight: bold;"  href="' . $reset_link . '" >Reset Your Password</a>' . "\n\n";
						
						$message .= '  <p>The IP used to make this request was: '.$this->request->server['REMOTE_ADDR'].'</p>' . "\n";
						
						
     					$message .= '<tr>
								<td align="center" valign="top" id="m_-796796385940949692m_-3558039569370535758templateFooter" style="background:#333333 none no-repeat center/cover;background-color:#333333;background-image:none;background-repeat:no-repeat;background-position:center;background-size:cover;border-top:0;border-bottom:0;padding-top:0px;padding-bottom:0px">
									
									<table align="center" border="0" cellpadding="0" cellspacing="0" width="100%" class="m_-796796385940949692m_-3558039569370535758templateContainer" style="border-collapse:collapse;max-width:600px!important">
										<tbody><tr>
                                			<td valign="top" class="m_-796796385940949692m_-3558039569370535758footerContainer" style="background:transparent none no-repeat center/cover;background-color:transparent;background-image:none;background-repeat:no-repeat;background-position:center;background-size:cover;border-top:0;border-bottom:0;padding-top:0;padding-bottom:0"><table border="0" cellpadding="0" cellspacing="0" width="100%" class="m_-796796385940949692m_-3558039569370535758mcnFollowBlock" style="min-width:100%;border-collapse:collapse">
    <tbody class="m_-796796385940949692m_-3558039569370535758mcnFollowBlockOuter">
        <tr>
            <td align="center" valign="top" style="padding:9px" class="m_-796796385940949692m_-3558039569370535758mcnFollowBlockInner">
                <table border="0" cellpadding="0" cellspacing="0" width="100%" class="m_-796796385940949692m_-3558039569370535758mcnFollowContentContainer" style="min-width:100%;border-collapse:collapse">
    <tbody><tr>
        <td align="center" style="padding-left:9px;padding-right:9px">
            <table border="0" cellpadding="0" cellspacing="0" width="100%" style="min-width:100%;border-collapse:collapse" class="m_-796796385940949692m_-3558039569370535758mcnFollowContent">
                <tbody><tr>
                    <td align="center" valign="top" style="padding-top:9px;padding-right:9px;padding-left:9px">
                        <table align="center" border="0" cellpadding="0" cellspacing="0" style="border-collapse:collapse">
                            <tbody><tr>
                                <td align="center" valign="top">
                                    

                                        


                                            <table align="left" border="0" cellpadding="0" cellspacing="0" style="display:inline;border-collapse:collapse">
                                                <tbody><tr>
                                                    <td valign="top" style="padding-right:10px;padding-bottom:9px" class="m_-796796385940949692m_-3558039569370535758mcnFollowContentItemContainer">
                                                        <table border="0" cellpadding="0" cellspacing="0" width="100%" class="m_-796796385940949692m_-3558039569370535758mcnFollowContentItem" style="border-collapse:collapse">
                                                            <tbody><tr>
                                                                <td align="left" valign="middle" style="padding-top:5px;padding-right:10px;padding-bottom:5px;padding-left:9px">
                                                                    <table align="left" border="0" cellpadding="0" cellspacing="0" width="" style="border-collapse:collapse">
                                                                        <tbody><tr>

                                                                                <td align="center" valign="middle" width="24" class="m_-796796385940949692m_-3558039569370535758mcnFollowIconContent">
                                                                                    <a href="http://salatty.us15.list-manage2.com/track/click?u=2be189a88860ff7a27590527c&amp;id=5e1380a50f&amp;e=8cc71b16e4" target="_blank" data-saferedirecturl="https://www.google.com/url?hl=en&amp;q=http://salatty.us15.list-manage2.com/track/click?u%3D2be189a88860ff7a27590527c%26id%3D5e1380a50f%26e%3D8cc71b16e4&amp;source=gmail&amp;ust=1497415163214000&amp;usg=AFQjCNHN5teuabNU82rOLcTN7zDhqMthFQ"><img src="https://ci3.googleusercontent.com/proxy/LTqnuW6bD1SXY3wzyRXm28PTDj-ALToQYjl7sjRGkkbtkuA-9cr1kRCIdu3kjlInfnJLuttucJW-5NK93ge4ch_6gAxLV7Y8wpAZFElRA-zCKa7RN6K2IgamPymPTyY5cNfsu4Fj5DMyUT0=s0-d-e1-ft#https://cdn-images.mailchimp.com/icons/social-block-v2/outline-light-instagram-48.png" style="display:block;border:0;height:auto;outline:none;text-decoration:none" height="24" width="24" class="CToWUd"></a>
                                                                                </td>


                                                                        </tr>
                                                                    </tbody></table>
                                                                </td>
                                                            </tr>
                                                        </tbody></table>
                                                    </td>
                                                </tr>
                                            </tbody></table>

                                        

                                        


                                            <table align="left" border="0" cellpadding="0" cellspacing="0" style="display:inline;border-collapse:collapse">
                                                <tbody><tr>
                                                    <td valign="top" style="padding-right:10px;padding-bottom:9px" class="m_-796796385940949692m_-3558039569370535758mcnFollowContentItemContainer">
                                                        <table border="0" cellpadding="0" cellspacing="0" width="100%" class="m_-796796385940949692m_-3558039569370535758mcnFollowContentItem" style="border-collapse:collapse">
                                                            <tbody><tr>
                                                                <td align="left" valign="middle" style="padding-top:5px;padding-right:10px;padding-bottom:5px;padding-left:9px">
                                                                    <table align="left" border="0" cellpadding="0" cellspacing="0" width="" style="border-collapse:collapse">
                                                                        <tbody><tr>

                                                                                <td align="center" valign="middle" width="24" class="m_-796796385940949692m_-3558039569370535758mcnFollowIconContent">
                                                                                    <a href="http://salatty.us15.list-manage2.com/track/click?u=2be189a88860ff7a27590527c&amp;id=1246f08f29&amp;e=8cc71b16e4" target="_blank" data-saferedirecturl="https://www.google.com/url?hl=en&amp;q=http://salatty.us15.list-manage2.com/track/click?u%3D2be189a88860ff7a27590527c%26id%3D1246f08f29%26e%3D8cc71b16e4&amp;source=gmail&amp;ust=1497415163214000&amp;usg=AFQjCNHwQr2Ui_xFxq_5t_Qy2CEuIbsr6A"><img src="https://ci4.googleusercontent.com/proxy/8j-DDnLusVH50YFUKm2i383mq41zzkTF0OmfaicBkjqbHcMUathKBT2sedC9niEZakoPEtRHargdZ4RbQjfIuq8GbtTu18d89xfHhaPIB2F5Lpp4cNaLZoDImoeXaRHVsy7_i-xdOFXoMg=s0-d-e1-ft#https://cdn-images.mailchimp.com/icons/social-block-v2/outline-light-facebook-48.png" style="display:block;border:0;height:auto;outline:none;text-decoration:none" height="24" width="24" class="CToWUd"></a>
                                                                                </td>


                                                                        </tr>
                                                                    </tbody></table>
                                                                </td>
                                                            </tr>
                                                        </tbody></table>
                                                    </td>
                                                </tr>
                                            </tbody></table>

                                        

                                        


                                            <table align="left" border="0" cellpadding="0" cellspacing="0" style="display:inline;border-collapse:collapse">
                                                <tbody><tr>
                                                    <td valign="top" style="padding-right:10px;padding-bottom:9px" class="m_-796796385940949692m_-3558039569370535758mcnFollowContentItemContainer">
                                                        <table border="0" cellpadding="0" cellspacing="0" width="100%" class="m_-796796385940949692m_-3558039569370535758mcnFollowContentItem" style="border-collapse:collapse">
                                                            <tbody><tr>
                                                                <td align="left" valign="middle" style="padding-top:5px;padding-right:10px;padding-bottom:5px;padding-left:9px">
                                                                    <table align="left" border="0" cellpadding="0" cellspacing="0" width="" style="border-collapse:collapse">
                                                                        <tbody><tr>

                                                                                <td align="center" valign="middle" width="24" class="m_-796796385940949692m_-3558039569370535758mcnFollowIconContent">
                                                                                    <a href="http://salatty.us15.list-manage.com/track/click?u=2be189a88860ff7a27590527c&amp;id=89458e06ab&amp;e=8cc71b16e4" target="_blank" data-saferedirecturl="https://www.google.com/url?hl=en&amp;q=http://salatty.us15.list-manage.com/track/click?u%3D2be189a88860ff7a27590527c%26id%3D89458e06ab%26e%3D8cc71b16e4&amp;source=gmail&amp;ust=1497415163215000&amp;usg=AFQjCNGAt9kuMsKoNhux3BvDJUsazNFk0g"><img src="https://ci6.googleusercontent.com/proxy/eaQG4rpaZxGwH-rEAXH75vzcChjc63kc1kaLs3r7RuNM_pKZzdAi--XXmC7Hshqi15T7UcrQb4-Jyy5uCUL2jnst3AVeYh9BucqKdnT3SWD1LP9xJT3lKcewZZ7CV5wwYQI6moZb1XGb=s0-d-e1-ft#https://cdn-images.mailchimp.com/icons/social-block-v2/outline-light-twitter-48.png" style="display:block;border:0;height:auto;outline:none;text-decoration:none" height="24" width="24" class="CToWUd"></a>
                                                                                </td>


                                                                        </tr>
                                                                    </tbody></table>
                                                                </td>
                                                            </tr>
                                                        </tbody></table>
                                                    </td>
                                                </tr>
                                            </tbody></table>

                                        

                                        


                                            <table align="left" border="0" cellpadding="0" cellspacing="0" style="display:inline;border-collapse:collapse">
                                                <tbody><tr>
                                                    <td valign="top" style="padding-right:10px;padding-bottom:9px" class="m_-796796385940949692m_-3558039569370535758mcnFollowContentItemContainer">
                                                        <table border="0" cellpadding="0" cellspacing="0" width="100%" class="m_-796796385940949692m_-3558039569370535758mcnFollowContentItem" style="border-collapse:collapse">
                                                            <tbody><tr>
                                                                <td align="left" valign="middle" style="padding-top:5px;padding-right:10px;padding-bottom:5px;padding-left:9px">
                                                                    <table align="left" border="0" cellpadding="0" cellspacing="0" width="" style="border-collapse:collapse">
                                                                        <tbody><tr>

                                                                                <td align="center" valign="middle" width="24" class="m_-796796385940949692m_-3558039569370535758mcnFollowIconContent">
                                                                                    <a href="http://salatty.us15.list-manage1.com/track/click?u=2be189a88860ff7a27590527c&amp;id=2a844cd7f2&amp;e=8cc71b16e4" target="_blank" data-saferedirecturl="https://www.google.com/url?hl=en&amp;q=http://salatty.us15.list-manage1.com/track/click?u%3D2be189a88860ff7a27590527c%26id%3D2a844cd7f2%26e%3D8cc71b16e4&amp;source=gmail&amp;ust=1497415163215000&amp;usg=AFQjCNFzpiQs5INrMEJv5C9ng6sQ88BX8w"><img src="https://ci6.googleusercontent.com/proxy/MADOI2jFbLatf1-ebbcl7-uoUnt1nnWKiKJZiCEYwnrD8A2-g_kyrvqqlokUPzWAcIVMT_uTCunFZ2pp0QHMpdSWGsj-4pSV-mQaioMqno6m-8wGENcz2aP0PYIauGXModewkbMW-65g=s0-d-e1-ft#https://cdn-images.mailchimp.com/icons/social-block-v2/outline-light-youtube-48.png" style="display:block;border:0;height:auto;outline:none;text-decoration:none" height="24" width="24" class="CToWUd"></a>
                                                                                </td>


                                                                        </tr>
                                                                    </tbody></table>
                                                                </td>
                                                            </tr>
                                                        </tbody></table>
                                                    </td>
                                                </tr>
                                            </tbody></table>

                                        

                                        


                                            <table align="left" border="0" cellpadding="0" cellspacing="0" style="display:inline;border-collapse:collapse">
                                                <tbody><tr>
                                                    <td valign="top" style="padding-right:10px;padding-bottom:9px" class="m_-796796385940949692m_-3558039569370535758mcnFollowContentItemContainer">
                                                        <table border="0" cellpadding="0" cellspacing="0" width="100%" class="m_-796796385940949692m_-3558039569370535758mcnFollowContentItem" style="border-collapse:collapse">
                                                            <tbody><tr>
                                                                <td align="left" valign="middle" style="padding-top:5px;padding-right:10px;padding-bottom:5px;padding-left:9px">
                                                                    <table align="left" border="0" cellpadding="0" cellspacing="0" width="" style="border-collapse:collapse">
                                                                        <tbody><tr>

                                                                                <td align="center" valign="middle" width="24" class="m_-796796385940949692m_-3558039569370535758mcnFollowIconContent">
                                                                                    <a href="http://salatty.us15.list-manage.com/track/click?u=2be189a88860ff7a27590527c&amp;id=5c9f94b5ec&amp;e=8cc71b16e4" target="_blank" data-saferedirecturl="https://www.google.com/url?hl=en&amp;q=http://salatty.us15.list-manage.com/track/click?u%3D2be189a88860ff7a27590527c%26id%3D5c9f94b5ec%26e%3D8cc71b16e4&amp;source=gmail&amp;ust=1497415163215000&amp;usg=AFQjCNFjw8YapEn_L9IE6hGtphqUq_Nm_w"><img src="https://ci5.googleusercontent.com/proxy/5y8PimBRwcJBDb92kgDtUSrz_5KShhUQXNrb_Q28YQUosmMtQUAzDq9N6tEwFsnVGQP4sLp24o68NhNmE3IMgZZ6NHCKjsRv-MAYKV-cZyd_4N9RZ82T8Z0xDMI-awQpxRK2_me4=s0-d-e1-ft#https://cdn-images.mailchimp.com/icons/social-block-v2/outline-light-link-48.png" style="display:block;border:0;height:auto;outline:none;text-decoration:none" height="24" width="24" class="CToWUd"></a>
                                                                                </td>


                                                                        </tr>
                                                                    </tbody></table>
                                                                </td>
                                                            </tr>
                                                        </tbody></table>
                                                    </td>
                                                </tr>
                                            </tbody></table>

                                        

                                        


                                            <table align="left" border="0" cellpadding="0" cellspacing="0" style="display:inline;border-collapse:collapse">
                                                <tbody><tr>
                                                    <td valign="top" style="padding-right:0;padding-bottom:9px" class="m_-796796385940949692m_-3558039569370535758mcnFollowContentItemContainer">
                                                        <table border="0" cellpadding="0" cellspacing="0" width="100%" class="m_-796796385940949692m_-3558039569370535758mcnFollowContentItem" style="border-collapse:collapse">
                                                            <tbody><tr>
                                                                <td align="left" valign="middle" style="padding-top:5px;padding-right:10px;padding-bottom:5px;padding-left:9px">
                                                                    <table align="left" border="0" cellpadding="0" cellspacing="0" width="" style="border-collapse:collapse">
                                                                        <tbody><tr>

                                                                                <td align="center" valign="middle" width="24" class="m_-796796385940949692m_-3558039569370535758mcnFollowIconContent">
                                                                                    <a href="mailto:info@careeb.com" target="_blank"><img src="https://ci6.googleusercontent.com/proxy/1ndAbrj-5Ey_WnqItTjgqASqs2hoYmIkv9VRKJm_isa4ZnX36yc1qyKXTbkvC4bEpg7pAKiEHMJbNnQKUTBgFvBVoBDhWXm4Sxb8t51dGrjwRRo3tSfX-X4Uw_uTbGzEYGV2LY1hWEAOPhUbgt48-Nc=s0-d-e1-ft#https://cdn-images.mailchimp.com/icons/social-block-v2/outline-light-forwardtofriend-48.png" style="display:block;border:0;height:auto;outline:none;text-decoration:none" height="24" width="24" class="CToWUd"></a>
                                                                                </td>


                                                                        </tr>
                                                                    </tbody></table>
                                                                </td>
                                                            </tr>
                                                        </tbody></table>
                                                    </td>
                                                </tr>
                                            </tbody></table>

                                        

                                    
                                </td>
                            </tr>
                        </tbody></table>
                    </td>
                </tr>
            </tbody></table>
        </td>
    </tr>
</tbody></table>

            </td>
        </tr>
    </tbody>
</table><table border="0" cellpadding="0" cellspacing="0" width="100%" class="m_-796796385940949692m_-3558039569370535758mcnDividerBlock" style="min-width:100%;border-collapse:collapse;table-layout:fixed!important">
    <tbody class="m_-796796385940949692m_-3558039569370535758mcnDividerBlockOuter">
        <tr>
            <td class="m_-796796385940949692m_-3558039569370535758mcnDividerBlockInner" style="min-width:100%;padding:0px 18px">
                <table class="m_-796796385940949692m_-3558039569370535758mcnDividerContent" border="0" cellpadding="0" cellspacing="0" width="100%" style="min-width:100%;border-top:1px solid #ffffff;border-collapse:collapse">
                    <tbody><tr>
                        <td>
                            <span></span>
                        </td>
                    </tr>
                </tbody></table>

            </td>
        </tr>
    </tbody>
</table><table border="0" cellpadding="0" cellspacing="0" width="100%" class="m_-796796385940949692m_-3558039569370535758mcnTextBlock" style="min-width:100%;border-collapse:collapse">
    <tbody class="m_-796796385940949692m_-3558039569370535758mcnTextBlockOuter">
        <tr>
            <td valign="top" class="m_-796796385940949692m_-3558039569370535758mcnTextBlockInner" style="padding-top:9px">
              	
			
				
                <table align="left" border="0" cellpadding="0" cellspacing="0" style="max-width:100%;min-width:100%;border-collapse:collapse" width="100%" class="m_-796796385940949692m_-3558039569370535758mcnTextContentContainer">
                    <tbody><tr>

                        <td valign="top" class="m_-796796385940949692m_-3558039569370535758mcnTextContent" style="padding:0px 18px 9px;line-height:200%;word-break:break-word;color:#ffffff;font-family:Helvetica;font-size:12px;text-align:center">

                            <span style="font-size:12px"><em>Copyright © Careeb, All rights reserved.</em></span>
                        </td>
                    </tr>
                </tbody></table>
				

				
            </td>
        </tr>
    </tbody>
</table></td>
										</tr>
									</tbody></table>
									
								</td>
                            </tr>';
						$message .= '</body></html>' . "\n";
		                $mail = new Mail();
						$mail->protocol = $this->config->get('config_mail_protocol');
						$mail->parameter = $this->config->get('config_mail_parameter');
						$mail->smtp_hostname = $this->config->get('config_mail_smtp_hostname');
						$mail->smtp_username = $this->config->get('config_mail_smtp_username');
						$mail->smtp_password = html_entity_decode($this->config->get('config_mail_smtp_password'), ENT_QUOTES, 'UTF-8');
						$mail->smtp_port = $this->config->get('config_mail_smtp_port');
						$mail->smtp_timeout = $this->config->get('config_mail_smtp_timeout');
						$mail->setTo($this->request->post['email']);
						$mail->setFrom($this->config->get('config_email'));
						$mail->setSender(html_entity_decode($this->config->get('config_name'), ENT_QUOTES, 'UTF-8'));
						$mail->setSubject(html_entity_decode($subject, ENT_QUOTES, 'UTF-8'));
						// $mail->setText(html_entity_decode($message, ENT_QUOTES, 'UTF-8'));
						$mail->setHtml($message);
						$mail->send();

		               	$this->load->model('account/activity');

		                // Add to activity log
						$activity_data = array(
							'customer_id' => $customer_info['customer_id'],
							'name'        => $customer_info['firstname'] . ' ' . $customer_info['lastname']
						);

						$this->model_account_activity->addActivity('forgotten', $activity_data);
		                
		            }
					
					$this->session->data['success'] = $this->language->get('text_success_email');
		          
					//$this->response->redirect($this->url->link('account/login', '', 'SSL'));
		            $this->response->redirect($this->url->link('account/forgotten', '', 'SSL'));
		        }
			}elseif (empty($this->request->post['email']) && empty($this->request->post['mobile'])) {
			$this->error['warning'] = $this->language->get('error_mobile_email');}
		}
		if(isset($this->session->data['success'])){
			$data['message_success'] = $this->session->data['success'];
		} else {
			$data['message_success'] = '';
		}
        $data['mobile_format'] = $this->language->get('entry_mobile_format');
		$data['breadcrumbs'] = array();

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_home'),
			'href' => $this->url->link('common/home')
		);

		

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_forgotten'),
			'href' => $this->url->link('account/forgotten', '', 'SSL')
		);

		$data['heading_title'] = $this->language->get('heading_title');

		$data['text_your_email'] = $this->language->get('text_your_email');
		$data['text_email'] = $this->language->get('text_email');

		$data['entry_email'] = $this->language->get('entry_email');
        
        $data['text_your_mobile'] = $this->language->get('text_your_mobile');
		$data['text_mobile'] = $this->language->get('text_mobile');

		$data['entry_mobile'] = $this->language->get('entry_mobile');

		$data['button_continue'] = $this->language->get('button_continue');
		$data['button_back'] = $this->language->get('button_back');

		if (isset($this->error['warning'])) {
			$data['error_warning'] = $this->error['warning'];
		} else {
			$data['error_warning'] = '';
		}

		$data['action'] = $this->url->link('account/forgotten', '', 'SSL');

		//$data['back'] = $this->url->link('account/login', '', 'SSL');
        $data['back'] = $this->url->link('common/home', '', 'SSL');

		$data['column_left'] = $this->load->controller('common/column_left');
		$data['column_right'] = $this->load->controller('common/column_right');
		$data['content_top'] = $this->load->controller('common/content_top');
		$data['content_bottom'] = $this->load->controller('common/content_bottom');
		$data['footer'] = $this->load->controller('common/footer');
       
  
		$data['header'] = $this->load->controller('common/header');

		if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/template/account/forgotten.tpl')) {
			$this->response->setOutput($this->load->view($this->config->get('config_template') . '/template/account/forgotten.tpl', $data));
		} else {
			$this->response->setOutput($this->load->view('default/template/account/forgotten.tpl', $data));
		}
	}

	protected function validate() {
		if (!isset($this->request->post['mobile']) || empty($this->request->post['mobile'])) {
			$this->error['warning'] = $this->language->get('error_mobile');
		} elseif (!$this->model_account_customer->getTotalCustomersByTelephone($this->request->post['mobile'])) {
			$this->error['warning'] = $this->language->get('error_mobile');
		}

		$customer_info = $this->model_account_customer->getCustomerByTelephone($this->request->post['mobile']);

		if ($customer_info && !$customer_info['approved']) {
		    $this->error['warning'] = $this->language->get('error_approved');
		}
        
		return !$this->error;
	}
    protected function validateOld() {
		if (!isset($this->request->post['email'])) {
			$this->error['warning'] = $this->language->get('error_email');
		} elseif (!$this->model_account_customer->getTotalCustomersByEmail($this->request->post['email'])) {
			$this->error['warning'] = $this->language->get('error_email');
		}

		$customer_info = $this->model_account_customer->getCustomerByEmail($this->request->post['email']);

		if ($customer_info && !$customer_info['approved']) {
		    $this->error['warning'] = $this->language->get('error_approved');
		}

		return !$this->error;
	}
}
