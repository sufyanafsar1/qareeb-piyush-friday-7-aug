<?php
class ControllerAccountSimpleRegister extends Controller {
	private $error = array();

public function index() {
   
/**
 * if ($this->customer->isLogged()) {
 * 			$this->response->redirect($this->url->link('account/account', '', 'SSL'));
 * 		}
 */

		$this->load->language('account/register');

	//	$this->document->setTitle($this->language->get('heading_title'));

		$this->document->addScript('catalog/view/javascript/jquery/datetimepicker/moment.js');
		$this->document->addScript('catalog/view/javascript/jquery/datetimepicker/bootstrap-datetimepicker.min.js');
		$this->document->addStyle('catalog/view/javascript/jquery/datetimepicker/bootstrap-datetimepicker.min.css');

		$this->load->model('account/customer');

		if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validate()) {
			$customer_id = $this->model_account_customer->addCustomer($this->request->post);

			// Clear any previous login attempts for unregistered accounts.
			$this->model_account_customer->deleteLoginAttempts($this->request->post['email']);

			$this->customer->login($this->request->post['email'], $this->request->post['password']);

			unset($this->session->data['guest']);

			

			// Add to activity log
			$this->load->model('account/activity');

			$activity_data = array(
				'customer_id' => $customer_id,
				'name'        => $this->request->post['firstname'] . ' ' . $this->request->post['lastname']
			);

			$this->model_account_activity->addActivity('register', $activity_data);


			$this->response->redirect($this->url->link('account/success'));
		}

	
		$data['heading_title'] = $this->language->get('heading_title');

		$data['text_account_already'] = sprintf($this->language->get('text_account_already'), $this->url->link('account/login', '', 'SSL'));
		$data['text_your_details'] = $this->language->get('text_your_details');
		$data['text_your_address'] = $this->language->get('text_your_address');
		$data['text_your_password'] = $this->language->get('text_your_password');
		$data['text_newsletter'] = $this->language->get('text_newsletter');
		$data['text_yes'] = $this->language->get('text_yes');
		$data['text_no'] = $this->language->get('text_no');
		$data['text_select'] = $this->language->get('text_select');
		$data['text_none'] = $this->language->get('text_none');
		$data['text_loading'] = $this->language->get('text_loading');

		$data['entry_customer_group'] = $this->language->get('entry_customer_group');
		$data['entry_firstname'] = $this->language->get('entry_firstname');
		$data['entry_lastname'] = $this->language->get('entry_lastname');
		$data['entry_email'] = $this->language->get('entry_email');
		$data['entry_telephone'] = $this->language->get('entry_telephone');
		$data['entry_fax'] = $this->language->get('entry_fax');
		$data['entry_company'] = $this->language->get('entry_company');
		$data['entry_address_1'] = $this->language->get('entry_address_1');
		$data['entry_address_2'] = $this->language->get('entry_address_2');
		$data['entry_postcode'] = $this->language->get('entry_postcode');
		$data['entry_city'] = $this->language->get('entry_city');
		$data['entry_country'] = $this->language->get('entry_country');
		$data['entry_zone'] = $this->language->get('entry_zone');
		$data['entry_newsletter'] = $this->language->get('entry_newsletter');
		$data['entry_password'] = $this->language->get('entry_password');
		$data['entry_confirm'] = $this->language->get('entry_confirm');
         $data['mobile_format'] = $this->language->get('entry_mobile_format');
		$data['button_continue'] = $this->language->get('button_continue');
		$data['button_upload'] = $this->language->get('button_upload');

		if (isset($this->error['warning'])) {
			$data['error_warning'] = $this->error['warning'];
		} else {
			$data['error_warning'] = '';
		}

		if (isset($this->error['firstname'])) {
			$data['error_firstname'] = $this->error['firstname'];
		} else {
			$data['error_firstname'] = '';
		}

		if (isset($this->error['lastname'])) {
			$data['error_lastname'] = $this->error['lastname'];
		} else {
			$data['error_lastname'] = '';
		}

		if (isset($this->error['email'])) {
			$data['error_email'] = $this->error['email'];
		} else {
			$data['error_email'] = '';
		}

		if (isset($this->error['telephone'])) {
			$data['error_telephone'] = $this->error['telephone'];
		} else {
			$data['error_telephone'] = '';
		}

		if (isset($this->error['address_1'])) {
			$data['error_address_1'] = $this->error['address_1'];
		} else {
			$data['error_address_1'] = '';
		}

	

		if (isset($this->error['password'])) {
			$data['error_password'] = $this->error['password'];
		} else {
			$data['error_password'] = '';
		}

		if (isset($this->error['confirm'])) {
			$data['error_confirm'] = $this->error['confirm'];
		} else {
			$data['error_confirm'] = '';
		}

		$data['action'] = $this->url->link('account/register', '', 'SSL');

		$data['customer_groups'] = array();

		if (is_array($this->config->get('config_customer_group_display'))) {
			$this->load->model('account/customer_group');

			$customer_groups = $this->model_account_customer_group->getCustomerGroups();

			foreach ($customer_groups as $customer_group) {
				if (in_array($customer_group['customer_group_id'], $this->config->get('config_customer_group_display'))) {
					$data['customer_groups'][] = $customer_group;
				}
			}
		}

		if (isset($this->request->post['customer_group_id'])) {
			$data['customer_group_id'] = $this->request->post['customer_group_id'];
		} else {
			$data['customer_group_id'] = $this->config->get('config_customer_group_id');
		}

		if (isset($this->request->post['firstname'])) {
			$data['firstname'] = $this->request->post['firstname'];
		} else {
			$data['firstname'] = '';
		}

		if (isset($this->request->post['lastname'])) {
			$data['lastname'] = $this->request->post['lastname'];
		} else {
			$data['lastname'] = '';
		}

		if (isset($this->request->post['email'])) {
			$data['email'] = $this->request->post['email'];
		} else {
			$data['email'] = '';
		}

		if (isset($this->request->post['telephone'])) {
			$data['telephone'] = $this->request->post['telephone'];
		} else {
			$data['telephone'] = '';
		}

		if (isset($this->request->post['fax'])) {
			$data['fax'] = $this->request->post['fax'];
		} else {
			$data['fax'] = '';
		}

		if (isset($this->request->post['company'])) {
			$data['company'] = $this->request->post['company'];
		} else {
			$data['company'] = '';
		}

		if (isset($this->request->post['address_1'])) {
			$data['address_1'] = $this->request->post['address_1'];
		} else {
			$data['address_1'] = '';
		}

		if (isset($this->request->post['address_2'])) {
			$data['address_2'] = $this->request->post['address_2'];
		} else {
			$data['address_2'] = '';
		}

		

		if (isset($this->request->post['password'])) {
			$data['password'] = $this->request->post['password'];
		} else {
			$data['password'] = '';
		}

		if (isset($this->request->post['confirm'])) {
			$data['confirm'] = $this->request->post['confirm'];
		} else {
			$data['confirm'] = '';
		}

		

		// Captcha
		if ($this->config->get($this->config->get('config_captcha') . '_status') && in_array('register', (array)$this->config->get('config_captcha_page'))) {
			$data['captcha'] = $this->load->controller('captcha/' . $this->config->get('config_captcha'), $this->error);
		} else {
			$data['captcha'] = '';
		}

		if ($this->config->get('config_account_id')) {
			$this->load->model('catalog/information');

			$information_info = $this->model_catalog_information->getInformation($this->config->get('config_account_id'));

			if ($information_info) {
				$data['text_agree'] = sprintf($this->language->get('text_agree'), $this->url->link('information/information/agree', 'information_id=' . $this->config->get('config_account_id'), 'SSL'), $information_info['title'], $information_info['title']);
			} else {
				$data['text_agree'] = '';
			}
		} else {
			$data['text_agree'] = '';
		}

		if (isset($this->request->post['agree'])) {
			$data['agree'] = $this->request->post['agree'];
		} else {
			$data['agree'] = false;
		}

	

		if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/template/account/signup.tpl')) {
		return	$this->load->view($this->config->get('config_template') . '/template/account/signup.tpl', $data);
		} else {
		return	$this->load->view('default/template/account/register.tpl', $data);
		}
	}

public function registerTempCustomer() {
        
       
        $jsonLogged =array();
        $this->load->language('account/register');
		$this->load->model('account/customer');
        $this->language->load('account/success');
		if ($this->customer->isLogged()) {
			$jsonLogged['islogged'] = true;
		}
            
		if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validateRegister()) {
		  $data  = $this->request->post;
          $data["password"] = createPassword();
         
          
			$customer_id = $this->model_account_customer->addCustomerTemp($data);
            if(isset($customer_id) && $customer_id > 0) // Send Sms to user with Password
            {
               $message = $this->language->get('text_otp') ." ". $data["password"] ." ". $this->language->get('text_otp_continue') ." Careeb.com";
                                
                $smsData =  sendSMS($this->request->post['telephone'],$message);                     
         
            }
            $json=array();
            $json['sms_status'] = $smsData;
            $json['islogged'] = $jsonLogged['islogged'];
           	$json['heading_title'] = $this->language->get('heading_title');  
            $json['text_message'] = $this->language->get('text_login_telephone');  
            $json['success'] = true;
            $json['password_sent']= $data["password"];
            $json['mobile_registered']= $this->request->post['telephone'];
			// Clear any previous login attempts for unregistered accounts.
			$this->model_account_customer->deleteLoginAttempts($this->request->post['email']);

			$this->customer->login($this->request->post['email'], $this->request->post['password']);

			unset($this->session->data['guest']);

		
             if ($this->customer->isLogged()) {
				$json['now_login'] = true;
			}
			
		}

	
        $json['button_continue'] = $this->language->get('button_continue');
        $json['button_verify'] = $this->language->get('button_verify');
        $json['button_resend'] = $this->language->get('button_resend');
        $json['text_verify_password']   = $this->language->get('text_verify_password');
	    

		if (isset($this->error['warning'])) {
			$json['error_warning'] = $this->error['warning'];
		} else {
			$json['error_warning'] = '';
		}
        if (isset($this->error['warning_mobile'])) {
			$json['error_warning_mobile'] = $this->error['warning_mobile'];
		} else {
			$json['error_warning_mobile'] = '';
		}

		if (isset($this->error['firstname'])) {
			$json['error_firstname'] = $this->error['firstname'];
		} else {
			$json['error_firstname'] = '';
		}
        if (isset($this->error['lastname'])) {
			$json['error_lastname'] = $this->error['lastname'];
		} else {
			$json['error_lastname'] = '';
		}

	
		if (isset($this->error['email'])) {
			$json['error_email'] = $this->error['email'];
		} else {
			$json['error_email'] = '';
		}

		if (isset($this->error['telephone'])) {
			$json['error_telephone'] = $this->error['telephone'];
		} else {
			$json['error_telephone'] = '';
		}

		if (isset($this->error['address_1'])) {
			$json['error_address_1'] = $this->error['address_1'];
		} else {
			$json['error_address_1'] = '';
		}

		

		if (isset($this->error['password'])) {
			$json['error_password'] = $this->error['password'];
		} else {
			$json['error_password'] = '';
		}

		if (isset($this->error['confirm'])) {
			$json['error_confirm'] = $this->error['confirm'];
		} else {
			$json['error_confirm'] = '';
		}


		if ($this->config->get('config_account_id')) {
			$this->load->model('catalog/information');

			$information_info = $this->model_catalog_information->getInformation($this->config->get('config_account_id'));

			if ($information_info) {
				$json['text_agree'] = sprintf($this->language->get('text_agree'), $this->url->link('information/information/agree', 'information_id=' . $this->config->get('config_account_id'), 'SSL'), $information_info['title'], $information_info['title']);
			} else {
				$json['text_agree'] = '';
			}
		} else {
			$json['text_agree'] = '';
		}

		if (isset($this->request->post['agree'])) {
			$json['agree'] = $this->request->post['agree'];
		} else {
			$json['agree'] = false;
            $json['success'] = false;
		}

	    $this->response->addHeader('Content-Type: application/json');
		$this->response->setOutput(json_encode($json));

	}
    public function resendPassword() {
        $this->load->language('account/register');		
        $this->language->load('account/success');
         $json=array();
         $json['success'] = true;
         if (($this->request->server['REQUEST_METHOD'] == 'POST'))
          {
            $message = $this->language->get('text_otp') ." ". $this->request->post['userPassword'] ." ". $this->language->get('text_otp_continue') ." Careeb.com ";
            $smsData =  sendSMS($this->request->post['mobile'],$message);
            $json['sms'] =$smsData;
            if($smsData=="OK")
            {
                $json['success'] = true;
            }    
          }
           	$json['text_resend_password'] = $this->language->get('text_resend_password');  
           $json['button_verify'] = $this->language->get('button_verify');
        $json['button_resend'] = $this->language->get('button_resend');
        $json['text_verify_password']   = $this->language->get('text_verify_password');
          $json['password_sent']= $this->request->post['userPassword'];
          $json['mobile_registered']= $this->request->post['mobile'];
        $this->response->addHeader('Content-Type: application/json');
		$this->response->setOutput(json_encode($json));
    }
	  public function register() {
        
        $json=array();
        $data  =array();
        $this->load->language('account/register');
		$this->load->model('account/customer');
        $this->language->load('account/success');
		if ($this->customer->isLogged()) {
			$json['islogged'] =true;
		}
            
		if (($this->request->server['REQUEST_METHOD'] == 'POST')) {
		  
          $postedData  = $this->request->post; // get mobile and password
          $customerdata = $this->model_account_customer->getTempCustomer($this->request->post['mobile'],$this->request->post['password']);        // get temp customer data
         
          if(isset($customerdata) && count($customerdata) > 0)
          {
            $data  = $customerdata;
          }
         
          
        
          
			$customer_id = $this->model_account_customer->addCustomer($data);
            
			$this->load->language('mail/forgotten');
			$link = $this->url->link('account/forgotten', 'SSL');
			$c_name = $data['firstname'] . ' ' . $data['lastname'];
			$subject = "Thank you for registering at Careeb.com";
			$message  = '<html dir="ltr" lang="en">' . "\n";
     					$message .= '  <head>' . "\n";
    					$message .= '    <title> Subject </title>' . "\n";
     					$message .= '    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">' . "\n";
     					$message .= '  </head>' . "\n";
     					$message .= '  <body> ' . "\n";
						$message .= '  <p>Dear '.$c_name.' </p>' . "</p><br>";
						$message .= '  <p>Welcome https://www.careeb.com Thank you for registering with us.' . "</p><br>";
						$message .= '  <p>Your login information is as follows: ' . "</p>";
						$message .= '  <p>Username : '.$data['email']. "</p>";
						$message .= '  <p>Password: ' .$data['password']. "</p><br><br>";
						$message .= '  <p>To login, please access here type the username given above.' . "</p><br>";
						$message .= '  <p>If you wish to reset your password please <a href="' . $link . '">click here</a>' . "</p><br>";
						$message .= '  <p>We request you to make note of this information and keep safe place for future reference.' . "</p><br>";
						$message .= '  <p>For any questions or queries, please contact Customer Support at support@careeb.com' . "</p>";
						

						$message .= '</body></html>' . "\n";
		                $mail = new Mail();
						$mail->protocol = $this->config->get('config_mail_protocol');
						$mail->parameter = $this->config->get('config_mail_parameter');
						$mail->smtp_hostname = $this->config->get('config_mail_smtp_hostname');
						$mail->smtp_username = $this->config->get('config_mail_smtp_username');
						$mail->smtp_password = html_entity_decode($this->config->get('config_mail_smtp_password'), ENT_QUOTES, 'UTF-8');
						$mail->smtp_port = $this->config->get('config_mail_smtp_port');
						$mail->smtp_timeout = $this->config->get('config_mail_smtp_timeout');
						$mail->setTo($data['email']);
						$mail->setFrom($this->config->get('config_email'));
						$mail->setSender(html_entity_decode($this->config->get('config_name'), ENT_QUOTES, 'UTF-8'));
						$mail->setSubject(html_entity_decode($subject, ENT_QUOTES, 'UTF-8'));
						// $mail->setText(html_entity_decode($message, ENT_QUOTES, 'UTF-8'));
						$mail->setHtml($message);
						$mail->send();

			if(isset($customer_id) && $customer_id > 0) // Send Sms to user with Password
            {       
                $json['success'] = true;
                $message= $this->language->get('text_verify_password')."\n";                      
                $json['text_verified'] = $message;
                
                // Delete Customer Temp record
                $this->model_account_customer->deleteTempCustomer($this->request->post['mobile'],$this->request->post['password']);  
            }
            else{
                $json['error_warning'] = $this->language->get('error_verify')."\n";  ;
            }
            
            
			// Clear any previous login attempts for unregistered accounts.
			$this->model_account_customer->deleteLoginAttempts($this->request->post['email']);

			$this->customer->login($this->request->post['email'], $this->request->post['password']);
			
			unset($this->session->data['guest']);

			// Add to activity log
			$this->load->model('account/activity');

				$activity_data = array(
				'customer_id' => $customer_id,
				'name'        => $data['firstname'] . ' ' . $data['lastname']
			);

			$this->model_account_activity->addActivity('register', $activity_data);
             if ($this->customer->isLogged()) {
				$json['now_login'] = true;
			}
			
		}
	
        $json['button_continue'] = $this->language->get('button_continue');
        $json['text_verification_message'] = $this->language->get('text_verification');  
	    $this->response->addHeader('Content-Type: application/json');
		$this->response->setOutput(json_encode($json));

	}
    public function registerBkup() {
        
        $jsonLogged =array();
        $this->load->language('account/register');
		$this->load->model('account/customer');
        $this->language->load('account/success');
		if ($this->customer->isLogged()) {
			$jsonLogged['islogged'] = true;
		}
            
		if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validateRegister()) {
		  $data  = $this->request->post;
          $data["password"] = createPassword();
        
          
			$customer_id = $this->model_account_customer->addCustomer($data);
            if(isset($customer_id) && $customer_id > 0) // Send Sms to user with Password
            {
                $message = sprintf($this->language->get('text_subject'), html_entity_decode($this->config->get('config_name'), ENT_QUOTES, 'UTF-8')) . "\n";
                $message .= $this->language->get('text_password') ." ". $data["password"]. "\n";
               
                
                $smsData =  sendSMS($this->request->post['telephone'],$message);               
         
            }
            $json=array();
            $json['sms_status'] = $smsData;
            $json['islogged'] = $jsonLogged['islogged'];
           	$json['heading_title'] = $this->language->get('heading_title');  
            $json['text_message'] = $this->language->get('text_login_telephone');  
            $json['success'] = true;
			// Clear any previous login attempts for unregistered accounts.
			$this->model_account_customer->deleteLoginAttempts($this->request->post['email']);

			$this->customer->login($this->request->post['email'], $this->request->post['password']);

			unset($this->session->data['guest']);

			// Add to activity log
			$this->load->model('account/activity');

			$activity_data = array(
				'customer_id' => $customer_id,
				'name'        => $this->request->post['firstname'] . ' ' . $this->request->post['lastname']
			);

			$this->model_account_activity->addActivity('register', $activity_data);
             if ($this->customer->isLogged()) {
				$json['now_login'] = true;
			}
			
		}

	
        $json['button_continue'] = $this->language->get('button_continue');
	

		if (isset($this->error['warning'])) {
			$json['error_warning'] = $this->error['warning'];
		} else {
			$json['error_warning'] = '';
		}
        if (isset($this->error['warning_mobile'])) {
			$json['error_warning_mobile'] = $this->error['warning_mobile'];
		} else {
			$json['error_warning_mobile'] = '';
		}

		if (isset($this->error['firstname'])) {
			$json['error_firstname'] = $this->error['firstname'];
		} else {
			$json['error_firstname'] = '';
		}
        if (isset($this->error['lastname'])) {
			$json['error_lastname'] = $this->error['lastname'];
		} else {
			$json['error_lastname'] = '';
		}

	
		if (isset($this->error['email'])) {
			$json['error_email'] = $this->error['email'];
		} else {
			$json['error_email'] = '';
		}

		if (isset($this->error['telephone'])) {
			$json['error_telephone'] = $this->error['telephone'];
		} else {
			$json['error_telephone'] = '';
		}

		if (isset($this->error['address_1'])) {
			$json['error_address_1'] = $this->error['address_1'];
		} else {
			$json['error_address_1'] = '';
		}

		

		if (isset($this->error['password'])) {
			$json['error_password'] = $this->error['password'];
		} else {
			$json['error_password'] = '';
		}

		if (isset($this->error['confirm'])) {
			$json['error_confirm'] = $this->error['confirm'];
		} else {
			$json['error_confirm'] = '';
		}


		if ($this->config->get('config_account_id')) {
			$this->load->model('catalog/information');

			$information_info = $this->model_catalog_information->getInformation($this->config->get('config_account_id'));

			if ($information_info) {
				$json['text_agree'] = sprintf($this->language->get('text_agree'), $this->url->link('information/information/agree', 'information_id=' . $this->config->get('config_account_id'), 'SSL'), $information_info['title'], $information_info['title']);
			} else {
				$json['text_agree'] = '';
			}
		} else {
			$json['text_agree'] = '';
		}

		if (isset($this->request->post['agree'])) {
			$json['agree'] = $this->request->post['agree'];
		} else {
			$json['agree'] = false;
		}

	    $this->response->addHeader('Content-Type: application/json');
		$this->response->setOutput(json_encode($json));

	}
   


	private function validateRegister() {
		if ((utf8_strlen(trim($this->request->post['firstname'])) < 1) || (utf8_strlen(trim($this->request->post['firstname'])) > 32)) {
			$this->error['firstname'] = $this->language->get('error_firstname');
		}

	if ((utf8_strlen(trim($this->request->post['lastname'])) < 1) || (utf8_strlen(trim($this->request->post['lastname'])) > 32)) {
			$this->error['lastname'] = $this->language->get('error_lastname');
		}

		if ((utf8_strlen($this->request->post['email']) > 96) || !preg_match('/^[^\@]+@.*.[a-z]{2,15}$/i', $this->request->post['email'])) {
			$this->error['email'] = $this->language->get('error_email');
		}

		if ($this->model_account_customer->getTotalCustomersByEmail($this->request->post['email'])) {
			$this->error['warning'] = $this->language->get('error_exists');
		}
		
		if(isset($this->request->post['telephone'])){
			$this->request->post['telephone'] = str_replace(' ', '', $this->request->post['telephone']);
		} else {			
           $this->error['telephone'] = $this->language->get('error_telephone');
		}        
			
		if(!preg_match('/^(9665)(5|0|3|6|4|9|1|8|7)([0-9]{7})$/', $this->request->post['telephone']))
        {
           $this->error['telephone'] = $this->language->get('error_telephone');
        }

		//if ((utf8_strlen($this->request->post['telephone']) < 3) || (utf8_strlen($this->request->post['telephone']) > 32)) {
//			$this->error['telephone'] = $this->language->get('error_telephone');
//		}
      if(!empty($this->request->post['telephone']))
      {
        if ($this->model_account_customer->getTotalCustomersByTelephone($this->request->post['telephone'])) {
			$this->error['warning_mobile'] = $this->language->get('error_exists_mobile');
		}
      }
        

		if ((utf8_strlen(trim($this->request->post['address_1'])) < 3) || (utf8_strlen(trim($this->request->post['address_1'])) > 128)) {
			$this->error['address_1'] = $this->language->get('error_address_1');
		}

		return !$this->error;
	}

	private function validate() {
		if ((utf8_strlen(trim($this->request->post['firstname'])) < 1) || (utf8_strlen(trim($this->request->post['firstname'])) > 32)) {
			$this->error['firstname'] = $this->language->get('error_firstname');
		}

		if ((utf8_strlen(trim($this->request->post['lastname'])) < 1) || (utf8_strlen(trim($this->request->post['lastname'])) > 32)) {
			$this->error['lastname'] = $this->language->get('error_lastname');
		}

		if ((utf8_strlen($this->request->post['email']) > 96) || !preg_match('/^[^\@]+@.*.[a-z]{2,15}$/i', $this->request->post['email'])) {
			$this->error['email'] = $this->language->get('error_email');
		}

		if ($this->model_account_customer->getTotalCustomersByEmail($this->request->post['email'])) {
			$this->error['warning'] = $this->language->get('error_exists');
		}

		if ((utf8_strlen($this->request->post['telephone']) < 3) || (utf8_strlen($this->request->post['telephone']) > 32)) {
			$this->error['telephone'] = $this->language->get('error_telephone');
		}

		if ((utf8_strlen(trim($this->request->post['address_1'])) < 3) || (utf8_strlen(trim($this->request->post['address_1'])) > 128)) {
			$this->error['address_1'] = $this->language->get('error_address_1');
		}

		if ((utf8_strlen(trim($this->request->post['city'])) < 2) || (utf8_strlen(trim($this->request->post['city'])) > 128)) {
			$this->error['city'] = $this->language->get('error_city');
		}

		$this->load->model('localisation/country');

		$country_info = $this->model_localisation_country->getCountry($this->request->post['country_id']);

		if ($country_info && $country_info['postcode_required'] && (utf8_strlen(trim($this->request->post['postcode'])) < 2 || utf8_strlen(trim($this->request->post['postcode'])) > 10)) {
			$this->error['postcode'] = $this->language->get('error_postcode');
		}

		if ($this->request->post['country_id'] == '') {
			$this->error['country'] = $this->language->get('error_country');
		}

		if (!isset($this->request->post['zone_id']) || $this->request->post['zone_id'] == '') {
			$this->error['zone'] = $this->language->get('error_zone');
		}

		// Customer Group
		if (isset($this->request->post['customer_group_id']) && is_array($this->config->get('config_customer_group_display')) && in_array($this->request->post['customer_group_id'], $this->config->get('config_customer_group_display'))) {
			$customer_group_id = $this->request->post['customer_group_id'];
		} else {
			$customer_group_id = $this->config->get('config_customer_group_id');
		}

		// Custom field validation
		$this->load->model('account/custom_field');

		$custom_fields = $this->model_account_custom_field->getCustomFields($customer_group_id);

		foreach ($custom_fields as $custom_field) {
			if ($custom_field['required'] && empty($this->request->post['custom_field'][$custom_field['location']][$custom_field['custom_field_id']])) {
				$this->error['custom_field'][$custom_field['custom_field_id']] = sprintf($this->language->get('error_custom_field'), $custom_field['name']);
			}
		}

		if ((utf8_strlen($this->request->post['password']) < 4) || (utf8_strlen($this->request->post['password']) > 20)) {
			$this->error['password'] = $this->language->get('error_password');
		}

		if ($this->request->post['confirm'] != $this->request->post['password']) {
			$this->error['confirm'] = $this->language->get('error_confirm');
		}

		// Captcha
		if ($this->config->get($this->config->get('config_captcha') . '_status') && in_array('register', (array)$this->config->get('config_captcha_page'))) {
			$captcha = $this->load->controller('captcha/' . $this->config->get('config_captcha') . '/validate');

			if ($captcha) {
				$this->error['captcha'] = $captcha;
			}
		}

		// Agree to terms
		if ($this->config->get('config_account_id')) {
			$this->load->model('catalog/information');

			$information_info = $this->model_catalog_information->getInformation($this->config->get('config_account_id'));

			if ($information_info && !isset($this->request->post['agree'])) {
				$this->error['warning'] = sprintf($this->language->get('error_agree'), $information_info['title']);
			}
		}

		return !$this->error;
	}

	public function customfield() {
		$json = array();

		$this->load->model('account/custom_field');

		// Customer Group
		if (isset($this->request->get['customer_group_id']) && is_array($this->config->get('config_customer_group_display')) && in_array($this->request->get['customer_group_id'], $this->config->get('config_customer_group_display'))) {
			$customer_group_id = $this->request->get['customer_group_id'];
		} else {
			$customer_group_id = $this->config->get('config_customer_group_id');
		}

		$custom_fields = $this->model_account_custom_field->getCustomFields($customer_group_id);

		foreach ($custom_fields as $custom_field) {
			$json[] = array(
				'custom_field_id' => $custom_field['custom_field_id'],
				'required'        => $custom_field['required']
			);
		}

		$this->response->addHeader('Content-Type: application/json');
		$this->response->setOutput(json_encode($json));
	}
}
