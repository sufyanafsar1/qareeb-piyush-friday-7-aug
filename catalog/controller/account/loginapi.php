<?php
class ControllerAccountLoginapi extends Controller
{
	private $error = array();

	public function index()
	{
		$this->load->model('account/customer');
		$this->load->language('account/login');

		if ($this->validate()) {

			// Add to activity log
			$this->load->model('account/activity');

			$activity_data = array(
				'customer_id' => $this->customer->getId(),
				'name'        => $this->customer->getFirstName() . ' ' . $this->customer->getLastName()
			);

			$this->model_account_activity->addActivity('login', $activity_data);

			$data['message'] = 'success';
			$data['customer_info'] = $this->model_account_customer->getCustomerByEmail($this->request->post['email']);

			$this->request->post['customer_id'] = $this->customer->getId();

			// update mobile_reg_id
			$userMobileData  = $this->request->post;
			$this->model_account_customer->updateUserMobileId($userMobileData);
		} else {
			$data['message'] = $this->error['warning'];
			$data['customer_info'] = '';
		}
		print_r(json_encode($data));
	}

	public function fblogin()
	{
		$this->load->model('account/customer');
		$this->load->language('account/login');


		print_r($this->customer->login("rituparnosarkar@outloddok.com", '', true));
		die();

		if ($this->validate()) {
			$data['message'] = 'success';
			$data['customer_info'] = $this->model_account_customer->getCustomerByEmail($this->request->post['email']);
		} else {
			$data['message'] = $this->error['warning'];
			$data['customer_info'] = '';
		}
		print_r(json_encode($data));
	}

	protected function validate()
	{

		// Check how many login attempts have been made.
		$login_info = $this->model_account_customer->getLoginAttempts($this->request->post['email']);

		if ($login_info && ($login_info['total'] >= $this->config->get('config_login_attempts')) && strtotime('-1 hour') < strtotime($login_info['date_modified'])) {
			$this->error['warning'] = $this->language->get('error_attempts');
		}

		// Check if customer has been approved.
		$customer_info = $this->model_account_customer->getCustomerByEmail($this->request->post['email']);


		if ($customer_info && !$customer_info['approved']) {
			$this->error['warning'] = $this->language->get('error_approved');
		}

		if (!$this->error) {
			if (!$this->customer->login($this->request->post['email'], $this->request->post['password'])) {
				$this->error['warning'] = $this->language->get('error_login');
				$this->model_account_customer->addLoginAttempt($this->request->post['email']);
			} else {
				$this->model_account_customer->deleteLoginAttempts($this->request->post['email']);
				$this->event->trigger('post.customer.login');
			}
		}

		return !$this->error;
	}

	public function getConfigRadius()
	{
		$datainfo['config_radius'] = $this->config->get('config_radius');
		print_r(json_encode($datainfo));
	}

	public function customerLogout()
	{
		$this->load->model('account/customer');
		$deleteUserMobileData  = $this->request->get;
		$this->model_account_customer->deleteUserMobileId($deleteUserMobileData);
	}
}
