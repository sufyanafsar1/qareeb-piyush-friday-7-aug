<?php
class ControllerAccountOrderapi extends Controller
{
	private $error = array();

	public function index()
	{
		$data['orders'] = array();

		$language_id = $this->request->get['language_id'];
		$is_ios      = (int) (isset($this->request->get['ios'])) ? $this->request->get['ios'] : 0;

		$this->load->model('account/order');

		$query = $this->db->query("SELECT o.order_id, o.firstname, o.lastname, o.order_status_id, os.name as status, o.date_added, o.total, o.currency_code, o.currency_value FROM `" . DB_PREFIX . "order` o LEFT JOIN " . DB_PREFIX . "order_status os ON (o.order_status_id = os.order_status_id) WHERE o.customer_id = '" . (int) $this->request->get['customer_id'] . "' AND o.order_status_id > '0' AND o.store_id = '" . (int) $this->config->get('config_store_id') . "' AND os.language_id = '" . (int) $this->config->get('config_language_id') . "' ORDER BY o.order_id DESC");

		$results = $query->rows;

		foreach ($results as $result) {
			$product_total = $this->model_account_order->getTotalOrderProductsByOrderId($result['order_id']);
			$voucher_total = $this->model_account_order->getTotalOrderVouchersByOrderId($result['order_id']);

			$Total1 = $this->model_account_order->getOrderTotals1($result['order_id']);
			$Total2 = $this->model_account_order->getOrderTotals2($result['order_id']);

			$date_added = date("Y-m-d", strtotime($result['date_added']));
			$date = '2018-11-02';

			$beforDate = 0;
			$afterDate = 0;
			if ($result['date_added'] != '0000-00-00 00:00:00') {
				if ($date_added <= $date) {
					$Final_Total = ($Total1 + $Total2);
				} else {
					$Final_Total = $Total1;
				}
			} else {
				$Final_Total = ($Total1 + $Total2);
			}

			$seller = $this->model_account_order->getSellerOrderProducts($result['order_id']);

			$sellerName  = (isset($language_id) && $language_id == 3) ? $seller[0]['lastname'] : $seller[0]['firstname'];

			$delivery_date_time = '';
			$orderDeliveryRes = $this->db->query("SELECT times FROM " . DB_PREFIX . "delivery_time WHERE order_id = '" . (int) $result['order_id'] . "'");
			$delivery_date_time =  $orderDeliveryRes->row['times'];
			// if($delivery_date_time!=''){
			// $tmpArr = explode(" - ",$delivery_date_time);

			// $delivery_date_time = date('H:i',strtotime(trim($tmpArr[0])));
			// $delivery_date_time .= ' '.date('H:i',strtotime(trim($tmpArr[1])));

			// }




			$data['orders'][] = array(
				'order_id'           => $result['order_id'],
				'name'               => $result['firstname'] . ' ' . $result['lastname'],
				'status'             => $result['status'],
				'order_status_id'    => $result['order_status_id'],
				'delivery_date_time' => $delivery_date_time,
				'date_added'         => date($this->language->get('date_format_short'), strtotime($result['date_added'])),
				'products'           => ($product_total + $voucher_total),
				'total'              => number_format($Final_Total, 2),
				'seller_name'        => $sellerName,
				'seller_id'          => $seller[0]['seller_id']
			);
		}
		if ($is_ios) {
			$data = array('msg' => 'success', 'result' => $data['orders']);
			echo json_encode($data);
			die;
		} else {
			print_r(json_encode($data['orders']));
		}
	}

public function infov2test()
	{
		$this->load->language('account/order');
		$this->load->model('account/order');
		$this->load->model('slot/timing');

		$order_id = (int) $this->request->get['order_id'];
		$customer_id = (int) $this->request->get['customer_id'];
		$order_info = $this->model_account_order->getOrderMobile($order_id, $customer_id);
		$language_id =  $this->request->get['language_id'];

		//City_id		
		$city_id = 0;
		if (isset($this->request->get['city_id'])) {
			$city_id = $this->request->get['city_id'];
		}

		if (count($order_info) > 0) {
			if ($order_info['invoice_no']) {
				$datainfo['invoice_no'] = $order_info['invoice_prefix'] . $order_info['invoice_no'];
			} else {
				$datainfo['invoice_no'] = '';
			}

			$datainfo['order_id'] = $this->request->get['order_id'];
			$datainfo['date_added'] = date($this->language->get('date_format_short'), strtotime($order_info['date_added']));
			$datainfo['delivery_date_time'] =  $this->model_slot_timing->getDeliveryTime($order_id);
			if ($order_info['payment_address_format']) {
				$format = $order_info['payment_address_format'];
			} else {
				$format = '{firstname} {lastname}' . "\n" . '{company}' . "\n" . '{address_1}' . "\n" . '{address_2}' . "\n" . '{city} {postcode}' . "\n" . '{zone}' . "\n" . '{country}';
			}

			$find = array(
				'{firstname}',
				'{lastname}',
				'{company}',
				'{address_1}',
				'{address_2}',
				'{city}',
				'{postcode}',
				'{zone}',
				'{zone_code}',
				'{country}'
			);

			$replace = array(
				'firstname' => $order_info['payment_firstname'],
				'lastname'  => $order_info['payment_lastname'],
				'company'   => $order_info['payment_company'],
				'address_1' => $order_info['payment_address_1'],
				'address_2' => $order_info['payment_address_2'],
				'city'      => $order_info['payment_city'],
				'postcode'  => $order_info['payment_postcode'],
				'zone'      => $order_info['payment_zone'],
				'zone_code' => $order_info['payment_zone_code'],
				'country'   => $order_info['payment_country']
			);

			$datainfo['payment_address'] = str_replace(array("\r\n", "\r", "\n"), '<br />', preg_replace(array("/\s\s+/", "/\r\r+/", "/\n\n+/"), '<br />', trim(str_replace($find, $replace, $format))));;

			$datainfo['payment_method'] = $order_info['payment_method'];

			if ($order_info['shipping_address_format']) {
				$format = $order_info['shipping_address_format'];
			} else {
				$format = '{firstname} {lastname}' . "\n" . '{company}' . "\n" . '{address_1}' . "\n" . '{address_2}' . "\n" . '{city} {postcode}' . "\n" . '{zone}' . "\n" . '{country}';
			}

			$find = array(
				'{firstname}',
				'{lastname}',
				'{company}',
				'{address_1}',
				'{address_2}',
				'{city}',
				'{postcode}',
				'{zone}',
				'{zone_code}',
				'{country}'
			);

			$replace = array(
				'firstname' => $order_info['shipping_firstname'],
				'lastname'  => $order_info['shipping_lastname'],
				'company'   => $order_info['shipping_company'],
				'address_1' => $order_info['shipping_address_1'],
				'address_2' => $order_info['shipping_address_2'],
				'city'      => $order_info['shipping_city'],
				'postcode'  => $order_info['shipping_postcode'],
				'zone'      => $order_info['shipping_zone'],
				'zone_code' => $order_info['shipping_zone_code'],
				'country'   => $order_info['shipping_country']
			);

			$datainfo['shipping_address'] = str_replace(array("\r\n", "\r", "\n"), ' ', preg_replace(array("/\s\s+/", "/\r\r+/", "/\n\n+/"), ' ', trim(str_replace($find, $replace, $format))));

			$datainfo['shipping_method'] = $order_info['shipping_method'];

			$this->load->model('catalog/product');
			$this->load->model('tool/upload');

			// Products
			$datainfo['products'] = array();

			$products = $this->model_account_order->getOrderProductstest($this->request->get['order_id']);
			
			foreach ($products as $product) {
				$option_data = array();

				$options = $this->model_account_order->getOrderOptions($this->request->get['order_id'], $product['order_product_id']);

				foreach ($options as $option) {
					if ($option['type'] != 'file') {
						$value = $option['value'];
					} else {
						$upload_info = $this->model_tool_upload->getUploadByCode($option['value']);

						if ($upload_info) {
							$value = $upload_info['name'];
						} else {
							$value = '';
						}
					}

					$option_data[] = array(
						'name'  => $option['name'],
						'value' => (utf8_strlen($value) > 20 ? utf8_substr($value, 0, 20) . '..' : $value)
					);
				}

				$product_info = $this->model_catalog_product->getProductBySellerIdAPI($product['product_id'], $product['seller_id'], $language_id, $city_id); //$this->model_catalog_product->getProduct($product['product_id']);
				$sellerName  = (isset($language_id) && $language_id == 3) ? $product['lastname'] : $product['firstname'];



				$datainfo['products'][] = array(
					'id' => $product['product_id'],
					'name'     => html_entity_decode($product_info['name']),
					'image'     => $product_info['image'],
					'model'    => $product['model'],
					'upc'    => $product_info['upc'],
					'option'   => $option_data,
					'quantity' => $product['quantity'],
					'price'    => number_format($product['price'] + ($this->config->get('config_tax') ? $product['tax'] : 0), 2),
					'total'    => $this->currency->format($product['total'] + ($this->config->get('config_tax') ? ($product['tax'] * $product['quantity']) : 0), $order_info['currency_code'], $order_info['currency_value']),
					'reorder'  => "",
					'return'   => "",
					'seller_name'  => $sellerName,
					'seller_id'  => $product['seller_id']
				);
			}

			// Voucher
			$datainfo['vouchers'] = array();

			$vouchers = $this->model_account_order->getOrderVouchers($this->request->get['order_id']);

			foreach ($vouchers as $voucher) {
				$datainfo['vouchers'][] = array(
					'description' => $voucher['description'],
					'amount'      => $this->currency->format($voucher['amount'], $order_info['currency_code'], $order_info['currency_value'])
				);
			}

			// Totals
			$datainfo['totals'] = array();

			$totals = $this->model_account_order->getOrderTotals($this->request->get['order_id']);

			$deliveryChargesArr = $this->db->query("
							select oc_sellers.delivery_charges, oc_sellers.seller_id, oc_sellers.firstname, oc_sellers.lastname 
							from oc_order_seller_shipping_fee 
							join oc_sellers on oc_sellers.seller_id = oc_order_seller_shipping_fee.seller_id 
							where oc_order_seller_shipping_fee.order_id = '" . $this->request->get['order_id'] . "'")->rows;

			foreach ($totals as $total) {

				if ($total['code'] == 'total') {
					$deliveryCharges = 0;
					foreach ($deliveryChargesArr as $deliveryChargesRow) {

						$deliveryCharges = $deliveryChargesRow['delivery_charges'];
						$data['totals'][] = array(
							'title' => 'Delivery Charges',
							'text'  => $this->currency->format($deliveryCharges),
						);
						$total['value'] += $deliveryCharges;
					}
				}


				$datainfo['totals'][] = array(
					'title' => $total['title'],
					'text'  => number_format($total['value'], 2),
				);
			}


			$datainfo['comment'] = nl2br($order_info['comment']);

			// History
			$datainfo['histories'] = array();

			$results = $this->model_account_order->getOrderHistories($this->request->get['order_id'], $language_id);

			foreach ($results as $result) {
				$datainfo['histories'][] = array(
					'date_added' => date($this->language->get('date_format_short'), strtotime($result['date_added'])),
					'status'     => $result['status'],
					'comment'    => $result['notify'] ? nl2br($result['comment']) : ''
				);
			}


			//header('Content-Type: application/json');
			echo json_encode($datainfo);
		}
	}
	
	public function info()
	{
		$this->load->language('account/order');
		$this->load->model('account/order');
		$this->load->model('slot/timing');

		$order_id = (int) $this->request->get['order_id'];
		$customer_id = (int) $this->request->get['customer_id'];
		$order_info = $this->model_account_order->getOrderMobile($order_id, $customer_id);
		$language_id =  $this->request->get['language_id'];

		//City_id		
		$city_id = 0;
		if (isset($this->request->get['city_id'])) {
			$city_id = $this->request->get['city_id'];
		}

		if (count($order_info) > 0) {
			if ($order_info['invoice_no']) {
				$datainfo['invoice_no'] = $order_info['invoice_prefix'] . $order_info['invoice_no'];
			} else {
				$datainfo['invoice_no'] = '';
			}

			$datainfo['order_id'] = $this->request->get['order_id'];
			$datainfo['date_added'] = date($this->language->get('date_format_short'), strtotime($order_info['date_added']));
			$datainfo['delivery_date_time'] =  $this->model_slot_timing->getDeliveryTime($order_id);
			if ($order_info['payment_address_format']) {
				$format = $order_info['payment_address_format'];
			} else {
				$format = '{firstname} {lastname}' . "\n" . '{company}' . "\n" . '{address_1}' . "\n" . '{address_2}' . "\n" . '{city} {postcode}' . "\n" . '{zone}' . "\n" . '{country}';
			}

			$find = array(
				'{firstname}',
				'{lastname}',
				'{company}',
				'{address_1}',
				'{address_2}',
				'{city}',
				'{postcode}',
				'{zone}',
				'{zone_code}',
				'{country}'
			);

			$replace = array(
				'firstname' => $order_info['payment_firstname'],
				'lastname'  => $order_info['payment_lastname'],
				'company'   => $order_info['payment_company'],
				'address_1' => $order_info['payment_address_1'],
				'address_2' => $order_info['payment_address_2'],
				'city'      => $order_info['payment_city'],
				'postcode'  => $order_info['payment_postcode'],
				'zone'      => $order_info['payment_zone'],
				'zone_code' => $order_info['payment_zone_code'],
				'country'   => $order_info['payment_country']
			);

			$datainfo['payment_address'] = str_replace(array("\r\n", "\r", "\n"), '<br />', preg_replace(array("/\s\s+/", "/\r\r+/", "/\n\n+/"), '<br />', trim(str_replace($find, $replace, $format))));;

			$datainfo['payment_method'] = $order_info['payment_method'];

			if ($order_info['shipping_address_format']) {
				$format = $order_info['shipping_address_format'];
			} else {
				$format = '{firstname} {lastname}' . "\n" . '{company}' . "\n" . '{address_1}' . "\n" . '{address_2}' . "\n" . '{city} {postcode}' . "\n" . '{zone}' . "\n" . '{country}';
			}

			$find = array(
				'{firstname}',
				'{lastname}',
				'{company}',
				'{address_1}',
				'{address_2}',
				'{city}',
				'{postcode}',
				'{zone}',
				'{zone_code}',
				'{country}'
			);

			$replace = array(
				'firstname' => $order_info['shipping_firstname'],
				'lastname'  => $order_info['shipping_lastname'],
				'company'   => $order_info['shipping_company'],
				'address_1' => $order_info['shipping_address_1'],
				'address_2' => $order_info['shipping_address_2'],
				'city'      => $order_info['shipping_city'],
				'postcode'  => $order_info['shipping_postcode'],
				'zone'      => $order_info['shipping_zone'],
				'zone_code' => $order_info['shipping_zone_code'],
				'country'   => $order_info['shipping_country']
			);

			$datainfo['shipping_address'] = str_replace(array("\r\n", "\r", "\n"), ' ', preg_replace(array("/\s\s+/", "/\r\r+/", "/\n\n+/"), ' ', trim(str_replace($find, $replace, $format))));

			$datainfo['shipping_method'] = $order_info['shipping_method'];

			$this->load->model('catalog/product');
			$this->load->model('tool/upload');

			// Products
			$datainfo['products'] = array();

			$products = $this->model_account_order->getOrderProducts($this->request->get['order_id']);

			foreach ($products as $product) {
				$option_data = array();

				$options = $this->model_account_order->getOrderOptions($this->request->get['order_id'], $product['order_product_id']);

				foreach ($options as $option) {
					if ($option['type'] != 'file') {
						$value = $option['value'];
					} else {
						$upload_info = $this->model_tool_upload->getUploadByCode($option['value']);

						if ($upload_info) {
							$value = $upload_info['name'];
						} else {
							$value = '';
						}
					}

					$option_data[] = array(
						'name'  => $option['name'],
						'value' => (utf8_strlen($value) > 20 ? utf8_substr($value, 0, 20) . '..' : $value)
					);
				}

				$product_info = $this->model_catalog_product->getProductBySellerIdAPI($product['product_id'], $product['seller_id'], $language_id, $city_id); //$this->model_catalog_product->getProduct($product['product_id']);
				$sellerName  = (isset($language_id) && $language_id == 3) ? $product['lastname'] : $product['firstname'];



				$datainfo['products'][] = array(
					'id' => $product['product_id'],
					'name'     => html_entity_decode($product_info['name']),
					'image'     => $product_info['image'],
					'model'    => $product['model'],
					'upc'    => $product_info['upc'],
					'option'   => $option_data,
					'quantity' => $product['quantity'],
					'price'    => number_format($product['price'] + ($this->config->get('config_tax') ? $product['tax'] : 0), 2),
					'total'    => $this->currency->format($product['total'] + ($this->config->get('config_tax') ? ($product['tax'] * $product['quantity']) : 0), $order_info['currency_code'], $order_info['currency_value']),
					'reorder'  => "",
					'return'   => "",
					'seller_name'  => $sellerName,
					'seller_id'  => $product['seller_id']
				);
			}

			// Voucher
			$datainfo['vouchers'] = array();

			$vouchers = $this->model_account_order->getOrderVouchers($this->request->get['order_id']);

			foreach ($vouchers as $voucher) {
				$datainfo['vouchers'][] = array(
					'description' => $voucher['description'],
					'amount'      => $this->currency->format($voucher['amount'], $order_info['currency_code'], $order_info['currency_value'])
				);
			}

			// Totals
			$datainfo['totals'] = array();

			$totals = $this->model_account_order->getOrderTotals($this->request->get['order_id']);

			$deliveryChargesArr = $this->db->query("
							select oc_sellers.delivery_charges, oc_sellers.seller_id, oc_sellers.firstname, oc_sellers.lastname 
							from oc_order_seller_shipping_fee 
							join oc_sellers on oc_sellers.seller_id = oc_order_seller_shipping_fee.seller_id 
							where oc_order_seller_shipping_fee.order_id = '" . $this->request->get['order_id'] . "'")->rows;

			foreach ($totals as $total) {

				if ($total['code'] == 'total') {
					$deliveryCharges = 0;
					foreach ($deliveryChargesArr as $deliveryChargesRow) {

						$deliveryCharges = $deliveryChargesRow['delivery_charges'];
						$data['totals'][] = array(
							'title' => 'Delivery Charges',
							'text'  => $this->currency->format($deliveryCharges),
						);
						$total['value'] += $deliveryCharges;
					}
				}


				$datainfo['totals'][] = array(
					'title' => $total['title'],
					'text'  => number_format($total['value'], 2),
				);
			}


			$datainfo['comment'] = nl2br($order_info['comment']);

			// History
			$datainfo['histories'] = array();

			$results = $this->model_account_order->getOrderHistories($this->request->get['order_id'], $language_id);

			foreach ($results as $result) {
				$datainfo['histories'][] = array(
					'date_added' => date($this->language->get('date_format_short'), strtotime($result['date_added'])),
					'status'     => $result['status'],
					'comment'    => $result['notify'] ? nl2br($result['comment']) : ''
				);
			}


			//header('Content-Type: application/json');
			echo json_encode($datainfo);
		}
	}

	public function infov2()
	{
		$this->load->language('account/order');
		$this->load->model('account/order');
		$this->load->model('slot/timing');

		$order_id = (int) $this->request->get['order_id'];
		$customer_id = (int) $this->request->get['customer_id'];
		$order_info = $this->model_account_order->getOrderMobile($order_id, $customer_id);
		$language_id =  $this->request->get['language_id'];

		//City_id		
		$city_id = 0;
		if (isset($this->request->get['city_id'])) {
			$city_id = $this->request->get['city_id'];
		}

		if (count($order_info) > 0) {
			if ($order_info['invoice_no']) {
				$datainfo['invoice_no'] = $order_info['invoice_prefix'] . $order_info['invoice_no'];
			} else {
				$datainfo['invoice_no'] = '';
			}

			$datainfo['order_id'] = $this->request->get['order_id'];
			$datainfo['date_added'] = date($this->language->get('date_format_short'), strtotime($order_info['date_added']));
			$datainfo['delivery_date_time'] =  $this->model_slot_timing->getDeliveryTime($order_id);
			// $datainfo['delivery_date_time'] =  $this->model_slot_timing->getDeliveryTimeTest($order_id);

			if ($order_info['payment_address_format']) {
				$format = $order_info['payment_address_format'];
			} else {
				$format = '{firstname} {lastname}' . "\n" . '{company}' . "\n" . '{address_1}' . "\n" . '{address_2}' . "\n" . '{city} {postcode}' . "\n" . '{zone}' . "\n" . '{country}';
			}

			$find = array(
				'{firstname}',
				'{lastname}',
				'{company}',
				'{address_1}',
				'{address_2}',
				'{city}',
				'{postcode}',
				'{zone}',
				'{zone_code}',
				'{country}'
			);

			$replace = array(
				'firstname' => $order_info['payment_firstname'],
				'lastname'  => $order_info['payment_lastname'],
				'company'   => $order_info['payment_company'],
				'address_1' => $order_info['payment_address_1'],
				'address_2' => $order_info['payment_address_2'],
				'city'      => $order_info['payment_city'],
				'postcode'  => $order_info['payment_postcode'],
				'zone'      => $order_info['payment_zone'],
				'zone_code' => $order_info['payment_zone_code'],
				'country'   => $order_info['payment_country']
			);

			$datainfo['payment_address'] = str_replace(array("\r\n", "\r", "\n"), '<br />', preg_replace(array("/\s\s+/", "/\r\r+/", "/\n\n+/"), '<br />', trim(str_replace($find, $replace, $format))));;

			$datainfo['payment_method'] = $order_info['payment_method'];

			if ($order_info['shipping_address_format']) {
				$format = $order_info['shipping_address_format'];
			} else {
				$format = '{firstname} {lastname}' . "\n" . '{company}' . "\n" . '{address_1}' . "\n" . '{address_2}' . "\n" . '{city} {postcode}' . "\n" . '{zone}' . "\n" . '{country}';
			}

			$find = array(
				'{firstname}',
				'{lastname}',
				'{company}',
				'{address_1}',
				'{address_2}',
				'{city}',
				'{postcode}',
				'{zone}',
				'{zone_code}',
				'{country}'
			);

			$replace = array(
				'firstname' => $order_info['shipping_firstname'],
				'lastname'  => $order_info['shipping_lastname'],
				'company'   => $order_info['shipping_company'],
				'address_1' => $order_info['shipping_address_1'],
				'address_2' => $order_info['shipping_address_2'],
				'city'      => $order_info['shipping_city'],
				'postcode'  => $order_info['shipping_postcode'],
				'zone'      => $order_info['shipping_zone'],
				'zone_code' => $order_info['shipping_zone_code'],
				'country'   => $order_info['shipping_country']
			);

			$datainfo['shipping_address'] = str_replace(array("\r\n", "\r", "\n"), ' ', preg_replace(array("/\s\s+/", "/\r\r+/", "/\n\n+/"), ' ', trim(str_replace($find, $replace, $format))));

			$datainfo['shipping_method'] = $order_info['shipping_method'];

			$this->load->model('catalog/product');
			$this->load->model('tool/upload');

			// Products
			$datainfo['products'] = array();

			$products = $this->model_account_order->getOrderProducts($this->request->get['order_id']);
			$product_totals = 0;
			foreach ($products as $product) {
				$option_data = array();

				$options = $this->model_account_order->getOrderOptions($this->request->get['order_id'], $product['order_product_id']);

				foreach ($options as $option) {
					if ($option['type'] != 'file') {
						$value = $option['value'];
					} else {
						$upload_info = $this->model_tool_upload->getUploadByCode($option['value']);

						if ($upload_info) {
							$value = $upload_info['name'];
						} else {
							$value = '';
						}
					}

					$option_data[] = array(
						'name'  => $option['name'],
						'value' => (utf8_strlen($value) > 20 ? utf8_substr($value, 0, 20) . '..' : $value)
					);
				}

				$product_info = $this->model_catalog_product->getProductBySellerIdAPI($product['product_id'], $product['seller_id'], $language_id, $city_id); //$this->model_catalog_product->getProduct($product['product_id']);
				$sellerName  = (isset($language_id) && $language_id == 3) ? $product['lastname'] : $product['firstname'];
				$product_status = 'Found';
				if ($product['product_status'] == 2) {
					$product_totals += $product['total'];
					$product_status = 'Not Found';
				}
				$datainfo['products'][] = array(
					'id' => $product['product_id'],
					'name'     => html_entity_decode($product_info['name']),
					'image'     => $product_info['image'],
					'model'    => $product['model'],
					'upc'    => $product_info['upc'],
					'option'   => $option_data,
					'product_status' => $product_status,
					'quantity' => $product['quantity'],
					'price'    => number_format($product['price'] + ($this->config->get('config_tax') ? $product['tax'] : 0), 2),
					'total'    => $this->currency->format($product['total'] + ($this->config->get('config_tax') ? ($product['tax'] * $product['quantity']) : 0), $order_info['currency_code'], $order_info['currency_value']),
					'reorder'  => "",
					'return'   => "",
					'seller_name'  => $sellerName,
					'seller_id'  => $product['seller_id']
				);
			}

			// Voucher
			$datainfo['vouchers'] = array();

			$vouchers = $this->model_account_order->getOrderVouchers($this->request->get['order_id']);

			foreach ($vouchers as $voucher) {
				$datainfo['vouchers'][] = array(
					'description' => $voucher['description'],
					'amount'      => $this->currency->format($voucher['amount'], $order_info['currency_code'], $order_info['currency_value'])
				);
			}

			// Totals
			$datainfo['totals'] = array();

			$totals = $this->model_account_order->getOrderTotals($this->request->get['order_id']);

			$deliveryChargesArr = $this->db->query("
							select oc_sellers.delivery_charges, oc_sellers.seller_id, oc_order_seller_shipping_fee.shipping_charges, oc_sellers.firstname, oc_sellers.lastname 
							from oc_order_seller_shipping_fee 
							join oc_sellers on oc_sellers.seller_id = oc_order_seller_shipping_fee.seller_id 
							where oc_order_seller_shipping_fee.order_id = '" . $this->request->get['order_id'] . "'")->rows;


			foreach ($totals as $total) {
				if ($total['code'] == 'sub_total') {
					$total['value'] -= $product_totals;
				}


				if ($total['code'] == 'total') {
					$deliveryCharges = 0;
					foreach ($deliveryChargesArr as $deliveryChargesRow) {

						$deliveryCharges = $deliveryChargesRow['shipping_charges'];
						$data['totals'][] = array(
							'title' => 'Delivery Charges',
							'text'  => $this->currency->format($deliveryCharges),
						);
						$total['value'] += $deliveryCharges;
					}
					$total['value'] -= $product_totals;
				}


				$datainfo['totals'][] = array(
					'title' => $total['title'],
					'text'  => number_format($total['value'], 2),
				);
			}

			$datainfo['comment'] = nl2br($order_info['comment']);

			// History
			$datainfo['histories'] = array();

			$results = $this->model_account_order->getOrderHistories($this->request->get['order_id'], $language_id);

			foreach ($results as $result) {
				$datainfo['histories'][] = array(
					'date_added' => date($this->language->get('date_format_short'), strtotime($result['date_added'])),
					'status'     => $result['status'],
					'comment'    => $result['notify'] ? nl2br($result['comment']) : ''
				);
			}

			//header('Content-Type: application/json');
			echo json_encode($datainfo);
		}
	}

	

	public function validateOrderProduct()
	{

		$this->load->model('store/store');
		$this->load->model('catalog/product');
		$this->load->model('saccount/seller');
		$this->load->model('tool/image');

		//Parameters

		$product_id = (int) $this->request->get['product_id'];
		$seller_req = (int) $this->request->get['seller_req'];
		$is_area = (int) $this->request->get['is_area'];

		$data = array();
		$data['storeExist'] = false;
		$data['productExist'] = false;

		if ($is_area == 1) //GEt AreaId
		{
			$area_id = (int) $this->request->get['area_id'];
			$storesArray = $this->model_store_store->getStoresByAreaId($area_id);
		} else if ($is_area == 0) {

			$lat = $this->request->get['lat'];
			$lng = $this->request->get['lng'];
			$radiusKm = $this->request->get['radius'];

			$storesArray = $this->model_store_store->getSellerByRadius($radiusKm, $lat, $lng);
		}



		//If Stores Exist
		if ($storesArray) {
			foreach ($storesArray as $store) {
				$storesId[] = $store['seller_id'];
			}
			if (in_array($seller_req, $storesId)) {
				$data['storeExist'] = true;

				$data['seller'] = $this->model_saccount_seller->getSeller($seller_req);
				$data['seller']['image'] = $this->model_tool_image->resize($data['seller']['image'], 720, 406);
				$data['seller']['banner'] = $this->model_tool_image->resize($data['seller']['banner'], 2880, 966);
				$sellerProduct = $this->model_catalog_product->getProductBySellerIdAPI($product_id, $seller_req, 1);
				$data['productExist'] = ($sellerProduct) ? true : false;
			}
		}

		print_r(json_encode($data));
	}

	function mailSendNew()
	{

		$mailContent = '<table style="background-color:#fafbfd; font-family:sans-serif; -webkit-font-smoothing: antialiased; font-size:14px; line-height:1.4; margin:0; padding:0; width: 100%;  color: #5d7686;   padding-top:15px;">
		<tbody>
		<tr></tr>
		<td class="container" style="font-family:sans-serif; font-size:14px; vertical-align: top;color:#5d7686;display:block; margin:0 auto!important; max-width:580px; border-top-left-radius:5px; border-top-right-radius:5px; height:225px; background-color:#69489d">			<div class="content" style="padding:32px 25px 0;">				<p style="text-align:center; padding:0px 0px 20px 0px; margin:0px;"><a href="https://www.qareeb.com"><img src="https://www.qareeb.com/image/mail-image/qareeb_logo.png" class="qareeb-logo" style="width:110px; height:73px; object-fit:contain"></a></p>
		<table role="presentation" class="main" style="background:#fff; border-radius:3px; width:100%">
		<tbody>
		<tr>
		<td class="wrapper" style="font-family:sans-serif; font-size:14px; vertical-align: top;color:#5d7686; box-sizing:border-box; padding:15px">							<p style=" font-size:20px;font-weight:bold; margin-top:10px; font-family:sans-serif; margin-bottom: 25px;">Welcome to Qareeb</p>
		</td>
		</tr>
		</tbody>
		</table>
		</div>
		</td>
		</tr>
		</tbody>
		</table>';

		$img_file = 'https://www.qareeb.com/image/cache/catalog/Salatty_Products/Grocery/Ketchup nd Sauses/KDD Bechamel Sauce 1L-60x60.jpg';
		//echo $imgUrl  = urlencode($img_file);
		$imgUrl = str_replace(' ', '%20', $img_file);


		$img_file2 = 'https://www.qareeb.com/image/cache/catalog/Salatty_Products/Bevarages/starbucks/Starbucks Double Shot Vanilla Bean 11Oz-60x60.jpg';

		$imgUrl2 = str_replace(' ', '%20', $img_file2);



		$mailContent .= '<table style="background-color:#fafbfd;font-family:sans-serif;color:#5d7686;font-size:14px;line-height:1.4;margin:0;padding:0;width:100%">	<tbody>		<tr>						<td style="font-family:sans-serif;font-size:14px;vertical-align:top;color:#5d7686;box-sizing:border-box;display:block;margin:0 auto;max-width:580px;padding:0 25px;border-left:1px solid #e5e6e6;border-right:1px solid #e5e6e6;border-bottom:1px solid #e5e6e6">								<table class="m_7002162342239494702main" style="background:#fff;border-radius:3px;width:100%">					<tbody>						<tr>							<td class="m_7002162342239494702wrapper" style="padding:0px 20px 20px 20px">									<p>Thank you for your bussiness, we received your order our ground team will contact you soon</p>				<p style="font-family:sans-serif;font-size:14px;font-weight:400;margin:0;margin-bottom:15px">To view your order click on the link below:</p>				<p style="font-family:sans-serif;font-size:14px;font-weight:400;margin:0;margin-bottom:15px"><a href="https://www.qareeb.com/index.php?route=account/order/info&amp;order_id=4334" target="_blank" data-saferedirecturl="https://www.google.com/url?q=https://www.qareeb.com/index.php?route%3Daccount/order/info%26order_id%3D4334&amp;source=gmail&amp;ust=1555072241599000&amp;usg=AFQjCNHDcLrgtQhMHSNAkNRr-_LPOJDUVQ">https://www.qareeb.com/index.<wbr>php?route=account/order/info&amp;<wbr>order_id=4334</a></p>				<table style="border-collapse:collapse;width:100%;border:1px solid #dddddd;margin-bottom:20px" cellspacing="0" cellpadding="0" border="0">					<thead>								<tr style="background-color:#efefef">							<td style="font-family:sans-serif;font-size:12px;vertical-align:top;color:#5d7686;border-right:1px solid #dddddd;border-bottom:1px solid #dddddd;background-color:#efefef;font-weight:bold;text-align:left;padding:7px">Order Details</td>							<td style="font-family:sans-serif;font-size:12px;vertical-align:top;color:#5d7686;border-right:1px so lid #dddddd;border-bottom:1px solid #dddddd;background-color:#efefef;font-weight:bold;text-align:right;padding:7px"> HyperMarket</td>						</tr>					</thead>					<tbody>						<tr>							<td style="font-family:sans-serif;font-size:12px;vertical-align:top;color:#5d7686;border-right:1px solid #dddddd;border-bottom:1px solid #dddddd;text-align:left;padding:7px">				<p style="font-family:sans-serif;font-size:12px;font-weight:400;margin:0"><b>Order ID: </b> 4334</p>								<p style="font-family:sans-serif;font-size:12px;font-weight:400;margin:0"><b>Date Added: </b> 11/04/2019</p>								<p style="font-family:sans-serif;font-size:12px;font-weight:400;margin:0"><b>Delivery Date &amp; Time : </b>11-04-2019 05:00 pm - 11-04-2019 07:00 pm</p>								<p style="font-family:sans-serif;font-size:12px;font-weight:400;margin:0"><b>Payment Method : </b>Cash On Delivery</p>								<p style="font-family:sans-serif;font-size:12px;font-weight:400;margin:0"><b>Shipping Method : </b>Free Shipping</p>							</td>							<td style="padding:7px">						<p style="font-family:sans-serif;font-size:12px;font-weight:400;margin:0"><b>E-mail :</b> <a href="mailto:pishago.developer@gmail.com" target="_blank">pishago.developer@gmail.com</a></p>								<p style="font-family:sans-serif;font-size:12px;font-weight:400;margin:0"><b>Mobile :</b> 966541233214</p>								<p style="font-family:sans-serif;font-size:12px;font-weight:400;margin:0"><b>Order Status :</b> Pending</p>							</td>						</tr>					</tbody>				</table>												<table style="border-collapse:collapse;width:100%;border:1px solid #dddddd;margin-bottom:20px">					<thead>								<tr>							<td style="font-family:sans-serif;font-size:12px;vertical-align:top;color:#5d7686;border-right:1px solid #dddddd;border-bottom:1px solid #dddddd;background-color:#efefef;font-weight:bold;text-align:left;padding:7px;color:#222222">Delivery Address</td>						</tr>					</thead>					<tbody>						<tr>							<td>pishago Developer<br>Rajkot<br>Rajkot<br>Riyadh<br>Saudi Arabia</td>						</tr>					</tbody>				</table>								<table style="border-collapse:collapse;width:100%;border-top:1px solid #dddddd;border-left:1px solid #dddddd;margin-bottom:20px">
		<thead> <tr>
		<td style="font-size:12px;border-right:1px solid #dddddd;border-bottom:1px solid #dddddd;background-color:#efefef;font-weight:bold;text-align:left;padding:7px;color:#222222">Image</td>
	  
		<td style="font-size:12px;border-right:1px solid #dddddd;border-bottom:1px solid #dddddd;background-color:#efefef;font-weight:bold;text-align:left;padding:7px;color:#222222">Product</td>
		
		<td style="font-size:12px;border-right:1px solid #dddddd;border-bottom:1px solid #dddddd;background-color:#efefef;font-weight:bold;text-align:right;padding:7px;color:#222222">QTY</td>
		
		<td style="font-size:12px;border-right:1px solid #dddddd;border-bottom:1px solid #dddddd;background-color:#efefef;font-weight:bold;text-align:right;padding:7px;color:#222222">Price</td>
		<td style="font-size:12px;border-right:1px solid #dddddd;border-bottom:1px solid #dddddd;background-color:#efefef;font-weight:bold;text-align:right;padding:7px;color:#222222">Total</td>
		</tr>
		</thead>
		<tbody><tr>
		<td style="font-size:12px;border-right:1px solid #dddddd;border-bottom:1px solid #dddddd;text-align:left;padding:7px"><a href="https://www.qareeb.com/index.php?route=product/product&amp;product_id=8475" target="_blank" data-saferedirecturl="https://www.google.com/url?q=https://www.qareeb.com/index.php?route%3Dproduct/product%26product_id%3D8475&amp;source=gmail&amp;ust=1555072241599000&amp;usg=AFQjCNHxem-8esnOYsMGRhn2xcWRtRMvPA"><img src="' . $imgUrl . '" title="KDD Bechamel Sauce 1L" style="float:left"></a></td>
	  
		<td style="font-size:12px;border-right:1px solid #dddddd;border-bottom:1px solid #dddddd;text-align:left;padding:7px">KDD Bechamel Sauce 1L<br>6271002700517</td><td style="font-size:12px;border-right:1px solid #dddddd;border-bottom:1px solid #dddddd;text-align:right;padding:7px">1</td>
		
		<td style="font-size:12px;border-right:1px solid #dddddd;border-bottom:1px solid #dddddd;text-align:right;padding:7px">SR 14.30</td>
		
		<td style="font-size:12px;border-right:1px solid #dddddd;border-bottom:1px solid #dddddd;text-align:right;padding:7px">SR 14.30</td>
		
		</tr><tr>
			<td style="font-size:12px;border-right:1px solid #dddddd;border-bottom:1px solid #dddddd;text-align:left;padding:7px"><a href="https://www.qareeb.com/index.php?route=product/product&amp;product_id=7004" target="_blank" data-saferedirecturl="https://www.google.com/url?q=https://www.qareeb.com/index.php?route%3Dproduct/product%26product_id%3D7004&amp;source=gmail&amp;ust=1555072241599000&amp;usg=AFQjCNGTovR75IPAlAPJRAvmmbLWlTs6xA"><img src="' . $imgUrl2 . '" title="Starbucks Double Shot Vanilla Bean 11oz" style="float:left"></a></td>
		
			<td style="font-size:12px;border-right:1px solid #dddddd;border-bottom:1px solid #dddddd;text-align:left;padding:7px">Starbucks Double Shot Vanilla Bean 11oz<br>012000044953</td><td style="font-size:12px;border-right:1px solid #dddddd;border-bottom:1px solid #dddddd;text-align:right;padding:7px">5</td>
			
			<td style="font-size:12px;border-right:1px solid #dddddd;border-bottom:1px solid #dddddd;text-align:right;padding:7px">SR 22.20</td>
			
			<td style="font-size:12px;border-right:1px solid #dddddd;border-bottom:1px solid #dddddd;text-align:right;padding:7px">SR 111.00</td>
			
		</tr></tbody><tfoot><tr>
			<td style="font-size:12px;border-right:1px solid #dddddd;border-bottom:1px solid #dddddd;text-align:right;padding:7px" colspan="4"><b>Subtotal:</b></td>
			
			<td style="font-size:12px;border-right:1px solid #dddddd;border-bottom:1px solid #dddddd;text-align:right;padding:7px">SR 125.30</td>
		</tr><tr>
			<td style="font-size:12px;border-right:1px solid #dddddd;border-bottom:1px solid #dddddd;text-align:right;padding:7px" colspan="4"><b> Delivery Charges:</b></td>
			
			<td style="font-size:12px;border-right:1px solid #dddddd;border-bottom:1px solid #dddddd;text-align:right;padding:7px">SR 20.00</td>
		</tr><tr>
			<td style="font-size:12px;border-right:1px solid #dddddd;border-bottom:1px solid #dddddd;text-align:right;padding:7px" colspan="4"><b>Value Added Tax(5 %):</b></td>
			
			<td style="font-size:12px;border-right:1px solid #dddddd;border-bottom:1px solid #dddddd;text-align:right;padding:7px">SR 5.97</td>
		</tr><tr>
			<td style="font-size:12px;border-right:1px solid #dddddd;border-bottom:1px solid #dddddd;text-align:right;padding:7px" colspan="4"><b>Total:</b></td>
			
			<td style="font-size:12px;border-right:1px solid #dddddd;border-bottom:1px solid #dddddd;text-align:right;padding:7px">SR 145.30</td>
		</tr></tfoot></table>																						<p style="font-family:sans-serif;font-size:14px;font-weight:400;margin:0;margin-bottom:15px">Please reply to this e-mail if you have any questions.</p>												<hr style="border:0.5px solid #dde7f2;margin:20px 0">								<p style="font-family:sans-serif;font-size:14px;font-weight:400;margin:0;margin-bottom:15px">Thanks for choosing Qareeb,<br><strong>The Qareeb Team.</strong></p>								</td>						</tr>					</tbody>				</table>				<table class="m_7002162342239494702main" style="padding:10px;text-align:center;width:100%">					<tbody>						<tr>							<td class="m_7002162342239494702wrapper" style="padding:0px 20px 20px 20px">											<p style="font-size:14px;font-weight:bold;clear:both;margin:10px;font-family:sans-serif">DOWNLOAD THE APP</p>								<p style="margin:0px"><a href="https://itunes.apple.com/sa/app//id1453159366" style="color:#3498db;text-decoration:underline" target="_blank" data-saferedirecturl="https://www.google.com/url?q=https://itunes.apple.com/sa/app//id1453159366&amp;source=gmail&amp;ust=1555072241599000&amp;usg=AFQjCNHRO4o7CbisugdnPLR1iIWFVhDxmw"><img src="https://ci3.googleusercontent.com/proxy/_mYTPRy7bOdvEByt8f-fE2UQFM7SZJcnXjy1xU0M3KgWQFmoo_q2VGKm-TY-tTfzqJhi-oB5eFgxWzevX0h0fj_BO-ZbtZ8bX6D3Xw=s0-d-e1-ft#https://www.qareeb.com/image/mail-image/app-store-btn.png" class="m_7002162342239494702app-st m_7002162342239494702ore-btn CToWUd" style="width:120px;height:33px;object-fit:contain"></a><a href="https://play.google.com/store/apps/details?id=com.qareeb.user" style="color:#3498db;text-decoration:underline" target="_blank" data-saferedirecturl="https://www.google.com/url?q=https://play.google.com/store/apps/details?id%3Dcom.qareeb.user&amp;source=gmail&amp;ust=1555072241599000&amp;usg=AFQjCNHVzfLpqRhbyKS2h4NP5gghjBvicA"><img src="https://ci4.googleusercontent.com/proxy/5oXMUxcd-6I3_zKm-xt32bwp5vPsF7hWRsZ_tINEOrJS227YXsmWvATvFgBST4fQbUt8gGiAnHgBOOITv8PQcLLNmNSUDIO8NTGtd5g=s0-d-e1-ft#https://www.qareeb.com/image/mail-image/play-store-btn.png" class="m_7002162342239494702play-store-btn CToWUd" style="width:120px;height:33px;object-fit:contain"></a></p>																</td>						</tr>					</tbody>				</table>							</td>		</tr>	</tbody></table>';



		$mailContent .= '<table style="background-color:#fafbfd; font-family:sans-serif; color: #5d7686; -webkit-font-smoothing:antialiased; font-size:14px; line-height:1.4; margin:0; padding:0;     width:100%;">
		<tbody>
		<tr>
				<td style="font-family:sans-serif; font-size:14px; vertical-align: top;color:#5d7686;max-width:580px;">			<div class="footer" style="clear:both; margin-top:10px; text-align:center; width:100%">			  <table role="presentation" style="border-collapse: separate; mso-table-lspace: 0; mso-table-rspace: 0; width: 100%;" cellspacing="0" cellpadding="0" border="0">
				<tbody>
		<tr>
				  <td class="content-block" style="font-family:sans-serif; font-size:14px; vertical-align: top;color:#5d7686; padding-bottom:10px; padding-top:10px;color:#999; font-size:12px; text-align:center">					<p class="apple-link" style="color:#999; font-size:12px; text-align:center; padding: 0; margin: 0;">You received this email because you signed up for a Qareeb account.</p>
					<div style="margin-top:20px;">													<p style="font-size: 14px; font-weight: 400; margin: 0;"><strong>Stay connected</strong></p>
						<p style="margin: 0;"><a href="https://www.facebook.com/QareebKSA"><img src="https://www.qareeb.com/image/mail-image/icon-fb.png" class="fb" style="margin-left:2px;margin-right:2px"></a><a href="https://twitter.com/QareebKSA"><img src="https://www.qareeb.com/image/mail-image/icon-twitter.png" class="twitter" style="margin-left:2px;margin-right:2px"></a><a href="https://www.instagram.com/QareebKSA/"><img src="https://www.qareeb.com/image/mail-image/icon-insta.png" class="insta" style="margin-right:2px"></a><a href="https://www.snapchat.com/QareebKSA"><img src="https://www.qareeb.com/image/mail-image/icon-snapchat.png" class="snapchat" style="margin-right:2px"></a></p>
					</div>
				  </td>
				</tr>
				<tr>
				  <td class="content-block powered-by" style="font-family:sans-serif; font-size:14px; vertical-align: top;color:#5d7686;color:#999; font-size:12px; text-align:center; text-decoration:none">					<p style="color:#999; font-size:12px; text-align:center;font-family:sans-serif; font-size:14px; font-weight:400; margin:0; margin-bottom:15px; list-style-position:inside; margin-left:5px"><a href="https://www.qareeb.com/index.php?route=information/information&amp;information_id=3" style="color:#999; font-size:12px; text-align:center;">Privacy Policy</a>   |   <a href="https://www.qareeb.com/index.php?route=information/contact" style="color:#999; font-size:12px; text-align:center;">Contact Support</a></p>
					<p style="color:#999; font-size:12px; text-align:center;font-family:sans-serif; font-weight:400; margin:0; margin-bottom:15px;">© 2019 Qareeb Technologies FZ-LLC.</p>
				  </td>
				</tr>
			  </tbody>
		</table>
			</div>
									</td>
					</tr>
		</tbody>
		</table>';


		$mail = new Mail();
		$mail->protocol = 'smtp';
		$mail->parameter = '-f orders@qareeb.com';
		$mail->smtp_hostname = 'ssl://qareeb.com';
		//$mail->smtp_hostname = 'email-smtp.us-east-1.amazonaws.com';
		$mail->smtp_username = 'AKIA233HZBLQWH3ACSVJ';
		$mail->smtp_password = 'BBrK9+0QX5xHzNYoj98CxU/uq/kSHbXZqaXpiTo9PbAr';
		$mail->smtp_port = 25;
		$mail->smtp_timeout = 10;

		$mail->setTo('pishago.developer@gmail.com');
		$mail->setFrom('orders@qareeb.com');
		$mail->setSender('Qareeb', ENT_QUOTES, 'UTF-8');
		$mail->setSubject(html_entity_decode('Testing', ENT_QUOTES, 'UTF-8'));
		$mail->setHtml($mailContent);
		//$mail->setText($text);
		$mail->send();



		echo $mailContent;
		exit;
	}
}
