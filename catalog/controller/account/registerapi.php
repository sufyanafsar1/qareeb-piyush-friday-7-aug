<?php
class ControllerAccountRegisterapi extends Controller
{
	private $error = array();

	public function index()
	{
		
		$this->load->language('account/register');

		$this->load->model('account/customer');

		if ($this->validate()) {

			$customer_id = $this->model_account_customer->addCustomer($this->request->post);

			// Add to activity log
			$this->load->model('account/activity');

			$activity_data = array(
				'customer_id' => $customer_id,
				'name'        => $this->request->post['firstname'] . ' ' . $this->request->post['lastname']
			);

			$this->model_account_activity->addActivity('register', $activity_data);

			$data['message'] = 'success';
		} else {
			if (isset($this->error['firstname'])) {
				$data['message'] = $this->error['firstname'];
			} else if (isset($this->error['lastname'])) {
				$data['message'] = $this->error['lastname'];
			} else if (isset($this->error['email'])) {
				$data['message'] = $this->error['email'];
			} else if (isset($this->error['warning'])) {
				$data['message'] = $this->error['warning'];
			} else if (isset($this->error['password'])) {
				$data['message'] = $this->error['password'];
			} else if (isset($this->error['confirm'])) {
				$data['message'] = $this->error['confirm'];
			}
		}
		print_r(json_encode($data));
	}

	public function validate()
	{
		if ((utf8_strlen(trim($this->request->post['firstname'])) < 1) || (utf8_strlen(trim($this->request->post['firstname'])) > 32)) {
			$this->error['firstname'] = $this->language->get('error_firstname');
		}

		if ((utf8_strlen(trim($this->request->post['lastname'])) < 1) || (utf8_strlen(trim($this->request->post['lastname'])) > 32)) {
			$this->error['lastname'] = $this->language->get('error_lastname');
		}

		if ((utf8_strlen($this->request->post['email']) > 96) || !preg_match('/^[^\@]+@.*.[a-z]{2,15}$/i', $this->request->post['email'])) {
			$this->error['email'] = $this->language->get('error_email');
		}

		if ($this->model_account_customer->getTotalCustomersByEmail($this->request->post['email'])) {
			$this->error['warning'] = $this->language->get('error_exists');
		}

		if ((utf8_strlen($this->request->post['password']) < 4) || (utf8_strlen($this->request->post['password']) > 20)) {
			$this->error['password'] = $this->language->get('error_password');
		}

		if ($this->request->post['confirm'] != $this->request->post['password']) {
			$this->error['confirm'] = $this->language->get('error_confirm');
		}

		return !$this->error;
	}
	public function get_citc_regions()
	{
		$this->load->model('account/address');
		
		$data = $this->model_account_address->get_citc_regions($this->request->get['language_id']);
		echo json_encode($data, JSON_UNESCAPED_UNICODE);
		exit;
	}
	
	public function get_citc_city()
	{
		$this->load->model('account/address');
		$data = $this->model_account_address->get_citc_city($this->request->get['region_id'], $this->request->get['language_id']);
		echo json_encode($data, JSON_UNESCAPED_UNICODE);
		exit;
	}
}
