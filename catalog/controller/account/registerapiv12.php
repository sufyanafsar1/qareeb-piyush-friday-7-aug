<?php
#include ('../payment/includes/autoload.php');
class ControllerAccountRegisterapiv2 extends Controller {
	private $error = array();

	public function index() {
		$this->load->language('account/register');
		$this->load->model('account/customer');

		if ($this->validateRegister()) {
	
			 $data  = $this->request->post;
			
			error_log(print_r($data,true), 3, '/home/centos/app/upload/system/storage/logs/error.log'); 
			 $data["password"] = createPassword();
			
			$customer_id = $this->model_account_customer->addCustomerTemp($data);
			$message = $this->language->get('text_otp') ." ". $data["password"] ." ". $this->language->get('text_otp_continue') ." Qareeb.com ";
			$smsData =  sendSMS((int)$this->request->post['telephone'],$message);
			
					
			error_log(print_r($smsData,true), 3, '/home/centos/app/upload/system/storage/logs/error.log'); 
			
			$data['message'] = 'success';
			$data['sentPassword'] = $data["password"];
			//$data['mobile'] = $this->request->post['telephone'];
		}else{
			
         
			if(isset($this->error['firstname'])){
				$data['message'] = $this->error['firstname'];
			}else if(isset($this->error['lastname'])){
				$data['message'] = $this->error['lastname'];
			}else if(isset($this->error['email'])){
				$data['message'] = $this->error['email'];
			}else if(isset($this->error['warning_mobile'])){
				$data['message'] = $this->error['warning_mobile'];
			}else if(isset($this->error['password'])){
				$data['message'] = $this->error['password'];
			}else if(isset($this->error['confirm'])){
				$data['message'] = $this->error['confirm'];
			}else if(isset($this->error['telephone'])){
				$data['message'] = $this->error['telephone'];
			}else if(isset($this->error['address_1'])){
				$data['message'] = $this->error['address_1'];
			}else if(isset($this->error['warning'])){
				$data['message'] = $this->error['warning'];
			}
			
			
		}
		
		print_r(json_encode($data));
	}
	
	
	

	public function validate() {
		if ((utf8_strlen(trim($this->request->post['firstname'])) < 1) || (utf8_strlen(trim($this->request->post['firstname'])) > 32)) {
			$this->error['firstname'] = $this->language->get('error_firstname');
		}

		if ((utf8_strlen(trim($this->request->post['lastname'])) < 1) || (utf8_strlen(trim($this->request->post['lastname'])) > 32)) {
			$this->error['lastname'] = $this->language->get('error_lastname');
		}

		if ((utf8_strlen($this->request->post['email']) > 96) || !preg_match('/^[^\@]+@.*.[a-z]{2,15}$/i', $this->request->post['email'])) {
			$this->error['email'] = $this->language->get('error_email');
		}

		if ($this->model_account_customer->getTotalCustomersByEmail($this->request->post['email'])) {
			$this->error['warning'] = $this->language->get('error_exists');
		}		


		return !$this->error;
	}
	
	private function validateRegister() {
	    
		if ((utf8_strlen(trim($this->request->post['firstname'])) < 1) || (utf8_strlen(trim($this->request->post['firstname'])) > 32)) {
			$this->error['firstname'] = $this->language->get('error_firstname');
		}

	if ((utf8_strlen(trim($this->request->post['lastname'])) < 1) || (utf8_strlen(trim($this->request->post['lastname'])) > 32)) {
			$this->error['lastname'] = $this->language->get('error_lastname');
		}

		if ((utf8_strlen(trim($this->request->post['email'])) > 96) || !preg_match('/^[^\@]+@.*.[a-z]{2,15}$/i', trim($this->request->post['email']))) {
			$this->error['email'] = $this->language->get('error_email');
		}

		if ($this->model_account_customer->getTotalCustomersByEmail($this->request->post['email'])) {
			$this->error['warning'] = $this->language->get('error_exists');
		}
        

		if ((utf8_strlen((int)$this->request->post['telephone']) < 3) || (utf8_strlen($this->request->post['telephone']) > 32)) {
			$this->error['telephone'] = $this->language->get('error_telephone');
		}
		
		//if(!preg_match('/^(9665)(5|0|3|6|4|9|1|8|7)([0-9]{7})$/', $this->request->post['telephone']))
		if(!preg_match('/^(9665)(5|0|3|6|4|9|1|8|7)([0-9]{7})$/', (int)$this->request->post['telephone']))
        {
           $this->error['telephone'] = $this->language->get('error_telephone');
        }
		
        if ($this->model_account_customer->getTotalCustomersByTelephone((int)$this->request->post['telephone'])) {
			$this->error['warning_mobile'] = $this->language->get('error_exists_mobile');
		}

		if ((utf8_strlen(trim($this->request->post['address_1'])) < 3) || (utf8_strlen(trim($this->request->post['address_1'])) > 128)) {
			$this->error['address_1'] = $this->language->get('error_address_1');
		}
      
		return !$this->error;
	}
	
	public function  updateUserMobileId(){
		$this->load->model('account/customer');
		$userMobileData  = $this->request->post;
		$this->model_account_customer->updateUserMobileId($userMobileData);

		$data['message'] = 'success';
         print_r(json_encode($data));

	}

	public function samplesms() {
		$this->load->language('account/register');
		$this->load->model('account/customer');
			
			$message = "sample test can you see from... Qareeb.com ";
			$smsData =  sendSMS("+966 50 417 7866",$message);
			$smsData =  sendSMS("+923333389786",$message);
			$smsData =  sendSMS("+923214071836",$message);
			
					
			error_log(print_r($smsData,true), 3, '/home/centos/app/upload/system/storage/logs/sms.log'); 
			
			
		}
	
	
	
}
