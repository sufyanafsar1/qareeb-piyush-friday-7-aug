<?php
#include ('../payment/includes/autoload.php');
class ControllerAccountRegisterapiv3test extends Controller {
	private $error = array();

	public function index() {
		
		$this->load->language('account/register');
		$this->load->model('account/customer');

		if ($this->validateRegister()) {
		
			$data_post  = $this->request->post;
		
			error_log(print_r($data_post,true), 3, '/home/centos/app/upload/system/storage/logs/error.log'); 
			 $data_post["password"] = $this->request->post['password'];
			
			$customer_id = $this->model_account_customer->addCustomerTemp($data_post);
			//$message = $this->language->get('text_otp') ." ". $data_post["password"] ." ". $this->language->get('text_otp_continue') ." Qareeb.com ";
			
			$customerdata = $this->model_account_customer->getTempCustomer($this->request->post['telephone'],$this->request->post['password']);	
		
			if(isset($customerdata) && count($customerdata) > 0)
			{
			$customer_id = $this->model_account_customer->addCustomerApi($customerdata);
			
			if(isset($customer_id) && $customer_id > 0) 
			{       
                $data['success'] = true;
                
				$query1 = $this->db->query("SELECT * from oc_customer where customer_id='" . $customer_id . "'");
 
				$data['customer']= $query1->row;
   
                $this->model_account_customer->deleteTempCustomer($this->request->post['telephone'],$this->request->post['password']); 
				
				$this->load->language('mail/forgotten');
			
				$link = $this->url->link('account/forgotten', 'SSL');
				
				$c_name = $customerdata['firstname'] . ' ' . $customerdata['lastname'];
							
				$click_here_link .= '<a href="'.$link.'" >click here</a>';
				
				$this->load->model('catalog/product');	

				$template = $this->model_catalog_product->getEmailTemplate('customer-register-email');	

				$tags = array("[CUSTOMER_NAME]", "[CUSTOMER_EMAIL]", "[CUSTOMER_PASSWORD]", "[CLICK_HERE_LINK]");
				$tagsValues = array($c_name, $customerdata['email'], $customerdata['password'], $click_here_link);
				
				//get subject
				$subject = html_entity_decode($template['subject'], ENT_QUOTES, 'UTF-8');
				$subject = str_replace($tags, $tagsValues, $subject);
				$subject = html_entity_decode($subject, ENT_QUOTES);
					
				//get message body
				$msg = html_entity_decode($template['description'], ENT_QUOTES, 'UTF-8');
				$msg = str_replace($tags, $tagsValues, $msg);
				$msg = html_entity_decode($msg, ENT_QUOTES);
						
				$header = html_entity_decode($this->config->get('config_mail_header'), ENT_QUOTES, 'UTF-8');
				$header = str_replace($tags, $tagsValues, $header);
				$message = html_entity_decode($header, ENT_QUOTES);
				$message .= $msg;
				$message .= html_entity_decode($this->config->get('config_mail_footer'), ENT_QUOTES, 'UTF-8');
				// echo $message;
				// exit;
				$mail = new Mail();
				$mail->protocol = $this->config->get('config_mail_protocol');
				$mail->parameter = $this->config->get('config_mail_parameter');
				$mail->smtp_hostname = $this->config->get('config_mail_smtp_hostname');
				$mail->smtp_username = $this->config->get('config_mail_smtp_username');
				$mail->smtp_password = html_entity_decode($this->config->get('config_mail_smtp_password'), ENT_QUOTES, 'UTF-8');
				$mail->smtp_port = $this->config->get('config_mail_smtp_port');
				$mail->smtp_timeout = $this->config->get('config_mail_smtp_timeout');
				$mail->setTo($customerdata['email']);
				$mail->setFrom($this->config->get('config_email'));
				$mail->setSender(html_entity_decode($this->config->get('config_name'), ENT_QUOTES, 'UTF-8'));
				$mail->setSubject(html_entity_decode($subject, ENT_QUOTES, 'UTF-8'));
				// $mail->setText(html_entity_decode($message, ENT_QUOTES, 'UTF-8'));
				$mail->setHtml($message);
				$mail->send();				
				
			} else {
					$data["error"]="customer can not be added";
			}
          } else {
		   $data["error"]="customer not found";
		  }
			
			//$smsData =  sendSMS((int)$this->request->post['telephone'],$message);
				
			error_log(print_r($smsData,true), 3, '/home/centos/app/upload/system/storage/logs/error.log'); 
		
				//$data['message'] = 'success';
			//$data['sentPassword'] = $data["password"];
			//$data['mobile'] = $this->request->post['telephone'];
		}else{
			
         
			if(isset($this->error['firstname'])){
				$data['message'] = $this->error['firstname'];
			}else if(isset($this->error['lastname'])){
				$data['message'] = $this->error['lastname'];
			}else if(isset($this->error['email'])){
				$data['message'] = $this->error['email'];
			}else if(isset($this->error['warning_mobile'])){
				$data['message'] = $this->error['warning_mobile'];
			}else if(isset($this->error['password'])){
				$data['message'] = $this->error['password'];
			}else if(isset($this->error['confirm'])){
				$data['message'] = $this->error['confirm'];
			}else if(isset($this->error['telephone'])){
				$data['message'] = $this->error['telephone'];
			}else if(isset($this->error['address_1'])){
				$data['message'] = $this->error['address_1'];
			}else if(isset($this->error['warning'])){
				$data['message'] = $this->error['warning'];
			}
		}
		
		print_r(json_encode($data));
	}
	
	public function validate() {
		if ((utf8_strlen(trim($this->request->post['firstname'])) < 1) || (utf8_strlen(trim($this->request->post['firstname'])) > 32)) {
			$this->error['firstname'] = $this->language->get('error_firstname');
		}

		if ((utf8_strlen(trim($this->request->post['lastname'])) < 1) || (utf8_strlen(trim($this->request->post['lastname'])) > 32)) {
			$this->error['lastname'] = $this->language->get('error_lastname');
		}

		if ((utf8_strlen($this->request->post['email']) > 96) || !preg_match('/^[^\@]+@.*.[a-z]{2,15}$/i', $this->request->post['email'])) {
			$this->error['email'] = $this->language->get('error_email');
		}

		if ($this->model_account_customer->getTotalCustomersByEmail($this->request->post['email'])) {
			$this->error['warning'] = $this->language->get('error_exists');
		}		


		return !$this->error;
	}
	
	private function validateRegister() {
	    
		if ((utf8_strlen(html_entity_decode($this->request->post['password'], ENT_QUOTES, 'UTF-8')) < 4) || (utf8_strlen(html_entity_decode($this->request->post['password'], ENT_QUOTES, 'UTF-8')) > 40)) {
			$this->error['password'] = $this->language->get('error_password');
		}

		if ($this->request->post['confirm'] != $this->request->post['password']) {
			$this->error['confirm'] = $this->language->get('error_confirm');
		}
		
		if ((utf8_strlen(trim($this->request->post['firstname'])) < 1) || (utf8_strlen(trim($this->request->post['firstname'])) > 32)) {
			$this->error['firstname'] = $this->language->get('error_firstname');
		}

		if ((utf8_strlen(trim($this->request->post['lastname'])) < 1) || (utf8_strlen(trim($this->request->post['lastname'])) > 32)) {
			$this->error['lastname'] = $this->language->get('error_lastname');
		}

		if ((utf8_strlen(trim($this->request->post['email'])) > 96) || !preg_match('/^[^\@]+@.*.[a-z]{2,15}$/i', trim($this->request->post['email']))) {
			$this->error['email'] = $this->language->get('error_email');
		}

		if ($this->model_account_customer->getTotalCustomersByEmail($this->request->post['email'])) {
			$this->error['warning'] = $this->language->get('error_exists');
		}
        

		if ((utf8_strlen((int)$this->request->post['telephone']) < 3) || (utf8_strlen($this->request->post['telephone']) > 32)) {
			$this->error['telephone'] = $this->language->get('error_telephone');
		}
		
		//if(!preg_match('/^(9665)(5|0|3|6|4|9|1|8|7)([0-9]{7})$/', $this->request->post['telephone']))
		if(!preg_match('/^(9665)(5|0|3|6|4|9|1|8|7)([0-9]{7})$/', (int)$this->request->post['telephone']))
        {
           $this->error['telephone'] = $this->language->get('error_telephone');
        }
		
        if ($this->model_account_customer->getTotalCustomersByTelephone((int)$this->request->post['telephone'])) {
			$this->error['warning_mobile'] = $this->language->get('error_exists_mobile');
		}

		if ((utf8_strlen(trim($this->request->post['address_1'])) < 3) || (utf8_strlen(trim($this->request->post['address_1'])) > 128)) {
			$this->error['address_1'] = $this->language->get('error_address_1');
		}
      
		return !$this->error;
	}
	
	public function  updateUserMobileId(){
		$this->load->model('account/customer');
		$userMobileData  = $this->request->post;
		$this->model_account_customer->updateUserMobileId($userMobileData);

		$data['message'] = 'success';
         print_r(json_encode($data));

	}

	public function samplesms() {
		$this->load->language('account/register');
		$this->load->model('account/customer');
			
			$message = "sample test can you see from... Qareeb.com ";
			$smsData =  sendSMS("+966 50 417 7866",$message);
			$smsData =  sendSMS("+923333389786",$message);
			$smsData =  sendSMS("+923214071836",$message);
			
					
			error_log(print_r($smsData,true), 3, '/home/centos/app/upload/system/storage/logs/sms.log'); 
			
			
		}
	
	
	
}
