<?php
class ControllerAccountResetPwdLink extends Controller {
	private $error = array();

	public function index() {
		
		if ($this->customer->isLogged()) {
			$this->response->redirect($this->url->link('account/account', '', 'SSL'));
		}
	
		if (isset($this->request->get['code'])) {
			$code = $this->request->get['code'];
		} else {
			$code = '';
		}
		
		
		//$email = 'pishago.developer@gmail.com';
		$this->load->model('account/customer');

		$customer_info = $this->model_account_customer->getCustomerByEmail($this->request->get['email']);

		if (isset($this->request->get['email'])) {
			 $email = $this->request->get['email'];
		} else {
			$email = '';
		}
		if (1>0) {
		
			$this->load->language('account/reset_pwd_link');

			$this->document->setTitle($this->language->get('heading_title'));
			//echo $this->request->server['REQUEST_METHOD'];
			if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validate() == 1) {
			
					 $password = $this->request->post['password'];
					
					 $email = $this->request->post['email'];
					
					$this->model_account_customer->editPassword($email, $password);

					$this->session->data['success'] = $this->language->get('text_success');

					$this->response->redirect($this->url->link('common/home', '', 'SSL'));
			}
			 
			$data['heading_title'] = $this->language->get('heading_title');

			$data['text_password'] = $this->language->get('text_password');

			$data['entry_password'] = $this->language->get('entry_password');
			$data['entry_confirm'] = $this->language->get('entry_confirm');
			$data['email'] = $customer_info['email'];
			$data['button_save'] = $this->language->get('button_save');
			$data['button_cancel'] = $this->language->get('button_cancel');
			$data['button_continue'] = $this->language->get('button_continue');
			$data['email'] = $email;
			$data['breadcrumbs'] = array();

			$data['breadcrumbs'][] = array(
				'text' => $this->language->get('text_home'),
				'href' => $this->url->link('account/account', '', 'SSL')
			);

			$data['breadcrumbs'][] = array(
				'text' => $this->language->get('heading_title'),
				'href' => $this->url->link('account/reset_pwd_link', '', 'SSL')
			);

			if (isset($this->error['password'])) {
				$data['error_password'] = $this->error['password'];
			} else {
				$data['error_password'] = '';
			}

			if (isset($this->error['confirm'])) {
				$data['error_confirm'] = $this->error['confirm'];
			} else {
				$data['error_confirm'] = '';
			}

			$data['action'] = $this->url->link('account/reset_pwd_link', '' , 'SSL');

			$data['cancel'] = $this->url->link('commom/home', '', 'SSL');

			if (isset($this->request->post['password'])) {
				$data['password'] = $this->request->post['password'];
			} else {
				$data['password'] = '';
			}

			if (isset($this->request->post['email'])) {
				$data['email'] = $this->request->post['email'];
			} 

			if (isset($this->request->post['confirm'])) {
				$data['confirm'] = $this->request->post['confirm'];
			} else {
				$data['confirm'] = '';
			}

			$data['column_left'] = $this->load->controller('common/column_left');
			$data['column_right'] = $this->load->controller('common/column_right');
			$data['content_top'] = $this->load->controller('common/content_top');
			$data['content_bottom'] = $this->load->controller('common/content_bottom');
			$data['footer'] = $this->load->controller('common/footer');
			$data['header'] = $this->load->controller('common/header');
			//echo'<pre>'; print_r($data); exit;
	
			
			if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/template/account/reset_pwd_link.tpl')) {
			$this->response->setOutput($this->load->view($this->config->get('config_template') . '/template/account/reset_pwd_link.tpl', $data));
			} else {
				$this->response->setOutput($this->load->view('default/template/account/reset_pwd_link.tpl', $data));
			}
			
			
			
		} else {
			/*$this->load->model('setting/setting');

			$this->model_setting_setting->editSettingValue('config', 'config_password', '0');*/

			return new Action('common/home');
		}
	}

	protected function validate() {
		$error= 0;
		if ((utf8_strlen($this->request->post['password']) < 4) || (utf8_strlen($this->request->post['password']) > 20)) {
			$this->error['password'] = $this->language->get('error_password');
			$error= 1;
		}

		if ($this->request->post['confirm'] != $this->request->post['password']) {
			$this->error['confirm'] = $this->language->get('error_confirm');
			$error= 1;
		}
		if($error == 1){
			return false;
		}else{
			return true; 	
		}
	}
}