<?php
class ControllerAccountReviewApi extends Controller {
	
	public function index() {
		
		$json =array();
		
		$this->language->load('account/review');

		$this->document->setTitle($this->language->get('heading_title'));
		
		$this->load->model('account/review');
		
		if (!$this->customer->isLogged()) {
			$json['error'] = 'please login first';
		} else if ((utf8_strlen($this->request->post['author']) < 3) || (utf8_strlen($this->request->post['author']) > 64)) {
			$json['error_author'] = $this->language->get('error_author');
			
		} else if (empty($this->request->post['rating'])) {
			$json['error_rating'] = $this->language->get('error_rating');
			
		} else if (empty($this->request->post['rate_rider'])) {
			$json['error_rate_rider'] = $this->language->get('error_rate_rider');
			
		} else if (empty($this->request->post['overall_rating'])) {
			$json['error_overall_rating'] = $this->language->get('error_overall_rating');
			
		} else if (utf8_strlen($this->request->post['text']) < 1) {
			$json['error_text'] = $this->language->get('error_text');
			
		} else if (!$this->request->post['seller_id']) {
			$json['error'] = $this->language->get('error_seller');
			
		} else if (!$this->request->post['order_id']) {
			$json['error'] = $this->language->get('error_order');
		}
			
		if(!$json) {			
			$this->model_account_review->addReview($this->request->post);		
			$json['success'] = $this->language->get('text_success');
		}
		
		$this->response->addHeader('Content-Type: application/json');
		$this->response->setOutput(json_encode($json));
	}	
}
?>