<?php
class ControllerAccountValidateUserApi extends Controller {
	private $error = array();

	public function index() {
		$this->load->language('account/register');

		$this->load->model('account/customer');
      	
      	$responce = array();
        $customerdata = $this->model_account_customer->getTempCustomer($this->request->post['mobile'],$this->request->post['password']);	
		if(isset($customerdata) && count($customerdata) > 0)
		{
			$customer_id = $this->model_account_customer->addCustomerApi($customerdata);
			
			if(isset($customer_id) && $customer_id > 0) 
			{       
                $responce['success'] = true;
                 $query1 = $this->db->query("SELECT * from oc_customer where customer_id='" . $customer_id . "'");
 
				$responce['customer']= $query1->row;
   
                $this->model_account_customer->deleteTempCustomer($this->request->post['mobile'],$this->request->post['password']); 
				
				$this->load->language('mail/forgotten');
			
				$link = $this->url->link('account/forgotten', 'SSL');
				
				$c_name = $customerdata['firstname'] . ' ' . $customerdata['lastname'];
							
				$click_here_link .= '<a href="'.$link.'" >click here</a>';
				
				$this->load->model('catalog/product');	

				$template = $this->model_catalog_product->getEmailTemplate('customer-register-email');	

				$tags = array("[CUSTOMER_NAME]", "[CUSTOMER_EMAIL]", "[CUSTOMER_PASSWORD]", "[CLICK_HERE_LINK]");
				$tagsValues = array($c_name, $customerdata['email'], $customerdata['password'], $click_here_link);
				
				//get subject
				$subject = html_entity_decode($template['subject'], ENT_QUOTES, 'UTF-8');
				$subject = str_replace($tags, $tagsValues, $subject);
				$subject = html_entity_decode($subject, ENT_QUOTES);
					
				//get message body
				$msg = html_entity_decode($template['description'], ENT_QUOTES, 'UTF-8');
				$msg = str_replace($tags, $tagsValues, $msg);
				$msg = html_entity_decode($msg, ENT_QUOTES);
						
				$header = html_entity_decode($this->config->get('config_mail_header'), ENT_QUOTES, 'UTF-8');
				$header = str_replace($tags, $tagsValues, $header);
				$message = html_entity_decode($header, ENT_QUOTES);
				$message .= $msg;
				$message .= html_entity_decode($this->config->get('config_mail_footer'), ENT_QUOTES, 'UTF-8');
				
				$mail = new Mail();
				$mail->protocol = $this->config->get('config_mail_protocol');
				$mail->parameter = $this->config->get('config_mail_parameter');
				$mail->smtp_hostname = $this->config->get('config_mail_smtp_hostname');
				$mail->smtp_username = $this->config->get('config_mail_smtp_username');
				$mail->smtp_password = html_entity_decode($this->config->get('config_mail_smtp_password'), ENT_QUOTES, 'UTF-8');
				$mail->smtp_port = $this->config->get('config_mail_smtp_port');
				$mail->smtp_timeout = $this->config->get('config_mail_smtp_timeout');
				$mail->setTo($customerdata['email']);
				$mail->setFrom($this->config->get('config_email'));
				$mail->setSender(html_entity_decode($this->config->get('config_name'), ENT_QUOTES, 'UTF-8'));
				$mail->setSubject(html_entity_decode($subject, ENT_QUOTES, 'UTF-8'));
				// $mail->setText(html_entity_decode($message, ENT_QUOTES, 'UTF-8'));
				$mail->setHtml($message);
				$mail->send();
								
                /* $link = $this->url->link('account/forgotten', 'SSL');
				$c_name = $customerdata['firstname'] . ' ' . $customerdata['lastname'];
				$subject = "Thank you for registering at Careeb.com";
				$message  = '<html dir="ltr" lang="en">' . "\n";
				$message .= '  <head>' . "\n";
				$message .= '    <title> Subject </title>' . "\n";
				$message .= '    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">' . "\n";
				$message .= '  </head>' . "\n";
				$message .= '  <body> ' . "\n";
				$message .= '  <p>Dear '.$c_name.' </p>' . "</p><br>";
				$message .= '  <p>Welcome https://www.qareeb.com Thank you for registering with us.' . "</p><br>";
				$message .= '  <p>Your login information is as follows: ' . "</p>";
				$message .= '  <p>Username : '.$customerdata['email']. "</p>";
				$message .= '  <p>Password: ' .$customerdata['password']. "</p><br><br>";
				$message .= '  <p>To login, please access here type the username given above.' . "</p><br>";
				$message .= '  <p>If you wish to reset your password please <a href="' . $link . '">click here</a>' . "</p><br>";
				$message .= '  <p>We request you to make note of this information and keep safe place for future reference.' . "</p><br>";
				$message .= '  <p>For any questions or queries, please contact Customer Support at support@careeb.com' . "</p>";
				

				$message .= '</body></html>' . "\n";
                $mail = new Mail();
				$mail->protocol = $this->config->get('config_mail_protocol');
				$mail->parameter = $this->config->get('config_mail_parameter');
				$mail->smtp_hostname = $this->config->get('config_mail_smtp_hostname');
				$mail->smtp_username = $this->config->get('config_mail_smtp_username');
				$mail->smtp_password = html_entity_decode($this->config->get('config_mail_smtp_password'), ENT_QUOTES, 'UTF-8');
				$mail->smtp_port = $this->config->get('config_mail_smtp_port');
				$mail->smtp_timeout = $this->config->get('config_mail_smtp_timeout');
				$mail->setTo($customerdata['email']);
				$mail->setFrom($this->config->get('config_email'));
				$mail->setSender(html_entity_decode($this->config->get('config_name'), ENT_QUOTES, 'UTF-8'));
				$mail->setSubject(html_entity_decode($subject, ENT_QUOTES, 'UTF-8'));
				// $mail->setText(html_entity_decode($message, ENT_QUOTES, 'UTF-8'));
				
				$mail->setHtml($message);
				try{
					@$mail->send(); 
				}
				catch(Exception $e){
					echo "unable to send an email";
					print_r($e);

				} */
				
			} else {
					$responce["error"]="customer can not be added";
			}
          }	else
		      {
			   $responce["error"]="customer not found";
		      }

		print_r(json_encode($responce));
	}

	
}