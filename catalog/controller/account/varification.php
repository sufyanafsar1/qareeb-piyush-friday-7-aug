<?php
class ControllerAccountVarification extends Controller {
	private $error = array();

	public function index() {
		

		if(!isset($this->session->data['optCode'])){
			$this->response->redirect($this->url->link('account/register', '', true));
		}	
		
		if ($this->customer->isLogged()) {
			$this->response->redirect($this->url->link('account/account', '', true));
		}

		$this->load->language('account/varification');

		$this->document->setTitle($this->language->get('heading_title'));

		$this->load->model('account/customer');
	
		
		$postedData  = $this->request->post; // get mobile and password
		
		$customerdata = $this->model_account_customer->getTempCustomer($this->session->data['telephone'],$this->session->data['optCode']);        // get temp customer data
		
		if(isset($customerdata) && count($customerdata) > 0)
		{
			$data  = $customerdata;
		}
		
		/* $characters = '123456789';
		$charactersLength = strlen($characters);
		$password = '';
		for ($i = 0; $i < 6; $i++) {
			$password .= $characters[rand(0, $charactersLength - 1)];
		}	
		 */
		$data['password'] = $this->session->data['password'];
	
		$customer_id = $this->model_account_customer->addCustomer($data);
		
		$this->load->language('mail/forgotten');
		
		$link = $this->url->link('account/forgotten', 'SSL');
		
		$c_name = $data['firstname'] . ' ' . $data['lastname'];
					
		$click_here_link .= '<a href="'.$link.'" >click here</a>';
		
		
		$this->load->model('catalog/product');	

		$template = $this->model_catalog_product->getEmailTemplate('customer-register-email');	

		$tags = array("[CUSTOMER_NAME]", "[CUSTOMER_EMAIL]", "[CUSTOMER_PASSWORD]", "[CLICK_HERE_LINK]");
		$tagsValues = array($c_name, $data['email'], $data['password'], $click_here_link);
		
		//get subject
		$subject = html_entity_decode($template['subject'], ENT_QUOTES, 'UTF-8');
		$subject = str_replace($tags, $tagsValues, $subject);
		$subject = html_entity_decode($subject, ENT_QUOTES);
			
		//get message body
		$msg = html_entity_decode($template['description'], ENT_QUOTES, 'UTF-8');
		$msg = str_replace($tags, $tagsValues, $msg);
		$msg = html_entity_decode($msg, ENT_QUOTES);
				
		$header = html_entity_decode($this->config->get('config_mail_header'), ENT_QUOTES, 'UTF-8');
		$header = str_replace($tags, $tagsValues, $header);
		$message = html_entity_decode($header, ENT_QUOTES);
		$message .= $msg;
		$message .= html_entity_decode($this->config->get('config_mail_footer'), ENT_QUOTES, 'UTF-8');
		
		$mail = new Mail();
		$mail->protocol = $this->config->get('config_mail_protocol');
		$mail->parameter = $this->config->get('config_mail_parameter');
		$mail->smtp_hostname = $this->config->get('config_mail_smtp_hostname');
		$mail->smtp_username = $this->config->get('config_mail_smtp_username');
		$mail->smtp_password = html_entity_decode($this->config->get('config_mail_smtp_password'), ENT_QUOTES, 'UTF-8');
		$mail->smtp_port = $this->config->get('config_mail_smtp_port');
		$mail->smtp_timeout = $this->config->get('config_mail_smtp_timeout');
		$mail->setTo($data['email']);
		$mail->setFrom($this->config->get('config_email'));
		$mail->setSender(html_entity_decode($this->config->get('config_name'), ENT_QUOTES, 'UTF-8'));
		$mail->setSubject(html_entity_decode($subject, ENT_QUOTES, 'UTF-8'));
		// $mail->setText(html_entity_decode($message, ENT_QUOTES, 'UTF-8'));
		$mail->setHtml($message);
		$mail->send();
		
		// Clear any previous login attempts for unregistered accounts.
		$this->model_account_customer->deleteLoginAttempts($this->session->data['email']);

		//$this->customer->login($this->request->post['email'], $this->request->post['password']);
		$this->customer->login($this->session->data['email'], $data['password']);
		
		unset($this->session->data['guest']);
		unset($this->session->data['password']);
		// Add to activity log
		$this->load->model('account/activity');

			$activity_data = array(
			'customer_id' => $customer_id,
			'name'        => $data['firstname'] . ' ' . $data['lastname']
		);

		$this->model_account_activity->addActivity('register', $activity_data);
		
		unset($this->session->data['optCode']);
		
		$this->response->redirect($this->url->link('account/success', '', 'SSL'));
		
		/* if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validate()) {
					
			$postedData  = $this->request->post; // get mobile and password
			$customerdata = $this->model_account_customer->getTempCustomer($this->request->post['telephone'],$this->request->post['password']);        // get temp customer data
			
			if(isset($customerdata) && count($customerdata) > 0)
			{
				$data  = $customerdata;
			}
			
			$characters = '123456789';
			$charactersLength = strlen($characters);
			$password = '';
			for ($i = 0; $i < 6; $i++) {
				$password .= $characters[rand(0, $charactersLength - 1)];
			}	
			
			$data['password'] = $password;
        
			$customer_id = $this->model_account_customer->addCustomer($data);
            
			$this->load->language('mail/forgotten');
			
			$link = $this->url->link('account/forgotten', 'SSL');
			
			$c_name = $data['firstname'] . ' ' . $data['lastname'];
						
			$click_here_link .= '<a href="'.$link.'" >click here</a>';
			
			
			$this->load->model('catalog/product');	

			$template = $this->model_catalog_product->getEmailTemplate('customer-register-email');	

			$tags = array("[CUSTOMER_NAME]", "[CUSTOMER_EMAIL]", "[CUSTOMER_PASSWORD]", "[CLICK_HERE_LINK]");
			$tagsValues = array($c_name, $data['email'], $data['password'], $click_here_link);
			
			//get subject
			$subject = html_entity_decode($template['subject'], ENT_QUOTES, 'UTF-8');
			$subject = str_replace($tags, $tagsValues, $subject);
			$subject = html_entity_decode($subject, ENT_QUOTES);
				
			//get message body
			$msg = html_entity_decode($template['description'], ENT_QUOTES, 'UTF-8');
			$msg = str_replace($tags, $tagsValues, $msg);
			$msg = html_entity_decode($msg, ENT_QUOTES);
					
			$header = html_entity_decode($this->config->get('config_mail_header'), ENT_QUOTES, 'UTF-8');
			$header = str_replace($tags, $tagsValues, $header);
			$message = html_entity_decode($header, ENT_QUOTES);
			$message .= $msg;
			$message .= html_entity_decode($this->config->get('config_mail_footer'), ENT_QUOTES, 'UTF-8');
			
			$mail = new Mail();
			$mail->protocol = $this->config->get('config_mail_protocol');
			$mail->parameter = $this->config->get('config_mail_parameter');
			$mail->smtp_hostname = $this->config->get('config_mail_smtp_hostname');
			$mail->smtp_username = $this->config->get('config_mail_smtp_username');
			$mail->smtp_password = html_entity_decode($this->config->get('config_mail_smtp_password'), ENT_QUOTES, 'UTF-8');
			$mail->smtp_port = $this->config->get('config_mail_smtp_port');
			$mail->smtp_timeout = $this->config->get('config_mail_smtp_timeout');
			$mail->setTo($data['email']);
			$mail->setFrom($this->config->get('config_email'));
			$mail->setSender(html_entity_decode($this->config->get('config_name'), ENT_QUOTES, 'UTF-8'));
			$mail->setSubject(html_entity_decode($subject, ENT_QUOTES, 'UTF-8'));
			// $mail->setText(html_entity_decode($message, ENT_QUOTES, 'UTF-8'));
			$mail->setHtml($message);
			$mail->send();
            
			// Clear any previous login attempts for unregistered accounts.
			$this->model_account_customer->deleteLoginAttempts($this->request->post['email']);

			$this->customer->login($this->request->post['email'], $this->request->post['password']);
			
			unset($this->session->data['guest']);

			// Add to activity log
			$this->load->model('account/activity');

				$activity_data = array(
				'customer_id' => $customer_id,
				'name'        => $data['firstname'] . ' ' . $data['lastname']
			);

			$this->model_account_activity->addActivity('register', $activity_data);
			
			unset($this->session->data['optCode']);
            
			$this->response->redirect($this->url->link('account/success', '', 'SSL'));
		} */

		$data['breadcrumbs'] = array();

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_home'),
			'href' => $this->url->link('common/home')
		);

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_account'),
			'href' => $this->url->link('account/account', '', true)
		);

		$data['breadcrumbs'][] = array(
			'text' => 'Varification Your OTP',
			'href' => $this->url->link('account/varification', '', true)
		);

		if (isset($this->error['warning'])) {
			$data['error_warning'] = $this->error['warning'];
		} else {
			$data['error_warning'] = '';
		}

		$data['action'] = $this->url->link('account/varification', '', true);
		$data['resend'] = $this->url->link('account/register/resendPassword&telephone='.$this->session->data['telephone'], '', true);
		

		$data['back'] = $this->url->link('account/login', '', true);

		if (isset($this->request->post['password'])) {
			$data['password'] = $this->request->post['password'];
		} else {
			$data['password'] = '';
		}
		
		if (isset($this->session->data['optCode'])) {
			$data['passwordSent'] = $this->session->data['optCode'];
		} else {
			$data['passwordSent'] = '';
		}
		
		if (isset($this->session->data['telephone'])) {
			$data['telephone'] = $this->session->data['telephone'];
		} else {
			$data['telephone'] = '';
		}
		
		$data['text_varification'] = $this->language->get('text_varification');
		$data['heading_title'] = $this->language->get('heading_title');
		
		$data['button_continue'] = $this->language->get('button_continue');
		$data['button_resend'] = $this->language->get('button_resend');
		$data['entry_password'] = $this->language->get('entry_password');
		
		$data['column_left'] = $this->load->controller('common/column_left');
		$data['column_right'] = $this->load->controller('common/column_right');
		$data['content_top'] = $this->load->controller('common/content_top');
		$data['content_bottom'] = $this->load->controller('common/content_bottom');
		$data['footer'] = $this->load->controller('common/footer');
		$data['header'] = $this->load->controller('common/header');

		$this->response->setOutput($this->load->view('default/template/account/varification.tpl', $data));
	}

	protected function validate() {
							
		if (!isset($this->request->post['password'])) {
			$this->error['warning'] = $this->language->get('error_email');
		} 
		
		if ($this->request->post['password'] != $this->request->post['passwordSent']) {
			$this->error['warning'] = $this->language->get('error_approved');
		} 
		
		return !$this->error;
	}
}
