<?php
class ControllerAccountWishList extends Controller {
	public function index() {
		if (!$this->customer->isLogged()) {
			$this->session->data['redirect'] = $this->url->link('account/wishlist', '', 'SSL');

			$this->response->redirect($this->url->link('account/login', '', 'SSL'));
		}

		$this->load->language('account/wishlist');

		$this->load->model('account/wishlist');

		$this->load->model('catalog/product');

		$this->load->model('tool/image');

		if (isset($this->request->get['remove'])) {
			// Remove Wishlist
			$this->model_account_wishlist->deleteWishlistBySeller($this->request->get['remove'],$this->request->get['seller_id']);
            //Remove item from session
            $existInList = false;
            foreach ($this->session->data['wishlist'] as $key => $value) {
				if ($value[0] == $this->request->get['remove'] && $value[2] == $this->request->get['seller_id']) {
					$existInList = true;
					$index = $key;
				}
			}
			if ($existInList) {
				unset($this->session->data['wishlist'][$index]);
			}
            // $index = array_search($this->request->get['remove'], $this->session->data['wishlist']);			
			

			$this->session->data['success'] = $this->language->get('text_remove');

			$this->response->redirect($this->url->link('account/wishlist'));
		}

		$this->document->setTitle($this->language->get('heading_title'));

		$data['breadcrumbs'] = array();

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_home'),
			'href' => $this->url->link('common/home')
		);

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_account'),
			'href' => $this->url->link('account/account', '', 'SSL')
		);

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('heading_title'),
			'href' => $this->url->link('account/wishlist')
		);

		$data['heading_title'] = $this->language->get('heading_title');

		$data['text_empty'] = $this->language->get('text_empty');
 	    $data['text_seller'] = $this->language->get('text_seller');
        $data['text_category'] = $this->language->get('text_category');
        
		$data['column_image'] = $this->language->get('column_image');
		$data['column_name'] = $this->language->get('column_name');
		$data['column_model'] = $this->language->get('column_model');
		$data['column_stock'] = $this->language->get('column_stock');
		$data['column_price'] = $this->language->get('column_price');
		$data['column_action'] = $this->language->get('column_action');

		$data['button_continue'] = $this->language->get('button_continue');
		$data['button_cart'] = $this->language->get('button_cart');
		$data['button_remove'] = $this->language->get('button_remove');

		if (isset($this->session->data['success'])) {
			$data['success'] = $this->session->data['success'];

			unset($this->session->data['success']);
		} else {
			$data['success'] = '';
		}

		$data['products'] = array();
		if (isset($this->request->get['cat']) && !empty($this->request->get['cat'])) {
			$cat = $this->request->get['cat'];
			$results = $this->model_account_wishlist->getWishlistByCategory($cat);	
		}else{
			$results = $this->model_account_wishlist->getWishlist();	
		}
         $languageId = (int)$this->config->get('config_language_id');
        $this->load->model('catalog/seller');
		foreach ($results as $result) {
		
			$product_info = $this->model_catalog_product->getProductBySellerId($result['product_id'],$result['seller_id']);
            $seller_info = $this->model_catalog_seller->getSeller($result['seller_id']);
           
			if ($product_info) {
				if ($product_info['image']) {
					$image = $this->model_tool_image->resize($product_info['image'], $this->config->get('config_image_wishlist_width'), $this->config->get('config_image_wishlist_height'));
				} else {
					$image = false;
				}

				if ($product_info['quantity'] <= 0) {
					$stock = $product_info['stock_status'];
				} elseif ($this->config->get('config_stock_display')) {
					$stock = $product_info['quantity'];
				} else {
					$stock = $this->language->get('text_instock');
				}

				if (($this->config->get('config_customer_price') && $this->customer->isLogged()) || !$this->config->get('config_customer_price')) {
					$price = $this->currency->format($this->tax->calculate($product_info['price'], $product_info['tax_class_id'], $this->config->get('config_tax')));
				} else {
					$price = false;
				}

				if ((float)$product_info['special']) {
					$special = $this->currency->format($this->tax->calculate($product_info['special'], $product_info['tax_class_id'], $this->config->get('config_tax')));
				} else {
					$special = false;
				}
                 $store_name = ($languageId==1)? $seller_info['firstname'] : $seller_info['lastname'];
				$data['products'][] = array(
					'product_id' => $product_info['product_id'],
					'thumb'      => $image,
					'name'       => $product_info['name'],
                    'minimum'       => $product_info['minimum'],
					'model'      => $product_info['model'],
					'stock'      => $stock,
					'price'      => $price,
                    'seller_id'  => $result['seller_id'],
                    'seller_name' => $store_name,
                    'category_name' => $result['category_name'],
					'special'    => $special,
					'href'       => $this->url->link('product/product', 'product_id=' . $product_info['product_id']),
					'remove'     => $this->url->link('account/wishlist', 'remove=' . $product_info['product_id'].'&seller_id='.$result['seller_id'])
				);
               
			} else {
				$this->model_account_wishlist->deleteWishlistBySeller($result['product_id'],$result['seller_id']);
			}
		}
        
        $data['product_data'] = array();
		$data['cartData'] = $this->cart->getProducts();
        	if (!count($data['cartData'])) {
			$data['cartData'] = array();
		}
		$data['product_data'] = array(
			'products'       => $data['products'],			
			'cart_data'		 => $data['cartData']
		);

		$data['continue'] = $this->url->link('account/account', '', 'SSL');

		$data['column_left'] = $this->load->controller('common/column_left');
		$data['column_right'] = $this->load->controller('common/column_right');
		$data['content_top'] = $this->load->controller('common/content_top');
		$data['content_bottom'] = $this->load->controller('common/content_bottom');
		$data['footer'] = $this->load->controller('common/footer');
		$data['header'] = $this->load->controller('common/header');
		$this->load->model('account/wishlist');
		$data['wishlist_cat'] = $this->model_account_wishlist->getWishlistCategory();
		
		if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/template/account/wishlist.tpl')) {
			$this->response->setOutput($this->load->view($this->config->get('config_template') . '/template/account/wishlist.tpl', $data));
		} else {
			$this->response->setOutput($this->load->view('default/template/account/wishlist.tpl', $data));
		}
	}

	public function add() {
		$this->load->language('account/wishlist');

		$json = array();

		if (isset($this->request->post['wishlist_product_id'])) {
			$product_id = $this->request->post['wishlist_product_id'];
		} else {
			$product_id = 0;
		}
		if ($this->request->post['wishlist_catergory'] == 'custom') {
			$cat_name = $this->request->post['wishlist_new_category'] != ''? $this->request->post['wishlist_new_category'] : 'Main';
		}else{
			$cat_name = $this->request->post['wishlist_category'] != ''? $this->request->post['wishlist_category'] : 'Main';
		}
		$this->load->model('catalog/product');

		$product_info = $this->model_catalog_product->getProduct($product_id);

		if ($product_info) {
			if (!isset($this->session->data['wishlist'])) {
				$this->session->data['wishlist'] = array();
			}
			$existInList = true;
			foreach ($this->session->data['wishlist'] as $key => $value) {
				if ($value[0] == $product_id) {
					$existInList = false;
				}
			}
			if ($existInList) {
				$this->session->data['wishlist'][] = array($product_id,$cat_name);
			}
			
			if ($this->customer->isLogged()) {
				// Edit customers cart
				$this->load->model('account/wishlist');

				$this->model_account_wishlist->addWishlistCategory($product_id, $cat_name);

				$json['success'] = sprintf($this->language->get('text_success'), $this->url->link('product/product', 'product_id=' . (int)$product_id), $product_info['name'], $this->url->link('account/wishlist'));

				$json['total'] = sprintf($this->language->get('text_wishlist'), $this->model_account_wishlist->getTotalWishlist());
			} else {
				
				$json['success'] = sprintf($this->language->get('text_login'), $this->url->link('account/login', '', 'SSL'), $this->url->link('account/register', '', 'SSL'), $this->url->link('product/product', 'product_id=' . (int)$product_id), $product_info['name'], $this->url->link('account/wishlist'));

				$json['total'] = sprintf($this->language->get('text_wishlist'), (isset($this->session->data['wishlist']) ? count($this->session->data['wishlist']) : 0));
			}
		}

		$this->response->addHeader('Content-Type: application/json');
		$this->response->setOutput(json_encode($json));
	}

	public function wishlist_product_remove() {
		$this->load->language('account/wishlist');

		$json = array();

		if (isset($this->request->post['product_id'])) {
			$product_id = $this->request->post['product_id'];
		} else {
			$product_id = 0;
		}
        if (isset($this->request->post['seller_id'])) {
			$seller_id = $this->request->post['seller_id'];
		} else {
			$seller_id = 1;
		}
		
		if ($this->customer->isLogged()) {
			// Edit customers cart
			$this->load->model('account/wishlist');
			$existInList = false;
			foreach ($this->session->data['wishlist'] as $key => $value) {
				if ($value[0] == $product_id && $value[2] == $seller_id) {
					$existInList = true;
					$index = $key;
				}
			}
			if ($existInList) {
				unset($this->session->data['wishlist'][$index]);
			}
			
			// $index = array_search($this->request->post['product_id'], $this->session->data['wishlist']);
			
			// unset($this->session->data['wishlist'][$index]);
			
			$this->model_account_wishlist->deleteWishlistBySeller($this->request->post['product_id'],$this->request->post['seller_id']);

			$json['success'] = sprintf($this->language->get('text_remove'), $this->url->link('product/product', 'product_id=' . (int)$this->request->post['product_id']), $product_info['name'], $this->url->link('account/wishlist'));

			$json['total'] = sprintf($this->language->get('text_wishlist'), $this->model_account_wishlist->getTotalWishlist());
		} else {
			$existInList = false;
			foreach ($this->session->data['wishlist'] as $key => $value) {
				if ($value[0] == $product_id && $value[2] == $seller_id) {
					$existInList = true;
					$index = $key;
				}
			}
			if ($existInList) {
				unset($this->session->data['wishlist'][$index]);
			}
			// $index = array_search($this->request->post['product_id'], $this->session->data['wishlist']);
			
			// unset($this->session->data['wishlist'][$index]);

			$json['success'] = sprintf($this->language->get('text_login'), $this->url->link('account/login', '', 'SSL'), $this->url->link('account/register', '', 'SSL'), $this->url->link('product/product', 'product_id=' . (int)$this->request->post['product_id']), $product_info['name'], $this->url->link('account/wishlist'));

			$json['total'] = sprintf($this->language->get('text_wishlist'), (isset($this->session->data['wishlist']) ? count($this->session->data['wishlist']) : 0));
		}

		$this->response->addHeader('Content-Type: application/json');
		$this->response->setOutput(json_encode($json));
	}
}
