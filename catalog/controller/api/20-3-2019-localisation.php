<?php
class ControllerApiLocalisation extends Controller {

    //Get Cities By LanguageId
    public function getAllCities(){


        $languageId = (isset($this->request->get['language'])) ? $this->request->get['language'] : 1;

        //Get All Cities
        $this->load->model('localisation/city');
        $cities = array();
        $cities = $this->model_localisation_city->getCitiesByLanguage($languageId);

        if(count($cities) > 0){
            $data = array('msg' => 'success', 'result' => $cities);
        }else{
            $data = array('msg' => 'failed', 'result' => '');
        }

        echo json_encode($data);

    }


    //Get Areas By City
    public function getAreasByCityId(){


        $city_id = (int)$this->request->get['cityId'];
        $languageId = (isset($this->request->get['language'])) ? $this->request->get['language'] : 1;

        //Get All Areas
        $this->load->model('localisation/area');

        $areas = array();
        $areas = $this->model_localisation_area->getAreasByCityIdV2($city_id,$languageId);

        if(count($areas) > 0){
            $data = array('msg' => 'success', 'result' => $areas);
        }else{
            $data = array('msg' => 'failed', 'result' => '');
        }

        echo json_encode($data);

    }
	
	//Get Seller Info Using lat-lng
    public function getlatlngBySeller(){


        $lat = $this->request->get['lat'];
		$lng = $this->request->get['lng'];
		
        //Get All Areas
        $this->load->model('localisation/area');

        $sellerinfo = array();
        $sellerinfo = $this->model_localisation_area->getLatLngBySellerInfo($lat,$lng);
		
        echo json_encode($sellerinfo);

    }

    //Get Stores By Area
    public function getStoresByAreabk(){

        $area_id = (int)$this->request->get['areaId'];
        $this->load->model('store/store');
        $this->load->model('tool/image');

        //Get Sellers against area_id
        $stores = $this->model_store_store->getStoresByAreaId($area_id);
        foreach($stores as $key => $value)
        {
            $stores[$key]['image'] = $this->model_tool_image->resize($stores[$key]['image'], 720, 406);
            $stores[$key]['banner'] = $this->model_tool_image->resize($stores[$key]['banner'], 1498,286);
        }

        if(count($stores) > 0){
            $data = array('msg' => 'success', 'result' => $stores);
        }else{
            $data = array('msg' => 'failed', 'result' => '');
        }

        echo json_encode($data);
    }
	
	//Get Stores By Area
    public function getStoresTimeContent(){	
		
        $seller_id = (int)$this->request->get['seller_id'];
		$language = (int)$this->request->get['language'];
		
        $this->load->model('store/store');
        $this->load->model('tool/image');
		$this->load->model('catalog/seller');

        //Get Sellers against area_id
        $stores = $this->model_store_store->getStoreSellerTimeContent($seller_id);
		
		$timeArray = $this->getStoreTimeContent($stores);
						
        echo json_encode($timeArray);
    }
	
	//Get Stores By Area
    public function getStoresByArea(){	
		
        $area_id = (int)$this->request->get['areaId'];
        $this->load->model('store/store');
        $this->load->model('tool/image');
		$this->load->model('catalog/seller');

        //Get Sellers against area_id
        $stores = $this->model_store_store->getStoresByAreaId($area_id);
		foreach($stores as $key => $value)
        {		 
			$storesArray[$key]['seller_id'] = $stores[$key]['seller_id'];
			$storesArray[$key]['firstname'] = $stores[$key]['firstname'];
			$storesArray[$key]['lastname'] = $stores[$key]['lastname'];
			$storesArray[$key]['email'] = $stores[$key]['email'];
			$storesArray[$key]['minimum_order'] = $stores[$key]['minimum_order'];
			$storesArray[$key]['delivery_charges'] = number_format($stores[$key]['delivery_charges'], 2);
			
            $storesArray[$key]['image'] = $this->model_tool_image->resize($stores[$key]['image'], 720, 406);
            $storesArray[$key]['banner'] = $this->model_tool_image->resize($stores[$key]['banner'], 1498,286);
			
			$timeArray = $this->getStoreDeliveryTime($value);
			
			$storesArray[$key]['time_content'] = $timeArray['time_content'];
			$storesArray[$key]['time'] = $timeArray['time'];
			$storesArray[$key]['store_status'] = $timeArray['store_status'];
			
			$storesArray[$key]['rating'] = number_format($this->model_catalog_seller->getSellerRating($stores[$key]['seller_id']),1);
			
        }
		
        if(count($storesArray) > 0){
            $data = array('msg' => 'success', 'result' => $storesArray);
        }else{
            $data = array('msg' => 'failed', 'result' => '');
        }

        echo json_encode($data);
    }
	
	public function getStoresByArea2(){	
		
        $area_id = (int)$this->request->get['areaId'];
        $this->load->model('store/store');
        $this->load->model('tool/image');
		$this->load->model('catalog/seller');

        //Get Sellers against area_id
        $stores = $this->model_store_store->getStoresByAreaId($area_id);
		echo '<pre>';
		foreach($stores as $key => $value)
        {		 
			$storesArray[$key]['seller_id'] = $stores[$key]['seller_id'];
			$storesArray[$key]['firstname'] = $stores[$key]['firstname'];
			$storesArray[$key]['lastname'] = $stores[$key]['lastname'];
			$storesArray[$key]['email'] = $stores[$key]['email'];
			$storesArray[$key]['minimum_order'] = $stores[$key]['minimum_order'];
			$storesArray[$key]['delivery_charges'] = number_format($stores[$key]['delivery_charges'], 2);
			
            $storesArray[$key]['image'] = $this->model_tool_image->resize($stores[$key]['image'], 720, 406);
            $storesArray[$key]['banner'] = $this->model_tool_image->resize($stores[$key]['banner'], 1498,286);
			
			$timeArray = $this->getStoreDeliveryTime2($value);
			
			$storesArray[$key]['time_content'] = $timeArray['time_content'];
			$storesArray[$key]['time'] = $timeArray['time'];
			$storesArray[$key]['store_status'] = $timeArray['store_status'];
			
			$storesArray[$key]['rating'] = number_format($this->model_catalog_seller->getSellerRating($stores[$key]['seller_id']),1);
			
        }
		
        if(count($storesArray) > 0){
            $data = array('msg' => 'success', 'result' => $storesArray);
        }else{
            $data = array('msg' => 'failed', 'result' => '');
        }

        echo json_encode($data);
    }
	
	public function getStoreDeliveryTime2($seller_info){
		print_r($seller_info);
		
		$text_close          = 'Close';		
		$text_today          = 'Today';
		$text_tomorrow       = 'Tomorrow';			
		$text_next_delivery  = 'Next Delivery';
		$text_no_delivery    = 'No Delivery Available';
		$text_unavailable    = 'Unavailable';
		$languageId = (isset($this->request->get['language'])) ? $this->request->get['language'] : 1;
		date_default_timezone_get();
		
		$now = strtotime("now");		
		$interval = $seller_info["delivery_timegap"] * 60;
		$package = $seller_info["package_ready"] * 60;
		$nowdata = dayData("now");
		print_r($nowdata);
		
		$fdata = dayData(" +1 day");			
		$sdata = dayData(" +2 day");
		$tdata = dayData(" +3 day");
		$ffdata = dayData(" +4 day");
		$fffdata = dayData(" +5 day");
		$ssdata = dayData(" +6 day");
		
			
		if ( $seller_info[strtolower(substr($nowdata["half"], 0, 3)) . "_start_time"] != "00:00:00" && $seller_info[strtolower(substr($nowdata["half"], 0, 3)) . "_end_time"] != "00:00:00" ) {
			
			
			$nowdata["date"]. " " . $seller_info[strtolower(substr($nowdata["half"], 0, 3)) . "_start_time"] ;
		
			$nowstarttime = strtotime($nowdata["date"]. " " . $seller_info[strtolower(substr($nowdata["half"], 0, 3)) . "_start_time"]) + $interval;
			
			$nowendtime = strtotime($nowdata["date"]. " " . $seller_info[strtolower(substr($nowdata["half"], 0, 3)) . "_end_time"]);
			
			$nowoutput = ""; $nownextdelivery = ""; $nowdelivery = ""; $j = 0;
			
			for( $i = $nowstarttime; $i < $nowendtime; $i += $interval) {
				echo date('h:i:s',$i);
				
				echo '---';
			}
						
			for( $i = $nowstarttime; $i < $nowendtime; $i += $interval) {
				$nntt = $i + $interval;				
				if( $i < $now ) {				
					$nowoutput.= "<li class=unavailable>unavailable</li>";
					$sellerinfo['time_content'] = $text_no_delivery;
					$sellerinfo['time'] = '00:00 pm - 00:00 pm';						
				} else {					
					if ( $j == 0 ) {
						$nextTime = $now + $package;
						if ($i < $nextTime) {							
							$nowoutput.= "<li class=unavailable> echo $text_unavailable</li>";
							$sellerinfo['time_content'] = $text_no_delivery;
							$sellerinfo['time'] = '00:00 pm - 00:00 pm';						
						
						} else {
							
							$nownextdelivery = arabicTime(strtotime(date("h:i a", $i)),$languageId);
						
							$nowdelivery = arabicTime(strtotime(date("h:i a", $i)),$languageId)." - ".arabicTime(strtotime(date("h:i a", $nntt)),$languageId);
							
							$sellerinfo['time_content'] = $text_next_delivery.' '.$text_today.' '.$nowdelivery;
							$sellerinfo['time'] = $nowdelivery;												
							
							$nowoutput.= "<li>".arabicTime(strtotime(date("h:i a", $i)),$languageId)." to ".arabicTime(strtotime(date("h:i a", $nntt)),$languageId)."</li>";
							$j++;
						}
					} else {						
						$nowoutput.= "<li>".arabicTime(strtotime(date("h:i a", $i)),$languageId)." to ".arabicTime(strtotime(date("h:i a", $nntt)),$languageId)."</li>"; $j++;
					}
				}				
			}
			
			if(($nowstarttime < $now) && ($nowendtime > $now)){
				$sellerinfo['store_status'] = $text_open;				
			}else {
				$sellerinfo['store_status'] = 'Close';
			}				
			
			if($nowdata["full"] !=	date("l")){				

				if ( $seller_info[strtolower(substr($fdata["half"], 0, 3)) . "_start_time"] != "00:00:00" && $seller_info[strtolower(substr($fdata["half"], 0, 3)) . "_end_time"] != "00:00:00" ) {
					
					$fstarttime = strtotime($fdata["date"]. " " . $seller_info[strtolower(substr($fdata["half"], 0, 3)) . "_start_time"]);
					
					$fendtime = strtotime($fdata["date"]. " " . $seller_info[strtolower(substr($fdata["half"], 0, 3)) . "_end_time"]);
					
					$foutput = ""; $fnextdelivery = ""; $fdelivery = ""; $k = 0;
					
					for( $i = $fstarttime; $i < $fendtime; $i += $interval) {
						$nntt = $i + $interval;
						if( $i < $now ) {
							$foutput.= "<li class=unavailable>echo $text_unavailable</li>";
							$sellerinfo['time_content'] = $text_no_delivery;
							$sellerinfo['time'] = '00:00 pm - 00:00 pm';							
						} else {
							if ( $k == 0 ) {
								$nextTime = $now + $package;
								if ($i < $nextTime) {
									$foutput.= "<li class=unavailable>echo $text_unavailable</li>";

									$sellerinfo['time_content'] = $text_no_delivery;
									$sellerinfo['time'] = '00:00 pm - 00:00 pm';								
								} else {
									$fnextdelivery = arabicTime(strtotime(date("h:i a", $i)),$languageId);
									
									$fdelivery = arabicTime(strtotime(date("h:i a", $i)),$languageId)." - ".arabicTime(strtotime(date("h:i a", $nntt)),$languageId);
																	
									$sellerinfo['time_content'] = $text_next_delivery.' '.$text_tomorrow.' '.$fdelivery;
									$sellerinfo['time'] = $fdelivery;									
									
									$foutput.= "<li>".arabicTime(strtotime(date("h:i a", $i)),$languageId)." to ".arabicTime(strtotime(date("h:i a", $nntt)),$languageId)."</li>"; $k++;
								}
							} else {
								$foutput.= "<li>".arabicTime(strtotime(date("h:i a", $i)),$languageId)." to ".arabicTime(strtotime(date("h:i a", $nntt)),$languageId)."</li>"; $k++;
									
							}
							
						}
					}
				}
								
				if(($fstarttime < $now) && ($fendtime > $now)){
					$sellerinfo['store_status'] = 'Open';				
				} else {
					$sellerinfo['store_status'] = 'Close';
				}				
				 
			}

			print_r($sellerinfo);
			exit;	
			if(empty($sellerinfo)){
				$sellerinfo['time_content'] = "No Delivery Available";
				$sellerinfo['time'] = '00:00 pm - 00:00 pm';
				$sellerinfo['store_status'] = 'Close';				
			}
		} else if ( $seller_info[strtolower(substr($fdata["half"], 0, 3)) . "_start_time"] != "00:00:00" && $seller_info[strtolower(substr($fdata["half"], 0, 3)) . "_end_time"] != "00:00:00" ) {
			$fstarttime = strtotime($fdata["date"]. " " . $seller_info[strtolower(substr($fdata["half"], 0, 3)) . "_start_time"]) + $interval;
			
			$fendtime = strtotime($fdata["date"]. " " . $seller_info[strtolower(substr($fdata["half"], 0, 3)) . "_end_time"]);
			
			$foutput = ""; $fnextdelivery = ""; $fdelivery = ""; $k = 0;
			
			for( $i = $fstarttime; $i < $fendtime; $i += $interval) {
				$nntt = $i + $interval;
				if( $i < $now ) {
					$foutput.= "<li class=unavailable>echo $text_unavailable</li>";
					$sellerinfo['time_content'] = $text_no_delivery;
					$sellerinfo['time'] = '00:00 pm - 00:00 pm';
					$sellerinfo['store_status'] = 'Close';
				} else {
					if ( $k == 0 ) {
						$nextTime = $now + $package;
						if ($i < $nextTime) {
							$foutput.= "<li class=unavailable>echo $text_unavailable</li>";

							$sellerinfo['time_content'] = $text_no_delivery;
							$sellerinfo['time'] = '00:00 pm - 00:00 pm';
							$sellerinfo['store_status'] = 'Close';
						} else {
							$fnextdelivery = arabicTime(strtotime(date("h:i a", $i)),$languageId);
							
							$fdelivery = arabicTime(strtotime(date("h:i a", $i)),$languageId)." - ".arabicTime(strtotime(date("h:i a", $nntt)),$languageId);
															
							$sellerinfo['time_content'] = $text_next_delivery.' '.$text_tomorrow.' '.$fdelivery;
							$sellerinfo['time'] = $fdelivery;
							$sellerinfo['store_status'] = 'Close';
							
							$foutput.= "<li>".arabicTime(strtotime(date("h:i a", $i)),$languageId)." to ".arabicTime(strtotime(date("h:i a", $nntt)),$languageId)."</li>"; $k++;
						}
					} else {
						$foutput.= "<li>".arabicTime(strtotime(date("h:i a", $i)),$languageId)." to ".arabicTime(strtotime(date("h:i a", $nntt)),$languageId)."</li>"; $k++;
							
					}
					
				}
			}
		} else if ( $seller_info[strtolower(substr($sdata["half"], 0, 3)) . "_start_time"] != "00:00:00" && $seller_info[strtolower(substr($sdata["half"], 0, 3)) . "_end_time"] != "00:00:00" ) {
			$sstarttime = strtotime($sdata["date"]. " " . $seller_info[strtolower(substr($sdata["half"], 0, 3)) . "_start_time"]) + $interval;
			
			$sendtime = strtotime($sdata["date"]. " " . $seller_info[strtolower(substr($sdata["half"], 0, 3)) . "_end_time"]);
			
			$soutput = ""; $snextdelivery = ""; $sdelivery = ""; $l = 0;
			for( $i = $sstarttime; $i < $sendtime; $i += $interval) {
				$nntt = $i + $interval;
				if( $i < $now ) {
					$soutput.= "<li class=unavailable>echo $text_unavailable</li>";
					$sellerinfo['time_content'] = $text_no_delivery;
					$sellerinfo['time'] = '00:00 pm - 00:00 pm';
					$sellerinfo['store_status'] = 'Close';
				} else {
					if ( $l == 0 ) {
						$nextTime = $now + $package;
						if ($i < $nextTime) {
							$soutput.= "<li class=unavailable>echo $text_unavailable</li>";
							
							$sellerinfo['time_content'] = $text_no_delivery;
							$sellerinfo['time'] = '00:00 pm - 00:00 pm';
							$sellerinfo['store_status'] = 'Close';
						} else {
							$snextdelivery = arabicTime(strtotime(date("h:i a", $i)),$languageId);
							
							$name = strtolower($sdata["full"]);
							$sdelivery = $days[$name] . " " . arabicTime(strtotime(date("h:i a", $i)),$languageId)." - ".arabicTime(strtotime(date("h:i a", $nntt)),$languageId);
															
							$sellerinfo['time_content'] = $text_next_delivery.' '.$sdelivery;							
							$sellerinfo['time'] = $sdelivery;
							$sellerinfo['store_status'] = 'Close';
							
							$soutput.= "<li>".arabicTime(strtotime(date("h:i a", $i)),$languageId)." to ".arabicTime(strtotime(date("h:i a", $nntt)),$languageId)."</li>"; $l++;
						}
					} else {
						$soutput.= "<li>".arabicTime(strtotime(date("h:i a", $i)),$languageId)." to ".arabicTime(strtotime(date("h:i a", $nntt)),$languageId)."</li>"; $l++;						
					}
				}
			}
		} else if ( $seller_info[strtolower(substr($tdata["half"], 0, 3)) . "_start_time"] != "00:00:00" && $seller_info[strtolower(substr($tdata["half"], 0, 3)) . "_end_time"] != "00:00:00" ) {
			$tstarttime = strtotime($tdata["date"]. " " . $seller_info[strtolower(substr($tdata["half"], 0, 3)) . "_start_time"]) + $interval;
			
			$tendtime = strtotime($tdata["date"]. " " . $seller_info[strtolower(substr($tdata["half"], 0, 3)) . "_end_time"]);
			
			$toutput = ""; $tnextdelivery = ""; $tdelivery = ""; $m = 0;
				
			for( $i = $tstarttime; $i < $tendtime; $i += $interval) {
				$nntt = $i + $interval;
				if( $i < $now ) {
					$toutput.= "<li class=unavailable>echo $text_unavailable</li>";
					
					$sellerinfo['time_content'] = $text_no_delivery;
					$sellerinfo['time'] = '00:00 pm - 00:00 pm';
					$sellerinfo['store_status'] = 'Close';
				} else {
					if ( $m == 0 ) {
						$nextTime = $now + $package;
						if ($i < $nextTime) {
							$toutput.= "<li class=unavailable>echo $text_unavailable</li>";
							
							$sellerinfo['time_content'] = $text_no_delivery;
							$sellerinfo['time'] = '00:00 pm - 00:00 pm';
							$sellerinfo['store_status'] = 'Close';
						} else {
							$tnextdelivery = arabicTime(strtotime(date("h:i a", $i)),$languageId);
							
							$name = strtolower($tdata["full"]);
							
							$tdelivery = $days[$name] . " " . arabicTime(strtotime(date("h:i a", $i)),$languageId)." - ".arabicTime(strtotime(date("h:i a", $nntt)),$languageId);
							
							$sellerinfo['time_content'] = $text_next_delivery.' '.$tdelivery;							
							$sellerinfo['time'] = $tdelivery;
							$sellerinfo['store_status'] = 'Close';
							
							$toutput.= "<li>".arabicTime(strtotime(date("h:i a", $i)),$languageId)." to ".arabicTime(strtotime(date("h:i a", $nntt)),$languageId)."</li>"; $m++;
						}
					} else {
						$toutput.= "<li>".arabicTime(strtotime(date("h:i a", $i)),$languageId)." to ".arabicTime(strtotime(date("h:i a", $nntt)),$languageId)."</li>"; $m++;						
					}
				}
			}
		} else if ( $seller_info[strtolower(substr($ffdata["half"], 0, 3)) . "_start_time"] != "00:00:00" && $seller_info[strtolower(substr($ffdata["half"], 0, 3)) . "_end_time"] != "00:00:00" ) {
			$ffstarttime = strtotime($ffdata["date"]. " " . $seller_info[strtolower(substr($ffdata["half"], 0, 3)) . "_start_time"]) + $interval;
			
			$ffendtime = strtotime($ffdata["date"]. " " . $seller_info[strtolower(substr($ffdata["half"], 0, 3)) . "_end_time"]);
			
			$ffoutput = ""; $ffnextdelivery = ""; $ffdelivery = ""; $n = 0;
			
			for( $i = $ffstarttime; $i < $ffendtime; $i += $interval) {
				$nntt = $i + $interval;
				if( $i < $now ) {
					$foutput.= "<li class=unavailable>echo $text_unavailable</li>";

					$sellerinfo['time_content'] = $text_no_delivery;
					$sellerinfo['time'] = '00:00 pm - 00:00 pm';
					$sellerinfo['store_status'] = 'Close';
				} else {
					if ( $n == 0 ) {
						$nextTime = $now + $package;
						if ($i < $nextTime) {
							$ffoutput.= "<li class=unavailable>echo $text_unavailable</li>";

							$sellerinfo['time_content'] = $text_no_delivery;
							$sellerinfo['time'] = '00:00 pm - 00:00 pm';
							$sellerinfo['store_status'] = 'Close';
						} else {
							$ffnextdelivery = arabicTime(strtotime(date("h:i a", $i)),$languageId);
							$name = strtolower($ffdata["full"]);
							$ffdelivery = $days[$name] . " " . arabicTime(strtotime(date("h:i a", $i)),$languageId)." - ".arabicTime(strtotime(date("h:i a", $nntt)),$languageId);
							
							$sellerinfo['time_content'] = $text_next_delivery.' '.$ffdelivery;							
							$sellerinfo['time'] = $nowdelivery;
							$sellerinfo['store_status'] = 'Close';
			
							$ffoutput.= "<li>".arabicTime(strtotime(date("h:i a", $i)),$languageId)." to ".arabicTime(strtotime(date("h:i a", $nntt)),$languageId)."</li>"; $n++;
						}
					} else {
						$ffoutput.= "<li>".arabicTime(strtotime(date("h:i a", $i)),$languageId)." to ".arabicTime(strtotime(date("h:i a", $nntt)),$languageId)."</li>"; $n++;
					}
				}
			}
		} else if ( $seller_info[strtolower(substr($fffdata["half"], 0, 3)) . "_start_time"] != "00:00:00" && $seller_info[strtolower(substr($fffdata["half"], 0, 3)) . "_end_time"] != "00:00:00" ) {
			$fffstarttime = strtotime($fffdata["date"]. " " . $seller_info[strtolower(substr($fffdata["half"], 0, 3)) . "_start_time"]) + $interval;
			
			$fffendtime = strtotime($fffdata["date"]. " " . $seller_info[strtolower(substr($fffdata["half"], 0, 3)) . "_end_time"]);
			
			$fffoutput = ""; $fffnextdelivery = ""; $fffdelivery = ""; $o = 0;
			
			for( $i = $fffstarttime; $i < $fffendtime; $i += $interval) {
				$nntt = $i + $interval;
				if( $i < $now ) {
					$fffoutput.= "<li class=unavailable>echo $text_unavailable</li>";

					$sellerinfo['time_content'] = $text_no_delivery;
					$sellerinfo['time'] = '00:00 pm - 00:00 pm';
					$sellerinfo['store_status'] = 'Close';
				} else {
					if ( $o == 0 ) {
						$nextTime = $now + $package;
						if ($i < $nextTime) {
							$fffoutput.= "<li class=unavailable>echo $text_unavailable</li>";
							
							$sellerinfo['time_content'] = $text_no_delivery;
							$sellerinfo['time'] = '00:00 pm - 00:00 pm';
							$sellerinfo['store_status'] = 'Close';
						} else {
							$fffnextdelivery = arabicTime(strtotime(date("h:i a", $i)),$languageId);

							$name = strtolower($fffdata["full"]);

							$fffdelivery = $days[$name] . " " . arabicTime(strtotime(date("h:i a", $i)),$languageId)." - ".arabicTime(strtotime(date("h:i a", $nntt)),$languageId);
							
							$sellerinfo['time_content'] = $text_next_delivery.' '.$fffdelivery;							
							$sellerinfo['time'] = $fffdelivery;
							$sellerinfo['store_status'] = 'Close';
			
							$fffoutput.= "<li>".arabicTime(strtotime(date("h:i a", $i)),$languageId)." to ".arabicTime(strtotime(date("h:i a", $nntt)),$languageId)."</li>"; $o++;
						}
					} else {
						$fffoutput.= "<li>".arabicTime(strtotime(date("h:i a", $i)),$languageId)." to ".arabicTime(strtotime(date("h:i a", $nntt)),$languageId)."</li>"; $o++;
					}					
				}
			}
		} else if ( $seller_info[strtolower(substr($ssdata["half"], 0, 3)) . "_start_time"] != "00:00:00" && $seller_info[strtolower(substr($ssdata["half"], 0, 3)) . "_end_time"] != "00:00:00" ) {
			$ssstarttime = strtotime($ssdata["date"]. " " . $seller_info[strtolower(substr($ssdata["half"], 0, 3)) . "_start_time"]) + $interval;
			$ssendtime = strtotime($ssdata["date"]. " " . $seller_info[strtolower(substr($ssdata["half"], 0, 3)) . "_end_time"]);
			$ssoutput = ""; $ssnextdelivery = ""; $ssdelivery = ""; $p = 0;
			for( $i = $ssstarttime; $i < $ssendtime; $i += $interval) {
				$nntt = $i + $interval;
				if( $i < $now ) {
					$ssoutput.= "<li class=unavailable>echo $text_unavailable</li>";
	
					$sellerinfo['time_content'] = $text_no_delivery;
					$sellerinfo['time'] = '00:00 pm - 00:00 pm';
					$sellerinfo['store_status'] = 'Close';
				} else {
					if ( $p == 0 ) {
					$nextTime = $now + $package;
						if ($i < $nextTime) {
							$ssoutput.= "<li class=unavailable>echo $text_unavailable</li>";
							
							$sellerinfo['time_content'] = $text_no_delivery;
							$sellerinfo['time'] = '00:00 pm - 00:00 pm';
							$sellerinfo['store_status'] = 'Close';
						} else {
							$ssnextdelivery = arabicTime(strtotime(date("h:i a", $i)),$languageId);

							$name = strtolower($ssdata["full"]);
							$ssdelivery = $days[$name] . " " . arabicTime(strtotime(date("h:i a", $i)),$languageId)." - ".arabicTime(strtotime(date("h:i a", $nntt)),$languageId);
							
							$sellerinfo['time_content'] = $text_next_delivery.' '.$ssdelivery;											
							$sellerinfo['time'] = $ssdelivery;
							$sellerinfo['store_status'] = 'Close';
							
							$ssoutput.= "<li>".arabicTime(strtotime(date("h:i a", $i)),$languageId)." to ".arabicTime(strtotime(date("h:i a", $nntt)),$languageId)."</li>"; $p++;
						}
					} else {
						$ssoutput.= "<li>".arabicTime(strtotime(date("h:i a", $i)),$languageId)." to ".arabicTime(strtotime(date("h:i a", $nntt)),$languageId)."</li>"; $p++;
					}
				}
			}
		} else {
			$sellerinfo['time_content'] = $text_no_delivery;
			$sellerinfo['time'] = '00:00 pm - 00:00 pm';
			$sellerinfo['store_status'] = 'Close';
		}
print_r($sellerinfo);
exit;
		return $sellerinfo;
		
	}
	
	public function getStoreDeliveryTime($seller_info){
		
		$this->load->language('api/localisation');
		
		$languageId = (isset($this->request->get['language'])) ? $this->request->get['language'] : 1;
		
		if($languageId == 1){							
			$text_today          = $this->language->get('text_today');
			$text_tomorrow       = $this->language->get('text_tomorrow');			
			$text_next_delivery  = $this->language->get('text_next_delivery');
			$text_no_delivery    = $this->language->get('text_no_delivery');
			$text_unavailable    = $this->language->get('text_unavailable');
		} else {			
			$text_today          = $this->language->get('text_today_arabic');
			$text_tomorrow       = $this->language->get('text_tomorrow_arabic');			
			$text_next_delivery  = $this->language->get('text_next_delivery_arabic');
			$text_no_delivery    = $this->language->get('text_no_delivery_arabic');
			$text_unavailable    = $this->language->get('text_unavailable_arabic');
		}
		
		$text_open          = $this->language->get('text_open');
		$text_close          = $this->language->get('text_close');
		
		date_default_timezone_get();
		
		$now = strtotime("now");
		$interval = $seller_info["delivery_timegap"] * 60;
		$package = $seller_info["package_ready"] * 60;
		$nowdata = dayData("now");

		
		$fdata = dayData(" +1 day");			
		$sdata = dayData(" +2 day");
		$tdata = dayData(" +3 day");
		$ffdata = dayData(" +4 day");
		$fffdata = dayData(" +5 day");
		$ssdata = dayData(" +6 day");
			
		if ( $seller_info[strtolower(substr($nowdata["half"], 0, 3)) . "_start_time"] != "00:00:00" && $seller_info[strtolower(substr($nowdata["half"], 0, 3)) . "_end_time"] != "00:00:00" ) {
			
			$nowstarttime = strtotime($nowdata["date"]. " " . $seller_info[strtolower(substr($nowdata["half"], 0, 3)) . "_start_time"]) + $interval;
			
			$nowendtime = strtotime($nowdata["date"]. " " . $seller_info[strtolower(substr($nowdata["half"], 0, 3)) . "_end_time"]);
			
			$nowoutput = ""; $nownextdelivery = ""; $nowdelivery = ""; $j = 0;
			
			for( $i = $nowstarttime; $i < $nowendtime; $i += $interval) {
				$nntt = $i + $interval;
				if( $i < $now ) {
					$nowoutput.= "<li class=unavailable>echo $text_unavailable</li>";
					$sellerinfo['time_content'] = $text_no_delivery;
					$sellerinfo['time'] = '00:00 pm - 00:00 pm';					
				} else {
					if ( $j == 0 ) {
					  $nextTime = $now + $package;
					  if ($i < $nextTime) {
						$nowoutput.= "<li class=unavailable> echo $text_unavailable</li>";
						$sellerinfo['time_content'] = $text_no_delivery;
						$sellerinfo['time'] = '00:00 pm - 00:00 pm';						
					  } else {

						$nownextdelivery = arabicTime(strtotime(date("h:i a", $i)),$languageId);
						
						$nowdelivery = arabicTime(strtotime(date("h:i a", $i)),$languageId)." - ".arabicTime(strtotime(date("h:i a", $nntt)),$languageId);
						
						$sellerinfo['time_content'] = $text_next_delivery.' '.$text_today.' '.$nowdelivery;
						$sellerinfo['time'] = $nowdelivery;
												
						$nowoutput.= "<li>".arabicTime(strtotime(date("h:i a", $i)),$languageId)." to ".arabicTime(strtotime(date("h:i a", $nntt)),$languageId)."</li>";
						$j++;
					  }
					} else {
						$nowoutput.= "<li>".arabicTime(strtotime(date("h:i a", $i)),$languageId)." to ".arabicTime(strtotime(date("h:i a", $nntt)),$languageId)."</li>"; $j++;
					}
				}
			}
			
			if(empty($sellerinfo)){
				$sellerinfo['time_content'] = $text_no_delivery;
				$sellerinfo['time'] = '00:00 pm - 00:00 pm';				
			}
			
			if(($nowstarttime < $now) && ($nowendtime > $now)){
				$sellerinfo['store_status'] = $text_open;			
			}else {
				$sellerinfo['store_status'] = $text_close;
			}	
			
			if($nowdata["full"] !=	date("l")){				

				if ( $seller_info[strtolower(substr($fdata["half"], 0, 3)) . "_start_time"] != "00:00:00" && $seller_info[strtolower(substr($fdata["half"], 0, 3)) . "_end_time"] != "00:00:00" ) {
					
					$fstarttime = strtotime($fdata["date"]. " " . $seller_info[strtolower(substr($fdata["half"], 0, 3)) . "_start_time"]);
					
					$fendtime = strtotime($fdata["date"]. " " . $seller_info[strtolower(substr($fdata["half"], 0, 3)) . "_end_time"]);
					
					$foutput = ""; $fnextdelivery = ""; $fdelivery = ""; $k = 0;
					
					for( $i = $fstarttime; $i < $fendtime; $i += $interval) {
						$nntt = $i + $interval;
						if( $i < $now ) {
							$foutput.= "<li class=unavailable>echo $text_unavailable</li>";
							$sellerinfo['time_content'] = $text_no_delivery;
							$sellerinfo['time'] = '00:00 pm - 00:00 pm';							
						} else {
							if ( $k == 0 ) {
								$nextTime = $now + $package;
								if ($i < $nextTime) {
									$foutput.= "<li class=unavailable>echo $text_unavailable</li>";

									$sellerinfo['time_content'] = $text_no_delivery;
									$sellerinfo['time'] = '00:00 pm - 00:00 pm';								
								} else {
									$fnextdelivery = arabicTime(strtotime(date("h:i a", $i)),$languageId);
									
									$fdelivery = arabicTime(strtotime(date("h:i a", $i)),$languageId)." - ".arabicTime(strtotime(date("h:i a", $nntt)),$languageId);
																	
									$sellerinfo['time_content'] = $text_next_delivery.' '.$text_tomorrow.' '.$fdelivery;
									$sellerinfo['time'] = $fdelivery;									
									
									$foutput.= "<li>".arabicTime(strtotime(date("h:i a", $i)),$languageId)." to ".arabicTime(strtotime(date("h:i a", $nntt)),$languageId)."</li>"; $k++;
								}
							} else {
								$foutput.= "<li>".arabicTime(strtotime(date("h:i a", $i)),$languageId)." to ".arabicTime(strtotime(date("h:i a", $nntt)),$languageId)."</li>"; $k++;
									
							}
							
						}
					}
				}
								
				if(($fstarttime < $now) && ($fendtime > $now)){
					$sellerinfo['store_status'] = $text_open;			
				} else {
					$sellerinfo['store_status'] = $text_close;
				}				
				 
			}			
		} else if ( $seller_info[strtolower(substr($fdata["half"], 0, 3)) . "_start_time"] != "00:00:00" && $seller_info[strtolower(substr($fdata["half"], 0, 3)) . "_end_time"] != "00:00:00" ) {
			$fstarttime = strtotime($fdata["date"]. " " . $seller_info[strtolower(substr($fdata["half"], 0, 3)) . "_start_time"]) + $interval;
			
			$fendtime = strtotime($fdata["date"]. " " . $seller_info[strtolower(substr($fdata["half"], 0, 3)) . "_end_time"]);
			
			$foutput = ""; $fnextdelivery = ""; $fdelivery = ""; $k = 0;
			
			for( $i = $fstarttime; $i < $fendtime; $i += $interval) {
				$nntt = $i + $interval;
				if( $i < $now ) {
					$foutput.= "<li class=unavailable>echo $text_unavailable</li>";
					$sellerinfo['time_content'] = $text_no_delivery;
					$sellerinfo['time'] = '00:00 pm - 00:00 pm';
					$sellerinfo['store_status'] = $text_close;
				} else {
					if ( $k == 0 ) {
						$nextTime = $now + $package;
						if ($i < $nextTime) {
							$foutput.= "<li class=unavailable>echo $text_unavailable</li>";

							$sellerinfo['time_content'] = $text_no_delivery;
							$sellerinfo['time'] = '00:00 pm - 00:00 pm';
							$sellerinfo['store_status'] = $text_close;
						} else {
							$fnextdelivery = arabicTime(strtotime(date("h:i a", $i)),$languageId);
							
							$fdelivery = arabicTime(strtotime(date("h:i a", $i)),$languageId)." - ".arabicTime(strtotime(date("h:i a", $nntt)),$languageId);
															
							$sellerinfo['time_content'] = $text_next_delivery.' '.$text_tomorrow.' '.$fdelivery;
							$sellerinfo['time'] = $fdelivery;
							$sellerinfo['store_status'] = $text_close;
							
							$foutput.= "<li>".arabicTime(strtotime(date("h:i a", $i)),$languageId)." to ".arabicTime(strtotime(date("h:i a", $nntt)),$languageId)."</li>"; $k++;
						}
					} else {
						$foutput.= "<li>".arabicTime(strtotime(date("h:i a", $i)),$languageId)." to ".arabicTime(strtotime(date("h:i a", $nntt)),$languageId)."</li>"; $k++;
							
					}
					
				}
			}
		} else if ( $seller_info[strtolower(substr($sdata["half"], 0, 3)) . "_start_time"] != "00:00:00" && $seller_info[strtolower(substr($sdata["half"], 0, 3)) . "_end_time"] != "00:00:00" ) {
			$sstarttime = strtotime($sdata["date"]. " " . $seller_info[strtolower(substr($sdata["half"], 0, 3)) . "_start_time"]) + $interval;
			
			$sendtime = strtotime($sdata["date"]. " " . $seller_info[strtolower(substr($sdata["half"], 0, 3)) . "_end_time"]);
			
			$soutput = ""; $snextdelivery = ""; $sdelivery = ""; $l = 0;
			for( $i = $sstarttime; $i < $sendtime; $i += $interval) {
				$nntt = $i + $interval;
				if( $i < $now ) {
					$soutput.= "<li class=unavailable>echo $text_unavailable</li>";
					$sellerinfo['time_content'] = $text_no_delivery;
					$sellerinfo['time'] = '00:00 pm - 00:00 pm';
					$sellerinfo['store_status'] = $text_close;
				} else {
					if ( $l == 0 ) {
						$nextTime = $now + $package;
						if ($i < $nextTime) {
							$soutput.= "<li class=unavailable>echo $text_unavailable</li>";
							
							$sellerinfo['time_content'] = $text_no_delivery;
							$sellerinfo['time'] = '00:00 pm - 00:00 pm';
							$sellerinfo['store_status'] = $text_close;
						} else {
							$snextdelivery = arabicTime(strtotime(date("h:i a", $i)),$languageId);
							
							$name = strtolower($sdata["full"]);
							$sdelivery = $days[$name] . " " . arabicTime(strtotime(date("h:i a", $i)),$languageId)." - ".arabicTime(strtotime(date("h:i a", $nntt)),$languageId);
															
							$sellerinfo['time_content'] = $text_next_delivery.' '.$sdelivery;							
							$sellerinfo['time'] = $sdelivery;
							$sellerinfo['store_status'] = $text_close;
							
							$soutput.= "<li>".arabicTime(strtotime(date("h:i a", $i)),$languageId)." to ".arabicTime(strtotime(date("h:i a", $nntt)),$languageId)."</li>"; $l++;
						}
					} else {
						$soutput.= "<li>".arabicTime(strtotime(date("h:i a", $i)),$languageId)." to ".arabicTime(strtotime(date("h:i a", $nntt)),$languageId)."</li>"; $l++;						
					}
				}
			}
		} else if ( $seller_info[strtolower(substr($tdata["half"], 0, 3)) . "_start_time"] != "00:00:00" && $seller_info[strtolower(substr($tdata["half"], 0, 3)) . "_end_time"] != "00:00:00" ) {
			$tstarttime = strtotime($tdata["date"]. " " . $seller_info[strtolower(substr($tdata["half"], 0, 3)) . "_start_time"]) + $interval;
			
			$tendtime = strtotime($tdata["date"]. " " . $seller_info[strtolower(substr($tdata["half"], 0, 3)) . "_end_time"]);
			
			$toutput = ""; $tnextdelivery = ""; $tdelivery = ""; $m = 0;
				
			for( $i = $tstarttime; $i < $tendtime; $i += $interval) {
				$nntt = $i + $interval;
				if( $i < $now ) {
					$toutput.= "<li class=unavailable>echo $text_unavailable</li>";
					
					$sellerinfo['time_content'] = $text_no_delivery;
					$sellerinfo['time'] = '00:00 pm - 00:00 pm';
					$sellerinfo['store_status'] = $text_close;
				} else {
					if ( $m == 0 ) {
						$nextTime = $now + $package;
						if ($i < $nextTime) {
							$toutput.= "<li class=unavailable>echo $text_unavailable</li>";
							
							$sellerinfo['time_content'] = $text_no_delivery;
							$sellerinfo['time'] = '00:00 pm - 00:00 pm';
							$sellerinfo['store_status'] = $text_close;
						} else {
							$tnextdelivery = arabicTime(strtotime(date("h:i a", $i)),$languageId);
							
							$name = strtolower($tdata["full"]);
							
							$tdelivery = $days[$name] . " " . arabicTime(strtotime(date("h:i a", $i)),$languageId)." - ".arabicTime(strtotime(date("h:i a", $nntt)),$languageId);
							
							$sellerinfo['time_content'] = $text_next_delivery.' '.$tdelivery;							
							$sellerinfo['time'] = $tdelivery;
							$sellerinfo['store_status'] = $text_close;
							
							$toutput.= "<li>".arabicTime(strtotime(date("h:i a", $i)),$languageId)." to ".arabicTime(strtotime(date("h:i a", $nntt)),$languageId)."</li>"; $m++;
						}
					} else {
						$toutput.= "<li>".arabicTime(strtotime(date("h:i a", $i)),$languageId)." to ".arabicTime(strtotime(date("h:i a", $nntt)),$languageId)."</li>"; $m++;						
					}
				}
			}
		} else if ( $seller_info[strtolower(substr($ffdata["half"], 0, 3)) . "_start_time"] != "00:00:00" && $seller_info[strtolower(substr($ffdata["half"], 0, 3)) . "_end_time"] != "00:00:00" ) {
			$ffstarttime = strtotime($ffdata["date"]. " " . $seller_info[strtolower(substr($ffdata["half"], 0, 3)) . "_start_time"]) + $interval;
			
			$ffendtime = strtotime($ffdata["date"]. " " . $seller_info[strtolower(substr($ffdata["half"], 0, 3)) . "_end_time"]);
			
			$ffoutput = ""; $ffnextdelivery = ""; $ffdelivery = ""; $n = 0;
			
			for( $i = $ffstarttime; $i < $ffendtime; $i += $interval) {
				$nntt = $i + $interval;
				if( $i < $now ) {
					$foutput.= "<li class=unavailable>echo $text_unavailable</li>";

					$sellerinfo['time_content'] = $text_no_delivery;
					$sellerinfo['time'] = '00:00 pm - 00:00 pm';
					$sellerinfo['store_status'] = $text_close;
				} else {
					if ( $n == 0 ) {
						$nextTime = $now + $package;
						if ($i < $nextTime) {
							$ffoutput.= "<li class=unavailable>echo $text_unavailable</li>";

							$sellerinfo['time_content'] = $text_no_delivery;
							$sellerinfo['time'] = '00:00 pm - 00:00 pm';
							$sellerinfo['store_status'] = $text_close;
						} else {
							$ffnextdelivery = arabicTime(strtotime(date("h:i a", $i)),$languageId);
							$name = strtolower($ffdata["full"]);
							$ffdelivery = $days[$name] . " " . arabicTime(strtotime(date("h:i a", $i)),$languageId)." - ".arabicTime(strtotime(date("h:i a", $nntt)),$languageId);
							
							$sellerinfo['time_content'] = $text_next_delivery.' '.$ffdelivery;							
							$sellerinfo['time'] = $nowdelivery;
							$sellerinfo['store_status'] = $text_close;
			
							$ffoutput.= "<li>".arabicTime(strtotime(date("h:i a", $i)),$languageId)." to ".arabicTime(strtotime(date("h:i a", $nntt)),$languageId)."</li>"; $n++;
						}
					} else {
						$ffoutput.= "<li>".arabicTime(strtotime(date("h:i a", $i)),$languageId)." to ".arabicTime(strtotime(date("h:i a", $nntt)),$languageId)."</li>"; $n++;
					}
				}
			}
		} else if ( $seller_info[strtolower(substr($fffdata["half"], 0, 3)) . "_start_time"] != "00:00:00" && $seller_info[strtolower(substr($fffdata["half"], 0, 3)) . "_end_time"] != "00:00:00" ) {
			$fffstarttime = strtotime($fffdata["date"]. " " . $seller_info[strtolower(substr($fffdata["half"], 0, 3)) . "_start_time"]) + $interval;
			
			$fffendtime = strtotime($fffdata["date"]. " " . $seller_info[strtolower(substr($fffdata["half"], 0, 3)) . "_end_time"]);
			
			$fffoutput = ""; $fffnextdelivery = ""; $fffdelivery = ""; $o = 0;
			
			for( $i = $fffstarttime; $i < $fffendtime; $i += $interval) {
				$nntt = $i + $interval;
				if( $i < $now ) {
					$fffoutput.= "<li class=unavailable>echo $text_unavailable</li>";

					$sellerinfo['time_content'] = $text_no_delivery;
					$sellerinfo['time'] = '00:00 pm - 00:00 pm';
					$sellerinfo['store_status'] = $text_close;
				} else {
					if ( $o == 0 ) {
						$nextTime = $now + $package;
						if ($i < $nextTime) {
							$fffoutput.= "<li class=unavailable>echo $text_unavailable</li>";
							
							$sellerinfo['time_content'] = $text_no_delivery;
							$sellerinfo['time'] = '00:00 pm - 00:00 pm';
							$sellerinfo['store_status'] = $text_close;
						} else {
							$fffnextdelivery = arabicTime(strtotime(date("h:i a", $i)),$languageId);

							$name = strtolower($fffdata["full"]);

							$fffdelivery = $days[$name] . " " . arabicTime(strtotime(date("h:i a", $i)),$languageId)." - ".arabicTime(strtotime(date("h:i a", $nntt)),$languageId);
							
							$sellerinfo['time_content'] = $text_next_delivery.' '.$fffdelivery;							
							$sellerinfo['time'] = $fffdelivery;
							$sellerinfo['store_status'] = $text_close;
			
							$fffoutput.= "<li>".arabicTime(strtotime(date("h:i a", $i)),$languageId)." to ".arabicTime(strtotime(date("h:i a", $nntt)),$languageId)."</li>"; $o++;
						}
					} else {
						$fffoutput.= "<li>".arabicTime(strtotime(date("h:i a", $i)),$languageId)." to ".arabicTime(strtotime(date("h:i a", $nntt)),$languageId)."</li>"; $o++;
					}					
				}
			}
		} else if ( $seller_info[strtolower(substr($ssdata["half"], 0, 3)) . "_start_time"] != "00:00:00" && $seller_info[strtolower(substr($ssdata["half"], 0, 3)) . "_end_time"] != "00:00:00" ) {
			$ssstarttime = strtotime($ssdata["date"]. " " . $seller_info[strtolower(substr($ssdata["half"], 0, 3)) . "_start_time"]) + $interval;
			$ssendtime = strtotime($ssdata["date"]. " " . $seller_info[strtolower(substr($ssdata["half"], 0, 3)) . "_end_time"]);
			$ssoutput = ""; $ssnextdelivery = ""; $ssdelivery = ""; $p = 0;
			for( $i = $ssstarttime; $i < $ssendtime; $i += $interval) {
				$nntt = $i + $interval;
				if( $i < $now ) {
					$ssoutput.= "<li class=unavailable>echo $text_unavailable</li>";
	
					$sellerinfo['time_content'] = $text_no_delivery;
					$sellerinfo['time'] = '00:00 pm - 00:00 pm';
					$sellerinfo['store_status'] = $text_close;
				} else {
					if ( $p == 0 ) {
					$nextTime = $now + $package;
						if ($i < $nextTime) {
							$ssoutput.= "<li class=unavailable>echo $text_unavailable</li>";
							
							$sellerinfo['time_content'] = $text_no_delivery;
							$sellerinfo['time'] = '00:00 pm - 00:00 pm';
							$sellerinfo['store_status'] = $text_close;
						} else {
							$ssnextdelivery = arabicTime(strtotime(date("h:i a", $i)),$languageId);

							$name = strtolower($ssdata["full"]);
							$ssdelivery = $days[$name] . " " . arabicTime(strtotime(date("h:i a", $i)),$languageId)." - ".arabicTime(strtotime(date("h:i a", $nntt)),$languageId);
							
							$sellerinfo['time_content'] = $text_next_delivery.' '.$ssdelivery;											
							$sellerinfo['time'] = $ssdelivery;
							$sellerinfo['store_status'] = $text_close;
							
							$ssoutput.= "<li>".arabicTime(strtotime(date("h:i a", $i)),$languageId)." to ".arabicTime(strtotime(date("h:i a", $nntt)),$languageId)."</li>"; $p++;
						}
					} else {
						$ssoutput.= "<li>".arabicTime(strtotime(date("h:i a", $i)),$languageId)." to ".arabicTime(strtotime(date("h:i a", $nntt)),$languageId)."</li>"; $p++;
					}
				}
			}
		} else {
			$sellerinfo['time_content'] = $text_no_delivery;
			$sellerinfo['time'] = '00:00 pm - 00:00 pm';
			$sellerinfo['store_status'] = $text_close;
		}

		return $sellerinfo;
		
	}
	
	public function getStoreTimeContent($seller_info){
		
		$this->load->language('api/localisation');
		
		$languageId = (isset($this->request->get['language'])) ? $this->request->get['language'] : 1;
		
		if($languageId == 1){							
			$text_today          = $this->language->get('text_today');
			$text_tomorrow       = $this->language->get('text_tomorrow');			
			$text_next_delivery  = $this->language->get('text_next_delivery');
			$text_no_delivery    = $this->language->get('text_no_delivery');
			$text_unavailable    = $this->language->get('text_unavailable');
			$text_open          = $this->language->get('text_open');
			$text_close          = $this->language->get('text_close');
		} else {			
			$text_today          = $this->language->get('text_today_arabic');
			$text_tomorrow       = $this->language->get('text_tomorrow_arabic');			
			$text_next_delivery  = $this->language->get('text_next_delivery_arabic');
			$text_no_delivery    = $this->language->get('text_no_delivery_arabic');
			$text_unavailable    = $this->language->get('text_unavailable_arabic');
			$text_open          = $this->language->get('text_open_arabic');
			$text_close          = $this->language->get('text_close_arabic');
		}
		
		date_default_timezone_get();
		
		$now = strtotime("now");
		$interval = $seller_info["delivery_timegap"] * 60;
		$package = $seller_info["package_ready"] * 60;
		$nowdata = dayData("now");

		
		$fdata = dayData(" +1 day");			
		$sdata = dayData(" +2 day");
		$tdata = dayData(" +3 day");
		$ffdata = dayData(" +4 day");
		$fffdata = dayData(" +5 day");
		$ssdata = dayData(" +6 day");
			
		if ( $seller_info[strtolower(substr($nowdata["half"], 0, 3)) . "_start_time"] != "00:00:00" && $seller_info[strtolower(substr($nowdata["half"], 0, 3)) . "_end_time"] != "00:00:00" ) {
			
			$nowstarttime = strtotime($nowdata["date"]. " " . $seller_info[strtolower(substr($nowdata["half"], 0, 3)) . "_start_time"]) + $interval;
			
			$nowendtime = strtotime($nowdata["date"]. " " . $seller_info[strtolower(substr($nowdata["half"], 0, 3)) . "_end_time"]);
			
			$nowoutput = ""; $nownextdelivery = ""; $nowdelivery = ""; $j = 0;
			
			for( $i = $nowstarttime; $i < $nowendtime; $i += $interval) {
				$nntt = $i + $interval;
				if( $i < $now ) {
					$nowoutput.= "<li class=unavailable>echo $text_unavailable</li>";
					$sellerinfo['time_content'] = $text_no_delivery;
					$sellerinfo['time'] = '00:00 pm - 00:00 pm';					
				} else {
					if ( $j == 0 ) {
					  $nextTime = $now + $package;
					  if ($i < $nextTime) {
						$nowoutput.= "<li class=unavailable> echo $text_unavailable</li>";
						$sellerinfo['time_content'] = $text_no_delivery;
						$sellerinfo['time'] = '00:00 pm - 00:00 pm';						
					  } else {

						$nownextdelivery = arabicTime(strtotime(date("h:i a", $i)),$languageId);
						
						$nowdelivery = arabicTime(strtotime(date("h:i a", $i)),$languageId)." - ".arabicTime(strtotime(date("h:i a", $nntt)),$languageId);
						
						$sellerinfo['time_content'] = $text_next_delivery.' '.$text_today.' '.$nowdelivery;
						$sellerinfo['time'] = $nowdelivery;
												
						$nowoutput.= "<li>".arabicTime(strtotime(date("h:i a", $i)),$languageId)." to ".arabicTime(strtotime(date("h:i a", $nntt)),$languageId)."</li>";
						$j++;
					  }
					} else {
						$nowoutput.= "<li>".arabicTime(strtotime(date("h:i a", $i)),$languageId)." to ".arabicTime(strtotime(date("h:i a", $nntt)),$languageId)."</li>"; $j++;
					}
				}
			}
			
			if(empty($sellerinfo)){
				$sellerinfo['time_content'] = $text_no_delivery;
				$sellerinfo['time'] = '00:00 pm - 00:00 pm';				
			}
			
			if(($nowstarttime < $now) && ($nowendtime > $now)){
				$sellerinfo['store_status'] = $text_open;			
			}else {
				$sellerinfo['store_status'] = $text_close;
			}	
			
			if($nowdata["full"] !=	date("l")){				

				if ( $seller_info[strtolower(substr($fdata["half"], 0, 3)) . "_start_time"] != "00:00:00" && $seller_info[strtolower(substr($fdata["half"], 0, 3)) . "_end_time"] != "00:00:00" ) {
					
					$fstarttime = strtotime($fdata["date"]. " " . $seller_info[strtolower(substr($fdata["half"], 0, 3)) . "_start_time"]);
					
					$fendtime = strtotime($fdata["date"]. " " . $seller_info[strtolower(substr($fdata["half"], 0, 3)) . "_end_time"]);
					
					$foutput = ""; $fnextdelivery = ""; $fdelivery = ""; $k = 0;
					
					for( $i = $fstarttime; $i < $fendtime; $i += $interval) {
						$nntt = $i + $interval;
						if( $i < $now ) {
							$foutput.= "<li class=unavailable>echo $text_unavailable</li>";
							$sellerinfo['time_content'] = $text_no_delivery;
							$sellerinfo['time'] = '00:00 pm - 00:00 pm';							
						} else {
							if ( $k == 0 ) {
								$nextTime = $now + $package;
								if ($i < $nextTime) {
									$foutput.= "<li class=unavailable>echo $text_unavailable</li>";

									$sellerinfo['time_content'] = $text_no_delivery;
									$sellerinfo['time'] = '00:00 pm - 00:00 pm';								
								} else {
									$fnextdelivery = arabicTime(strtotime(date("h:i a", $i)),$languageId);
									
									$fdelivery = arabicTime(strtotime(date("h:i a", $i)),$languageId)." - ".arabicTime(strtotime(date("h:i a", $nntt)),$languageId);
																	
									$sellerinfo['time_content'] = $text_next_delivery.' '.$text_tomorrow.' '.$fdelivery;
									$sellerinfo['time'] = $fdelivery;									
									
									$foutput.= "<li>".arabicTime(strtotime(date("h:i a", $i)),$languageId)." to ".arabicTime(strtotime(date("h:i a", $nntt)),$languageId)."</li>"; $k++;
								}
							} else {
								$foutput.= "<li>".arabicTime(strtotime(date("h:i a", $i)),$languageId)." to ".arabicTime(strtotime(date("h:i a", $nntt)),$languageId)."</li>"; $k++;
									
							}
							
						}
					}
				}
								
				if(($fstarttime < $now) && ($fendtime > $now)){
					$sellerinfo['store_status'] = $text_open;			
				} else {
					$sellerinfo['store_status'] = $text_close;
				}				
			 	 
			}
			
		} else if ( $seller_info[strtolower(substr($fdata["half"], 0, 3)) . "_start_time"] != "00:00:00" && $seller_info[strtolower(substr($fdata["half"], 0, 3)) . "_end_time"] != "00:00:00" ) {
			$fstarttime = strtotime($fdata["date"]. " " . $seller_info[strtolower(substr($fdata["half"], 0, 3)) . "_start_time"]) + $interval;
			
			$fendtime = strtotime($fdata["date"]. " " . $seller_info[strtolower(substr($fdata["half"], 0, 3)) . "_end_time"]);
			
			$foutput = ""; $fnextdelivery = ""; $fdelivery = ""; $k = 0;
			
			for( $i = $fstarttime; $i < $fendtime; $i += $interval) {
				$nntt = $i + $interval;
				if( $i < $now ) {
					$foutput.= "<li class=unavailable>echo $text_unavailable</li>";
					$sellerinfo['time_content'] = $text_no_delivery;					
					$sellerinfo['store_status'] = $text_close;
				} else {
					if ( $k == 0 ) {
						$nextTime = $now + $package;
						if ($i < $nextTime) {
							$foutput.= "<li class=unavailable>echo $text_unavailable</li>";

							$sellerinfo['time_content'] = $text_no_delivery;						
							$sellerinfo['store_status'] = $text_close;
						} else {
							$fnextdelivery = arabicTime(strtotime(date("h:i a", $i)),$languageId);
							
							$fdelivery = arabicTime(strtotime(date("h:i a", $i)),$languageId)." - ".arabicTime(strtotime(date("h:i a", $nntt)),$languageId);
															
							$sellerinfo['time_content'] = $text_next_delivery.' '.$text_tomorrow.' '.$fdelivery;
							$sellerinfo['store_status'] = $text_close;
							
							$foutput.= "<li>".arabicTime(strtotime(date("h:i a", $i)),$languageId)." to ".arabicTime(strtotime(date("h:i a", $nntt)),$languageId)."</li>"; $k++;
						}
					} else {
						$foutput.= "<li>".arabicTime(strtotime(date("h:i a", $i)),$languageId)." to ".arabicTime(strtotime(date("h:i a", $nntt)),$languageId)."</li>"; $k++;
							
					}
					
				}
			}
		} else if ( $seller_info[strtolower(substr($sdata["half"], 0, 3)) . "_start_time"] != "00:00:00" && $seller_info[strtolower(substr($sdata["half"], 0, 3)) . "_end_time"] != "00:00:00" ) {
			$sstarttime = strtotime($sdata["date"]. " " . $seller_info[strtolower(substr($sdata["half"], 0, 3)) . "_start_time"]) + $interval;
			
			$sendtime = strtotime($sdata["date"]. " " . $seller_info[strtolower(substr($sdata["half"], 0, 3)) . "_end_time"]);
			
			$soutput = ""; $snextdelivery = ""; $sdelivery = ""; $l = 0;
			for( $i = $sstarttime; $i < $sendtime; $i += $interval) {
				$nntt = $i + $interval;
				if( $i < $now ) {
					$soutput.= "<li class=unavailable>echo $text_unavailable</li>";
					$sellerinfo['time_content'] = $text_no_delivery;					
					$sellerinfo['store_status'] = $text_close;
				} else {
					if ( $l == 0 ) {
						$nextTime = $now + $package;
						if ($i < $nextTime) {
							$soutput.= "<li class=unavailable>echo $text_unavailable</li>";
							
							$sellerinfo['time_content'] = $text_no_delivery;							
							$sellerinfo['store_status'] = $text_close;
						} else {
							$snextdelivery = arabicTime(strtotime(date("h:i a", $i)),$languageId);
							
							$name = strtolower($sdata["full"]);
							$sdelivery = $days[$name] . " " . arabicTime(strtotime(date("h:i a", $i)),$languageId)." - ".arabicTime(strtotime(date("h:i a", $nntt)),$languageId);
															
							$sellerinfo['time_content'] = $text_next_delivery.' '.$sdelivery;							
							$sellerinfo['store_status'] = $text_close;
							
							$soutput.= "<li>".arabicTime(strtotime(date("h:i a", $i)),$languageId)." to ".arabicTime(strtotime(date("h:i a", $nntt)),$languageId)."</li>"; $l++;
						}
					} else {
						$soutput.= "<li>".arabicTime(strtotime(date("h:i a", $i)),$languageId)." to ".arabicTime(strtotime(date("h:i a", $nntt)),$languageId)."</li>"; $l++;						
					}
				}
			}
		} else if ( $seller_info[strtolower(substr($tdata["half"], 0, 3)) . "_start_time"] != "00:00:00" && $seller_info[strtolower(substr($tdata["half"], 0, 3)) . "_end_time"] != "00:00:00" ) {
			$tstarttime = strtotime($tdata["date"]. " " . $seller_info[strtolower(substr($tdata["half"], 0, 3)) . "_start_time"]) + $interval;
			
			$tendtime = strtotime($tdata["date"]. " " . $seller_info[strtolower(substr($tdata["half"], 0, 3)) . "_end_time"]);
			
			$toutput = ""; $tnextdelivery = ""; $tdelivery = ""; $m = 0;
				
			for( $i = $tstarttime; $i < $tendtime; $i += $interval) {
				$nntt = $i + $interval;
				if( $i < $now ) {
					$toutput.= "<li class=unavailable>echo $text_unavailable</li>";
					
					$sellerinfo['time_content'] = $text_no_delivery;					
					$sellerinfo['store_status'] = $text_close;
				} else {
					if ( $m == 0 ) {
						$nextTime = $now + $package;
						if ($i < $nextTime) {
							$toutput.= "<li class=unavailable>echo $text_unavailable</li>";
							
							$sellerinfo['time_content'] = $text_no_delivery;							
							$sellerinfo['store_status'] = $text_close;
						} else {
							$tnextdelivery = arabicTime(strtotime(date("h:i a", $i)),$languageId);
							
							$name = strtolower($tdata["full"]);
							
							$tdelivery = $days[$name] . " " . arabicTime(strtotime(date("h:i a", $i)),$languageId)." - ".arabicTime(strtotime(date("h:i a", $nntt)),$languageId);
							
							$sellerinfo['time_content'] = $text_next_delivery.' '.$tdelivery;					
							$sellerinfo['store_status'] = $text_close;
							
							$toutput.= "<li>".arabicTime(strtotime(date("h:i a", $i)),$languageId)." to ".arabicTime(strtotime(date("h:i a", $nntt)),$languageId)."</li>"; $m++;
						}
					} else {
						$toutput.= "<li>".arabicTime(strtotime(date("h:i a", $i)),$languageId)." to ".arabicTime(strtotime(date("h:i a", $nntt)),$languageId)."</li>"; $m++;						
					}
				}
			}
		} else if ( $seller_info[strtolower(substr($ffdata["half"], 0, 3)) . "_start_time"] != "00:00:00" && $seller_info[strtolower(substr($ffdata["half"], 0, 3)) . "_end_time"] != "00:00:00" ) {
			$ffstarttime = strtotime($ffdata["date"]. " " . $seller_info[strtolower(substr($ffdata["half"], 0, 3)) . "_start_time"]) + $interval;
			
			$ffendtime = strtotime($ffdata["date"]. " " . $seller_info[strtolower(substr($ffdata["half"], 0, 3)) . "_end_time"]);
			
			$ffoutput = ""; $ffnextdelivery = ""; $ffdelivery = ""; $n = 0;
			
			for( $i = $ffstarttime; $i < $ffendtime; $i += $interval) {
				$nntt = $i + $interval;
				if( $i < $now ) {
					$foutput.= "<li class=unavailable>echo $text_unavailable</li>";

					$sellerinfo['time_content'] = $text_no_delivery;					
					$sellerinfo['store_status'] = $text_close;
				} else {
					if ( $n == 0 ) {
						$nextTime = $now + $package;
						if ($i < $nextTime) {
							$ffoutput.= "<li class=unavailable>echo $text_unavailable</li>";

							$sellerinfo['time_content'] = $text_no_delivery;							
							$sellerinfo['store_status'] = $text_close;
						} else {
							$ffnextdelivery = arabicTime(strtotime(date("h:i a", $i)),$languageId);
							$name = strtolower($ffdata["full"]);
							$ffdelivery = $days[$name] . " " . arabicTime(strtotime(date("h:i a", $i)),$languageId)." - ".arabicTime(strtotime(date("h:i a", $nntt)),$languageId);
							
							$sellerinfo['time_content'] = $text_next_delivery.' '.$ffdelivery;					
							$sellerinfo['store_status'] = $text_close;
			
							$ffoutput.= "<li>".arabicTime(strtotime(date("h:i a", $i)),$languageId)." to ".arabicTime(strtotime(date("h:i a", $nntt)),$languageId)."</li>"; $n++;
						}
					} else {
						$ffoutput.= "<li>".arabicTime(strtotime(date("h:i a", $i)),$languageId)." to ".arabicTime(strtotime(date("h:i a", $nntt)),$languageId)."</li>"; $n++;
					}
				}
			}
		} else if ( $seller_info[strtolower(substr($fffdata["half"], 0, 3)) . "_start_time"] != "00:00:00" && $seller_info[strtolower(substr($fffdata["half"], 0, 3)) . "_end_time"] != "00:00:00" ) {
			$fffstarttime = strtotime($fffdata["date"]. " " . $seller_info[strtolower(substr($fffdata["half"], 0, 3)) . "_start_time"]) + $interval;
			
			$fffendtime = strtotime($fffdata["date"]. " " . $seller_info[strtolower(substr($fffdata["half"], 0, 3)) . "_end_time"]);
			
			$fffoutput = ""; $fffnextdelivery = ""; $fffdelivery = ""; $o = 0;
			
			for( $i = $fffstarttime; $i < $fffendtime; $i += $interval) {
				$nntt = $i + $interval;
				if( $i < $now ) {
					$fffoutput.= "<li class=unavailable>echo $text_unavailable</li>";

					$sellerinfo['time_content'] = $text_no_delivery;
					$sellerinfo['store_status'] = $text_close;
				} else {
					if ( $o == 0 ) {
						$nextTime = $now + $package;
						if ($i < $nextTime) {
							$fffoutput.= "<li class=unavailable>echo $text_unavailable</li>";
							
							$sellerinfo['time_content'] = $text_no_delivery;
							$sellerinfo['store_status'] = $text_close;
						} else {
							$fffnextdelivery = arabicTime(strtotime(date("h:i a", $i)),$languageId);

							$name = strtolower($fffdata["full"]);

							$fffdelivery = $days[$name] . " " . arabicTime(strtotime(date("h:i a", $i)),$languageId)." - ".arabicTime(strtotime(date("h:i a", $nntt)),$languageId);
							
							$sellerinfo['time_content'] = $text_next_delivery.' '.$fffdelivery;	
							$sellerinfo['store_status'] = $text_close;
			
							$fffoutput.= "<li>".arabicTime(strtotime(date("h:i a", $i)),$languageId)." to ".arabicTime(strtotime(date("h:i a", $nntt)),$languageId)."</li>"; $o++;
						}
					} else {
						$fffoutput.= "<li>".arabicTime(strtotime(date("h:i a", $i)),$languageId)." to ".arabicTime(strtotime(date("h:i a", $nntt)),$languageId)."</li>"; $o++;
					}					
				}
			}
		} else if ( $seller_info[strtolower(substr($ssdata["half"], 0, 3)) . "_start_time"] != "00:00:00" && $seller_info[strtolower(substr($ssdata["half"], 0, 3)) . "_end_time"] != "00:00:00" ) {
			$ssstarttime = strtotime($ssdata["date"]. " " . $seller_info[strtolower(substr($ssdata["half"], 0, 3)) . "_start_time"]) + $interval;
			$ssendtime = strtotime($ssdata["date"]. " " . $seller_info[strtolower(substr($ssdata["half"], 0, 3)) . "_end_time"]);
			$ssoutput = ""; $ssnextdelivery = ""; $ssdelivery = ""; $p = 0;
			for( $i = $ssstarttime; $i < $ssendtime; $i += $interval) {
				$nntt = $i + $interval;
				if( $i < $now ) {
					$ssoutput.= "<li class=unavailable>echo $text_unavailable</li>";
	
					$sellerinfo['time_content'] = $text_no_delivery;
					$sellerinfo['store_status'] = $text_close;
				} else {
					if ( $p == 0 ) {
					$nextTime = $now + $package;
						if ($i < $nextTime) {
							$ssoutput.= "<li class=unavailable>echo $text_unavailable</li>";
							
							$sellerinfo['time_content'] = $text_no_delivery;
							$sellerinfo['store_status'] = $text_close;
						} else {
							$ssnextdelivery = arabicTime(strtotime(date("h:i a", $i)),$languageId);

							$name = strtolower($ssdata["full"]);
							$ssdelivery = $days[$name] . " " . arabicTime(strtotime(date("h:i a", $i)),$languageId)." - ".arabicTime(strtotime(date("h:i a", $nntt)),$languageId);
							
							$sellerinfo['time_content'] = $text_next_delivery.' '.$ssdelivery;
							$sellerinfo['store_status'] = $text_close;
							
							$ssoutput.= "<li>".arabicTime(strtotime(date("h:i a", $i)),$languageId)." to ".arabicTime(strtotime(date("h:i a", $nntt)),$languageId)."</li>"; $p++;
						}
					} else {
						$ssoutput.= "<li>".arabicTime(strtotime(date("h:i a", $i)),$languageId)." to ".arabicTime(strtotime(date("h:i a", $nntt)),$languageId)."</li>"; $p++;
					}
				}
			}
		} else {
			$sellerinfo['time_content'] = $text_no_delivery;			
			$sellerinfo['store_status'] = $text_close;
		}

		return $sellerinfo;
		
	}

    //Get Stores By Location
    public function getStoresByCurrentLocation(){

        /* $lat = $this->request->get['lat'];
        $lng = $this->request->get['lng'];
        $radiusKm = $this->request->get['radius'];

        $this->load->model('store/store');
        $this->load->model('tool/image');
        //Get Sellers against lat long
        $stores = $this->model_store_store->getSellerByRadius($radiusKm, $lat, $lng);
        foreach($stores as $key => $value)
        {
			$stores[$key]['storelogo'] = $this->model_tool_image->resize($stores[$key]['image'], 50, 50);
            $stores[$key]['image'] = $this->model_tool_image->resize($stores[$key]['image'], 720, 406);
            $stores[$key]['banner'] = $this->model_tool_image->resize($stores[$key]['banner'], 1498,286);
        }

        if(count($stores) > 0){
            $data = array('msg' => 'success', 'result' => $stores);
        }else{
            $data = array('msg' => 'failed', 'result' => '');
        } */
		
		 $lat = $this->request->get['lat'];
        $lng = $this->request->get['lng'];
        $radiusKm = $this->request->get['radius'];

        $this->load->model('store/store');
        $this->load->model('tool/image');
		$this->load->model('catalog/seller');
        //Get Sellers against lat long
        $stores = $this->model_store_store->getSellerByRadius($radiusKm, $lat, $lng);
		
		foreach($stores as $key => $value)
        {		 
			$storesArray[$key]['seller_id'] = $stores[$key]['seller_id'];
			$storesArray[$key]['firstname'] = $stores[$key]['firstname'];
			$storesArray[$key]['lastname'] = $stores[$key]['lastname'];
			$storesArray[$key]['email'] = $stores[$key]['email'];
			$storesArray[$key]['minimum_order'] = $stores[$key]['minimum_order'];
			$storesArray[$key]['delivery_charges'] = $stores[$key]['delivery_charges'];
			
			$storesArray[$key]['longitude'] = $stores[$key]['longitude'];
			$storesArray[$key]['latitude'] = $stores[$key]['latitude'];			
			$storesArray[$key]['package_ready'] = $stores[$key]['package_ready'];
			$storesArray[$key]['delivery_timegap'] = $stores[$key]['delivery_timegap'];	
			
			$storesArray[$key]['mon_start_time'] = $stores[$key]['mon_start_time'];
			$storesArray[$key]['mon_end_time'] = $stores[$key]['mon_end_time'];			
			$storesArray[$key]['tue_start_time'] = $stores[$key]['tue_start_time'];
			$storesArray[$key]['tue_end_time'] = $stores[$key]['tue_end_time'];
			$storesArray[$key]['wed_start_time'] = $stores[$key]['wed_start_time'];
			$storesArray[$key]['wed_end_time'] = $stores[$key]['wed_end_time'];			
			$storesArray[$key]['thu_start_time'] = $stores[$key]['thu_start_time'];
			$storesArray[$key]['thu_end_time'] = $stores[$key]['thu_end_time'];			
			$storesArray[$key]['fri_start_time'] = $stores[$key]['fri_start_time'];
			$storesArray[$key]['fri_end_time'] = $stores[$key]['fri_end_time'];
			$storesArray[$key]['sat_start_time'] = $stores[$key]['sat_start_time'];
			$storesArray[$key]['sat_end_time'] = $stores[$key]['sat_end_time'];			
			$storesArray[$key]['sun_start_time'] = $stores[$key]['sun_start_time'];
			$storesArray[$key]['sun_end_time'] = $stores[$key]['sun_end_time'];
			
			$storesArray[$key]['storelogo'] = $this->model_tool_image->resize($stores[$key]['image'], 50, 50);
			
            $storesArray[$key]['image'] = $this->model_tool_image->resize($stores[$key]['image'], 720, 406);
            $storesArray[$key]['banner'] = $this->model_tool_image->resize($stores[$key]['banner'], 1498,286);
			
			$timeArray = $this->getStoreDeliveryTime($value);
			
			$storesArray[$key]['time_content'] = $timeArray['time_content'];
			$storesArray[$key]['time'] = $timeArray['time'];
			$storesArray[$key]['store_status'] = $timeArray['store_status'];	

			$storesArray[$key]['rating'] = number_format($this->model_catalog_seller->getSellerRating($stores[$key]['seller_id']),1);
        }
		
		if(count($storesArray) > 0){
            $data = array('msg' => 'success', 'result' => $storesArray);
        }else{
            $data = array('msg' => 'failed', 'result' => '');
        }

        echo json_encode($data);
    }

	//Get Status
    public function getStatus(){
		$data = array();
        $data['config_api_status'] = $this->config->get('config_api_status');		
		$data['config_android_app_url'] = $this->config->get('config_android_app_url');
		$data['config_android_app_version'] = $this->config->get('config_android_app_version');			
		
		echo json_encode($data);
    }


}//Controller End


?>
