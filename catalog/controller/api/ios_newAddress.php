<?php
class ControllerApiiosNewAddress extends Controller {
	// parameter to post
	// firstname
	// lastname
	// address
	// city
	// country_id
	// customerid
	public function add() {		
		$keys = array(
			'firstname',
			'lastname',
			'company',
			'address',
			'address_2',
			'postcode',
			'city',
			'zone_id',
			'country_id',
			'customerid'
		);
		$required = array(
			'firstname',
			'lastname',
			'address',
			'zone_id',
			'country_id',
			'customerid'
		);
		$data = array();
		foreach ($keys as $key) {
			if (!isset($this->request->post[$key])) {
				$data[$key] = '';
			}else{
				$data[$key] = $this->request->post[$key];
			}
		}

		
		if ($data['customerid'] == '' || $data['firstname'] == '' || $data['lastname'] == '' || $data['address'] == '' || $data['zone_id'] == '' || $data['country_id'] == '') {
			
			echo json_encode(array('error' => 'These fields are required', 'list' => $required));
		}else{
			$this->load->model('api/address');
			$address = $this->model_api_address->addAddress($data);
			
			echo json_encode(array('address_id' => $address));
		}		
	}
	
	public function getCustomerCity() {
		
		$this->load->model('api/address');
		
		$output = $this->model_api_address->getCustomerCityIos();
			
		echo json_encode($output,JSON_UNESCAPED_SLASHES);
		exit;
	}
}