<?php
class ControllerApiiosSocialLogin extends Controller {
	public function facebook_login(){
		$data = $this->request->post;
		//if (isset($data['email']) && isset($data['id'])) {
		if (isset($data['email'])) {
			$this->load->model('api/sociallogin');
			$customer_id = $this->model_api_sociallogin->getCustomerByEmail($data['email']);
			if ($customer_id) {
				$this->customer->login($data['email'], '', true);
			}else{
				$data['password'] = $this->password();
		        $customer_id = $this->model_api_sociallogin->addCustomer($data);	
		        $this->load->model('account/customer');
				$this->model_account_customer->deleteLoginAttempts($data['email']);
				$this->customer->login($data['email'], $data['password']);
				unset($this->session->data['guest']);
				$this->load->model('account/activity');

				$activity_data = array(
					'customer_id' => $customer_id,
					'name'        => $data['name']
				);

				$this->model_account_activity->addActivity('register', $activity_data);
			}
	        $result = array('msg' => 'success','customer_id'=>$customer_id);
	    }else{
	    	$result = array('msg' => 'failed', 'result' => 'Invaild Email');
	    }
	    echo json_encode($result);
	}

	private function password($length = 8) {
        $chars = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789';
        $count = strlen($chars);

        for ($i = 0, $result = ''; $i < $length; $i++) {
            $index = rand(0, $count - 1);
            $result .= substr($chars, $index, 1);
        }

        return $result;
    }
    
       public function fbLogin(){
        	$data = $this->request->post;
            if (isset($data['email'])) {
                
               	$this->load->model('account/customer');
               	$customer_info = $this->model_account_customer->getCustomerByEmail($this->request->post['email']);	
                
                if(isset($customer_info) && !empty($customer_info)){
                    $result = array('msg' => 'success', 'customer_info' => $customer_info);
                }else{
                    $result = array('msg' => 'failed', 'result' => 'Invaild Email');
                }
            }
            else{
	    	$result = array('msg' => 'failed', 'result' => 'Invaild Email');
	    }
        
        echo json_encode($result);
    }
}