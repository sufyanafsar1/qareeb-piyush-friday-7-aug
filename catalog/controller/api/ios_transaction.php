<?php
class ControllerApiiosTransaction extends Controller {
	public function getAllTransactions(){
		$customer_id = (int)$this->request->post['customer_id'];
		$this->load->model('api/transaction');
		$results = $this->model_api_transaction->getTransactions($customer_id);
		if (count($results)) {
			$amount = 0;
			foreach ($results as $result) {
				$data['transactions'][] = array(
					'amount'      => $this->currency->format($result['amount'], $this->config->get('config_currency')),
					'description' => $result['description'],
					'date_added'  => date($this->language->get('date_format_short'), strtotime($result['date_added']))
				);
				$amount += $result['amount'];
			}
			$data['total_amount'] = $this->currency->format($amount, $this->config->get('config_currency'));
			$data = array('msg' => 'success', 'result' => $data);
			echo json_encode($data);
		}else{
            $data = array('msg' => 'failed', 'result' => null);
            echo json_encode($data);
        }
	}
}