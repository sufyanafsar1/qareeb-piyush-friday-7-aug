<?php
class ControllerApiNewAddress extends Controller {
	// parameter to post
	// firstname
	// lastname
	// address
	// city
	// country_id
	// customerid
	public function add() {		
		$keys = array(
			'firstname',
			'lastname',
			'company',
			'address',
			'address_2',
			'postcode',
			'region_id',
			'city',
			'zone_id',
			'city_id',
			'country_id',
			'customerid'
		);
		$required = array(
			'firstname',
			'lastname',
			'address',			
			'city_id',
			'country_id',
			'customerid'
		);
		$data = array();
		foreach ($keys as $key) {
			if (!isset($this->request->post[$key])) {
				$data[$key] = '';
			}else{
				$data[$key] = $this->request->post[$key];
			}
		}
		if ($data['customerid'] == '' || $data['firstname'] == '' || $data['lastname'] == '' || $data['address'] == '' || $data['country_id'] == '') {
			
			echo json_encode(array('error' => 'These fields are required', 'list' => $required));
		}else{
			// $data['city_id'] = $this->request->post['city_id'];
			$data['zone_id'] = $this->request->post['city_id'];
			$this->load->model('api/address');
			if(!isset($this->request->post['region_id'])){
				$data['region_id'] ='';
			}
			$address = $this->model_api_address->addAddress($data);
			
			// Add to activity log
			$this->load->model('account/activity');
			
			$activity_data = array(
				'customer_id' => $data['customerid'],
				'name'        => $data['firstname'] . ' ' . $data['lastname']
			);

			$this->model_account_activity->addActivity('address_add', $activity_data);
			
			echo json_encode(array('address_id' => $address));
		}		
	}
	
	public function getCustomerCity() {
		
		$language_id = $this->request->get['language'];
		
		$this->load->model('api/address');
		
		$output = $this->model_api_address->getCustomerCityApi($language_id);
			
		echo json_encode($output,JSON_UNESCAPED_SLASHES);
		exit;
	}
}