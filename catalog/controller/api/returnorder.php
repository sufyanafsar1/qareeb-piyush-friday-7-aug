<?php
class ControllerApiReturnOrder extends Controller {
	public function add() {

		$this->load->model('api/returnorder');
		
		$this->load->model('account/customer');

		$customer_id = (int)$this->request->post['customer_id'];		
		$data['order_id'] = (int)$this->request->post['order_id'];
		$data['product_id'] = (int)$this->request->post['product_id'];
		$data['comment'] = $this->request->post['comment'];
		$data['reason_id'] = (int)$this->request->post['reason_id'];
		$data['date_ordered'] = date('Y-m-d',strtotime($this->request->post['date_ordered']));
		$data['order_quantity'] = $this->request->post['order_quantity'];
		$data['customer'] = $this->model_account_customer->getCustomer($customer_id);
		$this->load->model('catalog/product');
		$products = $this->model_catalog_product->getProduct($data['product_id']);
		$data['product']['name'] = $products['name'];
		$data['product']['model'] = $products['model'];
		// Add to activity log
		$this->load->model('account/activity');

		if (count($data['customer']) && count($products)) {
			$return_id = $this->model_api_returnorder->addReturn($data);
			$activity_data = array(
				'customer_id' => $data['customer']['customer_id'],
				'name'        => $data['customer']['firstname'] . ' ' . $data['customer']['lastname'],
				'return_id'   => $return_id
			);

			$this->model_account_activity->addActivity('return_account', $activity_data);
			$response = array('msg' => 'success', 'result' => 'Return order successfuly submitted.');
		}else{
			$response = array('msg' => 'failed', 'result' => 'No record found against customer.');
		}
		echo json_encode($response);
	}
}
?>