<?php
class ControllerApiTimeSlot extends Controller {
    
    public function getDates() {
        $store_id = $this->get_store_id();
        $res = array("msg"=>"Failed");
        
        $sqlSetting = "SELECT show_slots,delivery_starts,time_format,date_format,time_zone FROM " . DB_PREFIX . "slot_setting WHERE store_id =".$store_id;
       
        $query = $this->db->query($sqlSetting); 
        
        if( $query->num_rows>0){
          
            $limit = $query->row['show_slots'];
            $lead = $query->row['delivery_starts'];
            $time_format = $query->row['time_format'];
            $date_format = $query->row['date_format'];
            $time_zone = $query->row['time_zone'];
        }
        
        if($limit==""){ $limit=1;}
        
        $_SESSION['time_zone'] = $time_zone;
        
        date_default_timezone_set($time_zone);
        
        $added_lead_time = date('Y-m-d H:i:s', strtotime("+$lead hour"));
      
        $data = array();
         
        $sqlSlots = "SELECT * FROM " . DB_PREFIX . "one_year_slot WHERE store_id = ".$store_id." AND from_date_time>='".$added_lead_time."' ORDER BY from_date_time ASC LIMIT $limit";
        
        $q = $this->db->query($sqlSlots);
        
        if($q->num_rows>0){
        
            $start_id = $q->rows[0]['one_slot_id'];
            foreach ($q->rows as $key=>$result) {
                
                if($date_format=="dd-mm-yy"){
                    $d = explode("-",$result['date']);
                    
                    $year = $d[0];
                    $month= $d[1];
                    $dat = $d[2];
                    
                    $ress = $dat."-".$month."-".$year;
                    
                }else{
                    
                    $ress = $result['date'];
                    
                }
                
                $data1 = $result['one_slot_id'];
                $data[] = $ress;
                
            }
            
            if($data!=""){
                $res = array();
                $res['dates'] = array_values(array_unique($data));                    
                $res['ids'] = array($start_id,$data1);   
            }
        }
        print_r(json_encode($res));    
    }

    public function getTime(){
        $store_id = $this->get_store_id();
        $inputdata = $this->request->post;
        
        $sqlSetting = "SELECT show_slots,delivery_starts,time_format,date_format,time_zone FROM " . DB_PREFIX . "slot_setting WHERE store_id=".$store_id;
        
        $query = $this->db->query($sqlSetting); 

        if( $query->num_rows>0){
          
            $time_format = $query->row['time_format'];
            $date_format = $query->row['date_format'];
            $lead = $query->row['delivery_starts'];
            $time_zone = $query->row['time_zone'];
        }    
        
            
        date_default_timezone_set($time_zone); 
            
        $SearchTime = "";
        
        $SearchTime = $inputdata['search_time'];
        
        if($SearchTime){
            if($date_format=='dd-mm-yy'){
                $d = explode("-",$SearchTime);
                $year = $d[2];
                $month= $d[1];
                $dat = $d[0];
                $SearchTime = $year."-".$month."-".$dat;
            }
        }
      
        $start = $inputdata['start_id'];
        
        $last_id = $inputdata['last_id'];
        
        $added_lead_time = date('H:i:s', strtotime("+$lead hour"));
        
        $lead_time = $SearchTime." ".$added_lead_time;
       
        $q = $this->db->query("SELECT * from (SELECT slot_timing,to_date_time,from_date_time, one_slot_id,max_no, (SELECT COUNT(*) FROM " . DB_PREFIX . "delivery_time v WHERE store_id=".$store_id." AND times LIKE p.slot_timing ) countSlot FROM " . DB_PREFIX . "one_year_slot p WHERE store_id=".$store_id." AND to_date_time like '%".$SearchTime."%' AND one_slot_id >= '".$start."' AND Status='ENABLE') a WHERE a.countSlot!=a.max_no ORDER BY a.one_slot_id ASC");
        
        if($q->num_rows>0){
            
            foreach($q->rows as $val):
                if($last_id>=$val['one_slot_id']){
                    $to = explode(" ",$val['to_date_time']);
                    $from = explode(" ",$val['from_date_time']);
                    $slot_timing = $val['slot_timing'];
                    if($from[1]>date('H:i:s')){
                        if($time_format=='24 ( Military/Continental Time )'){    
                            $time_in_12_hour_format_from  = date("H:i:s", strtotime($from[1]));
                            $time_in_12_hour_format_to  = date("H:i:s", strtotime($to[1]));
                        }else if($time_format=="12 ( AM/PM and O clock )"){
                            $time_in_12_hour_format_from  = date("g:i:s A", strtotime($from[1]));
                            $time_in_12_hour_format_to  = date("g:i:s A", strtotime($to[1]));
                        }   
                        $times[] =  $time_in_12_hour_format_from." - ".$time_in_12_hour_format_to; 
                    }else{
                        if($time_format=='24 ( Military/Continental Time )'){       
                            $time_in_12_hour_format_from  = date("H:i:s", strtotime($from[1]));
                            $time_in_12_hour_format_to  = date("H:i:s", strtotime($to[1]));
                        }else if($time_format=="12 ( AM/PM and O clock )"){
                            $time_in_12_hour_format_from  = date("g:i:s A", strtotime($from[1]));
                            $time_in_12_hour_format_to  = date("g:i:s A", strtotime($to[1]));
                        } 
                        $times[] =  $time_in_12_hour_format_from." - ".$time_in_12_hour_format_to; 
                    }
                }
            endforeach;
            print_r(json_encode($times));
        } else{
            $res = array("msg"=>"Failed");
            print_r(json_encode($res));
        }
    }

	public function getDatesAndTime() {
        $store_id = $this->get_store_id();
        
        $sqlSetting = "SELECT show_slots,delivery_starts,time_format,date_format,time_zone FROM " . DB_PREFIX . "slot_setting WHERE store_id =".$store_id;
      
        $query = $this->db->query($sqlSetting); 
        
        if( $query->num_rows>0){
          
            $limit = $query->row['show_slots'];
            $lead = $query->row['delivery_starts'];
            $time_format = $query->row['time_format'];
            $date_format = $query->row['date_format'];
            $time_zone = $query->row['time_zone'];
        }
        
        if($limit==""){ $limit=1;}
        
        $_SESSION['time_zone'] = $time_zone;
        
        date_default_timezone_set($time_zone);
        
        $added_lead_time = date('Y-m-d H:i:s', strtotime("+$lead hour"));
      
        $data = array();
         
        $sqlSlots = "SELECT * FROM " . DB_PREFIX . "one_year_slot WHERE store_id = ".$store_id." AND from_date_time>='".$added_lead_time."' ORDER BY from_date_time ASC LIMIT $limit";
        
        $q = $this->db->query($sqlSlots);
        
        if($q->num_rows>0){
        
            $start_id = $q->rows[0]['one_slot_id'];
      
            foreach ($q->rows as $key=>$result) {
                
                if($date_format=="dd-mm-yy"){
                    
                    $d = explode("-",$result['date']);
                    
                    $year = $d[0];
                    $month= $d[1];
                    $dat = $d[2];
                    
                    $ress = $dat."-".$month."-".$year;
                    
                }else{
                    
                    $ress = $result['date'];
                    
                }
                
                $data1 = $result['one_slot_id'];
                $data[] = $ress;
            }
           
            
            if($data!=""){
                $res = array();
                $res['dates'] = array_values(array_unique($data));                    
                $res['ids'] = array($start_id,$data1);
            }
        }

        $datetime = array();
        foreach ($res['dates'] as $key_date => $value_date) {
            $date = $this->getTimeByDate($value_date,$res['ids'][0],$res['ids'][1]);
            if (isset($date['msg'])) {
                $datetime = $date;
            }else{
                foreach ($date as $key => $value) {
                    $datetime[] = $value_date. ' : ' .$value;
                }
            }
        }
        if (count($datetime)) {
            $datetime = json_encode($datetime);
            $data = array('msg' => 'success', 'result' => $datetime);
            echo json_encode($data);
        }else{
            $data = array('msg' => 'failed', 'result' => null);
            echo json_encode($data);
        }
        
	}
    
    public function getTimeByDate($search_date, $start_id , $last_id){
        $store_id = $this->get_store_id();
        
        $sqlSetting = "SELECT show_slots,delivery_starts,time_format,date_format,time_zone FROM " . DB_PREFIX . "slot_setting WHERE store_id=".$store_id;
        
        $query = $this->db->query($sqlSetting); 

        if( $query->num_rows>0){
          
            $time_format = $query->row['time_format'];
            $date_format = $query->row['date_format'];
            $lead = $query->row['delivery_starts'];
            $time_zone = $query->row['time_zone'];
        }    
        
            
        date_default_timezone_set($time_zone); 
            
        $SearchTime = "";
        
        $SearchTime = $search_date;
        
        if($SearchTime){
            if($date_format=='dd-mm-yy'){
                $d = explode("-",$SearchTime);
                $year = $d[2];
                $month= $d[1];
                $dat = $d[0];
                $SearchTime = $year."-".$month."-".$dat;
            }
        }
      
        $start = $start_id;
        
        $added_lead_time = date('H:i:s', strtotime("+$lead hour"));
        
        $lead_time = $SearchTime." ".$added_lead_time;
       
        $q = $this->db->query("SELECT * from (SELECT slot_timing,to_date_time,from_date_time, one_slot_id,max_no, (SELECT COUNT(*) FROM " . DB_PREFIX . "delivery_time v WHERE store_id=".$store_id." AND times LIKE p.slot_timing ) countSlot FROM " . DB_PREFIX . "one_year_slot p WHERE store_id=".$store_id." AND to_date_time like '%".$SearchTime."%' AND one_slot_id >= '".$start."' AND Status='ENABLE') a WHERE a.countSlot!=a.max_no ORDER BY a.one_slot_id ASC");
        
        if($q->num_rows>0){
            
            foreach($q->rows as $val):
                if($last_id >= $val['one_slot_id']){
                    $to = explode(" ",$val['to_date_time']);
                    $from = explode(" ",$val['from_date_time']);
                    $slot_timing = $val['slot_timing'];
                    if($from[1]>date('H:i:s')){
                        if($time_format=='24 ( Military/Continental Time )'){    
                            $time_in_12_hour_format_from  = date("H:i:s", strtotime($from[1]));
                            $time_in_12_hour_format_to  = date("H:i:s", strtotime($to[1]));
                        }else if($time_format=="12 ( AM/PM and O clock )"){
                            $time_in_12_hour_format_from  = date("g:i:s A", strtotime($from[1]));
                            $time_in_12_hour_format_to  = date("g:i:s A", strtotime($to[1]));
                        }   
                        $times[] =  $time_in_12_hour_format_from." - ".$time_in_12_hour_format_to; 
                    }else{
                        if($time_format=='24 ( Military/Continental Time )'){       
                            $time_in_12_hour_format_from  = date("H:i:s", strtotime($from[1]));
                            $time_in_12_hour_format_to  = date("H:i:s", strtotime($to[1]));
                        }else if($time_format=="12 ( AM/PM and O clock )"){
                            $time_in_12_hour_format_from  = date("g:i:s A", strtotime($from[1]));
                            $time_in_12_hour_format_to  = date("g:i:s A", strtotime($to[1]));
                        } 
                        $times[] =  $time_in_12_hour_format_from." - ".$time_in_12_hour_format_to; 
                    }
                }
            endforeach;
            // print_r(json_encode($times));
            return $times;
        } else{
            $res = array("msg"=>"Failed");
            return $res;
            // print_r(json_encode($res));
        }
    }
    
    public function validateTimeSlot() {
        $store_id = $this->get_store_id();
        $inputdata = $this->request->post;        
        
        $time_slot_date =  $inputdata['delivery_date'];
        $time_slot_time =  $inputdata['delivery_time'];
        $expl = explode("-",$time_slot_time);
        $time = $time_slot_date." ".$expl[0]." - ".$time_slot_date." ".$expl[1];
        if(isset($time) && $time!==""):
            $this->load->model('slot/timing');
            $result = $this->model_slot_timing->validateTimeSlot($time,$store_id); 
            if($result == "Success")
            {
                $res = array("msg"=> $result, "dt" => $time); 
            }
            else{
                $res = array("msg"=> $result); 
            }
            
              print_r(json_encode($res));
        endif;
    }
    
    public function get_store_id()
    {
        $store_id = '';
        $this->load->model('setting/store');
        $storedata = $this->model_setting_store->getStores();
        foreach ($storedata as $key => $value) {
            if (strpos( $url, 'https' ) !== false) {
                $stores = str_replace('https://','',$value['url']);
            }else{
                $stores = str_replace('http://','',$value['url']);
            }
            $storewwww=str_replace('www.','',$stores);
            $store[$value['store_id']]=str_replace('.qareeb.com','',$storewwww);
        }
        if (isset($_COOKIE['tracking_city'])) {
            if (in_array($_COOKIE['tracking_city'], $store)) {
                $store_id = array_search($_COOKIE['tracking_city'],$store);
            }else{
                $store_id = 0;
            }
        }else{
            $url= $_SERVER[''];
            foreach ($store as $key => $value) {
                if (strpos( $url, $value ) !== false) {
                    $store_id = $key;
                    break;
                }
            }
            if ($store_id == '') {
                $store_id = 0;
            }
        }
        return $store_id;
    }
    
    
    public function getStoreDeliverySlots() {

        $seller = (int)$this->request->get['seller'];
        $this->load->model('slot/timing');

        //Get Delivery Slots
        $timeSlots = $this->model_slot_timing->getDeliverySlots($seller);
        if(isset($timeSlots) && !empty($timeSlots)){
            
            $dayWiseTimeSlot =  array("Monday" => $timeSlots['mon_start_time'].' - '.$timeSlots['mon_end_time'] ,"Tuesday" => $timeSlots['tue_start_time'].' - '.$timeSlots['tue_end_time'],"Wednesday" => $timeSlots['wed_start_time'].' - '.$timeSlots['wed_end_time'],"Thursday" => $timeSlots['thu_start_time'].' - '.$timeSlots['thu_end_time'],"Friday" => $timeSlots['fri_start_time'].' - '.$timeSlots['fri_end_time'],"Saturday" => $timeSlots['sat_start_time'].' - '.$timeSlots['sat_end_time'],"Sunday" => $timeSlots['sun_start_time'].' - '.$timeSlots['sun_end_time']);
            foreach ($dayWiseTimeSlot as $key => $value) {
                    $datetime[] = $key. ' : ' .$value;
                }
            
            $result =  array('msg' => 'Success', 'result' => $datetime);
        }else{
            $result =  array('msg' => 'Failed', 'result' => '');
        }

        echo json_encode($result);

    }
}
?>