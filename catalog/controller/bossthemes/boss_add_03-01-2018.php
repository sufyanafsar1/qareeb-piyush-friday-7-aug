<?php 
class ControllerBossthemesBossAdd extends Controller {
	public function cart() {
		
		$this->load->language('checkout/cart');
		$this->load->language('bossthemes/boss_add');


		$json = array();

		if (isset($this->request->post['product_id'])) {
			$product_id = (int)$this->request->post['product_id'];
		} else {
			$product_id = 0;
		}
 	      if (isset($this->request->post['seller_id'])) {
			$seller_id = (int)$this->request->post['seller_id'];
		} else {
			$seller_id = 1;
		}

		$this->load->model('catalog/product');


		//$product_info = $this->model_catalog_product->getProduct($product_id,0);

            $product_info = $this->model_catalog_product->getProductBySellerId($product_id,$seller_id);

		if ($product_info) {
			if (isset($this->request->post['quantity']) && ((int)$this->request->post['quantity'] >= $product_info['minimum'])) {
				$quantity = (int)$this->request->post['quantity'];
			} else {
				$quantity = 1;
			}
			
			if (isset($this->request->post['option'])) {
				$option = array_filter($this->request->post['option']);
			} else {
				$option = array();
			}

			$product_options = $this->model_catalog_product->getProductOptions($this->request->post['product_id']);

			foreach ($product_options as $product_option) {
				if ($product_option['required'] && empty($option[$product_option['product_option_id']])) {
					$json['error']['option'][$product_option['product_option_id']] = sprintf($this->language->get('error_required'), $product_option['name']);
				}
			}

			if (isset($this->request->post['recurring_id'])) {
				$recurring_id = $this->request->post['recurring_id'];
			} else {
				$recurring_id = 0;
			}

			$recurrings = $this->model_catalog_product->getProfiles($product_info['product_id']);

			if ($recurrings) {
				$recurring_ids = array();

				foreach ($recurrings as $recurring) {
					$recurring_ids[] = $recurring['recurring_id'];
				}

				if (!in_array($recurring_id, $recurring_ids)) {
					$json['error']['recurring'] = $this->language->get('error_recurring_required');
				}
			}

			if (!$json) {
			   
				$this->cart->add($this->request->post['product_id'], $quantity, $option, $recurring_id);
				
				$this->load->model('tool/image'); 
				//$image = $this->model_tool_image->resize($product_info['image'], $this->config->get('config_image_cart_width'), $this->config->get('config_image_cart_height'));
				
				$image = $this->model_tool_image->resize($product_info['image'],80,80);
        
				$json['title'] = $this->language->get('text_title_cart');
				$json['thumb'] = sprintf($this->language->get('text_thumb'), $image);
               
				$json['success'] = sprintf($this->language->get('text_success_cart'), $this->url->link('product/product', 'product_id=' . $this->request->post['product_id']), $product_info['name'], $this->url->link('checkout/cart'));

				unset($this->session->data['shipping_method']);
				unset($this->session->data['shipping_methods']);
				unset($this->session->data['payment_method']);
				unset($this->session->data['payment_methods']);

				// Totals
				$this->load->model('extension/extension');
				$json['next_day_delivery_active'] = $product_info['next_day_delivery_active'];

				$total_data = array();
				$total = 0;
				$taxes = $this->cart->getTaxes();

				// Display prices
				if (($this->config->get('config_customer_price') && $this->customer->isLogged()) || !$this->config->get('config_customer_price')) {
					$sort_order = array();

					$results = $this->model_extension_extension->getExtensions('total');

					foreach ($results as $key => $value) {
						$sort_order[$key] = $this->config->get($value['code'] . '_sort_order');
					}

					array_multisort($sort_order, SORT_ASC, $results);

				/**
                 * 	foreach ($results as $result) {
                 * 						if ($this->config->get($result['code'] . '_status')) {
                 * 							$this->load->model('total/' . $result['code']);
                
                 * 							//$this->{'model_total_' . $result['code']}->getTotal($total_data, $total, $taxes);
                 *                             
                 * 						}
                 * 					}
                 */
                 
            foreach ($results as $result) {
			   if($result['code']=="sub_total" || $result['code']=="coupon")
               {               
              
    				if ($this->config->get($result['code'] . '_status')) {
    					$this->load->model('total/' . $result['code']);
    			
                        $this->{'model_total_' . $result['code']}->getTotalV2($total_data, $total, $taxes,$seller_id);
    				}
                }
			}

					$sort_order = array();

					foreach ($total_data as $key => $value) {
						$sort_order[$key] = $value['sort_order'];
					}

					array_multisort($sort_order, SORT_ASC, $total_data);
				}
			
                $json['cartId']  ="";
                foreach ($this->cart->getProducts() as $product) {
                    if($product['product_id'] == $this->request->post['product_id'])
                    {
                        $json['cartId']  = $product['cart_id'];
                        break;
                    }
                    
                }
                $json['lang'] = $_SESSION['default']['language'];
				$json['total'] = sprintf($this->language->get('text_items'), $this->cart->countProducts() + (isset($this->session->data['vouchers']) ? count($this->session->data['vouchers']) : 0), $this->currency->format($total));
                 // $json['items'] = $this->language->get('text_items');
                //  $json['total'] =  $this->cart->countProducts() + (isset($this->session->data['vouchers']) ? count($this->session->data['vouchers']) : 0);
         			} else {
				$json['redirect'] = str_replace('&amp;', '&', $this->url->link('product/product', 'product_id=' . $this->request->post['product_id']));
			}
		}

		$this->response->addHeader('Content-Type: application/json');
		$this->response->setOutput(json_encode($json));
	}
    
    public function checkWishlistOnLogin(){       
       
       // Get customer wishlist
     $this->load->model('account/wishlist');
	 $wishlistItems =   $this->model_account_wishlist->getWishlist();
    
    /**
 *  foreach($wishlistItems as $witem){  // set database items 
 *         
 *         $customerWishlist[] = $witem['product_id'];
 *       
 *      }
 */
   	
   	$existInList = true;
    
    
  

     foreach($this->session->data['wishlist'] as $item)
     {        
        //if (!in_array($item[0], $customerWishlist)) // if item not save in database but exists in session 
        //{            
         //  $this->model_account_wishlist->addWishlistCategory($item[0],$item[1]); //save item in database
        //}
        
        $totalItems = (int)$this->model_account_wishlist->getWishlistByProductAndSeller($item[0],$item[2]);
        
        if($totalItems == 0)
        {
             $this->model_account_wishlist->addWishlistCategory($item[0],$item[1],$item[2]); //save item in database
        }
     }
     //if item exists in database but not in session
     foreach($wishlistItems as $witem)
     {   
     	$existInList = true;
     	foreach ($this->session->data['wishlist'] as $key => $value) {
			if ($value[0] == $witem['product_id'] && $value[2] == $witem['seller_id'] ) {
				$existInList = false;
			}
		}
        if ($existInList) // if item not save in session but exists in database 
        {            
           $this->session->data['wishlist'][] = array($witem['product_id'],$witem['category_name'],$witem['seller_id']);
        }        
     }
     
     
     
     
     //If wishlist is empty in session theme   remove the items from database
    // if (count($this->session->data['wishlist']) <= 0) {
//       
//            foreach($customerWishlist as $cWishlist)
//            {
//                $this->model_account_wishlist->deleteWishlist($cWishlist);
//            }
//		}
      
  }		
	
	public function wishlist() {
	  
      
		$this->load->language('account/wishlist');
		$this->load->language('bossthemes/boss_add');

		$json = array();

		if (!isset($this->session->data['wishlist'])) {
			$this->session->data['wishlist'] = array();
		}

		if (isset($this->request->post['product_id'])) {
			$product_id = $this->request->post['product_id'];
		} else {
			$product_id = 0;
		}

		$this->load->model('catalog/product');

		$product_info = $this->model_catalog_product->getProduct($product_id);

		if ($product_info) {
		
			$json['title'] = $this->language->get('text_title_wishlist');
			
			if (!in_array($this->request->post['product_id'], $this->session->data['wishlist'])) {
				$this->session->data['wishlist'][] = (int)$this->request->post['product_id'];
				
				$this->load->model('tool/image'); 
				$image = $this->model_tool_image->resize($product_info['image'], $this->config->get('config_image_cart_width'), $this->config->get('config_image_cart_height'));
				
				$json['thumb'] = sprintf($this->language->get('text_thumb'), $image);

				if ($this->customer->isLogged()) {
				// Edit customers cart
				$this->load->model('account/wishlist');
				$this->model_account_wishlist->addWishlist($this->request->post['product_id']);
				$json['success'] = sprintf($this->language->get('text_success'), $this->url->link('product/product', 'product_id=' . (int)$this->request->post['product_id']), $product_info['name'], $this->url->link('account/wishlist'));
				$json['total'] = sprintf($this->language->get('text_wishlist'), $this->model_account_wishlist->getTotalWishlist());
			} else {
				if (!isset($this->session->data['wishlist'])) {
					$this->session->data['wishlist'] = array();
				}
				$this->session->data['wishlist'][] = $this->request->post['product_id'];
				$this->session->data['wishlist'] = array_unique($this->session->data['wishlist']);
				$json['success'] = sprintf($this->language->get('text_login'), $this->url->link('account/login', '', true), $this->url->link('account/register', '', true), $this->url->link('product/product', 'product_id=' . (int)$this->request->post['product_id']), $product_info['name'], $this->url->link('account/wishlist'));
				$json['total'] = sprintf($this->language->get('text_wishlist'), (isset($this->session->data['wishlist']) ? count($this->session->data['wishlist']) : 0));
			}
			} else {
				$json['info'] = sprintf($this->language->get('text_exists'), $this->url->link('product/product', 'product_id=' . (int)$this->request->post['product_id']), $product_info['name'], $this->url->link('account/wishlist'));
			}

			$json['total'] = sprintf($this->language->get('text_wishlist'), (isset($this->session->data['wishlist']) ? count($this->session->data['wishlist']) : 0));
		}

		$this->response->addHeader('Content-Type: application/json');
		$this->response->setOutput(json_encode($json));
	}

	public function wishlist_cat() {
	  
     
		$this->load->language('account/wishlist');
		$this->load->language('bossthemes/boss_add');

		$json = array();

		if (!isset($this->session->data['wishlist'])) {
			$this->session->data['wishlist'] = array();
		}

		if (isset($this->request->post['wishlist_product_id'])) {
			$product_id = $this->request->post['wishlist_product_id'];
		} else {
			$product_id = 0;
		}
        if (isset($this->request->post['wishlist_seller_id'])) {
			$seller_id = $this->request->post['wishlist_seller_id'];
		} else {
			$seller_id = 1;
		}

		$this->load->model('catalog/product');
		 $languageid =1;
		 if($_SESSION['default']['language'] == 'ar'){
		    $languageid = 3; 
		 }
		$product_info = $this->model_catalog_product->getProductes($product_id,$languageid);

		if ($product_info) {
		
			$json['title'] = $this->language->get('text_title_wishlist');
			$existInList = true;
			foreach ($this->session->data['wishlist'] as $key => $value) {
				if ($value[0] == $this->request->post['wishlist_product_id']  &&  $value[2] == $this->request->post['wishlist_seller_id']) {
					$existInList = false;
				}
			}
            
			if ($existInList) {
				if ($this->request->post['wishlist_catergory'] == 'custom') {
					$cat_name = $this->request->post['wishlist_new_category'] != ''? $this->request->post['wishlist_new_category'] : 'Main';
				}else{
					$cat_name = $this->request->post['wishlist_catergory'] != ''? $this->request->post['wishlist_catergory'] : 'Main';
				}
				
				
				$this->load->model('tool/image'); 
				$image = $this->model_tool_image->resize($product_info['image'], $this->config->get('config_image_cart_width'), $this->config->get('config_image_cart_height'));
				
				$json['thumb'] = sprintf($this->language->get('text_thumb'), $image);
				

				if ($this->customer->isLogged()) {
					// Edit customers cart
                 
					$this->load->model('account/wishlist');
					$this->model_account_wishlist->addWishlistCategory($this->request->post['wishlist_product_id'], $cat_name,$this->request->post['wishlist_seller_id']);
					$this->session->data['wishlist'][] = array($product_id,$cat_name,$seller_id);
                    $json['success'] = sprintf($this->language->get('text_success'), $this->url->link('product/product', 'product_id=' . $product_id), $product_info['name'], $this->url->link('account/wishlist'));
					$json['total'] = sprintf($this->language->get('text_wishlist'), $this->model_account_wishlist->getTotalWishlist());
				} else {
				    $this->session->data['wishlist'][] = array($product_id,$cat_name,$seller_id);
					$json['success'] = sprintf($this->language->get('text_login'), $this->url->link('account/login', '', true), $this->url->link('account/register', '', true), $this->url->link('product/product', 'product_id=' . $product_id), $product_info['name'], $this->url->link('account/wishlist'));
					$json['total'] = sprintf($this->language->get('text_wishlist'), (isset($this->session->data['wishlist']) ? count($this->session->data['wishlist']) : 0));
				}
			} else {
				$json['info'] = sprintf($this->language->get('text_exists'), $this->url->link('product/product', 'product_id=' . $product_id), $product_info['name'], $this->url->link('account/wishlist'));
			}

			$json['total'] = sprintf($this->language->get('text_wishlist'), (isset($this->session->data['wishlist']) ? count($this->session->data['wishlist']) : 0));
		}

		$this->response->addHeader('Content-Type: application/json');
		$this->response->setOutput(json_encode($json));
	}
	
	public function compare() {
		$this->load->language('product/compare');
		
		$this->load->language('bossthemes/boss_add');

		$json = array();

		if (!isset($this->session->data['compare'])) {
			$this->session->data['compare'] = array();
		}

		if (isset($this->request->post['product_id'])) {
			$product_id = $this->request->post['product_id'];
		} else {
			$product_id = 0;
		}

		$this->load->model('catalog/product');

		$product_info = $this->model_catalog_product->getProduct($product_id);

		if ($product_info) {
			if (!in_array($this->request->post['product_id'], $this->session->data['compare'])) {
				if (count($this->session->data['compare']) >= 4) {
					array_shift($this->session->data['compare']);
				}

				$this->session->data['compare'][] = $this->request->post['product_id'];
			}
			
			$this->load->model('tool/image'); 
			$image = $this->model_tool_image->resize($product_info['image'], $this->config->get('config_image_cart_width'), $this->config->get('config_image_cart_height'));
      
			$json['title'] = $this->language->get('text_title_compare');
			$json['thumb'] = sprintf($this->language->get('text_thumb'), $image);

			$json['success'] = sprintf($this->language->get('text_success_compare'), $this->url->link('product/product', 'product_id=' . $this->request->post['product_id']), $product_info['name'], $this->url->link('product/compare'));

			$json['total'] = sprintf($this->language->get('text_compare'), (isset($this->session->data['compare']) ? count($this->session->data['compare']) : 0));
		}

		$this->response->addHeader('Content-Type: application/json');
		$this->response->setOutput(json_encode($json));
	}
}
?>