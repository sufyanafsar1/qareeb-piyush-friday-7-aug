<?php
class ControllerCheckoutConfirmapiNew extends Controller
{		
	public function getOrderNextDeliverySlot($seller_id, $delivery_time){
		        
        $this->load->model('store/store');
        $this->load->model('tool/image');
		$this->load->model('catalog/seller');

		$sqlAll = "SELECT s.seller_id, s.package_ready, s.delivery_timegap, s.mon_start_time, s.mon_end_time, s.tue_start_time, s.tue_end_time, s.wed_start_time, s.wed_end_time, s.thu_start_time, s.thu_end_time, s.fri_start_time, s.fri_end_time, s.sat_start_time, s.sat_end_time, s.sun_start_time, s.sun_end_time, s.sortorder FROM " . DB_PREFIX . "sellers s WHERE s.seller_id = '".$seller_id."'";
		
		$queryAll = $this->db->query($sqlAll);			
		$seller_info = $queryAll->row;
		
		$timeArray = $this->getStoreDeliveryTimeNew($seller_info, $delivery_time);
			
		return $timeArray['time_content'];		
    }
	
	public function getStoreDeliveryTimeNew($seller_info, $delivery_time){
		
		$this->load->language('api/localisation');
		
		$languageId = (isset($this->request->get['language'])) ? $this->request->get['language'] : 1;
		
		if($languageId == 1){							
			$text_today          = $this->language->get('text_today');
			$text_tomorrow       = $this->language->get('text_tomorrow');			
			$text_next_delivery  = $this->language->get('text_next_delivery');
			$text_no_delivery    = $this->language->get('text_no_delivery');
			$text_unavailable    = $this->language->get('text_unavailable');
		} else {			
			$text_today          = $this->language->get('text_today_arabic');
			$text_tomorrow       = $this->language->get('text_tomorrow_arabic');			
			$text_next_delivery  = $this->language->get('text_next_delivery_arabic');
			$text_no_delivery    = $this->language->get('text_no_delivery_arabic');
			$text_unavailable    = $this->language->get('text_unavailable_arabic');
		}
		
		$text_open          = $this->language->get('text_open');
		$text_close          = $this->language->get('text_close');
		
		date_default_timezone_get();
		
		$interval = $seller_info["delivery_timegap"] * 60;
		$package = $seller_info["package_ready"] * 60;
				
		$times = explode(" - ",$delivery_time);		
		
		$now = strtotime($times[0]);	
		$full = date('l', strtotime($times[0]));
		$half = date('D', strtotime($times[0]));
		$time = date('h:i a',strtotime($times[0]));		
		$short_time = date('g:i:s',strtotime($times[0]));
		$strtime = strtotime($times[0]);
		$date = date('Y-m-d',strtotime($times[0]));
		$dateDb = date('d-m-Y',strtotime($times[0]));
			
		$nowdata = array(
			'full'       => $full,
			'half'   => $half,
			'time'   => $time,
			'short_time' => $short_time,
			'strtime'    => $strtime,
			'date' => $date			
		);	
		
		$fdata = dayData(" +1 day");			
		$sdata = dayData(" +2 day");
		$tdata = dayData(" +3 day");
		$ffdata = dayData(" +4 day");
		$fffdata = dayData(" +5 day");
		$ssdata = dayData(" +6 day");
		
		if($nowdata["full"] ==	date("l")){	
			if ( $seller_info[strtolower(substr($nowdata["half"], 0, 3)) . "_start_time"] != "00:00:00" && $seller_info[strtolower(substr($nowdata["half"], 0, 3)) . "_end_time"] != "00:00:00" ) {
				
				$nowstarttime = strtotime($nowdata["date"]. " " . $seller_info[strtolower(substr($nowdata["half"], 0, 3)) . "_start_time"]) + $interval;
				
				$nowendtime = strtotime($nowdata["date"]. " " . $seller_info[strtolower(substr($nowdata["half"], 0, 3)) . "_end_time"]);
				
				$nowoutput = ""; $nownextdelivery = ""; $nowdelivery = ""; $j = 0;
				
				for( $i = $nowstarttime; $i < $nowendtime; $i += $interval) {
					$nntt = $i + $interval;
					if( $i < $now ) {									
					} else {
						if ( $j == 0 ) {
							$nextTime = $now + $package;
							if ($i < $nextTime) {											
							} else {

								$nowdelivery = arabicTime(strtotime(date("h:i:s a", $i)),$languageId)." - ".arabicTime(strtotime(date("h:i:s a", $nntt)),$languageId);
								
								//$sellerinfo['time_content'] = $dateDb.' '.$text_next_delivery.' '.$text_today.' '.$nowdelivery;
								
								$sellerinfo['time_content'] = $dateDb.' '.date("H:i:s", $i)."  - ".$dateDb.'  '.date("H:i:s", $nntt);
											
								$j++;
							}
						} 
					}
				}
			}
		}
		
		if(empty($sellerinfo)){
			// +1 Day
			if ( $seller_info[strtolower(substr($fdata["half"], 0, 3)) . "_start_time"] != "00:00:00" && $seller_info[strtolower(substr($fdata["half"], 0, 3)) . "_end_time"] != "00:00:00" ) {
				
				$fstarttime = strtotime($fdata["date"]. " " . $seller_info[strtolower(substr($fdata["half"], 0, 3)) . "_start_time"]);
				
				$fendtime = strtotime($fdata["date"]. " " . $seller_info[strtolower(substr($fdata["half"], 0, 3)) . "_end_time"]);
				
				$foutput = ""; $fnextdelivery = ""; $fdelivery = ""; $k = 0;
				
				for( $i = $fstarttime; $i < $fendtime; $i += $interval) {
					$nntt = $i + $interval;
					if( $i < $now ) {												
					} else {
						if ( $k == 0 ) {
							$nextTime = $now + $package;
							if ($i < $nextTime) {															
							} else {
								
								$fdelivery = arabicTime(strtotime(date("h:i:s a", $i)),$languageId)." - ".arabicTime(strtotime(date("h:i:s a", $nntt)),$languageId);
																
								//$sellerinfo['time_content'] = $dateDb.' '.$text_next_delivery.' +1 day '.$text_tomorrow.' '.$fdelivery;
								
								$sellerinfo['time_content'] = $dateDb.' '.date("H:i:s", $i)."  - ".$dateDb.'  '.date("H:i:s", $nntt);
								
								$k++;
							}
						} 
					}
				}
			}
		}
					
		if(empty($sellerinfo)){	
			// +2 Day			
			if ( $seller_info[strtolower(substr($sdata["half"], 0, 3)) . "_start_time"] != "00:00:00" && $seller_info[strtolower(substr($sdata["half"], 0, 3)) . "_end_time"] != "00:00:00" ) {
				$sstarttime = strtotime($sdata["date"]. " " . $seller_info[strtolower(substr($sdata["half"], 0, 3)) . "_start_time"]) + $interval;
				
				$sendtime = strtotime($sdata["date"]. " " . $seller_info[strtolower(substr($sdata["half"], 0, 3)) . "_end_time"]);
				
				$soutput = ""; $snextdelivery = ""; $sdelivery = ""; $l = 0;
				for( $i = $sstarttime; $i < $sendtime; $i += $interval) {
					$nntt = $i + $interval;
					if( $i < $now ) {						
					} else {
						if ( $l == 0 ) {
							$nextTime = $now + $package;
							if ($i < $nextTime) {								
							} else {
							
								$name = strtolower($sdata["full"]);
								$sdelivery = $days[$name] . " " .$dateDb.' '.arabicTime(strtotime(date("h:i:s a", $i)),$languageId)." - ".$dateDb.' '.arabicTime(strtotime(date("h:i:s a", $nntt)),$languageId);
																
								//$sellerinfo['time_content'] = $dateDb.' '.$text_next_delivery.' +2 day '.$sdelivery;		
								
								$sellerinfo['time_content'] = $dateDb.' '.date("H:i:s", $i)." - ".$dateDb.' '.date("H:i:s", $nntt);
								
								$l++;
							}
						} 
					}
				}
			}
		}
		
		if(empty($sellerinfo)){	
			// +3 Day				
			if ( $seller_info[strtolower(substr($tdata["half"], 0, 3)) . "_start_time"] != "00:00:00" && $seller_info[strtolower(substr($tdata["half"], 0, 3)) . "_end_time"] != "00:00:00" ) {
				$tstarttime = strtotime($tdata["date"]. " " . $seller_info[strtolower(substr($tdata["half"], 0, 3)) . "_start_time"]) + $interval;
				
				$tendtime = strtotime($tdata["date"]. " " . $seller_info[strtolower(substr($tdata["half"], 0, 3)) . "_end_time"]);
				
				$toutput = ""; $tnextdelivery = ""; $tdelivery = ""; $m = 0;
					
				for( $i = $tstarttime; $i < $tendtime; $i += $interval) {
					$nntt = $i + $interval;
					if( $i < $now ) {						
					} else {
						if ( $m == 0 ) {
							$nextTime = $now + $package;
							if ($i < $nextTime) {								
							} else {
								
								$name = strtolower($tdata["full"]);
								
								$tdelivery = $days[$name] . " " . arabicTime(strtotime(date("h:i:s a", $i)),$languageId)." - ".arabicTime(strtotime(date("h:i:s a", $nntt)),$languageId);
								
								//$sellerinfo['time_content'] = $dateDb.' '.$text_next_delivery.' +3 day '.$tdelivery;
								
								$sellerinfo['time_content'] = $dateDb.' '.date("H:i:s", $i)."  - ".$dateDb.'  '.date("H:i:s", $nntt);
								$m++;
							}
						} 
					}
				}
			} 
		}
		
		if(empty($sellerinfo)){	
			// +4 Day		
			if ( $seller_info[strtolower(substr($ffdata["half"], 0, 3)) . "_start_time"] != "00:00:00" && $seller_info[strtolower(substr($ffdata["half"], 0, 3)) . "_end_time"] != "00:00:00" ) {
				$ffstarttime = strtotime($ffdata["date"]. " " . $seller_info[strtolower(substr($ffdata["half"], 0, 3)) . "_start_time"]) + $interval;
				
				$ffendtime = strtotime($ffdata["date"]. " " . $seller_info[strtolower(substr($ffdata["half"], 0, 3)) . "_end_time"]);
				
				$ffoutput = ""; $ffnextdelivery = ""; $ffdelivery = ""; $n = 0;
				
				for( $i = $ffstarttime; $i < $ffendtime; $i += $interval) {
					$nntt = $i + $interval;
					if( $i < $now ) {						
					} else {
						if ( $n == 0 ) {
							$nextTime = $now + $package;
							if ($i < $nextTime) {								
							} else {
								
								$name = strtolower($ffdata["full"]);
								$ffdelivery = $days[$name] . " " . arabicTime(strtotime(date("h:i:s a", $i)),$languageId)." - ".arabicTime(strtotime(date("h:i:s a", $nntt)),$languageId);
								
								//$sellerinfo['time_content'] = $dateDb.' '.$text_next_delivery.' +4 day '.$ffdelivery;	
								
								$sellerinfo['time_content'] = $dateDb.' '.date("H:i:s", $i)."  - ".$dateDb.'  '.date("H:i:s", $nntt);
								$n++;
							}
						}
					}
				}
			} 
		}
		
		if(empty($sellerinfo)){	
			// +5 Day	
			if ( $seller_info[strtolower(substr($fffdata["half"], 0, 3)) . "_start_time"] != "00:00:00" && $seller_info[strtolower(substr($fffdata["half"], 0, 3)) . "_end_time"] != "00:00:00" ) {
				$fffstarttime = strtotime($fffdata["date"]. " " . $seller_info[strtolower(substr($fffdata["half"], 0, 3)) . "_start_time"]) + $interval;
				
				$fffendtime = strtotime($fffdata["date"]. " " . $seller_info[strtolower(substr($fffdata["half"], 0, 3)) . "_end_time"]);
				
				$fffoutput = ""; $fffnextdelivery = ""; $fffdelivery = ""; $o = 0;
				
				for( $i = $fffstarttime; $i < $fffendtime; $i += $interval) {
					$nntt = $i + $interval;
					if( $i < $now ) {						
					} else {
						if ( $o == 0 ) {
							$nextTime = $now + $package;
							if ($i < $nextTime) {								
							} else {
								
								$name = strtolower($fffdata["full"]);

								$fffdelivery = $days[$name] . " " . arabicTime(strtotime(date("h:i:s a", $i)),$languageId)." - ".arabicTime(strtotime(date("h:i:s a", $nntt)),$languageId);
								
								//$sellerinfo['time_content'] = $dateDb.' '.$text_next_delivery.' +5 day '.$fffdelivery;	
								
								$sellerinfo['time_content'] = $dateDb.' '.date("H:i:s", $i)."  - ".$dateDb.'  '.date("H:i:s", $nntt);
								
								$o++;
							}
						}					
					}
				}
			} 
		}
		
		if(empty($sellerinfo)){	
			// +6 Day
			if ( $seller_info[strtolower(substr($ssdata["half"], 0, 3)) . "_start_time"] != "00:00:00" && $seller_info[strtolower(substr($ssdata["half"], 0, 3)) . "_end_time"] != "00:00:00" ) {
				$ssstarttime = strtotime($ssdata["date"]. " " . $seller_info[strtolower(substr($ssdata["half"], 0, 3)) . "_start_time"]) + $interval;
				$ssendtime = strtotime($ssdata["date"]. " " . $seller_info[strtolower(substr($ssdata["half"], 0, 3)) . "_end_time"]);
				$ssoutput = ""; $ssnextdelivery = ""; $ssdelivery = ""; $p = 0;
				for( $i = $ssstarttime; $i < $ssendtime; $i += $interval) {
					$nntt = $i + $interval;
					if( $i < $now ) {						
					} else {
						if ( $p == 0 ) {
						$nextTime = $now + $package;
							if ($i < $nextTime) {								
							} else {
								
								$name = strtolower($ssdata["full"]);
								$ssdelivery = $days[$name] . " " . arabicTime(strtotime(date("h:i:s a", $i)),$languageId)." - ".arabicTime(strtotime(date("h:i:s a", $nntt)),$languageId);
								
								//$sellerinfo['time_content'] = $dateDb.' '.$text_next_delivery.' +6 day '.$ssdelivery;	
								
								$sellerinfo['time_content'] = $dateDb.' '.date("H:i:s", $i)."  - ".$dateDb.'  '.date("H:i:s", $nntt);
								$p++;
							}
						} 
					}
				}
			}
		}
			
		return $sellerinfo;
		
	}
	
    public function confirmOrderV3New()
    {
        $this->load->language('total/coupon');
        $couponMsg = $this->language->get('error_coupon');
		
		/* --------------------------------------- */
		$this->load->model('store/store');   

		//City_id		
		$city_id = 0;		
		if(isset($this->request->get['city_id'])){
			$city_id = $this->request->get['city_id'];
		}

		$allProducts = $this->cart->getMobileProductsNew($this->request->post['customer_id'],$this->request->post['language_id'],'',$city_id);

		foreach($allProducts as $key1 => $value1) {
			$total = 0;
			$storeId = $key1;			
			$sellerInfo = $this->model_store_store->getSellerByID($storeId);

			$deliveryCharges = (isset($sellerInfo) && $sellerInfo['delivery_charges']) ? (int)$sellerInfo['delivery_charges'] : 0;			
						
			foreach($value1 as $key => $value)
			{				
				$total = $total + $value['total'];
			}
		 
			$allProductsNew[] = array(
				'seller_id'       => $key1,
				'seller_nameen'   => $sellerInfo['firstname'],
				'seller_namear'   => $sellerInfo['lastname'],
				'delivery_charges' => $deliveryCharges,
				'minimum_order'    => (int)$sellerInfo['minimum_order'],
				'seller_total' => $total,
				'store_credit'     => $this->currency->format($amount, $this->config->get('config_currency')),
				'products'         => $value1				
			);			
		}	
		
		$order_data = array();

        $order_data['totals'] = array();
        $total = 0;
		
		//$cart_info = $this->db->query("SELECT * FROM " . DB_PREFIX . "cart WHERE customer_id = '" . $this->request->post['customer_id'] . "'");
		
		$balance = $this->db->query("SELECT SUM(amount) AS total FROM " . DB_PREFIX . "customer_transaction WHERE customer_id = '" . $this->request->post['customer_id'] . "'");
		
		foreach ($balance->rows as $balance_total) {
            $credit_total = $balance_total['total'];            
        }
		
		$isCoupon = true;
		//if (count($cart_info->rows) > 0) {
		if (count($allProductsNew) > 0) {
			
			$voucher = 0;
			
			if (isset($this->request->post['coupon']) && $this->request->post['coupon'] != 'undefined' && $this->request->post['coupon'] != 'null') {

				$this->load->model('total/coupon');

				$coupon = $this->request->post['coupon'];

				$customer = (int) $this->request->post['customer_id'];
				$device = $this->request->post['device_id'];
				$language = 1;

				$coupon_info = $this->model_total_coupon->getMobileCoupon($coupon, $customer, $language, $device);

				$isCoupon = ($coupon_info) ? true : false;

				$voucher_info = $this->model_total_coupon->getMobileTotal($coupon, $customer, $language, $device,$allProductsNew[0]['seller_id']);
				$couponId = $coupon_info['coupon_id'];

				if ($voucher_info) {
					$voucher = $voucher_info[0]['value'];
				}
			} 
			

			
			if ($isCoupon) {

				foreach($allProductsNew as $cart_infoNew) {
					
					$i = 0;
					$total = 0;					
					
					//$seller_id = (isset($this->request->post['seller_id'])) ? $this->request->post['seller_id'] : 1;
					$seller_id = $cart_infoNew['seller_id'];
					 
					$this->load->model('catalog/product');

					$this->load->language('checkout/checkout');

					$order_data['invoice_prefix'] = $this->config->get('config_invoice_prefix');
					$order_data['store_id'] = $this->config->get('config_store_id');
					$order_data['store_name'] = $this->config->get('config_name') . " - Mobile App";
					$order_data['seller_id'] = $seller_id;

					if ($order_data['store_id']) {
						$order_data['store_url'] = $this->config->get('config_url');
					} else {
						$order_data['store_url'] = HTTP_SERVER;
					}

					$this->load->model('account/customer');

					$customer_info = $this->model_account_customer->getCustomer($this->request->post['customer_id']); //******************************************8

					$address_info = $this->db->query("SELECT DISTINCT * FROM " . DB_PREFIX . "address WHERE address_id = '" . (int) $customer_info['address_id'] . "' AND customer_id = '" . (int) $customer_info['customer_id'] . "'");

					$this->load->model('localisation/zone');
					$this->load->model('localisation/country');

					$zone_info = $this->model_localisation_zone->getZone($address_info->row['zone_id']);
					$country_info = $this->model_localisation_country->getCountry($address_info->row['country_id']);

					$order_data['customer_id'] = $this->request->post['customer_id']; //******************************************8
					$order_data['customer_group_id'] = $customer_info['customer_group_id'];
					$order_data['firstname'] = $customer_info['firstname'];
					$order_data['lastname'] = $customer_info['lastname'];
					$order_data['email'] = $customer_info['email'];
					$order_data['telephone'] = $customer_info['telephone'];
					$order_data['fax'] = $customer_info['fax'];
					$order_data['custom_field'] = unserialize($customer_info['custom_field']);

					if (isset($zone_info['name'])) {
						$payment_zone = $zone_info['name'];
					} else {
						$payment_zone = '';
					}

					$order_data['payment_firstname'] = $customer_info['firstname'];
					$order_data['payment_lastname'] = $customer_info['lastname'];
					$order_data['payment_company'] = $address_info->row['company'];
					$order_data['payment_address_1'] = $address_info->row['address_1'];
					$order_data['payment_address_2'] = $address_info->row['address_2'];
					$order_data['payment_city'] = $address_info->row['city'];
					$order_data['payment_postcode'] = $address_info->row['postcode'];
					$order_data['payment_zone'] = $payment_zone;
					$order_data['payment_zone_id'] = $address_info->row['zone_id'];
					$order_data['payment_country'] = $country_info['name'];
					$order_data['payment_country_id'] = $address_info->row['country_id'];
					$order_data['payment_address_format'] = '';
					$order_data['payment_custom_field'] = '';
					$order_data['payment_method'] = $this->request->post['payment_method'];
					$order_data['payment_code'] = $this->request->post['payment_code'];

					$zoneId = getZoneId($this->request->post['input-shipping-zone']);
					$zone_info = $this->model_localisation_zone->getZone($zoneId);
					$country_info = $this->model_localisation_country->getCountry($this->request->post['input-shipping-country']);

					if (isset($zone_info['name'])) {
						$shipping_zone = $zone_info['name'];
					} else {
						$shipping_zone = '';
					}

					$order_data['shipping_firstname'] = $this->request->post['input-shipping-firstname'];
					$order_data['shipping_lastname'] = $this->request->post['input-shipping-lastname'];
					$order_data['shipping_company'] = $this->request->post['input-shipping-company'];
					$order_data['shipping_address_1'] = $this->request->post['input-shipping-address-1'];
					$order_data['shipping_address_2'] = $this->request->post['input-shipping-address-2'];
					$order_data['shipping_city'] = $this->request->post['input-shipping-city'];
					$order_data['shipping_postcode'] = $this->request->post['input-shipping-postcode'];
					$order_data['shipping_zone'] = $shipping_zone;
					$order_data['shipping_zone_id'] = $zoneId;
					$order_data['shipping_country'] = $country_info['name'];
					$order_data['shipping_country_id'] = $this->request->post['input-shipping-country'];
					$order_data['shipping_address_format'] = "";
					$order_data['shipping_custom_field'] = array();
					//$order_data['shipping_method']         = $shipping_method;
					$order_data['shipping_method'] = '';
					$order_data['shipping_code'] = $this->request->post['shipping_code'];

					$order_data['products'] = array();

					if (isset($_GET['input-default-address'])) {
						$this->db->query("UPDATE " . DB_PREFIX . "address SET firstname = '" . $this->db->escape($this->request->post['input-shipping-firstname']) . "', lastname = '" . $this->db->escape($this->request->post['input-shipping-lastname']) . "', company = '" . $this->db->escape($this->request->post['input-shipping-company']) . "', address_1 = '" . $this->db->escape($this->request->post['input-shipping-address-1']) . "', address_2 = '" . $this->db->escape($this->request->post['input-shipping-address-2']) . "', postcode = '" . $this->db->escape($this->request->post['input-shipping-postcode']) . "', city = '" . $this->db->escape($this->request->post['input-shipping-city']) . "', zone_id = '" . (int) $zoneId . "', country_id = '" . (int) $this->request->post['input-shipping-country'] . "' WHERE address_id  = '" . (int) $this->request->post['address_id'] . "' AND customer_id = '" . (int) $this->request->post['customer_id'] . "'");
					}

					$i = 0;
					//foreach ($cart_info->rows as $cart_data) {
					foreach ($cart_infoNew['products'] as $cart_data) {

						$prod_info = $this->model_catalog_product->getProduct($cart_data['product_id'], $seller_id);

						$option_price = 0;
						$option_points = 0;
						$option_weight = 0;

						$option_data = array();

						foreach (json_decode($cart_data['option']) as $product_option_id => $value){
							
							$option_query = $this->db->query("SELECT po.product_option_id, po.option_id, od.name, o.type FROM " . DB_PREFIX . "product_option po LEFT JOIN `" . DB_PREFIX . "option` o ON (po.option_id = o.option_id) LEFT JOIN " . DB_PREFIX . "option_description od ON (o.option_id = od.option_id) WHERE po.product_option_id = '" . (int)$product_option_id . "' AND po.product_id = '" . (int)$cart_data['product_id'] . "' AND od.language_id = '" . (int)$this->config->get('config_language_id') . "'");

							if ($option_query->num_rows) {
								
								if ($option_query->row['type'] == 'select' || $option_query->row['type'] == 'radio' || $option_query->row['type'] == 'image') {
									
									$option_value_query = $this->db->query("SELECT pov.option_value_id, ovd.name, pov.quantity, pov.subtract, pov.price, pov.price_prefix, pov.points, pov.points_prefix, pov.weight, pov.weight_prefix FROM " . DB_PREFIX . "product_option_value pov LEFT JOIN " . DB_PREFIX . "option_value ov ON (pov.option_value_id = ov.option_value_id) LEFT JOIN " . DB_PREFIX . "option_value_description ovd ON (ov.option_value_id = ovd.option_value_id) WHERE pov.product_option_value_id = '" . (int)$value . "' AND pov.product_option_id = '" . (int)$product_option_id . "' AND pov.seller_id = '" . (int)$seller_id . "' AND ovd.language_id = '" . (int)$this->config->get('config_language_id') . "'");

									if ($option_value_query->num_rows) {
										if ($option_value_query->row['price_prefix'] == '+') {
											$option_price += $option_value_query->row['price'];
										} elseif ($option_value_query->row['price_prefix'] == '-') {
											$option_price -= $option_value_query->row['price'];
										}

										if ($option_value_query->row['points_prefix'] == '+') {
											$option_points += $option_value_query->row['points'];
										} elseif ($option_value_query->row['points_prefix'] == '-') {
											$option_points -= $option_value_query->row['points'];
										}

										if ($option_value_query->row['weight_prefix'] == '+') {
											$option_weight += $option_value_query->row['weight'];
										} elseif ($option_value_query->row['weight_prefix'] == '-') {
											$option_weight -= $option_value_query->row['weight'];
										}

										if ($option_value_query->row['subtract'] && (!$option_value_query->row['quantity'] || ($option_value_query->row['quantity'] < $cart_data['quantity']))) {
											$stock = false;
										}

										$option_data[] = array(
											'product_option_id'       => $product_option_id,
											'product_option_value_id' => $value,
											'option_id'               => $option_query->row['option_id'],
											'option_value_id'         => $option_value_query->row['option_value_id'],
											'name'                    => $option_query->row['name'],
											'value'                   => $option_value_query->row['name'],
											'type'                    => $option_query->row['type'],
											'quantity'                => $option_value_query->row['quantity'],
											'subtract'                => $option_value_query->row['subtract'],
											'price'                   => $option_value_query->row['price'],
											'price_prefix'            => $option_value_query->row['price_prefix'],
											'points'                  => $option_value_query->row['points'],
											'points_prefix'           => $option_value_query->row['points_prefix'],
											'weight'                  => $option_value_query->row['weight'],
											'weight_prefix'           => $option_value_query->row['weight_prefix']
										);
									}
								} elseif ($option_query->row['type'] == 'checkbox' && is_array($value)) {
									foreach ($value as $product_option_value_id) {
										$option_value_query = $this->db->query("SELECT pov.option_value_id, ovd.name, pov.quantity, pov.subtract, pov.price, pov.price_prefix, pov.points, pov.points_prefix, pov.weight, pov.weight_prefix FROM " . DB_PREFIX . "product_option_value pov LEFT JOIN " . DB_PREFIX . "option_value ov ON (pov.option_value_id = ov.option_value_id) LEFT JOIN " . DB_PREFIX . "option_value_description ovd ON (ov.option_value_id = ovd.option_value_id) WHERE pov.product_option_value_id = '" . (int)$product_option_value_id . "' AND pov.product_option_id = '" . (int)$product_option_id . "' AND ovd.language_id = '" . (int)$this->config->get('config_language_id') . "'");

										if ($option_value_query->num_rows) {
											if ($option_value_query->row['price_prefix'] == '+') {
												$option_price += $option_value_query->row['price'];
											} elseif ($option_value_query->row['price_prefix'] == '-') {
												$option_price -= $option_value_query->row['price'];
											}

											if ($option_value_query->row['points_prefix'] == '+') {
												$option_points += $option_value_query->row['points'];
											} elseif ($option_value_query->row['points_prefix'] == '-') {
												$option_points -= $option_value_query->row['points'];
											}

											if ($option_value_query->row['weight_prefix'] == '+') {
												$option_weight += $option_value_query->row['weight'];
											} elseif ($option_value_query->row['weight_prefix'] == '-') {
												$option_weight -= $option_value_query->row['weight'];
											}

											if ($option_value_query->row['subtract'] && (!$option_value_query->row['quantity'] || ($option_value_query->row['quantity'] < $cart_data['quantity']))) {
												$stock = false;
											}

											$option_data[] = array(
												'product_option_id'       => $product_option_id,
												'product_option_value_id' => $product_option_value_id,
												'option_id'               => $option_query->row['option_id'],
												'option_value_id'         => $option_value_query->row['option_value_id'],
												'name'                    => $option_query->row['name'],
												'value'                   => $option_value_query->row['name'],
												'type'                    => $option_query->row['type'],
												'quantity'                => $option_value_query->row['quantity'],
												'subtract'                => $option_value_query->row['subtract'],
												'price'                   => $option_value_query->row['price'],
												'price_prefix'            => $option_value_query->row['price_prefix'],
												'points'                  => $option_value_query->row['points'],
												'points_prefix'           => $option_value_query->row['points_prefix'],
												'weight'                  => $option_value_query->row['weight'],
												'weight_prefix'           => $option_value_query->row['weight_prefix']
											);
										}
									}
								} elseif ($option_query->row['type'] == 'text' || $option_query->row['type'] == 'textarea' || $option_query->row['type'] == 'file' || $option_query->row['type'] == 'date' || $option_query->row['type'] == 'datetime' || $option_query->row['type'] == 'time') {
									$option_data[] = array(
										'product_option_id'       => $product_option_id,
										'product_option_value_id' => '',
										'option_id'               => $option_query->row['option_id'],
										'option_value_id'         => '',
										'name'                    => $option_query->row['name'],
										'value'                   => $value,
										'type'                    => $option_query->row['type'],
										'quantity'                => '',
										'subtract'                => '',
										'price'                   => '',
										'price_prefix'            => '',
										'points'                  => '',
										'points_prefix'           => '',
										'weight'                  => '',
										'weight_prefix'           => ''
									);
								}
							}
						}

						if ($prod_info['special'] == null) {
							$total = ($total + (($prod_info['price'] + $option_price) * $cart_data['quantity']));
						} else {
							$total = ($total + (($prod_info['special'] + $option_price) * $cart_data['quantity']));
						}


						if ($prod_info['special'] == null) {
							$order_data['products'][] = array(
								'product_id' => $prod_info['product_id'],
								'name' => $prod_info['name'],
								'model' => $prod_info['model'],
								'option' => $option_data,
								'download' => array(),
								'quantity' => $cart_data['quantity'],
								'subtract' => $prod_info['subtract'],
								'price'    => ($prod_info['price'] + $option_price),
								'total'   => ($prod_info['price'] + $option_price) * $cart_data['quantity'],
								'tax' => 0,
								'reward' => 0,
								'seller_id' => $seller_id,
							);
							$i++;
						} else {
							$order_data['products'][] = array(
								'product_id' => $prod_info['product_id'],
								'name' => $prod_info['name'],
								'model' => $prod_info['model'],
								'option' => $option_data,
								'download' => array(),
								'quantity' => $cart_data['quantity'],
								'subtract' => $prod_info['subtract'],                           
								'price'    => ($prod_info['special'] + $option_price),
								'total'   => ($prod_info['special'] + $option_price) * $cart_data['quantity'],
								'tax' => 0,
								'reward' => 0,
								'seller_id' => $seller_id,
							);
							$i++;
						}
					}
										
					// Gift Voucher
					$order_data['vouchers'] = array();

					if (!empty($this->session->data['vouchers'])) {
						foreach ($this->session->data['vouchers'] as $voucher) {
							$order_data['vouchers'][] = array(
								'description' => $voucher['description'],
								'code' => substr(md5(mt_rand()), 0, 10),
								'to_name' => $voucher['to_name'],
								'to_email' => $voucher['to_email'],
								'from_name' => $voucher['from_name'],
								'from_email' => $voucher['from_email'],
								'voucher_theme_id' => $voucher['voucher_theme_id'],
								'message' => $voucher['message'],
								'amount' => $voucher['amount'],
							);
						}
					}

					$deliveryCharges = 0;
					$deliveryCharges = $cart_infoNew['delivery_charges'];
					
					$deliveryChargesArr = array();
					
					$deliveryChargesArr[] = array(
						'delivery_charges' => $deliveryCharges,
						'seller_id' => $seller_id,
						'firstname' => $cart_infoNew['seller_nameen'],
						'lastname' => $cart_infoNew['seller_namear']
					);
					
					$this->session->data['deliveryChargesArr'] = '';
					$this->session->data['deliveryChargesArr'] = $deliveryChargesArr;

					$order_data['comment'] = $this->request->post['order_comments'];
					$order_data['total'] = $total;

					if (isset($this->request->cookie['tracking'])) {
						$order_data['tracking'] = $this->request->cookie['tracking'];

						$subtotal = $this->cart->getSubTotal();

						// Affiliate
						$this->load->model('affiliate/affiliate');

						$affiliate_info = $this->model_affiliate_affiliate->getAffiliateByCode($this->request->cookie['tracking']);

						if ($affiliate_info) {
							$order_data['affiliate_id'] = $affiliate_info['affiliate_id'];
							$order_data['commission'] = ($subtotal / 100) * $affiliate_info['commission'];
						} else {
							$order_data['affiliate_id'] = 0;
							$order_data['commission'] = 0;
						}

						// Marketing
						$this->load->model('checkout/marketing');

						$marketing_info = $this->model_checkout_marketing->getMarketingByCode($this->request->cookie['tracking']);

						if ($marketing_info) {
							$order_data['marketing_id'] = $marketing_info['marketing_id'];
						} else {
							$order_data['marketing_id'] = 0;
						}
					} else {
						$order_data['affiliate_id'] = 0;
						$order_data['commission'] = 0;
						$order_data['marketing_id'] = 0;
						$order_data['tracking'] = '';
					}

					$order_data['language_id'] = $this->config->get('config_language_id');
					$order_data['currency_id'] = $this->currency->getId();
					$order_data['currency_code'] = $this->currency->getCode();
					$order_data['currency_value'] = $this->currency->getValue($this->currency->getCode());
					$order_data['ip'] = $this->request->server['REMOTE_ADDR'];

					if (!empty($this->request->server['HTTP_X_FORWARDED_FOR'])) {
						$order_data['forwarded_ip'] = $this->request->server['HTTP_X_FORWARDED_FOR'];
					} elseif (!empty($this->request->server['HTTP_CLIENT_IP'])) {
						$order_data['forwarded_ip'] = $this->request->server['HTTP_CLIENT_IP'];
					} else {
						$order_data['forwarded_ip'] = '';
					}

					if (isset($this->request->server['HTTP_USER_AGENT'])) {
						$order_data['user_agent'] = $this->request->server['HTTP_USER_AGENT'];
					} else {
						$order_data['user_agent'] = '';
					}

					if (isset($this->request->server['HTTP_ACCEPT_LANGUAGE'])) {
						$order_data['accept_language'] = $this->request->server['HTTP_ACCEPT_LANGUAGE'];
					} else {
						$order_data['accept_language'] = '';
					}

					$total_data[0] = array(
						'code' => 'sub_total',
						'title' => 'Sub-Total',
						'text' => 'Sub-Total',
						'value' => $total,
						'sort_order' => 0,
					);

					$total_data[1] = array(
						'code' => 'vat',
						'title' => $this->request->post['vat_text'],
						'value' => $this->request->post['vat_value'],
						'sort_order' => 1,
					);
					
					
					$isCouponRequired = false;
					if ($voucher != '0') {
						$isCouponRequired = true;
						//$total = $voucher_info[1];
						
						$total -= $voucher;

						$total_data[3] = array(
							'code' => 'coupon',
							'title' => $voucher_info[0]['title'],
							'value' => -$voucher,
							'sort_order' => 3,
						);
					}

					$this->load->model('checkout/order');
					if ($this->config->get('credit_status')) {
						$this->load->language('total/credit');

						if ((float) $credit_total) {
							if ($credit_total > $total) {
								$credit = $total;
							} else {
								$credit = $credit_total;
							}

							// if ($credit > 0) {
							$total -= $credit;
							$total_data[4] = array(
								'code' => 'credit',
								'title' => 'Store Credit',
								'value' => -$credit,
								'sort_order' => 4,
							);

							//}

							if ($credit_total > $credit) {
								$credit_total = $credit_total - $credit;
							} else {
								$credit_total = $credit - $credit_total;
							}
						}

					}

					$total_data[5] = array(
						'code' => 'total',
						'title' => 'Total',
						'value' => $total + $deliveryCharges,
						'sort_order' => 5,
					);

					$order_data['totals'] = $total_data;
					//debugging($order_data);

					/* echo "<pre>";
					print_r($order_data);
					exit; */
					
					//coupon blank 
					$voucher = 0;
					
					$delivery_time = $this->getOrderNextDeliverySlot($seller_id, $this->request->post['delivery_time']);
					
					if(empty($delivery_time)){
						$delivery_time = $this->request->post['delivery_time'];
					}
					
					$order_id = $this->model_checkout_order->addOrderNew($order_data);

					// Add to activity log
					$this->load->model('account/activity');

					$activity_data = array(
						'customer_id' => $customer_info['customer_id'],
						'name' => $customer_info['firstname'] . ' ' . $customer_info['lastname'],
						'order_id' => $order_id,
					);

					$this->model_account_activity->addActivity('order_account', $activity_data);

					$this->load->model('checkout/order');
					$order_info = $this->model_checkout_order->getOrder($order_id);

					$order_total_query = $this->db->query("SELECT * FROM `" . DB_PREFIX . "order_total` WHERE order_id = '" . (int) $order_id . "' ORDER BY sort_order ASC");

					foreach ($order_total_query->rows as $order_total) {
						$this->load->model('total/' . $order_total['code']);

						if (method_exists($this->{'model_total_' . $order_total['code']}, 'confirm')) {
							$this->{'model_total_' . $order_total['code']}->confirm($order_info, $order_total);
						}
					}

					$output[] = array(
						"message" => true,
						"orderid" => $order_id,
						"total" => $total + $deliveryCharges,
						"payment_firstname" => $order_data['payment_firstname'],
						"payment_lastname" => $order_data['payment_lastname'],
						"email" => $order_data['email'],
						"telephone" => $order_data['telephone'],
						"address_1" => $order_data['payment_address_1'],
						"address_2" => $order_data['payment_address_2'],
					);

					$this->load->model('module/deliveryTime/order');
					$this->model_module_deliveryTime_order->addDeliveryTimeV2($order_id, $delivery_time);
					
					try{
						$this->model_checkout_order->addOrderHistoryNew($order_id, 1, $order_data['comment'], true);
					} catch(Exception $e){
						print_r($e);
					}					

					// Add Coupon History
					if ($isCouponRequired) {
						$this->model_total_coupon->couponHistory($couponId, $order_id, $customer, $voucher);
					}					   	
				}
			} else {
				$output = array("message" => $couponMsg);
			}
		
			/* --------------------------------------- */

        } else {
            $output = array(
                "message" => 'No product found in cart',
            );
        }
        echo json_encode($output);

    }

}
