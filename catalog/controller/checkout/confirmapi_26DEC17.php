<?php
class ControllerCheckoutConfirmapi extends Controller
{
    public function index()
    {
        $order_data = array();
        
        $order_data['totals'] = array();
        $total                = 0;
      
        $cart_info = $this->db->query("SELECT * FROM " . DB_PREFIX . "cartmobile WHERE device_id = '" . $this->request->get['device_id'] . "'");
        //$cart_info = $this->db->query("SELECT * FROM " . DB_PREFIX . "cart WHERE customer_id = '" . $this->request->get['customer_id'] . "'");
        if(count($cart_info->rows) > 0){
        $i       = 0;
        $total   = 0;
        $voucher = 0;
        $voucher = $cart_info->rows[0]['voucher'];
        $this->load->model('catalog/product');
        
        foreach ($cart_info->rows as $cart_data) {
            $quantity  = $cart_info->rows[$i]['product_quantity'];
            $prod_info = $this->model_catalog_product->getProduct($cart_info->rows[$i]['product_id']);
            if ($prod_info['special'] == null) {
                $total = ($total + ($prod_info['price'] * $quantity));
            } else {
                $total = ($total + ($prod_info['special'] * $quantity));
            }
            $i++;
        }
        
        
        if ($this->request->get['gift_wrapping'] == "1") {
            $total         = (50 + (float) $total);
            $total_data[0] = array(
                'code' => 'gift_wrapping_total',
                'title' => 'Gif Wrapping',
                'text' => 'Jewellry Box (Rs. 50.00)',
                'value' => '50',
                'sort_order' => 0
            );
        } else {
            $total_data[0] = array(
                'code' => 'sub_total',
                'title' => 'Sub-Total',
                'text' => 'Sub-Total',
                'value' => $total,
                'sort_order' => 0
            );
        }
        if ($total > 100) {
            if ($this->request->get['shipping_code'] == "flat.flat") {
                $total           = (0 + (float) $total);
                $shipping_method = 'Low Order Fee';
                $total_data[1]   = array(
                    'code' => 'low_order_fee',
                    'title' => 'Low Order Fee',
                    'value' => '0',
                    'sort_order' => 3
                );
            }
        } else {
            if ($this->request->get['shipping_code'] == "flat.flat") {
                $total           = (15 + (float) $total);
                $shipping_method = 'Low Order Fee';
                $total_data[1]   = array(
                    'code' => 'low_order_fee',
                    'title' => 'Low Order Fee',
                    'value' => '15',
                    'sort_order' => 3
                );
            }
        }
       
        if ($voucher != '0') {
            $total         = $voucher;
            $total_data[2] = array(
                'code' => 'voucher',
                'title' => 'Voucher',
                'value' => $voucher,
                'sort_order' => 8
            );
        }
        $total_data[3] = array(
            'code' => 'total',
            'title' => 'Total',
            'value' => $total,
            'sort_order' => 9
        );
        
        if ($voucher != '0') {
            
                  $order_data['totals'] = array(
                              $total_data[0],
                              $total_data[1],
                              $total_data[2],
                              $total_data[3]
                          );
 
            }
            else{
                
                   $order_data['totals'] = array(
                        $total_data[0],
                        $total_data[1],
                        $total_data[3]
                    );
                
            }
        
      
        
        
         
        //array_multisort($sort_order, SORT_ASC, $order_data['totals']);
        
        $this->load->language('checkout/checkout');
        
        $order_data['invoice_prefix'] = $this->config->get('config_invoice_prefix');
        $order_data['store_id']       = $this->config->get('config_store_id');
        $order_data['store_name']     = $this->config->get('config_name') . " - Mobile App";
        
        if ($order_data['store_id']) {
            $order_data['store_url'] = $this->config->get('config_url');
        } else {
            $order_data['store_url'] = HTTP_SERVER;
        }
        
       
        $this->load->model('account/customer');
        
        $customer_info = $this->model_account_customer->getCustomer($this->request->get['customer_id']); //******************************************8
        
        $address_info = $this->db->query("SELECT DISTINCT * FROM " . DB_PREFIX . "address WHERE address_id = '" . (int) $customer_info['address_id'] . "' AND customer_id = '" . (int) $customer_info['customer_id'] . "'");
        
        $this->load->model('localisation/zone');
        $this->load->model('localisation/country');
        
        $zone_info    = $this->model_localisation_zone->getZone($address_info->row['zone_id']);
        $country_info = $this->model_localisation_country->getCountry($address_info->row['country_id']);
        
        $order_data['customer_id']       = $this->request->get['customer_id']; //******************************************8
        $order_data['customer_group_id'] = $customer_info['customer_group_id'];
        $order_data['firstname']         = $customer_info['firstname'];
        $order_data['lastname']          = $customer_info['lastname'];
        $order_data['email']             = $customer_info['email'];
        $order_data['telephone']         = $customer_info['telephone'];
        $order_data['fax']               = $customer_info['fax'];
        $order_data['custom_field']      = unserialize($customer_info['custom_field']);
        
        
        $order_data['payment_firstname']      = $customer_info['firstname'];
        $order_data['payment_lastname']       = $customer_info['lastname'];
        $order_data['payment_company']        = $address_info->row['company'];
        $order_data['payment_address_1']      = $address_info->row['address_1'];
        $order_data['payment_address_2']      = $address_info->row['address_2'];
        $order_data['payment_city']           = $address_info->row['city'];
        $order_data['payment_postcode']       = $address_info->row['postcode'];
        $order_data['payment_zone']           = $zone_info['name'];
        $order_data['payment_zone_id']        = $address_info->row['zone_id'];
        $order_data['payment_country']        = $country_info['name'];
        $order_data['payment_country_id']     = $address_info->row['country_id'];
        $order_data['payment_address_format'] = '';
        $order_data['payment_custom_field']   = '';
        $order_data['payment_method']         = $this->request->get['payment_method'];
        $order_data['payment_code']           = $this->request->get['payment_code'];
        
        //$zone_info    = $this->model_localisation_zone->getZone($this->request->get['input-shipping-zone']);
        $zoneId =  getZoneId($this->request->get['input-shipping-zone']);
        $zone_info    = $this->model_localisation_zone->getZone($zoneId);
        $country_info = $this->model_localisation_country->getCountry($this->request->get['input-shipping-country']);
        
        $order_data['shipping_firstname']      = $this->request->get['input-shipping-firstname'];
        $order_data['shipping_lastname']       = $this->request->get['input-shipping-lastname'];
        $order_data['shipping_company']        = $this->request->get['input-shipping-company'];
        $order_data['shipping_address_1']      = $this->request->get['input-shipping-address-1'];
        $order_data['shipping_address_2']      = $this->request->get['input-shipping-address-2'];
        $order_data['shipping_city']           = $this->request->get['input-shipping-city'];
        $order_data['shipping_postcode']       = $this->request->get['input-shipping-postcode'];
        $order_data['shipping_zone']           = $zone_info['name'];
        $order_data['shipping_zone_id']        = $zoneId;
        $order_data['shipping_country']        = $country_info['name'];
        $order_data['shipping_country_id']     = $this->request->get['input-shipping-country'];
        $order_data['shipping_address_format'] = "";
        $order_data['shipping_custom_field']   = array();
        $order_data['shipping_method']         = $shipping_method;
        $order_data['shipping_code']           = $this->request->get['shipping_code'];
        
        $order_data['products'] = array();
        
        if (isset($_GET['input-default-address'])) {
            $this->db->query("UPDATE " . DB_PREFIX . "address SET firstname = '" . $this->db->escape($this->request->get['input-shipping-firstname']) . "', lastname = '" . $this->db->escape($this->request->get['input-shipping-lastname']) . "', company = '" . $this->db->escape($this->request->get['input-shipping-company']) . "', address_1 = '" . $this->db->escape($this->request->get['input-shipping-address-1']) . "', address_2 = '" . $this->db->escape($this->request->get['input-shipping-address-2']) . "', postcode = '" . $this->db->escape($this->request->get['input-shipping-postcode']) . "', city = '" . $this->db->escape($this->request->get['input-shipping-city']) . "', zone_id = '" . (int) $zoneId . "', country_id = '" . (int) $this->request->get['input-shipping-country'] . "' WHERE address_id  = '" . (int) $this->request->get['address_id'] . "' AND customer_id = '" . (int) $this->request->get['customer_id'] . "'");
        }

        /*print("<pre>");
        print_r($cart_info->rows);
        die();*/
        //$cart_info
        
        //foreach ($this->cart->getProducts() as $product) {
        $i = 0;
        foreach ($cart_info->rows as $cart_data) {
            $prod_info = $this->model_catalog_product->getProduct($cart_info->rows[$i]['product_id']);
            
            $option_data = array();
            
            $product_options = $this->db->query("SELECT * FROM " . DB_PREFIX . "product_option_value 
 WHERE product_id = '" . (int) $prod_info['product_id'] . "' AND option_value_id = '" . (int) $cart_info->rows[$i]['product_option'] . "'");
            
            foreach ($product_options->rows as $option) {
                
                $option_value             = $this->db->query("SELECT * FROM " . DB_PREFIX . "option_description WHERE option_id = '" . (int) $option['option_id'] . "'");
                $option_type              = $this->db->query("SELECT * FROM " . DB_PREFIX . "option WHERE option_id = '" . (int) $option['option_id'] . "'");
                $option_value_description = $this->db->query("SELECT * FROM " . DB_PREFIX . "option_value_description WHERE option_value_id = '" . (int) $option['option_value_id'] . "'");
                
                $option_data[] = array(
                    'product_option_id' => $option['product_option_id'],
                    'product_option_value_id' => $option['product_option_value_id'],
                    'option_id' => $option['option_id'],
                    'option_value_id' => $option['option_value_id'],
                    'name' => $option_value->row['name'],
                    'value' => $option_value_description->row['name'],
                    'type' => $option_type->row['type']
                );
            }
            if ($prod_info['special'] == null) {
                $order_data['products'][] = array(
                    'product_id' => $prod_info['product_id'],
                    'name' => $prod_info['name'],
                    'model' => $prod_info['model'],
                    'option' => $option_data,
                    'download' => array(),
                    'quantity' => $cart_info->rows[$i]['product_quantity'],
                    'subtract' => $prod_info['subtract'],
                    'price' => ($prod_info['price'] * 1),
                    'total' => ($prod_info['price'] * (int) $cart_info->rows[$i]['product_quantity']),
                    'tax' => 0,
                    'reward' => 0
                );
                $i++;
            } else {
                $order_data['products'][] = array(
                    'product_id' => $prod_info['product_id'],
                    'name' => $prod_info['name'],
                    'model' => $prod_info['model'],
                    'option' => $option_data,
                    'download' => array(),
                    'quantity' => $cart_info->rows[$i]['product_quantity'],
                    'subtract' => $prod_info['subtract'],
                    'price' => ($prod_info['special'] * 1),
                    'total' => ($prod_info['special'] * (int) $cart_info->rows[$i]['product_quantity']),
                    'tax' => 0,
                    'reward' => 0
                );
                $i++;
            }
        }
        
        // Gift Voucher
        $order_data['vouchers'] = array();
        
        if (!empty($this->session->data['vouchers'])) {
            foreach ($this->session->data['vouchers'] as $voucher) {
                $order_data['vouchers'][] = array(
                    'description' => $voucher['description'],
                    'code' => substr(md5(mt_rand()), 0, 10),
                    'to_name' => $voucher['to_name'],
                    'to_email' => $voucher['to_email'],
                    'from_name' => $voucher['from_name'],
                    'from_email' => $voucher['from_email'],
                    'voucher_theme_id' => $voucher['voucher_theme_id'],
                    'message' => $voucher['message'],
                    'amount' => $voucher['amount']
                );
            }
        }
        
        $order_data['comment'] = "";
        $order_data['total']   = $total;
        
        if (isset($this->request->cookie['tracking'])) {
            $order_data['tracking'] = $this->request->cookie['tracking'];
            
            $subtotal = $this->cart->getSubTotal();
            
            // Affiliate
            $this->load->model('affiliate/affiliate');
            
            $affiliate_info = $this->model_affiliate_affiliate->getAffiliateByCode($this->request->cookie['tracking']);
            
            if ($affiliate_info) {
                $order_data['affiliate_id'] = $affiliate_info['affiliate_id'];
                $order_data['commission']   = ($subtotal / 100) * $affiliate_info['commission'];
            } else {
                $order_data['affiliate_id'] = 0;
                $order_data['commission']   = 0;
            }
            
            // Marketing
            $this->load->model('checkout/marketing');
            
            $marketing_info = $this->model_checkout_marketing->getMarketingByCode($this->request->cookie['tracking']);
            
            if ($marketing_info) {
                $order_data['marketing_id'] = $marketing_info['marketing_id'];
            } else {
                $order_data['marketing_id'] = 0;
            }
        } else {
            $order_data['affiliate_id'] = 0;
            $order_data['commission']   = 0;
            $order_data['marketing_id'] = 0;
            $order_data['tracking']     = '';
        }
        
        $order_data['language_id']    = $this->config->get('config_language_id');
        $order_data['currency_id']    = $this->currency->getId();
        $order_data['currency_code']  = $this->currency->getCode();
        $order_data['currency_value'] = $this->currency->getValue($this->currency->getCode());
        $order_data['ip']             = $this->request->server['REMOTE_ADDR'];
        
        if (!empty($this->request->server['HTTP_X_FORWARDED_FOR'])) {
            $order_data['forwarded_ip'] = $this->request->server['HTTP_X_FORWARDED_FOR'];
        } elseif (!empty($this->request->server['HTTP_CLIENT_IP'])) {
            $order_data['forwarded_ip'] = $this->request->server['HTTP_CLIENT_IP'];
        } else {
            $order_data['forwarded_ip'] = '';
        }
        
        if (isset($this->request->server['HTTP_USER_AGENT'])) {
            $order_data['user_agent'] = $this->request->server['HTTP_USER_AGENT'];
        } else {
            $order_data['user_agent'] = '';
        }
        
        if (isset($this->request->server['HTTP_ACCEPT_LANGUAGE'])) {
            $order_data['accept_language'] = $this->request->server['HTTP_ACCEPT_LANGUAGE'];
        } else {
            $order_data['accept_language'] = '';
        }
        
        	
        $this->load->model('checkout/order');
        // debugging($order_data);
        //$order_id = 100;
        $order_id = $this->model_checkout_order->addOrder($order_data);

	
        $output = array(
            "message" => true,
            "orderid" => $order_id,
            "total" => $total,
            "payment_firstname" => $order_data['payment_firstname'],
            "payment_lastname" => $order_data['payment_lastname'],
            "email" => $order_data['email'],
            "telephone" => $order_data['telephone'],
            "address_1" => $order_data['payment_address_1'],
            "address_2" => $order_data['payment_address_2']
        );
        	$this->model_checkout_order->addOrderHistory($order_id, 1, $comment = '',true);
        }
        else{
            $output = array(
            "message" => 'No product found in cart',            
            );
        }
        echo json_encode($output);
	     
        //}
       
    }
      public function confirmOrder()
    {
        $order_data = array();
        
        $order_data['totals'] = array();
        $total                = 0;
      
        $cart_info = $this->db->query("SELECT * FROM " . DB_PREFIX . "cart WHERE customer_id = '" . $this->request->get['customer_id'] . "'");
       
       
        if(count($cart_info->rows) > 0){
        $i       = 0;
        $total   = 0;
        $voucher = 0;
       // $voucher = $cart_info->rows[0]['voucher'];
       
        $this->load->model('catalog/product');
        
        foreach ($cart_info->rows as $cart_data) {
           
            $quantity  = $cart_data['quantity'];
            $prod_info = $this->model_catalog_product->getProduct($cart_data['product_id']);
            
            if ($prod_info['special'] == null) {
                $total = ($total + ($prod_info['price'] * $quantity));
            } else {
                $total = ($total + ($prod_info['special'] * $quantity));
            }
            $i++;
        }
       
        
        if ($this->request->get['gift_wrapping'] == "1") {
            $total         = (50 + (float) $total);
           
            $total_data[0] = array(
                'code' => 'gift_wrapping_total',
                'title' => 'Gif Wrapping',
                'text' => 'Jewellry Box (Rs. 50.00)',
                'value' => '50',
                'sort_order' => 0
            );
        } else {
            $total_data[0] = array(
                'code' => 'sub_total',
                'title' => 'Sub-Total',
                'text' => 'Sub-Total',
                'value' => $total,
                'sort_order' => 0
            );
        }
        if ($total > 100) {
            if ($this->request->get['shipping_code'] == "flat.flat") {
                $total           = (0 + (float) $total);
                
                $shipping_method = 'Low Order Fee';
                $total_data[1]   = array(
                    'code' => 'low_order_fee',
                    'title' => 'Low Order Fee',
                    'value' => '0',
                    'sort_order' => 3
                );
            }
        } else {
            if ($this->request->get['shipping_code'] == "flat.flat") {
                $total           = (15 + (float) $total);
              
                $shipping_method = 'Low Order Fee';
                $total_data[1]   = array(
                    'code' => 'low_order_fee',
                    'title' => 'Low Order Fee',
                    'value' => '15',
                    'sort_order' => 3
                );
            }
        }
       
        if ($voucher != '0') {
           
            $total         = $voucher;
           
            $total_data[2] = array(
                'code' => 'voucher',
                'title' => 'Voucher',
                'value' => $voucher,
                'sort_order' => 8
            );
        }
        $total_data[3] = array(
            'code' => 'total',
            'title' => 'Total',
            'value' => $total,
            'sort_order' => 9
        );
        
        if ($voucher != '0') {
            
                  $order_data['totals'] = array(
                              $total_data[0],
                              $total_data[1],
                              $total_data[2],
                              $total_data[3]
                          );
 
            }
            else{
                
                   $order_data['totals'] = array(
                        $total_data[0],
                        $total_data[1],
                        $total_data[3]
                    );
                
            }
        
      
      
        
         
        //array_multisort($sort_order, SORT_ASC, $order_data['totals']);
        
        $this->load->language('checkout/checkout');
        
        $order_data['invoice_prefix'] = $this->config->get('config_invoice_prefix');
        $order_data['store_id']       = $this->config->get('config_store_id');
        $order_data['store_name']     = $this->config->get('config_name') . " - Mobile App";
        
        if ($order_data['store_id']) {
            $order_data['store_url'] = $this->config->get('config_url');
        } else {
            $order_data['store_url'] = HTTP_SERVER;
        }
        
       
        $this->load->model('account/customer');
        
        $customer_info = $this->model_account_customer->getCustomer($this->request->get['customer_id']); //******************************************8
        
        $address_info = $this->db->query("SELECT DISTINCT * FROM " . DB_PREFIX . "address WHERE address_id = '" . (int) $customer_info['address_id'] . "' AND customer_id = '" . (int) $customer_info['customer_id'] . "'");
        
        $this->load->model('localisation/zone');
        $this->load->model('localisation/country');
        
        $zone_info    = $this->model_localisation_zone->getZone($address_info->row['zone_id']);
        $country_info = $this->model_localisation_country->getCountry($address_info->row['country_id']);
        
        $order_data['customer_id']       = $this->request->get['customer_id']; //******************************************8
        $order_data['customer_group_id'] = $customer_info['customer_group_id'];
        $order_data['firstname']         = $customer_info['firstname'];
        $order_data['lastname']          = $customer_info['lastname'];
        $order_data['email']             = $customer_info['email'];
        $order_data['telephone']         = $customer_info['telephone'];
        $order_data['fax']               = $customer_info['fax'];
        $order_data['custom_field']      = unserialize($customer_info['custom_field']);
        
        
        $order_data['payment_firstname']      = $customer_info['firstname'];
        $order_data['payment_lastname']       = $customer_info['lastname'];
        $order_data['payment_company']        = $address_info->row['company'];
        $order_data['payment_address_1']      = $address_info->row['address_1'];
        $order_data['payment_address_2']      = $address_info->row['address_2'];
        $order_data['payment_city']           = $address_info->row['city'];
        $order_data['payment_postcode']       = $address_info->row['postcode'];
        $order_data['payment_zone']           = $zone_info['name'];
        $order_data['payment_zone_id']        = $address_info->row['zone_id'];
        $order_data['payment_country']        = $country_info['name'];
        $order_data['payment_country_id']     = $address_info->row['country_id'];
        $order_data['payment_address_format'] = '';
        $order_data['payment_custom_field']   = '';
        $order_data['payment_method']         = $this->request->get['payment_method'];
        $order_data['payment_code']           = $this->request->get['payment_code'];
        
        //$zone_info    = $this->model_localisation_zone->getZone($this->request->get['input-shipping-zone']);
        $zoneId =  getZoneId($this->request->get['input-shipping-zone']);
        $zone_info    = $this->model_localisation_zone->getZone($zoneId);
        $country_info = $this->model_localisation_country->getCountry($this->request->get['input-shipping-country']);
        
        $order_data['shipping_firstname']      = $this->request->get['input-shipping-firstname'];
        $order_data['shipping_lastname']       = $this->request->get['input-shipping-lastname'];
        $order_data['shipping_company']        = $this->request->get['input-shipping-company'];
        $order_data['shipping_address_1']      = $this->request->get['input-shipping-address-1'];
        $order_data['shipping_address_2']      = $this->request->get['input-shipping-address-2'];
        $order_data['shipping_city']           = $this->request->get['input-shipping-city'];
        $order_data['shipping_postcode']       = $this->request->get['input-shipping-postcode'];
        $order_data['shipping_zone']           = $zone_info['name'];
        $order_data['shipping_zone_id']        = $zoneId;
        $order_data['shipping_country']        = $country_info['name'];
        $order_data['shipping_country_id']     = $this->request->get['input-shipping-country'];
        $order_data['shipping_address_format'] = "";
        $order_data['shipping_custom_field']   = array();
        $order_data['shipping_method']         = $shipping_method;
        $order_data['shipping_code']           = $this->request->get['shipping_code'];
        
        $order_data['products'] = array();
        
        if (isset($_GET['input-default-address'])) {
            $this->db->query("UPDATE " . DB_PREFIX . "address SET firstname = '" . $this->db->escape($this->request->get['input-shipping-firstname']) . "', lastname = '" . $this->db->escape($this->request->get['input-shipping-lastname']) . "', company = '" . $this->db->escape($this->request->get['input-shipping-company']) . "', address_1 = '" . $this->db->escape($this->request->get['input-shipping-address-1']) . "', address_2 = '" . $this->db->escape($this->request->get['input-shipping-address-2']) . "', postcode = '" . $this->db->escape($this->request->get['input-shipping-postcode']) . "', city = '" . $this->db->escape($this->request->get['input-shipping-city']) . "', zone_id = '" . (int) $zoneId . "', country_id = '" . (int) $this->request->get['input-shipping-country'] . "' WHERE address_id  = '" . (int) $this->request->get['address_id'] . "' AND customer_id = '" . (int) $this->request->get['customer_id'] . "'");
        }

       
        $i = 0;
        foreach ($cart_info->rows as $cart_data) {
            $prod_info = $this->model_catalog_product->getProduct($cart_info->rows[$i]['product_id']);
            
            $option_data = array();
            
            $product_options = $this->db->query("SELECT * FROM " . DB_PREFIX . "product_option_value 
 WHERE product_id = '" . (int) $prod_info['product_id'] . "' AND option_value_id = '" . (int) $cart_info->rows[$i]['product_option'] . "'");
            
            foreach ($product_options->rows as $option) {
                
                $option_value             = $this->db->query("SELECT * FROM " . DB_PREFIX . "option_description WHERE option_id = '" . (int) $option['option_id'] . "'");
                $option_type              = $this->db->query("SELECT * FROM " . DB_PREFIX . "option WHERE option_id = '" . (int) $option['option_id'] . "'");
                $option_value_description = $this->db->query("SELECT * FROM " . DB_PREFIX . "option_value_description WHERE option_value_id = '" . (int) $option['option_value_id'] . "'");
                
                $option_data[] = array(
                    'product_option_id' => $option['product_option_id'],
                    'product_option_value_id' => $option['product_option_value_id'],
                    'option_id' => $option['option_id'],
                    'option_value_id' => $option['option_value_id'],
                    'name' => $option_value->row['name'],
                    'value' => $option_value_description->row['name'],
                    'type' => $option_type->row['type']
                );
            }
            if ($prod_info['special'] == null) {
                $order_data['products'][] = array(
                    'product_id' => $prod_info['product_id'],
                    'name' => $prod_info['name'],
                    'model' => $prod_info['model'],
                    'option' => $option_data,
                    'download' => array(),
                    'quantity' => $cart_info->rows[$i]['quantity'],
                    'subtract' => $prod_info['subtract'],
                    'price' => ($prod_info['price'] * 1),
                    'total' => ($prod_info['price'] * (int) $cart_info->rows[$i]['quantity']),
                    'tax' => 0,
                    'reward' => 0
                );
                $i++;
            } else {
                $order_data['products'][] = array(
                    'product_id' => $prod_info['product_id'],
                    'name' => $prod_info['name'],
                    'model' => $prod_info['model'],
                    'option' => $option_data,
                    'download' => array(),
                    'quantity' => $cart_info->rows[$i]['quantity'],
                    'subtract' => $prod_info['subtract'],
                    'price' => ($prod_info['special'] * 1),
                    'total' => ($prod_info['special'] * (int) $cart_info->rows[$i]['quantity']),
                    'tax' => 0,
                    'reward' => 0
                );
                $i++;
            }
        }
        
        // Gift Voucher
        $order_data['vouchers'] = array();
        
        if (!empty($this->session->data['vouchers'])) {
            foreach ($this->session->data['vouchers'] as $voucher) {
                $order_data['vouchers'][] = array(
                    'description' => $voucher['description'],
                    'code' => substr(md5(mt_rand()), 0, 10),
                    'to_name' => $voucher['to_name'],
                    'to_email' => $voucher['to_email'],
                    'from_name' => $voucher['from_name'],
                    'from_email' => $voucher['from_email'],
                    'voucher_theme_id' => $voucher['voucher_theme_id'],
                    'message' => $voucher['message'],
                    'amount' => $voucher['amount']
                );
            }
        }
        
        $order_data['comment'] = "";
        $order_data['total']   = $total;
        
        if (isset($this->request->cookie['tracking'])) {
            $order_data['tracking'] = $this->request->cookie['tracking'];
            
            $subtotal = $this->cart->getSubTotal();
            
            // Affiliate
            $this->load->model('affiliate/affiliate');
            
            $affiliate_info = $this->model_affiliate_affiliate->getAffiliateByCode($this->request->cookie['tracking']);
            
            if ($affiliate_info) {
                $order_data['affiliate_id'] = $affiliate_info['affiliate_id'];
                $order_data['commission']   = ($subtotal / 100) * $affiliate_info['commission'];
            } else {
                $order_data['affiliate_id'] = 0;
                $order_data['commission']   = 0;
            }
            
            // Marketing
            $this->load->model('checkout/marketing');
            
            $marketing_info = $this->model_checkout_marketing->getMarketingByCode($this->request->cookie['tracking']);
            
            if ($marketing_info) {
                $order_data['marketing_id'] = $marketing_info['marketing_id'];
            } else {
                $order_data['marketing_id'] = 0;
            }
        } else {
            $order_data['affiliate_id'] = 0;
            $order_data['commission']   = 0;
            $order_data['marketing_id'] = 0;
            $order_data['tracking']     = '';
        }
        
        $order_data['language_id']    = $this->config->get('config_language_id');
        $order_data['currency_id']    = $this->currency->getId();
        $order_data['currency_code']  = $this->currency->getCode();
        $order_data['currency_value'] = $this->currency->getValue($this->currency->getCode());
        $order_data['ip']             = $this->request->server['REMOTE_ADDR'];
        
        if (!empty($this->request->server['HTTP_X_FORWARDED_FOR'])) {
            $order_data['forwarded_ip'] = $this->request->server['HTTP_X_FORWARDED_FOR'];
        } elseif (!empty($this->request->server['HTTP_CLIENT_IP'])) {
            $order_data['forwarded_ip'] = $this->request->server['HTTP_CLIENT_IP'];
        } else {
            $order_data['forwarded_ip'] = '';
        }
        
        if (isset($this->request->server['HTTP_USER_AGENT'])) {
            $order_data['user_agent'] = $this->request->server['HTTP_USER_AGENT'];
        } else {
            $order_data['user_agent'] = '';
        }
        
        if (isset($this->request->server['HTTP_ACCEPT_LANGUAGE'])) {
            $order_data['accept_language'] = $this->request->server['HTTP_ACCEPT_LANGUAGE'];
        } else {
            $order_data['accept_language'] = '';
        }
        
        	
        $this->load->model('checkout/order');
        // debugging($order_data);
               $order_id = $this->model_checkout_order->addOrder($order_data);

	
        $output = array(
            "message" => true,
            "orderid" => $order_id,
            "total" => $total,
            "payment_firstname" => $order_data['payment_firstname'],
            "payment_lastname" => $order_data['payment_lastname'],
            "email" => $order_data['email'],
            "telephone" => $order_data['telephone'],
            "address_1" => $order_data['payment_address_1'],
            "address_2" => $order_data['payment_address_2']
        );
        	$this->model_checkout_order->addOrderHistory($order_id, 1, $comment = '',true);
        	
        	$this->load->model('module/deliveryTime/order');
            $this->model_module_deliveryTime_order->addDeliveryTimeV2($order_id,$this->request->get['delivery_time']);
        }
        else{
            $output = array(
            "message" => 'No product found in cart',            
            );
        }
        echo json_encode($output);
	    
       
    }
    
    public function confirmOrderV2()
    {
        $this->load->language('total/coupon');
        $couponMsg = $this->language->get('error_coupon');
        
        $order_data = array();
        
        $order_data['totals'] = array();
        $total                = 0;

       
        $cart_info = $this->db->query("SELECT * FROM " . DB_PREFIX . "cart WHERE customer_id = '" . $this->request->get['customer_id'] . "'");
         $balance = $this->db->query("SELECT SUM(amount) AS total FROM " . DB_PREFIX . "customer_transaction WHERE customer_id = '" . $this->request->get['customer_id'] . "'");
         foreach ($balance->rows as $balance_total) {
       $credit_total = $balance_total['total'];
    //    echo "<pre>";print_r($credit_total);echo "</pre>";die;
         }
       $isCoupon = true;
        if(count($cart_info->rows) > 0){
            
        $i       = 0;
        $total   = 0;
        $voucher = 0;    
       if (isset($this->request->get['coupon']) && $this->request->get['coupon'] !='undefined' && $this->request->get['coupon'] !='null') {
        
                $this->load->model('total/coupon');        
                
                
                $coupon   = $this->request->get['coupon'];
                
                $customer = (int) $this->request->get['customer_id'];
                $device   =  $this->request->get['device_id'];
                $language   =  1;
                
                $coupon_info = $this->model_total_coupon->getMobileCoupon($coupon,$customer,$language,$device);
       
                $isCoupon = ($coupon_info) ? true : false;
                
                $voucher_info = $this->model_total_coupon->getMobileTotal($coupon,$customer,$language,$device);    
                 $couponId =  $coupon_info['coupon_id'];
                
                if($voucher_info)
                {                    
                    $voucher = $voucher_info[0]['value'];
                } 
        } // Coupon code
        if($isCoupon){
        
        $this->load->model('catalog/product');
        
        foreach ($cart_info->rows as $cart_data) {
           
            $quantity  = $cart_data['quantity'];
            $prod_info = $this->model_catalog_product->getProduct($cart_data['product_id']);
            
            if ($prod_info['special'] == null) {
                $total = ($total + ($prod_info['price'] * $quantity));
            } else {
                $total = ($total + ($prod_info['special'] * $quantity));
            }
            $i++;
        }
       
        
        if ($this->request->get['gift_wrapping'] == "1") {
            $total         = (50 + (float) $total);
           
            $total_data[0] = array(
                'code' => 'gift_wrapping_total',
                'title' => 'Gif Wrapping',
                'text' => 'Jewellry Box (Rs. 50.00)',
                'value' => '50',
                'sort_order' => 0
            );
        } else {
            $total_data[0] = array(
                'code' => 'sub_total',
                'title' => 'Sub-Total',
                'text' => 'Sub-Total',
                'value' => $total,
                'sort_order' => 0
            );
        }
        if ($total > 100) {
            if ($this->request->get['shipping_code'] == "flat.flat") {
                $total           = (0 + (float) $total);
                
                $shipping_method = 'Low Order Fee';
                $total_data[1]   = array(
                    'code' => 'low_order_fee',
                    'title' => 'Low Order Fee',
                    'value' => '0',
                    'sort_order' => 3
                );
            }
        } else {
            if ($this->request->get['shipping_code'] == "flat.flat") {
                $total           = (15 + (float) $total);
              
                $shipping_method = 'Low Order Fee';
                $total_data[1]   = array(
                    'code' => 'low_order_fee',
                    'title' => 'Low Order Fee',
                    'value' => '15',
                    'sort_order' => 3
                );
            }
        }
       
        if ($voucher != '0') {
           $isCouponRequired = true;
            $total  = $voucher_info[1];
           
            $total_data[2] = array(
                'code' => $voucher_info[0]['code'],
                'title' => $voucher_info[0]['title'],
                'value' => -$voucher,
                'sort_order' => 8
            );
        }
        $total_data[3] = array(
            'code' => 'total',
            'title' => 'Total',
            'value' => $total,
            'sort_order' => 9
        );
        
        if ($voucher != '0') {
            $isCouponRequired = true;
                  $order_data['totals'] = array(
                              $total_data[0],
                              $total_data[1],
                              $total_data[2],
                              $total_data[3]
                          );
 
            }
            else{
                
                   $order_data['totals'] = array(
                        $total_data[0],
                        $total_data[1],
                        $total_data[3]
                    );
                
            }
        
      
      
        
         
        //array_multisort($sort_order, SORT_ASC, $order_data['totals']);
        
        $this->load->language('checkout/checkout');
        
        $order_data['invoice_prefix'] = $this->config->get('config_invoice_prefix');
        $order_data['store_id']       = $this->config->get('config_store_id');
        $order_data['store_name']     = $this->config->get('config_name') . " - Mobile App";
        
        if ($order_data['store_id']) {
            $order_data['store_url'] = $this->config->get('config_url');
        } else {
            $order_data['store_url'] = HTTP_SERVER;
        }
        
       
        $this->load->model('account/customer');
        
        $customer_info = $this->model_account_customer->getCustomer($this->request->get['customer_id']); //******************************************8
        
        $address_info = $this->db->query("SELECT DISTINCT * FROM " . DB_PREFIX . "address WHERE address_id = '" . (int) $customer_info['address_id'] . "' AND customer_id = '" . (int) $customer_info['customer_id'] . "'");
        
        $this->load->model('localisation/zone');
        $this->load->model('localisation/country');
        
        $zone_info    = $this->model_localisation_zone->getZone($address_info->row['zone_id']);
        $country_info = $this->model_localisation_country->getCountry($address_info->row['country_id']);
        
        $order_data['customer_id']       = $this->request->get['customer_id']; //******************************************8
        $order_data['customer_group_id'] = $customer_info['customer_group_id'];
        $order_data['firstname']         = $customer_info['firstname'];
        $order_data['lastname']          = $customer_info['lastname'];
        $order_data['email']             = $customer_info['email'];
        $order_data['telephone']         = $customer_info['telephone'];
        $order_data['fax']               = $customer_info['fax'];
        $order_data['custom_field']      = unserialize($customer_info['custom_field']);
        
        
        $order_data['payment_firstname']      = $customer_info['firstname'];
        $order_data['payment_lastname']       = $customer_info['lastname'];
        $order_data['payment_company']        = $address_info->row['company'];
        $order_data['payment_address_1']      = $address_info->row['address_1'];
        $order_data['payment_address_2']      = $address_info->row['address_2'];
        $order_data['payment_city']           = $address_info->row['city'];
        $order_data['payment_postcode']       = $address_info->row['postcode'];
        $order_data['payment_zone']           = $zone_info['name'];
        $order_data['payment_zone_id']        = $address_info->row['zone_id'];
        $order_data['payment_country']        = $country_info['name'];
        $order_data['payment_country_id']     = $address_info->row['country_id'];
        $order_data['payment_address_format'] = '';
        $order_data['payment_custom_field']   = '';
        $order_data['payment_method']         = $this->request->get['payment_method'];
        $order_data['payment_code']           = $this->request->get['payment_code'];
        
        //$zone_info    = $this->model_localisation_zone->getZone($this->request->get['input-shipping-zone']);
        $zoneId =  getZoneId($this->request->get['input-shipping-zone']);
        $zone_info    = $this->model_localisation_zone->getZone($zoneId);
        $country_info = $this->model_localisation_country->getCountry($this->request->get['input-shipping-country']);
        
        $order_data['shipping_firstname']      = $this->request->get['input-shipping-firstname'];
        $order_data['shipping_lastname']       = $this->request->get['input-shipping-lastname'];
        $order_data['shipping_company']        = $this->request->get['input-shipping-company'];
        $order_data['shipping_address_1']      = $this->request->get['input-shipping-address-1'];
        $order_data['shipping_address_2']      = $this->request->get['input-shipping-address-2'];
        $order_data['shipping_city']           = $this->request->get['input-shipping-city'];
        $order_data['shipping_postcode']       = $this->request->get['input-shipping-postcode'];
        $order_data['shipping_zone']           = $zone_info['name'];
        $order_data['shipping_zone_id']        = $zoneId;
        $order_data['shipping_country']        = $country_info['name'];
        $order_data['shipping_country_id']     = $this->request->get['input-shipping-country'];
        $order_data['shipping_address_format'] = "";
        $order_data['shipping_custom_field']   = array();
        $order_data['shipping_method']         = $shipping_method;
        $order_data['shipping_code']           = $this->request->get['shipping_code'];
        
        $order_data['products'] = array();
        
        if (isset($_GET['input-default-address'])) {
            $this->db->query("UPDATE " . DB_PREFIX . "address SET firstname = '" . $this->db->escape($this->request->get['input-shipping-firstname']) . "', lastname = '" . $this->db->escape($this->request->get['input-shipping-lastname']) . "', company = '" . $this->db->escape($this->request->get['input-shipping-company']) . "', address_1 = '" . $this->db->escape($this->request->get['input-shipping-address-1']) . "', address_2 = '" . $this->db->escape($this->request->get['input-shipping-address-2']) . "', postcode = '" . $this->db->escape($this->request->get['input-shipping-postcode']) . "', city = '" . $this->db->escape($this->request->get['input-shipping-city']) . "', zone_id = '" . (int) $zoneId . "', country_id = '" . (int) $this->request->get['input-shipping-country'] . "' WHERE address_id  = '" . (int) $this->request->get['address_id'] . "' AND customer_id = '" . (int) $this->request->get['customer_id'] . "'");
        }

       
        $i = 0;
        foreach ($cart_info->rows as $cart_data) {
            $prod_info = $this->model_catalog_product->getProduct($cart_info->rows[$i]['product_id']);
            
            $option_data = array();
            
            $product_options = $this->db->query("SELECT * FROM " . DB_PREFIX . "product_option_value 
 WHERE product_id = '" . (int) $prod_info['product_id'] . "' AND option_value_id = '" . (int) $cart_info->rows[$i]['product_option'] . "'");
            
            foreach ($product_options->rows as $option) {
                
                $option_value             = $this->db->query("SELECT * FROM " . DB_PREFIX . "option_description WHERE option_id = '" . (int) $option['option_id'] . "'");
                $option_type              = $this->db->query("SELECT * FROM " . DB_PREFIX . "option WHERE option_id = '" . (int) $option['option_id'] . "'");
                $option_value_description = $this->db->query("SELECT * FROM " . DB_PREFIX . "option_value_description WHERE option_value_id = '" . (int) $option['option_value_id'] . "'");
                
                $option_data[] = array(
                    'product_option_id' => $option['product_option_id'],
                    'product_option_value_id' => $option['product_option_value_id'],
                    'option_id' => $option['option_id'],
                    'option_value_id' => $option['option_value_id'],
                    'name' => $option_value->row['name'],
                    'value' => $option_value_description->row['name'],
                    'type' => $option_type->row['type']
                );
            }
            if ($prod_info['special'] == null) {
                $order_data['products'][] = array(
                    'product_id' => $prod_info['product_id'],
                    'name' => $prod_info['name'],
                    'model' => $prod_info['model'],
                    'option' => $option_data,
                    'download' => array(),
                    'quantity' => $cart_info->rows[$i]['quantity'],
                    'subtract' => $prod_info['subtract'],
                    'price' => ($prod_info['price'] * 1),
                    'total' => ($prod_info['price'] * (int) $cart_info->rows[$i]['quantity']),
                    'tax' => 0,
                    'reward' => 0
                );
                $i++;
            } else {
                $order_data['products'][] = array(
                    'product_id' => $prod_info['product_id'],
                    'name' => $prod_info['name'],
                    'model' => $prod_info['model'],
                    'option' => $option_data,
                    'download' => array(),
                    'quantity' => $cart_info->rows[$i]['quantity'],
                    'subtract' => $prod_info['subtract'],
                    'price' => ($prod_info['special'] * 1),
                    'total' => ($prod_info['special'] * (int) $cart_info->rows[$i]['quantity']),
                    'tax' => 0,
                    'reward' => 0
                );
                $i++;
            }
        }
        
        // Gift Voucher
        $order_data['vouchers'] = array();
        
        if (!empty($this->session->data['vouchers'])) {
            foreach ($this->session->data['vouchers'] as $voucher) {
                $order_data['vouchers'][] = array(
                    'description' => $voucher['description'],
                    'code' => substr(md5(mt_rand()), 0, 10),
                    'to_name' => $voucher['to_name'],
                    'to_email' => $voucher['to_email'],
                    'from_name' => $voucher['from_name'],
                    'from_email' => $voucher['from_email'],
                    'voucher_theme_id' => $voucher['voucher_theme_id'],
                    'message' => $voucher['message'],
                    'amount' => $voucher['amount']
                );
            }
        }
        
        $order_data['comment'] = "";
        $order_data['total']   = $total;
        
        if (isset($this->request->cookie['tracking'])) {
            $order_data['tracking'] = $this->request->cookie['tracking'];
            
            $subtotal = $this->cart->getSubTotal();
            
            // Affiliate
            $this->load->model('affiliate/affiliate');
            
            $affiliate_info = $this->model_affiliate_affiliate->getAffiliateByCode($this->request->cookie['tracking']);
            
            if ($affiliate_info) {
                $order_data['affiliate_id'] = $affiliate_info['affiliate_id'];
                $order_data['commission']   = ($subtotal / 100) * $affiliate_info['commission'];
            } else {
                $order_data['affiliate_id'] = 0;
                $order_data['commission']   = 0;
            }
            
            // Marketing
            $this->load->model('checkout/marketing');
            
            $marketing_info = $this->model_checkout_marketing->getMarketingByCode($this->request->cookie['tracking']);
            
            if ($marketing_info) {
                $order_data['marketing_id'] = $marketing_info['marketing_id'];
            } else {
                $order_data['marketing_id'] = 0;
            }
        } else {
            $order_data['affiliate_id'] = 0;
            $order_data['commission']   = 0;
            $order_data['marketing_id'] = 0;
            $order_data['tracking']     = '';
        }
        
        $order_data['language_id']    = $this->config->get('config_language_id');
        $order_data['currency_id']    = $this->currency->getId();
        $order_data['currency_code']  = $this->currency->getCode();
        $order_data['currency_value'] = $this->currency->getValue($this->currency->getCode());
        $order_data['ip']             = $this->request->server['REMOTE_ADDR'];
        
        if (!empty($this->request->server['HTTP_X_FORWARDED_FOR'])) {
            $order_data['forwarded_ip'] = $this->request->server['HTTP_X_FORWARDED_FOR'];
        } elseif (!empty($this->request->server['HTTP_CLIENT_IP'])) {
            $order_data['forwarded_ip'] = $this->request->server['HTTP_CLIENT_IP'];
        } else {
            $order_data['forwarded_ip'] = '';
        }
        
        if (isset($this->request->server['HTTP_USER_AGENT'])) {
            $order_data['user_agent'] = $this->request->server['HTTP_USER_AGENT'];
        } else {
            $order_data['user_agent'] = '';
        }
        
        if (isset($this->request->server['HTTP_ACCEPT_LANGUAGE'])) {
            $order_data['accept_language'] = $this->request->server['HTTP_ACCEPT_LANGUAGE'];
        } else {
            $order_data['accept_language'] = '';
        }
        
        	
        $this->load->model('checkout/order');
        if ($this->config->get('credit_status')) {
			$this->load->language('total/credit');

			

			if ((float)$credit_total) {
				if ($credit_total > $total) {
					$credit = $total;
				} else {
					$credit = $credit_total;
				}
                
                
				if ($credit > 0) {
                     $total -= $credit;
                    // $order_data['total']   = $total;
            
                    
					$total_data[4] = array(
						'code'       => 'credit',
						'title'      => 'Store Credit',
						'value'      => -$credit,
						'sort_order' => 7
					);
                    
                    $total_data[3] = array(
            'code' => 'total',
            'title' => 'Total',
            'value' => $total,
            'sort_order' => 9
        );
        $order_data['totals'] = array(
                        $total_data[0],
                        $total_data[1],
                        $total_data[3],
                        $total_data[4]
                    );
					
                   


				}
                if ($credit_total>$credit) {
                                        $credit_total = $credit_total-$credit;
                }else {
                    $credit_total =$credit-$credit_total;
                }
			}
            
			
		
		}
        // debugging($order_data);
               $order_id = $this->model_checkout_order->addOrder($order_data);

	
        $output = array(
            "message" => true,
            "orderid" => $order_id,
            "total" => $total,
            "payment_firstname" => $order_data['payment_firstname'],
            "payment_lastname" => $order_data['payment_lastname'],
            "email" => $order_data['email'],
            "telephone" => $order_data['telephone'],
            "address_1" => $order_data['payment_address_1'],
            "address_2" => $order_data['payment_address_2']
        );
        	
        	
        	$this->load->model('module/deliveryTime/order');
            $this->model_module_deliveryTime_order->addDeliveryTimeV2($order_id,$this->request->get['delivery_time']);
            
            $this->model_checkout_order->addOrderHistory($order_id, 1, $comment = '',true);
            // Add Coupon History
            if($isCouponRequired){
                $this->model_total_coupon->couponHistory($couponId,$order_id,$customer,$voucher) ;
            }
            
            
            
            } // Coupon True
            else{
                $output = array( "message" => $couponMsg);
            }
        } // Products
        else{
            $output = array(
            "message" => 'No product found in cart',            
            );
        }
        echo json_encode($output);
	    
       
    }
    
    public function updateAddress()
    {
        $this->db->query("UPDATE " . DB_PREFIX . "address SET firstname = '" . $this->db->escape($this->request->get['input-shipping-firstname']) . "', lastname = '" . $this->db->escape($this->request->get['input-shipping-lastname']) . "', company = '" . $this->db->escape($this->request->get['input-shipping-company']) . "', address_1 = '" . $this->db->escape($this->request->get['input-shipping-address-1']) . "', address_2 = '" . $this->db->escape($this->request->get['input-shipping-address-2']) . "', postcode = '" . $this->db->escape($this->request->get['input-shipping-postcode']) . "', city = '" . $this->db->escape($this->request->get['input-shipping-city']) . "', zone_id = '" . (int) $this->request->get['input-shipping-zone'] . "', country_id = '" . (int) $this->request->get['input-shipping-country'] . "' WHERE address_id  = '" . (int) $this->request->get['address_id'] . "' AND customer_id = '" . (int) $this->request->get['customer_id'] . "'");
        
        echo true;
    }
    
}