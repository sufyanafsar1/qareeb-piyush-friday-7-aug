<?php
class ControllerCheckoutConfirmapiNewtest extends Controller
{
	public function getOrderNextDeliverySlot($seller_info, $delivery_time)
	{

		$timeArray = $this->getStoreDeliveryTimeNew($seller_info, $delivery_time);
		return $timeArray['time_content'];
	}

	public function getStoreDeliveryTimeNew($seller_info, $delivery_time)
	{

		$this->load->language('api/localisation');

		$languageId = (isset($this->request->get['language'])) ? $this->request->get['language'] : 1;

		if ($languageId == 1) {
			$text_today          = $this->language->get('text_today');
			$text_tomorrow       = $this->language->get('text_tomorrow');
			$text_next_delivery  = $this->language->get('text_next_delivery');
			$text_no_delivery    = $this->language->get('text_no_delivery');
			$text_unavailable    = $this->language->get('text_unavailable');
		} else {
			$text_today          = $this->language->get('text_today_arabic');
			$text_tomorrow       = $this->language->get('text_tomorrow_arabic');
			$text_next_delivery  = $this->language->get('text_next_delivery_arabic');
			$text_no_delivery    = $this->language->get('text_no_delivery_arabic');
			$text_unavailable    = $this->language->get('text_unavailable_arabic');
		}

		$text_open          = $this->language->get('text_open');
		$text_close          = $this->language->get('text_close');

		date_default_timezone_get();

		$interval = $seller_info["delivery_timegap"] * 60;
		$package = $seller_info["package_ready"] * 60;

		//$times = explode(" - ",$delivery_time);		
		if (strpos($delivery_time, ':PM') !== false) {
			$times = str_replace(":PM", " PM", $delivery_time);
		} else if (strpos($delivery_time, ':AM') !== false) {
			$times = str_replace(":AM", " AM", $delivery_time);
		} else {
			$times = $delivery_time;
		}

		$now = strtotime($times);
		$full = date('l', strtotime($times));
		$half = date('D', strtotime($times));
		$time = date('h:i a', strtotime($times));
		$short_time = date('g:i:s', strtotime($times));
		$strtime = strtotime($times);
		//$date = date('Y-m-d',strtotime($times));
		if ($times == 'Store closed') {
			$date = date('Y-m-d');
			$times = $date;
		} else {
			$date = date('Y-m-d', strtotime($times));
		}

		/* $timeNew = substr_replace($times ,"", -3);;
		$dateDb = date("d-m-Y",strtotime($timeNew)); */

		$timeNew = explode(" ", $times);
		$dateDb =  date('d-m-Y', strtotime($timeNew[0]));

		$nowdata = array(
			'full'       => $full,
			'half'   => $half,
			'time'   => $time,
			'short_time' => $short_time,
			'strtime'    => $strtime,
			'date' => $date
		);

		/* if((!isset($delivery_time)) && ($delivery_time == '')){
			$now = strtotime("now");			
			$nowdata = dayData("now");		
		} */

		// Declare and define two dates 
		//$date1 = strtotime(date('Y-m-d H:i:s')); 
		//$date2 = strtotime($times); 
		$date1 = strtotime(date('d-m-Y'));
		$date2 = strtotime(date('d-m-Y', strtotime($times)));
		$diff = abs($date2 - $date1);
		$days = floor(($diff - $years * 365 * 60 * 60 * 24 -  $months * 30 * 60 * 60 * 24) / (60 * 60 * 24));

		/* if((isset($days)) && ($days != 0)){
			$fdata = dayData(" +".$days." day");
		} else {
			$fdata = dayData(" +1 day");			
		}
		$sdata = dayData(" +2 day"); */

		if ((isset($days)) && ($days != 0)) {
			$fdata = dayData(" +" . $days . " day");
			$snumber = $days + 1;
		} else {
			$fdata = dayData(" +1 day");
			$snumber = 2;
		}
		$sdata = dayData(" +" . $snumber . " day");

		// $tdata = dayData(" +3 day");
		// $ffdata = dayData(" +4 day");
		// $fffdata = dayData(" +5 day");
		// $ssdata = dayData(" +6 day");

		if ((!isset($date2)) || ($date2 == 0)) {
			$now = strtotime("now");
			$nowdata = dayData("now");
		}


		if ($nowdata["full"] ==	date("l")) {
			if ($seller_info[strtolower(substr($nowdata["half"], 0, 3)) . "_start_time"] != "00:00:00" && $seller_info[strtolower(substr($nowdata["half"], 0, 3)) . "_end_time"] != "00:00:00") {

				$nowstarttime = strtotime($nowdata["date"] . " " . $seller_info[strtolower(substr($nowdata["half"], 0, 3)) . "_start_time"]) + $interval;

				$nowendtime = strtotime($nowdata["date"] . " " . $seller_info[strtolower(substr($nowdata["half"], 0, 3)) . "_end_time"]);

				$nowoutput = "";
				$nownextdelivery = "";
				$nowdelivery = "";
				$j = 0;

				for ($i = $nowstarttime; $i < $nowendtime; $i += $interval) {
					$nntt = $i + $interval;
					if ($i < $now) {
					} else {
						if ($j == 0) {
							$nextTime = $now + $package;
							if ($i < $nextTime) {
							} else {

								$nowdelivery = arabicTime(strtotime(date("h:i:s a", $i)), $languageId) . " - " . arabicTime(strtotime(date("h:i:s a", $nntt)), $languageId);

								//$sellerinfo['time_content'] = $dateDb.' '.$text_next_delivery.' '.$text_today.' '.$nowdelivery;

								$sellerinfo['time_content'] = $dateDb . ' ' . date("H:i:s", $i) . "  - " . $dateDb . '  ' . date("H:i:s", $nntt);

								$lastTime = strtotime($dateDb . ' ' . date("H:i:s", $nntt));

								$j++;
							}
						}
					}
				}
			}
		}

		$nowTime = strtotime(date("d-m-Y H:i:s"));
		$dateDb =  date('d-m-Y', strtotime($fdata["date"]));
		//if(empty($sellerinfo)){
		if ((empty($sellerinfo)) || ($lastTime < $nowTime)) {
			// +1 Day
			if ($seller_info[strtolower(substr($fdata["half"], 0, 3)) . "_start_time"] != "00:00:00" && $seller_info[strtolower(substr($fdata["half"], 0, 3)) . "_end_time"] != "00:00:00") {

				$fstarttime = strtotime($fdata["date"] . " " . $seller_info[strtolower(substr($fdata["half"], 0, 3)) . "_start_time"]);

				$fendtime = strtotime($fdata["date"] . " " . $seller_info[strtolower(substr($fdata["half"], 0, 3)) . "_end_time"]);

				$fendtimeInt = strtotime($fdata["date"] . " " . $seller_info[strtolower(substr($fdata["half"], 0, 3)) . "_end_time"])  + $interval;

				if (($now < $fstarttime) && ($now > $fendtime)) {

					$dateDb =  date('d-m-Y', strtotime($dateDb . "+1 day"));
				} else if ($now < $fstarttime) {

					$dateDb =  date('d-m-Y', strtotime($dateDb));
				} else if ($now > $fendtime) {

					$dateDb =  date('d-m-Y', strtotime($dateDb . "+1 day"));
				}

				$foutput = "";
				$fnextdelivery = "";
				$fdelivery = "";
				$k = 0;

				for ($i = $fstarttime; $i < $fendtime; $i += $interval) {
					$nntt = $i + $interval;
					if ($i < $now) {
					} else {
						if ($k == 0) {
							$nextTime = $now + $package;
							if ($i < $nextTime) {
							} else {

								$fdelivery = arabicTime(strtotime(date("h:i:s a", $i)), $languageId) . " - " . arabicTime(strtotime(date("h:i:s a", $nntt)), $languageId);

								//$sellerinfo['time_content'] = $dateDb.' '.$text_next_delivery.' +1 day '.$text_tomorrow.' '.$fdelivery;

								$sellerinfo['time_content'] = $dateDb . ' ' . date("H:i:s", $i) . "  - " . $dateDb . '  ' . date("H:i:s", $nntt);

								$k++;
							}
						}
					}
				}
			}
		}

		$dateDb =  date('d-m-Y', strtotime($timeNew[0]));

		if (empty($sellerinfo)) {
			// +2 Day			
			if ($seller_info[strtolower(substr($sdata["half"], 0, 3)) . "_start_time"] != "00:00:00" && $seller_info[strtolower(substr($sdata["half"], 0, 3)) . "_end_time"] != "00:00:00") {
				$sstarttime = strtotime($sdata["date"] . " " . $seller_info[strtolower(substr($sdata["half"], 0, 3)) . "_start_time"]) + $interval;

				$sendtime = strtotime($sdata["date"] . " " . $seller_info[strtolower(substr($sdata["half"], 0, 3)) . "_end_time"]);

				$sendtime = strtotime($sdata["date"] . " " . $seller_info[strtolower(substr($sdata["half"], 0, 3)) . "_end_time"])  + $interval;

				if (($now < $sstarttime) || ($now > $sendtime)) {

					$dateDb =  date('d-m-Y', strtotime($dateDb . "+1 day"));
				}

				$soutput = "";
				$snextdelivery = "";
				$sdelivery = "";
				$l = 0;
				for ($i = $sstarttime; $i < $sendtime; $i += $interval) {
					$nntt = $i + $interval;
					if ($i < $now) {
					} else {
						if ($l == 0) {
							$nextTime = $now + $package;
							if ($i < $nextTime) {
							} else {

								$name = strtolower($sdata["full"]);
								$sdelivery = $days[$name] . " " . $dateDb . ' ' . arabicTime(strtotime(date("h:i:s a", $i)), $languageId) . " - " . $dateDb . ' ' . arabicTime(strtotime(date("h:i:s a", $nntt)), $languageId);

								//$sellerinfo['time_content'] = $dateDb.' '.$text_next_delivery.' +2 day '.$sdelivery;		

								$sellerinfo['time_content'] = $dateDb . ' ' . date("H:i:s", $i) . " - " . $dateDb . ' ' . date("H:i:s", $nntt);

								//$sellerinfo['time_content'] = date('d-m-Y', strtotime($fdata["date"])).' '.date("H:i:s", $i)."  - ".$dateDb.'  '.date("H:i:s", $nntt);

								$l++;
							}
						}
					}
				}
			}
		}

		/* 
		if(empty($sellerinfo)){	
			// +3 Day				
			if ( $seller_info[strtolower(substr($tdata["half"], 0, 3)) . "_start_time"] != "00:00:00" && $seller_info[strtolower(substr($tdata["half"], 0, 3)) . "_end_time"] != "00:00:00" ) {
				$tstarttime = strtotime($tdata["date"]. " " . $seller_info[strtolower(substr($tdata["half"], 0, 3)) . "_start_time"]) + $interval;
				
				$tendtime = strtotime($tdata["date"]. " " . $seller_info[strtolower(substr($tdata["half"], 0, 3)) . "_end_time"]);
				
				$toutput = ""; $tnextdelivery = ""; $tdelivery = ""; $m = 0;
					
				for( $i = $tstarttime; $i < $tendtime; $i += $interval) {
					$nntt = $i + $interval;
					if( $i < $now ) {						
					} else {
						if ( $m == 0 ) {
							$nextTime = $now + $package;
							if ($i < $nextTime) {								
							} else {
								
								$name = strtolower($tdata["full"]);
								
								$tdelivery = $days[$name] . " " . arabicTime(strtotime(date("h:i:s a", $i)),$languageId)." - ".arabicTime(strtotime(date("h:i:s a", $nntt)),$languageId);
								
								//$sellerinfo['time_content'] = $dateDb.' '.$text_next_delivery.' +3 day '.$tdelivery;
								
								$sellerinfo['time_content'] = $dateDb.' '.date("H:i:s", $i)."  - ".$dateDb.'  '.date("H:i:s", $nntt);
								$m++;
							}
						} 
					}
				}
			} 
		}
		
		if(empty($sellerinfo)){	
			// +4 Day		
			if ( $seller_info[strtolower(substr($ffdata["half"], 0, 3)) . "_start_time"] != "00:00:00" && $seller_info[strtolower(substr($ffdata["half"], 0, 3)) . "_end_time"] != "00:00:00" ) {
				$ffstarttime = strtotime($ffdata["date"]. " " . $seller_info[strtolower(substr($ffdata["half"], 0, 3)) . "_start_time"]) + $interval;
				
				$ffendtime = strtotime($ffdata["date"]. " " . $seller_info[strtolower(substr($ffdata["half"], 0, 3)) . "_end_time"]);
				
				$ffoutput = ""; $ffnextdelivery = ""; $ffdelivery = ""; $n = 0;
				
				for( $i = $ffstarttime; $i < $ffendtime; $i += $interval) {
					$nntt = $i + $interval;
					if( $i < $now ) {						
					} else {
						if ( $n == 0 ) {
							$nextTime = $now + $package;
							if ($i < $nextTime) {								
							} else {
								
								$name = strtolower($ffdata["full"]);
								$ffdelivery = $days[$name] . " " . arabicTime(strtotime(date("h:i:s a", $i)),$languageId)." - ".arabicTime(strtotime(date("h:i:s a", $nntt)),$languageId);
								
								//$sellerinfo['time_content'] = $dateDb.' '.$text_next_delivery.' +4 day '.$ffdelivery;	
								
								$sellerinfo['time_content'] = $dateDb.' '.date("H:i:s", $i)."  - ".$dateDb.'  '.date("H:i:s", $nntt);
								$n++;
							}
						}
					}
				}
			} 
		}
		
		if(empty($sellerinfo)){	
			// +5 Day	
			if ( $seller_info[strtolower(substr($fffdata["half"], 0, 3)) . "_start_time"] != "00:00:00" && $seller_info[strtolower(substr($fffdata["half"], 0, 3)) . "_end_time"] != "00:00:00" ) {
				$fffstarttime = strtotime($fffdata["date"]. " " . $seller_info[strtolower(substr($fffdata["half"], 0, 3)) . "_start_time"]) + $interval;
				
				$fffendtime = strtotime($fffdata["date"]. " " . $seller_info[strtolower(substr($fffdata["half"], 0, 3)) . "_end_time"]);
				
				$fffoutput = ""; $fffnextdelivery = ""; $fffdelivery = ""; $o = 0;
				
				for( $i = $fffstarttime; $i < $fffendtime; $i += $interval) {
					$nntt = $i + $interval;
					if( $i < $now ) {						
					} else {
						if ( $o == 0 ) {
							$nextTime = $now + $package;
							if ($i < $nextTime) {								
							} else {
								
								$name = strtolower($fffdata["full"]);

								$fffdelivery = $days[$name] . " " . arabicTime(strtotime(date("h:i:s a", $i)),$languageId)." - ".arabicTime(strtotime(date("h:i:s a", $nntt)),$languageId);
								
								//$sellerinfo['time_content'] = $dateDb.' '.$text_next_delivery.' +5 day '.$fffdelivery;	
								
								$sellerinfo['time_content'] = $dateDb.' '.date("H:i:s", $i)."  - ".$dateDb.'  '.date("H:i:s", $nntt);
								
								$o++;
							}
						}					
					}
				}
			} 
		}
		
		if(empty($sellerinfo)){	
			// +6 Day
			if ( $seller_info[strtolower(substr($ssdata["half"], 0, 3)) . "_start_time"] != "00:00:00" && $seller_info[strtolower(substr($ssdata["half"], 0, 3)) . "_end_time"] != "00:00:00" ) {
				$ssstarttime = strtotime($ssdata["date"]. " " . $seller_info[strtolower(substr($ssdata["half"], 0, 3)) . "_start_time"]) + $interval;
				$ssendtime = strtotime($ssdata["date"]. " " . $seller_info[strtolower(substr($ssdata["half"], 0, 3)) . "_end_time"]);
				$ssoutput = ""; $ssnextdelivery = ""; $ssdelivery = ""; $p = 0;
				for( $i = $ssstarttime; $i < $ssendtime; $i += $interval) {
					$nntt = $i + $interval;
					if( $i < $now ) {						
					} else {
						if ( $p == 0 ) {
						$nextTime = $now + $package;
							if ($i < $nextTime) {								
							} else {
								
								$name = strtolower($ssdata["full"]);
								$ssdelivery = $days[$name] . " " . arabicTime(strtotime(date("h:i:s a", $i)),$languageId)." - ".arabicTime(strtotime(date("h:i:s a", $nntt)),$languageId);
								
								//$sellerinfo['time_content'] = $dateDb.' '.$text_next_delivery.' +6 day '.$ssdelivery;	
								
								$sellerinfo['time_content'] = $dateDb.' '.date("H:i:s", $i)."  - ".$dateDb.'  '.date("H:i:s", $nntt);
								$p++;
							}
						} 
					}
				}
			}
		} */

		return $sellerinfo;
	}

	public function sendMessageOrderPlaceIOS($mobile_user_id, $order_id, $sellerNameEn, $sellerNameAr, $language_id)
	{

		if ($language_id == 3) {
			$content = 'Your order #' . $order_id . ' has been placed at ' . $sellerNameAr;
		} else {
			$content = 'Your order #' . $order_id . ' has been placed at ' . $sellerNameEn;
		}

		// API access key from Google FCM App Console
		$API_ACCESS_KEY = "AAAAfV-61mU:APA91bGpW29mpsrMjXC3W2Nv2Eazmp0yNKIqKBl4FqsfFbb8K4XL0JOZEH2Q3wnuKjCvVRG5U7O3OffPARt9bFtCFvrI3ZrDjWWHi9A_zrcbdJuQh78RcV7bTCJHFXK-25xQsW2M3SlO";

		// 'vibrate' available in GCM, but not in FCM
		$fcmMsg = array(
			'body' => $content
		);

		$fcmFields = array(
			'to' => $mobile_user_id,
			'priority' => 'high',
			'notification' => $fcmMsg
		);

		$headers = array(
			'Authorization: key=' . $API_ACCESS_KEY,
			'Content-Type: application/json'
		);

		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, 'https://fcm.googleapis.com/fcm/send');
		curl_setopt($ch, CURLOPT_POST, true);
		curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
		curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($fcmFields));
		$result = curl_exec($ch);
		curl_close($ch);
	}

	public function sendMessageOrderPlace($mobile_user_id, $order_id, $sellerNameEn, $sellerNameAr, $language_id)
	{

		if ($language_id == 3) {
			$content = array("ar" => 'Your order #' . $order_id . ' has been placed at ' . $sellerNameAr . '.');
		} else {
			$content = array("en" => 'Your order #' . $order_id . ' has been placed at ' . $sellerNameEn . '.');
		}

		$fields = array(
			'app_id' => "c7e4b4b0-091a-42bd-a879-02b103bc2178",
			'include_player_ids' => array($mobile_user_id),
			'data' => array("foo" => "bar"),
			'contents' => $content
		);

		$fields = json_encode($fields);

		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, "https://onesignal.com/api/v1/notifications");
		curl_setopt($ch, CURLOPT_HTTPHEADER, array(
			'Content-Type: application/json; charset=utf-8',
			'Authorization: Basic NjE1MmNlOTEtNGM5MC00YmFjLWJmNjUtNTFkYTMxZGVlNzA2'
		));
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
		curl_setopt($ch, CURLOPT_HEADER, FALSE);
		curl_setopt($ch, CURLOPT_POST, TRUE);
		curl_setopt($ch, CURLOPT_POSTFIELDS, $fields);
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);

		$response = curl_exec($ch);
		curl_close($ch);
	}

	public function confirmOrderV3New()
	{
		$this->load->language('total/coupon');
		$couponMsg = $this->language->get('error_coupon');

		/* --------------------------------------- */
		$this->load->model('store/store');

		//City_id
		$city_id = 0;
		if (isset($this->request->post['city_id'])) {
			$city_id = $this->request->post['city_id'];
		}
		$this->request->post['sequence'] = $this->model_store_store->getMaxOrderSequence();
		$allProducts = $this->cart->getMobileProductsNew($this->request->post['customer_id'], $this->request->post['language_id'], '', $city_id);
		
		
		
		
		$seller_disable = 1;
		$seller_disable_name = '';
		foreach ($allProducts as $key1 => $value1) {

			$seller_info = $this->db->query("SELECT status,username FROM " . DB_PREFIX . "sellers WHERE `seller_id` = " . $key1 . "");

			if ($seller_info->row['status'] == 0) {
				$seller_disable = 0;
				$seller_disable_name = $seller_info->row['username'];
			}

			$total = 0;
			$storeId = $key1;
			$sellerInfo = $this->model_store_store->getSellerByID($storeId);
			$storeIds[$key1] = $sellerInfo;

			$deliveryCharges = (isset($sellerInfo) && $sellerInfo['delivery_charges']) ? (int) $sellerInfo['delivery_charges'] : 0;

			foreach ($value1 as $key => $value) {
				$total = $total + $value['total'];
			}
			
			$totals_check[$key1] = $total;
			$allProductsNew[] = array(
				'seller_id'        => $key1,
				'seller_nameen'    => $sellerInfo['firstname'],
				'seller_namear'    => $sellerInfo['lastname'],
				'delivery_charges' => $deliveryCharges,
				'minimum_order'    => (int) $sellerInfo['minimum_order'],
				'seller_total'     => $total,
				'store_credit'     => $this->currency->format($amount, $this->config->get('config_currency')),
				'products'         => $value1
			);
		}

		foreach ($storeIds as $store_id => $value) {
			//echo $store_id;
			if ($value['minimum_order'] > $totals_check[$store_id]) {
				$output = array("message" => 'pls minimum order total check');
				echo json_encode($output);
				exit;
			}
		}
		$order_data = array();

		$order_data['totals'] = array();
		$total = 0;

		//$cart_info = $this->db->query("SELECT * FROM " . DB_PREFIX . "cart WHERE customer_id = '" . $this->request->post['customer_id'] . "'");

		$balance = $this->db->query("SELECT SUM(amount) AS total FROM " . DB_PREFIX . "customer_transaction WHERE customer_id = '" . $this->request->post['customer_id'] . "'");

		foreach ($balance->rows as $balance_total) {
			$credit_total = $balance_total['total'];
		}

		$isCoupon = true;
		//if (count($cart_info->rows) > 0) {
		if ($seller_disable) {
			if (count($allProductsNew) > 0) {

				$voucher = 0;

				if (isset($this->request->post['coupon']) && $this->request->post['coupon'] != 'undefined' && $this->request->post['coupon'] != 'null') {

					$this->load->model('total/coupon');

					$coupon = $this->request->post['coupon'];

					$customer = (int) $this->request->post['customer_id'];
					$device = $this->request->post['device_id'];
					$language = 1;

					$coupon_info = $this->model_total_coupon->getMobileCoupon($coupon, $customer, $language, $device);

					$isCoupon = ($coupon_info) ? true : false;

					$voucher_info = $this->model_total_coupon->getMobileTotalNew($coupon, $customer, $language, $device, $allProductsNew[0]['seller_id']);
					$couponId = $coupon_info['coupon_id'];

					if ($voucher_info) {
						$voucher = $voucher_info[0]['value'];
					}
				}

				if ($isCoupon) {
					$orderTotal = 0;
					foreach ($allProductsNew as $cart_infoNew) {

						$i     = 0;
						$total = 0;

						//$seller_id = (isset($this->request->post['seller_id'])) ? $this->request->post['seller_id'] : 1;
						$seller_id = $cart_infoNew['seller_id'];

						$this->load->model('catalog/product');

						$this->load->language('checkout/checkout');

						$order_data['invoice_prefix'] = $this->config->get('config_invoice_prefix');
						$order_data['store_id']       = $this->config->get('config_store_id');
						$order_data['store_name']     = $this->config->get('config_name') . " - Mobile App";
						$order_data['seller_id']      = $seller_id;

						if ($order_data['store_id']) {
							$order_data['store_url'] = $this->config->get('config_url');
						} else {
							$order_data['store_url'] = HTTP_SERVER;
						}

						$this->load->model('account/customer');

						$customer_info = $this->model_account_customer->getCustomer($this->request->post['customer_id']); //******************************************8

						$address_info = $this->db->query("SELECT DISTINCT * FROM " . DB_PREFIX . "address WHERE address_id = '" . (int) $customer_info['address_id'] . "' AND customer_id = '" . (int) $customer_info['customer_id'] . "'");

						$this->load->model('localisation/zone');
						$this->load->model('localisation/country');

						$zone_info    = $this->model_localisation_zone->getZone($address_info->row['zone_id']);
						$country_info = $this->model_localisation_country->getCountry($address_info->row['country_id']);

						$order_data['customer_id']       = $this->request->post['customer_id'];          //******************************************8
						$order_data['customer_group_id'] = $customer_info['customer_group_id'];
						$order_data['firstname']         = $customer_info['firstname'];
						$order_data['lastname']          = $customer_info['lastname'];
						$order_data['email']             = $customer_info['email'];
						$order_data['telephone']         = $customer_info['telephone'];
						$order_data['fax']               = $customer_info['fax'];
						$order_data['custom_field']      = unserialize($customer_info['custom_field']);

						if (isset($zone_info['name'])) {
							$payment_zone = $zone_info['name'];
						} else {
							$payment_zone = '';
						}

						$order_data['payment_firstname']      = $customer_info['firstname'];
						$order_data['payment_lastname']       = $customer_info['lastname'];
						$order_data['payment_company']        = $address_info->row['company'];
						$order_data['payment_address_1']      = $address_info->row['address_1'];
						$order_data['payment_address_2']      = $address_info->row['address_2'];
						$order_data['payment_city']           = $address_info->row['city'];
						$order_data['payment_postcode']       = $address_info->row['postcode'];
						$order_data['payment_zone']           = $payment_zone;
						$order_data['payment_zone_id']        = $address_info->row['zone_id'];
						$order_data['payment_country']        = $country_info['name'];
						$order_data['payment_country_id']     = $address_info->row['country_id'];
						$order_data['payment_address_format'] = '';
						$order_data['payment_custom_field']   = '';
						$order_data['payment_method']         = $this->request->post['payment_method'];
						$order_data['payment_code']           = $this->request->post['payment_code'];

						$zoneId       = getZoneId($this->request->post['input-shipping-zone']);
						$zone_info    = $this->model_localisation_zone->getZone($zoneId);
						$country_info = $this->model_localisation_country->getCountry($this->request->post['input-shipping-country']);

						if (isset($zone_info['name'])) {
							$shipping_zone = $zone_info['name'];
						} else {
							$shipping_zone = '';
						}

						$order_data['shipping_firstname']      = $this->request->post['input-shipping-firstname'];
						$order_data['shipping_lastname']       = $this->request->post['input-shipping-lastname'];
						$order_data['shipping_company']        = $this->request->post['input-shipping-company'];
						$order_data['shipping_address_1']      = $this->request->post['input-shipping-address-1'];
						$order_data['shipping_address_2']      = $this->request->post['input-shipping-address-2'];
						$order_data['shipping_city']           = $this->request->post['input-shipping-city'];
						$order_data['shipping_postcode']       = $this->request->post['input-shipping-postcode'];
						$order_data['shipping_zone']           = $shipping_zone;
						$order_data['shipping_zone_id']        = $zoneId;
						$order_data['shipping_country']        = $country_info['name'];
						$order_data['shipping_country_id']     = $this->request->post['input-shipping-country'];
						$order_data['shipping_address_format'] = "";
						$order_data['shipping_custom_field']   = array();
						//$order_data['shipping_method']         = $shipping_method;
						$order_data['shipping_method'] = '';
						$order_data['shipping_code']   = $this->request->post['shipping_code'];
						$order_data['products']        = array();

						if (isset($_GET['input-default-address'])) {
							$this->db->query("UPDATE " . DB_PREFIX . "address SET firstname = '" . $this->db->escape($this->request->post['input-shipping-firstname']) . "', lastname = '" . $this->db->escape($this->request->post['input-shipping-lastname']) . "', company = '" . $this->db->escape($this->request->post['input-shipping-company']) . "', address_1 = '" . $this->db->escape($this->request->post['input-shipping-address-1']) . "', address_2 = '" . $this->db->escape($this->request->post['input-shipping-address-2']) . "', postcode = '" . $this->db->escape($this->request->post['input-shipping-postcode']) . "', city = '" . $this->db->escape($this->request->post['input-shipping-city']) . "', zone_id = '" . (int) $zoneId . "', country_id = '" . (int) $this->request->post['input-shipping-country'] . "' WHERE address_id  = '" . (int) $this->request->post['address_id'] . "' AND customer_id = '" . (int) $this->request->post['customer_id'] . "'");
						}

						$i = 0;
						//foreach ($cart_info->rows as $cart_data) {
						foreach ($cart_infoNew['products'] as $cart_data) {

							$prod_info = $this->model_catalog_product->getProduct($cart_data['product_id'], $seller_id);

							$option_price = 0;
							$option_points = 0;
							$option_weight = 0;

							$option_data = array();

							foreach (json_decode($cart_data['option']) as $product_option_id => $value) {

								$option_query = $this->db->query("SELECT po.product_option_id, po.option_id, od.name, o.type FROM " . DB_PREFIX . "product_option po LEFT JOIN `" . DB_PREFIX . "option` o ON (po.option_id = o.option_id) LEFT JOIN " . DB_PREFIX . "option_description od ON (o.option_id = od.option_id) WHERE po.product_option_id = '" . (int) $product_option_id . "' AND po.product_id = '" . (int) $cart_data['product_id'] . "' AND od.language_id = '" . (int) $this->request->post['language_id'] . "'");

								if ($option_query->num_rows) {

									if ($option_query->row['type'] == 'select' || $option_query->row['type'] == 'radio' || $option_query->row['type'] == 'image') {

										$option_value_query = $this->db->query("SELECT pov.option_value_id, ovd.name, pov.quantity, pov.subtract, pov.price, pov.price_prefix, pov.points, pov.points_prefix, pov.weight, pov.weight_prefix FROM " . DB_PREFIX . "product_option_value pov LEFT JOIN " . DB_PREFIX . "option_value ov ON (pov.option_value_id = ov.option_value_id) LEFT JOIN " . DB_PREFIX . "option_value_description ovd ON (ov.option_value_id = ovd.option_value_id) WHERE pov.product_option_value_id = '" . (int) $value . "' AND pov.product_option_id = '" . (int) $product_option_id . "' AND pov.seller_id = '" . (int) $seller_id . "' AND ovd.language_id = '" . (int) $this->request->post['language_id'] . "'");

										if ($option_value_query->num_rows) {
											if ($option_value_query->row['price_prefix'] == '+') {
												$option_price += $option_value_query->row['price'];
											} elseif ($option_value_query->row['price_prefix'] == '-') {
												$option_price -= $option_value_query->row['price'];
											}

											if ($option_value_query->row['points_prefix'] == '+') {
												$option_points += $option_value_query->row['points'];
											} elseif ($option_value_query->row['points_prefix'] == '-') {
												$option_points -= $option_value_query->row['points'];
											}

											if ($option_value_query->row['weight_prefix'] == '+') {
												$option_weight += $option_value_query->row['weight'];
											} elseif ($option_value_query->row['weight_prefix'] == '-') {
												$option_weight -= $option_value_query->row['weight'];
											}

											if ($option_value_query->row['subtract'] && (!$option_value_query->row['quantity'] || ($option_value_query->row['quantity'] < $cart_data['quantity']))) {
												$stock = false;
											}

											$option_data[] = array(
												'product_option_id'       => $product_option_id,
												'product_option_value_id' => $value,
												'option_id'               => $option_query->row['option_id'],
												'option_value_id'         => $option_value_query->row['option_value_id'],
												'name'                    => $option_query->row['name'],
												'value'                   => $option_value_query->row['name'],
												'type'                    => $option_query->row['type'],
												'quantity'                => $option_value_query->row['quantity'],
												'subtract'                => $option_value_query->row['subtract'],
												'price'                   => $option_value_query->row['price'],
												'price_prefix'            => $option_value_query->row['price_prefix'],
												'points'                  => $option_value_query->row['points'],
												'points_prefix'           => $option_value_query->row['points_prefix'],
												'weight'                  => $option_value_query->row['weight'],
												'weight_prefix'           => $option_value_query->row['weight_prefix']
											);
										}
									} elseif ($option_query->row['type'] == 'checkbox' && is_array($value)) {
										foreach ($value as $product_option_value_id) {
											$option_value_query = $this->db->query("SELECT pov.option_value_id, ovd.name, pov.quantity, pov.subtract, pov.price, pov.price_prefix, pov.points, pov.points_prefix, pov.weight, pov.weight_prefix FROM " . DB_PREFIX . "product_option_value pov LEFT JOIN " . DB_PREFIX . "option_value ov ON (pov.option_value_id = ov.option_value_id) LEFT JOIN " . DB_PREFIX . "option_value_description ovd ON (ov.option_value_id = ovd.option_value_id) WHERE pov.product_option_value_id = '" . (int) $product_option_value_id . "' AND pov.product_option_id = '" . (int) $product_option_id . "' AND ovd.language_id = '" . (int) $this->request->post['language_id'] . "'");

											if ($option_value_query->num_rows) {
												if ($option_value_query->row['price_prefix'] == '+') {
													$option_price += $option_value_query->row['price'];
												} elseif ($option_value_query->row['price_prefix'] == '-') {
													$option_price -= $option_value_query->row['price'];
												}

												if ($option_value_query->row['points_prefix'] == '+') {
													$option_points += $option_value_query->row['points'];
												} elseif ($option_value_query->row['points_prefix'] == '-') {
													$option_points -= $option_value_query->row['points'];
												}

												if ($option_value_query->row['weight_prefix'] == '+') {
													$option_weight += $option_value_query->row['weight'];
												} elseif ($option_value_query->row['weight_prefix'] == '-') {
													$option_weight -= $option_value_query->row['weight'];
												}

												if ($option_value_query->row['subtract'] && (!$option_value_query->row['quantity'] || ($option_value_query->row['quantity'] < $cart_data['quantity']))) {
													$stock = false;
												}

												$option_data[] = array(
													'product_option_id'       => $product_option_id,
													'product_option_value_id' => $product_option_value_id,
													'option_id'               => $option_query->row['option_id'],
													'option_value_id'         => $option_value_query->row['option_value_id'],
													'name'                    => $option_query->row['name'],
													'value'                   => $option_value_query->row['name'],
													'type'                    => $option_query->row['type'],
													'quantity'                => $option_value_query->row['quantity'],
													'subtract'                => $option_value_query->row['subtract'],
													'price'                   => $option_value_query->row['price'],
													'price_prefix'            => $option_value_query->row['price_prefix'],
													'points'                  => $option_value_query->row['points'],
													'points_prefix'           => $option_value_query->row['points_prefix'],
													'weight'                  => $option_value_query->row['weight'],
													'weight_prefix'           => $option_value_query->row['weight_prefix']
												);
											}
										}
									} elseif ($option_query->row['type'] == 'text' || $option_query->row['type'] == 'textarea' || $option_query->row['type'] == 'file' || $option_query->row['type'] == 'date' || $option_query->row['type'] == 'datetime' || $option_query->row['type'] == 'time') {
										$option_data[] = array(
											'product_option_id'       => $product_option_id,
											'product_option_value_id' => '',
											'option_id'               => $option_query->row['option_id'],
											'option_value_id'         => '',
											'name'                    => $option_query->row['name'],
											'value'                   => $value,
											'type'                    => $option_query->row['type'],
											'quantity'                => '',
											'subtract'                => '',
											'price'                   => '',
											'price_prefix'            => '',
											'points'                  => '',
											'points_prefix'           => '',
											'weight'                  => '',
											'weight_prefix'           => ''
										);
									}
								}
							}

							if ($prod_info['special'] == null) {
								$total = ($total + (($prod_info['price'] + $option_price) * $cart_data['quantity']));
							} else {
								$total = ($total + (($prod_info['special'] + $option_price) * $cart_data['quantity']));
							}


							if ($prod_info['special'] == null) {
								$order_data['products'][] = array(
									'product_id' => $prod_info['product_id'],
									'name'       => $prod_info['name'],
									'model'      => $prod_info['model'],
									'option'     => $option_data,
									'download'   => array(),
									'quantity'   => $cart_data['quantity'],
									'subtract'   => $prod_info['subtract'],
									'price'      => ($prod_info['price'] + $option_price),
									'total'      => ($prod_info['price'] + $option_price) * $cart_data['quantity'],
									'tax'        => 0,
									'reward'     => 0,
									'seller_id'  => $seller_id,
								);
								$i++;
							} else {
								$order_data['products'][] = array(
									'product_id' => $prod_info['product_id'],
									'name'       => $prod_info['name'],
									'model'      => $prod_info['model'],
									'option'     => $option_data,
									'download'   => array(),
									'quantity'   => $cart_data['quantity'],
									'subtract'   => $prod_info['subtract'],
									'price'      => ($prod_info['special'] + $option_price),
									'total'      => ($prod_info['special'] + $option_price) * $cart_data['quantity'],
									'tax'        => 0,
									'reward'     => 0,
									'seller_id'  => $seller_id,
								);
								$i++;
							}
						}

						// Gift Voucher
						$order_data['vouchers'] = array();

						if (!empty($this->session->data['vouchers'])) {
							foreach ($this->session->data['vouchers'] as $voucher) {
								$order_data['vouchers'][] = array(
									'description'      => $voucher['description'],
									'code'             => substr(md5(mt_rand()), 0, 10),
									'to_name'          => $voucher['to_name'],
									'to_email'         => $voucher['to_email'],
									'from_name'        => $voucher['from_name'],
									'from_email'       => $voucher['from_email'],
									'voucher_theme_id' => $voucher['voucher_theme_id'],
									'message'          => $voucher['message'],
									'amount'           => $voucher['amount'],
								);
							}
						}

						$deliveryCharges = 0;
						$deliveryCharges = $cart_infoNew['delivery_charges'];

						$deliveryChargesArr = array();

						$deliveryChargesArr[] = array(
							'delivery_charges' => $deliveryCharges,
							'seller_id'        => $seller_id,
							'firstname'        => $cart_infoNew['seller_nameen'],
							'lastname'         => $cart_infoNew['seller_namear']
						);

						$this->session->data['deliveryChargesArr'] = '';
						$this->session->data['deliveryChargesArr'] = $deliveryChargesArr;

						$order_data['comment'] = $this->request->post['order_comments'];
						$order_data['total']   = $total;

						if (isset($this->request->cookie['tracking'])) {
							$order_data['tracking'] = $this->request->cookie['tracking'];

							$subtotal = $this->cart->getSubTotal();

							// Affiliate
							$this->load->model('affiliate/affiliate');

							$affiliate_info = $this->model_affiliate_affiliate->getAffiliateByCode($this->request->cookie['tracking']);

							if ($affiliate_info) {
								$order_data['affiliate_id'] = $affiliate_info['affiliate_id'];
								$order_data['commission'] = ($subtotal / 100) * $affiliate_info['commission'];
							} else {
								$order_data['affiliate_id'] = 0;
								$order_data['commission'] = 0;
							}

							// Marketing
							$this->load->model('checkout/marketing');

							$marketing_info = $this->model_checkout_marketing->getMarketingByCode($this->request->cookie['tracking']);

							if ($marketing_info) {
								$order_data['marketing_id'] = $marketing_info['marketing_id'];
							} else {
								$order_data['marketing_id'] = 0;
							}
						} else {
							$order_data['affiliate_id'] = 0;
							$order_data['commission']   = 0;
							$order_data['marketing_id'] = 0;
							$order_data['tracking']     = '';
						}

						$order_data['language_id']    = $this->request->post['language_id'];
						$order_data['currency_id']    = $this->currency->getId();
						$order_data['currency_code']  = $this->currency->getCode();
						$order_data['currency_value'] = $this->currency->getValue($this->currency->getCode());
						$order_data['ip']             = $this->request->server['REMOTE_ADDR'];

						if (!empty($this->request->server['HTTP_X_FORWARDED_FOR'])) {
							$order_data['forwarded_ip'] = $this->request->server['HTTP_X_FORWARDED_FOR'];
						} elseif (!empty($this->request->server['HTTP_CLIENT_IP'])) {
							$order_data['forwarded_ip'] = $this->request->server['HTTP_CLIENT_IP'];
						} else {
							$order_data['forwarded_ip'] = '';
						}

						if (isset($this->request->server['HTTP_USER_AGENT'])) {
							$order_data['user_agent'] = $this->request->server['HTTP_USER_AGENT'];
						} else {
							$order_data['user_agent'] = '';
						}

						if (isset($this->request->server['HTTP_ACCEPT_LANGUAGE'])) {
							$order_data['accept_language'] = $this->request->server['HTTP_ACCEPT_LANGUAGE'];
						} else {
							$order_data['accept_language'] = '';
						}

						$total_data = array();

						$total_data[0] = array(
							'code'       => 'sub_total',
							'title'      => 'Sub-Total',
							'text'       => 'Sub-Total',
							'value'      => $total,
							'sort_order' => 0,
						);

						/* $total_data[1] = array(
							'code' => 'vat',
							'title' => $this->request->post['vat_text'],
							'value' => $this->request->post['vat_value'],
							'sort_order' => 1,
						); */

						$vat       = (float) $this->config->get('config_vat');
						$vat_total = 0;
						$vat_total = $total * $vat / ($vat + 100);

						$total_data[1] = array(
							'code'       => 'vat',
							'title'      => "Value Added Tax (".$vat."%)",
							'value'      => $vat_total,
							'sort_order' => 1,
						);


						$isCouponRequired = false;
						if ((isset($voucher)) && ($voucher > 0)) {
							$isCouponRequired = true;
							//$total = $voucher_info[1];

							$total -= $voucher;

							$total_data[3] = array(
								'code' => 'coupon',
								'title' => $voucher_info[0]['title'],
								'value' => -$voucher,
								'sort_order' => 3,
							);
						}

						$this->load->model('checkout/order');
						if ($this->config->get('credit_status')) {
							$this->load->language('total/credit');

							if ((float) $credit_total) {
								if ($credit_total > $total) {
									$credit = $total;
								} else {
									$credit = $credit_total;
								}

								// if ($credit > 0) {
								$total -= $credit;
								$total_data[4] = array(
									'code' => 'credit',
									'title' => 'Store Credit',
									'value' => -$credit,
									'sort_order' => 4,
								);

								//}

								if ($credit_total > $credit) {
									$credit_total = $credit_total - $credit;
								} else {
									$credit_total = $credit - $credit_total;
								}
							}
						}

						$sellerInfo_custom = $this->model_store_store->getSellerByID($seller_id);
						if ($sellerInfo_custom['free_delivery_limit'] > $total or $sellerInfo_custom['free_delivery_limit'] == 0) {
							$total_data[5] = array(
								'code' => 'total',
								'title' => 'Total',
								'value' => $total + $deliveryCharges,
								'sort_order' => 5,
							);
						} else {
							$total_data[5] = array(
								'code' => 'total',
								'title' => 'Total',
								'value' => $total,
								'sort_order' => 5,
							);
						}
						// $total_data[5] = array(
						// 'code' => 'total',
						// 'title' => 'Total',
						// 'value' => $total + $deliveryCharges,
						// 'sort_order' => 5,
			
						$order_data['totals'] = $total_data;
					// vat 
						
						foreach($order_data['totals'] as $key=>$totals_data){
							if($totals_data['code']	== 'vat'){
								$vat =  (float)$this->config->get('config_vat');
								
								if(isset($vat) && $vat > 0){
								   $this->load->model('total/vat');
									$value = $this->{'model_total_vat'}->getTotalvalue($total, $vat);
									$order_data['totals'][$key]['value'] = round($value,2);
								}			
							}

						}// vat 
						
						foreach($order_data['totals'] as $key=>$totals_data){
							if($totals_data['code']	== 'vat'){
								$vat =  (float)$this->config->get('config_vat');
								
								if(isset($vat) && $vat > 0){
								   $this->load->model('total/vat');
									$value = $this->{'model_total_vat'}->getTotalvalue($total, $vat);
									$order_data['totals'][$key]['value'] = round($value,2);
								}			
							}

						}
						//debugging($order_data);

						/* echo "<pre>";
						print_r($order_data);
						exit; */

						$sqlAll = "SELECT s.seller_id, s.firstname, s.lastname, s.package_ready, s.delivery_timegap, s.mon_start_time, s.mon_end_time, s.tue_start_time, s.tue_end_time, s.wed_start_time, s.wed_end_time, s.thu_start_time, s.thu_end_time, s.fri_start_time, s.fri_end_time, s.sat_start_time, s.sat_end_time, s.sun_start_time, s.sun_end_time, s.sortorder FROM " . DB_PREFIX . "sellers s WHERE s.seller_id = '" . $seller_id . "'";

						$queryAll = $this->db->query($sqlAll);
						$seller_info = $queryAll->row;
						
						$delivery_time = $this->request->post['delivery_time'];
						if($delivery_time == 'As soon as possible'){
							$delivery_time = date("h:i a");
						}
						$delivery_time_post = $this->getOrderNextDeliverySlot($seller_info,$delivery_time);

						if (empty($delivery_time_post)) {
							$delivery_time_post = $this->request->post['delivery_time'];
						}

						$order_data['sequence'] = $this->request->post['sequence'];
						//store_area_id
						$order_data['store_area_id'] =0;
						if(isset($this->request->post['store_area_id'])){
							if($this->request->post['store_area_id'] != ''){
								$order_data['store_area_id'] = $this->request->post['store_area_id'];
							}
						}
						//store_area_id
											
						$order_id = $this->model_checkout_order->addOrderNew($order_data);
						// post data Write 
						//print_r($order_id);
						
						$myfile = fopen("orderData/" . $order_id . ".txt", "w") or die("Unable to open file!");
						
						//$txt = "";
						$txt = "confirmOrderV3Newtest";
						foreach ($this->request->post as $keyNew => $valueNew) {
							$txt .= "\n" . $keyNew . "=" . $valueNew;
						}
						foreach ($order_data['products'] as $product_history) {
							$txt .= "\n \n product id = " . $product_history['product_id'] . " and quantity=" . $product_history['quantity'] . " and price =" . $product_history['price'] . " and option = " . $product_history['option'] . " and total= " . $product_history['total'];
						}
						foreach ($order_data['totals'] as $cart_total) {
							$txt .= "\n \n \n title = " . $cart_total['title'] . " and value=" . $cart_total['value'] . " and code =" . $cart_total['code'];
						}
						fwrite($myfile, $txt);
						fclose($myfile);


						// Add to activity log
						$this->load->model('account/activity');

						$activity_data = array(
							'customer_id' => $customer_info['customer_id'],
							'name' => $customer_info['firstname'] . ' ' . $customer_info['lastname'],
							'order_id' => $order_id,
						);

						$this->model_account_activity->addActivity('order_account', $activity_data);

						$this->load->model('checkout/order');
						$order_info = $this->model_checkout_order->getOrder($order_id);

						$order_total_query = $this->db->query("SELECT * FROM `" . DB_PREFIX . "order_total` WHERE order_id = '" . (int) $order_id . "' ORDER BY sort_order ASC");

						foreach ($order_total_query->rows as $order_total) {
							$this->load->model('total/' . $order_total['code']);

							if (method_exists($this->{'model_total_' . $order_total['code']}, 'confirm')) {
								$this->{'model_total_' . $order_total['code']}->confirm($order_info, $order_total);
							}
						}

						/* $output[] = array(
							"message" => true,
							"orderid" => $order_id,
							"total" => $total + $deliveryCharges,
							"payment_firstname" => $order_data['payment_firstname'],
							"payment_lastname" => $order_data['payment_lastname'],
							"email" => $order_data['email'],
							"telephone" => $order_data['telephone'],
							"address_1" => $order_data['payment_address_1'],
							"address_2" => $order_data['payment_address_2'],
						); */

						$orderTotal = $orderTotal + ($total + $deliveryCharges);

						$this->load->model('module/deliveryTime/order');
						$this->model_module_deliveryTime_order->addDeliveryTimeV2($order_id, $delivery_time_post);

						try {
							$this->model_checkout_order->addOrderHistoryNew($order_id, 1, $order_data['comment'], true);
						} catch (Exception $e) {
							//print_r($e);
							$output = array(
								"message" => 'Error in order Place.',
							);
							echo json_encode($output);
						}

						// Add Coupon History
						if ($isCouponRequired) {
							$this->model_total_coupon->couponHistory($couponId, $order_id, $customer, $voucher);
						}

						//coupon blank 
						$voucher = 0;

						//Notification Send Start
						//customer other device_id in notification Send
						$notifications = array();
						$notifications = $this->model_checkout_order->sendCustomerNotification($customer_info['customer_id']);

						if (count($notifications) > 0) {

							foreach ($notifications as $notification) {
								if ($notification['device_type'] == 1) {

									$this->sendMessageOrderPlace($notification['device_id'], $order_id, $seller_info['firstname'], $seller_info['lastname'], $this->request->post['language_id']);
								} else if ($notification['device_type'] == 2) {

									$this->sendMessageOrderPlaceIOS($notification['device_id'], $order_id, $seller_info['firstname'], $seller_info['lastname'], $this->request->post['language_id']);
								}
							}
						}
						//Notification Send end */
					}
					$myfile = fopen("orderData/" . $order_id . "_return.txt", "w") or die("Unable to open file!");
					$txt = "";
					$txt .= "\n total =" . $orderTotal;
					fwrite($myfile, $txt);
					fclose($myfile);


					$output = array(
						"message" => true,
						"total" => $orderTotal
					);
				} else {
					$output = array("message" => $couponMsg);
				}

				/* --------------------------------------- */
			} else {
				$output = array(
					"message" => 'No product found in cart',
				);
			}
		} else {
			$output = array(
				"message" => 'Seller Disabled ' . $seller_disable_name,
			);
		}

		echo json_encode($output);
	}


	//// new api only order
	public function confirmOrderV4()
	{
		
		//echo'ok';
		$this->load->language('total/coupon');
		$couponMsg = $this->language->get('error_coupon');
		$multi_order_id = array();
		$customer = 0;
		/* --------------------------------------- */
		$this->load->model('store/store');

		//City_id		
		$city_id = 0;
		if (isset($this->request->post['city_id'])) {
			$city_id = $this->request->post['city_id'];
		}

		$this->request->post['sequence'] = $this->model_store_store->getMaxOrderSequence();

		$allProducts = $this->cart->getMobileProductsNew($this->request->post['customer_id'], $this->request->post['language_id'], '', $city_id);

		$seller_disable = 1;
		$seller_disable_name = '';
		foreach ($allProducts as $key1 => $value1) {
			$seller_info = $this->db->query("SELECT status,username FROM " . DB_PREFIX . "sellers WHERE `seller_id` = " . $key1 . "");

			if ($seller_info->row['status'] == 0) {
				$seller_disable = 0;
				$seller_disable_name = $seller_info->row['username'];
			}

			$total = 0;
			$storeId = $key1;
			$sellerInfo = $this->model_store_store->getSellerByID($storeId);

			$deliveryCharges = (isset($sellerInfo) && $sellerInfo['delivery_charges']) ? (int) $sellerInfo['delivery_charges'] : 0;

			foreach ($value1 as $key => $value) {
				$total = $total + $value['total'];
			}

			$storeIds[$key1] = $sellerInfo;
			$totals_check[$key1] = $total;
			$allProductsNew[] = array(
				'seller_id'       => $key1,
				'seller_nameen'   => $sellerInfo['firstname'],
				'seller_namear'   => $sellerInfo['lastname'],
				'delivery_charges' => $deliveryCharges,
				'minimum_order'    => (int) $sellerInfo['minimum_order'],
				'seller_total' => $total,
				'store_credit'     => $this->currency->format($amount, $this->config->get('config_currency')),
				'products'         => $value1
			);
		}

		foreach ($storeIds as $store_id => $value) {
			//echo $store_id;
			if ($value['minimum_order'] > $totals_check[$store_id]) {
				$output = array("message" => 'pls minimum order total check');
				echo json_encode($output);
				exit;
			}
		}

		$order_data = array();

		$order_data['totals'] = array();
		$total = 0;

		//$cart_info = $this->db->query("SELECT * FROM " . DB_PREFIX . "cart WHERE customer_id = '" . $this->request->post['customer_id'] . "'");

		$balance = $this->db->query("SELECT SUM(amount) AS total FROM " . DB_PREFIX . "customer_transaction WHERE customer_id = '" . $this->request->post['customer_id'] . "'");

		foreach ($balance->rows as $balance_total) {
			$credit_total = $balance_total['total'];
		}

		$isCoupon = true;
		//if (count($cart_info->rows) > 0) {
		if ($seller_disable) {

			if (count($allProductsNew) > 0) {

				$voucher = 0;

				if (isset($this->request->post['coupon']) && $this->request->post['coupon'] != 'undefined' && $this->request->post['coupon'] != 'null') {

					$this->load->model('total/coupon');

					$coupon = $this->request->post['coupon'];

					$customer = (int) $this->request->post['customer_id'];
					$device = $this->request->post['device_id'];
					$language = 1;

					$coupon_info = $this->model_total_coupon->getMobileCoupon($coupon, $customer, $language, $device);

					$isCoupon = ($coupon_info) ? true : false;

					$voucher_info = $this->model_total_coupon->getMobileTotalNew($coupon, $customer, $language, $device, $allProductsNew[0]['seller_id']);
					$couponId = $coupon_info['coupon_id'];

					if ($voucher_info) {
						$voucher = $voucher_info[0]['value'];
					}
				}

				if ($isCoupon) {
					$orderTotal = 0;
					foreach ($allProductsNew as $cart_infoNew) {

						$i = 0;
						$total = 0;

						//$seller_id = (isset($this->request->post['seller_id'])) ? $this->request->post['seller_id'] : 1;
						$seller_id = $cart_infoNew['seller_id'];

						$this->load->model('catalog/product');

						$this->load->language('checkout/checkout');

						$order_data['invoice_prefix'] = $this->config->get('config_invoice_prefix');
						$order_data['store_id'] = $this->config->get('config_store_id');
						$order_data['store_name'] = $this->config->get('config_name') . " - Mobile App";
						$order_data['seller_id'] = $seller_id;

						if ($order_data['store_id']) {
							$order_data['store_url'] = $this->config->get('config_url');
						} else {
							$order_data['store_url'] = HTTP_SERVER;
						}

						$this->load->model('account/customer');

						$customer_info = $this->model_account_customer->getCustomer($this->request->post['customer_id']); //******************************************8

						$address_info = $this->db->query("SELECT DISTINCT * FROM " . DB_PREFIX . "address WHERE address_id = '" . (int) $customer_info['address_id'] . "' AND customer_id = '" . (int) $customer_info['customer_id'] . "'");

						$this->load->model('localisation/zone');
						$this->load->model('localisation/country');

						$zone_info = $this->model_localisation_zone->getZone($address_info->row['zone_id']);
						$country_info = $this->model_localisation_country->getCountry($address_info->row['country_id']);

						$order_data['customer_id'] = $this->request->post['customer_id']; //******************************************8
						$order_data['customer_group_id'] = $customer_info['customer_group_id'];
						$order_data['firstname'] = $customer_info['firstname'];
						$order_data['lastname'] = $customer_info['lastname'];
						$order_data['email'] = $customer_info['email'];
						$order_data['telephone'] = $customer_info['telephone'];
						$order_data['fax'] = $customer_info['fax'];
						$order_data['custom_field'] = unserialize($customer_info['custom_field']);

						if (isset($zone_info['name'])) {
							$payment_zone = $zone_info['name'];
						} else {
							$payment_zone = '';
						}

						$order_data['payment_firstname'] = $customer_info['firstname'];
						$order_data['payment_lastname'] = $customer_info['lastname'];
						$order_data['payment_company'] = $address_info->row['company'];
						$order_data['payment_address_1'] = $address_info->row['address_1'];
						$order_data['payment_address_2'] = $address_info->row['address_2'];
						$order_data['payment_city'] = $address_info->row['city'];
						$order_data['payment_postcode'] = $address_info->row['postcode'];
						$order_data['payment_zone'] = $payment_zone;
						$order_data['payment_zone_id'] = $address_info->row['zone_id'];
						$order_data['payment_country'] = $country_info['name'];
						$order_data['payment_country_id'] = $address_info->row['country_id'];
						$order_data['payment_address_format'] = '';
						$order_data['payment_custom_field'] = '';
						$order_data['payment_method'] = $this->request->post['payment_method'];
						$order_data['payment_code'] = $this->request->post['payment_code'];

						$zoneId = getZoneId($this->request->post['input-shipping-zone']);
						$zone_info = $this->model_localisation_zone->getZone($zoneId);
						$country_info = $this->model_localisation_country->getCountry($this->request->post['input-shipping-country']);

						if (isset($zone_info['name'])) {
							$shipping_zone = $zone_info['name'];
						} else {
							$shipping_zone = '';
						}

						$order_data['shipping_firstname'] = $this->request->post['input-shipping-firstname'];
						$order_data['shipping_lastname'] = $this->request->post['input-shipping-lastname'];
						$order_data['shipping_company'] = $this->request->post['input-shipping-company'];
						$order_data['shipping_address_1'] = $this->request->post['input-shipping-address-1'];
						$order_data['shipping_address_2'] = $this->request->post['input-shipping-address-2'];
						$order_data['shipping_city'] = $this->request->post['input-shipping-city'];
						$order_data['shipping_postcode'] = $this->request->post['input-shipping-postcode'];
						$order_data['shipping_zone'] = $shipping_zone;
						$order_data['shipping_zone_id'] = $zoneId;
						$order_data['shipping_country'] = $country_info['name'];
						$order_data['shipping_country_id'] = $this->request->post['input-shipping-country'];
						$order_data['shipping_address_format'] = "";
						$order_data['shipping_custom_field'] = array();
						//$order_data['shipping_method']         = $shipping_method;
						$order_data['shipping_method'] = '';
						$order_data['shipping_code'] = $this->request->post['shipping_code'];

						$order_data['products'] = array();

						if (isset($_GET['input-default-address'])) {
							$this->db->query("UPDATE " . DB_PREFIX . "address SET firstname = '" . $this->db->escape($this->request->post['input-shipping-firstname']) . "', lastname = '" . $this->db->escape($this->request->post['input-shipping-lastname']) . "', company = '" . $this->db->escape($this->request->post['input-shipping-company']) . "', address_1 = '" . $this->db->escape($this->request->post['input-shipping-address-1']) . "', address_2 = '" . $this->db->escape($this->request->post['input-shipping-address-2']) . "', postcode = '" . $this->db->escape($this->request->post['input-shipping-postcode']) . "', city = '" . $this->db->escape($this->request->post['input-shipping-city']) . "', zone_id = '" . (int) $zoneId . "', country_id = '" . (int) $this->request->post['input-shipping-country'] . "' WHERE address_id  = '" . (int) $this->request->post['address_id'] . "' AND customer_id = '" . (int) $this->request->post['customer_id'] . "'");
						}

						$i = 0;
						//foreach ($cart_info->rows as $cart_data) {
						foreach ($cart_infoNew['products'] as $cart_data) {

							$prod_info = $this->model_catalog_product->getProduct($cart_data['product_id'], $seller_id);

							$option_price = 0;
							$option_points = 0;
							$option_weight = 0;

							$option_data = array();



							foreach ($cart_data['option'] as $product_option_id => $value) {

								$option_query = $this->db->query("SELECT po.product_option_id, po.option_id, od.name, o.type FROM " . DB_PREFIX . "product_option po LEFT JOIN `" . DB_PREFIX . "option` o ON (po.option_id = o.option_id) LEFT JOIN " . DB_PREFIX . "option_description od ON (o.option_id = od.option_id) WHERE po.product_option_id = '" . (int) $product_option_id . "' AND po.product_id = '" . (int) $cart_data['product_id'] . "' AND od.language_id = '" . (int) $this->request->post['language_id'] . "'");
								$q = "SELECT po.product_option_id, po.option_id, od.name, o.type FROM " . DB_PREFIX . "product_option po LEFT JOIN `" . DB_PREFIX . "option` o ON (po.option_id = o.option_id) LEFT JOIN " . DB_PREFIX . "option_description od ON (o.option_id = od.option_id) WHERE po.product_option_id = '" . (int) $product_option_id . "' AND po.product_id = '" . (int) $cart_data['product_id'] . "' AND od.language_id = '" . (int) $this->request->post['language_id'] . "'";
								/// test this work 

								if ($option_query->num_rows) {

									if ($option_query->row['type'] == 'select' || $option_query->row['type'] == 'radio' || $option_query->row['type'] == 'image') {

										$option_value_query = $this->db->query("SELECT pov.option_value_id, ovd.name, pov.quantity, pov.subtract, pov.price, pov.price_prefix, pov.points, pov.points_prefix, pov.weight, pov.weight_prefix FROM " . DB_PREFIX . "product_option_value pov LEFT JOIN " . DB_PREFIX . "option_value ov ON (pov.option_value_id = ov.option_value_id) LEFT JOIN " . DB_PREFIX . "option_value_description ovd ON (ov.option_value_id = ovd.option_value_id) WHERE pov.product_option_value_id = '" . (int) $value . "' AND pov.product_option_id = '" . (int) $product_option_id . "' AND pov.seller_id = '" . (int) $seller_id . "' AND ovd.language_id = '" . (int) $this->request->post['language_id'] . "'");

										if ($option_value_query->num_rows) {
											if ($option_value_query->row['price_prefix'] == '+') {
												$option_price += $option_value_query->row['price'];
											} elseif ($option_value_query->row['price_prefix'] == '-') {
												$option_price -= $option_value_query->row['price'];
											}

											if ($option_value_query->row['points_prefix'] == '+') {
												$option_points += $option_value_query->row['points'];
											} elseif ($option_value_query->row['points_prefix'] == '-') {
												$option_points -= $option_value_query->row['points'];
											}

											if ($option_value_query->row['weight_prefix'] == '+') {
												$option_weight += $option_value_query->row['weight'];
											} elseif ($option_value_query->row['weight_prefix'] == '-') {
												$option_weight -= $option_value_query->row['weight'];
											}

											if ($option_value_query->row['subtract'] && (!$option_value_query->row['quantity'] || ($option_value_query->row['quantity'] < $cart_data['quantity']))) {
												$stock = false;
											}

											$option_data[] = array(
												'product_option_id'       => $product_option_id,
												'product_option_value_id' => $value,
												'option_id'               => $option_query->row['option_id'],
												'option_value_id'         => $option_value_query->row['option_value_id'],
												'name'                    => $option_query->row['name'],
												'value'                   => $option_value_query->row['name'],
												'type'                    => $option_query->row['type'],
												'quantity'                => $option_value_query->row['quantity'],
												'subtract'                => $option_value_query->row['subtract'],
												'price'                   => $option_value_query->row['price'],
												'price_prefix'            => $option_value_query->row['price_prefix'],
												'points'                  => $option_value_query->row['points'],
												'points_prefix'           => $option_value_query->row['points_prefix'],
												'weight'                  => $option_value_query->row['weight'],
												'weight_prefix'           => $option_value_query->row['weight_prefix']
											);
										}
									} elseif ($option_query->row['type'] == 'checkbox' && is_array($value)) {
										foreach ($value as $product_option_value_id) {
											$option_value_query = $this->db->query("SELECT pov.option_value_id, ovd.name, pov.quantity, pov.subtract, pov.price, pov.price_prefix, pov.points, pov.points_prefix, pov.weight, pov.weight_prefix FROM " . DB_PREFIX . "product_option_value pov LEFT JOIN " . DB_PREFIX . "option_value ov ON (pov.option_value_id = ov.option_value_id) LEFT JOIN " . DB_PREFIX . "option_value_description ovd ON (ov.option_value_id = ovd.option_value_id) WHERE pov.product_option_value_id = '" . (int) $product_option_value_id . "' AND pov.product_option_id = '" . (int) $product_option_id . "' AND ovd.language_id = '" . (int) $this->request->post['language_id'] . "'");

											if ($option_value_query->num_rows) {
												if ($option_value_query->row['price_prefix'] == '+') {
													$option_price += $option_value_query->row['price'];
												} elseif ($option_value_query->row['price_prefix'] == '-') {
													$option_price -= $option_value_query->row['price'];
												}

												if ($option_value_query->row['points_prefix'] == '+') {
													$option_points += $option_value_query->row['points'];
												} elseif ($option_value_query->row['points_prefix'] == '-') {
													$option_points -= $option_value_query->row['points'];
												}

												if ($option_value_query->row['weight_prefix'] == '+') {
													$option_weight += $option_value_query->row['weight'];
												} elseif ($option_value_query->row['weight_prefix'] == '-') {
													$option_weight -= $option_value_query->row['weight'];
												}

												if ($option_value_query->row['subtract'] && (!$option_value_query->row['quantity'] || ($option_value_query->row['quantity'] < $cart_data['quantity']))) {
													$stock = false;
												}

												$option_data[] = array(
													'product_option_id'       => $product_option_id,
													'product_option_value_id' => $product_option_value_id,
													'option_id'               => $option_query->row['option_id'],
													'option_value_id'         => $option_value_query->row['option_value_id'],
													'name'                    => $option_query->row['name'],
													'value'                   => $option_value_query->row['name'],
													'type'                    => $option_query->row['type'],
													'quantity'                => $option_value_query->row['quantity'],
													'subtract'                => $option_value_query->row['subtract'],
													'price'                   => $option_value_query->row['price'],
													'price_prefix'            => $option_value_query->row['price_prefix'],
													'points'                  => $option_value_query->row['points'],
													'points_prefix'           => $option_value_query->row['points_prefix'],
													'weight'                  => $option_value_query->row['weight'],
													'weight_prefix'           => $option_value_query->row['weight_prefix']
												);
											}
										}
									} elseif ($option_query->row['type'] == 'text' || $option_query->row['type'] == 'textarea' || $option_query->row['type'] == 'file' || $option_query->row['type'] == 'date' || $option_query->row['type'] == 'datetime' || $option_query->row['type'] == 'time') {
										$option_data[] = array(
											'product_option_id'       => $product_option_id,
											'product_option_value_id' => '',
											'option_id'               => $option_query->row['option_id'],
											'option_value_id'         => '',
											'name'                    => $option_query->row['name'],
											'value'                   => $value,
											'type'                    => $option_query->row['type'],
											'quantity'                => '',
											'subtract'                => '',
											'price'                   => '',
											'price_prefix'            => '',
											'points'                  => '',
											'points_prefix'           => '',
											'weight'                  => '',
											'weight_prefix'           => ''
										);
									}
								}
							}

							if ($prod_info['special'] == null) {
								$total = ($total + (($prod_info['price'] + $option_price) * $cart_data['quantity']));
							} else {
								$total = ($total + (($prod_info['special'] + $option_price) * $cart_data['quantity']));
							}


							if ($prod_info['special'] == null) {
								$order_data['products'][] = array(
									'product_id' => $prod_info['product_id'],
									'name' => $prod_info['name'],
									'model' => $prod_info['model'],
									'option' => $option_data,
									'download' => array(),
									'quantity' => $cart_data['quantity'],
									'subtract' => $prod_info['subtract'],
									'price'    => ($prod_info['price'] + $option_price),
									'total'   => ($prod_info['price'] + $option_price) * $cart_data['quantity'],
									'tax' => 0,
									'reward' => 0,
									'seller_id' => $seller_id,
								);
								$i++;
							} else {
								$order_data['products'][] = array(
									'product_id' => $prod_info['product_id'],
									'name' => $prod_info['name'],
									'model' => $prod_info['model'],
									'option' => $option_data,
									'download' => array(),
									'quantity' => $cart_data['quantity'],
									'subtract' => $prod_info['subtract'],
									'price'    => ($prod_info['special'] + $option_price),
									'total'   => ($prod_info['special'] + $option_price) * $cart_data['quantity'],
									'tax' => 0,
									'reward' => 0,
									'seller_id' => $seller_id,
								);
								$i++;
							}
						}

						// Gift Voucher
						$order_data['vouchers'] = array();

						if (!empty($this->session->data['vouchers'])) {
							foreach ($this->session->data['vouchers'] as $voucher) {
								$order_data['vouchers'][] = array(
									'description' => $voucher['description'],
									'code' => substr(md5(mt_rand()), 0, 10),
									'to_name' => $voucher['to_name'],
									'to_email' => $voucher['to_email'],
									'from_name' => $voucher['from_name'],
									'from_email' => $voucher['from_email'],
									'voucher_theme_id' => $voucher['voucher_theme_id'],
									'message' => $voucher['message'],
									'amount' => $voucher['amount'],
								);
							}
						}

						$deliveryCharges = 0;
						$deliveryCharges = $cart_infoNew['delivery_charges'];

						$deliveryChargesArr = array();

						$deliveryChargesArr[] = array(
							'delivery_charges' => $deliveryCharges,
							'seller_id' => $seller_id,
							'firstname' => $cart_infoNew['seller_nameen'],
							'lastname' => $cart_infoNew['seller_namear']
						);

						$this->session->data['deliveryChargesArr'] = '';
						$this->session->data['deliveryChargesArr'] = $deliveryChargesArr;

						$order_data['comment'] = $this->request->post['order_comments'];
						$order_data['total'] = $total;

						if (isset($this->request->cookie['tracking'])) {
							$order_data['tracking'] = $this->request->cookie['tracking'];

							$subtotal = $this->cart->getSubTotal();

							// Affiliate
							$this->load->model('affiliate/affiliate');

							$affiliate_info = $this->model_affiliate_affiliate->getAffiliateByCode($this->request->cookie['tracking']);

							if ($affiliate_info) {
								$order_data['affiliate_id'] = $affiliate_info['affiliate_id'];
								$order_data['commission'] = ($subtotal / 100) * $affiliate_info['commission'];
							} else {
								$order_data['affiliate_id'] = 0;
								$order_data['commission'] = 0;
							}

							// Marketing
							$this->load->model('checkout/marketing');

							$marketing_info = $this->model_checkout_marketing->getMarketingByCode($this->request->cookie['tracking']);

							if ($marketing_info) {
								$order_data['marketing_id'] = $marketing_info['marketing_id'];
							} else {
								$order_data['marketing_id'] = 0;
							}
						} else {
							$order_data['affiliate_id'] = 0;
							$order_data['commission'] = 0;
							$order_data['marketing_id'] = 0;
							$order_data['tracking'] = '';
						}

						$order_data['language_id'] = $this->request->post['language_id'];
						$order_data['currency_id'] = $this->currency->getId();
						$order_data['currency_code'] = $this->currency->getCode();
						$order_data['currency_value'] = $this->currency->getValue($this->currency->getCode());
						$order_data['ip'] = $this->request->server['REMOTE_ADDR'];

						if (!empty($this->request->server['HTTP_X_FORWARDED_FOR'])) {
							$order_data['forwarded_ip'] = $this->request->server['HTTP_X_FORWARDED_FOR'];
						} elseif (!empty($this->request->server['HTTP_CLIENT_IP'])) {
							$order_data['forwarded_ip'] = $this->request->server['HTTP_CLIENT_IP'];
						} else {
							$order_data['forwarded_ip'] = '';
						}

						if (isset($this->request->server['HTTP_USER_AGENT'])) {
							$order_data['user_agent'] = $this->request->server['HTTP_USER_AGENT'];
						} else {
							$order_data['user_agent'] = '';
						}

						if (isset($this->request->server['HTTP_ACCEPT_LANGUAGE'])) {
							$order_data['accept_language'] = $this->request->server['HTTP_ACCEPT_LANGUAGE'];
						} else {
							$order_data['accept_language'] = '';
						}

						$total_data = array();

						$total_data[0] = array(
							'code' => 'sub_total',
							'title' => 'Sub-Total',
							'text' => 'Sub-Total',
							'value' => $total,
							'sort_order' => 0,
						);

						/* $total_data[1] = array(
							'code' => 'vat',
							'title' => $this->request->post['vat_text'],
							'value' => $this->request->post['vat_value'],
							'sort_order' => 1,
						); */

						$vat =  (float) $this->config->get('config_vat');
						$vat_total = 0;
						$vat_total = $total * $vat / ($vat + 100);

						$total_data[1] = array(
							'code' => 'vat',
							'title' => "Value Added Tax (".$vat."%)",
							'value' => $vat_total,
							'sort_order' => 1,
						);


						$isCouponRequired = false;
						if ((isset($voucher)) && ($voucher > 0)) {
							$isCouponRequired = true;
							//$total = $voucher_info[1];

							$total -= $voucher;

							$total_data[3] = array(
								'code' => 'coupon',
								'title' => $voucher_info[0]['title'],
								'value' => -$voucher,
								'sort_order' => 3,
							);
						}

						$this->load->model('checkout/order');
						if ($this->config->get('credit_status')) {
							$this->load->language('total/credit');

							if ((float) $credit_total) {
								if ($credit_total > $total) {
									$credit = $total;
								} else {
									$credit = $credit_total;
								}

								// if ($credit > 0) {
								$total -= $credit;
								$total_data[4] = array(
									'code' => 'credit',
									'title' => 'Store Credit',
									'value' => -$credit,
									'sort_order' => 4,
								);

								//}

								if ($credit_total > $credit) {
									$credit_total = $credit_total - $credit;
								} else {
									$credit_total = $credit - $credit_total;
								}
							}
						}


						$sellerInfo_custom = $this->model_store_store->getSellerByID($seller_id);

						if ($sellerInfo_custom['free_delivery_limit'] > $total or $sellerInfo_custom['free_delivery_limit'] == 0) {
							$total_data[5] = array(
								'code' => 'total',
								'title' => 'Total',
								'value' => $total + $deliveryCharges,
								'sort_order' => 5,
							);
						} else {
							$total_data[5] = array(
								'code' => 'total',
								'title' => 'Total',
								'value' => $total,
								'sort_order' => 5,
							);
						}
						// $total_data[5] = array(
						// 'code' => 'total',
						// 'title' => 'Total',
						// 'value' => $total + $deliveryCharges,
						// 'sort_order' => 5,
						// );

						$order_data['totals'] = $total_data;
						
						// vat 
						
						foreach($order_data['totals'] as $key=>$totals_data){
							if($totals_data['code']	== 'vat'){
								$vat =  (float)$this->config->get('config_vat');
								
								if(isset($vat) && $vat > 0){
								   $this->load->model('total/vat');
									$value = $this->{'model_total_vat'}->getTotalvalue($total, $vat);
									$order_data['totals'][$key]['value'] = round($value,2);
								}			
							}

						}
						
						//debugging($order_data);


						$sqlAll = "SELECT s.seller_id, s.firstname, s.lastname, s.package_ready, s.delivery_timegap, s.mon_start_time, s.mon_end_time, s.tue_start_time, s.tue_end_time, s.wed_start_time, s.wed_end_time, s.thu_start_time, s.thu_end_time, s.fri_start_time, s.fri_end_time, s.sat_start_time, s.sat_end_time, s.sun_start_time, s.sun_end_time, s.sortorder FROM " . DB_PREFIX . "sellers s WHERE s.seller_id = '" . $seller_id . "'";

						$queryAll = $this->db->query($sqlAll);
						$seller_info = $queryAll->row;

						$delivery_time_post = $this->getOrderNextDeliverySlot($seller_info, $this->request->post['delivery_time']);

						if (empty($delivery_time_post)) {
							$delivery_time_post = $this->request->post['delivery_time'];
						}
						
						//store_area_id
						$order_data['store_area_id'] =0;
						if(isset($this->request->post['store_area_id'])){
							if($this->request->post['store_area_id'] != ''){
								$order_data['store_area_id'] = $this->request->post['store_area_id'];
							}
						}
						//store_area_id
						$order_data['sequence'] = $this->request->post['sequence'];

						$order_id = $this->model_checkout_order->addOrderNew($order_data);
						$multi_order_id[] = $order_id;
						// post data Write 
						$myfile = fopen("orderData/" . $order_id . ".txt", "w") or die("Unable to open file!");
						//$txt = "";
						$txt = "confirmOrderV4test";
						foreach ($this->request->post as $keyNew => $valueNew) {
							$txt .= "\n" . $keyNew . "=" . $valueNew;
						}
						foreach ($order_data['products'] as $product_history) {
							$txt .= "\n \n product id = " . $product_history['product_id'] . " and quantity=" . $product_history['quantity'] . " and price =" . $product_history['price'] . " and option = " . $product_history['option'] . " and total= " . $product_history['total'];
						}
						foreach ($order_data['totals'] as $cart_total) {
							$txt .= "\n \n \n title = " . $cart_total['title'] . " and value=" . $cart_total['value'] . " and code =" . $cart_total['code'];
						}
						fwrite($myfile, $txt);
						fclose($myfile);


						// Add to activity log
						$this->load->model('account/activity');

						$activity_data = array(
							'customer_id' => $customer_info['customer_id'],
							'name' => $customer_info['firstname'] . ' ' . $customer_info['lastname'],
							'order_id' => $order_id,
						);

						$this->model_account_activity->addActivity('order_account', $activity_data);

						$this->load->model('checkout/order');
						$order_info = $this->model_checkout_order->getOrder($order_id);

						$order_total_query = $this->db->query("SELECT * FROM `" . DB_PREFIX . "order_total` WHERE order_id = '" . (int) $order_id . "' ORDER BY sort_order ASC");

						foreach ($order_total_query->rows as $order_total) {
							$this->load->model('total/' . $order_total['code']);

							if (method_exists($this->{'model_total_' . $order_total['code']}, 'confirm')) {
								$this->{'model_total_' . $order_total['code']}->confirm($order_info, $order_total);
							}
						}

						/* $output[] = array(
							"message" => true,
							"orderid" => $order_id,
							"total" => $total + $deliveryCharges,
							"payment_firstname" => $order_data['payment_firstname'],
							"payment_lastname" => $order_data['payment_lastname'],
							"email" => $order_data['email'],
							"telephone" => $order_data['telephone'],
							"address_1" => $order_data['payment_address_1'],
							"address_2" => $order_data['payment_address_2'],
						); */

						$orderTotal = $orderTotal + ($total + $deliveryCharges);

						$this->load->model('module/deliveryTime/order');
						$this->model_module_deliveryTime_order->addDeliveryTimeV2($order_id, $delivery_time_post);

						try {
							$this->model_checkout_order->addOrderHistoryNew4($order_id, 27, $order_data['comment'], true);
						} catch (Exception $e) {
							//print_r($e);
							$output = array(
								"message" => 'Error in order Place.',
							);
							echo json_encode($output);
						}

						// Add Coupon History
						if ($isCouponRequired) {
							$this->model_total_coupon->couponHistory($couponId, $order_id, $customer, $voucher);
						}

						//coupon blank 
						$voucher = 0;

						//Notification Send Start
						//customer other device_id in notification Send
						/* 	$notifications = array();
						$notifications = $this->model_checkout_order->sendCustomerNotification($customer_info['customer_id']);
						
						if (count($notifications) > 0) {
							
							foreach($notifications as $notification) {
								if($notification['device_type'] == 1){
									
									$this->sendMessageOrderPlace($notification['device_id'], $order_id, $seller_info['firstname'], $seller_info['lastname'], $this->request->post['language_id']);
																
								} else if($notification['device_type'] == 2){
									
									$this->sendMessageOrderPlaceIOS($notification['device_id'], $order_id, $seller_info['firstname'], $seller_info['lastname'], $this->request->post['language_id']);
									
								}
							}
						} */
						//Notification Send end */
					}
					//$this->db->query("UPDATE `" . DB_PREFIX . "order_history` SET `order_status_id` = '1' WHERE `oc_order_history`.`order_history_id` = ".$order_id."");

					$this->db->query("DELETE FROM " . DB_PREFIX . "cart WHERE customer_id = '" . (int) $this->request->post['customer_id'] . "'");
					$List_multi_order_id = implode(',', $multi_order_id);

					$myfile = fopen("orderData/" . $order_id . "_return.txt", "w") or die("Unable to open file!");
					$txt = "confirmOrderV4test";
					$txt .= "\n total =" . $orderTotal . " and  order_ids= " . $List_multi_order_id;
					fwrite($myfile, $txt);
					fclose($myfile);

					$output = array(
						"message" => true,
						"total" => $orderTotal,
						"orders_id" => $List_multi_order_id
					);
				} else {
					$output = array("message" => $couponMsg);
				}

				/* --------------------------------------- */
			} else {
				$output = array(
					"message" => 'No product found in cart',
				);
			}
		} else {
			$output = array(
				"message" => 'Seller Disabled ' . $seller_disable_name,
			);
		}
		$this->db->query("DELETE FROM " . DB_PREFIX . "cart WHERE customer_id = '" . (int) $this->request->post['customer_id'] . "'");
		echo json_encode($output);
	}
	/////

	public function confirmPaymentNotification()
	{
		/* $this->request->post['customer_id']= 7;
		$this->request->post['orders_id'] = 6515;
		$this->request->post['language_id'] = 1; */


		if (isset($this->request->post['customer_id']) && isset($this->request->post['orders_id']) && isset($this->request->post['language_id'])) {

			if ($this->request->post['orders_id'] != '' && $this->request->post['customer_id'] != '' && $this->request->post['language_id']) {

				$orders_id = $this->request->post['orders_id'];
				$order_arr = explode(",", $orders_id);
				$customer_id_order = 0;
				foreach ($order_arr as $order_id) {

					$order_info = $this->db->query("SELECT * FROM " . DB_PREFIX . "order WHERE order_id = '" . (int) $order_id . "'");

					$customer_id_order = $order_info->row['customer_id'];
					/* echo"<pre>";
					print_r($order_info);
					exit; */
					if ($order_info->num_rows && $customer_id_order != 0) {
						$order_status_id = $order_info->row['order_status_id'];
						$this->load->model('checkout/order');
						$order_info = $this->model_checkout_order->getOrder($order_id);

						if ($order_status_id != 0) {

							$download_status = false;

							$order_product_query = $this->db->query("SELECT * FROM " . DB_PREFIX . "order_product WHERE order_id = '" . (int) $order_id . "'");
							foreach ($order_product_query->rows as $order_product) {
								// Check if there are any linked downloads
								$product_download_query = $this->db->query("SELECT COUNT(*) AS total FROM `" . DB_PREFIX . "product_to_download` WHERE product_id = '" . (int) $order_product['product_id'] . "'");

								if ($product_download_query->row['total']) {
									$download_status = true;
								}
							}

							// Load the language for any mails that might be required to be sent out
							$language = new Language($order_info['language_directory']);
							$language->load($order_info['language_directory']);
							$language->load('mail/order');

							$order_status_query = $this->db->query("SELECT * FROM " . DB_PREFIX . "order_status WHERE order_status_id = '" . (int) $order_status_id . "' AND language_id = '" . (int) $order_info['language_id'] . "'");

							$order_query = $this->db->query("SELECT * FROM " . DB_PREFIX . "delivery_time WHERE order_id = '" . (int) $order_id . "'");

							if ($order_query->num_rows) {
								$delivery_time = $order_query->row['delivery_time'];
							} else {
								$delivery_time = "Not Available";
							}

							if ($order_status_query->num_rows) {
								$order_status = $order_status_query->row['name'];
							} else {
								$order_status = '';
							}

							$subject = sprintf($language->get('text_new_subject'), html_entity_decode($order_info['store_name'], ENT_QUOTES, 'UTF-8'), $order_id);

							$logo_small = "catalog/salatty_icons/careeb_small_logo.png";

							// HTML Mail
							$data = array();

							$data['title']                 = sprintf($language->get('text_new_subject'), $order_info['store_name'], $order_id);
							$data['text_greeting']         = sprintf($language->get('text_new_greeting'), $order_info['store_name']);
							$data['text_link']             = $language->get('text_new_link');
							$data['text_download']         = $language->get('text_new_download');
							$data['text_order_detail']     = $language->get('text_new_order_detail');
							$data['text_instruction']      = $language->get('text_new_instruction');
							$data['text_order_id']         = $language->get('text_new_order_id');
							$data['text_date_added']       = $language->get('text_new_date_added');
							$data['text_payment_method']   = $language->get('text_new_payment_method');
							$data['text_shipping_method']  = $language->get('text_new_shipping_method');
							$data['text_email']            = $language->get('text_new_email');
							$data['text_telephone']        = $language->get('text_new_telephone');
							$data['text_ip']               = $language->get('text_new_ip');
							$data['text_order_status']     = $language->get('text_new_order_status');
							$data['text_payment_address']  = $language->get('text_new_payment_address');
							$data['text_shipping_address'] = $language->get('text_new_shipping_address');
							$data['text_product']          = $language->get('text_new_product');
							$data['text_model']            = $language->get('text_new_model');
							$data['text_quantity']         = $language->get('text_new_quantity');
							$data['text_price']            = $language->get('text_new_price');
							$data['text_total']            = $language->get('text_new_total');
							$data['text_footer']           = $language->get('text_new_footer');

							$data['logo']          = $this->config->get('config_url') . 'image/' . $logo_small;
							$data['store_name']    = $order_info['store_name'];
							$data['store_url']     = $order_info['store_url'];
							$data['customer_id']   = $order_info['customer_id'];
							$data['link']          = $order_info['store_url'] . 'index.php?route=account/order/info&order_id=' . $order_id;
							$data['delivery_time'] = $delivery_time;
							if ($download_status) {
								$data['download'] = $order_info['store_url'] . 'index.php?route=account/download';
							} else {
								$data['download'] = '';
							}

							$seller_info = $this->db->query("SELECT * FROM " . DB_PREFIX . "sellers WHERE seller_id = '" . (int) $order_info['seller_id'] . "'");

							$data['seller_name']     = $seller_info->row['firstname'];
							$data['order_id']        = $order_id;
							$data['date_added']      = date($language->get('datetime_format'), strtotime($order_info['date_added']));
							$data['payment_method']  = $order_info['payment_method'];
							$data['shipping_method'] = $order_info['shipping_method'];
							$data['email']           = $order_info['email'];
							$data['telephone']       = $order_info['telephone'];
							$data['ip']              = $order_info['ip'];
							$data['order_status']    = $order_status;

							if ($comment && $notify) {
								$data['comment'] = nl2br($comment);
							} else {
								$data['comment'] = '';
							}

							if ($order_info['payment_address_format']) {
								$format = $order_info['payment_address_format'];
							} else {
								$format = '{firstname} {lastname}' . "\n" . '{company}' . "\n" . '{address_1}' . "\n" . '{address_2}' . "\n" . '{city} {postcode}' . "\n" . '{zone}' . "\n" . '{country}';
							}
							$order_customer_store = $this->db->query("SELECT *  FROM `" . DB_PREFIX . "customer` WHERE `store` = 1 AND customer_id = " . $customer_id_order . "");


							if ($order_customer_store->num_rows) {
								$store = $order_customer_store->row['store'];
								$storename = ' - ' . $order_customer_store->row['storename'];
							} else {
								$store = '';
								$storename = '';
							}
							$find = array(
								'{firstname}',
								'{lastname}',
								'{company}',
								'{address_1}',
								'{address_2}',
								'{city}',
								'{postcode}',
								'{zone}',
								'{zone_code}',
								'{country}'
							);

							$replace = array(
								'firstname' => $order_info['payment_firstname'],
								'lastname'  => $order_info['payment_lastname'] . $storename,
								'company'   => $order_info['payment_company'],
								'address_1' => $order_info['payment_address_1'],
								'address_2' => $order_info['payment_address_2'],
								'city'      => $order_info['payment_city'],
								'postcode'  => $order_info['payment_postcode'],
								'zone'      => $order_info['payment_zone'],
								'zone_code' => $order_info['payment_zone_code'],
								'country'   => $order_info['payment_country']
							);

							$data['payment_address'] = str_replace(array("\r\n", "\r", "\n"), '<br />', preg_replace(array("/\s\s+/", "/\r\r+/", "/\n\n+/"), '<br />', trim(str_replace($find, $replace, $format))));

							if ($order_info['shipping_address_format']) {
								$format = $order_info['shipping_address_format'];
							} else {
								$format = '{firstname} {lastname}' . "\n" . '{company}' . "\n" . '{address_1}' . "\n" . '{address_2}' . "\n" . '{city} {postcode}' . "\n" . '{zone}' . "\n" . '{country}';
							}

							$find = array(
								'{firstname}',
								'{lastname}',
								'{company}',
								'{address_1}',
								'{address_2}',
								'{city}',
								'{postcode}',
								'{zone}',
								'{zone_code}',
								'{country}'
							);

							$replace = array(
								'firstname' => $order_info['shipping_firstname'],
								'lastname'  => $order_info['shipping_lastname'] . $storename,
								'company'   => $order_info['shipping_company'],
								'address_1' => $order_info['shipping_address_1'],
								'address_2' => $order_info['shipping_address_2'],
								'city'      => $order_info['shipping_city'],
								'postcode'  => $order_info['shipping_postcode'],
								'zone'      => $order_info['shipping_zone'],
								'zone_code' => $order_info['shipping_zone_code'],
								'country'   => $order_info['shipping_country']
							);

							$data['shipping_address'] = str_replace(array("\r\n", "\r", "\n"), '<br />', preg_replace(array("/\s\s+/", "/\r\r+/", "/\n\n+/"), '<br />', trim(str_replace($find, $replace, $format))));

							$this->load->model('tool/upload');

							$this->load->model('tool/image');
							$this->load->model('catalog/product');

							// Products
							$data['products'] = array();

							foreach ($order_product_query->rows as $product) {
								$option_data = array();

								$order_option_query = $this->db->query("SELECT * FROM " . DB_PREFIX . "order_option WHERE order_id = '" . (int) $order_id . "' AND order_product_id = '" . (int) $product['order_product_id'] . "'");

								foreach ($order_option_query->rows as $option) {
									if ($option['type'] != 'file') {
										$value = $option['value'];
									} else {
										$upload_info = $this->model_tool_upload->getUploadByCode($option['value']);

										if ($upload_info) {
											$value = $upload_info['name'];
										} else {
											$value = '';
										}
									}

									$option_data[] = array(
										'name'  => $option['name'],
										'value' => (utf8_strlen($value) > 20 ? utf8_substr($value, 0, 20) . '..' : $value)
									);
								}

								$product_info = $this->model_catalog_product->getProduct($product['product_id']);

								if ($product_info['image']) {
									//$image = $this->model_tool_image->resize(str_replace(" ","%20", $product_info['image']), 60, 60);
									$image = $this->model_tool_image->resize($product_info['image'], 60, 60);
								} else {
									$image = $this->model_tool_image->resize('placeholder.png', 60, 60);
								}

								$data['products'][] = array(
									'name'     => $product['name'],
									'model'    => $product['model'],
									'thumb'    => str_replace(' ', '%20', $image),
									'upc'      => $product_info['upc'],
									'option'   => $option_data,
									'quantity' => $product['quantity'],
									'price'    => $this->currency->format($product['price'] + ($this->config->get('config_tax') ? $product['tax'] : 0), $order_info['currency_code'], $order_info['currency_value']),
									'total'    => $this->currency->format($product['total'] + ($this->config->get('config_tax') ? ($product['tax'] * $product['quantity']) : 0), $order_info['currency_code'], $order_info['currency_value'])
								);
							}

							// Vouchers
							$data['vouchers'] = array();

							$order_voucher_query = $this->db->query("SELECT * FROM " . DB_PREFIX . "order_voucher WHERE order_id = '" . (int) $order_id . "'");

							foreach ($order_voucher_query->rows as $voucher) {
								$data['vouchers'][] = array(
									'description' => $voucher['description'],
									'amount'      => $this->currency->format($voucher['amount'], $order_info['currency_code'], $order_info['currency_value']),
								);
							}

							// Order Totals
							$order_total_query = $this->db->query("SELECT * FROM `" . DB_PREFIX . "order_total` WHERE order_id = '" . (int) $order_id . "' ORDER BY sort_order ASC");

							$deliveryChargesArr = $this->db->query("
											select oc_sellers.delivery_charges, oc_sellers.seller_id, oc_sellers.free_delivery_limit, oc_sellers.firstname, oc_sellers.lastname 
											from oc_order_seller_shipping_fee 
											join oc_sellers on oc_sellers.seller_id = oc_order_seller_shipping_fee.seller_id 
											where oc_order_seller_shipping_fee.order_id = '" . $order_id . "'")->rows;

							foreach ($order_total_query->rows as $total) {

								if ($total['code'] == 'vat') {
									$deliveryCharges = 0;
									foreach ($deliveryChargesArr as $deliveryChargesRow) {
										if ($deliveryChargesRow['free_delivery_limit'] > $order_info['total'] or $deliveryChargesRow['free_delivery_limit'] == 0) {

											$deliveryCharges = $deliveryChargesRow['delivery_charges'];
											$data['totals'][] = array(
												'title' => ' Delivery Charges',
												'text'  => $this->currency->format($deliveryCharges),
											);
										} else {
											$data['totals'][] = array(
												'title' => ' Delivery Charges',
												'text'  => $this->currency->format(0),
											);
										}
										//$total['value'] += $deliveryCharges;
									}
								}

								$data['totals'][] = array(
									'title' => $total['title'],
									'text'  => $this->currency->format($total['value'], $order_info['currency_code'], $order_info['currency_value']),
								);
							}

							if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/template/mail/order.tpl')) {
								$html = $this->load->view($this->config->get('config_template') . '/template/mail/order.tpl', $data);
							} else {
								$html = $this->load->view('default/template/mail/order.tpl', $data);
							}

							// Can not send confirmation emails for CBA orders as email is unknown
							$this->load->model('payment/amazon_checkout');

							if (!$this->model_payment_amazon_checkout->isAmazonOrder($order_info['order_id'])) {
								// Text Mail
								$text  = sprintf($language->get('text_new_greeting'), html_entity_decode($order_info['store_name'], ENT_QUOTES, 'UTF-8')) . "\n\n";
								$text .= $language->get('text_new_order_id') . ' ' . $order_id . "\n";
								$text .= $language->get('text_new_date_added') . ' ' . date($language->get('date_format_short'), strtotime($order_info['date_added'])) . "\n";
								$text .= $language->get('text_new_order_status') . ' ' . $order_status . "\n\n";

								if ($comment && $notify) {
									$text .= $language->get('text_new_instruction') . "\n\n";
									$text .= $comment . "\n\n";
								}

								// Products
								$text .= $language->get('text_new_products') . "\n";

								foreach ($order_product_query->rows as $product) {
									$text .= $product['quantity'] . 'x ' . $product['name'] . ' (' . $product['model'] . ') ' . html_entity_decode($this->currency->format($product['total'] + ($this->config->get('config_tax') ? ($product['tax'] * $product['quantity']) : 0), $order_info['currency_code'], $order_info['currency_value']), ENT_NOQUOTES, 'UTF-8') . "\n";

									$order_option_query = $this->db->query("SELECT * FROM " . DB_PREFIX . "order_option WHERE order_id = '" . (int) $order_id . "' AND order_product_id = '" . $product['order_product_id'] . "'");

									foreach ($order_option_query->rows as $option) {
										if ($option['type'] != 'file') {
											$value = $option['value'];
										} else {
											$upload_info = $this->model_tool_upload->getUploadByCode($option['value']);

											if ($upload_info) {
												$value = $upload_info['name'];
											} else {
												$value = '';
											}
										}

										$text .= chr(9) . '-' . $option['name'] . ' ' . (utf8_strlen($value) > 20 ? utf8_substr($value, 0, 20) . '..' : $value) . "\n";
									}
								}

								foreach ($order_voucher_query->rows as $voucher) {
									$text .= '1x ' . $voucher['description'] . ' ' . $this->currency->format($voucher['amount'], $order_info['currency_code'], $order_info['currency_value']);
								}

								$text .= "\n";

								$text .= $language->get('text_new_order_total') . "\n";

								foreach ($order_total_query->rows as $total) {
									$text .= $total['title'] . ': ' . html_entity_decode($this->currency->format($total['value'], $order_info['currency_code'], $order_info['currency_value']), ENT_NOQUOTES, 'UTF-8') . "\n";
								}

								$text .= "\n";

								if ($order_info['customer_id']) {
									$text .= $language->get('text_new_link') . "\n";
									$text .= $order_info['store_url'] . 'index.php?route=account/order/info&order_id=' . $order_id . "\n\n";
								}

								if ($download_status) {
									$text .= $language->get('text_new_download') . "\n";
									$text .= $order_info['store_url'] . 'index.php?route=account/download' . "\n\n";
								}

								// Comment
								if ($order_info['comment']) {
									$text .= $language->get('text_new_comment') . "\n\n";
									$text .= $order_info['comment'] . "\n\n";
								}

								$text .= $language->get('text_new_footer') . "\n\n";

								$template = $this->model_catalog_product->getEmailTemplate('customer-order-place');

								$order_link = $order_info['store_url'] . 'index.php?route=account/order/info&order_id=' . $order_id;

								$date_added = date($language->get('date_format_short'), strtotime($order_info['date_added']));

								if ($comment) {
									$comments = '<table style="border-collapse: collapse; width: 100%; border-top: 1px solid #DDDDDD; border-left: 1px solid #DDDDDD; margin-bottom: 20px;">
												<thead> <tr> <td style="font-size: 12px; border-right: 1px solid #DDDDDD; border-bottom: 1px solid #DDDDDD; background-color: #EFEFEF; font-weight: bold; text-align: left; padding: 7px; color: #222222;">' . $data['text_instruction'] . '</td>  </tr> </thead> <tbody> <tr> <td style="font-size: 12px;	border-right: 1px solid #DDDDDD; border-bottom: 1px solid #DDDDDD; text-align: left; padding: 7px;">' . $comment . '</td>  </tr>	</tbody> </table>';
								} else {
									$comments = '';
								}

								$product_information = '<table style="border-collapse:collapse; width:100%; border-top:1px solid #DDDDDD; border-left:1px solid #DDDDDD; margin-bottom:20px;">
										<thead> <tr>
											<td style="font-size: 12px; border-right: 1px solid #DDDDDD; border-bottom: 1px solid #DDDDDD; background-color: #EFEFEF; font-weight: bold; text-align: left; padding: 7px; color: #222222;">Image</td>
										  
											<td style="font-size: 12px; border-right: 1px solid #DDDDDD; border-bottom: 1px solid #DDDDDD; background-color: #EFEFEF; font-weight: bold; text-align: left; padding: 7px; color: #222222;">Product</td>
											
											<td style="font-size: 12px; border-right: 1px solid #DDDDDD; border-bottom: 1px solid #DDDDDD; background-color: #EFEFEF; font-weight: bold; text-align: right; padding: 7px; color: #222222;">QTY</td>
											
											<td style="font-size: 12px; border-right: 1px solid #DDDDDD; border-bottom: 1px solid #DDDDDD; background-color: #EFEFEF; font-weight: bold; text-align: right; padding: 7px; color: #222222;">Price</td>
											<td style="font-size: 12px; border-right: 1px solid #DDDDDD; border-bottom: 1px solid #DDDDDD; background-color: #EFEFEF; font-weight: bold; text-align: right; padding: 7px; color: #222222;">Total</td>
										  </tr>
										</thead>
									<tbody>';

								foreach ($data['products'] as $product) {

									$product_information .= '<tr>
											<td style="font-size:12px;border-right:1px solid #dddddd;border-bottom:1px solid #dddddd;text-align:left;padding:7px"><a href="' . $product['href'] . '"><img src="' . $product['thumb'] . '" title="' . $product['name'] . '" style="float:left;" /></a></td>
										  
											<td style="font-size: 12px;	border-right: 1px solid #DDDDDD; border-bottom: 1px solid #DDDDDD; text-align: left; padding: 7px;">';

									$product_information .= $product['name'] . '<br />' . $product['upc'];

									foreach ($product['option'] as $option) {
										$product_information .= '<br />&nbsp;<small> - ' . $option['name'] . ': ' . $option['value'] . '</small>';
									}

									$product_information .= '</td><td style="font-size: 12px;	border-right: 1px solid #DDDDDD; border-bottom: 1px solid #DDDDDD; text-align: right; padding: 7px;">' . $product['quantity'] . '</td>
											
											<td style="font-size: 12px;	border-right: 1px solid #DDDDDD; border-bottom: 1px solid #DDDDDD; text-align: right; padding: 7px;">' . $product['price'] . '</td>
											
											<td style="font-size: 12px;	border-right: 1px solid #DDDDDD; border-bottom: 1px solid #DDDDDD; text-align: right; padding: 7px;">' . $product['total'] . '</td>
											
										  </tr>';
								}


								foreach ($data['vouchers'] as $voucher) {
									$product_information .= '<tr>
											<td style="font-size: 12px;	border-right: 1px solid #DDDDDD; border-bottom: 1px solid #DDDDDD; text-align: left; padding: 7px;">' . $voucher['description'] . '</td>
											<td style="font-size: 12px;	border-right: 1px solid #DDDDDD; border-bottom: 1px solid #DDDDDD; text-align: left; padding: 7px;"></td>
											<td style="font-size: 12px;	border-right: 1px solid #DDDDDD; border-bottom: 1px solid #DDDDDD; text-align: right; padding: 7px;">1</td>
											<td style="font-size: 12px;	border-right: 1px solid #DDDDDD; border-bottom: 1px solid #DDDDDD; text-align: right; padding: 7px;">' . $voucher['amount'] . '</td>
											<td style="font-size: 12px;	border-right: 1px solid #DDDDDD; border-bottom: 1px solid #DDDDDD; text-align: right; padding: 7px;">' . $voucher['amount'] . '</td>
										</tr>';
								}

								$product_information .= '</tbody><tfoot>';
								foreach ($data['totals'] as $total) {
									$product_information .= '<tr>
											<td style="font-size: 12px;	border-right: 1px solid #DDDDDD; border-bottom: 1px solid #DDDDDD; text-align: right; padding: 7px;" colspan="4"><b>' . $total['title'] . ':</b></td>
											
											<td style="font-size: 12px;	border-right: 1px solid #DDDDDD; border-bottom: 1px solid #DDDDDD; text-align: right; padding: 7px;">' . $total['text'] . '</td>
										  </tr>';
								}
								$product_information .= '</tfoot></table>';


								$nowdateadd = strtotime(str_replace("/", "-", $date_added));
								$now = explode(' - ', $data['delivery_time']);
								$nowdate = explode(' ', $now[0]);
								if ($nowdateadd == strtotime($nowdate[0])) {
									$now = 'NOW';
								} else {
									$now = 'LATER';
								}

								$tags = array("[ORDER_ID]", "[ORDER_LINK]", "[STORE_NAME]", "[DATE_ADDED]", "[DELIVERY_DATE_TIME]", "[DELIVERY_DATE_NOW]", "[PAYMENT_METHOD]", "[SHIPPING_METHOD]", "[CUSTOMER_EMAIL]", "[MOBILE_NO]", "[ORDER_STATUS]", "[COMMENTS]", "[DELIVERY_ADDRESS]", "[PRODUCT_INFORMATION]", "[SUBJECT]");

								$tagsValues = array($order_id, $order_link, $data['seller_name'], $date_added, $data['delivery_time'], $now, $data['payment_method'], $data['shipping_method'], $data['email'], $data['telephone'], $data['order_status'], $comments, $data['shipping_address'], $product_information, $template['subject']);


								//get subject
								$subject = html_entity_decode($template['subject'], ENT_QUOTES, 'UTF-8');
								$subject = str_replace($tags, $tagsValues, $subject);
								$subject = html_entity_decode($subject, ENT_QUOTES);

								//get message body
								$msg = html_entity_decode($template['description'], ENT_QUOTES, 'UTF-8');
								$msg = str_replace($tags, $tagsValues, $msg);
								$msg = html_entity_decode($msg, ENT_QUOTES);

								$header = html_entity_decode($this->config->get('config_mail_header'), ENT_QUOTES, 'UTF-8');
								$header = str_replace($tags, $tagsValues, $header);
								$message = html_entity_decode($header, ENT_QUOTES);
								$message .= $msg;
								$message .= html_entity_decode($this->config->get('config_mail_footer'), ENT_QUOTES, 'UTF-8');

								$mail = new Mail();
								$mail->protocol = $this->config->get('config_mail_protocol');
								$mail->parameter = $this->config->get('config_mail_parameter');
								$mail->smtp_hostname = $this->config->get('config_mail_smtp_hostname');
								$mail->smtp_username = $this->config->get('config_mail_smtp_username');
								$mail->smtp_password = html_entity_decode($this->config->get('config_mail_smtp_password'), ENT_QUOTES, 'UTF-8');
								$mail->smtp_port = $this->config->get('config_mail_smtp_port');
								$mail->smtp_timeout = $this->config->get('config_mail_smtp_timeout');

								$mail->setTo($order_info['email']);
								$mail->setFrom($this->config->get('config_email'));
								$mail->setSender(html_entity_decode($order_info['store_name'], ENT_QUOTES, 'UTF-8'));
								$mail->setSubject(html_entity_decode($subject, ENT_QUOTES, 'UTF-8'));
								$mail->setHtml($message);
								//$mail->setText($text);
								$mail->send();
							}

							// Admin Alert Mail
							if ($this->config->get('config_email')) {
								$subject = sprintf($language->get('text_new_subject'), html_entity_decode($this->config->get('config_name'), ENT_QUOTES, 'UTF-8'), $order_id);

								// HTML Mail
								$data['text_greeting'] = '';
								//$data['text_greeting'] = $language->get('text_new_received');

								if ($comment) {
									$data['comment'] = nl2br($comment);
								} else if ($order_info['comment']) {
									$data['comment'] = $order_info['comment'];
								} else {
									$data['comment'] = '';
								}

								$data['text_download'] = '';

								$data['text_footer'] = '';

								$data['text_link'] = '';
								$data['link'] = '';
								$data['download'] = '';

								if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/template/mail/order.tpl')) {
									$html = $this->load->view($this->config->get('config_template') . '/template/mail/order.tpl', $data);
								} else {
									$html = $this->load->view('default/template/mail/order.tpl', $data);
								}

								// Text
								$text  = $language->get('text_new_received') . "\n\n";

								if ($order_info['comment']) {
									//$text .= $language->get('text_new_comment') . "\n\n";
									$text .= $order_info['comment'] . "\n\n";
								}

								$text .= $language->get('text_new_order_id') . ' ' . $order_id . "\n";
								$text .= $language->get('text_new_date_added') . ' ' . date($language->get('date_format_short'), strtotime($order_info['date_added'])) . "\n";
								$text .= $language->get('text_new_order_status') . ' ' . $order_status . "\n\n";
								$text .= $language->get('text_new_products') . "\n";

								foreach ($order_product_query->rows as $product) {
									$text .= $product['quantity'] . 'x ' . $product['name'] . ' (' . $product['model'] . ') ' . html_entity_decode($this->currency->format($product['total'] + ($this->config->get('config_tax') ? ($product['tax'] * $product['quantity']) : 0), $order_info['currency_code'], $order_info['currency_value']), ENT_NOQUOTES, 'UTF-8') . "\n";

									$order_option_query = $this->db->query("SELECT * FROM " . DB_PREFIX . "order_option WHERE order_id = '" . (int) $order_id . "' AND order_product_id = '" . $product['order_product_id'] . "'");

									foreach ($order_option_query->rows as $option) {
										if ($option['type'] != 'file') {
											$value = $option['value'];
										} else {
											$value = utf8_substr($option['value'], 0, utf8_strrpos($option['value'], '.'));
										}

										$text .= chr(9) . '-' . $option['name'] . ' ' . (utf8_strlen($value) > 20 ? utf8_substr($value, 0, 20) . '..' : $value) . "\n";
									}
								}

								foreach ($order_voucher_query->rows as $voucher) {
									$text .= '1x ' . $voucher['description'] . ' ' . $this->currency->format($voucher['amount'], $order_info['currency_code'], $order_info['currency_value']);
								}

								$text .= "\n";

								$text .= $language->get('text_new_order_total') . "\n";

								foreach ($order_total_query->rows as $total) {
									$text .= $total['title'] . ': ' . html_entity_decode($this->currency->format($total['value'], $order_info['currency_code'], $order_info['currency_value']), ENT_NOQUOTES, 'UTF-8') . "\n";
								}

								$template = $this->model_catalog_product->getEmailTemplate('customer-order-place-admin');

								$order_link = $order_info['store_url'] . 'index.php?route=account/order/info&order_id=' . $order_id;

								$date_added = date($language->get('date_format_short'), strtotime($order_info['date_added']));

								if ($comment) {
									$comments = '<table style="border-collapse: collapse; width: 100%; border-top: 1px solid #DDDDDD; border-left: 1px solid #DDDDDD; margin-bottom: 20px;">
												<thead> <tr> <td style="font-size: 12px; border-right: 1px solid #DDDDDD; border-bottom: 1px solid #DDDDDD; background-color: #EFEFEF; font-weight: bold; text-align: left; padding: 7px; color: #222222;">' . $data['text_instruction'] . '</td>  </tr> </thead> <tbody> <tr> <td style="font-size: 12px;	border-right: 1px solid #DDDDDD; border-bottom: 1px solid #DDDDDD; text-align: left; padding: 7px;">' . $comment . '</td>  </tr>	</tbody> </table>';
								} else {
									$comments = '';
								}

								$product_information = '<table style="border-collapse:collapse; width:100%; border-top:1px solid #DDDDDD; border-left:1px solid #DDDDDD; margin-bottom:20px;">
										<thead> <tr>
											<td style="font-size: 12px; border-right: 1px solid #DDDDDD; border-bottom: 1px solid #DDDDDD; background-color: #EFEFEF; font-weight: bold; text-align: left; padding: 7px; color: #222222;">Image</td>
										  
											<td style="font-size: 12px; border-right: 1px solid #DDDDDD; border-bottom: 1px solid #DDDDDD; background-color: #EFEFEF; font-weight: bold; text-align: left; padding: 7px; color: #222222;">Product</td>
											
											<td style="font-size: 12px; border-right: 1px solid #DDDDDD; border-bottom: 1px solid #DDDDDD; background-color: #EFEFEF; font-weight: bold; text-align: right; padding: 7px; color: #222222;">QTY</td>
											
											<td style="font-size: 12px; border-right: 1px solid #DDDDDD; border-bottom: 1px solid #DDDDDD; background-color: #EFEFEF; font-weight: bold; text-align: right; padding: 7px; color: #222222;">Price</td>
											<td style="font-size: 12px; border-right: 1px solid #DDDDDD; border-bottom: 1px solid #DDDDDD; background-color: #EFEFEF; font-weight: bold; text-align: right; padding: 7px; color: #222222;">Total</td>
										  </tr>
										</thead>
									<tbody>';

								foreach ($data['products'] as $product) {

									$product_information .= '<tr>
											<td style="font-size:12px;border-right:1px solid #dddddd;border-bottom:1px solid #dddddd;text-align:left;padding:7px"><a href="' . $product['href'] . '"><img src="' . $product['thumb'] . '" title="' . $product['name'] . '" style="float:left;" /></a></td>
										  
											<td style="font-size: 12px;	border-right: 1px solid #DDDDDD; border-bottom: 1px solid #DDDDDD; text-align: left; padding: 7px;">';

									$product_information .= $product['name'] . '<br />' . $product['upc'];

									foreach ($product['option'] as $option) {
										$product_information .= '<br />&nbsp;<small> - ' . $option['name'] . ': ' . $option['value'] . '</small>';
									}

									$product_information .= '</td><td style="font-size: 12px;	border-right: 1px solid #DDDDDD; border-bottom: 1px solid #DDDDDD; text-align: right; padding: 7px;">' . $product['quantity'] . '</td>
											
											<td style="font-size: 12px;	border-right: 1px solid #DDDDDD; border-bottom: 1px solid #DDDDDD; text-align: right; padding: 7px;">' . $product['price'] . '</td>
											
											<td style="font-size: 12px;	border-right: 1px solid #DDDDDD; border-bottom: 1px solid #DDDDDD; text-align: right; padding: 7px;">' . $product['total'] . '</td>
											
										  </tr>';
								}


								foreach ($data['vouchers'] as $voucher) {
									$product_information .= '<tr>
											<td style="font-size: 12px;	border-right: 1px solid #DDDDDD; border-bottom: 1px solid #DDDDDD; text-align: left; padding: 7px;">' . $voucher['description'] . '</td>
											<td style="font-size: 12px;	border-right: 1px solid #DDDDDD; border-bottom: 1px solid #DDDDDD; text-align: left; padding: 7px;"></td>
											<td style="font-size: 12px;	border-right: 1px solid #DDDDDD; border-bottom: 1px solid #DDDDDD; text-align: right; padding: 7px;">1</td>
											<td style="font-size: 12px;	border-right: 1px solid #DDDDDD; border-bottom: 1px solid #DDDDDD; text-align: right; padding: 7px;">' . $voucher['amount'] . '</td>
											<td style="font-size: 12px;	border-right: 1px solid #DDDDDD; border-bottom: 1px solid #DDDDDD; text-align: right; padding: 7px;">' . $voucher['amount'] . '</td>
										</tr>';
								}

								$product_information .= '</tbody><tfoot>';
								foreach ($data['totals'] as $total) {
									$product_information .= '<tr>
											<td style="font-size: 12px;	border-right: 1px solid #DDDDDD; border-bottom: 1px solid #DDDDDD; text-align: right; padding: 7px;" colspan="4"><b>' . $total['title'] . ':</b></td>
											
											<td style="font-size: 12px;	border-right: 1px solid #DDDDDD; border-bottom: 1px solid #DDDDDD; text-align: right; padding: 7px;">' . $total['text'] . '</td>
										  </tr>';
								}
								$product_information .= '</tfoot></table>';


								$nowdateadd = strtotime(str_replace("/", "-", $date_added));
								$now = explode(' - ', $data['delivery_time']);
								$nowdate = explode(' ', $now[0]);
								if ($nowdateadd == strtotime($nowdate[0])) {
									$now = 'NOW';
								} else {
									$now = 'LATER';
								}
								//goggle map link
								if (isset($order_info['shipping_address_1'])) {
									$googleaddress = $order_info['shipping_address_1'];
								}
								if (isset($order_info['shipping_address_2'])) {
									$googleaddress .= ' ' . $order_info['shipping_address_2'];
								}
								if (isset($order_info['shipping_address_2'])) {
									$googleaddress .= ' ' . $order_info['shipping_city'];
								}


								if ($order_info['shipping_address_format']) {
									$format = $order_info['shipping_address_format'];
								} else {
									$format = '{firstname} {lastname}' . "\n" . '{company}' . "\n" . '<a href="https://maps.google.com/?q=' . $googleaddress . '" target="_blank">{address_1}</a>' . "\n" . '{address_2}' . "\n" . '{city} {postcode}' . "\n" . '{zone}' . "\n" . '{country}';
								}

								$order_customer_store = $this->db->query("SELECT *  FROM `" . DB_PREFIX . "customer` WHERE `store` = 1 AND customer_id = " . $order_info['customer_id'] . "");

								if ($order_customer_store->num_rows) {
									$store = $order_customer_store->row['store'];
									$storename = ' - ' . $order_customer_store->row['storename'];
								} else {
									$store = '';
									$storename = '';
								}
								$find = array(
									'{firstname}',
									'{lastname}',
									'{company}',
									'{address_1}',
									'{address_2}',
									'{city}',
									'{postcode}',
									'{zone}',
									'{zone_code}',
									'{country}'
								);

								$replace = array(
									'firstname' => $order_info['shipping_firstname'],
									'lastname'  => $order_info['shipping_lastname'] . $storename,
									'company'   => $order_info['shipping_company'],
									'address_1' => $order_info['shipping_address_1'],
									'address_2' => $order_info['shipping_address_2'],
									'city'      => $order_info['shipping_city'],
									'postcode'  => $order_info['shipping_postcode'],
									'zone'      => $order_info['shipping_zone'],
									'zone_code' => $order_info['shipping_zone_code'],
									'country'   => $order_info['shipping_country']
								);

								$data['shipping_address'] = str_replace(array("\r\n", "\r", "\n"), '<br />', preg_replace(array("/\s\s+/", "/\r\r+/", "/\n\n+/"), '<br />', trim(str_replace($find, $replace, $format))));

								$tags = array("[ORDER_ID]", "[ORDER_LINK]", "[STORE_NAME]", "[DATE_ADDED]", "[DELIVERY_DATE_TIME]", "[PAYMENT_METHOD]", "[DELIVERY_DATE_NOW]", "[SHIPPING_METHOD]", "[CUSTOMER_EMAIL]", "[MOBILE_NO]", "[ORDER_STATUS]", "[COMMENTS]", "[DELIVERY_ADDRESS]", "[PRODUCT_INFORMATION]", "[SUBJECT]");

								$tagsValues = array($order_id, $order_link, $data['seller_name'], $date_added, $data['delivery_time'], $data['payment_method'], $now, $data['shipping_method'], $data['email'], $data['telephone'], $data['order_status'], $comments, $data['shipping_address'], $product_information, $template['subject']);


								//get subject
								$subject = html_entity_decode($template['subject'], ENT_QUOTES, 'UTF-8');
								$subject = str_replace($tags, $tagsValues, $subject);
								$subject = html_entity_decode($subject, ENT_QUOTES);

								//get message body
								$msg = html_entity_decode($template['description'], ENT_QUOTES, 'UTF-8');
								$msg = str_replace($tags, $tagsValues, $msg);
								$msg = html_entity_decode($msg, ENT_QUOTES);

								$header = html_entity_decode($this->config->get('config_mail_header'), ENT_QUOTES, 'UTF-8');
								$header = str_replace($tags, $tagsValues, $header);
								$message = html_entity_decode($header, ENT_QUOTES);
								$message .= $msg;
								$message .= html_entity_decode($this->config->get('config_mail_footer'), ENT_QUOTES, 'UTF-8');

								$mail = new Mail();
								$mail->protocol = $this->config->get('config_mail_protocol');
								$mail->parameter = $this->config->get('config_mail_parameter');
								$mail->smtp_hostname = $this->config->get('config_mail_smtp_hostname');
								$mail->smtp_username = $this->config->get('config_mail_smtp_username');
								$mail->smtp_password = html_entity_decode($this->config->get('config_mail_smtp_password'), ENT_QUOTES, 'UTF-8');
								$mail->smtp_port = $this->config->get('config_mail_smtp_port');
								$mail->smtp_timeout = $this->config->get('config_mail_smtp_timeout');

								$mail->setTo($this->config->get('config_email'));
								$mail->setFrom($this->config->get('config_email'));
								$mail->setSender(html_entity_decode($order_info['store_name'], ENT_QUOTES, 'UTF-8'));
								$mail->setSubject(html_entity_decode($subject, ENT_QUOTES, 'UTF-8'));
								$mail->setHtml($message);
								//$mail->setText($text);

								$mail->send();

								// Send to additional alert emails
								$emails = explode(',', $this->config->get('config_mail_alert'));

								foreach ($emails as $email) {
									if ($email && preg_match('/^[^\@]+@.*.[a-z]{2,15}$/i', $email)) {
										$mail->setTo($email);
										$mail->send();
									}
								}
							}
						}

						// If order status is not 0 then send update text email
						if ($order_info['order_status_id'] && $order_status_id && $notify) {
							$language = new Language($order_info['language_directory']);
							$language->load($order_info['language_directory']);
							$language->load('mail/order');
							$order_status_query = $this->db->query("SELECT * FROM " . DB_PREFIX . "order_status WHERE order_status_id = '" . (int) $order_status_id . "' AND language_id = '" . (int) $order_info['language_id'] . "'");
							$order_status = $order_status_query->row['name'];
							$cus_name = $order_info['shipping_firstname'] . " " . $order_info['shipping_lastname'];
							if ($comment) {
								$comment_text = $language->get('text_update_comment');
								$comment_message = strip_tags($comment);
							}
							$logo_link = $order_info['store_url'] . 'image/catalog/salatty_icons/careeb_small_logo.png';
							$subject = sprintf($language->get('text_update_subject'), html_entity_decode($order_info['store_name'], ENT_QUOTES, 'UTF-8'), $order_id);
							$message  = '<html dir="ltr" lang="en">' . "\n";
							$message .= '  <head>' . "\n";
							$message .= '    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">' . "\n";
							$message .= '  </head>' . "\n";
							$message .= '  <body> <img style="margin-left:85px" src="' . $logo_link . '" />' . "\n";
							$message .= '  <p>Dear ' . $cus_name . ' </p>' . "\n";
							$message .= '  <p>Thank you for using careeb.com for online Grocery Purchasing, Your order No ' . $order_id . ' has new status; </p>' . "\n";
							$message .= '  <p>Order Status: ' . $order_status . ' </p>' . "\n";
							$message .= '  <p>To view your order latest status please visit link below,  </p>' . "\n";
							$message .= '  <p> ' . $order_info['store_url'] . 'index.php?route=account/order/info&order_id=' . $order_id . ' </p>' . "\n\n";
							$message .= $comment_text . "\n\n";
							$message .= $comment_message . "\n";
							$message .= '  <p>in case having issue or Question please communicate with us at support@careeb.com </p>';
							$message .= '</body></html>' . "\n";

							$this->load->model('catalog/product');

							$template = $this->model_catalog_product->getEmailTemplate('admin-update-order-status');

							$order_link .= $order_info['store_url'] . 'index.php?route=account/order/info&order_id=' . $order_id;

							$tags = array("[CUSTOMER_NAME]", "[ORDER_STATUS]", "[ORDER_ID]", "[ORDER_LINK]", "[SUBJECT]");
							$tagsValues = array($cus_name, $order_status, $order_id, $order_link, $template['subject']);

							//get subject
							$subject = html_entity_decode($template['subject'], ENT_QUOTES, 'UTF-8');
							$subject = str_replace($tags, $tagsValues, $subject);
							$subject = html_entity_decode($subject, ENT_QUOTES);

							//get message body
							$msg = html_entity_decode($template['description'], ENT_QUOTES, 'UTF-8');
							$msg = str_replace($tags, $tagsValues, $msg);
							$msg = html_entity_decode($msg, ENT_QUOTES);

							$header = html_entity_decode($this->config->get('config_mail_header'), ENT_QUOTES, 'UTF-8');
							$header = str_replace($tags, $tagsValues, $header);
							$message = html_entity_decode($header, ENT_QUOTES);
							$message .= $msg;
							$message .= html_entity_decode($this->config->get('config_mail_footer'), ENT_QUOTES, 'UTF-8');

							$mail = new Mail();
							$mail->protocol = $this->config->get('config_mail_protocol');
							$mail->parameter = $this->config->get('config_mail_parameter');
							$mail->smtp_hostname = $this->config->get('config_mail_smtp_hostname');
							$mail->smtp_username = $this->config->get('config_mail_smtp_username');
							$mail->smtp_password = html_entity_decode($this->config->get('config_mail_smtp_password'), ENT_QUOTES, 'UTF-8');
							$mail->smtp_port = $this->config->get('config_mail_smtp_port');
							$mail->smtp_timeout = $this->config->get('config_mail_smtp_timeout');

							$mail->setTo($order_info['email']);
							$mail->setFrom($this->config->get('config_email'));
							$mail->setSender(html_entity_decode($order_info['store_name'], ENT_QUOTES, 'UTF-8'));
							$mail->setSubject(html_entity_decode($subject, ENT_QUOTES, 'UTF-8'));
							$mail->setHtml($message);
							$mail->send();
						}

						// If order status in the complete range create any vouchers that where in the order need to be made available.
						if (in_array($order_info['order_status_id'], $this->config->get('config_complete_status'))) {
							// Send out any gift voucher mails
							$this->load->model('checkout/voucher');

							$this->model_checkout_voucher->confirm($order_id);
						}

						//Notification Send Start
						//customer other device_id in notification Send
						$notifications = array();
						$notifications = $this->model_checkout_order->sendCustomerNotification($this->request->post['customer_id']);

						if (count($notifications) > 0) {

							foreach ($notifications as $notification) {
								if ($notification['device_type'] == 1) {
									$this->sendMessageOrderPlace($notification['device_id'], $order_id, $seller_info->row['firstname'], $seller_info->row['lastname'], $this->request->post['language_id']);
								} else if ($notification['device_type'] == 2) {
									$this->sendMessageOrderPlaceIOS($notification['device_id'], $order_id, $seller_info->row['firstname'], $seller_info->row['lastname'], $this->request->post['language_id']);
								}
							}
						}

						$this->db->query("INSERT INTO " . DB_PREFIX . "order_history SET order_id = '" . (int) $order_id . "', order_status_id = '2', date_added = NOW()");
						$this->db->query("UPDATE " . DB_PREFIX . "order SET `order_status_id` = '2' WHERE `oc_order`.`order_id` = '" . (int) $order_id . "'");

						//Notification Send end */
						$myfile = fopen("orderData/" . $order_id . "_return.txt", "w") or die("Unable to open file!");
						$txt = "";
						$txt .= "\n confirmPaymentNotificationtest order_id =" . $order_id;
						fwrite($myfile, $txt);
						fclose($myfile);
						$this->event->trigger('post.order.history.add', $order_id);
						$output = array("message" => 'Success');
						echo json_encode($output);
					} else {
						$output = array("message" => 'Failed');
						echo json_encode($output);
					}
				}
			} else {
				$output = array("message" => 'Failed');
				echo json_encode($output);
			}
		} else {
			$output = array("message" => 'Failed');
			echo json_encode($output);
		}
	}

	public function confirmOrderV3New3()
	{

		$this->load->language('total/coupon');
		$couponMsg = $this->language->get('error_coupon');

		/* --------------------------------------- */
		$this->load->model('store/store');

		//City_id		
		$city_id = 0;
		if (isset($this->request->post['city_id'])) {
			$city_id = $this->request->post['city_id'];
		}

		$this->request->post['sequence'] = $this->model_store_store->getMaxOrderSequence();
		$deviceId = $this->request->post['device_id'];
		$allProducts = $this->cart->getMobileProductsNew($this->request->post['customer_id'], $this->request->post['language_id'], $deviceId, $city_id);


		foreach ($allProducts as $key1 => $value1) {
			$total = 0;
			$storeId = $key1;
			$sellerInfo = $this->model_store_store->getSellerByID($storeId);

			$deliveryCharges = (isset($sellerInfo) && $sellerInfo['delivery_charges']) ? (int) $sellerInfo['delivery_charges'] : 0;

			foreach ($value1 as $key => $value) {
				$total = $total + $value['total'];
			}

			$allProductsNew[] = array(
				'seller_id'       => $key1,
				'seller_nameen'   => $sellerInfo['firstname'],
				'seller_namear'   => $sellerInfo['lastname'],
				'delivery_charges' => $deliveryCharges,
				'minimum_order'    => (int) $sellerInfo['minimum_order'],
				'seller_total' => $total,
				'store_credit'     => $this->currency->format($amount, $this->config->get('config_currency')),
				'products'         => $value1
			);
			
		}

		$order_data = array();

		$order_data['totals'] = array();
		$total = 0;

		//$cart_info = $this->db->query("SELECT * FROM " . DB_PREFIX . "cart WHERE customer_id = '" . $this->request->post['customer_id'] . "'");

		$balance = $this->db->query("SELECT SUM(amount) AS total FROM " . DB_PREFIX . "customer_transaction WHERE customer_id = '" . $this->request->post['customer_id'] . "'");

		foreach ($balance->rows as $balance_total) {
			$credit_total = $balance_total['total'];
		}

		$isCoupon = true;
		//if (count($cart_info->rows) > 0) {
		if (count($allProductsNew) > 0) {

			$voucher = 0;

			if (isset($this->request->post['coupon']) && $this->request->post['coupon'] != 'undefined' && $this->request->post['coupon'] != 'null') {

				$this->load->model('total/coupon');

				$coupon = $this->request->post['coupon'];

				$customer = (int) $this->request->post['customer_id'];
				$device = $this->request->post['device_id'];
				$language = 1;

				$coupon_info = $this->model_total_coupon->getMobileCoupon($coupon, $customer, $language, $device);

				$isCoupon = ($coupon_info) ? true : false;

				$voucher_info = $this->model_total_coupon->getMobileTotalNew($coupon, $customer, $language, $device, $allProductsNew[0]['seller_id']);
				$couponId = $coupon_info['coupon_id'];

				if ($voucher_info) {
					$voucher = $voucher_info[0]['value'];
				}
			}



			if ($isCoupon) {
				$orderTotal = 0;
				foreach ($allProductsNew as $cart_infoNew) {

					$i = 0;
					$total = 0;

					//$seller_id = (isset($this->request->post['seller_id'])) ? $this->request->post['seller_id'] : 1;
					$seller_id = $cart_infoNew['seller_id'];

					$this->load->model('catalog/product');

					$this->load->language('checkout/checkout');

					$order_data['invoice_prefix'] = $this->config->get('config_invoice_prefix');
					$order_data['store_id'] = $this->config->get('config_store_id');
					$order_data['store_name'] = $this->config->get('config_name') . " - Mobile App";
					$order_data['seller_id'] = $seller_id;

					if ($order_data['store_id']) {
						$order_data['store_url'] = $this->config->get('config_url');
					} else {
						$order_data['store_url'] = HTTP_SERVER;
					}

					$this->load->model('account/customer');

					$customer_info = $this->model_account_customer->getCustomer($this->request->post['customer_id']); //******************************************8

					$address_info = $this->db->query("SELECT DISTINCT * FROM " . DB_PREFIX . "address WHERE address_id = '" . (int) $customer_info['address_id'] . "' AND customer_id = '" . (int) $customer_info['customer_id'] . "'");

					$this->load->model('localisation/zone');
					$this->load->model('localisation/country');

					$zone_info = $this->model_localisation_zone->getZone($address_info->row['zone_id']);
					$country_info = $this->model_localisation_country->getCountry($address_info->row['country_id']);

					$order_data['customer_id'] = $this->request->post['customer_id']; //******************************************8
					$order_data['customer_group_id'] = $customer_info['customer_group_id'];
					$order_data['firstname'] = $customer_info['firstname'];
					$order_data['lastname'] = $customer_info['lastname'];
					$order_data['email'] = $customer_info['email'];
					$order_data['telephone'] = $customer_info['telephone'];
					$order_data['fax'] = $customer_info['fax'];
					$order_data['custom_field'] = unserialize($customer_info['custom_field']);

					if (isset($zone_info['name'])) {
						$payment_zone = $zone_info['name'];
					} else {
						$payment_zone = '';
					}

					$order_data['payment_firstname'] = $customer_info['firstname'];
					$order_data['payment_lastname'] = $customer_info['lastname'];
					$order_data['payment_company'] = $address_info->row['company'];
					$order_data['payment_address_1'] = $address_info->row['address_1'];
					$order_data['payment_address_2'] = $address_info->row['address_2'];
					$order_data['payment_city'] = $address_info->row['city'];
					$order_data['payment_postcode'] = $address_info->row['postcode'];
					$order_data['payment_zone'] = $payment_zone;
					$order_data['payment_zone_id'] = $address_info->row['zone_id'];
					$order_data['payment_country'] = $country_info['name'];
					$order_data['payment_country_id'] = $address_info->row['country_id'];
					$order_data['payment_address_format'] = '';
					$order_data['payment_custom_field'] = '';
					$order_data['payment_method'] = $this->request->post['payment_method'];
					$order_data['payment_code'] = $this->request->post['payment_code'];

					$zoneId = getZoneId($this->request->post['input-shipping-zone']);
					$zone_info = $this->model_localisation_zone->getZone($zoneId);
					$country_info = $this->model_localisation_country->getCountry($this->request->post['input-shipping-country']);

					if (isset($zone_info['name'])) {
						$shipping_zone = $zone_info['name'];
					} else {
						$shipping_zone = '';
					}

					$order_data['shipping_firstname'] = $this->request->post['input-shipping-firstname'];
					$order_data['shipping_lastname'] = $this->request->post['input-shipping-lastname'];
					$order_data['shipping_company'] = $this->request->post['input-shipping-company'];
					$order_data['shipping_address_1'] = $this->request->post['input-shipping-address-1'];
					$order_data['shipping_address_2'] = $this->request->post['input-shipping-address-2'];
					$order_data['shipping_city'] = $this->request->post['input-shipping-city'];
					$order_data['shipping_postcode'] = $this->request->post['input-shipping-postcode'];
					$order_data['shipping_zone'] = $shipping_zone;
					$order_data['shipping_zone_id'] = $zoneId;
					$order_data['shipping_country'] = $country_info['name'];
					$order_data['shipping_country_id'] = $this->request->post['input-shipping-country'];
					$order_data['shipping_address_format'] = "";
					$order_data['shipping_custom_field'] = array();
					//$order_data['shipping_method']         = $shipping_method;
					$order_data['shipping_method'] = '';
					$order_data['shipping_code'] = $this->request->post['shipping_code'];

					$order_data['products'] = array();

					if (isset($_GET['input-default-address'])) {
						$this->db->query("UPDATE " . DB_PREFIX . "address SET firstname = '" . $this->db->escape($this->request->post['input-shipping-firstname']) . "', lastname = '" . $this->db->escape($this->request->post['input-shipping-lastname']) . "', company = '" . $this->db->escape($this->request->post['input-shipping-company']) . "', address_1 = '" . $this->db->escape($this->request->post['input-shipping-address-1']) . "', address_2 = '" . $this->db->escape($this->request->post['input-shipping-address-2']) . "', postcode = '" . $this->db->escape($this->request->post['input-shipping-postcode']) . "', city = '" . $this->db->escape($this->request->post['input-shipping-city']) . "', zone_id = '" . (int) $zoneId . "', country_id = '" . (int) $this->request->post['input-shipping-country'] . "' WHERE address_id  = '" . (int) $this->request->post['address_id'] . "' AND customer_id = '" . (int) $this->request->post['customer_id'] . "'");
					}

					$i = 0;
					//foreach ($cart_info->rows as $cart_data) {
					foreach ($cart_infoNew['products'] as $cart_data) {

						$prod_info = $this->model_catalog_product->getProduct($cart_data['product_id'], $seller_id);

						$option_price = 0;
						$option_points = 0;
						$option_weight = 0;

						$option_data = array();

						foreach (json_decode($cart_data['option']) as $product_option_id => $value) {

							$option_query = $this->db->query("SELECT po.product_option_id, po.option_id, od.name, o.type FROM " . DB_PREFIX . "product_option po LEFT JOIN `" . DB_PREFIX . "option` o ON (po.option_id = o.option_id) LEFT JOIN " . DB_PREFIX . "option_description od ON (o.option_id = od.option_id) WHERE po.product_option_id = '" . (int) $product_option_id . "' AND po.product_id = '" . (int) $cart_data['product_id'] . "' AND od.language_id = '" . (int) $this->request->post['language_id'] . "'");

							if ($option_query->num_rows) {

								if ($option_query->row['type'] == 'select' || $option_query->row['type'] == 'radio' || $option_query->row['type'] == 'image') {

									$option_value_query = $this->db->query("SELECT pov.option_value_id, ovd.name, pov.quantity, pov.subtract, pov.price, pov.price_prefix, pov.points, pov.points_prefix, pov.weight, pov.weight_prefix FROM " . DB_PREFIX . "product_option_value pov LEFT JOIN " . DB_PREFIX . "option_value ov ON (pov.option_value_id = ov.option_value_id) LEFT JOIN " . DB_PREFIX . "option_value_description ovd ON (ov.option_value_id = ovd.option_value_id) WHERE pov.product_option_value_id = '" . (int) $value . "' AND pov.product_option_id = '" . (int) $product_option_id . "' AND pov.seller_id = '" . (int) $seller_id . "' AND ovd.language_id = '" . (int) $this->request->post['language_id'] . "'");

									if ($option_value_query->num_rows) {
										if ($option_value_query->row['price_prefix'] == '+') {
											$option_price += $option_value_query->row['price'];
										} elseif ($option_value_query->row['price_prefix'] == '-') {
											$option_price -= $option_value_query->row['price'];
										}

										if ($option_value_query->row['points_prefix'] == '+') {
											$option_points += $option_value_query->row['points'];
										} elseif ($option_value_query->row['points_prefix'] == '-') {
											$option_points -= $option_value_query->row['points'];
										}

										if ($option_value_query->row['weight_prefix'] == '+') {
											$option_weight += $option_value_query->row['weight'];
										} elseif ($option_value_query->row['weight_prefix'] == '-') {
											$option_weight -= $option_value_query->row['weight'];
										}

										if ($option_value_query->row['subtract'] && (!$option_value_query->row['quantity'] || ($option_value_query->row['quantity'] < $cart_data['quantity']))) {
											$stock = false;
										}

										$option_data[] = array(
											'product_option_id'       => $product_option_id,
											'product_option_value_id' => $value,
											'option_id'               => $option_query->row['option_id'],
											'option_value_id'         => $option_value_query->row['option_value_id'],
											'name'                    => $option_query->row['name'],
											'value'                   => $option_value_query->row['name'],
											'type'                    => $option_query->row['type'],
											'quantity'                => $option_value_query->row['quantity'],
											'subtract'                => $option_value_query->row['subtract'],
											'price'                   => $option_value_query->row['price'],
											'price_prefix'            => $option_value_query->row['price_prefix'],
											'points'                  => $option_value_query->row['points'],
											'points_prefix'           => $option_value_query->row['points_prefix'],
											'weight'                  => $option_value_query->row['weight'],
											'weight_prefix'           => $option_value_query->row['weight_prefix']
										);
									}
								} elseif ($option_query->row['type'] == 'checkbox' && is_array($value)) {
									foreach ($value as $product_option_value_id) {
										$option_value_query = $this->db->query("SELECT pov.option_value_id, ovd.name, pov.quantity, pov.subtract, pov.price, pov.price_prefix, pov.points, pov.points_prefix, pov.weight, pov.weight_prefix FROM " . DB_PREFIX . "product_option_value pov LEFT JOIN " . DB_PREFIX . "option_value ov ON (pov.option_value_id = ov.option_value_id) LEFT JOIN " . DB_PREFIX . "option_value_description ovd ON (ov.option_value_id = ovd.option_value_id) WHERE pov.product_option_value_id = '" . (int) $product_option_value_id . "' AND pov.product_option_id = '" . (int) $product_option_id . "' AND ovd.language_id = '" . (int) $this->request->post['language_id'] . "'");

										if ($option_value_query->num_rows) {
											if ($option_value_query->row['price_prefix'] == '+') {
												$option_price += $option_value_query->row['price'];
											} elseif ($option_value_query->row['price_prefix'] == '-') {
												$option_price -= $option_value_query->row['price'];
											}

											if ($option_value_query->row['points_prefix'] == '+') {
												$option_points += $option_value_query->row['points'];
											} elseif ($option_value_query->row['points_prefix'] == '-') {
												$option_points -= $option_value_query->row['points'];
											}

											if ($option_value_query->row['weight_prefix'] == '+') {
												$option_weight += $option_value_query->row['weight'];
											} elseif ($option_value_query->row['weight_prefix'] == '-') {
												$option_weight -= $option_value_query->row['weight'];
											}

											if ($option_value_query->row['subtract'] && (!$option_value_query->row['quantity'] || ($option_value_query->row['quantity'] < $cart_data['quantity']))) {
												$stock = false;
											}

											$option_data[] = array(
												'product_option_id'       => $product_option_id,
												'product_option_value_id' => $product_option_value_id,
												'option_id'               => $option_query->row['option_id'],
												'option_value_id'         => $option_value_query->row['option_value_id'],
												'name'                    => $option_query->row['name'],
												'value'                   => $option_value_query->row['name'],
												'type'                    => $option_query->row['type'],
												'quantity'                => $option_value_query->row['quantity'],
												'subtract'                => $option_value_query->row['subtract'],
												'price'                   => $option_value_query->row['price'],
												'price_prefix'            => $option_value_query->row['price_prefix'],
												'points'                  => $option_value_query->row['points'],
												'points_prefix'           => $option_value_query->row['points_prefix'],
												'weight'                  => $option_value_query->row['weight'],
												'weight_prefix'           => $option_value_query->row['weight_prefix']
											);
										}
									}
								} elseif ($option_query->row['type'] == 'text' || $option_query->row['type'] == 'textarea' || $option_query->row['type'] == 'file' || $option_query->row['type'] == 'date' || $option_query->row['type'] == 'datetime' || $option_query->row['type'] == 'time') {
									$option_data[] = array(
										'product_option_id'       => $product_option_id,
										'product_option_value_id' => '',
										'option_id'               => $option_query->row['option_id'],
										'option_value_id'         => '',
										'name'                    => $option_query->row['name'],
										'value'                   => $value,
										'type'                    => $option_query->row['type'],
										'quantity'                => '',
										'subtract'                => '',
										'price'                   => '',
										'price_prefix'            => '',
										'points'                  => '',
										'points_prefix'           => '',
										'weight'                  => '',
										'weight_prefix'           => ''
									);
								}
							}
						}

						if ($prod_info['special'] == null) {
							$total = ($total + (($prod_info['price'] + $option_price) * $cart_data['quantity']));
						} else {
							$total = ($total + (($prod_info['special'] + $option_price) * $cart_data['quantity']));
						}


						if ($prod_info['special'] == null) {
							$order_data['products'][] = array(
								'product_id' => $prod_info['product_id'],
								'name' => $prod_info['name'],
								'model' => $prod_info['model'],
								'option' => $option_data,
								'download' => array(),
								'quantity' => $cart_data['quantity'],
								'subtract' => $prod_info['subtract'],
								'price'    => ($prod_info['price'] + $option_price),
								'total'   => ($prod_info['price'] + $option_price) * $cart_data['quantity'],
								'tax' => 0,
								'reward' => 0,
								'seller_id' => $seller_id,
							);
							$i++;
						} else {
							$order_data['products'][] = array(
								'product_id' => $prod_info['product_id'],
								'name' => $prod_info['name'],
								'model' => $prod_info['model'],
								'option' => $option_data,
								'download' => array(),
								'quantity' => $cart_data['quantity'],
								'subtract' => $prod_info['subtract'],
								'price'    => ($prod_info['special'] + $option_price),
								'total'   => ($prod_info['special'] + $option_price) * $cart_data['quantity'],
								'tax' => 0,
								'reward' => 0,
								'seller_id' => $seller_id,
							);
							$i++;
						}
					}

					// Gift Voucher
					$order_data['vouchers'] = array();

					if (!empty($this->session->data['vouchers'])) {
						foreach ($this->session->data['vouchers'] as $voucher) {
							$order_data['vouchers'][] = array(
								'description' => $voucher['description'],
								'code' => substr(md5(mt_rand()), 0, 10),
								'to_name' => $voucher['to_name'],
								'to_email' => $voucher['to_email'],
								'from_name' => $voucher['from_name'],
								'from_email' => $voucher['from_email'],
								'voucher_theme_id' => $voucher['voucher_theme_id'],
								'message' => $voucher['message'],
								'amount' => $voucher['amount'],
							);
						}
					}

					$deliveryCharges = 0;
					$deliveryCharges = $cart_infoNew['delivery_charges'];

					$deliveryChargesArr = array();

					$deliveryChargesArr[] = array(
						'delivery_charges' => $deliveryCharges,
						'seller_id' => $seller_id,
						'firstname' => $cart_infoNew['seller_nameen'],
						'lastname' => $cart_infoNew['seller_namear']
					);

					$this->session->data['deliveryChargesArr'] = '';
					$this->session->data['deliveryChargesArr'] = $deliveryChargesArr;

					$order_data['comment'] = $this->request->post['order_comments'];
					$order_data['total'] = $total;

					if (isset($this->request->cookie['tracking'])) {
						$order_data['tracking'] = $this->request->cookie['tracking'];

						$subtotal = $this->cart->getSubTotal();

						// Affiliate
						$this->load->model('affiliate/affiliate');

						$affiliate_info = $this->model_affiliate_affiliate->getAffiliateByCode($this->request->cookie['tracking']);

						if ($affiliate_info) {
							$order_data['affiliate_id'] = $affiliate_info['affiliate_id'];
							$order_data['commission'] = ($subtotal / 100) * $affiliate_info['commission'];
						} else {
							$order_data['affiliate_id'] = 0;
							$order_data['commission'] = 0;
						}

						// Marketing
						$this->load->model('checkout/marketing');

						$marketing_info = $this->model_checkout_marketing->getMarketingByCode($this->request->cookie['tracking']);

						if ($marketing_info) {
							$order_data['marketing_id'] = $marketing_info['marketing_id'];
						} else {
							$order_data['marketing_id'] = 0;
						}
					} else {
						$order_data['affiliate_id'] = 0;
						$order_data['commission'] = 0;
						$order_data['marketing_id'] = 0;
						$order_data['tracking'] = '';
					}

					$order_data['language_id'] = $this->request->post['language_id'];
					$order_data['currency_id'] = $this->currency->getId();
					$order_data['currency_code'] = $this->currency->getCode();
					$order_data['currency_value'] = $this->currency->getValue($this->currency->getCode());
					$order_data['ip'] = $this->request->server['REMOTE_ADDR'];

					if (!empty($this->request->server['HTTP_X_FORWARDED_FOR'])) {
						$order_data['forwarded_ip'] = $this->request->server['HTTP_X_FORWARDED_FOR'];
					} elseif (!empty($this->request->server['HTTP_CLIENT_IP'])) {
						$order_data['forwarded_ip'] = $this->request->server['HTTP_CLIENT_IP'];
					} else {
						$order_data['forwarded_ip'] = '';
					}

					if (isset($this->request->server['HTTP_USER_AGENT'])) {
						$order_data['user_agent'] = $this->request->server['HTTP_USER_AGENT'];
					} else {
						$order_data['user_agent'] = '';
					}

					if (isset($this->request->server['HTTP_ACCEPT_LANGUAGE'])) {
						$order_data['accept_language'] = $this->request->server['HTTP_ACCEPT_LANGUAGE'];
					} else {
						$order_data['accept_language'] = '';
					}

					$total_data = array();

					$total_data[0] = array(
						'code' => 'sub_total',
						'title' => 'Sub-Total',
						'text' => 'Sub-Total',
						'value' => $total,
						'sort_order' => 0,
					);

					/* $total_data[1] = array(
						'code' => 'vat',
						'title' => $this->request->post['vat_text'],
						'value' => $this->request->post['vat_value'],
						'sort_order' => 1,
					); */

					$vat =  (float) $this->config->get('config_vat');
					$vat_total = 0;
					$vat_total = $total * $vat / ($vat + 100);

					$total_data[1] = array(
						'code' => 'vat',
						'title' => "Value Added Tax (".$vat."%)",
						'value' => $vat_total,
						'sort_order' => 1,
					);


					$isCouponRequired = false;
					if ((isset($voucher)) && ($voucher > 0)) {
						$isCouponRequired = true;
						//$total = $voucher_info[1];

						$total -= $voucher;

						$total_data[3] = array(
							'code' => 'coupon',
							'title' => $voucher_info[0]['title'],
							'value' => -$voucher,
							'sort_order' => 3,
						);
					}

					$this->load->model('checkout/order');
					if ($this->config->get('credit_status')) {
						$this->load->language('total/credit');

						if ((float) $credit_total) {
							if ($credit_total > $total) {
								$credit = $total;
							} else {
								$credit = $credit_total;
							}

							// if ($credit > 0) {
							$total -= $credit;
							$total_data[4] = array(
								'code' => 'credit',
								'title' => 'Store Credit',
								'value' => -$credit,
								'sort_order' => 4,
							);

							//}

							if ($credit_total > $credit) {
								$credit_total = $credit_total - $credit;
							} else {
								$credit_total = $credit - $credit_total;
							}
						}
					}

					$sellerInfo_custom = $this->model_store_store->getSellerByID($seller_id);

					if ($sellerInfo_custom['free_delivery_limit'] > $total or $sellerInfo_custom['free_delivery_limit'] == 0) {
						$total_data[5] = array(
							'code' => 'total',
							'title' => 'Total',
							'value' => $total + $deliveryCharges,
							'sort_order' => 5,
						);
					} else {
						$total_data[5] = array(
							'code' => 'total',
							'title' => 'Total',
							'value' => $total,
							'sort_order' => 5,
						);
					}
					// $total_data[5] = array(
					// 'code' => 'total',
					// 'title' => 'Total',
					// 'value' => $total + $deliveryCharges,
					// 'sort_order' => 5,
					// );

					$order_data['totals'] = $total_data;
					// vat 
						
						foreach($order_data['totals'] as $key=>$totals_data){
							if($totals_data['code']	== 'vat'){
								$vat =  (float)$this->config->get('config_vat');
								
								if(isset($vat) && $vat > 0){
								   $this->load->model('total/vat');
									$value = $this->{'model_total_vat'}->getTotalvalue($total, $vat);
									$order_data['totals'][$key]['value'] = round($value,2);
								}			
							}

						}
					//debugging($order_data);

					/* echo "<pre>";
					print_r($order_data);
					exit; */

					$sqlAll = "SELECT s.seller_id, s.firstname, s.lastname, s.package_ready, s.delivery_timegap, s.mon_start_time, s.mon_end_time, s.tue_start_time, s.tue_end_time, s.wed_start_time, s.wed_end_time, s.thu_start_time, s.thu_end_time, s.fri_start_time, s.fri_end_time, s.sat_start_time, s.sat_end_time, s.sun_start_time, s.sun_end_time, s.sortorder FROM " . DB_PREFIX . "sellers s WHERE s.seller_id = '" . $seller_id . "'";

					$queryAll = $this->db->query($sqlAll);
					$seller_info = $queryAll->row;

					$delivery_time_post = $this->getOrderNextDeliverySlot($seller_info, $this->request->post['delivery_time']);

					if (empty($delivery_time_post)) {
						$delivery_time_post = $this->request->post['delivery_time'];
					}

					$order_data['sequence'] = $this->request->post['sequence'];
					//store_area_id
						$order_data['store_area_id'] =0;
						if(isset($this->request->post['store_area_id'])){
							if($this->request->post['store_area_id'] != ''){
								$order_data['store_area_id'] = $this->request->post['store_area_id'];
							}
						}
						//store_area_id
					$order_id = $this->model_checkout_order->addOrderNew($order_data);

					// post data Write 
					$myfile = fopen("orderData/" . $order_id . ".txt", "w") or die("Unable to open file!");
					$txt = "confirmOrderV3New3test";

					foreach ($this->request->post as $keyNew => $valueNew) {
						$txt .= "\n" . $keyNew . "=" . $valueNew;
					}
					foreach ($order_data['products'] as $product_history) {
						$txt .= "\n \n product id = " . $product_history['product_id'] . " and quantity=" . $product_history['quantity'] . " and price =" . $product_history['price'] . " and option = " . $product_history['option'] . " and total= " . $product_history['total'];
					}
					foreach ($order_data['totals'] as $cart_total) {
						$txt .= "\n \n \n title = " . $cart_total['title'] . " and value=" . $cart_total['value'] . " and code =" . $cart_total['code'];
					}

					fwrite($myfile, $txt);
					fclose($myfile);


					// Add to activity log
					$this->load->model('account/activity');

					$activity_data = array(
						'customer_id' => $customer_info['customer_id'],
						'name' => $customer_info['firstname'] . ' ' . $customer_info['lastname'],
						'order_id' => $order_id,
					);

					$this->model_account_activity->addActivity('order_account', $activity_data);

					$this->load->model('checkout/order');
					$order_info = $this->model_checkout_order->getOrder($order_id);

					$order_total_query = $this->db->query("SELECT * FROM `" . DB_PREFIX . "order_total` WHERE order_id = '" . (int) $order_id . "' ORDER BY sort_order ASC");

					foreach ($order_total_query->rows as $order_total) {
						$this->load->model('total/' . $order_total['code']);

						if (method_exists($this->{'model_total_' . $order_total['code']}, 'confirm')) {
							$this->{'model_total_' . $order_total['code']}->confirm($order_info, $order_total);
						}
					}

					/* $output[] = array(
						"message" => true,
						"orderid" => $order_id,
						"total" => $total + $deliveryCharges,
						"payment_firstname" => $order_data['payment_firstname'],
						"payment_lastname" => $order_data['payment_lastname'],
						"email" => $order_data['email'],
						"telephone" => $order_data['telephone'],
						"address_1" => $order_data['payment_address_1'],
						"address_2" => $order_data['payment_address_2'],
					); */
					if ($sellerInfo_custom['free_delivery_limit'] > $total or $sellerInfo_custom['free_delivery_limit'] == 0) {
						$adddeliveryChargestotal =  $orderTotal + ($total + $deliveryCharges);
					} else {
						$adddeliveryChargestotal = 0;
					}
					$orderTotal = $adddeliveryChargestotal;
					$this->load->model('module/deliveryTime/order');
					$this->model_module_deliveryTime_order->addDeliveryTimeV2($order_id, $delivery_time_post);

					try {
						$this->model_checkout_order->addOrderHistoryNew($order_id, 1, $order_data['comment'], true);
					} catch (Exception $e) {
						//print_r($e);
						$output = array(
							"message" => 'Error in order Place.',
						);
						echo json_encode($output);
					}

					// Add Coupon History
					if ($isCouponRequired) {
						$this->model_total_coupon->couponHistory($couponId, $order_id, $customer, $voucher);
					}

					//coupon blank 
					$voucher = 0;

					//Notification Send Start
					//customer other device_id in notification Send
					$notifications = array();
					$notifications = $this->model_checkout_order->sendCustomerNotification($customer_info['customer_id']);

					if (count($notifications) > 0) {

						foreach ($notifications as $notification) {
							if ($notification['device_type'] == 1) {

								$this->sendMessageOrderPlace($notification['device_id'], $order_id, $seller_info['firstname'], $seller_info['lastname'], $this->request->post['language_id']);
							} else if ($notification['device_type'] == 2) {

								$this->sendMessageOrderPlaceIOS($notification['device_id'], $order_id, $seller_info['firstname'], $seller_info['lastname'], $this->request->post['language_id']);
							}
						}
					}
					//Notification Send end */
				}
				$myfile = fopen("orderData/" . $order_id . "_return.txt", "w") or die("Unable to open file!");
				$txt = "confirmOrderV3New3test";
				$txt .= "\n order_id =" . $order_id;
				fwrite($myfile, $txt);
				fclose($myfile);
				$output = array(
					"message" => true,
					"total" => $orderTotal
				);
			} else {
				$output = array("message" => $couponMsg);
			}

			/* --------------------------------------- */
		} else {
			$output = array(
				"message" => 'No product found in cart',
			);
		}

		echo json_encode($output);
	}
}
