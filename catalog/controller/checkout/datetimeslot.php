<?php
class ControllerCheckoutDatetimeslot extends Controller {
	public function index() {
		$this->load->language('checkout/datetimeslot');

		$data['text_date_select'] = $this->language->get('text_date_select');
		$data['text_time_select'] = $this->language->get('text_time_select');
		$this->load->language('checkout/checkout');
		$data['button_continue'] = $this->language->get('button_continue');
		$data['text_loading'] = $this->language->get('text_loading');
        $data['text_store_closed'] = $this->language->get('text_store_closed');
        
		$products = $this->cart->getProducts();

		$this->load->model('catalog/seller');
		$seller_time = $this->model_catalog_seller->getSeller($products[0]['seller_id']);

		$start_time = $seller_time[strtolower(date('D')).'_start_time'];
		$end_time = $seller_time[strtolower(date('D')).'_end_time'];
		$date = date('m/d/y');
		$days = array(
			strtolower('now') => date('l',strtotime($date)),
			strtolower(' + 1 day') => date('l',strtotime($date .'+ 1 day')), 
			strtolower(' + 2 day') => date('l',strtotime($date .'+ 2 day')), 
			strtolower(' + 3 day') => date('l',strtotime($date .'+ 3 day')), 
			strtolower(' + 4 day') => date('l',strtotime($date .'+ 4 day')), 
			strtolower(' + 5 day') => date('l',strtotime($date .'+ 5 day')), 
			strtolower(' + 6 day') => date('l',strtotime($date .'+ 6 day'))
		);
		
		$days_key = array(
			strtolower(date('D',strtotime($date))) => strtolower('now'),
			strtolower(date('D',strtotime($date .'+ 1 day'))) => strtolower(' + 1 day'),
			strtolower(date('D',strtotime($date .'+ 2 day'))) => strtolower(' + 2 day'),
			strtolower(date('D',strtotime($date .'+ 3 day'))) => strtolower(' + 3 day'),
			strtolower(date('D',strtotime($date .'+ 4 day'))) => strtolower(' + 4 day'),
			strtolower(date('D',strtotime($date .'+ 5 day'))) => strtolower(' + 5 day'),
			strtolower(date('D',strtotime($date .'+ 6 day'))) => strtolower(' + 6 day'),
		);
        //$currentDay =  date('D');
       
		foreach ($days_key as $key => $value) {
			if(strtotime($seller_time[$key.'_start_time']) == strtotime($seller_time[$key.'_end_time'])){
		
			  //  if($key != strtolower($currentDay)){
			     unset($days[$value]);
			  //  }
				
			}
		}

           $languageId = (int)$this->config->get('config_language_id');
           if($languageId==3){               
                foreach($days as $key => $value){               
                    $days[$key] = $this->language->get($value);
                } 
                
                 $data['day_slot']= $days;	           
           }else{
            $data['day_slot']= $days;	
           }

        	 
		$data['time_slots'] = $this->makeOneDaySlots($seller_time, 'now');

		if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/template/checkout/datetimeslot.tpl')) {
			$this->response->setOutput($this->load->view($this->config->get('config_template') . '/template/checkout/datetimeslot.tpl', $data));
		} else {
			$this->response->setOutput($this->load->view('default/template/checkout/datetimeslot.tpl', $data));
		}
	}

	public function timeslot_by_date(){
		$this->load->language('checkout/datetimeslot');
		$this->load->model('catalog/seller');
		$products = $this->cart->getProducts();

		$seller_time = $this->model_catalog_seller->getSeller($products[0]['seller_id']);
		
		$day = $this->request->post['date_slot'];

		$json['time_slots'] = $this->makeOneDaySlots($seller_time,$day);
        $json['button_continue'] = $this->language->get('button_continue');
        $json['text_store_closed'] = $this->language->get('text_store_closed');
		$this->response->addHeader('Content-Type: application/json');
		$this->response->setOutput(json_encode($json));
	}

	public function makeOneDaySlots($seller_info,$day){
		$now = strtotime("now");
        $languageId = (int)$this->config->get('config_language_id');
		$interval = $seller_info["delivery_timegap"] * 60;
		$package = $seller_info["package_ready"] * 60;
		$nowdata = dayData($day);

		if ( $seller_info[strtolower(substr($nowdata["half"], 0, 3)) . "_start_time"] != "00:00:00" && $seller_info[strtolower(substr($nowdata["half"], 0, 3)) . "_end_time"] != "00:00:00" ) {
			$nowstarttime = strtotime($nowdata["date"]. " " . $seller_info[strtolower(substr($nowdata["half"], 0, 3)) . "_start_time"]) + $interval;
			$nowendtime = strtotime($nowdata["date"]. " " . $seller_info[strtolower(substr($nowdata["half"], 0, 3)) . "_end_time"]);
			$j = 0;
			for( $i = $nowstarttime; $i < $nowendtime; $i += $interval) {
				$nntt = $i + $interval;
				if( $i >= $now ) {
				    if ( $j == 0 ) {
				      	$nextTime = $now + $package;
				      	if ($i >= $nextTime) {
							$d1 = date("d-m-Y h:i a", $i);
							$d2 = date("d-m-Y h:i a", $nntt);
						  
							$d3 = date("h:i a", $i);
							$d4 = date("h:i a", $nntt);
							
							if($languageId==3){  
								$d1 = arabicDate(strtotime($d1));
								$d2 =  arabicDate(strtotime($d2));
								
								$d3 = arabicDate(strtotime($d1));
								$d4 = arabicDate(strtotime($d2));
							}
				        	//$nowoutput['slots'][]= date("d-m-Y h:i a", $i)." - ".date("d-m-Y h:i a", $nntt); $j++;
                            $nowoutput['slots'][]= $d1." - ".$d2; $j++;
							$nowoutput['times'][]= $d3." - ".$d4; $j++;
				      	}
				    } else {
						$d1 = date("d-m-Y h:i a", $i);
						$d2 = date("d-m-Y h:i a", $nntt);
						
						$d3 = date("h:i a", $i);
						$d4 = date("h:i a", $nntt);
                         
						if($languageId==3){  
							$d1 = arabicDate(strtotime($d1));
							$d2 =  arabicDate(strtotime($d2));
							
							$d3 = arabicDate(strtotime($d3));
							$d4 = arabicDate(strtotime($d4));
						}
						$nowoutput['slots'][]= $d1." - ".$d2; $j++;
						$nowoutput['times'][]= $d3." - ".$d4; $j++;
				    }
			  	}
			}
			if (count($nowoutput['slots']) == 0) {
				$nowoutput['slots'] = array($this->language->get('text_store_closed'));
			}
		} else {
		  
			$nowoutput['slots'] = array($this->language->get('text_store_closed'));
		}
		return $nowoutput;
	}
	// public function makeOneDaySlots($start, $end, $interval, $date) {
	// 	// echo $starts
	// 	$data = array();
	// 	$starts = $date." ".$start;
	// 	$ends = $date." ".$end;

	// 	// start date
	// 	$starts = date("Y-m-d H:i:s", strtotime($starts));
	// 	// end date
	// 	$ends = date("Y-m-d H:i:s", strtotime($ends));

	// 	$date1 = new DateTime($starts);
	// 	$date2 = new DateTime($ends);
		
	// 	$diff = $date1->diff($date2);
		
	// 	$hour = $diff->h;
		
	// 	$interval = $interval;
	// 	$get_result = $hour/$interval);
	// 	$inter = 0;
	// 	// $inter = $inter - $interval;
	// 	for($i=1; $i<=$get_result; $i++):
	// 		$inter +=$interval;
	// 		$new_val =+ $inter + $interval;
	// 		if (date("H", strtotime("$starts + " .$inter. " hours")) <= date("H", strtotime("$starts + " .$new_val. " hours")) && date("d/m/Y H", strtotime("$starts + " .$inter. " hours")) >= date('d/m/Y H')) {
	// 			$save = date("d-m-Y h:i a", strtotime("$starts + " .$inter. " hours"))." "
	// 			. "- ".date("d-m-Y h:i a", strtotime("$starts + " .$new_val. " hours"));
	// 			$ToDate =  date("d-m-Y h:i a", strtotime("$starts + " .$inter. " hours"));
	// 			$Date   =  date("d-m-Y",strtotime($date));
	// 			$fromDate = date("d-m-Y h:i a", strtotime("$starts + " .$new_val. " hours"));
	// 			$data['slots'][] = $save;
	// 			$data['ToDate'][] = $ToDate;
	// 			$data['FromDate'][] = $fromDate;
	// 			$data['Date'][] = $Date;
	// 		}
	// 	endfor;
	// 	if (count($data['slots']) == 0) {
	// 		$data['slots'] = array('Store Closed');
	// 	}
	// 	return $data;
	// }

	public function save(){
		$json = array();
		if (!$this->customer->isLogged()) {
			$json['redirect'] = $this->url->link('checkout/checkout', '', 'SSL');
		}
	
		// Validate cart has products and has stock.
		if ((!$this->cart->hasProducts() && empty($this->session->data['vouchers'])) || (!$this->cart->hasStock() && !$this->config->get('config_stock_checkout'))) {
			$json['redirect'] = $this->url->link('checkout/cart');
		}

		$this->load->language('checkout/datetimeslot');
		if ($this->request->post['deliverytime'] == '') {
			$json['error']['warning'] = $this->language->get('error_datetime');
		}
		if (!$json) {
			$this->session->data['delivery_time'] = $this->request->post['deliverytime'];		
		}

		$this->response->addHeader('Content-Type: application/json');
		$this->response->setOutput(json_encode($json));
	}
	
	
	public function daysSlots() {
		
		$languageId= $this->request->get['language_id'];
		
		$this->session->data['language']  = ($languageId ==1)? 'en' :'ar';
		
		$this->load->language('checkout/datetimeslot');

		
		$this->load->language('checkout/checkout');	
       
        
	$sellerId= $this->request->get['seller_id'];
    
	
		$this->load->model('catalog/seller');
		$seller_time = $this->model_catalog_seller->getSeller($sellerId);

		if(isset($seller_time[strtolower(date('D')).'_start_time'])){
			$start_time = $seller_time[strtolower(date('D')).'_start_time'];
		} else {
			$start_time = '';
		}
		
		if(isset($seller_time[strtolower(date('D')).'_end_time'])){
			$end_time = $seller_time[strtolower(date('D')).'_end_time'];
		} else {
			$end_time = '';
		}
		
		$date = date('m/d/y');
		$days = array(
			strtolower('now') => date('l',strtotime($date)),
			strtolower(' + 1 day') => date('l',strtotime($date .'+ 1 day')), 
			strtolower(' + 2 day') => date('l',strtotime($date .'+ 2 day')), 
			strtolower(' + 3 day') => date('l',strtotime($date .'+ 3 day')), 
			strtolower(' + 4 day') => date('l',strtotime($date .'+ 4 day')), 
			strtolower(' + 5 day') => date('l',strtotime($date .'+ 5 day')), 
			strtolower(' + 6 day') => date('l',strtotime($date .'+ 6 day'))
		);
		
		$days_key = array(
			strtolower(date('D',strtotime($date))) => strtolower('now'),
			strtolower(date('D',strtotime($date .'+ 1 day'))) => strtolower(' + 1 day'),
			strtolower(date('D',strtotime($date .'+ 2 day'))) => strtolower(' + 2 day'),
			strtolower(date('D',strtotime($date .'+ 3 day'))) => strtolower(' + 3 day'),
			strtolower(date('D',strtotime($date .'+ 4 day'))) => strtolower(' + 4 day'),
			strtolower(date('D',strtotime($date .'+ 5 day'))) => strtolower(' + 5 day'),
			strtolower(date('D',strtotime($date .'+ 6 day'))) => strtolower(' + 6 day'),
		);
		
		$dateMonthFormat = array(
			0 => date('M d',strtotime($date)),
			1 => date('M d',strtotime($date .'+ 1 day')),
			2 => date('M d',strtotime($date .'+ 2 day')),
			3 => date('M d',strtotime($date .'+ 3 day')),
			4 => date('M d',strtotime($date .'+ 4 day')),
			5 => date('M d',strtotime($date .'+ 5 day')),
			6 => date('M d',strtotime($date .'+ 6 day')),
		);
		
        //$currentDay =  date('D');
       
		foreach ($days_key as $key => $value) {
			
			if((isset($seller_time[$key.'_start_time'])) && (isset($seller_time[$key.'_end_time']))){
				
				if(strtotime($seller_time[$key.'_start_time']) == strtotime($seller_time[$key.'_end_time'])){
			
				  //  if($key != strtolower($currentDay)){
					 unset($days[$value]);
				  //  }
					
				}
			}
		}
   
			
		if($languageId==3){               
			foreach($days as $key => $value){               
				$days[$key] = $this->language->get($value);
			} 
				
			$data['day_slot']= $days;	           
		}else{
			$data['day_slot']= $days;	
		}

		$dslot = array();
		$i=0;
		foreach($data['day_slot'] as $key => $value){
			//$dslot[] = array('value'=>$key, 'name'=>$value);
			$dslot[] = array('value'=>$key, 'name'=>$value, 'date'=>$dateMonthFormat[$i]);
			$i++;
		}
		
		$slots = array('msg'=>'success','slots'=>$dslot);   	
		print_r(json_encode($slots)); 
	}
	
	public function timeSlots(){
		$languageId= $this->request->get['language_id'];
		$this->session->data['language']  = ($languageId ==1)? 'en' :'ar';
		$this->load->model('catalog/seller');
		
		$sellerId= $this->request->get['seller_id'];
        
		$day = $this->request->get['day'];

		
		$seller_time = $this->model_catalog_seller->getSeller($sellerId);
		
		

		$slots = $this->makeOneDaySlots($seller_time,$day,$languageId);
       
		$this->response->setOutput(json_encode($slots));
	}
}