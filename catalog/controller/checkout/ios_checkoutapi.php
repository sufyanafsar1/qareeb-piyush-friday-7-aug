<?php
class ControllerCheckoutiosCheckoutapi extends Controller {
	public function getAddress() {
		
		$address_id = $this->request->get['address_id'];
		$customer_id = $this->request->get['customer_id'];
		
		$address_query = $this->db->query("SELECT DISTINCT * FROM " . DB_PREFIX . "address WHERE address_id = '" . (int)$address_id . "' AND customer_id = '" . (int)$customer_id . "'");

		if ($address_query->num_rows) {
			$country_query = $this->db->query("SELECT * FROM `" . DB_PREFIX . "country` WHERE country_id = '" . (int)$address_query->row['country_id'] . "'");

			if ($country_query->num_rows) {
				$country = $country_query->row['name'];
				$iso_code_2 = $country_query->row['iso_code_2'];
				$iso_code_3 = $country_query->row['iso_code_3'];
				$address_format = $country_query->row['address_format'];
			} else {
				$country = '';
				$iso_code_2 = '';
				$iso_code_3 = '';
				$address_format = '';
			}

			$zone_query = $this->db->query("SELECT * FROM `" . DB_PREFIX . "zone` WHERE zone_id = '" . (int)$address_query->row['zone_id'] . "'");

			if ($zone_query->num_rows) {
				$zone = $zone_query->row['name'];
				$zone_code = $zone_query->row['code'];
			} else {
				$zone = '';
				$zone_code = '';
			}

			$address_data = array(
				'address_id'     => $address_query->row['address_id'],
				'firstname'      => $address_query->row['firstname'],
				'lastname'       => $address_query->row['lastname'],
				'company'        => $address_query->row['company'],
				'address_1'      => $address_query->row['address_1'],
				'address_2'      => $address_query->row['address_2'],
				'postcode'       => $address_query->row['postcode'],
				'city'           => $address_query->row['city'],
				'zone_id'        => $address_query->row['zone_id'],
				'zone'           => $zone,
				'zone_code'      => $zone_code,
				'country_id'     => $address_query->row['country_id'],
				'country'        => $country,
				'iso_code_2'     => $iso_code_2,
				'iso_code_3'     => $iso_code_3,
				'address_format' => $address_format,
				'custom_field'   => unserialize($address_query->row['custom_field'])
			);

			print_r(json_encode($address_data));
		} else {
			echo "false";
		}	
	}

	public function getAllAddress() {
		
		$customer_id = $this->request->get['customer_id'];
		
		$address_query = $this->db->query("SELECT * FROM " . DB_PREFIX . "address WHERE customer_id = '" . $customer_id . "'");
		if ($address_query->num_rows) {
			foreach ($address_query->rows as $key => $value) {
				$country_query = $this->db->query("SELECT * FROM `" . DB_PREFIX . "country` WHERE country_id = '" . (int)$value['country_id'] . "'");

				if ($country_query->num_rows) {
					$country = $country_query->row['name'];
					$iso_code_2 = $country_query->row['iso_code_2'];
					$iso_code_3 = $country_query->row['iso_code_3'];
					$address_format = $country_query->row['address_format'];
				} else {
					$country = '';
					$iso_code_2 = '';
					$iso_code_3 = '';
					$address_format = '';
				}

				$zone_query = $this->db->query("SELECT * FROM `" . DB_PREFIX . "zone` WHERE zone_id = '" . (int)$value['zone_id'] . "'");

				if ($zone_query->num_rows) {
					$zone = $zone_query->row['name'];
					$zone_code = $zone_query->row['code'];
				} else {
					$zone = '';
					$zone_code = '';
				}

				$address_data[] = array(
					'address_id'     => $value['address_id'],
					'firstname'      => $value['firstname'],
					'lastname'       => $value['lastname'],
					'company'        => $value['company'],
					'address_1'      => $value['address_1'],
					'address_2'      => $value['address_2'],
					'postcode'       => $value['postcode'],
					'city'           => $value['city'],
					'zone_id'        => $value['zone_id'],
					'zone'           => $zone,
					'zone_code'      => $zone_code,
					'country_id'     => $value['country_id'],
					'country'        => $country,
					'iso_code_2'     => $iso_code_2,
					'iso_code_3'     => $iso_code_3,
					'address_format' => $address_format,
					'custom_field'   => unserialize($value['custom_field'])
				);	
			}
       
            $data = array('msg' => 'success', 'result' => $address_data);
            
             echo json_encode($data);
   		} else {
            $data = array('msg' => 'failed', 'result' => null);
             echo json_encode($data);
		}		
	}
}