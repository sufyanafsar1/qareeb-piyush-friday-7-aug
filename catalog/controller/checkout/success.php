<?php
class ControllerCheckoutSuccess extends Controller
{
	public function index()
	{
		$this->load->language('checkout/success');

		if (isset($this->session->data['order_id'])) {
			$this->cart->clear();


			$this->load->model('checkout/order');

			$order_info = $this->model_checkout_order->getOrder($this->session->data['order_id']);

			$order_total_query = $this->db->query("SELECT * FROM `" . DB_PREFIX . "order_total` WHERE order_id = '" . (int)$this->session->data['order_id'] . "' ORDER BY sort_order ASC");

			foreach ($order_total_query->rows as $order_total) {
				$this->load->model('total/' . $order_total['code']);

				if (method_exists($this->{'model_total_' . $order_total['code']}, 'confirm')) {
					$this->{'model_total_' . $order_total['code']}->confirm($order_info, $order_total);
				}
			}

			// Add to activity log
			$this->load->model('account/activity');

			if ($this->customer->isLogged()) {
				$activity_data = array(
					'customer_id' => $this->customer->getId(),
					'name'        => $this->customer->getFirstName() . ' ' . $this->customer->getLastName(),
					'order_id'    => $this->session->data['order_id']
				);

				$this->model_account_activity->addActivity('order_account', $activity_data);
			} else {
				$activity_data = array(
					'name'     => $this->session->data['guest']['firstname'] . ' ' . $this->session->data['guest']['lastname'],
					'order_id' => $this->session->data['order_id']
				);

				$this->model_account_activity->addActivity('order_guest', $activity_data);
			}

			$orderData = $this->session->data['order_data'];
			$otherData = $this->session->data['data'];

			
			$this->load->model('catalog/product');
			$this->load->model('account/customer');
			
			
			foreach ($this->session->data['orders_id'] as $order) {
				$prodcutData = [];
				// get area and seller id
				$sellerNAreaId = $this->db->query("SELECT `seller_area_id`, `seller_id` FROM " . DB_PREFIX . "order WHERE order_id = '". $order ."'")->row;
				$seller_info = $this->db->query(" SELECT * FROM " . DB_PREFIX . "sellers WHERE seller_id = '" . $sellerNAreaId['seller_id'] . "' AND status = 1")->row;

				// get authority id
				$authority = $this->db->query("SELECT * FROM citc_authorities WHERE id = '". $seller_info['authorities_id'] ."' AND status = 1")->row;
				// get citc category
				$citcCategory = $this->db->query("SELECT * FROM citc_category_list WHERE id = '" . $seller_info['category_list_id'] . "' AND status = 1")->row;

				
				foreach ($this->session->data['order_data']['products'] as $key => $cart_data) {
					if($cart_data['seller_id'] === $sellerNAreaId['seller_id']) {
						
						$prod_info = $this->model_catalog_product->getProduct($cart_data['product_id'], $sellerNAreaId['seller_id']);
						// Product Data 
						$prodcutData[] = [
							'image'              => $prod_info['image'],
							'name'               => $cart_data['name'],
							'model'              => $cart_data['model'],
							'quantity'           => $cart_data['quantity'],
							'price'              => $cart_data['price'],
							'total'              => $cart_data['total'],
							'source_product_id'  => $cart_data['product_id'],
							'tax'                => $cart_data['tax'],
							'source_customer_id' => $orderData['customer_id'],
							'sku'                => $prod_info['sku'],
							'upc'                => $prod_info['upc'],
							'weight'             => $prod_info['weight'],
							'description'        => $prod_info['description'],
							'country_code'       => '966',
							'product_status'     => $cart_data['status'],
							'seller_id'          => $cart_data['seller_id'],
						];
					}
				}
				
				// Customer's Information
				$customer_info = $this->model_account_customer->getCustomer($orderData['customer_id']);
				
				// Address Information
				$address_info = $this->db->query("SELECT DISTINCT * FROM " . DB_PREFIX . "address WHERE address_id = '" . (int) $this->session->data['shipping_address']['address_id'] . "' AND customer_id = '" . (int) $this->session->data['customer_id'] . "'")->row;
				// Time
				$time = $this->db->query("SELECT * FROM " . DB_PREFIX . "delivery_time WHERE order_id = '" . $order . "'")->row;
				$deliveryTime = explode(" - ", $time['delivery_time']);
				// Delivery Time in Minutes
				$diffDeliveryTimeInMin = (strtotime($deliveryTime[1]) - strtotime($deliveryTime[0])) / 60;
				// get address
				$address = $this->db->query(" SELECT * FROM " . DB_PREFIX . "address WHERE address_id = '" . $customer_info['address_id'] . "'")->row;
				// Get Citc region id and city id
				$citcRegion = $this->db->query(" SELECT * FROM citc_regions WHERE region_id = '" . $address['fk_region_id'] . "'")->row;
				$citcCity = $this->db->query(" SELECT * FROM citc_city WHERE city_id = '" . $address['fk_city_id'] . "'")->row;

				$apiArr = [
					'language_id'                      => $orderData['language_id'],
					'country_code'                     => '966',
					'city_id'                          => $orderData['shipping_country'],
					'customer_id'                      => $orderData['customer_id'],
					'delivery_time'                    => date("Y-m-d H:i:s", strtotime($deliveryTime[0])) . ' - ' . date("Y-m-d H:i:s", strtotime($deliveryTime[1])),
					'delivery_time_difference_in_mnts' => $diffDeliveryTimeInMin,
					'invoice_no'                       => $order,
					'invoice_prefix'                   => $orderData['invoice_prefix'],
					'order_status_id'                  => 1,
					'store_id'                         => $orderData['seller_id'],
					'area_id'                          => $sellerNAreaId['seller_area_id'],
					'authority_id'                     => $authority['authority_id'],
					'category_id'                      => $citcCategory['category_list_id'],
					'sub_total'                        => $orderData['totals'][0]['value'],
					'delivery_charges'                 => $this->session->data['deliveryChargesArr'][0]['delivery_charges'],
					'total'                            => $orderData['totals'][3]['value'],
					'commission'                       => $orderData['commission'],
					'currency_id'                      => $orderData['currency_id'],
					'currency_value'                   => $orderData['currency_value'],
					'currency_code'                    => $orderData['currency_code'],
					'payment_method'                   => $orderData['payment_method'],
					'payment_code'                     => $orderData['payment_code'],
					'qrcode'                           => '',
					'allocation_type'                  => '',
					'tracking_no'                      => '',
					'source'                           => '',
					'source_order_id'                  => $order,
					'source_order_date'                => date("Y-m-d h:i:s"),
					'payment_status'                   => '',
					'comment'                          => $orderData['comment'],

					'OrderShippingAddress' => [

						'shipping_firstname' => $orderData['shipping_firstname'],
						'shipping_lastname'  => $orderData['shipping_lastname'],
						'shipping_address_1' => $orderData['shipping_address_1'],
						'shipping_address_2' => $orderData['shipping_address_2'],
						'latitude'           => $address_info['latitude'],
						'longitude'          => $address_info['longitude'],
						'shipping_city'      => $orderData['shipping_city'],
						'shipping_postcode'  => $orderData['shipping_postcode'],
						'shipping_country'   => $orderData['shipping_country'],
						'type'               => '',
						'area_id'            => '',
						'city_id'            => '',
						'telephone'          => $orderData['telephone'],
						'shipping_email'     => $orderData['email'],
						'source_shipping_id' => '',
						'country_code'       => '966'
					],
					'customer' =>  [

						'first_name'         => $customer_info['firstname'],
						'last_name'          => $customer_info['lastname'],
						'email'              => $customer_info['email'],
						'phone'              => $customer_info['telephone'],
						'password'           => $customer_info['password'],
						'latitude'           => $address_info['latitude'],
						'longitude'          => $address_info['longitude'],
						'newsletter'         => $customer_info['newsletter'],
						'ip'                 => $customer_info['ip'],
						'date_added'         => $customer_info['date_added'],
						'user_type'          => $customer_info['user_type'],
						'mobile_user_id'     => $customer_info['mobile_user_id'],
						'mobile_reg_id'      => $customer_info['mobile_reg_id'],
						'salt'               => $customer_info['salt'],
						'address'            => $address_info['address_1'],
						'address_2'          => $address_info['address_2'],
						'city'               => $address_info['city'],
						'country_code'       => '966',
						'source'             => '',
						'door_image'         => '',
						'source_customer_id' => $orderData['customer_id'],
						'region_id' => $citcRegion['citc_id'],
						'city_id' => $citcCity['citc_id']
					],
					'products' => $prodcutData

				];
				
				

				// Store shipping data into json file
				$jsonData = json_encode($apiArr);
				$shippingDataFile = "orderData/file-". $apiArr['invoice_no'] . ".json";
				$handleDataFile = fopen($shippingDataFile, 'a+');
				fwrite($handleDataFile, $jsonData);
				fclose($handleDataFile);

				

				$curl = curl_init();

				curl_setopt_array($curl, array(
					CURLOPT_URL => "http://dev.algokraze.com:8000/api/v1/order/importOrders",
					CURLOPT_RETURNTRANSFER => true,
					CURLOPT_ENCODING => "",
					CURLOPT_MAXREDIRS => 10,
					CURLOPT_TIMEOUT => 0,
					CURLOPT_FOLLOWLOCATION => true,
					CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
					CURLOPT_CUSTOMREQUEST => "POST",
					CURLOPT_POSTFIELDS => $jsonData,
					CURLOPT_HTTPHEADER => array(
						"Content-Type: application/json",
						"Cookie: language=en; currency=SAR; SRVGROUP=common"
					),
				));

				$response = curl_exec($curl);
				curl_close($curl);
				$response = json_decode($response);

				// echo '<pre>';
				// print_r($response);
				// echo '</pre>';
				
			}

			unset($this->session->data['order_data']);
			unset($this->session->data['data']);
			unset($this->session->data['shipping_method']);
			unset($this->session->data['shipping_methods']);
			unset($this->session->data['payment_method']);
			unset($this->session->data['payment_methods']);
			unset($this->session->data['guest']);
			unset($this->session->data['comment']);
			unset($this->session->data['order_id']);
			unset($this->session->data['coupon']);
			unset($this->session->data['reward']);
			unset($this->session->data['voucher']);
			unset($this->session->data['vouchers']);
			unset($this->session->data['totals']);
			
			// exit;
		}

		$this->document->setTitle($this->language->get('heading_title'));

		$data['breadcrumbs'] = array();

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_home'),
			'href' => $this->url->link('common/home')
		);

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_basket'),
			'href' => $this->url->link('checkout/cart')
		);

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_checkout'),
			'href' => $this->url->link('checkout/checkout', '', 'SSL')
		);

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_success'),
			'href' => $this->url->link('checkout/success')
		);

		$data['heading_title'] = $this->language->get('heading_title');

		if ($this->customer->isLogged()) {
			$data['text_message'] = sprintf($this->language->get('text_customer'), $this->url->link('account/account', '', 'SSL'), $this->url->link('account/order', '', 'SSL'), $this->url->link('account/download', '', 'SSL'), $this->url->link('information/contact'));
		} else {
			$data['text_message'] = sprintf($this->language->get('text_guest'), $this->url->link('information/contact'));
		}

		$data['button_continue'] = $this->language->get('button_continue');

		$data['continue'] = $this->url->link('common/home');
		$data['delivery_on'] = $this->language->get('text_delivery_on');

		$data['column_left'] = $this->load->controller('common/column_left');
		$data['column_right'] = $this->load->controller('common/column_right');
		$data['content_top'] = $this->load->controller('common/content_top');
		$data['content_bottom'] = $this->load->controller('common/content_bottom');
		$data['footer'] = $this->load->controller('common/footer');
		$data['header'] = $this->load->controller('common/header');

		if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/template/common/success.tpl')) {
			$this->response->setOutput($this->load->view($this->config->get('config_template') . '/template/common/success.tpl', $data));
		} else {
			$this->response->setOutput($this->load->view('default/template/common/success.tpl', $data));
		}
	}
}
