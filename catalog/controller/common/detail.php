<?php
class ControllerCommonDetail extends Controller {
	public function index(){
		$this->document->setTitle($this->config->get('config_meta_title'));
		$this->document->setDescription($this->config->get('config_meta_description'));
		$this->document->setKeywords($this->config->get('config_meta_keyword'));

		if (isset($this->request->get['route'])) {
			$this->document->addLink(HTTP_SERVER, 'canonical');
		}
       
             unset($this->session->data['user_location']);
            if(isset($this->request->post['user_lat']) && isset($this->request->post['user_lng']))
            {
                
                $this->session->data['user_location'] = array('u_lat'=>$this->request->post['user_lat'],'u_lng'=>$this->request->post['user_lng'],'u_area'=>$this->request->post['user_area']);
            }
            unset($this->session->data['area']);
            if(isset($this->request->post['city_id']) && isset($this->request->post['area_id']))
            {
                
                $this->session->data['area'] = $this->request->post;
            }
        
          $this->load->language('common/home');
          $data['text_select_city'] = $this->language->get('text_select_city');
         $data['text_dammam'] = $this->language->get('text_dammam');
         $data['text_jeddah'] = $this->language->get('text_jeddah');
         $data['text_riyadh'] = $this->language->get('text_riyadh');
     
		$data['column_left'] = $this->load->controller('common/column_left');
		$data['column_right'] = $this->load->controller('common/column_right');
		$data['content_top'] = $this->load->controller('common/content_top');
		$data['content_bottom'] = $this->load->controller('common/content_bottom');
		$data['footer'] = $this->load->controller('common/footer');
		$data['header'] = $this->load->controller('common/header');
        
       
		if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/template/common/detail.tpl')) {
		 
			$this->response->setOutput($this->load->view($this->config->get('config_template') . '/template/common/detail.tpl', $data));
		} else {
		 
			$this->response->setOutput($this->load->view('default/template/common/home.tpl', $data));
		}
	}
}
?>