<?php
class ControllerCommonFooterHome extends Controller {
	public function index() {
		$this->load->language('common/footer');
		$data = array();
		
		$data['scripts'] = $this->document->getScripts('footer');

		$data['text_information'] = $this->language->get('text_information');
		$data['text_service'] = $this->language->get('text_service');
		$data['text_extra'] = $this->language->get('text_extra');
		$data['text_contact'] = $this->language->get('text_contact');
		$data['text_return'] = $this->language->get('text_return');
		$data['text_sitemap'] = $this->language->get('text_sitemap');
		$data['text_manufacturer'] = $this->language->get('text_manufacturer');
		$data['text_voucher'] = $this->language->get('text_voucher');
		$data['text_affiliate'] = $this->language->get('text_affiliate');
		$data['text_special'] = $this->language->get('text_special');
		$data['text_account'] = $this->language->get('text_account');
		$data['text_order'] = $this->language->get('text_order');
		$data['text_wishlist'] = $this->language->get('text_wishlist');
		$data['text_newsletter'] = $this->language->get('text_newsletter');
		$data['text_sell-with-us'] = $this->language->get('sell-with-us');
		$data['text_location_disable'] = $this->language->get('text_location_disable');
		
		$data['text_join_us'] = $this->language->get('text_join_us');
		$data['text_become_a_shopper'] = $this->language->get('text_become_a_shopper');
		$data['text_sell_with_us'] = $this->language->get('text_sell_with_us');
		$data['text_careers'] = $this->language->get('text_careers');
		
		$data['text_technologies'] = $this->language->get('text_technologies');
		$data['text_download_app'] = $this->language->get('text_download_app');
		$data['text_stay_connected'] = $this->language->get('text_stay_connected');

		$this->load->model('catalog/information');

		$data['informations'] = array();

		foreach ($this->model_catalog_information->getInformations() as $result) {
			if ($result['bottom']) {
				$data['informations'][] = array(
					'title' => $result['title'],
					'href'  => $this->url->link('information/information', 'information_id=' . $result['information_id'])
				);
			}
		}

		$data['contact'] = $this->url->link('information/contact');
		$data['return'] = $this->url->link('account/return/add', '', 'SSL');
		$data['sitemap'] = $this->url->link('information/sitemap');
		$data['manufacturer'] = $this->url->link('product/manufacturer');
		$data['voucher'] = $this->url->link('account/voucher', '', 'SSL');
		$data['affiliate'] = $this->url->link('affiliate/account', '', 'SSL');
		$data['special'] = $this->url->link('product/special');
		$data['account'] = $this->url->link('account/account', '', 'SSL');
		$data['order'] = $this->url->link('account/order', '', 'SSL');
		$data['wishlist'] = $this->url->link('account/wishlist', '', 'SSL');
		$data['newsletter'] = $this->url->link('account/newsletter', '', 'SSL');
		$data['bulk_order'] = $this->url->link('account/newsletter', '', 'SSL');
		$data['btfooter'] = $this->load->controller('bossthemes/btfooter');

		if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/template/common/footer_home.tpl')) {
			return $this->load->view($this->config->get('config_template') . '/template/common/footer_home.tpl', $data);
		} else {
			return $this->load->view('default/template/common/footer.tpl', $data);
		}
	}
}
