<?php
class ControllerCommonLanguageApi extends Controller {
	public function index() {
		$this->load->language('common/language');
		
		$this->load->model('localisation/language');

		$data['languages'] = array();

		$results = $this->model_localisation_language->getLanguages();

		foreach ($results as $result) {
			if ($result['status']) {
				$data['languages'][] = array(
					'name'  => $result['name'],
					'code'  => $result['code'],
					'image' => $result['image']
				);
			}
		}

		print_r(json_encode($results));
	}
}
?>