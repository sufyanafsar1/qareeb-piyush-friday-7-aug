<?php

class ControllerCommonStore extends Controller
{

    public function index()
    {

        //for purging cart data on change store.
        if (empty($_SESSION['storeID']) || $_SESSION['storeID'] == '') {
            if (isset($this->request->get['seller_id'])) {
                $_SESSION['storeID'] = $this->request->get['seller_id'];
            }
        }

        if (!empty($this->request->get['seller_id'])) {
            if ($_SESSION['storeID'] != $this->request->get['seller_id']) {

                $storeStatusCheck = 'changed';
                $_SESSION['storeID'] = $this->request->get['seller_id'];

                $this->language->load('module/cart');
                $this->cart->clear();
            } else if ($_SESSION['storeID'] == $this->request->get['seller_id']) {
                $storeStatusCheck = 'same';
            }
            $_SESSION['storeStatusCheck'] = $storeStatusCheck;
        }



        $this->load->model('store/store');
        $this->load->model('localisation/area');
        $this->load->language('common/store');
        $this->load->model('tool/image');

        $data['text_store'] = $this->language->get('text_store');
        $data['text_change_location'] = $this->language->get('text_change_location');
        $data['text_store_recommended'] = $this->language->get('text_store_recommended');
        $data['text_cart_warning'] = $this->language->get('text_cart_warning');
        $data['text_minimum'] = $this->language->get('text_minimum');
        $data['text_delivery'] = $this->language->get('text_delivery');

        $data['error_service'] = $this->language->get('error_service');
        $data['error_area'] = $this->language->get('error_area');
        $storesArray = array();

        if (isset($this->session->data['user_location'])) {

            $user_location = $this->session->data['user_location'];
            //$radiusKm = RADIUS; //5;
            $radiusKm = $this->config->get('config_radius');

            $lat = $user_location['u_lat'];
            $lng = $user_location['u_lng'];

            $storesArray = $this->model_store_store->getSellerByRadius($radiusKm, $lat, $lng);

            $data['area'] = $user_location['u_area'];
        } else if (isset($this->session->data['area'])) {
            if (isset($this->session->data['area']['area_id'])) {
                $area_id = $this->session->data['area']['area_id'];
            } else {
                $area_id = 0;
            }
            // Get Careeb Store Area List
            $default_store_areas = $this->config->get('config_area');

            $defaultStore = 0;
            if ((isset($this->session->data['area']['area_id'])) && (isset($default_store_areas))) {
                if (in_array($area_id, $default_store_areas)) {
                    $defaultStore = 1;
                }
            }

            //Get Sellers against area_id
            $storesArray = $this->model_store_store->getSellerByAreaId($area_id, $defaultStore);

            $area = $this->model_localisation_area->getArea($area_id);

            $data['area'] = $area['name'];
        } else {
            $data['area'] = '';
        }




        $stores = array();
        if ($storesArray) {
            $languageId = (int) $this->config->get('config_language_id');
            foreach ($storesArray as $store) {
                $store['name'] = ($languageId == 1) ? $store['firstname'] : $store['lastname'];
                if (isset($store['image']) && !empty($store['image'])) {
                    $store['image'] = $this->model_tool_image->resize($store['image'], 100, 100);
                } else {
                    $store['image'] = $this->model_tool_image->resize("default-image.jpg", 100, 100);
                }

                $store['minimum_order'] = $this->currency->format($store['minimum_order']);

                $store['delivery_charges'] = $this->currency->format($store['delivery_charges']);

                $store['action'] = $this->url->link('store/store', 'seller_id=' . $store['seller_id'], 'SSL');
                $stores[] = $store;
            }
        }


        $data['store_saved'] = 0;

        if (isset($_COOKIE['seller'])) {

            $data['store_saved'] = $_COOKIE['seller'];
        }


        $data['product_count'] = $this->cart->countProducts();
        $data['stores'] = $stores;
        if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/template/common/store.tpl')) {
            return $this->load->view($this->config->get('config_template') . '/template/common/store.tpl', $data);
        }
    }

    public function getStoresByAreaId()
    {
        
        // unset($this->session->data['sellerAreaId']);
        $this->session->data['sellerAreaId'] = $this->request->get['area_id'];
        $json = array();

        $this->load->model('store/store');
        $this->load->model('localisation/area');
        $this->load->language('common/store');
        $this->load->model('tool/image');

        $json['text_store']             = $this->language->get('text_store');
        $json['text_change_location']   = $this->language->get('text_change_location');
        $json['text_store_recommended'] = $this->language->get('text_store_recommended');
        $json['error_service']          = $this->language->get('error_service');
        $json['error_area']             = $this->language->get('error_area');


        // Remove Session and Cookies for Location
        unset($this->session->data['user_location']);
        unsetCareebCookie('user_location');

        // Remove Session and Cookies for area
        unset($this->session->data['area']);
        unsetCareebCookie('area');


        if (isset($this->request->get['user_lat']) && isset($this->request->get['user_lng'])) {

            //$this->request->get['user_lat'] = 24.774912;
            //$this->request->get['user_lng'] = 46.762457;

            $this->session->data['user_location'] = array('u_lat' => $this->request->get['user_lat'], 'u_lng' => $this->request->get['user_lng'], 'u_area' => $this->request->get['user_area']);

            //$radiusKm = RADIUS; //5;
            $radiusKm = $this->config->get('config_radius');
            $lat = $this->request->get['user_lat'];
            $lng = $this->request->get['user_lng'];

            $storesArray = $this->model_store_store->getSellerByRadius($radiusKm, $lat, $lng);

            $json['area'] = $this->request->get['user_area'];

            setCareebCookie('user_location', $this->request->get, true);
        }





        if (isset($this->request->get['city_id']) && isset($this->request->get['area_id'])) {

            $this->session->data['area'] = $this->request->get;

            $area_id = $this->request->get['area_id'];

            //Get Sellers against area_id
            $storesArray = $this->model_store_store->getStoresByAreaId($area_id);

            $area = $this->model_localisation_area->getArea($area_id);
            $json['area'] = $area['name'];

            setCareebCookie('area', $this->request->get, true);
        }



        $languageId = (int) $this->config->get('config_language_id');

        $stores = array();
        if ($storesArray) {

            foreach ($storesArray as $store) {

                if (isset($store['image']) && !empty($store['image'])) {
                    $store['image'] = $this->model_tool_image->resize($store['image'], 100, 100);
                } else {
                    $store['image'] = $this->model_tool_image->resize("default-image.jpg", 100, 100);
                }
                $store['name'] = ($languageId == 1) ? $store['firstname'] : $store['lastname'];
                $store['action'] = $this->url->link('store/store', 'seller_id=' . $store['seller_id'], 'SSL');

                $store['minimum_order'] = $this->currency->format($store['minimum_order']);

                $store['delivery_charges'] = $this->currency->format($store['delivery_charges']);

                $stores[] = $store;
            }
        }

        $json['stores'] = $stores;
        $json['total_stores'] = count($stores);

        $json['text_minimum'] = $this->language->get('text_minimum');
        $json['text_delivery'] = $this->language->get('text_delivery');

        $json['product_count'] = $this->cart->countProducts();


        $this->response->addHeader('Content-Type: application/json');
        $this->response->setOutput(json_encode($json));
    }
}
