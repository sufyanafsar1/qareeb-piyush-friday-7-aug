<?php
class ControllerDepartmentCategory extends Controller {
	public function index() {
		$this->load->language('product/category');
       
		$this->load->model('catalog/category');

		$this->load->model('catalog/product');

		$this->load->model('tool/image');

		$data['breadcrumbs'] = array();

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_home'),
			'href' => $this->url->link('common/home')
		);

	$this->document->setTitle($this->language->get('text_categories'));

   

		$categories_info = $this->model_catalog_category->getLimitedCategories(0,6,0);
      
		if ($categories_info) {		

			$data['heading_title'] = $this->language->get('text_categories');			
			$data['text_empty'] = $this->language->get('text_empty');		
           $data['view_all'] = $this->language->get('text_view_all');
		   //Buttons
			$data['button_load_more'] = $this->language->get('button_load_more');

			// Set the last category breadcrumb
			$data['breadcrumbs'][] = array(
				'text' => $this->language->get('text_categories'),
				'href' => $this->url->link('department/category')
			);
            $data['categories'] = array();
            foreach($categories_info as $category)
            {
                if ($category['image']) {
				$data['thumb'] = $this->model_tool_image->resize($category['image'], $this->config->get('config_image_category_width'), $this->config->get('config_image_category_height'));
    			} else {
    				$data['thumb'] = '';
    			}
                
                $data['description'] = html_entity_decode($category['description'], ENT_QUOTES, 'UTF-8');
                
                $data['categories'][] = array(
                    'id' => $category['category_id'],
					'name' => $category['name'],
                    'description' => $data['description'],
                    'image' => $data['thumb'],
					'href' => $this->url->link('product/category', 'path='. $category['category_id'] . $url)
				);
            }
			$data['content_top'] = $this->load->controller('common/content_top');		
			$data['footer'] = $this->load->controller('common/footer');
			$data['header'] = $this->load->controller('common/header');

			if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/template/department/category.tpl')) {
				$this->response->setOutput($this->load->view($this->config->get('config_template') . '/template/department/category.tpl', $data));
			} else {
				$this->response->setOutput($this->load->view('default/template/product/category.tpl', $data));
			}
		} else {
			

			$data['breadcrumbs'][] = array(
				'text' => $this->language->get('text_error'),
				'href' => $this->url->link('product/category', $url)
			);

			$this->document->setTitle($this->language->get('text_error'));

			$data['heading_title'] = $this->language->get('text_error');

			$data['text_error'] = $this->language->get('text_error');

			$data['button_continue'] = $this->language->get('button_continue');

			$data['continue'] = $this->url->link('common/home');

			$this->response->addHeader($this->request->server['SERVER_PROTOCOL'] . ' 404 Not Found');

			$data['column_left'] = $this->load->controller('common/column_left');
			$data['column_right'] = $this->load->controller('common/column_right');
			$data['content_top'] = $this->load->controller('common/content_top');
			$data['content_bottom'] = $this->load->controller('common/content_bottom');
			$data['footer'] = $this->load->controller('common/footer');
			$data['header'] = $this->load->controller('common/header');

			if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/template/error/not_found.tpl')) {
				$this->response->setOutput($this->load->view($this->config->get('config_template') . '/template/error/not_found.tpl', $data));
			} else {
				$this->response->setOutput($this->load->view('default/template/error/not_found.tpl', $data));
			}
		}
	}
    
    public function getMoreCategories()
    {
        $this->load->model('catalog/category');
   	    $this->load->model('tool/image');
        $this->load->language('product/category');
         
        
        $json=array();
        $json['view_all'] = $this->language->get('text_view_all');
		   //Buttons
			$json['button_load_more'] = $this->language->get('button_load_more');
        if(isset($_POST["lastid"]) && !empty($_POST["lastid"]))
        {
           
        
         $remainingCategories = $this->model_catalog_category->getRemainingCategories(0,$_POST["lastid"]);
         $allRows = count($remainingCategories);      

         $categoriesInfo = $this->model_catalog_category->getLimitedCategories(0,6,$_POST["lastid"]);
          if(count($categoriesInfo) > 0)
          {
            $json['success'] = true;
            $data['categories'] = array();
            foreach($categoriesInfo as $category)
            {
                if ($category['image']) {
				$data['thumb'] = $this->model_tool_image->resize($category['image'], $this->config->get('config_image_category_width'), $this->config->get('config_image_category_height'));
    			} else {
    				$data['thumb'] = '';
    			}
                
                $data['description'] = html_entity_decode($category['description'], ENT_QUOTES, 'UTF-8');
                
                $data['categories'][] = array(
                    'id' => $category['category_id'],
					'name' => $category['name'],
                    'description' => $data['description'],
                    'image' => $data['thumb'],
					'href' => $this->url->link('product/category', 'path='. $category['category_id'] . $url)
				);
            }
            
            $json['categories'] =  $data['categories'];
            $json['total_rows'] =  $allRows;
          }
          else{
            $json['success'] = false;
          }
            
 
        }
        
        
	    $this->response->addHeader('Content-Type: application/json');
		$this->response->setOutput(json_encode($json));
        
    }
}
