<?php
class ControllerModuleBossFeatured extends Controller {
	public function index($setting) {

		$this->document->addScript('catalog/view/javascript/bossthemes/carouFredSel-6.2.1.js');
		
		static $module = 0;
		$this->load->language('module/boss_featured');
		$data['button_cart'] = $this->language->get('button_cart');
		$data['button_wishlist'] = $this->language->get('button_wishlist');
		$data['button_compare'] = $this->language->get('button_compare');
		$data['template'] = $this->config->get('config_template');
		
		$data['title'] = isset($setting['title'][$this->config->get('config_language_id')])?$setting['title'][$this->config->get('config_language_id')]:'';
		$data['show_addition_image'] = isset($setting['addition_image'])?$setting['addition_image']:0;
		
		$this->load->model('catalog/product');
		$this->load->model('tool/image');
		
		$mainproduct = array();
		
		$products = array();
		
		if(isset($setting['show_pro_large']) && $setting['show_pro_large']){
		
			if (isset($setting['product_id']) && !empty($setting['product_id'])) {
				$product_id = $setting['product_id'];
			}
			
			$product_info = array();
			
			$product_info = $this->model_catalog_product->getProduct($product_id);
		
			if(isset($setting['img_width'])){
				$img_width = $setting['img_width'];
			}else{
				$img_width = 380;
			}
			
			if(isset($setting['img_height'])){
				$img_height = $setting['img_height'];
			}else{
				$img_height = 380;
			}
			if(isset($setting['img_add_width'])){
				$img_add_width = $setting['img_add_width'];
			}else{
				$img_add_width = 83;
			}
			
			if(isset($setting['img_add_height'])){
				$img_add_height = $setting['img_add_height'];
			}else{
				$img_add_height = 83;
			}
			
			if (!empty($product_info)) {
				if ($product_info['image']) {
					$image = $this->model_tool_image->resize($product_info['image'], $img_width, $img_height);
				} else {
					$image = false;
				}

				if (($this->config->get('config_customer_price') && $this->customer->isLogged()) || !$this->config->get('config_customer_price')) {
					$price = $this->currency->format($this->tax->calculate($product_info['price'], $product_info['tax_class_id'], $this->config->get('config_tax')));
				} else {
					$price = false;
				}
									
				if ((float)$product_info['special']) { 
					$special = $this->currency->format($this->tax->calculate($product_info['special'], $product_info['tax_class_id'], $this->config->get('config_tax')));
                                        //$discount   = 1-($product_info['special']/$product_info['price']);
                                        //$discount   =   $discount *100;
                                        $discount   =$product_info['price']-$product_info['special'];
                                        $discount = "SR ".$discount." Off";
                                } else {
					$special = false;
					$discount = false;
                                          //$discount   =   ((($product_info['price']-$product_info['special'])/100)*100)."%"." ".$this->language->get('text_off'); // Discount in percentage
				}
							
				if ($this->config->get('config_review_status')) {
					$rating = $product_info['rating'];
				} else {
					$rating = false;
				}
							
				$addition_images = $this->model_catalog_product->getProductImages($product_info['product_id']);
				$images_main = array();
				foreach ($addition_images as $addition_image) {
					$images_main[] = array(
						'popup' => $this->model_tool_image->resize($addition_image['image'], $img_width, $img_height),
						'thumb' => $this->model_tool_image->resize($addition_image['image'], $img_add_width, $img_add_height)
					);
				}			
				$mainproduct = array(
					'product_id' => $product_info['product_id'],
					'thumb'   	 => $image,
					'name'    	 => $product_info['name'],
					'description' => utf8_substr(strip_tags(html_entity_decode($product_info['description'], ENT_QUOTES, 'UTF-8')), 0, $this->config->get('config_product_description_length')) . '..',
					'price'   	 => $price,
					'special' 	 => $special,
					'discount' 	 => $discount,
					'rating'     => $rating,
					'reviews'    => sprintf($this->language->get('text_reviews'), (int)$product_info['reviews']),
					'href'    	 => $this->url->link('product/product', 'product_id=' . $product_info['product_id']),
					'images' => $images_main,
					'minimum'     => $product_info['minimum'] > 0 ? $product_info['minimum'] : 1,
				);
			}
		}
	
		if(isset($setting['limit'])){
			$limit = $setting['limit'];
		}else{
			$limit = 8;
		}
				
		if ($setting['type_product'] == "popular") {
			$results = $this->model_catalog_product->getPopularProducts($limit);
		}
		
		if ($setting['type_product'] == "special") {
			$data_sort = array(
				'sort'  => 'pd.name',
				'order' => 'ASC',
				'start' => 0,
				'limit' => $limit
			);
            $seller_id = (int)$this->request->get['seller_id'];
            if($seller_id > 0){
               
                   if(isset($this->session->data['user_location']))
                      {
                             
                        $user_location =    $this->session->data['user_location'];    
                        //$radiusKm = RADIUS; //5;
						$radiusKm = $this->config->get('config_radius');     
                    
                         $lat = $user_location['u_lat'];
                         $lng = $user_location['u_lng'];
                         $results = $this->model_catalog_product->getSellerProductSpecialsRadiusWise($data_sort,$radiusKm,$lat,$lng,$seller_id); 
                       
                    }else{
                       
                    $area_id = 0;
                    $area_id = $this->session->data['area']['area_id'];
                     $results = $this->model_catalog_product->getSellerProductSpecialsAreaWise($data_sort,$area_id,$seller_id); 
                    }
                
            }
            else{
                $results = $this->model_catalog_product->getProductSpecials($data_sort);
            }	
			
           
		
		}
		if ($setting['type_product'] == "best_seller") {
			$results = $this->model_catalog_product->getBestSellerProducts($limit);
		}
		if ($setting['type_product'] == "latest") {
			$results = $this->model_catalog_product->getLatestProducts($limit);
		}
		if ($setting['type_product'] == "category") {
			$data_sort = array(
				'filter_category_id' => $setting['category_id'],
				'sort'  => 'pd.name',
				'order' => 'ASC',
				'start' => 0,
				'limit' => $limit
			);
			$results = $this->model_catalog_product->getProducts($data_sort);
		}
		
		if ($setting['type_product'] == "featured") {
			if(isset($setting['product_featured'])){
				$pros_id = $setting['product_featured'];
			}else{
				$pros_id = array();
			}
			
			if(!empty($pros_id)){
				foreach ($pros_id as $product_id) {
					$product_info = $this->model_catalog_product->getProduct($product_id);

					if ($product_info) {
						$results[$product_id] = $product_info;
					}
				}
			}
		}
		
		if(!empty($results)){
		
			if(isset($setting['image_width'])){
				$image_width = $setting['image_width'];
			}else{
				$image_width = 200;
			}
			
			if(isset($setting['image_height'])){
				$image_height = $setting['image_height'];
			}else{
				$image_height = 200;
			}
		
			foreach ($results as $result) {
				if ($result['image']) {
					$image = $this->model_tool_image->resize($result['image'], $image_width, $image_height);
				} else {
					$image = false;
				}

				if (($this->config->get('config_customer_price') && $this->customer->isLogged()) || !$this->config->get('config_customer_price')) {
					$price = $this->currency->format($this->tax->calculate($result['price'], $result['tax_class_id'], $this->config->get('config_tax')));
				} else {
					$price = false;
				}
									
				if ((float)$result['special']) { 
					$special = $this->currency->format($this->tax->calculate($result['special'], $result['tax_class_id'], $this->config->get('config_tax')));
                                    //$discount   = 1-($result['special']/$result['price']);
                                    //$discount   =   $discount *100;
                                    //$discount = round($discount) . "% Off";
                                    
                                     $discount   =$result['price']-$result['special'];
                                $discount = "SR ".$discount." Off";
                                         
                                } else {
					$special = false;
                                        $discount=false;
				}
							
				if ($this->config->get('config_review_status')) {
					$rating = $result['rating'];
				} else {
					$rating = false;
				}
              
				if(isset($setting['limit_character'])){
					if(strlen($result['name']) > $setting['limit_character']){
						$b_name = utf8_substr(strip_tags(html_entity_decode($result['name'], ENT_QUOTES, 'UTF-8')), 0, $setting['limit_character']);	
					}else{
						$b_name = $result['name'];
					}
				}else{
					$b_name = $result['name'];
				}
                $language = $_SESSION['default']['language'];
                if($language =='ar')
                {
                    $p_name = $b_name;
                }
				else{
				    $p_name = wordwrap($b_name,20,"<br>\n",TRUE);
				}
				$products[] = array(
					'product_id' => $result['product_id'],
					'thumb'   	 => $image,
					'name'    	 => $p_name,
					'name_hover'    	 => $result['name'],
					'description' => utf8_substr(strip_tags(html_entity_decode($result['description'], ENT_QUOTES, 'UTF-8')), 0, $this->config->get('config_product_description_length')) . '..',
					'price'   	 => $price,
					'discount'   	 => $discount,
					'special' 	 => $special,
					'rating'     => $rating,
					'reviews'    => sprintf($this->language->get('text_reviews'), (int)$result['reviews']),
					'href'    	 => $this->url->link('product/product', 'product_id=' . $result['product_id']),
					'minimum'     => $result['minimum'] > 0 ? $result['minimum'] : 1,
				);
			}
		}
		
		if(isset($image_width)){
			$data['image_width'] = $image_width;
		}else{
			$data['image_width'] = 200;
		}
		
		if(isset($setting['num_row'])){
			$data['num_row'] = $setting['num_row'];
		}else{
			$data['num_row'] = 2;
		}
		
		if(isset($setting['nav_type'])){
			$data['nav_type'] = $setting['nav_type'];
		}else{
			$data['nav_type'] = 0;
		}
		$data['num_row'] = isset($setting['num_row'])?$setting['num_row']:1;
		$this->load->model('bossthemes/bossthemes');
		$position = $this->model_bossthemes_bossthemes->getLayoutModulesByCode('boss_featured.'.$setting['module_id']);
		
		if($position[0]['position'] == 'column_left'|| $position[0]['position']=='column_right'){
			$data['column'] = 'column'; 
		}else{
			$data['column'] = '';
		}
		
		//$data['column'] = '';
		
		if(isset($setting['per_row'])){
			if(((int)$setting['per_row']) > 8){
				$data['per_row'] = 5;
			}else{
				$data['per_row'] = $setting['per_row'];
			}
		}else{
			$data['per_row'] = 5;
		}
		
		if(isset($setting['show_slider'])){
			$data['show_slider'] = $setting['show_slider'];
		}else{
			$data['show_slider'] = true;
		}
		if(isset($setting['auto_scroll'])){
			$data['auto_scroll'] = $setting['auto_scroll'];
		}else{
			$data['auto_scroll'] = false;
		}
		
		$data['product_data'] = array();
		$data['cartData'] = $this->cart->getProducts();
		if (!count($data['cartData'])) {
			$data['cartData'] = array();
		}
		$data['product_data'] = array(
			'products'       => $products,
			'mainproduct'    => $mainproduct,
			'cart_data'		 => $data['cartData']
		);
		$data['module'] = $module++;
		$data['seller_id'] = $seller_id;	
		
		if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/template/module/boss_featured.tpl')) {
			return $this->load->view($this->config->get('config_template') . '/template/module/boss_featured.tpl', $data);
		} else {
			return $this->load->view('default/template/module/boss_featured.tpl', $data);
		}
	}
}
?>