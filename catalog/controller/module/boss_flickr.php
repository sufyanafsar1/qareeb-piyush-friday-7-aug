<?php
class ControllerModuleBossFlickr extends Controller {
	public function index($setting) {
		if(empty($setting)) return;	
		
		$boss_flickr = $setting['boss_flickr_module'];
		$data['heading_title'] = isset($boss_flickr['title'][$this->config->get('config_language_id')])?$boss_flickr['title'][$this->config->get('config_language_id')]:'';
		$data['api_key'] = isset($boss_flickr['api_key'])?$boss_flickr['api_key']:'';
		$data['user_id'] = isset($boss_flickr['user_id'])?$boss_flickr['user_id']:'';
		$data['limit'] = isset($boss_flickr['limit'])?$boss_flickr['limit']:6;
		$data['image_size'] = isset($boss_flickr['image_size'])?$boss_flickr['image_size']:1;
		
		
		
		if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/template/module/boss_flickr.tpl')) {
			return $this->load->view($this->config->get('config_template') . '/template/module/boss_flickr.tpl', $data);
		} else {
			return $this->load->view('default/template/module/boss_flickr.tpl', $data);
		}
	}
}
?>