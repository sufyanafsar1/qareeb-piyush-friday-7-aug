<?php 
class ControllerModuleBosslogin extends Controller {
	public function index() {
		$this->language->load('module/boss_login');
				
		$data['text_welcome_1'] = $this->language->get('text_welcome_1');
		$data['text_login'] = sprintf($this->language->get('text_login'), $this->url->link('account/login', '', 'SSL'));
		$data['text_welcome_2'] = sprintf($this->language->get('text_welcome_2') , $this->url->link('account/register', '', 'SSL'));
		$data['text_logged'] = sprintf($this->language->get('text_logged'), $this->url->link('account/account', '', 'SSL'), $this->customer->getFirstName(), $this->url->link('account/logout', '', 'SSL'));

		$data['logged'] = $this->customer->isLogged();
		
		if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/template/module/boss_login.tpl')) {
			return $this->load->view($this->config->get('config_template') . '/template/module/boss_login.tpl', $data);
		} else {
			return $this->load->view('default/template/module/boss_login.tpl', $data);
		}		
	}
}
?>