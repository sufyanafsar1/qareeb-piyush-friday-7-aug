<?php
class ControllerModuleBossRefinesearch extends Controller {
	public function index($setting) {
	  
 		if (isset($this->request->get['path'])) {
			$parts = explode('_', (string)$this->request->get['path']);
		} else {
			$parts = array();
		}
        
       	if (isset($this->request->get['seller_id'])) {
			$seller_id = $this->request->get['seller_id'] ;
		} else {
			$seller_id =1;
		}                

		$setting = $this->config->get('boss_refinesearch_module');
		$data['title'] = isset($setting['title'][$this->config->get('config_language_id')])?$setting['title'][$this->config->get('config_language_id')]:'';
		$category_id = end($parts);

		$this->load->model('catalog/category');
		
		$this->load->model('bossthemes/boss_refinesearch');

		$category_info = $this->model_catalog_category->getCategory($category_id);

		if ($category_info) {
			$this->load->language('module/boss_refinesearch');

			$data['heading_title'] = $this->language->get('heading_title');

			$data['button_filter'] = $this->language->get('button_filter');

			$url = '';

			if (isset($this->request->get['sort'])) {
				// $url .= '&sort=' . $this->request->get['sort'];
			}

			if (isset($this->request->get['order'])) {
				// $url .= '&order=' . $this->request->get['order'];
			}

			if (isset($this->request->get['limit'])) {
				$url .= '&limit=' . $this->request->get['limit'];
			}

			$data['action'] = str_replace('&amp;', '&', $this->url->link('product/category', 'path=' . $this->request->get['path'] . $url));

			if (isset($this->request->get['filter'])) {
				$data['filter_category'] = explode(',', $this->request->get['filter']);
			} else {
				$data['filter_category'] = array();
			}

			$this->load->model('catalog/product');

			$data['filter_groups'] = array();

			$filter_groups = $this->model_catalog_category->getCategoryFilters($category_id);
            
			$image_w = isset($setting['width_f'])?$setting['width_f']:30;
			$image_h = isset($setting['height_f'])?$setting['height_f']:30;
			if ($filter_groups) {
				foreach ($filter_groups as $filter_group) {
					$childen_data = array();

					foreach ($filter_group['filter'] as $filter) {
						$filter_data = array(
							'filter_category_id' => $category_id,
							'filter_filter'      => $filter['filter_id']
						);
						
						$result = $this->model_bossthemes_boss_refinesearch->getFilterImage($filter['filter_id']);
						
						if(!empty($result)){
							$image = $this->model_tool_image->resize($result['image'], $image_w,$image_h);
						}else{
							$image = '';
						}

						#$childen_data[] = array(
#							'filter_id' => $filter['filter_id'],
#							'image' 	=> $image,
#							'name'      => $filter['name'] . ($this->config->get('config_product_count') ? ' (' . $this->model_catalog_product->getTotalProducts($filter_data) . ')' : '')
#						);

                        $childen_data[] = array(
							'filter_id' => $filter['filter_id'],
							'image' 	=> $image,
							'name'      => $filter['name'] . ($this->config->get('config_product_count') ? ' (' . $this->model_catalog_product->getTotalProductsBySeller($filter_data,$seller_id) . ')' : '')
						);
					}

					$data['filter_groups'][] = array(
						'filter_group_id' => $filter_group['filter_group_id'],
						'name'            => $filter_group['name'],
						'show_image'      => isset($setting[$filter_group['filter_group_id']]['display'])?$setting[$filter_group['filter_group_id']]['display']:'image',
						'show_product'    => isset($setting[$filter_group['filter_group_id']]['under'])?$setting[$filter_group['filter_group_id']]['under']:0,
						'filter'          => $childen_data
					);
				}
				$data['seller_id'] = $seller_id;
				//echo '<pre>';print_r($data['filter_groups']);echo '</pre>';

				if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/template/module/boss_refinesearch.tpl')) {
					return $this->load->view($this->config->get('config_template') . '/template/module/boss_refinesearch.tpl', $data);
				} else {
					return $this->load->view('default/template/module/boss_refinesearch.tpl', $data);
				}
			}
		}
	}
}