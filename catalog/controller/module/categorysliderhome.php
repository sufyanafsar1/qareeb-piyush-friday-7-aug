<?php
class ControllerModuleCategorysliderhome extends Controller {
	public function index($setting) { 
		if(empty($setting)) return;	
		$data['heading_title'] = isset($setting['title'][$this->config->get('config_language_id')]) ? $setting['title'][$this->config->get('config_language_id')] : '';
		
		$data['column'] = isset($setting['column'])?$setting['column']:1;
		$data['width'] = isset($setting['width'])?$setting['width']:1;
		if (isset($this->request->get['path'])) {
			$parts = explode('_', (string)$this->request->get['path']);
		} else {
			$parts = array();
		}			
		
		if (isset($parts[0])) {
			$data['category_id'] = $parts[0];
		} else {
			$data['category_id'] = 0;
		}
		
		if (isset($parts[1])) {
			$data['child_id'] = $parts[1];
		} else {
			$data['child_id'] = 0;
		}
		
		if (isset($parts[0])) {
			$data['category_id'] = $parts[0];
		} else {
			$data['category_id'] = 0;
		}
		$this->document->addStyle('catalog/view/theme/' . $this->config->get('config_template') . '/css/jquery.bxslider.min.css');
		$this->document->addScript('catalog/view/theme/' . $this->config->get('config_template') . '/js/jquery.bxslider.min.js');
		$this->load->model('catalog/category');
		$this->load->model('catalog/product');
		$data['categories'] = array();
		$categories = $this->model_catalog_category->getCategories(0);
		foreach ($categories as $category) {
			$total = $this->model_catalog_product->getTotalProducts(array('filter_category_id' => $category['category_id']));
			if ($total) {
				$data['categories'][] = array(
					'category_id' => $category['category_id'],
					'name'        => $category['name'] . ($this->config->get('config_product_count') ? '<span>' . $total . ' products</span>' : ''),
					'href'        => $this->url->link('product/category', 'path=' . $category['category_id']),
					'image' 	  => 'image/'.$category['image']
				);
			}
		}					
		if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/template/module/categoryhomeslider.tpl')) {
			return $this->load->view($this->config->get('config_template') . '/template/module/categoryhomeslider.tpl', $data);
		} else {
			return $this->load->view('default/template/module/categoryhomeslider.tpl', $data);
		}
	}
	
	private function getChildrenCategory($category, $path)
	{
		$children_data = array();
		$children = $this->model_catalog_category->getCategories($category['category_id']);
		foreach ($children as $child) {
			$children_data[] = array(
				'name'  	=> $child['name'],
				'children' 	=> $this->getChildrenCategory($child, $path . '_' . $child['category_id']),
				'column'   	=> 1,
				'href'  => $this->url->link('product/category', 'path=' . $path . '_' . $child['category_id'])	
			);
		}
		return $children_data;
	}
}
?>