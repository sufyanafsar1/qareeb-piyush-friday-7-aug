<?php
class ControllerModuleContactForm extends Controller {
	public function index() {
		$this->load->language('module/contact_form');

		$data['heading_title'] = $this->language->get('heading_title');
		
		$data['firstli'] = $this->language->get('firstli');
		$data['secondi'] = $this->language->get('secondi');
		$data['thirdli'] = $this->language->get('thirdli');
		$data['forthli'] = $this->language->get('forthli');
		$data['fifthli'] = $this->language->get('fifthli');
		$data['sixthli'] = $this->language->get('sixthli');
		
		$data['avld_text'] = $this->language->get('avld_text');
		$data['download_text'] = $this->language->get('download_text');
		
		$data['text_need_assistances'] = $this->language->get('text_need_assistances');
		$data['text_query'] = $this->language->get('text_query');
		$data['text_query_mob'] = $this->language->get('text_query_mob');		
		
		$data['text_UAE'] = $this->language->get('text_UAE');
		$data['text_SA'] = $this->language->get('text_SA');
		
		$data['text_tel'] = $this->language->get('text_tel');
		$data['text_name'] = $this->language->get('text_name');
		$data['text_email'] = $this->language->get('text_email');
		$data['text_phone'] = $this->language->get('text_phone');
		$data['text_subject'] = $this->language->get('text_subject');
		$data['text_mesage'] = $this->language->get('text_mesage');
		$data['text_btn_subject'] = $this->language->get('text_btn_subject');
		
		if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/template/module/contact_form.tpl')) {
			return $this->load->view($this->config->get('config_template') . '/template/module/contact_form.tpl', $data);
		} else {
			return $this->load->view('default/template/module/contact_form.tpl', $data);
		}
	}	
}