<?php  
class ControllerModuleNewslettersubscribe extends Controller {
  	private $error = array();
	
	public function index($setting){
	
	   $data['title'] = $setting['title'][$this->config->get('config_language_id')];
	   
	   $this->load->language('module/newslettersubscribe');

      	$data['heading_title'] = $this->language->get('heading_title');	
		
      	$data['entry_name'] = $this->language->get('entry_name');	
      	$data['entry_email'] = $this->language->get('entry_email');	
		
		$data['text_join_our_mailing'] = $this->language->get('text_join_our_mailing');	
      	$data['text_never_miss'] = $this->language->get('text_never_miss');
		
      	$data['entry_button'] = $this->language->get('entry_button');	
		
      	$data['entry_unbutton'] = $this->language->get('entry_unbutton');	
		
      	$data['option_unsubscribe'] = $this->config->get('option_unsubscribe');	
		
      	$data['option_fields'] = $this->config->get('newslettersubscribe_option_field');	
		
		$data['option_fields1'] = $this->config->get('newslettersubscribe_option_field1');	
		$data['option_fields2'] = $this->config->get('newslettersubscribe_option_field2');	
		$data['option_fields3'] = $this->config->get('newslettersubscribe_option_field3');	
		$data['option_fields4'] = $this->config->get('newslettersubscribe_option_field4');	
		$data['option_fields5'] = $this->config->get('newslettersubscribe_option_field5');	
		$data['option_fields6'] = $this->config->get('newslettersubscribe_option_field6');
		
		$data['text_subscribe'] = $this->language->get('text_subscribe');	
		$data['text_email'] = $this->language->get('text_email');	
        $data['text_subscribe_now'] = $this->language->get('text_subscribe_now');	
		$data['sub_title'] = $setting['sub_title'][$this->config->get('config_language_id')];
		
		
		$this->id = 'newslettersubscribe';
		
		if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/template/module/newslettersubscribe.tpl')) {
			return $this->load->view($this->config->get('config_template') . '/template/module/newslettersubscribe.tpl', $data);
		} else {
			return $this->load->view('default/template/module/newslettersubscribe.tpl', $data);
		}
	   
	    $this->load->model('account/newslettersubscribe');
	   //check db
	    $this->model_account_newslettersubscribe->check_db();
	}
	
	public function subscribe(){
	
	if($this->config->get('newslettersubscribe_thickbox')){
	  $prefix_eval = "";
	}else{
	  $prefix_eval = "";
	}
	  
	  $this->language->load('module/newslettersubscribe');
	  
	  $this->load->model('account/newslettersubscribe');
	  
	  if(isset($this->request->post['subscribe_email']) and filter_var($this->request->post['subscribe_email'],FILTER_VALIDATE_EMAIL)){
           
		   if($this->config->get('newslettersubscribe_registered') and $this->model_account_newslettersubscribe->checkRegisteredUser($this->request->post)){
			   
			   
			    $this->model_account_newslettersubscribe->UpdateRegisterUsers($this->request->post,1);
				
				echo('$("'.$prefix_eval.' #subscribe_result").html("'.$this->language->get('subscribe').'");$("'.$prefix_eval.' #subscribe")[0].reset();');
			   
		    
		   }else if(!$this->model_account_newslettersubscribe->checkmailid($this->request->post)){
			 
				$this->model_account_newslettersubscribe->subscribe($this->request->post);
				
				//if($this->config->get('newslettersubscribe_mail_status')){					
				 
					$this->load->model('catalog/product');	

					$template = $this->model_catalog_product->getEmailTemplate('footer-newsletter');	

					$tags = array("[EMAIL_ADDRESS]", "[SUBJECT]");
					$tagsValues = array($this->request->post['subscribe_email'], $template['subject']);
					
					//get subject
					$subject = html_entity_decode($template['subject'], ENT_QUOTES, 'UTF-8');
					$subject = str_replace($tags, $tagsValues, $subject);
					$subject = html_entity_decode($subject, ENT_QUOTES);
											
					//get message body
					$msg = html_entity_decode($template['description'], ENT_QUOTES, 'UTF-8');
					$msg = str_replace($tags, $tagsValues, $msg);
					$msg = html_entity_decode($msg, ENT_QUOTES);
							
					$header = html_entity_decode($this->config->get('config_mail_header'), ENT_QUOTES, 'UTF-8');
					$header = str_replace($tags, $tagsValues, $header);
					$message = html_entity_decode($header, ENT_QUOTES);
					$message .= $msg;
					$message .= html_entity_decode($this->config->get('config_mail_footer'), ENT_QUOTES, 'UTF-8');
	 
					$mail = new Mail();
					$mail->protocol = $this->config->get('config_mail_protocol');
					$mail->parameter = $this->config->get('config_mail_parameter');
					$mail->smtp_hostname = $this->config->get('config_mail_smtp_hostname');
					$mail->smtp_username = $this->config->get('config_mail_smtp_username');
					$mail->smtp_password = html_entity_decode($this->config->get('config_mail_smtp_password'), ENT_QUOTES, 'UTF-8');
					$mail->smtp_port = $this->config->get('config_mail_smtp_port');
					$mail->smtp_timeout = $this->config->get('config_mail_smtp_timeout');		
					$mail->setTo($this->request->post['subscribe_email']);
					$mail->setFrom($this->config->get('config_email'));
					$mail->setSender(html_entity_decode($this->config->get('config_name'), ENT_QUOTES, 'UTF-8'));
					$mail->setSubject($subject);
					$mail->setHtml($message);
					$mail->send();					
					
				//}
				
				echo('$("'.$prefix_eval.' #subscribe_result").html("'.sprintf($this->language->get('subscribe'), $this->request->post['subscribe_email']).'");$("'.$prefix_eval.' #subscribe")[0].reset();');
			 
		   }else{
				  echo('$("'.$prefix_eval.' #subscribe_result_error").html("<span class=\"error\">'.$this->language->get('alreadyexist').'</span>");$("'.$prefix_eval.' #subscribe")[0].reset();');	 
		   }
		   
	  }else{
	    echo('$("'.$prefix_eval.' #subscribe_result_error").html("<span class=\"error\">'.$this->language->get('error_invalid').'</span>")');
	  }
	}

	public function unsubscribe(){
	  
	  if($this->config->get('newslettersubscribe_thickbox')){
		  $prefix_eval = "#TB_ajaxContent ";
	  }else{
	      $prefix_eval = "";
	  }
	  
	  $this->language->load('module/newslettersubscribe');
	  
	  $this->load->model('account/newslettersubscribe');
	  
	  if(isset($this->request->post['subscribe_email']) and filter_var($this->request->post['subscribe_email'],FILTER_VALIDATE_EMAIL)){
            
		    if($this->config->get('newslettersubscribe_registered') and $this->model_account_newslettersubscribe->checkRegisteredUser($this->request->post)){
			   
			    $this->model_account_newslettersubscribe->UpdateRegisterUsers($this->request->post,0);
				
			echo('$("'.$prefix_eval.' #subscribe_result").html("'.$this->language->get('unsubscribe').'");$("'.$prefix_eval.' #subscribe")[0].reset();');
			   
		    
		   }else if(!$this->model_account_newslettersubscribe->checkmailid($this->request->post)){
			 
		     echo('$("'.$prefix_eval.' #subscribe_result").html("'.$this->language->get('notexist').'");$("'.$prefix_eval.' #subscribe")[0].reset();');
			 
		   }else{
			   
			  if($this->config->get('option_unsubscribe')) {
				 $this->model_account_newslettersubscribe->unsubscribe($this->request->post);
				 echo('$("'.$prefix_eval.' #subscribe_result").html("'.$this->language->get('unsubscribe').'");$("'.$prefix_eval.' #subscribe")[0].reset();');
			  }
		   }
		   
	  }else{
	    echo('$("'.$prefix_eval.' #subscribe_result").html("<span class=\"error\">'.$this->language->get('error_invalid').'</span>")');
	  }
	}
	
	public function needDesktop(){
		
		$json =array();
		
		$this->load->language('module/contact_form');
		
		if ((utf8_strlen($this->request->post['name']) < 3) || (utf8_strlen($this->request->post['name']) > 64)) {
			$json['error_name'] = $this->language->get('error_name');
			
		} else if (empty($this->request->post['email'])) {
			$json['error_email'] = $this->language->get('error_email');
			
		} else if (empty($this->request->post['phone'])) {
			$json['error_phone'] = $this->language->get('error_phone');
			
		} else if (empty($this->request->post['subject'])) {
			$json['error_subject'] = $this->language->get('error_subject');
			
		} else if (utf8_strlen($this->request->post['message']) < 1) {
			$json['error_message'] = $this->language->get('error_message');
			
		} 
			
		if(!$json) {
	 
			$this->load->model('catalog/product');	
			
			$ip_address = $this->request->server['REMOTE_ADDR'];

			$template = $this->model_catalog_product->getEmailTemplate('need-assistance-admin-mail');	

			$tags = array("[NAME]", "[EMAIL]", "[PHONE]", "[SUBJECT]", "[MESAAGE]", "[IP_ADDRESS]");
			$tagsValues = array($this->request->post['name'], $this->request->post['email'], $this->request->post['phone'], $this->request->post['subject'], $this->request->post['message'],  $ip_address);
			
			//get subject
			$subject = html_entity_decode($template['subject'], ENT_QUOTES, 'UTF-8');
			$subject = str_replace($tags, $tagsValues, $subject);
			$subject = html_entity_decode($subject, ENT_QUOTES);
									
			//get message body
			$msg = html_entity_decode($template['description'], ENT_QUOTES, 'UTF-8');
			$msg = str_replace($tags, $tagsValues, $msg);
			$msg = html_entity_decode($msg, ENT_QUOTES);
					
			$header = html_entity_decode($this->config->get('config_mail_header'), ENT_QUOTES, 'UTF-8');
			$header = str_replace($tags, $tagsValues, $header);
			$message = html_entity_decode($header, ENT_QUOTES);
			$message .= $msg;
			$message .= html_entity_decode($this->config->get('config_mail_footer'), ENT_QUOTES, 'UTF-8');

			$mail = new Mail();
			$mail->protocol = $this->config->get('config_mail_protocol');
			$mail->parameter = $this->config->get('config_mail_parameter');
			$mail->smtp_hostname = $this->config->get('config_mail_smtp_hostname');
			$mail->smtp_username = $this->config->get('config_mail_smtp_username');
			$mail->smtp_password = html_entity_decode($this->config->get('config_mail_smtp_password'), ENT_QUOTES, 'UTF-8');
			$mail->smtp_port = $this->config->get('config_mail_smtp_port');
			$mail->smtp_timeout = $this->config->get('config_mail_smtp_timeout');		
			//$mail->setTo($this->config->get('config_email'));
			$mail->setTo('info@qareeb.com');
			$mail->setFrom($this->request->post['email']);
			$mail->setSender(html_entity_decode($this->config->get('config_name'), ENT_QUOTES, 'UTF-8'));
			$mail->setSubject($subject);
			$mail->setHtml($message);
			$mail->send();	
			
			$json['success'] = $this->language->get('text_success');
		}
				
		$this->response->addHeader('Content-Type: application/json');
		$this->response->setOutput(json_encode($json));
		
	}	
}
?>