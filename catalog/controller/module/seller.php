<?php
class ControllerModuleSeller extends Controller {
	public function index($setting) {
		$this->load->language('module/seller');
		$data['heading_title'] = $this->language->get('heading_title');

		$data['text_tax'] = $this->language->get('text_tax');

		$data['button_cart'] = $this->language->get('button_cart');
		$data['button_wishlist'] = $this->language->get('button_wishlist');
		$data['button_compare'] = $this->language->get('button_compare');

		$this->load->model('saccount/seller');

		$this->load->model('saccount/seller');

		$this->load->model('tool/image');

		$data['sellers'] = array();
		
	
		$sellers = explode(',', $this->config->get('seller_product'));

		if (empty($setting['limit'])) {
			$setting['limit'] = 4;
		}

		$sellers = array_slice($sellers, 0, (int)$setting['limit']);

		foreach ($sellers as $seller_id) {
				$seller_info = $this->model_saccount_seller->getSeller($seller_id);

			
			if ($seller_info) {
				if ($seller_info['image']) {
					$image = $this->model_tool_image->resize($seller_info['image'], $this->config->get('seller_width'),$this->config->get('seller_height'));
				} else {
					$image = $this->model_tool_image->resize('placeholder.png',$this->config->get('seller_width'), $this->config->get('seller_height'));
				}

				

				$data['sellers'][] = array(
					'seller_id' => $seller_info['seller_id'],
				'name'        => $seller_info['firstname'].' '.$seller_info['lastname'],
				'thumb'       =>$image,
				'href'        => $this->url->link('product/seller', 'seller_id=' . $seller_info['seller_id'])
				);
			}
		}

		if ($data['sellers']) {
			if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/template/module/seller.tpl')) {
				return $this->load->view($this->config->get('config_template') . '/template/module/seller.tpl', $data);
			} else {
				return $this->load->view('default/template/module/seller.tpl', $data);
			}
		}
	}
}