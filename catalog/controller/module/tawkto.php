<?php

/**
 * @package Tawk.to Integration
 * @author Tawk.to
 * @copyright (C) 2014- Tawk.to
 * @license GNU/GPLv3 http://www.gnu.org/licenses/gpl-3.0.html
 */


class ControllerModuleTawkto extends Controller {
	private static $displayed = false; //we include embed script only once

	public function index() {
		if(self::$displayed) {
			return;
		}

		self::$displayed = TRUE;

		$widget = $this->getWidget();

		if($widget === null) {
			echo '';
			return;
		}

		$data['page_id'] = $widget['page_id'];
		$data['widget_id'] = $widget['widget_id'];

		if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/template/module/tawkto.tpl')) {
			return $this->load->view($this->config->get('config_template') . '/template/module/tawkto.tpl', $data);
		} else {
			return $this->load->view('default/template/module/tawkto.tpl', $data);
		}
	}

	private function getWidget() {
		$this->load->model('setting/setting');

		$settings       = $this->model_setting_setting->getSetting('tawkto');
		$widgetSettings = isset($settings) ? $settings['tawkto_widget'] : null;
			if(!isset($widgetSettings)){
			return null;
		}

		$storeId = $this->config->get('config_store_id');
		$languageId = $this->config->get('config_language_id');
		$layoutId = $this->getLayoutId();

		$widget = null;

		if(isset($widgetSettings['widget_settings_for_'.$storeId])) {
			$widget = $widgetSettings['widget_settings_for_'.$storeId];
		}

		if(isset($widgetSettings['widget_settings_for_'.$storeId.'_'.$languageId])) {
			$widget = $widgetSettings['widget_settings_for_'.$storeId.'_'.$languageId];
		}

		if(isset($widgetSettings['widget_settings_for_'.$storeId.'_'.$languageId.'_'.$layoutId])) {
			$widget = $widgetSettings['widget_settings_for_'.$storeId.'_'.$languageId.'_'.$layoutId];
		}

		return $widget;
	}

	private function getLayoutId() {
		if (isset($this->request->get['route'])) {
			$route = $this->request->get['route'];
		} else {
			$route = 'common/home';
		}

		$this->load->model('design/layout');

		return $this->model_design_layout->getLayout($route);
	}
}