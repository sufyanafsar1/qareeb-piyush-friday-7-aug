<?php
class ControllerModuleTimeslotTimeslot extends Controller {

	public function index() {
        $store_id = $this->get_store_id();
        $this->load->model('slot/timing');
        $data['Intervals'] = $this->model_slot_timing->getTime($store_id);  
        
        $template = "timeslot_collapse";     
        $this->template = 'default/template/module/timeslot/'.$template.'.tpl';
		$this->response->setOutput($this->load->view($this->template, $data));
         
	}

    public function get_store_id()
    {
        $store_id = '';
        $this->load->model('setting/store');
        $storedata = $this->model_setting_store->getStores();
        foreach ($storedata as $key => $value) {
            if (strpos( $url, 'https' ) !== false) {
                $stores = str_replace('https://','',$value['url']);
            }else{
                $stores = str_replace('http://','',$value['url']);
            }
            $storewwww=str_replace('www.','',$stores);
            $store[$value['store_id']]=str_replace('.qareeb.com','',$storewwww);
        }
        if (isset($_COOKIE['tracking_city'])) {
            if (in_array($_COOKIE['tracking_city'], $store)) {
                $store_id = array_search($_COOKIE['tracking_city'],$store);
            }else{
                $store_id = 0;
            }
        }else{
            $url= $_SERVER[''];
            foreach ($store as $key => $value) {
                if (strpos( $url, $value ) !== false) {
                    $store_id = $key;
                    break;
                }
            }
            if ($store_id == '') {
                $store_id = 0;
            }
        }
        return $store_id;
    }

    public function save() {
        $store_id = $this->get_store_id();
        $time_slot_date =  $_POST['delivery_slot_time'];
        $time_slot_time =  $_POST['get_slot'];
        $expl = explode("-",$time_slot_time);
        $time = $time_slot_date." ".$expl[0]." - ".$time_slot_date." ".$expl[1];
        if(isset($time) && $time!==""):
            $this->load->model('slot/timing');
            $result = $this->model_slot_timing->countSlot($time,$store_id);   
        endif;
    }
        
    public function getDates(){
        $store_id = $this->get_store_id();
        $sql = "select show_slots,delivery_starts,time_format,date_format FROM " . DB_PREFIX . "slot_setting WHERE store_id=".$store_id;
        $query = $this->db->query($sql); 

        if( $query->num_rows>0){
          
            $time_format = $query->row['time_format'];
            $date_format = $query->row['date_format'];
            $lead = $query->row['delivery_starts'];
        }    
        
            
        date_default_timezone_set("Asia/Karachi");   
            
        $SearchTime = "";
        
        $SearchTime = $_POST['SearchTime'];
        
        if($SearchTime){
            if($date_format=='dd-mm-yy'){
                $d = explode("-",$SearchTime);
                $year = $d[2];
                $month= $d[1];
                $dat = $d[0];
                $SearchTime = $year."-".$month."-".$dat;
            }
        }
      
        $start = $_POST['start_id'];
        
        $last_id = $_POST['last_id'];
        
        $added_lead_time = date('H:i:s', strtotime("+$lead hour"));
        
        $lead_time = $SearchTime." ".$added_lead_time;
        
        $q = $this->db->query("SELECT * from (SELECT slot_timing,to_date_time,from_date_time, one_slot_id,max_no, (SELECT COUNT(*) FROM " . DB_PREFIX . "delivery_time v WHERE store_id=".$store_id." AND times LIKE p.slot_timing ) countSlot FROM " . DB_PREFIX . "one_year_slot p where store_id=".$store_id." AND to_date_time like '%".$SearchTime."%' and one_slot_id >= '".$start."' and Status='ENABLE') a where a.countSlot!=a.max_no");
        
        if($q->num_rows>0){
            if(isset($_SESSION['time_zone'])){
                $zone = "( ".$_SESSION['time_zone']." ) ";
            }else{
                $zone = "";
            }
            echo "<br><p class='description'>Please select the preferred Time to use on this order $zone.</p>";
            echo "<select class='form-control' name='get_slot' id='get_slots'>";
            foreach($q->rows as $val):
                
                if($last_id>=$val['one_slot_id']){
                    $to = explode(" ",$val['to_date_time']);
                    $from = explode(" ",$val['from_date_time']);
                    $slot_timing = $val['slot_timing'];
                    if($from[1]>date('H:i:s')){
                        if($time_format=='24 ( Military/Continental Time )'){    
                            $time_in_12_hour_format_from  = date("H:i:s", strtotime($from[1]));
                            $time_in_12_hour_format_to  = date("H:i:s", strtotime($to[1]));
                        }else if($time_format=="12 ( AM/PM and O clock )"){
                            $time_in_12_hour_format_from  = date("g:i:s A", strtotime($from[1]));
                            $time_in_12_hour_format_to  = date("g:i:s A", strtotime($to[1]));
                        }   
                        $times =  $time_in_12_hour_format_from." - ".$time_in_12_hour_format_to; ?>
                        <option><?php echo $times?></option>
                        <?php
                    }else{
                        if($time_format=='24 ( Military/Continental Time )'){       
                            $time_in_12_hour_format_from  = date("H:i:s", strtotime($from[1]));
                            $time_in_12_hour_format_to  = date("H:i:s", strtotime($to[1]));
                        }else if($time_format=="12 ( AM/PM and O clock )"){
                            $time_in_12_hour_format_from  = date("g:i:s A", strtotime($from[1]));
                            $time_in_12_hour_format_to  = date("g:i:s A", strtotime($to[1]));
                        } 
                        $times =  $time_in_12_hour_format_from." - ".$time_in_12_hour_format_to; ?>  
                        <option><?php echo $times?></option>   
                        <?php 
                    }
                }
            endforeach;
            echo "</select>";
        } else{
            echo "false";
        }
    }
}