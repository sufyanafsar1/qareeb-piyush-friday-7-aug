<?php

class ControllerPaymentStartPayments extends Controller {
    public $version = '0.3';

    public function index() {
        $this->language->load('payment/start_payments');
        $this->load->model('checkout/order');

        $data['button_confirm'] = $this->language->get('button_confirm');
        $data['text_wait'] = $this->language->get('text_wait');

        $order_info = $this->model_checkout_order->getOrder($this->session->data['order_id']);

        /* add API key, amount, currency, email to Beautiful.js */
        if ($this->config->get('start_payments_test')) {
            $data['open_api_key'] = $this->config->get('start_payments_entry_test_open_key');
        } else {
            $data['open_api_key'] = $this->config->get('start_payments_entry_live_open_key');
        }

        $data['email'] = $order_info['email'];
        $data['currency'] = $order_info['currency_code'];
        $data['amount'] = $this->get_amount_in_cents($order_info);

        $data['pay_url'] = $this->url->link('payment/start_payments/pay', '', 'SSL');

        if (preg_match("/^2.0/", VERSION) || preg_match("/^2.1/", VERSION)) {
            if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/template/payment/start_payments.tpl')) {
                return $this->load->view($this->config->get('config_template') . '/template/payment/start_payments.tpl', $data);
            } else {
                return $this->load->view('default/template/payment/start_payments.tpl', $data);
            }
        } else {
            if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . 'payment/start_payments.tpl')) {
                return $this->load->view($this->config->get('config_template') . 'payment/start_payments.tpl', $data);
            } else {
                return $this->load->view('payment/start_payments.tpl', $data);
            }
        }
    }

    public function pay() {
        require_once './vendor/payfort/start/Start.php';

        /* TODO: check where and how to load this file properly */
        #$this->response->redirect($this->url->link('checkout/success', '', true));
        # $this->response->redirect($this->url->link('checkout/failure', '', true));


        if ($this->config->get('start_payments_test')) {
            $secret_api_key = $this->config->get('start_payments_entry_test_secret_key');
        } else {
            $secret_api_key = $this->config->get('start_payments_entry_live_secret_key');
        }

        $this->load->model('checkout/order');

        /* collect order information */
        $order_id = $this->session->data['order_id'];
        $order_info = $this->model_checkout_order->getOrder($order_id);
        $this->load->model('module/deliveryTime/order');
        $this->model_module_deliveryTime_order->addDeliveryTime($this->session->data['order_id'],$this->session->data['delivery_time']);

        $amount_in_cents = $this->get_amount_in_cents($order_info);
        $order_description = "Charge for order: " . $order_id;
        $token = $_POST['payfortToken'];
        $email = $_POST['payfortEmail'];

        $capture = ($this->config->get('start_payments_transaction')) ? false : true;

        $charge_args = array(
            'description' => $order_description,
            'card' => $token,
            'currency' => $order_info['currency_code'],
            'email' => $email,
            'ip' => $_SERVER["REMOTE_ADDR"],
            'amount' => $amount_in_cents,
            'capture' => $capture,
            'shopping_cart' => $this->get_shopping_cart($order_info),
            'metadata' => array('reference_id' => $order_id)
        );

        $userAgent = 'Opencart ' . VERSION . ' / Start Plugin ' . $this->version;
        Start::$useCurl = true;
        Start::setUserAgent($userAgent);
        Start::setApiKey($secret_api_key);
        try {
            $charge = Start_Charge::create($charge_args);
            $this->model_checkout_order->addOrderHistory($order_id, $this->config->get('start_payments_order_status_id'), 'Start Charge ID: ' . $charge["id"]);
            $this->response->redirect($this->url->link('checkout/success', '', true));
        } catch (Exception $e) {
            $error_message = $e->getMessage();
            $this->session->data['error'] = $error_message;
            $this->response->redirect($this->url->link('checkout/checkout', '', true));
        }
    }

    /* this functions should go to separate class */
    private function get_currency_multiplier($currency) {
        if ($currency == 'KWD') {
            return 1000;
        } else {
            return 100;
        }
    }
    private function get_amount_in_cents($order_info, $amount = NULL) {
        if (!$amount) {
            $amount = $order_info['total'];
        }

        $currency_multiplier = $this->get_currency_multiplier($order_info['currency_code']);

        return $this->currency->format($amount, $order_info['currency_code'], $order_info['currency_value'], false) * $currency_multiplier;
    }

    private function get_billing_address($order_info) {
        return array(
            "first_name" => $order_info['payment_firstname'],
            "last_name" => $order_info['payment_lastname'],
            "country" => $order_info['payment_country'],
            "city" => $order_info['payment_city'],
            "address_1" => $order_info['payment_address_1'],
            "address_2" => $order_info['payment_address_2'],
            "phone" => $order_info['telephone'],
            "postcode" => $order_info['payment_postcode']
        );
    }

    private function get_shipping_address($order_info) {
        /* TODO: is this a proper way to check for shipping? */
        if ($this->cart->hasShipping()) {
            return array(
                "first_name" => $order_info['shipping_firstname'],
                "last_name" => $order_info['shipping_lastname'],
                "country" => $order_info['shipping_country'],
                "city" => $order_info['shipping_city'],
                "address_1" => $order_info['shipping_address_1'],
                "address_2" => $order_info['shipping_address_2'],
                "phone" => $order_info['telephone'],
                "postcode" => $order_info['shipping_postcode']
            );
        } else {
            return array();
        }
    }

    private function get_shipping_amount($order_info) {
        if ($this->cart->hasShipping() && isset($this->session->data['shipping_method'])) {
            return $this->get_amount_in_cents($order_info, $this->session->data['shipping_method']['cost']);
        } else {
            return NULL;
        }
    }

    private function get_shopping_cart($order_info) {
        $billing_address = $this->get_billing_address($order_info);
        $shipping_address = $this->get_shipping_address($order_info);
        $shipping_amount_in_cents = $this->get_shipping_amount($order_info);

        $user_name = $order_info['email'];

        if ($order_info['customer_id'] != 0) {
            $this->load->model('account/customer');
            $customer_info = $this->model_account_customer->getCustomer($this->customer->getId());
        }

        $registered_at = (isset($customer_info)) ? date(DATE_ISO8601, strtotime(date("Y-m-d H:i:s"))) : date(DATE_ISO8601, strtotime($customer_info['date_added']));

        $products = $this->cart->getProducts();
        $order_items_array_full = array();

        foreach ($products as $key => $items) {
            $order_items_array['title'] = $items['name'];
            $order_items_array['amount'] = $this->get_amount_in_cents($order_info, $items['price']);
            $order_items_array['quantity'] = $items['quantity'];

            array_push($order_items_array_full, $order_items_array);
        }

        $shopping_cart_array = array(
            'user_name' => $user_name,
            'registered_at' => $registered_at,
            'items' => $order_items_array_full,
            'billing_address' => $billing_address,
            'shipping_address' => $shipping_address,
            'shipping_amount' => $shipping_amount_in_cents
        );

        return $shopping_cart_array;
    }
}
