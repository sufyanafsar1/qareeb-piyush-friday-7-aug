<?php
class ControllerProductCategoryApi extends Controller {
	
	public function get_categories() {
		$this->load->language('product/category');

		$this->load->model('catalog/category');
		$this->load->model('tool/image');
		$language = (int)$this->request->get['language'];
		
		$categoryies = $this->model_catalog_category->getCategoriess($language);
		
		foreach($categoryies as $key => $value)
		{
		  $categoryies[$key]['image'] = $this->model_tool_image->resize($categoryies[$key]['image'], 720, 406);
		}
		
		print_r(json_encode($categoryies));
		
	}
    public function getStoreCategories() {

        $this->load->model('catalog/category');
        $this->load->model('tool/image');

        //Parameters
        $language = (int)$this->request->get['language'];
        $seller = (int)$this->request->get['seller'];
        $is_area = (int)$this->request->get['is_area'];

        if($is_area==1) //GEt AreaId
        {
            $area_id = (int)$this->request->get['area'];
            $categories = $this->model_catalog_category->getSellerCategoriesAreaWiseV2(0, $seller, $area_id,$language);
        }else if($is_area==0){
           
            $lat = $this->request->get['lat'];
            $lng = $this->request->get['lng'];
            $radiusKm = $this->request->get['radius'];
            $categories = $this->model_catalog_category->getSellerCategoriesRadiusWiseV2(0, $seller, $radiusKm, $lat, $lng,$language);
        }
        
		$categoriesNew = array();	
        if(count($categories) > 0 ){
            $i = 0;
			
			foreach ($categories as $result) {     
               
				$categoryProduct = $this->model_catalog_category->getSellerCategoriesProducts($seller, $result['category_id']);

				if($categoryProduct > 0){
					
					if($i==0){
						 $next_delivery = $this->getNextDeliverySlot($seller,$language);
					} else {
						$next_delivery = '';
					}
					
					$categoriesNew[] = array(
						'category_id'  => $result['category_id'],
						'image'       => $this->model_tool_image->resize($result['image'], 720, 406),
						'name'        => html_entity_decode($result['name']),
						'description' => $result['description'],
						'total_child'       => $result['total_child'],
						'next_delivery'     => $next_delivery
					);					
					$i++;
				}
            } 
			
            /* foreach($categories as $key => $value)
            {
                // if($i==0){
                    // $categories[$key]['next_delivery'] = $this->getNextDeliverySlot($seller,$language);
				// }
                // $categories[$key]['image'] = $this->model_tool_image->resize($categories[$key]['image'], 720, 406);
                // $i++; 
				
				$categoryProduct = $this->model_catalog_category->getSellerCategoriesProducts($seller, $categories[$key]['category_id']);

				if($categoryProduct > 0){
					$categoriesNew[$key]['category_id'] = $categories[$key]['category_id'];	
					$categoriesNew[$key]['image'] = $this->model_tool_image->resize($categories[$key]['image'], 720, 406);
					$categoriesNew[$key]['name'] = html_entity_decode($categories[$key]['name']);
					$categoriesNew[$key]['description'] = $categories[$key]['description'];				
					$categoriesNew[$key]['total_child'] = $categories[$key]['total_child'];
					
					if($i==0){
						 $categoriesNew[$key]['next_delivery'] = $this->getNextDeliverySlot($seller,$language);
					}
					 
					$i++;
				}
            }  */
			
			/* $categoriesNew = array();
			foreach($categories as $category)
            {            
				if($category['total_child'] > 0){
					$categoryProduct = $this->model_catalog_category->getSellerCategoriesProducts($seller, $category['category_id']);

					$categoriesNew[$i]['category_id'] = $category['category_id'];	
					$categoriesNew[$i]['image'] = $this->model_tool_image->resize($category['image'], 720, 406);
					$categoriesNew[$i]['name'] = html_entity_decode($category['name']);
					$categoriesNew[$i]['description'] = $category['description'];				
					$categoriesNew[$i]['total_child'] = $category['total_child'];
					
					if($i==0){
						 $categoriesNew[$i]['next_delivery'] = $this->getNextDeliverySlot($seller,$language);
					}
					 
					$i++;
					
				}
            } */

			// $result =  array('msg' => 'Success', 'result' => $categories);
		    $result =  array('msg' => 'Success', 'result' => $categoriesNew);
        }
        else{
            $result =  array('msg' => 'Failed', 'result' => '');
        }


        echo json_encode($result);

    }

    public function getStoreSubCategories() {

        $this->load->model('catalog/category');
        $this->load->model('tool/image');

        //Parameters
        $language = (int)$this->request->get['language'];
        $seller = (int)$this->request->get['seller'];
        $is_area = (int)$this->request->get['is_area'];
        $is_ios = (int)(isset($this->request->get['ios'])) ? $this->request->get['ios'] : 0;
        if (isset($this->request->get['category_id'])) {
            $category_id = (int)$this->request->get['category_id'];
        } else {
            $category_id = 0;
        }

        if($is_area==1) //GEt AreaId
        {
            $area_id = (int)$this->request->get['area'];
            $categories = $this->model_catalog_category->getSellerCategoriesAreaWiseV2($category_id, $seller, $area_id,$language);
        }else if($is_area==0){

            $lat = $this->request->get['lat'];
            $lng = $this->request->get['lng'];
            $radiusKm = $this->request->get['radius'];
            //die($lat.'---'.$lng.'-----'.$radiusKm.'----'.$seller);
            $categories = $this->model_catalog_category->getSellerCategoriesRadiusWiseV2($category_id, $seller, $radiusKm, $lat, $lng,$language);
        }

        foreach($categories as $key => $value)
        {
            $categories[$key]['image'] = $this->model_tool_image->resize($categories[$key]['image'], 720, 406);
			$categories[$key]['name'] = html_entity_decode($categories[$key]['name']);
        }
		
        if($is_ios) {
            $data = array('msg' => 'success', 'result' => $categories);
            echo json_encode($data);
            die;
        }else{
            print_r(json_encode($categories));
        }


    }
	
    public function getProductBYcategory() {
		$this->load->language('product/product');

		$this->load->model('catalog/product');
		$this->load->model('tool/image');		
		
		if (isset($this->request->get['category_id'])) {
			$category_id = (int)$this->request->get['category_id'];
		} else {
			$category_id = 0;
		}
		
		$filter_data = array(
			'filter_category_id'  => $category_id,
			'filter_sub_category' => false
		);		
		$language = (int)$this->request->get['language'];
		$device_id = $this->request->get['device_id'];
		$customer_id = (int)$this->request->get['customer_id'];
			
		$AllProducts = $this->model_catalog_product->getProductBYcategory($filter_data,$language,$device_id, $customer_id);
		
		foreach($AllProducts as $key => $value)
		{
		  $AllProducts[$key]['image'] = $this->model_tool_image->resize($AllProducts[$key]['image'], 470, 350);
		}
		print_r(json_encode($AllProducts));
		
	}

	public function getStoreCategoriesRemoved() {

        $this->load->model('catalog/category');
        $this->load->model('tool/image');

        //Parameters
        $language = (int)$this->request->get['language'];
        $seller = (int)$this->request->get['seller'];

        //Get Categories
        $categories = $this->model_catalog_category->getStoreCategories($seller,$language);

        if(count($categories) > 0 ){

            foreach($categories as $key => $value)
            {
                $categories[$key]['image'] = $this->model_tool_image->resize($categories[$key]['image'], 720, 406);
            }

            $result =  array('msg' => 'Success', 'result' => $categories);
        }
        else{
            $result =  array('msg' => 'Failed', 'result' => '');
        }


        echo json_encode($result);

    }
	public function getProducts() {
		$this->load->language('product/product');

		$this->load->model('catalog/product');
		$this->load->model('tool/image');		
		
		if (isset($this->request->get['category_id'])) {
			$category_id = (int)$this->request->get['category_id'];
		} else {
			$category_id = 0;
		}
		
		$filter_data = array(
			'filter_category_id'  => $category_id,
			'filter_sub_category' => false
		);		
		$language = (int)$this->request->get['language'];
		
			
		$AllProducts = $this->model_catalog_product->getProductss($filter_data,$language);
		
		foreach($AllProducts as $key => $value)
		{
		  $AllProducts[$key]['image'] = $this->model_tool_image->resize($AllProducts[$key]['image'], 470, 350);
		}
		print_r(json_encode($AllProducts));
		
	}
	public function get_subcategories() {
		$this->load->language('product/category');

		$this->load->model('catalog/category');
		$this->load->model('tool/image');
		$language = (int)$this->request->get['language'];
		if (isset($this->request->get['category_id'])) {
			$category_id = (int)$this->request->get['category_id'];
		} else {
			$category_id = 0;
		}

		$results = $this->model_catalog_category->getCategoriesss($category_id,$language);		
		print_r(json_encode($results));
		
	}
	public function getLatestProducts() {
		$this->load->language('product/product');

		$this->load->model('catalog/product');
		$this->load->model('tool/image');
		
		$LatestProducts = $this->model_catalog_product->getLatestProducts(20);
		
		foreach($LatestProducts as $key => $value)
		{
		  $LatestProducts[$key]['image'] = $this->model_tool_image->resize($LatestProducts[$key]['image'], 1200, 1200);
		}

		print_r(json_encode($LatestProducts));
		
	}
	public function getSpecialProducts() {
		$this->load->language('module/special');
		$this->load->model('catalog/product');
		$this->load->model('tool/image');
		
	$limit = 100;
		
        
        $data_sort = array(
				'sort'  => 'pd.name',
				'order' => 'ASC',
				'start' => 0,
				'limit' => $limit
			);
		
		$language = (int)$this->request->get['language'];
		//$results = $this->model_catalog_product->getProductSpecialss(30,$language);
		$results = $this->model_catalog_product->getProductSpecialss($data_sort,$language);
	//	shuffle($results);
		if ($results) {
			foreach ($results as $result) {
				if ($result['image']) {
					$image = $this->model_tool_image->resize($result['image'], $this->config->get('config_image_product_width'), $this->config->get('config_image_product_height'));
				} else {
					$image = $this->model_tool_image->resize('placeholder.png', $this->config->get('config_image_product_width'), $this->config->get('config_image_product_height'));
				}

				if (($this->config->get('config_customer_price') && $this->customer->isLogged()) || !$this->config->get('config_customer_price')) {
					$price = $this->currency->format($this->tax->calculate($result['price'], $result['tax_class_id'], $this->config->get('config_tax')));
				} else {
					$price = false;
				}

				if ((float)$result['special']) {
					$special = $this->currency->format($this->tax->calculate($result['special'], $result['tax_class_id'], $this->config->get('config_tax')));
				} else {
					$special = false;
				}

				if ($this->config->get('config_tax')) {
					$tax = $this->currency->format((float)$result['special'] ? $result['special'] : $result['price']);
				} else {
					$tax = false;
				}

				if ($this->config->get('config_review_status')) {
					$rating = $result['rating'];
				} else {
					$rating = false;
				}

				$data['products'][] = array(
					'product_id'  => $result['product_id'],
					'thumb'       => $image,
					'name'        => $result['name'],
					'description' => utf8_substr(strip_tags(html_entity_decode($result['description'], ENT_QUOTES, 'UTF-8')), 0, $this->config->get('config_product_description_length')) . '..',
					'price'       => $price,
					'special'     => $special,
					'tax'         => $tax,
					'rating'      => $rating,
					'href'        => $this->url->link('product/product', 'product_id=' . $result['product_id'])
				);
			}
		}
		print_r(json_encode($results));
		
	}
	public function getfeaturedproducts() {

  $query = $this->db->query("SELECT setting FROM " . DB_PREFIX . "module where code='featured'");

		if ($query->num_rows) {
			$a = stripslashes($query->row['setting']);
		print_r($a);
		}
	}
	
		public function getfeaturedproductsNew() {

        $query = $this->db->query("SELECT setting FROM " . DB_PREFIX . "module where module_id=61 and code='boss_featured'");
		if ($query->num_rows) {
			$featuredData = stripslashes($query->row['setting']);
            
            $b = json_decode($featuredData);
            
            $fProducts = array(
					'name'  => $b->name,
					'product'       => $b->product_featured,
					'limit'        => $b->limit,
					'width' =>   $b->image_width,
					'height' =>   $b->image_height,
					'status'     => $b->status				
				);
            $featuredProducts = json_encode($fProducts);
		print_r($featuredProducts);
		}
	}
	
	protected function getNextDeliverySlot($sellerId, $languageId){


        $_SESSION['default']['language'] =  ($languageId==1) ? 'en' : 'ar';

        $seller_info = seller($sellerId);
        $this->load->language('common/header');
        $nextDeliverySlot = "";
        $days = array('monday'=>$this->language->get('text_monday'),'tuesday'=>$this->language->get('text_tuesday'),'wednesday'=>$this->language->get('text_wednesday'),'thursday'=>$this->language->get('text_thursday'),'friday'=>$this->language->get('text_friday'),'saturday'=>$this->language->get('text_saturday'),'sunday'=>$this->language->get('text_sunday'));
        if ( $seller_info ) {
            date_default_timezone_get();
            $nowTime = date("g:i a");
            $now = strtotime("now");
            $interval = $seller_info["delivery_timegap"] * 60;
            $package = $seller_info["package_ready"] * 60;
            $nowdata = dayData("now");

            if ($seller_info[strtolower(substr($nowdata["half"], 0, 3)) . "_start_time"] != "00:00:00"
                    && $seller_info[strtolower(substr($nowdata["half"], 0, 3)) . "_end_time"] != "00:00:00")
                {

                $nowstarttime = strtotime($nowdata["date"] . " " . $seller_info[strtolower(substr($nowdata["half"], 0, 3)) . "_start_time"]) + $interval;
                $nowendtime = strtotime($nowdata["date"] . " " . $seller_info[strtolower(substr($nowdata["half"], 0, 3)) . "_end_time"]);

               // echo $nowdata["date"].'---'.$seller_info[strtolower(substr($nowdata["half"], 0, 3)) . "_start_time"].'---'.$interval; die();
                $nowoutput = "";
                $nownextdelivery = "";
                $nowdelivery = "";
                $j = 0;
                for ($i = $nowstarttime; $i < $nowendtime; $i += $interval) {
                    $nntt = $i + $interval;
                    if ($i < $now) {
                        $nowoutput .= "<li class=unavailable>".$this->language->get('text_unavailable')."</li>";
                    } else {
                        if ($j == 0) {
                            $nextTime = $now + $package;
                            if ($i < $nextTime) {
                                $nowoutput .= "<li class=unavailable>".$this->language->get('text_unavailable')."</li>";
                            } else {


                                $nownextdelivery = arabicTime(strtotime(date("h:i a", $i)), $languageId);
                                $nowdelivery = arabicTime(strtotime(date("h:i a", $i)), $languageId) . " - " . arabicTime(strtotime(date("h:i a", $nntt)), $languageId);
                                $nowoutput .= "<li>" . arabicTime(strtotime(date("h:i a", $i)), $languageId) . " to " . arabicTime(strtotime(date("h:i a", $nntt)), $languageId) . "</li>";
                                $j++;
                            }
                        } else {
                            $nowoutput .= "<li>" . arabicTime(strtotime(date("h:i a", $i)), $languageId) . " to " . arabicTime(strtotime(date("h:i a", $nntt)), $languageId) . "</li>";
                            $j++;
                        }
                    }
                }
            } else {
                $nownextdelivery = $nowoutput = $this->language->get('text_close');
            }

            $fdata = dayData(" +1 day");

            if ($seller_info[strtolower(substr($fdata["half"], 0, 3)) . "_start_time"] != "00:00:00" && $seller_info[strtolower(substr($fdata["half"], 0, 3)) . "_end_time"] != "00:00:00") {
                $fstarttime = strtotime($fdata["date"] . " " . $seller_info[strtolower(substr($fdata["half"], 0, 3)) . "_start_time"]) + $interval;
                $fendtime = strtotime($fdata["date"] . " " . $seller_info[strtolower(substr($fdata["half"], 0, 3)) . "_end_time"]);
                $foutput = "";
                $fnextdelivery = "";
                $fdelivery = "";
                $k = 0;
                for ($i = $fstarttime; $i < $fendtime; $i += $interval) {
                    $nntt = $i + $interval;
                    if ($i < $now) {
                        $foutput .= "<li class=unavailable>".$this->language->get('text_unavailable')."</li>";
                    } else {
                        if ($k == 0) {
                            $nextTime = $now + $package;
                            if ($i < $nextTime) {
                                $foutput .= "<li class=unavailable>".$this->language->get('text_unavailable')."</li>";
                            } else {
                                $fnextdelivery = arabicTime(strtotime(date("h:i a", $i)), $languageId);
                                $fdelivery = arabicTime(strtotime(date("h:i a", $i)), $languageId) . " - " . arabicTime(strtotime(date("h:i a", $nntt)), $languageId);
                                $foutput .= "<li>" . arabicTime(strtotime(date("h:i a", $i)), $languageId) . " to " . arabicTime(strtotime(date("h:i a", $nntt)), $languageId) . "</li>";
                                $k++;
                            }
                        } else {
                            $foutput .= "<li>" . arabicTime(strtotime(date("h:i a", $i)), $languageId) . " to " . arabicTime(strtotime(date("h:i a", $nntt)), $languageId) . "</li>";
                            $k++;
                        }
                        //if ( $k == 0 ) { $fnextdelivery = arabicTime(strtotime(date("h:i a", $i)),$languageId); $fdelivery = arabicTime(strtotime(date("h:i a", $i)),$languageId)." - ".arabicTime(strtotime(date("h:i a", $nntt)),$languageId); }
                        //$foutput.= "<li>".arabicTime(strtotime(date("h:i a", $i)),$languageId)." to ".arabicTime(strtotime(date("h:i a", $nntt)),$languageId)."</li>"; $k++;
                    }
                }
            } else {
                $fnextdelivery = $foutput = $this->language->get('text_close');
            }

            $sdata = dayData(" +2 day");
            if ($seller_info[strtolower(substr($sdata["half"], 0, 3)) . "_start_time"] != "00:00:00" && $seller_info[strtolower(substr($sdata["half"], 0, 3)) . "_end_time"] != "00:00:00") {
                $sstarttime = strtotime($sdata["date"] . " " . $seller_info[strtolower(substr($sdata["half"], 0, 3)) . "_start_time"]) + $interval;
                $sendtime = strtotime($sdata["date"] . " " . $seller_info[strtolower(substr($sdata["half"], 0, 3)) . "_end_time"]);
                $soutput = "";
                $snextdelivery = "";
                $sdelivery = "";
                $l = 0;
                for ($i = $sstarttime; $i < $sendtime; $i += $interval) {
                    $nntt = $i + $interval;
                    if ($i < $now) {
                        $soutput .= "<li class=unavailable>".$this->language->get('text_unavailable')."</li>";
                    } else {
                        if ($l == 0) {
                            $nextTime = $now + $package;
                            if ($i < $nextTime) {
                                $soutput .= "<li class=unavailable>".$this->language->get('text_unavailable')."</li>";
                            } else {
                                $snextdelivery = arabicTime(strtotime(date("h:i a", $i)), $languageId);
                                $name = strtolower($sdata["full"]);
                                $sdelivery = $days[$name] . " " . arabicTime(strtotime(date("h:i a", $i)), $languageId) . " - " . arabicTime(strtotime(date("h:i a", $nntt)), $languageId);
                                $soutput .= "<li>" . arabicTime(strtotime(date("h:i a", $i)), $languageId) . " to " . arabicTime(strtotime(date("h:i a", $nntt)), $languageId) . "</li>";
                                $l++;
                            }
                        } else {
                            $soutput .= "<li>" . arabicTime(strtotime(date("h:i a", $i)), $languageId) . " to " . arabicTime(strtotime(date("h:i a", $nntt)), $languageId) . "</li>";
                            $l++;
                        }
                        //if ( $l == 0 ) { $snextdelivery = date("h:i a", $i); }
                        //$soutput.= "<li>".date("h:i a", $i)." to ".date("h:i a", $nntt)."</li>"; $l++;
                    }
                }
            } else {
                $soutput = $snextdelivery = $this->language->get('text_close');
            }

            $tdata = dayData(" +3 day");
            if ($seller_info[strtolower(substr($tdata["half"], 0, 3)) . "_start_time"] != "00:00:00" && $seller_info[strtolower(substr($tdata["half"], 0, 3)) . "_end_time"] != "00:00:00") {
                $tstarttime = strtotime($tdata["date"] . " " . $seller_info[strtolower(substr($tdata["half"], 0, 3)) . "_start_time"]) + $interval;
                $tendtime = strtotime($tdata["date"] . " " . $seller_info[strtolower(substr($tdata["half"], 0, 3)) . "_end_time"]);
                $toutput = "";
                $tnextdelivery = "";
                $tdelivery = "";
                $m = 0;
                for ($i = $tstarttime; $i < $tendtime; $i += $interval) {
                    $nntt = $i + $interval;
                    if ($i < $now) {
                        $toutput .= "<li class=unavailable>".$this->language->get('text_unavailable')."</li>";
                    } else {
                        if ($m == 0) {
                            $nextTime = $now + $package;
                            if ($i < $nextTime) {
                                $toutput .= "<li class=unavailable>".$this->language->get('text_unavailable')."</li>";
                            } else {
                                $tnextdelivery = arabicTime(strtotime(date("h:i a", $i)), $languageId);
                                $name = strtolower($tdata["full"]);
                                $tdelivery = $days[$name] . " " . arabicTime(strtotime(date("h:i a", $i)), $languageId) . " - " . arabicTime(strtotime(date("h:i a", $nntt)), $languageId);
                                $toutput .= "<li>" . arabicTime(strtotime(date("h:i a", $i)), $languageId) . " to " . arabicTime(strtotime(date("h:i a", $nntt)), $languageId) . "</li>";
                                $m++;
                            }
                        } else {
                            $toutput .= "<li>" . arabicTime(strtotime(date("h:i a", $i)), $languageId) . " to " . arabicTime(strtotime(date("h:i a", $nntt)), $languageId) . "</li>";
                            $m++;
                        }
                        //if ( $m == 0 ) { $tnextdelivery = arabicTime(strtotime(date("h:i a", $i)),$languageId); }
                        //$toutput.= "<li>".arabicTime(strtotime(date("h:i a", $i)),$languageId)." to ".arabicTime(strtotime(date("h:i a", $nntt)),$languageId)."</li>"; $m++;
                    }
                }
            } else {
                $toutput = $tnextdelivery = $this->language->get('text_close');
            }

            $ffdata = dayData(" +4 day");
            if ($seller_info[strtolower(substr($ffdata["half"], 0, 3)) . "_start_time"] != "00:00:00" && $seller_info[strtolower(substr($ffdata["half"], 0, 3)) . "_end_time"] != "00:00:00") {
                $ffstarttime = strtotime($ffdata["date"] . " " . $seller_info[strtolower(substr($ffdata["half"], 0, 3)) . "_start_time"]) + $interval;
                $ffendtime = strtotime($ffdata["date"] . " " . $seller_info[strtolower(substr($ffdata["half"], 0, 3)) . "_end_time"]);
                $ffoutput = "";
                $ffnextdelivery = "";
                $ffdelivery = "";
                $n = 0;
                for ($i = $ffstarttime; $i < $ffendtime; $i += $interval) {
                    $nntt = $i + $interval;
                    if ($i < $now) {
                        $foutput .= "<li class=unavailable>".$this->language->get('text_unavailable')."</li>";
                    } else {
                        if ($n == 0) {
                            $nextTime = $now + $package;
                            if ($i < $nextTime) {
                                $ffoutput .= "<li class=unavailable>".$this->language->get('text_unavailable')."</li>";
                            } else {
                                $ffnextdelivery = arabicTime(strtotime(date("h:i a", $i)), $languageId);
                                $name = strtolower($ffdata["full"]);
                                $ffdelivery = $days[$name] . " " . arabicTime(strtotime(date("h:i a", $i)), $languageId) . " - " . arabicTime(strtotime(date("h:i a", $nntt)), $languageId);
                                $ffoutput .= "<li>" . arabicTime(strtotime(date("h:i a", $i)), $languageId) . " to " . arabicTime(strtotime(date("h:i a", $nntt)), $languageId) . "</li>";
                                $n++;
                            }
                        } else {
                            $ffoutput .= "<li>" . arabicTime(strtotime(date("h:i a", $i)), $languageId) . " to " . arabicTime(strtotime(date("h:i a", $nntt)), $languageId) . "</li>";
                            $n++;
                        }
                        //if ( $n == 0 ) { $ffnextdelivery = date("h:i a", $i); }
                        //$ffoutput.= "<li>".date("h:i a", $i)." to ".date("h:i a", $nntt)."</li>"; $n++;
                    }
                }
            } else {
                $ffoutput = $ffnextdelivery = $this->language->get('text_close');
            }

            $fffdata = dayData(" +5 day");
            if ($seller_info[strtolower(substr($fffdata["half"], 0, 3)) . "_start_time"] != "00:00:00" && $seller_info[strtolower(substr($fffdata["half"], 0, 3)) . "_end_time"] != "00:00:00") {
                $fffstarttime = strtotime($fffdata["date"] . " " . $seller_info[strtolower(substr($fffdata["half"], 0, 3)) . "_start_time"]) + $interval;
                $fffendtime = strtotime($fffdata["date"] . " " . $seller_info[strtolower(substr($fffdata["half"], 0, 3)) . "_end_time"]);
                $fffoutput = "";
                $fffnextdelivery = "";
                $fffdelivery = "";
                $o = 0;
                for ($i = $fffstarttime; $i < $fffendtime; $i += $interval) {
                    $nntt = $i + $interval;
                    if ($i < $now) {
                        $fffoutput .= "<li class=unavailable>".$this->language->get('text_unavailable')."</li>";
                    } else {
                        if ($o == 0) {
                            $nextTime = $now + $package;
                            if ($i < $nextTime) {
                                $fffoutput .= "<li class=unavailable>".$this->language->get('text_unavailable')."</li>";
                            } else {
                                $fffnextdelivery = arabicTime(strtotime(date("h:i a", $i)), $languageId);

                                $name = strtolower($fffdata["full"]);

                                $fffdelivery = $days[$name] . " " . arabicTime(strtotime(date("h:i a", $i)), $languageId) . " - " . arabicTime(strtotime(date("h:i a", $nntt)), $languageId);
                                $fffoutput .= "<li>" . arabicTime(strtotime(date("h:i a", $i)), $languageId) . " to " . arabicTime(strtotime(date("h:i a", $nntt)), $languageId) . "</li>";
                                $o++;
                            }
                        } else {
                            $fffoutput .= "<li>" . arabicTime(strtotime(date("h:i a", $i)), $languageId) . " to " . arabicTime(strtotime(date("h:i a", $nntt)), $languageId) . "</li>";
                            $o++;
                        }
                        //if ( $o == 0 ) { $fffnextdelivery = arabicTime(strtotime(date("h:i a", $i)),$languageId); }
                        //$fffoutput.= "<li>".arabicTime(strtotime(date("h:i a", $i)),$languageId)." to ".arabicTime(strtotime(date("h:i a", $nntt)),$languageId)."</li>"; $o++;
                    }
                }
            } else {
                $fffoutput = $fffnextdelivery = $this->language->get('text_close');
            }

            $ssdata = dayData(" +6 day");
            if ($seller_info[strtolower(substr($ssdata["half"], 0, 3)) . "_start_time"] != "00:00:00" && $seller_info[strtolower(substr($ssdata["half"], 0, 3)) . "_end_time"] != "00:00:00") {
                $ssstarttime = strtotime($ssdata["date"] . " " . $seller_info[strtolower(substr($ssdata["half"], 0, 3)) . "_start_time"]) + $interval;
                $ssendtime = strtotime($ssdata["date"] . " " . $seller_info[strtolower(substr($ssdata["half"], 0, 3)) . "_end_time"]);
                $ssoutput = "";
                $ssnextdelivery = "";
                $ssdelivery = "";
                $p = 0;
                for ($i = $ssstarttime; $i < $ssendtime; $i += $interval) {
                    $nntt = $i + $interval;
                    if ($i < $now) {
                        $ssoutput .= "<li class=unavailable>".$this->language->get('text_unavailable')."</li>";
                    } else {
                        if ($p == 0) {
                            $nextTime = $now + $package;
                            if ($i < $nextTime) {
                                $ssoutput .= "<li class=unavailable>".$this->language->get('text_unavailable')."</li>";
                            } else {
                                $ssnextdelivery = arabicTime(strtotime(date("h:i a", $i)), $languageId);

                                $name = strtolower($ssdata["full"]);
                                $ssdelivery = $days[$name] . " " . arabicTime(strtotime(date("h:i a", $i)), $languageId) . " - " . arabicTime(strtotime(date("h:i a", $nntt)), $languageId);
                                $ssoutput .= "<li>" . arabicTime(strtotime(date("h:i a", $i)), $languageId) . " to " . arabicTime(strtotime(date("h:i a", $nntt)), $languageId) . "</li>";
                                $p++;
                            }
                        } else {
                            $ssoutput .= "<li>" . arabicTime(strtotime(date("h:i a", $i)), $languageId) . " to " . arabicTime(strtotime(date("h:i a", $nntt)), $languageId) . "</li>";
                            $p++;
                        }

                    }
                }
            } else {
                $ssoutput = $ssnextdelivery = $this->language->get('text_close');
            }

                 if((isset($nowdelivery)) && ($nowdelivery != "")) {
                     $nextDeliverySlot = $this->language->get('text_next_delivery')."  ". $this->language->get('text_today')." ".$nowdelivery  ;
                } elseif((isset($fdelivery)) && ($fdelivery != "")) {
                     $nextDeliverySlot = $this->language->get('text_next_delivery') ."  ". $this->language->get('text_tomorrow')." ".$fdelivery ;
                } elseif((isset($sdelivery)) && ($sdelivery != "")) {
                     $nextDeliverySlot =  $this->language->get('text_next_delivery') ."  ".$sdelivery ;
                } elseif((isset($tdelivery)) && ($tdelivery != "")) {
                     $nextDeliverySlot =  $this->language->get('text_next_delivery') ."  ".$tdelivery ;
                } elseif((isset($ffdelivery)) && ($ffdelivery != "")) {
                     $nextDeliverySlot =  $this->language->get('text_next_delivery') ." ".$ffdelivery ;
                } elseif((isset($fffdelivery)) && ($fffdelivery != "")) {
                     $nextDeliverySlot =  $this->language->get('text_next_delivery') ." ".$fffdelivery ;
                } elseif((isset($ssdelivery)) && ($ssdelivery != "")) {
                     $nextDeliverySlot = $this->language->get('text_next_delivery') ." ".$ssdelivery ;
                } else {
                     $nextDeliverySlot = " ". $this->language->get('text_no_delivery') ;
                }


        }

        return $nextDeliverySlot;
    }//getNextDeliverySlot
	
	
}