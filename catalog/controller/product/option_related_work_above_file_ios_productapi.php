<?php
class ControllerProductIosProductapi extends Controller {
	
	public function getLatestProducts() {
		$this->load->language('product/product');

		$this->load->model('catalog/product');
		$this->load->model('tool/image');
		
		$LatestProducts = $this->model_catalog_product->getLatestProducts(20);
		
		foreach($LatestProducts as $key => $value)
		{
		  $LatestProducts[$key]['image'] = $this->model_tool_image->resize($LatestProducts[$key]['image'], 104, 120);
		}

		print_r(json_encode($LatestProducts));
		
	}
	public function getRelatedProducts() {
		$this->load->language('product/product');

		$this->load->model('catalog/product');
		$this->load->model('tool/image');
		if (isset($this->request->get['product_id'])) {
			$product_id = (int)$this->request->get['product_id'];
		} else {
			$product_id = 0;
		}
		$language = (int)$this->request->get['language'];
		$RelatedProducts = $this->model_catalog_product->getProductRelatedes($product_id,$language);
		
		foreach($RelatedProducts as $key => $value)
		{
		  $RelatedProducts[$key]['image'] = $this->model_tool_image->resize($RelatedProducts[$key]['image'], 104, 120);
		}

		print_r(json_encode($RelatedProducts));
		
	}
	
	public function getProductImages() {
		
		$this->load->model('catalog/product');
		$this->load->model('tool/image');
		
		if (isset($this->request->get['product_id'])) {
			$product_id = (int)$this->request->get['product_id'];
		} else {
			$product_id = 0;
		}
		
		$productImages = $this->model_catalog_product->getProductImages($product_id);
		
		foreach($productImages as $key => $value)
		{
		  $productImages[$key]['image'] = $this->model_tool_image->resize($productImages[$key]['image'], 570, 660);
		}
		
		print_r(json_encode($productImages));
	}
	public function getbanner() {
		static $module = 0;

		$this->load->model('design/banner');
		$this->load->model('tool/image');

		$data['banners'] = array();

		$results = $this->model_design_banner->getBanner(7);

		foreach ($results as $result) {
			if (is_file(DIR_IMAGE . $result['image'])) {
				$data['banners'][] = array(
					'title' => $result['title'],
					'link'  => $result['link'],
					'image' => $this->model_tool_image->resize($result['image'], 955, 380)
				);
			}
		}
		

		$data['module'] = $module++;

		print_r(json_encode($data['banners']));
		}
	public function getloginpic() {
		static $module = 0;

		$this->load->model('design/banner');
		$this->load->model('tool/image');

		$data['banners'] = array();

		$results = $this->model_design_banner->getBanner(11);

		foreach ($results as $result) {
			if (is_file(DIR_IMAGE . $result['image'])) {
				$data['banners'][] = array(
					'title' => $result['title'],
					'link'  => $result['link'],
					'image' => $this->model_tool_image->resize($result['image'], 955, 380)
				);
			}
		}
		

		$data['module'] = $module++;

		print_r(json_encode($data['banners']));
		}
	public function getProduct() {
		$this->load->language('product/product');

		$this->load->model('catalog/product');
		$this->load->model('tool/image');
		
		if (isset($this->request->get['product_id'])) {
			$product_id = (int)$this->request->get['product_id'];
		} else {
			$product_id = 0;
		}
		$language = (int)$this->request->get['language'];
		$productDetails = $this->model_catalog_product->getProductes($product_id,$language);
		$productDetails['description'] = html_entity_decode($productDetails['description']);
		
		
		$productDetails['options'] = array();

			foreach ($this->model_catalog_product->getProductOptions($this->request->get['product_id']) as $option) {
				$product_option_value_data = array();

				foreach ($option['product_option_value'] as $option_value) {
					if (!$option_value['subtract'] || ($option_value['quantity'] > 0)) {
						if ((($this->config->get('config_customer_price') && $this->customer->isLogged()) || !$this->config->get('config_customer_price')) && (float)$option_value['price']) {
							$price = $this->currency->format($this->tax->calculate($option_value['price'], $productDetails['tax_class_id'], $this->config->get('config_tax') ? 'P' : false));
						} else {
							$price = false;
						}

						$product_option_value_data[] = array(
							'product_option_value_id' => $option_value['product_option_value_id'],
							'option_value_id'         => $option_value['option_value_id'],
							'name'                    => $option_value['name'],
							'image'                   => $this->model_tool_image->resize($option_value['image'], 50, 50),
							'price'                   => $price,
							'price_prefix'            => $option_value['price_prefix']
						);
					}
				}

				$productDetails['options'][] = array(
					'product_option_id'    => $option['product_option_id'],
					'product_option_value' => $product_option_value_data,
					'option_id'            => $option['option_id'],
					'name'                 => $option['name'],
					'type'                 => $option['type'],
					'value'                => $option['value'],
					'required'             => $option['required']
				);
			}

			if ($productDetails['minimum']) {
				$productDetails['minimum'] = $productDetails['minimum'];
			} else {
				$productDetails['minimum'] = 1;
			}
		
		
		$productImage =  $productDetails['image'];
		$productImages = array();
		$productImages = $this->model_catalog_product->getProductImages($this->request->get['product_id']);
		
		foreach($productImages as $key => $value)
		{
		  $productImages[$key]['image'] = $this->model_tool_image->resize($productImages[$key]['image'], 470, 350);
		}	
		
		$productImage = $this->model_tool_image->resize($productImage, 500, 350);
		
		$productDetails['image'] = array();
		array_push($productDetails['image'],$productImage);
		if(count($productImages)>0){
			for($i=0;$i<=count($productImages)-1;$i++){
				array_push($productDetails['image'],$productImages[$i]['image']);	
			}
		}
		
		print_r(json_encode($productDetails));
		
	}
	
	public function getProductByDevice() {
		$this->load->language('product/product');

		$this->load->model('catalog/product');
		$this->load->model('tool/image');
		$this->load->model('saccount/seller');
		
		if (isset($this->request->get['product_id'])) {
			$product_id = (int)$this->request->get['product_id'];
		} else {
			$product_id = 0;
		}
		$language = (int)$this->request->get['language'];
		$device_id = $this->request->get['device_id'];
		$seller_id = (int)$this->request->get['seller_id'];
		
		//City_id		
		$city_id = 0;		
		if((isset($this->request->get['city_id'])) && ($this->request->get['city_id'] > 0) && ($this->request->get['city_id'] != '')){
			$city_id = $this->request->get['city_id'];
		}

		$productDetails = $this->model_catalog_product->getSellerProductesByDevice($product_id,$language,$device_id,$seller_id,$city_id);
		if (!$productDetails) {
            $data = array('msg' => 'failed', 'result' => null);
            echo json_encode($data);
            die;
		}
		$productDetails['description'] = strip_tags(html_entity_decode($productDetails['description']));
		//$productDetails['description'] = strip_tags($productDetails['description']);
		
		$product_price = 0;
		if((isset($productDetails['special'])) && ($productDetails['special'] > 0)){
			$product_price = $productDetails['special'];
		} else if((isset($productDetails['price'])) && ($productDetails['price'] > 0)){
			$product_price = $productDetails['price'];
		}
		
		$productDetails['options'] = array();

		foreach ($this->model_catalog_product->getProductOptions($this->request->get['product_id']) as $option) {
			$product_option_value_data = array();

			foreach ($option['product_option_value'] as $option_value) {
				if (!$option_value['subtract'] || ($option_value['quantity'] > 0)) {
					if ((($this->config->get('config_customer_price') && $this->customer->isLogged()) || !$this->config->get('config_customer_price')) && (float)$option_value['price']) {
						$price = $this->currency->format($this->tax->calculate(($option_value['price'] + $product_price), $productDetails['tax_class_id'], $this->config->get('config_tax') ? 'P' : false));
					} else {
						$price = 0;
					}

					$product_option_value_data[] = array(
						'product_option_value_id' => $option_value['product_option_value_id'],
						'option_value_id'         => $option_value['option_value_id'],
						'name'                    => $option_value['name'],
						'image'                   => $this->model_tool_image->resize($option_value['image'], 50, 50),
						'price'                   => $price,
						'price_prefix'            => $option_value['price_prefix']
					);
				}
			}

			$productDetails['options'][] = array(
				'product_option_id'    => $option['product_option_id'],
				'product_option_value' => $product_option_value_data,
				'option_id'            => $option['option_id'],
				'name'                 => $option['name'],
				'type'                 => $option['type'],
				'value'                => $option['value'],
				'required'             => $option['required']
			);
		}
		
		$productDetails['option_available'] = (int)$productDetails['option_available'];

		if ($productDetails['minimum']) {
			$productDetails['minimum'] = $productDetails['minimum'];
		} else {
			$productDetails['minimum'] = 1;
		}
		
		
		$productImage =  $productDetails['image'];
		$productImages = array();
		$productImages = $this->model_catalog_product->getProductImages($this->request->get['product_id']);
		
		foreach($productImages as $key => $value)
		{
		  $productImages[$key]['image'] = $this->model_tool_image->resize($productImages[$key]['image'], 470, 350);
		}	
		
		$productImage = $this->model_tool_image->resize($productImage, 500, 350);
		
		$productDetails['image'] = array();
		array_push($productDetails['image'],$productImage);
		if(count($productImages)>0){
			for($i=0;$i<=count($productImages)-1;$i++){
				array_push($productDetails['image'],$productImages[$i]['image']);	
			}
		}
		$productDetails['seller'] = $this->model_saccount_seller->getSeller($seller_id);
	    
        $data = array('msg' => 'success', 'result' => $productDetails);
        echo json_encode($data);
	}
	
	/*
	public function getProductByDevice() {
		$this->load->language('product/product');

		$this->load->model('catalog/product');
		$this->load->model('tool/image');
		
		if (isset($this->request->get['product_id'])) {
			$product_id = (int)$this->request->get['product_id'];
		} else {
			$product_id = 0;
		}
		$language = (int)$this->request->get['language'];
		$device_id = (int)$this->request->get['device_id'];
		$productDetails = $this->model_catalog_product->getProductesByDevice($product_id,$language,$device_id);
		if (!$productDetails) {
            $data = array('msg' => 'failed', 'result' => null);
            echo json_encode($data);
            die;
		}
		$productDetails['description'] = html_entity_decode($productDetails['description']);
		
		
		$productDetails['options'] = array();

			foreach ($this->model_catalog_product->getProductOptions($this->request->get['product_id']) as $option) {
				$product_option_value_data = array();

				foreach ($option['product_option_value'] as $option_value) {
					if (!$option_value['subtract'] || ($option_value['quantity'] > 0)) {
						if ((($this->config->get('config_customer_price') && $this->customer->isLogged()) || !$this->config->get('config_customer_price')) && (float)$option_value['price']) {
							$price = $this->currency->format($this->tax->calculate($option_value['price'], $productDetails['tax_class_id'], $this->config->get('config_tax') ? 'P' : false));
						} else {
							$price = false;
						}

						$product_option_value_data[] = array(
							'product_option_value_id' => $option_value['product_option_value_id'],
							'option_value_id'         => $option_value['option_value_id'],
							'name'                    => $option_value['name'],
							'image'                   => $this->model_tool_image->resize($option_value['image'], 50, 50),
							'price'                   => $price,
							'price_prefix'            => $option_value['price_prefix']
						);
					}
				}

				$productDetails['options'][] = array(
					'product_option_id'    => $option['product_option_id'],
					'product_option_value' => $product_option_value_data,
					'option_id'            => $option['option_id'],
					'name'                 => $option['name'],
					'type'                 => $option['type'],
					'value'                => $option['value'],
					'required'             => $option['required']
				);
			}

			if ($productDetails['minimum']) {
				$productDetails['minimum'] = $productDetails['minimum'];
			} else {
				$productDetails['minimum'] = 1;
			}
		
		
		$productImage =  $productDetails['image'];
		$productImages = array();
		$productImages = $this->model_catalog_product->getProductImages($this->request->get['product_id']);
		
		foreach($productImages as $key => $value)
		{
		  $productImages[$key]['image'] = $this->model_tool_image->resize($productImages[$key]['image'], 470, 350);
		}	
		
		$productImage = $this->model_tool_image->resize($productImage, 500, 350);
		
		$productDetails['image'] = array();
		array_push($productDetails['image'],$productImage);
		if(count($productImages)>0){
			for($i=0;$i<=count($productImages)-1;$i++){
				array_push($productDetails['image'],$productImages[$i]['image']);	
			}
		}

	    
        $data = array('msg' => 'success', 'result' => $productDetails);
        echo json_encode($data);
	}
	
	public function getProducts() {
		$this->load->language('product/product');

		$this->load->model('catalog/product');
		$this->load->model('tool/image');		
		
		if (isset($this->request->get['category_id'])) {
			$category_id = (int)$this->request->get['category_id'];
		} else {
			$category_id = 0;
		}
		
		$filter_data = array(
			'filter_category_id'  => $category_id,
			'filter_sub_category' => true
		);		
		$language = (int)$this->request->get['language'];
			
		$AllProducts = $this->model_catalog_product->getProductss($filter_data,$language);
		
		foreach($AllProducts as $key => $value)
		{
		  $AllProducts[$key]['image'] = $this->model_tool_image->resize($AllProducts[$key]['image'], 470, 350);
		}
		print_r(json_encode($AllProducts));
		
	}
	*/
	
	public function getProductssub() {
		$this->load->language('product/product');

		$this->load->model('catalog/product');
		$this->load->model('tool/image');		
		
		if (isset($this->request->get['category_id'])) {
			$category_id = (int)$this->request->get['category_id'];
		} else {
			$category_id = 0;
		}
		
		$filter_data = array(
			'filter_category_id'  => $category_id,
			'filter_sub_category' => true
		);		
					
		$language = (int)$this->request->get['language'];
		$AllProducts = $this->model_catalog_product->getProductss($filter_data,$language);
		foreach($AllProducts as $key => $value)
		{
		  $AllProducts[$key]['image'] = $this->model_tool_image->resize($AllProducts[$key]['image'], 470, 350);
		}
		print_r(json_encode($AllProducts));				
	}
	public function getProductssubs() {
		$this->load->language('product/product');

		$this->load->model('catalog/product');
		$this->load->model('tool/image');		
		
		if (isset($this->request->get['category_id'])) {
			$category_id = (int)$this->request->get['category_id'];
		} else {
			$category_id = 0;
		}
		
		$filter_data = array(
			'filter_category_id'  => $category_id,
			'filter_sub_category' => true			
		);		
		
			
		$language = (int)$this->request->get['language'];
		$AllProducts = $this->model_catalog_product->getProductssfilter($filter_data,$language);
		
		foreach($AllProducts as $key => $value)
		{
		  $AllProducts[$key]['image'] = $this->model_tool_image->resize($AllProducts[$key]['image'], 470, 350);
		}
		print_r(json_encode($AllProducts));
		
	}
	

  	public function addToCart() {
  		$this->load->language('product/product');		
  		$this->load->model('catalog/product');
  		
  		$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "cartmobile WHERE device_id = '" . $this->request->post['device_id'] . "' and product_id='". $this->request->post['product_id'] ."' and product_option='". $this->request->post['product_option'] ."' and customer_id='". $this->request->post['customer_id'] ."'");

  		if(count($query->row)>0){
  			$this->db->query("UPDATE " . DB_PREFIX . "cartmobile SET product_quantity = '".($query->row['product_quantity']+$this->request->post['product_quantity'])."' where device_id='". $this->request->post['device_id'] ."' and product_id='". $this->request->post['product_id'] ."' and product_option='". $this->request->post['product_option'] ."' and customer_id='". $this->request->post['customer_id'] ."'");
  		}else{		
  			$this->db->query("INSERT INTO " . DB_PREFIX . "cartmobile SET device_id = '".$this->request->post['device_id']."', product_id = '".$this->request->post['product_id']."', product_quantity = '".$this->request->post['product_quantity']."', product_option = '".$this->request->post['product_option']."',customer_id='". $this->request->post['customer_id'] ."'");
  		}
  		
  		echo "success";		
  	}
 	
 	public function addToCartV2() {
 	  //Parameters Required:
      # 1 - Product Id
      # 2 - Product Quantity
      # 3 - Customer Id
      # 3 - Device Id
 	 
      $this->load->language('product/product');
      $this->load->model('catalog/product');
      
		if (isset($this->request->post['product_id'])) {
			$product_id = (int)$this->request->post['product_id'];
		} else {
			$product_id = 0;
		}
        
        $product_info = $this->model_catalog_product->getProduct($product_id);
        
        if ($product_info) {
            
            if (isset($this->request->post['product_quantity']) && ((int)$this->request->post['product_quantity'] >= $product_info['minimum'])) {
				$quantity = (int)$this->request->post['product_quantity'];
			} else {
				$quantity = 1;
			}
			
		
				$option = array();
			
            
            $product_options = $this->model_catalog_product->getProductOptions($this->request->post['product_id']);
            
            foreach ($product_options as $product_option) {
				if ($product_option['required'] && empty($option[$product_option['product_option_id']])) {
					$json['error']['option'][$product_option['product_option_id']] = sprintf($this->language->get('error_required'), $product_option['name']);
				}
			}

			if (isset($this->request->post['recurring_id'])) {
				$recurring_id = $this->request->post['recurring_id'];
			} else {
				$recurring_id = 0;
			}

			$recurrings = $this->model_catalog_product->getProfiles($product_info['product_id']);
            
            if ($recurrings) {
				$recurring_ids = array();

				foreach ($recurrings as $recurring) {
					$recurring_ids[] = $recurring['recurring_id'];
				}

				if (!in_array($recurring_id, $recurring_ids)) {
					$json['error']['recurring'] = $this->language->get('error_recurring_required');
				}
			}
            
            if (!$json) {
			   
				$this->cart->addMobile($this->request->post['product_id'], $quantity, $option, $recurring_id,$this->request->post['customer_id'],$this->request->post['device_id']);
                echo "success"; 
            }
            else{
                echo "failed";	 
            }
        }
        else{
           echo "failed";	 
        }		
	}
	
 
 	public function addToCartV3() {
		//Parameters Required:
		# 1 - Product Id
		# 2 - Product Quantity
		# 3 - Customer Id
		# 3 - Device Id
		# 4 - Language Id

		$json = array();
		$this->load->language('product/product');
		$this->load->model('catalog/product');
      
		if (isset($this->request->post['product_id'])) {
			$product_id = (int)$this->request->post['product_id'];
		} else {
			$product_id = 0;
		}
		if (isset($this->request->post['seller_id'])) {
			$sellerId = (int)$this->request->post['seller_id'];
		} else {
			$sellerId = 1;
		}
		
		//parent seller get
		$getParentSellerId = $this->db->query("SELECT parent_id FROM " . DB_PREFIX . "sellers WHERE seller_id = '" . (int)$sellerId . "' AND parent_id != 0");
		
		if(count($getParentSellerId->row)==0){
			$seller_id = $sellerId;
		}else{			
			$seller_id = $getParentSellerId->row['parent_id'];
		}	
		
		//check it product in seller product or not
		$getSellerProductsId = $this->db->query("SELECT id FROM " . DB_PREFIX . "sellers_products WHERE seller_id = '" . (int)$seller_id . "' AND product_id = '".$product_id."'");
		
		if(count($getSellerProductsId->row)!=0){
		
			//City_id		
			$city_id = 0;		
			if((isset($this->request->post['city_id'])) && ($this->request->post['city_id'] > 0) && ($this->request->post['city_id'] != '')){
				$city_id = $this->request->post['city_id'];
			}
			
			$product_info = $this->model_catalog_product->getProduct($product_id);
			
			if ($product_info) {
				
				if (isset($this->request->post['product_quantity']) && ((int)$this->request->post['product_quantity'] >= $product_info['minimum'])) {
					$quantity = (int)$this->request->post['product_quantity'];
				} else {
					$quantity = 0;
				}
				
				$option = array();
				
				if (isset($this->request->post['option'])) {
					$option = array_filter($this->request->post['option']);
				} else {
					$option = array();
				}
							
				$product_options = $this->model_catalog_product->getProductOptions($this->request->post['product_id']);
			   
				foreach ($product_options as $product_option) {
					if ($product_option['required'] && empty($option[$product_option['product_option_id']])) {
						$json['error']['option'][$product_option['product_option_id']] = sprintf($this->language->get('error_required'), $product_option['name']);
					}
				}

				if (isset($this->request->post['recurring_id'])) {
					$recurring_id = $this->request->post['recurring_id'];
				} else {
					$recurring_id = 0;
				}

				$recurrings = $this->model_catalog_product->getProfiles($product_info['product_id']);
				
				if ($recurrings) {
					$recurring_ids = array();

					foreach ($recurrings as $recurring) {
						$recurring_ids[] = $recurring['recurring_id'];
					}

					if (!in_array($recurring_id, $recurring_ids)) {
						//$json['error']['recurring'] = $this->language->get('error_recurring_required');
					}
				}
		  
				if (!$json) {
					
					if(isset($this->request->post['customer_id'])){
						if($this->request->post['customer_id'] == 0){
							if($this->request->post['device_id'] == ''){
								$json['error'] = 'Something went wrong!';
							}
						}				
					} else {
						if($this->request->post['device_id'] == ''){
							$json['error'] = 'Something went wrong!';
						}
					}
					
					if (!$json) {
					
						$this->cart->addMobileV2($this->request->post['product_id'], $quantity, $option, $recurring_id,$this->request->post['customer_id'],$this->request->post['device_id'],$seller_id);
						
						// code is added for geting total quantity of products in cart
						$allProducts = $this->cart->getMobileProducts($this->request->post['customer_id'],$this->request->post['language_id'],$this->request->post['device_id'],$city_id);
						
						$quantity = 0;
						foreach ($allProducts as $key => $value) {
							$quantity += $value['quantity'];
						}
						$product_data = array('msg' => 'success', 'product' => $quantity);
						echo json_encode($product_data);
					} else {
						$product_data = array('msg' => 'failed', 'product' => 0);
						echo json_encode($product_data);
					}
				} else if($json){
					$product_data = array('msg' => 'option required', 'product' => 0);
					echo json_encode($product_data);
				}
				else{
					$product_data = array('msg' => 'failed', 'product' => 0);
					echo json_encode($product_data);
				}
			}
			else{
				$product_data = array('msg' => 'failed', 'product' => 0);
				echo json_encode($product_data);
			}
		} else {
			$product_data = array('msg' => 'This product not available in store', 'product' => 0);
			echo json_encode($product_data);
		}
	}
	

  	public function getCart() {
  		$this->load->language('product/product');		
  		$this->load->model('catalog/product');
  		$this->load->model('tool/image');
  		$language = (int)$this->request->get['language'];
  		$query = $this->db->query("SELECT DISTINCT c.cart_id, pd.name, p.image, c.product_option, c.product_quantity,c.voucher 
  		FROM " . DB_PREFIX . "product_description pd JOIN " . DB_PREFIX . "cartmobile c ON c.product_id = pd.product_id and pd.language_id='".$language."' JOIN " . DB_PREFIX . "product p
  		ON p.product_id = c.product_id WHERE c.device_id = '" . $this->request->get['device_id'] . "'group by cart_id DESC");
  		
  		$AllProducts = $query->rows;
  		
  		foreach($AllProducts as $key => $value)
  		{
  		  $AllProducts[$key]['image'] = $this->model_tool_image->resize($AllProducts[$key]['image'], 470, 350);
  		}
  		
  		print_r(json_encode($AllProducts));		
  	}
 
    
	public function getCartV2() {
    	/*    //Parameters Required:
          # 1 - Language Id
          # 2 - Customer Id
          # 3 - Device Id
          
           
		$this->load->language('product/product');		
		$this->load->model('catalog/product');
		$this->load->model('tool/image');
         
        $language = (int)$this->request->get['language'];
        $customerId = (int)$this->request->get['customer_id'];
        $deviceId = $this->request->get['device_id'];
        $allProducts = $this->cart->getMobileProducts($customerId,$language,$deviceId);
        foreach($allProducts as $key => $value)
  		{
  		  $allProducts[$key]['image'] = $this->model_tool_image->resize($allProducts[$key]['image'], 470, 350);
  		}
	
		print_r(json_encode($allProducts));	 */	
		
		//Parameters Required:
		# 1 - Language Id
		# 2 - Customer Id
		# 3 - Device Id

		/* ------------------------------------------------------------------------------------ */

		/* $this->load->language('product/product');
		$this->load->model('catalog/product');
		$this->load->model('tool/image');

		$language = (int)$this->request->get['language'];
		$customerId = (int)$this->request->get['customer_id'];
		$deviceId = $this->request->get['device_id'];
		$is_ios = (int)(isset($this->request->get['ios'])) ? $this->request->get['ios'] : 0;
		
		//City_id		
		$city_id = 0;		
		if((isset($this->request->get['city_id'])) && ($this->request->get['city_id'] > 0) && ($this->request->get['city_id'] != '')){
			$city_id = $this->request->get['city_id'];
		}

		$allProducts = $this->cart->getMobileProductsNew($customerId,$language,$deviceId,$city_id);
		
		foreach($allProducts as $key1 => $value1) {
		 
			foreach($value1 as $key => $value)
			{				
				$value1[$key]['image'] = $this->model_tool_image->resize($value['image'], 470, 350);
			}
		 
			$allProductsNew[] = array(
				'seller_id'       => $key1,				
				'products'         => $value1
			);
			
		}
		
        if($is_ios){
			$data = array('msg' => 'success', 'result' => $allProductsNew);
			echo json_encode($data);
			die;
		}else{
			print_r(json_encode($allProductsNew));
		} */
		
		/* ------------------------------------------------------------------------------------ */
		
		$this->load->language('product/product');
		$this->load->model('catalog/product');
		$this->load->model('tool/image');
		$this->load->model('total/coupon'); 
        $this->load->language('total/coupon');

		$language = (int)$this->request->get['language'];
		$customerId = (int)$this->request->get['customer_id'];
		$deviceId = $this->request->get['device_id'];
		$is_ios = (int)(isset($this->request->get['ios'])) ? $this->request->get['ios'] : 0;
		
		//City_id		
		$city_id = 0;		
		if((isset($this->request->get['city_id'])) && ($this->request->get['city_id'] > 0) && ($this->request->get['city_id'] != '')){		
			$city_id = $this->request->get['city_id'];
		}

		$allProducts = $this->cart->getMobileProductsNew($customerId,$language,$deviceId,$city_id);
		
		foreach($allProducts as $key1 => $value1) {
		 
			$coupon_code = '';
			$coupon = '';
			foreach($value1 as $key => $value)
			{				
				$value1[$key]['image'] = $this->model_tool_image->resize($value['image'], 470, 350);
				
				$coupon_code = $value['coupon_code'];
				
				if((isset($coupon_code)) && ($coupon_code != '')){
					
					$seller_id = $key1;
					//add New Code end					
					$coupon_info = $this->model_total_coupon->getMobileCoupon($coupon_code,$customerId,$language,$deviceId);
				 
					$isCoupon = ($coupon_info) ? true : false; 
					
					if($isCoupon){						
						$coupon_info = $this->model_total_coupon->getMobileTotalNew($coupon_code,$customerId,$language,$deviceId,$seller_id);
							
						$coupon = $coupon_info[0]['value'];
					}
				}
			}
			
			if((isset($coupon)) && ($coupon > 0)){
				$couponVal = $coupon;
			} else {
				$couponVal = 0;
			}
		 
			$allProductsNew[] = array(
				'seller_id'    => $key1,
				'coupon'       => $couponVal,				
				'products'     => $value1
			);
			
		}
		
        if($is_ios){
			$data = array('msg' => 'success', 'result' => $allProductsNew);
			echo json_encode($data);
			die;
		}else{
			print_r(json_encode($allProductsNew));
		}
	}
	
	public function getCartV2New() {
		//Parameters Required:
		# 1 - Language Id
		# 2 - Customer Id
		# 3 - Device Id


		$this->load->language('product/product');
		$this->load->model('catalog/product');
		$this->load->model('tool/image');

		$language = (int)$this->request->get['language'];
		$customerId = (int)$this->request->get['customer_id'];
		$deviceId = $this->request->get['device_id'];
		$is_ios = (int)(isset($this->request->get['ios'])) ? $this->request->get['ios'] : 0;
		
		//City_id		
		$city_id = 0;		
		if((isset($this->request->get['city_id'])) && ($this->request->get['city_id'] > 0) && ($this->request->get['city_id'] != '')){
			$city_id = $this->request->get['city_id'];
		}

		$allProducts = $this->cart->getMobileProductsNew($customerId,$language,$deviceId,$city_id);
		
		foreach($allProducts as $key1 => $value1) {
		 
			foreach($value1 as $key => $value)
			{				
				$value1[$key]['image'] = $this->model_tool_image->resize($value['image'], 470, 350);
			}
		 
			$allProductsNew[] = array(
				'seller_id'       => $key1,				
				'products'         => $value1
			);
			
		}
		
        if($is_ios){
			$data = array('msg' => 'success', 'result' => $allProductsNew);
			echo json_encode($data);
			die;
		}else{
			print_r(json_encode($allProductsNew));
		}
	}
	
	public function removeItemFromCart() {
		$query = $this->db->query("DELETE FROM " . DB_PREFIX . "cartmobile WHERE cart_id = '" . $this->request->post['cart_id'] . "'");
	}
    
    public function removeItemFromCartV2() {
		$query = $this->db->query("DELETE FROM " . DB_PREFIX . "cart WHERE cart_id = '" . $this->request->post['cart_id'] . "'");
	}
    
	public function addvoucher() {
		$query = $this->db->query("UPDATE  " . DB_PREFIX . "cartmobile SET voucher = '".$this->request->get['voucher']."'  where	device_id = '" . $this->request->get['device_id'] . "'");
		echo "success";
	}
	
	public function emptyCart() {
		$query = $this->db->query("DELETE FROM " . DB_PREFIX . "cartmobile WHERE 	device_id = '" . $this->request->post['device_id'] . "'");
	}
    	public function emptyCartV2() {
    	   $customerId = (int)$this->request->post['customer_id'];
    	   $deviceId = $this->request->post['device_id'];


			   $sqlQuery = "DELETE FROM " . DB_PREFIX . "cart WHERE device_id = '" . $deviceId . "'";
			   if ($customerId > 0) {
				   $sqlQuery .= " OR customer_id = '" . (int)$customerId . "'";
			   }
			   $query = $this->db->query($sqlQuery);
			   $data = array('msg' => 'success', 'result' => 'Items Removed Successfully');
			   echo json_encode($data);
			   die;

	}
	
	public function updateCartQuantity() {
		$query = $this->db->query("UPDATE " . DB_PREFIX . "cartmobile SET product_quantity = '".$this->request->post['product_quantity']."',voucher='0' WHERE cart_id = '" . $this->request->post['cart_id'] . "'");
	}
    public function updateCartQuantityV2() {
		$query = $this->db->query("UPDATE " . DB_PREFIX . "cart SET quantity = '".$this->request->post['product_quantity']."' WHERE cart_id = '" . $this->request->post['cart_id'] . "'");
	}
	
	public function updateCartQuantityV3() {
		$action = $this->request->post['action'];
		$product_quantity = $this->request->post['product_quantity'];
		if($action == 'add'){
			$query = $this->db->query("UPDATE " . DB_PREFIX . "cart SET quantity = quantity+1 WHERE cart_id = '" . $this->request->post['cart_id'] . "'");
		} else if($action == 'minus'){
			$query = $this->db->query("SELECT quantity FROM " . DB_PREFIX . "cart WHERE cart_id = '" . $this->request->post['cart_id'] . "'");
			
			if($query->num_rows>0 && $query->row['quantity']==1){
				$query = $this->db->query("DELETE FROM " . DB_PREFIX . "cart WHERE cart_id = '" . $this->request->post['cart_id'] . "'");
			}
			
			$query = $this->db->query("UPDATE " . DB_PREFIX . "cart SET quantity = quantity-1 WHERE cart_id = '" . $this->request->post['cart_id'] . "'");
		}
	}
    
    public function updateCartAfterLogin()
    {
          //Parameters Required:         
          # 1 - Customer Id
          # 2 - Device Id   
    
            ########################
            # Update cart against ##
            # Customer if product ##
            # already exist       ##
            ########################
            
            //Get all products of customer against customer_id
            $cart_query_web = $this->db->query("SELECT * FROM " . DB_PREFIX . "cart WHERE customer_id = '" . (int)$this->request->post['customer_id'] . "'");
    
            // Get Customer All Products where customer id= 0 device_id is not null
            $cart_query_mobile = $this->db->query("SELECT * FROM " . DB_PREFIX . "cart WHERE customer_id = '0' AND device_id = '" . $this->request->post['device_id'] . "'");
            
            
            foreach ($cart_query_mobile->rows as $cart) {
                // check if this product is already exist against customer
                 foreach ($cart_query_web->rows as $cartWeb) {
                    
                        if($cartWeb['product_id'] == $cart['product_id']){
                            // If Product Id exists update mobile product quantity and remove web product
                            
                            $newQuantity = (int)$cartWeb['quantity'] + (int)$cart['quantity'];
                           
                               $this->db->query("UPDATE " . DB_PREFIX . "cart SET session_id = '" . $cartWeb['session_id'] . "' , quantity = '" . (int)$newQuantity . "'  WHERE cart_id = '" . (int)$cart['cart_id'] . "'");                          
                              $this->db->query("DELETE FROM " . DB_PREFIX . "cart WHERE cart_id = '" . (int)$cartWeb['cart_id'] . "'");
                        }
                    
                    }                           
				
			}
    //Update customer_id against device_id
    $query = $this->db->query("UPDATE " . DB_PREFIX . "cart SET customer_id = '".$this->request->post['customer_id']."' WHERE device_id = '" . $this->request->post['device_id'] . "'");
    
    }
	
	public function addToWishlist() {
		
		$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "wishlist WHERE device_id = '" . $this->request->post['device_id'] . "' and product_id='". $this->request->post['product_id'] ."'");		
		if(count($query->row)==0){
			$this->db->query("INSERT INTO " . DB_PREFIX . "wishlist SET device_id = '".$this->request->post['device_id']."', product_id = '".$this->request->post['product_id']."'");
			echo json_encode(array('msg' => "success" ));
		}else{
			echo json_encode(array('msg' => "Already Added to wishlist" ));
		}		
	}
	
	    public function addToWishlistV2() {
        
        // 1- Product Already saved from  web
        
         $customerId = (int)$this->request->post['customer_id'];
         $sellerId = (int) (isset($this->request->post['seller_id'])) ? $this->request->post['seller_id'] : 1 ;
		 $concat = '';
         if($customerId > 0)
        {
            $concat .= " (customer_id = '" . (int)$customerId . "' OR device_id = '" . $this->request->post['device_id'] . "')"; 
        }else{
           $concat .= " device_id = '" . $this->request->post['device_id'] . "'";  
        }
        $sqlQuery = "SELECT * FROM " . DB_PREFIX . "customer_wishlist WHERE " . $concat . "  AND product_id='". $this->request->post['product_id'] ."'";
		
		//$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "customer_wishlist WHERE customer_id = '" . $this->request->post['customer_id'] . "' and product_id='". $this->request->post['product_id'] ."'");	
        $query = $this->db->query($sqlQuery);
        
        $data = $query->row;
        
		if(count($query->row)==0){
		  
			$this->db->query("INSERT INTO " . DB_PREFIX . "customer_wishlist SET seller_id = '".$sellerId."', customer_id = '".$this->request->post['customer_id']."' , device_id = '".$this->request->post['device_id']."', product_id = '".$this->request->post['product_id']."' , date_added = NOW()");
			echo "success";
		}
        else{
           // Check if Item added from Web. Then remove web item and save mobile item
            if($data['device_id'] == null) // Item added from web
            {
                $this->db->query("Delete FROM " . DB_PREFIX . "customer_wishlist  WHERE customer_id = '" . $this->request->post['customer_id'] . "' AND product_id='". $this->request->post['product_id'] ."'");
                $this->db->query("INSERT INTO " . DB_PREFIX . "customer_wishlist SET seller_id = '".$sellerId."', customer_id = '".$this->request->post['customer_id']."' , device_id = '".$this->request->post['device_id']."', product_id = '".$this->request->post['product_id']."' , date_added = NOW()");
			    echo "success";
            }
            
        }		
	}
    
    public function addToWishlistV2_27032018() {
        
        // 1- Product Already saved from  web
        
         $customerId = (int)$this->request->post['customer_id'];
         if($customerId > 0)
        {
            $concat .= " (customer_id = '" . (int)$customerId . "' OR device_id = '" . $this->request->post['device_id'] . "')"; 
        }else{
           $concat .= " device_id = '" . $this->request->post['device_id'] . "'";  
        }
        $sqlQuery = "SELECT * FROM " . DB_PREFIX . "customer_wishlist WHERE " . $concat . "  AND product_id='". $this->request->post['product_id'] ."'";
		
		//$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "customer_wishlist WHERE customer_id = '" . $this->request->post['customer_id'] . "' and product_id='". $this->request->post['product_id'] ."'");	
        $query = $this->db->query($sqlQuery);
        
        $data = $query->row;
        
		if(count($query->row)==0){
		  
			$this->db->query("INSERT INTO " . DB_PREFIX . "customer_wishlist SET customer_id = '".$this->request->post['customer_id']."' , device_id = '".$this->request->post['device_id']."', product_id = '".$this->request->post['product_id']."' , date_added = NOW()");
			echo "success";
		}
        else{
           // Check if Item added from Web. Then remove web item and save mobile item
            if($data['device_id'] == null) // Item added from web
            {
                $this->db->query("Delete FROM " . DB_PREFIX . "customer_wishlist  WHERE customer_id = '" . $this->request->post['customer_id'] . "' AND product_id='". $this->request->post['product_id'] ."'");
                $this->db->query("INSERT INTO " . DB_PREFIX . "customer_wishlist SET customer_id = '".$this->request->post['customer_id']."' , device_id = '".$this->request->post['device_id']."', product_id = '".$this->request->post['product_id']."' , date_added = NOW()");
			    echo "success";
            }
            
        }		
	}
	
	public function getWishlist() {
		
		$this->load->language('product/product');		
		$this->load->model('catalog/product');
		$this->load->model('tool/image');
		$language = (int)$this->request->get['language'];
		$query = $this->db->query("SELECT distinct wishlist_id, pd.name, image, p.price,sp.price as special, p.product_id
FROM " . DB_PREFIX . "product_description pd
JOIN " . DB_PREFIX . "wishlist w ON w.product_id = pd.product_id and pd.language_id= '" . (int)$language  . "'
JOIN " . DB_PREFIX . "product p ON p.product_id = w.product_id
LEFT JOIN " . DB_PREFIX . "product_special sp ON sp.product_id = w.product_id
WHERE w.device_id =  '" . $this->request->get['device_id'] . "' group by wishlist_id asc");
		
		$AllProducts = $query->rows;
		
		foreach($AllProducts as $key => $value)
		{
		  $AllProducts[$key]['image'] = $this->model_tool_image->resize($AllProducts[$key]['image'], 470, 350);
		}
		
		print_r(json_encode($AllProducts));		
	}
    
   	public function getWishlistV2() {
   	    
        $this->load->language('product/product');		
		$this->load->model('catalog/product');
		$this->load->model('tool/image');
        
		$language  = (int)$this->request->get['language'];
        $device_id = $this->request->get['device_id'];
        $customerId = (int)$this->request->get['customer_id'];
        
		$concat = '';
        if($customerId > 0)
        {
            $concat .= " (CW.customer_id = '" . (int)$customerId . "' OR CW.device_id = '" . $device_id . "')"; 
        }else{
           $concat .= " CW.device_id = '" . $device_id . "'";  
        }
        $sqlQuery = "SELECT DISTINCT PD.product_id, PD.name, P.image, P.price,PS.price as special,s.seller_id,s.firstname,s.lastname  FROM " . DB_PREFIX . "customer_wishlist CW 
                     INNER JOIN " . DB_PREFIX . "product P ON CW.product_id = P.product_id 
                     INNER JOIN " . DB_PREFIX . "product_description PD ON P.product_id = PD.product_id
                     LEFT  JOIN " . DB_PREFIX . "product_special PS ON CW.product_id = PS.product_id
                     LEFT  JOIN " . DB_PREFIX . "sellers s ON s.seller_id = CW.seller_id
                     WHERE " . $concat .  " AND PD.language_id='" . $language . "' GROUP BY PD.product_id ASC ";
        
       
		$query = $this->db->query($sqlQuery);
        $allProducts = $query->rows;
       
		foreach($allProducts as $key => $value)
		{
		  $allProducts[$key]['image'] = $this->model_tool_image->resize($allProducts[$key]['image'], 470, 350);
		}
        
		print_r(json_encode($allProducts));
        
	}
	
	public function removeItemFromWishlist() {
		$query = $this->db->query("DELETE FROM " . DB_PREFIX . "wishlist WHERE wishlist_id = '" . $this->request->post['wishlist_id'] . "'");
	}
    
    public function removeItemFromWishlistV2() {
        
       $deviceId = $this->request->post['device_id'];
       $customerId = (int)$this->request->post['customer_id'];
       $productId = (int)$this->request->post['product_id'];
       if($customerId > 0)
       {
        //die("DELETE FROM " . DB_PREFIX . "customer_wishlist WHERE customer_id = '" . $customerId . "' AND  product_id = '" . $productId . "'");
        $query = $this->db->query("DELETE FROM " . DB_PREFIX . "customer_wishlist WHERE customer_id = '" . $customerId . "' AND  product_id = '" . $productId . "'");
       }
       else{
        //die("DELETE FROM " . DB_PREFIX . "customer_wishlist WHERE device_id = '" . $deviceId . "' AND  product_id = '" . $productId . "'");
        $query = $this->db->query("DELETE FROM " . DB_PREFIX . "customer_wishlist WHERE device_id = '" . $deviceId . "' AND  product_id = '" . $productId . "'");
       }
       
		echo "success";
	}
    
    public function updateWishlistOnLogin()
    {
        //Parameters Required:         
          # 1 - Customer Id
          # 2 - Device Id   
    
            ############################
            # Update wishlsit against ##
            # Customer if product     ##
            # already exist           ##
            ############################
            
            //Get all products of customer against customer_id
            
            $wishlist_query_web = $this->db->query("SELECT * FROM " . DB_PREFIX . "customer_wishlist WHERE customer_id = '" . (int)$this->request->post['customer_id'] . "'");
    
            // Get Customer All Products where customer id= 0 device_id is not null
            $wishlist_query_mobile = $this->db->query("SELECT * FROM " . DB_PREFIX . "customer_wishlist WHERE customer_id = '0' AND device_id = '" . $this->request->post['device_id'] . "'");
            
            
            foreach ($wishlist_query_mobile->rows as $wishlist) {
                // check if this product is already exist against customer
                 foreach ($wishlist_query_web->rows as $wishlistWeb) {
                    
                        if($wishlistWeb['product_id'] == $wishlist['product_id']){
                            // If Product Id exists update mobile product and remove web product
                               $this->db->query("DELETE FROM " . DB_PREFIX . "customer_wishlist WHERE customer_id = '" . (int)$this->request->post['customer_id'] . "' AND product_id = '" . (int)$wishlist['product_id'] . "'");
                               $this->db->query("UPDATE " . DB_PREFIX . "customer_wishlist SET customer_id = '" . (int)$this->request->post['customer_id'] . "' WHERE product_id = '" . (int)$wishlist['product_id'] . "'");                          
                             
                        }
                    
                    }                           
				
			}
    //Update customer_id against device_id
    $query = $this->db->query("UPDATE " . DB_PREFIX . "customer_wishlist SET customer_id = '".$this->request->post['customer_id']."' WHERE device_id = '" . $this->request->post['device_id'] . "'");
    
    }
	
	public function getCountries() {
		$this->load->model('localisation/country');
		$this->load->model('localisation/zone');
		
		$countries=$this->model_localisation_country->getCountries();
		
		print_r(json_encode($countries));		
	}
	
	public function getZones() {
		$this->load->model('localisation/zone');
		
		if(!isset($this->request->get['country_id'])){		
			$zones=$this->model_localisation_zone->getZonesByCountryId(99);
		}else{
			$zones=$this->model_localisation_zone->getZonesByCountryId($this->request->get['country_id']);	
		}
		
		print_r(json_encode($zones));		
	}
	
	public function getShippingMethods() {
		$results = $this->model_extension_extension->getExtensions('shipping');
		
			
	}
		public function coupon() {
			$this->load->language('api/coupon');

		$json = array();

		$this->load->model('total/coupon');

		if (isset($this->request->get['coupon'])) {
			$coupon = $this->request->get['coupon'];
		} else {
			$coupon = '';
		}
		$status = true;

		$coupon_query = $this->db->query("SELECT * FROM `" . DB_PREFIX . "coupon` WHERE code = '" . $this->db->escape($coupon) . "' AND ((date_start = '0000-00-00' OR date_start < NOW()) AND (date_end = '0000-00-00' OR date_end > NOW())) AND status = '1'");
		
		$Allcoupons = $coupon_query->rows;
		
		if(empty($Allcoupons)){
		$json = "error";
		}
		else
		{$json = $Allcoupons;
			}
		
		print_r(json_encode($json));		

	}
	
	public function couponDiscount()
    {
        $this->load->model('total/coupon'); 
        $this->load->language('total/coupon');
        
        $coupon   = $this->request->post['coupon'];
        $customer = (int) $this->request->post['customer_id'];
        $device   =  $this->request->post['device_id'];
        $language   = (int) $this->request->post['language_id'];
		
		$seller_id = $this->request->post['seller_id'];
		
		//add New Code start
		//City_id		
		$city_id = 0;		
		if(isset($this->request->post['city_id'])){
			$city_id = $this->request->post['city_id'];
		}
		
		$allProducts = $this->cart->getMobileProductsNew($customer,$language,'',$city_id);

		foreach($allProducts as $key1 => $value1) {			
			$allProductsNew[] = array(
				'seller_id'       => $key1			
			);		
		}	
		
		$seller_id = $allProductsNew[0]['seller_id'];
		//add New Code end
        
        $coupon_info = $this->model_total_coupon->getMobileCoupon($coupon,$customer,$language,$device);
     
        $isCoupon = ($coupon_info) ? true : false; 
        
        if($isCoupon){
            //$coupon_info = $this->model_total_coupon->getMobileTotal($coupon,$customer,$language,$device,$seller_id);
			
			$coupon_info = $this->model_total_coupon->getMobileTotalNew($coupon,$customer,$language,$device,$seller_id);
        }else{
          $coupon_info = array("msg"=>$this->language->get('error_coupon'));// Error
            
        }
        print_r(json_encode($coupon_info));	
    }
    public function getCartWithDeliveryCharges() {
        //Parameters Required:
        # 1 - Language Id
        # 2 - Customer Id
        # 3 - Device Id
        # 4 - Store Id


        $this->load->language('product/product');
        $this->load->model('catalog/product');
        $this->load->model('tool/image');
        $this->load->model('store/store');
        $this->load->model('api/transaction');
        
        $language = (int)$this->request->get['language'];
        $customerId = (int)$this->request->get['customer_id'];
        $deviceId = $this->request->get['device_id'];
        $sellerId = (int)$this->request->get['seller_id'];
		
		//City_id		
		$city_id = 0;		
		if((isset($this->request->get['city_id'])) && ($this->request->get['city_id'] > 0) && ($this->request->get['city_id'] != '')){
			$city_id = $this->request->get['city_id'];
		}
  
        $allProducts = $this->cart->getMobileProductsV2($customerId,$language,$deviceId,$sellerId,$city_id);
        $results = $this->model_api_transaction->getTransactions($customerId);
        
        $amount = 0;
		foreach ($results as $result) {
			$amount += $result['amount'];
		}
        
        $count = 0;
        foreach($allProducts as $key => $value)
        {
            
            if($count==0){
                $storeId =$value['seller_id'];
                $sellerInfo = $this->model_store_store->getSellerByID($storeId);
                
                $deliveryCharges = (isset($sellerInfo) && $sellerInfo['delivery_charges']) ? (int)$sellerInfo['delivery_charges'] : 0;
                $allProducts[$key]['delivery_charges'] = $deliveryCharges;
                $allProducts[$key]['minimum_order'] = (int)$sellerInfo['minimum_order'];
                $allProducts[$key]['store_credit'] = $this->currency->format($amount, $this->config->get('config_currency'));
                $count++;
            }
            $allProducts[$key]['image'] = $this->model_tool_image->resize($allProducts[$key]['image'], 470, 350);
        }

        if(count($allProducts) > 0 ){
            $result =  array('msg' => 'Success', 'result' => $allProducts);
        }
        else{
            $result =  array('msg' => 'Failed', 'result' => '');
        }

        echo json_encode($result);
    }
	
	public function getCartNew() {
        //Parameters Required:
        # 1 - Language Id
        # 2 - Customer Id
        # 3 - Device Id
        # 4 - Store Id


        $this->load->language('product/product');
        $this->load->model('catalog/product');
        $this->load->model('tool/image');
        $this->load->model('store/store');
        $this->load->model('api/transaction');
        
        $language = (int)$this->request->get['language'];
        $customerId = (int)$this->request->get['customer_id'];
        $deviceId = $this->request->get['device_id'];
        $sellerId = (int)$this->request->get['seller_id'];
		
		//City_id		
		$city_id = 0;		
		if((isset($this->request->get['city_id'])) && ($this->request->get['city_id'] > 0) && ($this->request->get['city_id'] != '')){
			$city_id = $this->request->get['city_id'];
		}
  
        $allProducts = $this->cart->getMobileProductsNew($customerId,$language,$deviceId,$city_id);
		
        $results = $this->model_api_transaction->getTransactions($customerId);
        
        $amount = 0;
		foreach ($results as $result) {
			$amount += $result['amount'];
		}
		
		foreach($allProducts as $key1 => $value1) {
			$total = 0;
			$storeId = $key1;			
			$sellerInfo = $this->model_store_store->getSellerByID($storeId);

			$deliveryCharges = (isset($sellerInfo) && $sellerInfo['delivery_charges']) ? (int)$sellerInfo['delivery_charges'] : 0;			
						
			foreach($value1 as $key => $value)
			{
				$value1[$key]['image'] = $this->model_tool_image->resize($value['image'], 470, 350);
				$total = $total + $value['total'];

			}
		 
			$allProductsNew[] = array(
				'seller_id'       => $key1,
				'seller_nameen'   => $sellerInfo['firstname'],
				'seller_namear'   => $sellerInfo['lastname'],
				'delivery_charges' => $deliveryCharges,
				'minimum_order'    => (int)$sellerInfo['minimum_order'],
				'seller_total' => $total,
				'store_credit'     => $this->currency->format($amount, $this->config->get('config_currency')),
				'products'         => $value1
				
			);
			
		}		
		
        if(count($allProducts) > 0 ){
            $result =  array('msg' => 'Success', 'result' => $allProductsNew);
        }
        else{
            $result =  array('msg' => 'Failed', 'result' => '');
        }

        echo json_encode($result);
    }
    
    
    public function getSellerProductsByCategory() {

        $this->load->model('catalog/product');
        $this->load->model('tool/image');

        //Parameters
        $language = (int)$this->request->get['language'];
        $seller_id = (int)$this->request->get['seller'];
        $is_area = (int)$this->request->get['is_area'];
        $category = (int)$this->request->get['category_id'];
        $device_id = $this->request->get['device_id'];
        
        
        $filter = '';
        $sort = 'p.sort_order';
       	$order = 'ASC';
        $page = 1;
        $limit = 1000;
        
      $filter_data = array(
				'filter_category_id' => $category,
				'filter_filter'      => $filter,
				'sort'               => $sort,
				'order'              => $order,
				'start'              => ($page - 1) * $limit,
				'limit'              => $limit
			);


        if($is_area==1) //GEt AreaId
        {
            $area_id = (int)$this->request->get['area'];
            $products = $this->model_catalog_product->getSellerProductsAreaWiseAPI($filter_data,$area_id,$seller_id,$language,$device_id);
            
        }else if($is_area==0){
           
            $lat = $this->request->get['lat'];
            $lng = $this->request->get['lng'];
            $radiusKm = $this->request->get['radius'];
            
            $products = $this->model_catalog_product->getSellerProductsRadiusWiseAPI($filter_data,$radiusKm,$lat,$lng,$seller_id,$language,$device_id);
        }
        
        foreach($products as $key => $value)
		{
		    $in_wishlist =  $this->model_catalog_product->checkItemInWishlist($products[$key]['product_id'],$seller_id,$device_id);
			$product_qunatity_in_cart = $this->model_catalog_product->checkItemInCart($products[$key]['product_id'],$seller_id,$device_id);

		  $products[$key]['image'] = $this->model_tool_image->resize($products[$key]['image'], 470, 350);
		  $products[$key]['in_wishlist'] = $in_wishlist;
		  $products[$key]['product_qunatity_in_cart'] = $product_qunatity_in_cart;
		}
        
        
		print_r(json_encode($products));
		

    }
    
     public function getSellerProductsByCategoryIOS() {

        $this->load->model('catalog/product');
        $this->load->model('tool/image');

        //Parameters
        $language = (int)$this->request->get['language'];
        $seller_id = (int)$this->request->get['seller'];
        $is_area = (int)$this->request->get['is_area'];
        $category = (int)$this->request->get['category_id'];
        $device_id = $this->request->get['device_id'];
        
        $filter = '';
        $sort = 'p.sort_order';
       	$order = 'ASC';
        $page = 1;
        $limit = 1000;
        
      $filter_data = array(
				'filter_category_id' => $category,
				'filter_filter'      => $filter,
				'sort'               => $sort,
				'order'              => $order,
				'start'              => ($page - 1) * $limit,
				'limit'              => $limit
			);


        if($is_area==1) //GEt AreaId
        {
            $area_id = (int)$this->request->get['area'];
            $products = $this->model_catalog_product->getSellerProductsAreaWiseAPI($filter_data,$area_id,$seller_id,$language,$device_id);
            
        }else if($is_area==0){
           
            $lat = $this->request->get['lat'];
            $lng = $this->request->get['lng'];
            $radiusKm = $this->request->get['radius'];
            
            $products = $this->model_catalog_product->getSellerProductsRadiusWiseAPI($filter_data,$radiusKm,$lat,$lng,$seller_id,$language,$device_id);
        }
        

        foreach($products as $key => $value)
		{
		  $products[$key]['image'] = $this->model_tool_image->resize($products[$key]['image'], 470, 350);
		  
		   $in_wishlist =  $this->model_catalog_product->checkItemInWishlist($products[$key]['product_id'],$seller_id,$device_id);
		   $product_qunatity_in_cart = $this->model_catalog_product->checkItemInCart($products[$key]['product_id'],$seller_id,$device_id);
		   
		   $products[$key]['in_wishlist'] = $in_wishlist;
		  $products[$key]['product_qunatity_in_cart'] = $product_qunatity_in_cart;
		}
        
        
        $AllProducts['products'] = array();
		foreach ($products as $key => $value) {
			array_push($AllProducts['products'], $value);
		}
		echo(json_encode($AllProducts));
	

    }
	
	public function getSellerRelatedProducts() {
		$this->load->language('product/product');

		$this->load->model('catalog/product');
		$this->load->model('tool/image');
	
	    $product_id = (int)$this->request->get['product_id'];
		$language = (int)$this->request->get['language'];
        $seller_id = (int)$this->request->get['seller'];
        $is_area = (int)$this->request->get['is_area'];
        
        
        if($is_area==1) //GEt AreaId
        {
            $area_id = (int)$this->request->get['area'];
            $relatedProducts = $this->model_catalog_product->getSellerProductRelatedAreaWiseAPI($this->request->get['product_id'],$area_id,$seller_id,$language);
        }else if($is_area==0){
	       
            $lat = $this->request->get['lat'];
            $lng = $this->request->get['lng'];
            $radiusKm = $this->request->get['radius'];
            $relatedProducts = $this->model_catalog_product->getSellerProductRelatedRadiusWiseAPI($this->request->get['product_id'],$radiusKm,$lat,$lng,$seller_id,$language);
		}
        
        	
		foreach($relatedProducts as $key => $value)
		{
		  $relatedProducts[$key]['image'] = $this->model_tool_image->resize($relatedProducts[$key]['image'], 104, 120);
		}
		
		print_r(json_encode($relatedProducts));
		
	}
		public function getSellerRelatedProductsIOS() {
		$this->load->language('product/product');

		$this->load->model('catalog/product');
		$this->load->model('tool/image');
	
	    $product_id = (int)$this->request->get['product_id'];
		$language = (int)$this->request->get['language'];
        $seller_id = (int)$this->request->get['seller'];
        $is_area = (int)$this->request->get['is_area'];
        
        
        if($is_area==1) //GEt AreaId
        {
            $area_id = (int)$this->request->get['area'];
            $relatedProducts = $this->model_catalog_product->getSellerProductRelatedAreaWiseAPI($this->request->get['product_id'],$area_id,$seller_id,$language);
        }else if($is_area==0){
	       
            $lat = $this->request->get['lat'];
            $lng = $this->request->get['lng'];
            $radiusKm = $this->request->get['radius'];
            $relatedProducts = $this->model_catalog_product->getSellerProductRelatedRadiusWiseAPI($this->request->get['product_id'],$radiusKm,$lat,$lng,$seller_id,$language);
		}
        
        	
		foreach($relatedProducts as $key => $value)
		{
		  $relatedProducts[$key]['image'] = $this->model_tool_image->resize($relatedProducts[$key]['image'], 104, 120);
		}
		
		$AllProductss['products'] = array();
		foreach ($relatedProducts as $key => $value) {
		    
			array_push($AllProductss['products'], $value);
		}

		print_r(json_encode($AllProductss));
		
	}
	public function getSpecialProductsBySeller(){
        $this->load->model('catalog/product');
        $this->load->model('tool/image');
        $this->load->model('saccount/seller');		

        //Parameters
        $seller_id = (int)$this->request->get['seller'];
        $is_area = (int)$this->request->get['is_area'];
        $device_id = $this->request->get['device_id'];
        $language_id = $this->request->get['language_id'];

        $sort = 'p.sort_order';
        $order = 'ASC';
        $page = 1;
        $limit = 50;

        $filter_data = array(
            'sort'               => $sort,
            'order'              => $order,
            'start'              => ($page - 1) * $limit,
            'limit'              => $limit
        );

        if($is_area==1) //GEt AreaId
        {
            $area_id = (int)$this->request->get['area'];
            $products = $this->model_catalog_product->getSellerProductSpecialsAreaWise($filter_data, $area_id, $seller_id,$language_id);

        }else if($is_area==0){

            $lat = $this->request->get['lat'];
            $lng = $this->request->get['lng'];
            $radiusKm = $this->request->get['radius'];

			//$products = $this->model_catalog_product->getSellerCategoriesRadiusWise($filter_data, $radiusKm,$lat,$lng,$seller_id,$language_id);
            $products = $this->model_catalog_product->getSellerProductSpecialsRadiusWise($filter_data, $radiusKm,$lat,$lng,$seller_id,$language_id);
        }

       
        foreach($products as $key => $value)
        {
            unset($products[$key]['description']) ;
            $products[$key]['image'] = $this->model_tool_image->resize($products[$key]['image'], 470, 350);
            
            $in_wishlist =  $this->model_catalog_product->checkItemInWishlist($products[$key]['product_id'],$seller_id,$device_id);
	        $product_qunatity_in_cart = $this->model_catalog_product->checkItemInCart($products[$key]['product_id'],$seller_id,$device_id);

	        $products[$key]['in_wishlist'] = $in_wishlist;
	        $products[$key]['product_qunatity_in_cart'] = $product_qunatity_in_cart;
	        
            $products[$key]['seller'] = $this->model_saccount_seller->getSeller($seller_id);
        }
        

        $AllProducts['products'] = array();
        foreach ($products as $key => $value) {
			if(isset($value['product_id'])){
				array_push($AllProducts['products'], $value);
			}
        }


        echo(json_encode($AllProducts));
    }
    
    public function getProductByBarCode() {
		$this->load->language('product/product');

		$this->load->model('catalog/product');
		$this->load->model('tool/image');
		$this->load->model('saccount/seller');

		if (isset($this->request->get['barcode'])) {
			$barcode = $this->request->get['barcode'];
		} else {
			$barcode = 0;
		}
		$language = (int)$this->request->get['language'];
		$device_id = $this->request->get['device_id'];
		$customer_id = (int)$this->request->get['customer_id'];
		$seller_id = (int)$this->request->get['seller_id'];


		$productDetails = $this->model_catalog_product->getProductesByBarCode($barcode,$language,$device_id, $customer_id,$seller_id);
		if (!$productDetails) {
			$data = array('msg' => 'failed', 'result' => null);
			echo json_encode($data);
			die;
		}
		$productDetails['description'] = html_entity_decode($productDetails['description']);


		$productDetails['options'] = array();

		foreach ($this->model_catalog_product->getProductOptions($productDetails['product_id']) as $option) {
			$product_option_value_data = array();

			foreach ($option['product_option_value'] as $option_value) {
				if (!$option_value['subtract'] || ($option_value['quantity'] > 0)) {
					if ((($this->config->get('config_customer_price') && $this->customer->isLogged()) || !$this->config->get('config_customer_price')) && (float)$option_value['price']) {
						$price = $this->currency->format($this->tax->calculate($option_value['price'], $productDetails['tax_class_id'], $this->config->get('config_tax') ? 'P' : false));
					} else {
						$price = false;
					}

					$product_option_value_data[] = array(
						'product_option_value_id' => $option_value['product_option_value_id'],
						'option_value_id'         => $option_value['option_value_id'],
						'name'                    => $option_value['name'],
						'image'                   => $this->model_tool_image->resize($option_value['image'], 50, 50),
						'price'                   => $price,
						'price_prefix'            => $option_value['price_prefix']
					);
				}
			}

			$productDetails['options'][] = array(
				'product_option_id'    => $option['product_option_id'],
				'product_option_value' => $product_option_value_data,
				'option_id'            => $option['option_id'],
				'name'                 => $option['name'],
				'type'                 => $option['type'],
				'value'                => $option['value'],
				'required'             => $option['required']
			);
		}

		if ($productDetails['minimum']) {
			$productDetails['minimum'] = $productDetails['minimum'];
		} else {
			$productDetails['minimum'] = 1;
		}


		$productImage =  $productDetails['image'];
		$productImages = array();
		$productImages = $this->model_catalog_product->getProductImages($productDetails['product_id']);

		foreach($productImages as $key => $value)
		{
			$productImages[$key]['image'] = $this->model_tool_image->resize($productImages[$key]['image'], 470, 350);
		}

		$productImage = $this->model_tool_image->resize($productImage, 500, 350);

		$productDetails['image'] = array();
		array_push($productDetails['image'],$productImage);
		if(count($productImages)>0){
			for($i=0;$i<=count($productImages)-1;$i++){
				array_push($productDetails['image'],$productImages[$i]['image']);
			}
		}
		$productDetails['seller'] = $this->model_saccount_seller->getSeller($seller_id);
		$data = array('msg' => 'success', 'result' => $productDetails);
		echo json_encode($data);
	}
	
}