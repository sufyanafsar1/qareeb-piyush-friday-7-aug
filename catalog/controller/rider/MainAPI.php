<?php
class ControllerRiderMainAPI extends Controller {
	private $error = array();

	public function index() {
		$this->load->language('account/register');
      //  $this->language->load('account/success');
		$this->load->model('rider/dal');

			 $inputData  = $this->request->post;
			
			 
			
			$customer_id = $this->model_rider_dal->addRider($inputData);
		
			
			
			//$responceData['message'] = 'success';
			$responceData['userID'] = $customer_id;
		
		print_r(json_encode($responceData));
	}

	public function login()
	{
		$this->load->model('rider/dal');
		$inputData  = $this->request->request;

		$responceData = $this->model_rider_dal->loginRider($inputData);
		print_r(json_encode($responceData));
	}
	
	public function login1()
	{
		echo "<pre>";
		print_r($this->request->request);
		exit;
		$this->load->model('rider/dal');
		$inputData  = $this->request->post;

		$responceData = $this->model_rider_dal->loginRider($inputData);
		print_r(json_encode($responceData));
	}

	public function logout()
	{
		$this->load->model('rider/dal');
		$inputData  = $this->request->get;

		$responceData = $this->model_rider_dal->logout($inputData);
		print_r(json_encode($responceData));
	}

	public function forgotPassword(){
		$this->load->model('rider/dal');
		$inputData  = $this->request->get;
		$responceData = $this->model_rider_dal->forgotPassword($inputData);
		print_r(json_encode($responceData));
	}

	public function changePassword()
	{
		$this->load->model('rider/dal');
		$inputData  = $this->request->post;
		$responceData = $this->model_rider_dal->changePassword($inputData);
		print_r(json_encode($responceData));
	}
    
    public function pTest()
	{
	   $this->load->model('rider/dal');
	  	$responceData = $this->model_rider_dal->GetDevice_id(1);
		print_r(json_encode($responceData));
	   }
	
    public function pNotification()
	{
	   $this->load->model('rider/dal');
	  	$responceData = $this->model_rider_dal->pNotification();
		print_r(json_encode($responceData));
	   }



	
    public function GetRiderByID()
	{
		$this->load->model('rider/dal');
		 $inputData  = $this->request->post;
		$responceData = $this->model_rider_dal->GetRiderBy_ID($inputData);
		print_r(json_encode($responceData));
	}

	
	public function updateRider()
	{
		$this->load->model('rider/dal');
		 $inputData  = $this->request->post;
		$responceData = $this->model_rider_dal->updateRider($inputData);
		print_r(json_encode($responceData));
	}
    
    public function GetRiderByEmail()
	{
		$this->load->model('rider/dal');
		 $inputData  = $this->request->post;
		$responceData = $this->model_rider_dal->GetRiderByEmail($inputData);
		print_r(json_encode($responceData));
	}
	


   public function AddRiderCurrentLocation()
	{
		$this->load->model('rider/dal');
		 $inputData  = $this->request->post;
		$responceData = $this->model_rider_dal->AddRiderCurrentLocation($inputData);
		print_r(json_encode($responceData));
	}
    
    public function GetRiderLastLocation()
	{
		$this->load->model('rider/dal');
		 $inputData  = $this->request->post;
  
		$responceData = $this->model_rider_dal->GetRiderLastLocation($inputData);
        
		print_r(json_encode($responceData));
	}
    
    public function getUsersByStatus()
	{
		$this->load->model('rider/dal');
		 $inputData  = $this->request->get;
  
		$responceData = $this->model_rider_dal->GetUsersByStatus($inputData);
        
		print_r(json_encode($responceData));
	}

	public function ChangeUserActiveStatus()
	{
		$this->load->model('rider/dal');
		$inputData  = $this->request->get;

		$responceData = $this->model_rider_dal->ChangeUserActiveStatus($inputData);

		print_r(json_encode($responceData));
	}

	public function ChangeUserOnlineStatus()
	{
		$this->load->model('rider/dal');
		$inputData  = $this->request->get;

		$responceData = $this->model_rider_dal->ChangeUserOnlineStatus($inputData);

		print_r(json_encode($responceData));
	}
    
    public function getRidersByGivenTimeLimit()
    {
        $this->load->model('rider/dal');
        $inputData  = $this->request->post;
       
        $responceData = $this->model_rider_dal->getRidersByGivenTimeLimitV2($inputData);
     
        print_r(json_encode($responceData));
    }
    
    public function updateRiderAvailability()    
    {
        $this->load->model('rider/dal');
        $inputData  = $this->request->post;
        $responceData = $this->model_rider_dal->updateRiderAvailability($inputData);
        echo $responceData;
    }

    
    public function getRiderAvailiblityByRiderId()
    {
        $this->load->model('rider/dal');
        $inputData  = $this->request->get;
        $responceData = $this->model_rider_dal->getRiderAvailiblityByRiderId($inputData);
        print_r(json_encode($responceData));
    }



	public function addRiderArea()
	{
		$this->load->model('rider/dal');
		$inputData  = $this->request->get;
		$responceData = $this->model_rider_dal->addRiderArea($inputData);

		echo $responceData;
	}
	public function getRiderAreasById()
	{
		$this->load->model('rider/dal');
		$inputData  = $this->request->get;
		$responceData = $this->model_rider_dal->getRiderAreasById($inputData);
		print_r(json_encode($responceData));
	}
      public function addArea()
    {
        $this->load->model('rider/dal');
        $inputData  = $this->request->post;
        $responceData = $this->model_rider_dal->addArea($inputData);
       echo $responceData;
    }
    public function addAreaV2()
    {
        $this->load->model('rider/dal');
        $inputData  = $this->request->get;
        $responceData = $this->model_rider_dal->addAreaV2($inputData);
       echo $responceData;
    }
       public function getAreas()
    {
        $this->load->model('rider/dal');        
        $responceData = $this->model_rider_dal->getAreas();
        print_r(json_encode($responceData));
    }
    
    
    public function assignOrder(){
        $this->load->model('rider/dal');
        $inputData  = $this->request->post;
        $responceData = $this->model_rider_dal->assignOrder($inputData);
        print_r(json_encode($responceData));
    }
	
	public function removeOrder(){
        $this->load->model('rider/dal');
        $inputData  = $this->request->post;
        $responceData = $this->model_rider_dal->removeOrder($inputData);
        print_r(json_encode($responceData));
    }
    
    
    public function assignOrderV2(){
        $this->load->model('rider/dal');
        $inputData  = $this->request->post;
        $responceData = $this->model_rider_dal->assignOrderV2($inputData);
        print_r(json_encode($responceData));
    }
    

    
        public function getPendingMainOrders()
	{
		$this->load->model('rider/dal');
  
		$responceData = $this->model_rider_dal->getPendingMainOrders();
        
		print_r(json_encode($responceData));
	}
    
          public function getMainOrderDetailsByOrderID()
	{
		$this->load->model('rider/dal');
		 $inputData  = $this->request->get;
		$responceData = $this->model_rider_dal->getMainOrderDetailsByOrderID($inputData);
		print_r(json_encode($responceData));
	}
    
  public function AcceptOrder()
	{
	
        $this->load->model('rider/dal');
		 $inputData  = $this->request->get;
		$responceData = $this->model_rider_dal->AcceptOrder($inputData);
		print_r(json_encode($responceData));
	}
    
      public function RejectOrder()
	{
$this->load->model('rider/dal');
		 $inputData  = $this->request->get;
		$responceData = $this->model_rider_dal->RejectOrder($inputData);
		print_r(json_encode($responceData));
	}
     public function OrderDelivered()
	{
$this->load->model('rider/dal');
		 $inputData  = $this->request->get;
		$responceData = $this->model_rider_dal->OrderDelivered($inputData);
		print_r(json_encode($responceData));
	}
    
       public function OrderDeliveredToRider()
	{
$this->load->model('rider/dal');
		 $inputData  = $this->request->get;
		$responceData = $this->model_rider_dal->OrderDeliveredToRider($inputData);
		print_r(json_encode($responceData));
	}

	public function RiderOrderStatus()
	{
		$this->load->model('rider/dal');
		$inputData  = $this->request->get;
		$responceData = $this->model_rider_dal->RiderOrderStatus($inputData);
		print_r(json_encode($responceData));
	}
    
    public function TrackOrder()
	{
$this->load->model('rider/dal');
		 $inputData  = $this->request->get;
		$responceData = $this->model_rider_dal->TrackOrder($inputData);
		print_r(json_encode($responceData));
	}
    
    
    
			    public function GetUserOrderByStatus()
				{
			$this->load->model('rider/dal');
					 $inputData  = $this->request->get;
					$responceData = $this->model_rider_dal->GetUserOrderByStatus($inputData);
					print_r(json_encode($responceData));
				}
		     public function UpdateProductBarCode()
			{
		$this->load->model('rider/dal');
				 $inputData  = $this->request->get;
				$responceData = $this->model_rider_dal->UpdateProductBarCode($inputData);
				print_r(json_encode($responceData));
			}
    
    
		         public function GetAssignerOrderByStatus()
			{
		         $this->load->model('rider/dal');
				 $inputData  = $this->request->get;
				$responceData = $this->model_rider_dal->GetAssignerOrderByStatus($inputData);
				print_r(json_encode($responceData));
			}
    
    public function deleteRider($data)
    {
        $this->load->model('rider/dal');
		 $inputData  = $this->request->get;
		$responceData = $this->model_rider_dal->deleteRider($inputData);
		print_r(json_encode($responceData));
    }
    
		     public function getOrdersHistory()
		    {
		        $this->load->model('rider/dal');
				 $inputData  = $this->request->get;
				$responceData = $this->model_rider_dal->getOrders($inputData);
				print_r(json_encode($responceData));
		    }
			public function getOrdersHistorytest()
		    {
		        $this->load->model('rider/dal');
				 $inputData  = $this->request->get;
				$responceData = $this->model_rider_dal->getOrderstest($inputData);
				echo"<pre>";
				print_r($responceData);
				exit;
				print_r(json_encode($responceData));
		    }
		     public function getOrders()
		    {
		        $this->load->model('rider/dal');
				 $inputData  = $this->request->get;
				$responceData = $this->model_rider_dal->getOrders($inputData);
				print_r(json_encode($responceData));
		    }
		
		public function getAllOrders()
		{
			$this->load->model('rider/dal');
			$inputData  = $this->request->get;
			$responceData = $this->model_rider_dal->getAllOrders($inputData);
			print_r(json_encode($responceData));
		}
		public function getAllOrderstest()
		{
			$this->load->model('rider/dal');
			$inputData  = $this->request->get;
			$responceData = $this->model_rider_dal->getAllOrderstest($inputData);
			echo'<pre>';
			print_r($responceData);
			exit;
			print_r(json_encode($responceData));
		}
		public function getOrderProducts()
		{
			$this->load->model('rider/dal');
			$inputData  = $this->request->get;
			$responceData = $this->model_rider_dal->getOrderProducts($inputData);
			print_r(json_encode($responceData));
		}
		
		public function getOrderProductsv2()
		{
			$this->load->model('rider/dal');
			$inputData  = $this->request->get;
			$responceData = $this->model_rider_dal->getOrderProductsv2new($inputData);
			print_r(json_encode($responceData));
		}
		
			public function getOrderProduct()
			{
				$this->load->model('rider/dal');
				$inputData  = $this->request->get;
				$responceData = $this->model_rider_dal->getOrderProduct($inputData);
				print_r(json_encode($responceData));
			}
			public function updateProductStatus()
			{
				$this->load->model('rider/dal');
				$inputData  = $this->request->get;
				$responceData = $this->model_rider_dal->updateProductStatus($inputData);
				print_r(json_encode($responceData));
			}


	public function updatemultiProductStatus()
	{
		$json = file_get_contents('php://input');		
		$data = json_decode($json);
		// echo'<pre>';
		// print_r($data);
		// exit;
		$this->load->model('rider/dal');
		$inputData  = $data;
		$responceData = $this->model_rider_dal->updatemultiProductStatus($inputData);
		print_r(json_encode($responceData));
	
	}
     public function addSignature()
    {
        $this->load->model('rider/dal');
		 $inputData  = $this->request->post;
		$responceData = $this->model_rider_dal->addSignature($inputData);
		print_r(json_encode($responceData));
    }
 public function addSignaturetest()
    {
        $this->load->model('rider/dal');
		 $inputData  = $this->request->post;
		$responceData = $this->model_rider_dal->addSignaturetest($inputData);
		print_r(json_encode($responceData));
    }

    public function addParcel()
    {
        $this->load->model('rider/dal');
		 $inputData  = $this->request->post;
		$responceData = $this->model_rider_dal->addParcel($inputData);
		print_r(json_encode($responceData));
    }

	public function startShopping()
	{
		$this->load->model('rider/dal');
		$inputData  = $this->request->post;
		$responceData = $this->model_rider_dal->startShopping($inputData);
		print_r(json_encode($responceData));
	}

    public function outForDelivery()
    {
        $this->load->model('rider/dal');
        $inputData  = $this->request->post;
        $responceData = $this->model_rider_dal->outForDelivery($inputData);
        print_r(json_encode($responceData));
    }
    
	public function validate() {
		if ((utf8_strlen(trim($this->request->post['firstname'])) < 1) || (utf8_strlen(trim($this->request->post['firstname'])) > 32)) {
			$this->error['firstname'] = $this->language->get('error_firstname');
		}

		if ((utf8_strlen(trim($this->request->post['lastname'])) < 1) || (utf8_strlen(trim($this->request->post['lastname'])) > 32)) {
			$this->error['lastname'] = $this->language->get('error_lastname');
		}

		if ((utf8_strlen($this->request->post['email']) > 96) || !preg_match('/^[^\@]+@.*.[a-z]{2,15}$/i', $this->request->post['email'])) {
			$this->error['email'] = $this->language->get('error_email');
		}

		if ($this->model_rider_dal->getTotalCustomersByEmail($this->request->post['email'])) {
			$this->error['warning'] = $this->language->get('error_exists');
		}		


		return !$this->error;
	}
	
	private function validateRegister() {
		if ((utf8_strlen(trim($this->request->post['firstname'])) < 1) || (utf8_strlen(trim($this->request->post['firstname'])) > 32)) {
			$this->error['firstname'] = $this->language->get('error_firstname');
		}

	if ((utf8_strlen(trim($this->request->post['lastname'])) < 1) || (utf8_strlen(trim($this->request->post['lastname'])) > 32)) {
			$this->error['lastname'] = $this->language->get('error_lastname');
		}

		if ((utf8_strlen($this->request->post['email']) > 96) || !preg_match('/^[^\@]+@.*.[a-z]{2,15}$/i', $this->request->post['email'])) {
			$this->error['email'] = $this->language->get('error_email');
		}

		if ($this->model_rider_dal->getTotalCustomersByEmail($this->request->post['email'])) {
			$this->error['warning'] = $this->language->get('error_exists');
		}
        

		if ((utf8_strlen($this->request->post['telephone']) < 3) || (utf8_strlen($this->request->post['telephone']) > 32)) {
			$this->error['telephone'] = $this->language->get('error_telephone');
		}
        if ($this->model_rider_dal->getTotalCustomersByTelephone($this->request->post['telephone'])) {
			$this->error['warning_mobile'] = $this->language->get('error_exists_mobile');
		}

		if ((utf8_strlen(trim($this->request->post['address_1'])) < 3) || (utf8_strlen(trim($this->request->post['address_1'])) > 128)) {
			$this->error['address_1'] = $this->language->get('error_address_1');
		}
      
		return !$this->error;
	}


	public function addSubstitution(){
		$this->load->model('rider/dal');
		$inputData  = $this->request->post;
		$responceData = $this->model_rider_dal->addSubstitution($inputData);
		print_r(json_encode($responceData));
	}
	public function askForSubstitution()
    {
        $this->load->model('rider/dal');
        $inputData  = $this->request->post;
        
        $responceData = $this->model_rider_dal->askForSubstitution($inputData);
        print_r(json_encode($responceData));
    }

    public function substitutionNotification()
    {
        $this->load->model('rider/dal');
        $inputData  = $this->request->post;
        $responceData = $this->model_rider_dal->substitutionNotification($inputData);
        print_r(json_encode($responceData));
    }

    public function orderPayment()
    {
        $this->load->model('rider/dal');
        $inputData  = $this->request->post;
        $responceData = $this->model_rider_dal->orderPayment($inputData);
        print_r(json_encode($responceData));
    }

    public function addProductInvoice()
    {
       
        $this->load->model('rider/dal');
        $inputData  = $this->request->post;
        $responceData = $this->model_rider_dal->addProductInvoice($inputData);
        print_r(json_encode($responceData));
    }
    
    public function checkProductBarCode()
	{
		$this->load->model('rider/dal');
		$inputData  = $this->request->post;
		$responceData = $this->model_rider_dal->checkProductBarCode($inputData);
		print_r(json_encode($responceData));
	}

	public function  updateRiderMobileId(){
		$this->load->model('rider/dal');
		$userMobileData  = $this->request->post;
		$this->model_rider_dal->updateRiderMobileId($userMobileData);
		$data['message'] = 'success';
		print_r(json_encode($data));

	}
}
