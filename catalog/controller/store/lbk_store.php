<?php

class ControllerStoreStore extends Controller {

    public function index() {

        $this->load->model('catalog/product');
        $this->load->model('catalog/category');
        $this->load->model('tool/image');
        $this->load->language('product/product');
        $data['button_cart'] = $this->language->get('button_cart');

        $this->document->addStyle('catalog/view/theme/' . $this->config->get('config_template') . '/css/jquery.bxslider.min.css');
        $this->document->addScript('catalog/view/theme/' . $this->config->get('config_template') . '/js/jquery.bxslider.min.js');


        if (isset($this->request->get['seller_id'])) {
            $seller_id = $this->request->get['seller_id'];
        } else {
            $seller_id = $_SESSION['storeID'];
        }
        if (empty($_SESSION['storeID']) || $_SESSION['storeID'] == '') {
            $_SESSION['storeID'] = $_REQUEST['seller_id'];
        }

        if(!empty($_REQUEST['seller_id'])) {
            if ($_SESSION['storeID'] != $_REQUEST['seller_id']) {

                $storeStatusCheck = 'changed';
                $_SESSION['storeID'] = $_REQUEST['seller_id'];

                $this->language->load('module/cart');
                $this->cart->clear();
            } else if ($_SESSION['storeID'] == $_REQUEST['seller_id']) {
                $storeStatusCheck = 'same';
            }
            $_SESSION['storeStatusCheck'] = $storeStatusCheck;
        }
        //Unset and Set seller in cookies      
         unsetCareebCookie('seller');
         setCareebCookie('seller', $seller_id,false);       
         
         
        $this->load->model('store/store');
        $data['sellerData'] = $this->model_store_store->getSellerByID($seller_id);


        $this->document->setTitle($this->config->get('config_meta_title'));
        $this->document->setDescription($this->config->get('config_meta_description'));
        $this->document->setKeywords($this->config->get('config_meta_keyword'));

        if (isset($this->request->get['route'])) {
            $this->document->addLink(HTTP_SERVER, 'canonical');
        }


        if (isset($this->request->post['city_id']) && isset($this->request->post['area_id'])) {
            unset($this->session->data['area']);
            $this->session->data['area'] = $this->request->post;
        }

        if (isset($this->session->data['user_location'])) {

            $user_location = $this->session->data['user_location'];
           $radiusKm = RADIUS; //5;

            $lat = $user_location['u_lat'];
            $lng = $user_location['u_lng'];
            $categoryResults = $this->model_catalog_category->getSellerCategoriesRadiusWise(0, $seller_id, $radiusKm, $lat, $lng);
        } else {
            $area_id = 0;
            $area_id = $this->session->data['area']['area_id'];
            $categoryResults = $this->model_catalog_category->getSellerCategoriesAreaWise(0, $seller_id, $area_id);
        }



        $results = $this->model_catalog_product->getSellerProductsV2($seller_id, 50);

        $data['products'] = array();
        $data['categories'] = array();
        foreach ($results as $result) {

            if ($result) {
                if ($result['image'] && file_exists(DIR_IMAGE . $result['image'])) {
                    $image = $this->model_tool_image->resize($result['image'], $this->config->get('config_image_product_width'), $this->config->get('config_image_product_height'));
                } else {
                    $image = $this->model_tool_image->resize('no_image.png', $this->config->get('config_image_product_width'), $this->config->get('config_image_product_height'));
                }
                if (isset($result['special'])) {
                    $special_price = $this->currency->format($this->tax->calculate($result['special'], $result['tax_class_id'], $this->config->get('config_tax')));
                } else {
                    $special_price = false;
                }

                $data['products'][] = array(
                    'product_id' => $result['product_id'],
                    'name' => $result['name'],
                    'date_added' => $result['date_added'],
                    'model' => $result['model'],
                    'price' => $this->currency->format($this->tax->calculate($result['price'], $result['tax_class_id'], $this->config->get('config_tax'))),
                    'special' => $special_price,
                    'sku' => $result['sku'],
                    'image' => $image,
                    'quantity' => $result['squantity'],
                    'minimum' => $result['minimum'] > 0 ? $result['minimum'] : 1,
                );
            }
        }

        foreach ($categoryResults as $result) {


            if ($result['image'] && file_exists(DIR_IMAGE . $result['image'])) {
                $image = $this->model_tool_image->resize($result['image'], 300, 165);
            } else {
                $image = $this->model_tool_image->resize('no_image.png', 300, 165);
            }
            // 'href'        => $this->url->link('product/category', 'path=' . $result['category_id'], '&seller_id='  . $seller_id),

            $data['categories'][] = array(
                'category_id' => $result['category_id'],
                'name' => $result['name'],
                'description' => $result['description'],
                'image' => $image,
                'href' => "index.php?route=product/category&path=" . $result['category_id'] . "&seller_id=" . $seller_id,
            );
        }
        $data['product_data'] = array();
        $data['cartData'] = $this->cart->getProducts();
        if (!count($data['cartData'])) {
            $data['cartData'] = array();
        }else{
            foreach($data['cartData'] as $product){
                if($product['seller_id'] != $_SESSION['storeID']){
                    $this->cart->clear();
                    break;
                    $data['cartData'] = array();
                }
            }

        }
        $data['product_data'] = array(
            'products' => $data['products'],
            'mainproduct' => $mainproduct,
            'cart_data' => $data['cartData']
        );


        $data['search'] = $this->load->controller('common/search');
        $data['seller_id'] = $seller_id;
        $data['column_left'] = $this->load->controller('common/column_left');
        $data['column_right'] = $this->load->controller('common/column_right');
        $data['content_top'] = $this->load->controller('common/content_top');
        $data['content_bottom'] = $this->load->controller('common/content_bottom');
        $data['footer'] = $this->load->controller('common/footer');
        $data['header'] = $this->load->controller('common/header');

        $getTime = $this->sellertimesettings->getTimebhutta($seller_id);

        $data['getTime'] = $getTime;
        
        if ($getTime['storeTimeStatus'] != 1) {
            $data['next_delivery'] = $this->language->get("text_no_delivery_slot");
        } else {
            $data['next_delivery'] = $getTime['storeTimeStatusText'];
        }


        if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/template/store/store.tpl')) {
            $this->response->setOutput($this->load->view($this->config->get('config_template') . '/template/store/store.tpl', $data));
        }
    }

}

?>