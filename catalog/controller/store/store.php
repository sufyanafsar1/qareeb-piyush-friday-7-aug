<?php

class ControllerStoreStore extends Controller
{

	public function index()
	{
		
		$this->load->model('catalog/product');
		$this->load->model('catalog/category');
		$this->load->model('tool/image');
		$this->load->language('product/product');

		$data['button_cart']            = $this->language->get('button_cart');
		$data['text_categories']        = $this->language->get('text_categories');
		$data['text_products']          = $this->language->get('text_products');
		$data['text_special_products']  = $this->language->get('text_special_products');
		$data['text_featured_products'] = $this->language->get('text_featured_products');
		
		$this->document->addStyle('catalog/view/theme/' . $this->config->get('config_template') . '/css/jquery.bxslider.min.css');
		$this->document->addScript('catalog/view/theme/' . $this->config->get('config_template') . '/js/jquery.bxslider.min.js');


		if (isset($this->request->get['seller_id'])) {
			$seller_id = $this->request->get['seller_id'];
		} else {
			$seller_id = $_SESSION['storeID'];
		}
		if (empty($_SESSION['storeID']) || $_SESSION['storeID'] == '') {
			$_SESSION['storeID'] = $_REQUEST['seller_id'];
		}

		if (!empty($_REQUEST['seller_id'])) {
			if ($_SESSION['storeID'] != $_REQUEST['seller_id']) {

				// $storeStatusCheck = 'changed';
				$storeStatusCheck = 'same';
				$_SESSION['storeID'] = $_REQUEST['seller_id'];

				$this->language->load('module/cart');
				//$this->cart->clear();
			} else if ($_SESSION['storeID'] == $_REQUEST['seller_id']) {
				$storeStatusCheck = 'same';
			}
			$_SESSION['storeStatusCheck'] = $storeStatusCheck;
		}
		//Unset and Set seller in cookies      
		unsetCareebCookie('seller');
		setCareebCookie('seller', $seller_id, false);


		$this->load->model('store/store');
		$data['sellerData'] = $this->model_store_store->getSellerByID($seller_id);
		

		$this->document->setTitle($this->config->get('config_meta_title'));
		$this->document->setDescription($this->config->get('config_meta_description'));
		$this->document->setKeywords($this->config->get('config_meta_keyword'));

		if (isset($this->request->get['route'])) {
			$this->document->addLink(HTTP_SERVER, 'canonical');
		}

		if (isset($this->request->post['city_id']) && isset($this->request->post['area_id'])) {
			unset($this->session->data['area']);
			$this->session->data['area'] = $this->request->post;
		}

		if (isset($this->session->data['user_location'])) {

			$user_location = $this->session->data['user_location'];
			$radiusKm = RADIUS; //5;
			$radiusKm = 10;

			$lat = $user_location['u_lat'];
			$lng = $user_location['u_lng'];

			$categoryResults = $this->model_catalog_category->getSellerCategoriesRadiusWise(0, $seller_id, $radiusKm, $lat, $lng);
		} else {
			if (isset($_REQUEST['seller_id']) && ($_REQUEST['seller_id'] > 0)) {

				if (!isset($this->session->data['area']['area_id'])) {

					$dataAreaS = $this->model_catalog_category->getAreaS($_REQUEST['seller_id']);

					$this->session->data['area']['area_id'] = $dataAreaS['area_id'];
					$this->session->data['area']['city_id'] = $dataAreaS['city_id'];
				}

				$area_id = $this->session->data['area']['area_id'];
			} else {
				$area_id = 0;
			}
			$categoryResults = $this->model_catalog_category->getSellerCategoriesAreaWise(0, $seller_id, $area_id);
		}


		$language_id = (int) $this->config->get('config_language_id');
		$limit_data['start'] = 0;
		$limit_data['limit'] = 20;

		$results = $this->model_catalog_product->getSellerProductsV3($seller_id, $limit_data, $language_id);
		
		$data['products'] = array();
		$data['categories'] = array();
		foreach ($results as $result) {
			$special_int = 0;
			$price_int = 0;
			if ($result) {

				if ($result['image']) {
					$image = $this->model_tool_image->resize($result['image'], $this->config->get('config_image_product_width'), $this->config->get('config_image_product_height'));
				} else {
					$image = $this->model_tool_image->resize('placeholder.png', $this->config->get('config_image_product_width'), $this->config->get('config_image_product_height'));
				}

				if (($this->config->get('config_customer_price') && $this->customer->isLogged()) || !$this->config->get('config_customer_price')) {
					$price_int = $result['price'];
					$price = $this->currency->format($this->tax->calculate($result['price'], $result['tax_class_id'], $this->config->get('config_tax')));
				} else {
					$price = false;
				}

				if ((float) $result['special']) {
					$special_int = $result['special'];
					$special = $this->currency->format($this->tax->calculate($result['special'], $result['tax_class_id'], $this->config->get('config_tax')));
				} else {
					$special = false;
				}

				if ($this->config->get('config_tax')) {
					$tax = $this->currency->format((float) $result['special'] ? $result['special'] : $result['price']);
				} else {
					$tax = false;
				}

				if ($this->config->get('config_review_status')) {
					$rating = (int) $result['rating'];
				} else {
					$rating = false;
				}

				$options = array();

				if ($result['option_available'] == 1) {

					foreach ($this->model_catalog_product->getProductOptions($result['product_id']) as $option) {
						$product_option_value_data = array();

						foreach ($option['product_option_value'] as $option_value) {
							if (!$option_value['subtract'] || ($option_value['quantity'] > 0)) {
								if ((($this->config->get('config_customer_price') && $this->customer->isLogged()) || !$this->config->get('config_customer_price')) && (float) $option_value['price']) {
									//$price = $this->currency->format($this->tax->calculate($option_value['price'], $result['tax_class_id'], $this->config->get('config_tax') ? 'P' : false));
								} else {
									//$price = false;
								}

								if (isset($option_value['image'])) {
									$option_img = $this->model_tool_image->resize($option_value['image'], 50, 50);
								} else {
									$option_img = '';
								}

								$product_option_value_data[] = array(
									'product_option_value_id' => $option_value['product_option_value_id'],
									'option_value_id'         => $option_value['option_value_id'],
									'name'                    => $option_value['name'],
									'image'                   => $option_img,
									'price'                   => $price,
									'price_prefix'            => $option_value['price_prefix']
								);
							}
						}

						$options[] = array(
							'product_option_id'    => $option['product_option_id'],
							'product_option_value' => $product_option_value_data,
							'option_id'            => $option['option_id'],
							'name'                 => $option['name'],
							'type'                 => $option['type'],
							'value'                => $option['value'],
							'required'             => $option['required']
						);
					}
				}

				//discount 
				$discount = 0;
				if ($special_int != 0 && $special_int != '') {
					$discount = round((($price_int - $special_int) / $price_int) * 100, 0);
					if ($discount > 100 && $discount < 0 && $discount != 0) {
						$discount = 0;
					}
				}

				$data['products'][] = array(
					'product_id'  => $result['product_id'],
					'thumb'       => $image,
					'name'        => $result['name'],
					'description' => utf8_substr(strip_tags(html_entity_decode($result['description'], ENT_QUOTES, 'UTF-8')), 0, $this->config->get('config_product_description_length')) . '..',
					'price'       => $price,
					'special'     => $special,
					'discount'    => $discount,
					'tax'         => $tax,
					'minimum'     => $result['minimum'] > 0 ? $result['minimum'] : 1,
					'rating'      => $result['rating'],
					//'href'        => $this->url->link('product/product', 'path=' . $this->request->get['path'] . '&product_id=' . $result['product_id'] . $url),
					'options'     => $options,
				);
			}

			/* if ($result) {
                if ($result['image'] && file_exists(DIR_IMAGE . $result['image'])) {
                    $image = $this->model_tool_image->resize($result['image'], $this->config->get('config_image_product_width'), $this->config->get('config_image_product_height'));
                } else {
                    $image = $this->model_tool_image->resize('no_image.png', $this->config->get('config_image_product_width'), $this->config->get('config_image_product_height'));
                }
                if (isset($result['special'])) {
                    $special_price = $this->currency->format($this->tax->calculate($result['special'], $result['tax_class_id'], $this->config->get('config_tax')));
                } else {
                    $special_price = false;
                }

                $data['products'][] = array(
                    'product_id' => $result['product_id'],
                    'name' => $result['name'],
                    'date_added' => $result['date_added'],
                    'model' => $result['model'],
                    'price' => $this->currency->format($this->tax->calculate($result['price'], $result['tax_class_id'], $this->config->get('config_tax'))),
                    'special' => $special_price,
                    'sku' => $result['sku'],
                    'image' => $image,
                    'quantity' => $result['squantity'],
                    'minimum' => $result['minimum'] > 0 ? $result['minimum'] : 1,
                );
            } */
		}



		$limit_data2['start'] = 0;
		$limit_data2['limit'] = 50;
		$results_featured_products = $this->model_catalog_product->getStoreFeatureProductApi($seller_id, $limit_data2, $language_id);
		// if ($this->request->get['test']) {
			// echo '<pre>';
			// print_r($results_featured_products);
			// echo '</pre>';
			// exit;
		// }
		$data['featured_products'] = array();
		foreach ($results_featured_products as $result_featured_product) {
			$special_int = 0;
			$price_int = 0;
			if ($result_featured_product) {

				if ($result_featured_product['image']) {
					$image = $this->model_tool_image->resize($result_featured_product['image'], $this->config->get('config_image_product_width'), $this->config->get('config_image_product_height'));
				} else {
					$image = $this->model_tool_image->resize('placeholder.png', $this->config->get('config_image_product_width'), $this->config->get('config_image_product_height'));
				}

				if (($this->config->get('config_customer_price') && $this->customer->isLogged()) || !$this->config->get('config_customer_price')) {
					$price = $this->currency->format($this->tax->calculate($result_featured_product['price'], $result_featured_product['tax_class_id'], $this->config->get('config_tax')));
					$price_int = $result_featured_product['price'];
				} else {
					$price = false;
				}

				if ((float) $result_featured_product['special']) {
					$special = $this->currency->format($this->tax->calculate($result_featured_product['special'], $result_featured_product['tax_class_id'], $this->config->get('config_tax')));
					$special_int = $result_featured_product['special'];
				} else {
					$special = false;
				}

				if ($this->config->get('config_tax')) {
					$tax = $this->currency->format((float) $result_featured_product['special'] ? $result_featured_product['special'] : $result_featured_product['price']);
				} else {
					$tax = false;
				}

				if ($this->config->get('config_review_status')) {
					$rating = (int) $result_featured_product['rating'];
				} else {
					$rating = false;
				}

				$options = array();

				if ($result_featured_product['option_available'] == 1) {

					foreach ($this->model_catalog_product->getProductOptions($result_featured_product['product_id']) as $option) {
						$product_option_value_data = array();

						foreach ($option['product_option_value'] as $option_value) {
							if (!$option_value['subtract'] || ($option_value['quantity'] > 0)) {
								if ((($this->config->get('config_customer_price') && $this->customer->isLogged()) || !$this->config->get('config_customer_price')) && (float) $option_value['price']) {
									//$price = $this->currency->format($this->tax->calculate($option_value['price'], $result_featured_product['tax_class_id'], $this->config->get('config_tax') ? 'P' : false));
								} else {
									//$price = false;
								}

								if (isset($option_value['image'])) {
									$option_img = $this->model_tool_image->resize($option_value['image'], 50, 50);
								} else {
									$option_img = '';
								}

								$product_option_value_data[] = array(
									'product_option_value_id' => $option_value['product_option_value_id'],
									'option_value_id'         => $option_value['option_value_id'],
									'name'                    => $option_value['name'],
									'image'                   => $option_img,
									'price'                   => $price,
									'price_prefix'            => $option_value['price_prefix']
								);
							}
						}

						$options[] = array(
							'product_option_id'    => $option['product_option_id'],
							'product_option_value' => $product_option_value_data,
							'option_id'            => $option['option_id'],
							'name'                 => $option['name'],
							'type'                 => $option['type'],
							'value'                => $option['value'],
							'required'             => $option['required']
						);
					}
				}
				//discount 
				$discount = 0;
				if ($special_int != 0 && $special_int != '') {
					$discount = round((($price_int - $special_int) / $price_int) * 100, 0);
					if ($discount > 100 && $discount < 0 && $discount != 0) {
						$discount = 0;
					}
				}
				$data['featured_products'][] = array(
					'product_id'  => $result_featured_product['product_id'],
					'thumb'       => $image,
					'name'        => $result_featured_product['name'],
					'description' => utf8_substr(strip_tags(html_entity_decode($result_featured_product['description'], ENT_QUOTES, 'UTF-8')), 0, $this->config->get('config_product_description_length')) . '..',
					'price'       => $price,
					'special'     => $special,
					'discount'    => $discount,
					'tax'         => $tax,
					'minimum'     => $result_featured_product['minimum'] > 0 ? $result_featured_product['minimum'] : 1,
					'rating'      => $result_featured_product['rating'],
					//'href'        => $this->url->link('product/product', 'path=' . $this->request->get['path'] . '&product_id=' . $result['product_id'] . $url),
					'options'     => $options,
				);
			}
		}

		// echo"<pre>";
		// print_r($data['featured_products']);
		// exit;

		foreach ($categoryResults as $result) {

			$categoryProduct = $this->model_catalog_category->getSellerCategoriesProducts($seller_id, $result['category_id']);

			if ($categoryProduct > 0) {

				if ($result['image'] && file_exists(DIR_IMAGE . $result['image'])) {
					$image = $this->model_tool_image->resize($result['image'], 300, 165);
				} else {
					//$image = $this->model_tool_image->resize('no_image.png', 300, 165);
					$image = $this->model_tool_image->resize('placeholder.png', 300, 165);
				}
				// 'href'        => $this->url->link('product/category', 'path=' . $result['category_id'], '&seller_id='  . $seller_id),

				$data['categories'][] = array(
					'category_id' => $result['category_id'],
					'name' => $result['name'],
					'description' => $result['description'],
					'image' => $image,
					'href' => "index.php?route=product/category&path=" . $result['category_id'] . "&seller_id=" . $seller_id,
				);
			}
		}
		$data['product_data'] = array();
		$data['cartData'] = $this->cart->getProducts();
		if (!count($data['cartData'])) {
			$data['cartData'] = array();
		} else {
			foreach ($data['cartData'] as $product) {
				if ($product['seller_id'] != $_SESSION['storeID']) {
					//$this->cart->clear();
					break;
					//$data['cartData'] = array();
				}
			}
		}
		/* $data['product_data'] = array(
            'products' => $data['products'],
            'mainproduct' => $mainproduct,
            'cart_data' => $data['cartData']
        ); */


		$data['product_data'] = array(
			'products' => $data['products'],
			'cart_data' => $data['cartData']
		);
		
		$data['search'] = $this->load->controller('common/search');
		$data['seller_id'] = $seller_id;
		//$data['column_left'] = $this->load->controller('common/column_left');
		//$data['column_right'] = $this->load->controller('common/column_right');
		//$data['content_top'] = $this->load->controller('common/content_top');
		//$data['content_bottom'] = $this->load->controller('common/content_bottom');
		$data['footer'] = $this->load->controller('common/footer');
		$data['header'] = $this->load->controller('common/header');
	
		$getTime = $this->sellertimesettings->getTimebhutta($seller_id);

		$data['getTime'] = $getTime;

		if ($getTime['storeTimeStatus'] != 1) {
			$data['next_delivery'] = $this->language->get("text_no_delivery_slot");
		} else {
			$data['next_delivery'] = $getTime['storeTimeStatusText'];
		}
		$data['language_id'] = $language_id;

		// if ($this->request->get['test']) {
		// 	echo '<pre>';
		// 	print_r($data);
		// 	echo '</pre>';
		// 	exit;
		// }
		
		if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/template/store/store.tpl')) {
			$this->response->setOutput($this->load->view($this->config->get('config_template') . '/template/store/store.tpl', $data));
		}
	}


	public function specialProduct()
	{

		$this->load->model('catalog/product');
		$this->load->model('tool/image');
		$this->load->language('product/product');


		$seller_id = $this->request->get['seller_id'];


		$language_id = $this->request->get['language_id'];
		$limit_data['start'] = 0;
		$limit_data['limit'] = 10;

		$results = $this->model_catalog_product->getStoreSpecialProductApi($seller_id, $limit_data, $language_id);

		$data['products'] = array();
		foreach ($results as $result) {

			if ($result) {

				if ($result['image']) {
					$image = $this->model_tool_image->resize($result['image'], $this->config->get('config_image_product_width'), $this->config->get('config_image_product_height'));
				} else {
					$image = $this->model_tool_image->resize('placeholder.png', $this->config->get('config_image_product_width'), $this->config->get('config_image_product_height'));
				}

				if (($this->config->get('config_customer_price') && $this->customer->isLogged()) || !$this->config->get('config_customer_price')) {
					$price = $this->currency->format($this->tax->calculate($result['price'], $result['tax_class_id'], $this->config->get('config_tax')));
				} else {
					$price = false;
				}

				if ((float) $result['special']) {
					$special = $this->currency->format($this->tax->calculate($result['special'], $result['tax_class_id'], $this->config->get('config_tax')));
				} else {
					$special = false;
				}

				if ($this->config->get('config_tax')) {
					$tax = $this->currency->format((float) $result['special'] ? $result['special'] : $result['price']);
				} else {
					$tax = false;
				}

				if ($this->config->get('config_review_status')) {
					$rating = (int) $result['rating'];
				} else {
					$rating = false;
				}

				$options = array();

				if ($result['option_available'] == 1) {

					foreach ($this->model_catalog_product->getProductOptions($result['product_id']) as $option) {
						$product_option_value_data = array();

						foreach ($option['product_option_value'] as $option_value) {
							if (!$option_value['subtract'] || ($option_value['quantity'] > 0)) {
								if ((($this->config->get('config_customer_price') && $this->customer->isLogged()) || !$this->config->get('config_customer_price')) && (float) $option_value['price']) {
									$price = $this->currency->format($this->tax->calculate($option_value['price'], $result['tax_class_id'], $this->config->get('config_tax') ? 'P' : false));
								} else {
									$price = $this->currency->format($this->tax->calculate(0.00, $result['tax_class_id'], $this->config->get('config_tax')));;
								}

								if (isset($option_value['image'])) {
									$option_img = $this->model_tool_image->resize($option_value['image'], 50, 50);
								} else {
									$option_img = '';
								}

								$product_option_value_data[] = array(
									'product_option_value_id' => $option_value['product_option_value_id'],
									'option_value_id'         => $option_value['option_value_id'],
									'name'                    => $option_value['name'],
									'image'                   => $option_img,
									'price'                   => $price,
									'price_prefix'            => $option_value['price_prefix']
								);
							}
						}

						$options[] = array(
							'product_option_id'    => $option['product_option_id'],
							'product_option_value' => $product_option_value_data,
							'option_id'            => $option['option_id'],
							'name'                 => $option['name'],
							'type'                 => $option['type'],
							'value'                => $option['value'],
							'required'             => $option['required']
						);
					}
				}

				$data['products'][] = array(
					'product_id'  => $result['product_id'],
					'thumb'       => $image,
					'name'        => $result['name'],
					'description' => utf8_substr(strip_tags(html_entity_decode($result['description'], ENT_QUOTES, 'UTF-8')), 0, $this->config->get('config_product_description_length')) . '..',
					'price'       => $price,
					'special'     => $special,
					'tax'         => $tax,
					'minimum'     => $result['minimum'] > 0 ? $result['minimum'] : 1,
					'rating'      => $result['rating'],
					//'href'        => $this->url->link('product/product', 'path=' . $this->request->get['path'] . '&product_id=' . $result['product_id'] . $url),
					'options'     => $options,
				);
			}
		}

		echo json_encode($data);
	}

	public function featureProduct()
	{

		$this->load->model('catalog/product');
		$this->load->model('tool/image');
		$this->load->language('product/product');


		$seller_id = $this->request->get['seller_id'];


		$language_id = $this->request->get['language_id'];
		$limit_data['start'] = 0;
		$limit_data['limit'] = 10;

		$results = $this->model_catalog_product->getStoreFeatureProductApi($seller_id, $limit_data, $language_id);

		$data['products'] = array();
		foreach ($results as $result) {

			if ($result) {

				if ($result['image']) {
					$image = $this->model_tool_image->resize($result['image'], $this->config->get('config_image_product_width'), $this->config->get('config_image_product_height'));
				} else {
					$image = $this->model_tool_image->resize('placeholder.png', $this->config->get('config_image_product_width'), $this->config->get('config_image_product_height'));
				}

				if (($this->config->get('config_customer_price') && $this->customer->isLogged()) || !$this->config->get('config_customer_price')) {
					$price = $this->currency->format($this->tax->calculate($result['price'], $result['tax_class_id'], $this->config->get('config_tax')));
				} else {
					$price = false;
				}

				if ((float) $result['special']) {
					$special = $this->currency->format($this->tax->calculate($result['special'], $result['tax_class_id'], $this->config->get('config_tax')));
				} else {
					$special = false;
				}

				if ($this->config->get('config_tax')) {
					$tax = $this->currency->format((float) $result['special'] ? $result['special'] : $result['price']);
				} else {
					$tax = false;
				}

				if ($this->config->get('config_review_status')) {
					$rating = (int) $result['rating'];
				} else {
					$rating = false;
				}

				$options = array();

				if ($result['option_available'] == 1) {

					foreach ($this->model_catalog_product->getProductOptions($result['product_id']) as $option) {
						$product_option_value_data = array();

						foreach ($option['product_option_value'] as $option_value) {
							if (!$option_value['subtract'] || ($option_value['quantity'] > 0)) {
								if ((($this->config->get('config_customer_price') && $this->customer->isLogged()) || !$this->config->get('config_customer_price')) && (float) $option_value['price']) {
									$price = $this->currency->format($this->tax->calculate($option_value['price'], $result['tax_class_id'], $this->config->get('config_tax') ? 'P' : false));
								} else {
									$price = false;
								}

								if (isset($option_value['image'])) {
									$option_img = $this->model_tool_image->resize($option_value['image'], 50, 50);
								} else {
									$option_img = '';
								}

								$product_option_value_data[] = array(
									'product_option_value_id' => $option_value['product_option_value_id'],
									'option_value_id'         => $option_value['option_value_id'],
									'name'                    => $option_value['name'],
									'image'                   => $option_img,
									'price'                   => $price,
									'price_prefix'            => $option_value['price_prefix']
								);
							}
						}

						$options[] = array(
							'product_option_id'    => $option['product_option_id'],
							'product_option_value' => $product_option_value_data,
							'option_id'            => $option['option_id'],
							'name'                 => $option['name'],
							'type'                 => $option['type'],
							'value'                => $option['value'],
							'required'             => $option['required']
						);
					}
				}

				$data['products'][] = array(
					'product_id'  => $result['product_id'],
					'thumb'       => $image,
					'name'        => $result['name'],
					'description' => utf8_substr(strip_tags(html_entity_decode($result['description'], ENT_QUOTES, 'UTF-8')), 0, $this->config->get('config_product_description_length')) . '..',
					'price'       => $price,
					'special'     => $special,
					'tax'         => $tax,
					'minimum'     => $result['minimum'] > 0 ? $result['minimum'] : 1,
					'rating'      => $result['rating'],
					//'href'        => $this->url->link('product/product', 'path=' . $this->request->get['path'] . '&product_id=' . $result['product_id'] . $url),
					'options'     => $options,
				);
			}
		}
		
		echo json_encode($data);
	}

	public function sellerRating()
	{

		$this->load->model('catalog/seller');
		$this->load->model('tool/image');
		$this->load->language('product/product');


		$seller_id = $this->request->get['seller_id'];

		echo number_format($this->model_catalog_seller->getSellerRating($seller_id), 1);
	}

	public function storeHomeCategory()
	{

		$seller_id = $this->request->get['seller_id'];
		$language_id = $this->request->get['language_id'];

		$this->load->model('store/store');
		$data['sellerData'] = $this->model_store_store->getSellerByID($seller_id);

		$categoryArray = array();

		$query = $this->db->query("SELECT DISTINCT category_id,name FROM  " . DB_PREFIX . "category_description cd 
		 WHERE cd.category_id IN (" . $data['sellerData']['special_category'] . ") AND cd.language_id = " . $language_id);
		echo json_encode($query->rows);


		/*if(isset($data['sellerData']['special_category'])){
			$categoryArray['category'] = explode(",",$data['sellerData']['special_category']);
		}
		echo json_encode($categoryArray);
		*/
	}
}
