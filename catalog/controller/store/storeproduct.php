<?php

class ControllerStoreStoreProduct extends Controller {

    public function index() {

        $this->load->model('catalog/product');
        $this->load->model('catalog/category');
        $this->load->model('tool/image');
        $this->load->language('product/product');
        $data['button_cart'] = $this->language->get('button_cart');

        if (isset($this->request->get['seller_id'])) {
            $seller_id = $this->request->get['seller_id'];
        } else {
            $seller_id = $_SESSION['storeID'];
        }
        
		if (isset($this->session->data['page'])) {
			$page = $this->session->data['page'] + 1;
		} else {
			$page = 1;
		}

		if (isset($this->session->data['limit'])) {
			$limit = (int)$this->session->data['limit'] + 10;
		} else {
			$limit = 10;
		}

		$filter_data = array(			
			'start'              =>  $this->request->get['start'],//$this->session->data['limit'] + 1,
			'limit'              =>  $this->request->get['limit']//$limit
		);
		
		$this->session->data['limit'] = $this->session->data['limit'] + 10;

		
		$language_id = (int)$this->config->get('config_language_id');
        $results = $this->model_catalog_product->getSellerProductsV3($seller_id, $filter_data, $language_id);
		// echo '<pre>';
		// print_r($results);
		// echo '</pre>';
		// exit;
        $data['products'] = array();
       
        foreach ($results as $result) {		
			$special_int = 0;
			$price_int = 0;
			if ($result) {			
				if ($result['image']) {
					$image = $this->model_tool_image->resize($result['image'], $this->config->get('config_image_product_width'), $this->config->get('config_image_product_height'));
				} else {
					$image = $this->model_tool_image->resize('placeholder.png', $this->config->get('config_image_product_width'), $this->config->get('config_image_product_height'));
				}

				if (($this->config->get('config_customer_price') && $this->customer->isLogged()) || !$this->config->get('config_customer_price')) {
					$price = $this->currency->format($this->tax->calculate($result['price'], $result['tax_class_id'], $this->config->get('config_tax')));
					$price_int = $result['price'];
				} else {
					$price = false;
				}

				if ((float)$result['special']) {
					$special_int = $result['special'];
					$special = $this->currency->format($this->tax->calculate($result['special'], $result['tax_class_id'], $this->config->get('config_tax')));
				} else {
					$special = false;
				}

				if ($this->config->get('config_tax')) {
					$tax = $this->currency->format((float)$result['special'] ? $result['special'] : $result['price']);
				} else {
					$tax = false;
				}

				if ($this->config->get('config_review_status')) {
					$rating = (int)$result['rating'];
				} else {
					$rating = false;
				}
				
				$options = array();
				
				if($result['option_available'] == 1){

					foreach ($this->model_catalog_product->getProductOptions($result['product_id']) as $option) {
						$product_option_value_data = array();
					
						foreach ($option['product_option_value'] as $option_value) {
							if (!$option_value['subtract'] || ($option_value['quantity'] > 0)) {
								if ((($this->config->get('config_customer_price') && $this->customer->isLogged()) || !$this->config->get('config_customer_price')) && (float)$option_value['price']) {
									// $price_int = $option_value['price'];
									// $price = $this->currency->format($this->tax->calculate($option_value['price'], $result['tax_class_id'], $this->config->get('config_tax') ? 'P' : false));
								} else {
									//$price = false;
								}
								
								if(isset($option_value['image'])){
									$option_img = $this->model_tool_image->resize($option_value['image'], 50, 50);
								} else {
									$option_img = '';
								}

								$product_option_value_data[] = array(
									'product_option_value_id' => $option_value['product_option_value_id'],
									'option_value_id'         => $option_value['option_value_id'],
									'name'                    => $option_value['name'],
									'image'                   => $option_img,
									'price'                   => $price,
									'price_prefix'            => $option_value['price_prefix']
								);
							}
						}

						$options[] = array(
							'product_option_id'    => $option['product_option_id'],
							'product_option_value' => $product_option_value_data,
							'option_id'            => $option['option_id'],
							'name'                 => $option['name'],
							'type'                 => $option['type'],
							'value'                => $option['value'],
							'required'             => $option['required']
						);
					}
				}
				//discount 
				$discount = 0;
				if($special_int != 0 && $special_int != ''){
					$discount = round((($price_int-$special_int)/$price_int)*100, 0); 
					if($discount > 100 && $discount < 0 && $discount != 0) {
						$discount = 0;
					}
				}
				$data['products'][] = array(
					'product_id'  => $result['product_id'],
					'thumb'       => $image,
					'name'        => $result['name'],
					'description' => utf8_substr(strip_tags(html_entity_decode($result['description'], ENT_QUOTES, 'UTF-8')), 0, $this->config->get('config_product_description_length')) . '..',
					'price'       => $price,
					'special'     => $special,
					'discount'    => $discount,
					'tax'         => $tax,
					'minimum'     => $result['minimum'] > 0 ? $result['minimum'] : 1,
					'rating'      => $result['rating'],
					//'href'        => $this->url->link('product/product', 'path=' . $this->request->get['path'] . '&product_id=' . $result['product_id'] . $url),
					'options'     => $options,
				);
		
			}			
        }
		
        $data['product_data'] = array();
        $data['cartData'] = $this->cart->getProducts();
        if (!count($data['cartData'])) {
            $data['cartData'] = array();
        }else{
            foreach($data['cartData'] as $product){
                if($product['seller_id'] != $_SESSION['storeID']){
                    //$this->cart->clear();
                    break;
                    //$data['cartData'] = array();
                }
            }

        }
        
		$data['product_data'] = array(
            'products' => $data['products'],            
            'cart_data' => $data['cartData']
        );

		 $data['seller_id'] = $seller_id;

        if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/template/store/storeproduct.tpl')) {
            $this->response->setOutput($this->load->view($this->config->get('config_template') . '/template/store/storeproduct.tpl', $data));
        }
    }
}

?>
