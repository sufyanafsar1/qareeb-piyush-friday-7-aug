<?php
class ControllerTotalCouponPopup extends Controller {
	public function index() {
		if ($this->config->get('coupon_status')) {
			$this->load->language('total/coupon');

			$data['heading_title'] = $this->language->get('heading_title');

			$data['text_loading'] = $this->language->get('text_loading');

			$data['entry_coupon'] = $this->language->get('entry_coupon_popup');

			$data['button_coupon'] = $this->language->get('button_coupon');

			if (isset($this->session->data['coupon'])) {
				$data['coupon'] = $this->session->data['coupon'];
			} else {
				$data['coupon'] = '';
			}
		
			if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/template/total/coupon_popup.tpl')) {
				return $this->load->view($this->config->get('config_template') . '/template/total/coupon_popup.tpl', $data);
			} else {
				return $this->load->view('default/template/total/coupon_popup.tpl', $data);
			}
		}
	}

	public function coupon() {
		$this->load->language('total/coupon');

		$json = array();

		$this->load->model('total/coupon');

		if (isset($this->request->post['coupon'])) {
			$coupon = $this->request->post['coupon'];
		} else {
			$coupon = '';
		}
		$session_id =$this->session->getId();
		$customer = $this->customer->getId();
		$allProducts = $this->cart->getProducts();
		$language_id = 1;
		$seller =[];
		foreach ($allProducts as $key1 => $value1) {
			$seller[$value1['seller_id']]  = $value1['seller_id'];
		}
		
	
		$coupon_info = $this->model_total_coupon->getDesktopCouponnew($coupon,$customer,$language_id,$session_id,$seller);
		// $coupon_info = $this->model_total_coupon->getCoupon($coupon);
	
		if (empty($this->request->post['coupon'])) {
			$json['error'] = $this->language->get('error_empty');

			unset($this->session->data['coupon']);
		} elseif ($coupon_info) {
			$this->session->data['coupon'] = $this->request->post['coupon'];
			
			$this->session->data['success'] = $this->language->get('text_success');

			$json['redirect'] = $this->url->link('common/home');
		} else {
			$json['error'] = $this->language->get('error_coupon');
		}

		$this->response->addHeader('Content-Type: application/json');
		$this->response->setOutput(json_encode($json));
	}
}
