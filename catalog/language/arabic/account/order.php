<?php
//  Website: WWW.OpenCartArab.com
//  E-Mail : info@OpenCartArab.com

// Heading
$_['heading_title']         = 'تـفـاصـيـل الـطـلب';

// Text
$_['text_account']          = 'الحساب';
$_['text_order']            = 'معلومات الطلب';
$_['text_order_detail']     = 'تفاصيل الطلب';
$_['text_invoice_no']       = 'رقم الفاتورة :';
$_['text_order_id']         = 'رقم الطلب :';
$_['text_date_added']       = 'تاريخ الطلب :';
$_['text_shipping_address'] = 'عنوان الشحن';
$_['text_shipping_method']  = 'طريقة الشحن :';
$_['text_payment_address']  = 'عنوان الدفع';
$_['text_payment_method']   = 'طريقة الدفع :';
$_['text_comment']          = 'الملاحظات على الطلب';
$_['text_history']          = 'سجل الطلب';
$_['text_success']          = 'لقد قمت بنجاح بإضافة المنتج من الطلب رقم #%s إلى سلة الشراء!';
$_['text_empty']            = 'لا توجد طلبات خاصة بك !';
$_['text_error']            = 'لم يتم العثور على طلبات !';
$_['text_seller']            = 'منتج من:';
$_['text_products']            = 'منتجات:';
$_['text_status']            = 'حالة:';
$_['text_rate']            = 'تقيم هذا متجر:';
$_['text_shipping_fee']    = 'رسوم توصيل';


// Column
$_['column_order_id']        = 'رقم الطلب :';
$_['column_product']        = 'المنتجات :';
$_['column_customer']       = 'العميل :';
$_['column_name']           = 'اسم المنتج';
$_['column_model']          = 'النوع';
$_['column_quantity']       = 'الكمية';
$_['column_price']          = 'السعر';
$_['column_total']          = 'المجموع';
$_['column_action']         = 'تحرير';
$_['column_date_added']     = 'تاريخ الطلب';
$_['column_status']         = 'حالة الطلب';
$_['column_comment']        = 'ملاحظات';
$_['column_review']        = 'استعراض';
$_['text_review']        = 'استعراض';
// Error
$_['error_reorder']         = '%s هذا المنتج غير متوفر حالياً لإعادة طلبه.';
$_['text_store']         = "لا يوجد متجر في المنطقة المحددة!";
$_['text_product']         = 'المنتج غير موجود في المتجر!';
$_['text_cart']         = 'ستتم إزالة عناصر سلة التسوق الخاصة بك أثناء التغيير إلى متجر آخر. هل أنت متأكد من الانتقال إلى هذا المتجر؟';