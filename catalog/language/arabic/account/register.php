<?php
//  Website: WWW.OpenCartArab.com
//  E-Mail : info@OpenCartArab.com

// Heading
$_['heading_title']        = 'انشاء حساب جديد';

// Text
$_['text_account']         = 'الحساب';
$_['text_register']        = 'تسجيل';
$_['text_account_already'] = 'إذا كان لديك حساب معنا ، الرجاء الدخول إلى <a href="%s">صفحة تسجيل الدخول</a>.';
$_['text_your_details']    = 'معلوماتك الشخصية';
$_['text_your_address']    = 'العناوين الخاصة بك';
$_['text_newsletter']      = 'القائمة البريدية';
$_['text_your_password']   = 'كلمة المرور الخاصة بك';
$_['text_agree']           = 'لقد قرأت ووافقت على <a href="%s" class="agree"><b>%s</b></a>';
$_['text_otp']           = 'Dear customer, please use OTP';
$_['text_otp_continue']       = 'to continue registeration in';
$_['text_login_telephone'] = 'تم إنشاء حسابك بنجاح وتم إرسال رمز تحقق إلى الرسائل القصيرة الخاصة بك ، يرجى إدخاله أدناه ، والنقر فوق التحقق من الحساب';
$_['text_verify_password'] = 'أدخل كلمة المرور المستردة للتحقق، وإلا لن يكون الحساب نشطا';
$_['text_verified_password'] = 'تم الآن تنشيط حسابك';
$_['text_resend_password'] = 'يتم إرسال كلمة المرور على هاتفك النقال';
$_['text_verification']    = '..تم التحقق';
// Button
$_['button_continue']         = 'استمر';
$_['button_resend']           = 'أعد إرسال الرمز';
$_['button_verify']           = 'التحقق من الحساب';

// Entry
$_['entry_customer_group'] = 'مجموعة العميل';
$_['entry_firstname']      = 'الاسم الأول';
$_['entry_lastname']       = 'الاسم الاخير';
$_['entry_email']          = 'البريد الالكتروني';
$_['entry_telephone']      = 'رقم الجوال';
$_['entry_fax']            = 'فاكس';
$_['entry_company']        = 'الشركة';
$_['entry_address_1']      = 'العنوان الاول';
$_['entry_address_2']      = 'العنوان الثاني';
$_['entry_postcode']       = 'صندوق البريد';
$_['entry_country']        = 'البلد';
$_['entry_zone']           = 'المنطقة / المحافظة';
$_['entry_newsletter']     = 'اشترك';
$_['entry_password']       = 'كلمة المرور';
$_['entry_confirm']        = 'تأكيد كلمة المرور';
$_['city_text']            = 'مدينة';
$_['entry_city']           = 'المدينة';
$_['entry_region']         = 'منطقة';

// Error
$_['error_exists']         = 'تحذير : البريد الالكتروني مسجل مسبقا!';
$_['error_exists']         = 'تحذير: عنوان بريدك الإلكتروني مسجل بالفعل ، يرجى تسجيل الدخول أو النقر فوق
<a href="index.php?route=account/forgotten" class="agree"><b>هل نسيت كلمة المرور</b></a>';
$_['error_firstname']      = 'الاسم الأول يجب أن يكون أكثر من 1 وأقل من 32 حرفا!';
$_['error_lastname']       = 'الاسم الاخير يجب أن يكون أكثر من 1 وأقل من 32 حرفا!';
$_['error_email']          = 'البريد الإلكتروني غير صحيح الرجاء إعادة كتابته من جديد!';
//$_['error_telephone']      = 'رقم الهاتف يجب أن يكون أكثر من 3 وأقل من 32 رقم!';
$_['error_telephone']      = 'يجب أن يبدأ تشغيل الجوال 966';
$_['error_address_1']      = 'يجب ان يكون بين 3 و 128 رمز !';
// $_['error_city']           = 'يجب ان يكون بين 2 و 128 رمز !';
$_['error_postcode']     = 'يجب ان يكون بين 2 و 10 رمز !';
$_['error_country']      = 'الرجاء الاختيار !';
$_['error_zone']         = 'الرجاء الاختيار !';
$_['error_custom_field'] = '%s مطلوب !';
$_['error_password']     = 'كلمة المرور يجب أن تكون أكثر من 3 وأقل من 20 حرفا!';
$_['error_confirm']      = 'لم تتطابق كلمة المرور الرجاء إعادة كتابته مرة أخرى!';
$_['error_agree']        = 'تحذير : يجب أن توافق على %s !';
$_['text_sign_u_with']   = 'سجل مع';
$_['text_or']            = 'أو';
$_['error_city']         = 'حقل المدينة مطلوب!';
$_['error_region']       = 'حقل المنطقة مطلوب!';