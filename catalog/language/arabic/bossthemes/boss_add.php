<?php
// Cart

// Text
$_['text_title_cart']             = 'تم أضف بالسلة';
$_['text_thumb']             = '<img src="%s" alt="" />';
$_['text_success_cart']           = '<a href="%s">%s</a> <a href="%s">تم أضف</a> ال سلة '; 
$_['text_items']    = '%s منتجات - %s';
$_['text_order_receive_today'] = 'يوصلك طلبك اليوم  ';
$_['text_order_receive_tomorrow'] = 'يوصلك طلبك غدً   ';
$_['text_order_receive'] = 'يوصلك طلبك ';
// Error
$_['error_required']         = '%s required!';

// Wishlist
$_['text_title_wishlist']             = 'Product added to wish list';
$_['text_success_wishlist']           = 'Success: You have added <a href="%s">%s</a> to your <a href="%s">wish list</a>!';

// Compare
$_['text_title_compare']             = 'Product added to compare';
$_['text_success_compare']           = 'Success: You have added <a href="%s">%s</a> to your <a href="%s">product comparison</a>!';
$_['text_no_delivery'] = 'لا يوجد التوصيل';
?>