<?php
//  Website: WWW.OpenCartArab.com
//  E-Mail : info@OpenCartArab.com

// Text
$_['text_items']    = '%s منتجات - %s';
$_['text_empty']    = 'سلة الشراء فارغة !';
$_['text_cart']     = 'معاينة السلة';
$_['text_checkout'] = 'إنهاء الطلب';
$_['text_recurring']  = 'ملف الدفع';
//$_['text_items']    = 'حقيبة التسوق';
$_['text_free_delivery'] = 'واؤو ! فتحت التوصيل المجاني';
$_['text_no_free_delivery'] = 'تسوق  %s ...ريال او اكثر لفتح التوصيل مجاني';

$_['text_no_delivery_slot'] = 'لايوجد وقت التوصيل ';
$_['text_delivery_slot'] = " :التوصيل القادم اليوم";

$_['text_seller']            = 'منتج من:';

$_['text_order_receive_today'] = 'يوصلك طلبك اليوم  ';
$_['text_order_receive_tomorrow'] = 'يوصلك طلبك غدً   ';


$_['text_minimum_order']          = 'حد الأقل';