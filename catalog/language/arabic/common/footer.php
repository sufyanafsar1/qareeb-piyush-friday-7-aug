<?php

//  Website: WWW.OpenCartArab.com

//  E-Mail : info@OpenCartArab.com



// Text

$_['text_information']  = 'معلومات';

$_['text_service']      = 'خدمات العملاء';

$_['text_extra']        = 'إضافات';

$_['text_contact']      = 'اتصل بنا';



$_['text_sitemap']      = 'خريطة الموقع';

$_['text_manufacturer'] = 'الشركات';

$_['text_voucher']      = 'قسائم الهدايا';

$_['text_affiliate']    = 'نظام العمولة';

$_['text_special']      = 'العروض المميزة';

$_['text_account']      = 'حسابي';

$_['text_order']        = 'طلباتي';

$_['text_wishlist']     = 'قائمة رغباتي';

$_['text_newsletter']   = 'القائمة البريدية';
$_['sell-with-us']   = 'بيع معنا';

$_['text_powered']      = '<br /> %s &copy; %s';

$_['text_careeb']       = 'قريب تكنولوجيز ذ.م.م جميع الحقوق محفوظة';

$_['text_location_disable'] 	 = 'آسف! موقعك على الجوال معطّل';
$_['text_city'] 		 = 'اختر المدينة';
$_['text_region'] 		 = 'اختر المنطقة';
$_['text_area'] 		 = 'اختر حى';

$_['text_current_city'] = 'المدينة الحالية: ';
$_['text_current_area'] = 'المنطقة الحالية: ';

$_['text_shop']      = 'متجر';
$_['text_about']     = 'عنا';
$_['text_careers']     = 'وظائف';
$_['text_contact']      = 'اتصل بنا';
$_['txt_contact_us'] = '!اتصل بنا';

$_['text_press']     = 'المركز الأعلامي';
$_['text_faq']     = 'التعليمات';
$_['text_terms']      = 'شروط والأحكام';
$_['text_return']       = 'سياسة ارجاع';

$_['text_join_us'] = 'انضم إلينا';
$_['text_become_a_shopper'] = 'كن متسوقًا';
$_['text_sell_with_us'] = 'بع معنا';

$_['text_technologies'] = '٢٠١٩ © شركة قريب تكنولوجيز ش م م';
$_['text_download_app'] = 'حمل التطبيق';
$_['text_stay_connected'] = 'كن على تواصل';

$_['text_register_store']       = 'سجل متجر خاص بك';
$_['text_register_why']       = 'ليش سجل';
$_['text_register_merchant']       = 'دخول تاجر';