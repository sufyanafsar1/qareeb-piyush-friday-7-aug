<?php

$_['text_home']          = 'الرئيسية';
$_['text_wishlist']      = 'قائمة رغباتي (%s)';
$_['text_shopping_cart'] = 'سلة الشراء';
$_['text_category']      = 'أقسام الموقع';
$_['text_account']       = 'حسابي';
$_['text_register']      = 'تسجيل جديد';
$_['text_login']         = 'تسجيل الدخول';
$_['text_order']         = 'طلباتي';
$_['text_transaction']   = 'رصيدي';
$_['text_download']      = 'ملفات التنزيل';
$_['text_logout']        = 'خروج';
$_['text_checkout']      = 'إنهاء الطلب';
$_['text_search']        = 'بحث';
$_['text_all']           = 'اذهب الى قسم';
$_['text_store_credit']= 'مخزن اعتمادات';
$_['text_account_deatil']= 'تفاصيل الحساب';
$_['text_store']= 'متجر';
$_['text_billing']= 'الفواتير';
$_['txt_community'] = 'مجمعات';
$_['text_find_me'] = 'تحدد موقع لي';

$_['text_change_location'] = 'غير موقعك';

$_['text_close']          = 'مغلق';
$_['text_store_closed']          = 'متجر مغلق';
$_['text_today']          = 'اليوم';
$_['text_tomorrow']          = 'غدا';
$_['text_monday']          = 'الأثنين';
$_['text_tuesday']          = 'الثلاثاء';
$_['text_wednesday']          = 'الاربعاء';
$_['text_thursday']          = 'الخميس';
$_['text_friday']          = 'الجمعة';
$_['text_saturday']          = 'السبت';
$_['text_sunday']          = 'الأحد';
$_['text_current_time']          = 'وقت الحالي';
$_['text_delivery_schedule']          = 'جدول التوصيل';
$_['text_delivery_checkout']          = 'اختر وقت المناسب لك عند الخروج';
$_['text_delivery_charges']          = 'رسوم التوصيل';
$_['text_order_limit']          = 'حد الأقل';
$_['text_next_delivery']          = 'التوصيل التالى';
$_['text_no_delivery']          = 'لا يوجد التوصيل';
$_['text_unavailable']          = 'غير متوفر';

$_['text_vat_included1'] = 'الأسعار شاملة ';
$_['text_vat_included2'] = ' ضريبة القيمة المُضافة';
