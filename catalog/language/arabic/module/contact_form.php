<?php
// Heading	
$_['heading_title']    = 'كل ما تحتاجه يمكنك الحصول<br>عليه من المتاجر المفضلة لديك';
$_['firstli']    = 'سهولة الوصول والاستخدام';
$_['secondi']    = 'مناسب وفعال من حيث التكلفة';
$_['thirdli']    = 'لا لصفوف انتظار مزعجة';
$_['forthli']    = 'عروض خصم اسبوعية';
$_['fifthli']    = 'أقل وقت للتسليم';
$_['sixthli']    = 'متسوقين مخلصين وذوي مهارة';
$_['avld_text']    = 'متوفر علي أندرويد و iOS';
$_['download_text']    = 'حمل التطبيق';

$_['text_need_assistances']    = 'تحتاج الى مساعدة؟';
$_['text_query']       = 'إذا كان لديك أي استفسارات أو تحتاج إلى دعم ، فلا </br> تتردد في الاتصال بنا!';
$_['text_query_mob']       = 'إذا كان لديك أي استفسارات أو تحتاج إلى دعم ، فلا </br> تتردد في الاتصال بنا!';
$_['text_UAE']      = '<b>الإمارات</b><br>الطابق 41 ، برج الإمارات ، </br>شارع الشيخ زايد ، دبي ، الإمارات العربية المتحدة';
$_['text_SA']   = '<b>المملكة العربية السعودية</b><br>زا سبيس، الطابق الثاني ، عليا بلازا<br>طريق الثمامة ، حي الربيع ، الرياض';
$_['text_tel']     = 'الهاتف: +966 553044161<br>info@qareeb.com';
$_['text_name']        = 'الإسم';
$_['text_email']    = 'بريد إلكتروني';
$_['text_phone']     = 'جوال';
$_['text_subject']    = 'الموضوع';
$_['text_mesage']       = 'رسالتك';
$_['text_btn_subject']    = 'إعتمد';

// Error
$_['error_name']     = 'Name must be between 3 and 64 characters!';
$_['error_email']    = 'Email must be required';
$_['error_phone']    = 'Phone must be required!';
$_['error_subject']  = 'Subject must be required!';
$_['error_message']  = 'Message must be required!';
$_['text_success']   = 'Thank You.';
