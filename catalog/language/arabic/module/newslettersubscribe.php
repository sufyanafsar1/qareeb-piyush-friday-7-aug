<?php
// Heading 
$_['heading_title'] 	 = 'ابقى على اتصال';

//Fields
$_['entry_email'] 		 = 'البريد الإلكتروني';
$_['entry_name'] 		 = 'اسم';

//Buttons
$_['entry_button'] 		 = 'الاشتراك';
$_['entry_unbutton'] 	 = 'إلغاء الاشتراك';

//text
$_['text_subscribe'] 	 = 'اشترك هنا';
$_['text_subscribe_now'] 	 = 'ضع بريدك الإلكتروني هنا';
$_['text_email'] 	 = 'وضع عنوان البريد الإلكتروني الخاص بك هنا';

$_['mail_subject']   	 = 'النشرة الإخبارية الاشتراك';

$_['error_invalid']	    	 = "<div class='alert alert-danger'><i class='fa fa-exclamation-circle'></i> تحذير: بريد إلكتروني خاطئ</div> ";

$_['subscribe']	    	 = "<div class='alert alert-success'><i class='fa fa-exclamation-circle'></i> نجاح: شكرا لك على الاشتراك في النشرة الإخبارية, %s.</div> ";

$_['unsubscribe'] 	     = 'إلغاء الاشتراك بنجاح';
$_['alreadyexist'] 	     = 'موجود مسبقا';
$_['notexist'] 	    	 = 'معرف البريد الإلكتروني غير موجود';

$_['text_join_our_mailing'] = 'انضم لقائمتنا البريدية';
$_['text_never_miss'] 	   = 'لا تفوت اي تحديث';

?>