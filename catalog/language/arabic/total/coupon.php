<?php
//  Website: WWW.OpenCartArab.com
//  E-Mail : info@OpenCartArab.com


// Text
$_['text_coupon']  = 'قسيمة التخفيض(%s)';
$_['heading_title'] = 'Use Coupon Code';

// Text
$_['text_success']  = 'Success: Your coupon discount has been applied!';

// Entry
$_['entry_coupon']  = 'Enter your coupon here';
$_['entry_coupon_popup']  = 'كوبون';

// Error
$_['error_coupon']  = 'Warning: Coupon is either invalid, expired or reached its usage limit!';
$_['error_empty']   = 'Warning: Please enter a coupon code!';

$_['button_coupon']   = 'طبق';