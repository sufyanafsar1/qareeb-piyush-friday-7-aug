<?php
// Heading
$_['heading_title']      = 'My Account';

// Text
$_['text_account']       = 'Account';
$_['text_my_account']    = 'My Account';
$_['text_my_orders']     = 'My Orders';
$_['text_my_newsletter'] = 'Newsletter';
$_['text_edit']          = 'Edit Account';
$_['text_password']      = 'Password';
$_['text_address']       = 'Address Book';
$_['text_wishlist']      = 'Wish List';
$_['text_order']         = 'Order History';
$_['text_download']      = 'Downloads';
$_['text_reward']        = 'Reward Points';
$_['text_return']        = 'Returns';
//$_['text_transaction']   = 'Your Transactions';
$_['text_transaction']   = 'Store Credits';
$_['text_newsletter']    = 'Newsletter';
$_['text_recurring']     = 'Recurring payments';
$_['text_transactions']  = 'Transactions';
$_['text_invite']  = "Invite your Friend";
