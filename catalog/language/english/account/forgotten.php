<?php
// Heading
$_['heading_title']   = 'Forgot Your Password?';

// Text
$_['text_account']    = 'Account';
$_['text_forgotten']  = 'Forgot Password';
$_['text_your_email'] = 'Your E-Mail Address';
$_['text_email']      = 'Enter the e-mail address associated with your account. Click submit to have your password e-mailed to you.';
$_['text_success']    = 'Success: A new password has been sent to your mobile.';
$_['text_success_email']    = 'Success: A new password has been sent to your email.';
$_['text_your_mobile'] = 'Your Mobile No. OR E-mail Address';
$_['text_mobile']      = 'Enter mobile no. or e-mail address associated with your account. Click submit to have your password send to you on mobile or Email.';
$_['text_associated_email_address']      = 'Enter your Qareeb-associated email address and we’ll send a link to change your password.';

// Entry
$_['entry_email']           = 'E-Mail Address';
$_['entry_email_placeholder'] = 'user@gmail.com';
$_['entry_mobile']          = 'Mobile';
$_['entry_mobile_format']   = 'e.g 966xxxxxxxxx';
$_['entry_reset_password']   = 'Reset your password';

// Error
$_['error_email']     = 'Warning: The E-Mail Address was not found in our records, please try again!';
$_['error_mobile_email'] = 'Warning: Please Enter Contact number or Email';
$_['error_mobile']    = 'Warning: Mobile No. was not found in our records, please try again!';
$_['error_approved']  = 'Warning: Your account requires approval before you can login.';

