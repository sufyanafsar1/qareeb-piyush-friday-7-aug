 <?php
// Heading
$_['heading_title']        = 'Create an account';

// Text
$_['text_account']         = 'Account';
$_['text_register']        = 'Register';
$_['text_account_already'] = 'If you already have an account with us, please login at the <a href="%s">login page</a>.';
$_['text_your_details']    = 'Your Personal Details';
$_['text_your_address']    = 'Your Address';
$_['text_newsletter']      = 'Newsletter';
$_['text_your_password']   = 'Your Password';
$_['text_agree']           = 'By Signing up, you agree to our <a href="%s" class="agree"><u>Terms</u></a> and <a href="%s" class="agree"><u>%s</u></a>';
$_['text_login_telephone'] = 'Your account is successfully created and a verification code is sent to via SMS, please enter it below, and click verify';
$_['text_verify_password'] = 'Enter recived password for verification, otherwise account will not be active';
$_['text_verified_password'] = 'Your account has now been activated';
$_['text_resend_password'] = 'Password is sent on your mobile';
$_['text_verification']    = 'Thank you for verification. Your password and further instructions have been sent to your e-mail.';
$_['entry_mobile_format']   = 'e.g 9665xxxxxxxx';
$_['text_password']       = 'Your password is:';
$_['text_otp']           = 'Dear customer, please use OTP';
$_['text_otp_continue']       = 'to continue registeration in';
$_['text_subject']        = '%s - Thank you for registering';

// Button
$_['button_continue']         = 'Create your account';
$_['button_resend']           = 'Resend Code';
$_['button_verify']           = 'Verify Account';
// Entry
$_['entry_customer_group'] = 'Customer Group';
$_['entry_firstname']      = 'First Name';
$_['entry_lastname']       = 'Last Name';
$_['entry_email']          = 'E-Mail';
$_['entry_telephone']      = 'Mobile';
$_['entry_fax']            = 'Fax';
$_['entry_company']        = 'Company';
$_['entry_address_1']      = 'Address';
$_['entry_address_2']      = 'Address 2';
$_['entry_postcode']       = 'Post Code';
$_['entry_city']           = 'City';
$_['entry_country']        = 'Country';
$_['entry_zone']           = 'Zone';
$_['entry_newsletter']     = 'Subscribe to our newsletter';
$_['entry_password']       = 'Password';
$_['entry_confirm']        = 'Password Confirm';
$_['entry_region']         = 'Region';
$_['entry_city']           = 'City';

// Error
$_['error_exists']         = 'Warning: Your email address is already registered, kindly login OR click on <a href="index.php?route=account/forgotten" class="agree"><b>Forgot Password</b></a>';
$_['error_exists_mobile']  = 'Warning: Mobile Number is already registered!';
$_['error_firstname']      = 'First Name must be between 1 and 32 characters!';
$_['error_lastname']       = 'Last Name must be between 1 and 32 characters!';
$_['error_email']          = 'E-Mail Address does not appear to be valid!';
//$_['error_telephone']      = 'Mobile must be between 3 and 32 characters!';
$_['error_telephone']    = 'Mobile must be start with 9665 and length must be 8 digits!';
$_['error_address_1']    = 'Address 1 must be between 3 and 128 characters!';
$_['error_city']         = 'City must be between 2 and 128 characters!';
$_['error_postcode']     = 'Postcode must be between 2 and 10 characters!';
$_['error_country']      = 'Please select a country!';
$_['error_zone']         = 'Please select a region / state!';
$_['error_custom_field'] = '%s required!';
$_['error_password']     = 'Password must be between 4 and 20 characters!';
$_['error_confirm']      = 'Password confirmation does not match password!';
$_['error_agree']        = 'Warning: You must agree to the %s!';
$_['error_verify']       = 'Warning: Some error occured. Verify again';
$_['text_sign_u_with']   = 'SIGN UP WITH';
$_['text_or']            = 'OR';
$_['error_region']       = 'Region field is required!';
$_['error_city']         = 'City field is required!';