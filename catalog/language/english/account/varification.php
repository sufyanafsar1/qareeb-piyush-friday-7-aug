		<?php
// Heading
$_['heading_title']   = 'Varification Your OTP';

// Text
$_['text_account']    = 'Account';
$_['button_continue']  = 'Verify Account';
$_['button_resend']  = 'Resend Code';
$_['text_your_email'] = 'Your Varification OTP';
$_['text_varification']  = 'Your account is successfully created and a verification code is sent to via SMS, please enter it below, and click verify
';

// Entry
$_['entry_password']     = 'Enter recived password for verification, otherwise account will not be active';

// Error
$_['error_email']     = 'Warning: please Enter Your OTP!';
$_['error_approved']  = 'Warning: please Enter Valid OTP.';
