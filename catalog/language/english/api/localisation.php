<?php
$_['text_close']         	 = 'close';
$_['text_open']         	 = 'open';
$_['text_today']          = 'Today';
$_['text_tomorrow']       = 'Tomorrow';
$_['text_next_delivery']          = 'Next Delivery';
$_['text_no_delivery']          = 'No Delivery Available';
$_['text_unavailable']          = 'Unavailable';

$_['text_close_arabic']         	 = 'غلق';
$_['text_open_arabic']         	 = 'فتح';
$_['text_today_arabic']          = 'اليوم';
$_['text_tomorrow_arabic']       = 'غدا';
$_['text_next_delivery_arabic']          = 'التسليم القادم';
$_['text_no_delivery_arabic']          = 'لا يوجد توصيل متاح';
$_['text_unavailable_arabic']          = 'غير متوفره';
