<?php
// Cart

// Text
$_['text_title_cart']             = 'Product added to Cart';
$_['text_thumb']             = '<img src="%s" alt="" />';
$_['text_success_cart']           = '<a href="%s">%s</a> added to <a href="%s">shopping cart</a>!';
$_['text_items']             = '<b>%s item(s) - %s</b>';
$_['text_order_receive_today'] = 'You will be receive your order today at ';
$_['text_order_receive_tomorrow'] = 'You will be receive your order tomorrow at ';
$_['text_order_receive'] = 'You will be receive your order on';
$_['text_no_delivery'] = 'No delivery available';


// Error
$_['error_required']         = '%s required!';	

// Wishlist
$_['text_title_wishlist']             = 'Product added to wish list';
$_['text_success_wishlist']           = 'Success: You have added <a href="%s">%s</a> to your <a href="%s">wish list</a>!';

// Compare
$_['text_title_compare']             = 'Product added to compare';
$_['text_success_compare']           = 'Success: You have added <a href="%s">%s</a> to your <a href="%s">product comparison</a>!';

?>