<?php
// Text
$_['text_home']          = 'Home';
$_['text_wishlist']      = 'Wish List (%s)';
$_['text_shopping_cart'] = 'Shopping Cart';
$_['text_category']      = 'Categories';
$_['text_account']       = 'My Account';
$_['text_register']      = 'Register';
$_['text_login']         = 'Login';
$_['text_order']         = 'Order History';
$_['text_transaction']   = 'Transactions';
$_['text_download']      = 'Downloads';
$_['text_logout']        = 'Logout';
$_['text_checkout']      = 'Checkout';
$_['text_search']        = 'Search';
$_['text_all']           = 'Show All';
$_['text_account_deatil']= 'Account Details';
$_['text_billing']		 = 'Billing';
$_['text_store_credit']	 = 'Store Credit';
$_['text_how_it_work'] 	 = 'How it Works';
$_['text_visit_careeb']  = 'Visit Qareeb.com';
$_['text_shopping'] 	 = 'We\'ll do the shopping for you.';
$_['text_grocery'] 		 = 'Order Groceries, Fruit, Vegetable, Meat and More<br/>from neighborhood store.';
$_['text_city'] 		 = 'Select City';
$_['text_region'] 		 = 'Select Region';
$_['text_area'] 		 = 'Select Area';
$_['text_find'] 		 = 'Find';
$_['text_locate_me'] 	 = 'Locate Me';
$_['text_location_disable'] 	 = 'Sorry! Your current location is disabled';

$_['Dammam']			 = 'Damman';
$_['Jeddah']			 = 'Jeddah';
$_['Riyadh']			 = 'Riyadh';

$_['Al-Ada'] = 'Al-Ada';
$_['Al-Arifi'] = 'Al-Arifi';
$_['Al-Badia'] = 'Al-Badia';
$_['Al-Murjan'] = 'Al-Murjan';
$_['An-Nahda'] = 'An-Nahda';
$_['As-Safa'] = 'As-Safa';