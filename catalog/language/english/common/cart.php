<?php
// Text
 $_['text_items']     = '%s item(s) - %s';
$_['text_empty']     = 'Your shopping cart is empty!';
$_['text_cart']      = 'View Cart';
$_['text_checkout']  = 'Checkout';
$_['text_recurring'] = 'Payment Profile';
//$_['text_items'] = 'Shopping Bag';
$_['text_free_delivery'] = 'WOW! Now you have free delivery!';
$_['text_no_free_delivery'] = 'Shop with %s Riyals more to unlock free Delivery!';

$_['text_no_delivery_slot'] = 'No Delivery Slot Available.';
$_['text_delivery_slot'] = "Next Delivery Today: ";

$_['text_order_receive_today'] = 'You will be receive your order today at ';
$_['text_order_receive_tomorrow'] = 'You will be receive your order tomorrow at ';
$_['text_minimum_order'] = 'Minimum order limit is ';
$_['text_store']= 'Stores';