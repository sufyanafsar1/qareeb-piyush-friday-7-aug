<?php
// Text
$_['text_information']  = 'Information';
$_['text_service']      = 'Customer Service';
$_['text_extra']        = 'Extras';
$_['text_return']       = 'Returns';
$_['text_sitemap']      = 'Site Map';
$_['text_manufacturer'] = 'Brands';
$_['text_voucher']      = 'Gift Vouchers';
$_['text_affiliate']    = 'Affiliates';
$_['text_special']      = 'Special Offers';
$_['text_account']      = 'My Account';
$_['text_order']        = 'Order History';
$_['text_wishlist']     = 'Wish List';
$_['text_newsletter']   = 'Newsletter';
$_['text_sell-with-us'] = 'Sell With US';
$_['text_powered']      = '&copy; %s %s';
$_['text_careeb']       = 'Qareeb Technologies LLC All Rights Reserved';
$_['text_join_us'] = 'Join us';
$_['text_become_a_shopper'] = 'Become a Shopper';
$_['text_sell_with_us'] = 'Sell With US';

$_['text_technologies'] = '© 2019 Qareeb Technologies FZ - LLC';
$_['text_download_app'] = 'Download the app';
$_['text_stay_connected'] = 'Stay connected';

$_['text_location_disable'] 	 = 'Sorry! Your current location is disabled';
$_['text_city'] 		 = 'Select City';
$_['text_area'] 		 = 'Select Area';
$_['text_current_city'] = 'Current City: ';
$_['text_current_area'] = 'Current Area: ';

$_['text_shop']      = 'Shop';
$_['text_about']     = 'About';
$_['text_careers']     = 'Careers';
$_['text_contact']      = 'Contact';

$_['txt_contact_us'] = 'Connect with us!';
$_['text_press']     = 'Press';
$_['text_faq']     = 'FAQ';
$_['text_terms']      = 'Terms & Conditions';
$_['text_return']       = 'Returns';

$_['text_register_store']       = 'Register Your Store';
$_['text_register_why']       = 'Why Register';
$_['text_register_merchant']       = 'Merchant Signin';