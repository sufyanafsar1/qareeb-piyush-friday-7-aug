<?php
// Text
$_['text_home']          = 'Home';
$_['text_wishlist']      = 'Wish List (%s)';
$_['text_shopping_cart'] = 'Shopping Cart';
$_['text_category']      = 'Categories';
$_['text_account']       = 'My Account';
$_['text_register']      = 'Register';
$_['text_login']         = 'Login';
$_['text_order']         = 'Order History';
$_['text_transaction']   = 'Transactions';
$_['text_download']      = 'Downloads';
$_['text_logout']        = 'Logout';
$_['text_checkout']      = 'Checkout';
$_['text_search']        = 'Search';
$_['text_all']           = 'Show All';
$_['text_account_deatil']= 'Account Details';
$_['text_billing']= 'Billing';
$_['text_store_credit']= 'Store Credit';
$_['text_store']= 'Stores';
$_['txt_community'] = 'COMMUNITIES';
$_['text_find_me'] = 'Find Me';
$_['text_change_location'] = 'Location';

$_['text_close']          = 'Close';
$_['text_store_closed']          = 'Store Closed';
$_['text_today']          = 'Today';
$_['text_tomorrow']          = 'Tomorrow';
$_['text_monday']          = 'Monday';
$_['text_tuesday']          = 'Tuesday';
$_['text_wednesday']          = 'Wednesday';
$_['text_thursday']          = 'Thursday';
$_['text_friday']          = 'Friday';
$_['text_saturday']          = 'Saturday';
$_['text_sunday']          = 'Sunday';
$_['text_current_time']          = 'Current Time';
$_['text_delivery_schedule']          = 'Available Delivery Schedule';
$_['text_delivery_checkout']          = 'You can choose delivery slot upon checkout';
$_['text_delivery_charges']          = 'Delivery Charges';
$_['text_order_limit']          = 'Minimum Order Limit';
$_['text_next_delivery']          = 'Next Delivery';
$_['text_no_delivery']          = 'No Delivery Available';
$_['text_unavailable']          = 'Unavailable';
$_['text_vat_included1'] = 'All prices are inclusive ';
$_['text_vat_included2'] = 'VAT';


