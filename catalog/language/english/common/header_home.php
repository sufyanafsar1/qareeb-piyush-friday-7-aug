<?php
// Text
$_['text_home']          = 'Home';
$_['text_wishlist']      = 'Wish List (%s)';
$_['text_shopping_cart'] = 'Shopping Cart';
$_['text_category']      = 'Categories';
$_['text_account']       = 'My Account';
$_['text_register']      = 'Register';
$_['text_login']         = 'Login';
$_['text_order']         = 'Order History';
$_['text_transaction']   = 'Transactions';
$_['text_download']      = 'Downloads';
$_['text_logout']        = 'Logout';
$_['text_checkout']      = 'Checkout';
$_['text_search']        = 'Search';
$_['text_all']           = 'Show All';
$_['text_account_deatil']= 'Account Details';
$_['text_billing']		 = 'Billing';
$_['text_store_credit']	 = 'Store Credit';
$_['text_how_it_work'] 	 = 'How it Works';
$_['text_visit_careeb']  = 'Visit Qareeb.com';
$_['text_shopping'] 	 = 'Let us shop for you!';
$_['text_grocery'] 		 = 'Order fresh groceries and more with quick delivery!';
$_['text_grocery_mobile'] 		 = 'Order fresh groceries and more </br>with quick delivery!';
$_['text_city'] 		 = 'Select Your City';
$_['text_region'] 		 = 'Select Region';
$_['text_area'] 		 = 'Select Your Area';
//$_['text_find'] 		 = 'Find your favorite store';
$_['text_find'] 		 = 'Find Nearest Store';
$_['text_locate_me'] 	 = 'Locate Me';
$_['text_location_disable'] 	 = 'Sorry! Your current location is disabled';
$_['partner_stores'] 	 = 'Partner Stores';
$_['serving_areas'] 	 = 'Serving Areas';
$_['supports'] 	 = 'Supports';

$_['Dammam']			 = 'Damman';
$_['Jeddah']			 = 'Jeddah';
$_['Riyadh']			 = 'Riyadh';
$_['text_or']			 = 'OR';

$_['Al-Ada'] = 'Al-Ada';
$_['Al-Arifi'] = 'Al-Arifi';
$_['Al-Badia'] = 'Al-Badia';
$_['Al-Murjan'] = 'Al-Murjan';
$_['An-Nahda'] = 'An-Nahda';
$_['As-Safa'] = 'As-Safa';