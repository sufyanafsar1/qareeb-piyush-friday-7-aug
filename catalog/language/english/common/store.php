<?php
$_['text_store']= 'Stores in';
$_['text_change_location']= 'Change your location';
$_['text_store_recommended']= 'Recommended Stores';
$_['text_minimum'] = "Minimum";
$_['text_delivery'] = "Delivery";

$_['error_service'] = "Service Unavailable at";
$_['error_area'] = "Sorry we are not available at your location.";
$_['text_cart_warning']         = 'Your cart items will be removed while changing to other store. Are you sure to move to this store?';