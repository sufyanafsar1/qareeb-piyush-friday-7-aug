<?php
// Heading

$_['heading_title']    = 'Everything you Need<br>Delivered from your Favorite Stores';
$_['firstli']    = 'Easy to access and to use.';
$_['secondi']    = 'Affordable and cost effective.';
$_['thirdli']    = 'No annoying waiting queue.';
$_['forthli']    = 'Weekly discount offers.';
$_['fifthli']    = 'Minimal delivery time.';
$_['sixthli']    = 'Dedicated and skilled shoppers.';
$_['avld_text']    = 'Available on Android & iOS';
$_['download_text']    = 'Download Now';

$_['text_need_assistances']    = 'Need Assistance?';
$_['text_query']       = 'If you have queries or need support with anything,</br> feel free to contact us!';
$_['text_query_mob']       = 'If you have queries or need support with </br>anything, feel free to contact us!';
$_['text_UAE']      = '<b>UAE</b><br>Level 41, Emirates Tower,<br>Sheikh Zayed Road, Dubai, UAE';
$_['text_SA']   = '<b>Saudi Arabia</b><br>The Space, 2nd Floor , Alia Plaza<br>Thumama Rd, Ar Rabi District, Riyadh';
$_['text_tel']     = 'Tel: +966 553044161<br>info@qareeb.com';
$_['text_name']        = 'Name';
$_['text_email']    = 'Email';
$_['text_phone']     = 'Phone';
$_['text_subject']    = 'Subject';
$_['text_mesage']       = 'Your Message';
$_['text_btn_subject']    = 'Submit';

// Error
$_['error_name']     = 'Name must be between 3 and 64 characters!';
$_['error_email']    = 'Email must be required';
$_['error_phone']    = 'Phone must be required!';
$_['error_subject']  = 'Subject must be required!';
$_['error_message']  = 'Message must be required!';
$_['text_success']   = 'Thank You.';
