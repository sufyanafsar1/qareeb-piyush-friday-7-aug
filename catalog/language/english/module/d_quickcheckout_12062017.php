<?php 

$_['title_payment_address']  						= 'Payment details';
$_['description_payment_address']  					= '';
$_['title_shipping_address']  						= 'Delivery details';
$_['description_shipping_address'] 					= '';
$_['title_shipping_method']  						= 'Shipping method';
$_['description_shipping_method'] 					= '';
$_['title_payment_method'] 							= 'Payment method';
$_['description_payment_method'] 					= '';
$_['title_shopping_cart'] 							= 'Shopping cart';
$_['description_shopping_сart'] 					= '';
$_['description_agreement'] 					    = 'BY CLICKING ON NEXT YOU AGREE ON OUR PRIVACY POLICY';

$_['text_bank_transfer'] = "Please transfer the total amount to the following bank account.";
$_['text_bank_details'] 					        = 'Bank Name: National Commercial Bank [Bank Al Ahli] <br/>
 Account Title: أنظمة الاسطول للاتصالات والتقنية المعلومات <br/>
Account Number 27048897000102 <br/>
IBAN No.   SA5510000027048897000102 <br/> <br/> <br/>' ;


$_['text_bank_warning'] = 'Your order will not ship until we receive payment.';



$_['title_delivery'] 							   = 'Delivery Address';
$_['title_payment'] 							   = 'Delivery & Payment';
$_['title_summary'] 							   = 'Summary';

$_['error_field_required'] 							= 'This field is required';

$_['entry_email_confirm'] 							= 'Confirm E-mail';
$_['error_email_confirm'] 							= 'Email confirmation does not match email!';


$_['step_option_guest_desciption'] = 'Use guest checkout to buy without creating an account';

$_['error_step_payment_address_fields_company'] = 'Company name required more the 3 and less the 128 characters';
$_['error_step_shipping_address_fields_company'] = 'Company name required more the 3 and less the 128 characters';

$_['error_step_confirm_fields_comment'] = 'Please fill in the comment to the order';


$_['button_next'] 	 = 'Next';


?>