<?php
// Heading 
$_['heading_title'] 	 = 'keep in touch';

//Fields
$_['entry_email'] 		 = 'Email';
$_['entry_name'] 		 = 'Name';

//Buttons
$_['entry_button'] 		 = 'Subscribe';
$_['entry_unbutton'] 	 = 'Unsubscribe';

//text
$_['text_subscribe_now'] 	 = 'yourname@email.com';
$_['text_subscribe'] 	 = 'Subscribe Here';
$_['text_email'] 	 = 'Put your email address here';

$_['mail_subject']   	 = 'News Letter Subscribe';

//Error
$_['error_invalid']	    	 = "<div class='alert alert-danger'><i class='fa fa-exclamation-circle'></i> Warning: Invalid Email</div> ";

$_['subscribe']	    	 = "<div class='alert alert-success'><i class='fa fa-exclamation-circle'></i> Success: Thank you for subscribing to our newsletter, %s.</div> ";
$_['unsubscribe'] 	     = 'Unsubscribed Successfully';
$_['alreadyexist'] 	     = 'Already Exist';
$_['notexist'] 	    	 = 'Email Id not exist';
$_['text_join_our_mailing'] = 'Join our mailing list.';
$_['text_never_miss'] 	   = 'Never miss an update!';



?>