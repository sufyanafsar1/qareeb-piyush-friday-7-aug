<?php
class ModelApiAddress extends Model {
	public function addAddress($data) {
		$this->db->query("INSERT INTO " . DB_PREFIX . "address SET customer_id = '" . (int)$data['customerid'] . "', firstname = '" . $this->db->escape($data['firstname']) . "', lastname = '" . $this->db->escape($data['lastname']) . "', company = '" . $this->db->escape($data['company']) . "', address_1 = '" . $this->db->escape($data['address']) . "', address_2 = '" . $this->db->escape($data['address_2']) . "', postcode = '" . $this->db->escape($data['postcode']) . "', city = '" . $this->db->escape($data['city']) . "', fk_region_id = '" . (int)$this->db->escape($data['region_id']) . "', fk_city_id = '" . (int)$this->db->escape($data['city_id']) . "', zone_id = '" . (int)$data['zone_id'] . "', country_id = '" . (int)$data['country_id'] . "'");

		$address_id = $this->db->getLastId();
		return $address_id;
	}
	
	public function getCustomerCityApi($language_id) {
		
		$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "zone WHERE status = '1'");

		$output = array();
		
		foreach ($query->rows as $result) {
			
			if($language_id == 1){
				$name = $result['name'];
			} else {
				$name = $result['name_ar'];
			}
			
			$output[] = array(
				'city_id'       => $result['zone_id'],
				'name'          => $name,				
				'status'        => $result['status']				
			);		
		}
		
		return $output;	
	}
	
	public function getCustomerCityIos() {
		
		$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "zone WHERE status = '1'");

		$output = array();
		
		foreach ($query->rows as $result) {
			
			$output[] = array(
				'city_id'       => $result['zone_id'],
				'name'          => $result['name'],
				'name_ar'       => $result['name_ar'],
				'status'        => $result['status']				
			);		
		}
		
		return $output;	
	}
}