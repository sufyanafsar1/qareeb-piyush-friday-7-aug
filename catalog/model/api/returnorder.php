<?php
class ModelApiReturnorder extends Model {
	public function addReturn($data) {

		$this->db->query("INSERT INTO `" . DB_PREFIX . "return` SET order_id = '" . (int)$data['order_id'] . "', customer_id = '" . (int)$data['customer']['customer_id'] . "' , product_id = '" . $this->db->escape($data['product_id']) . "', firstname = '" . $this->db->escape($data['customer']['firstname']) . "', lastname = '" . $this->db->escape($data['customer']['lastname']) . "', email = '" . $this->db->escape($data['customer']['email']) . "', telephone = '" . $this->db->escape($data['customer']['telephone']) . "', product = '" . $this->db->escape($data['product']['name']) . "', model = '" . $this->db->escape($data['product']['model']) . "', quantity = ".(int)$data['order_quantity'].", opened = 'no', return_reason_id = " . (int)$data['reason_id'] . ", return_status_id = '" . (int)$this->config->get('config_return_status_id') . "', comment = '" . $this->db->escape($data['comment']) . "', date_ordered = '" . $this->db->escape($data['date_ordered']) . "', date_added = NOW(), date_modified = NOW()");

		$return_id = $this->db->getLastId();

		return $return_id;
	}
}
?>