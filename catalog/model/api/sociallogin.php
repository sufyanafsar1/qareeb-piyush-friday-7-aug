<?php 
class ModelApiSocialLogin extends Model {
	public function getCustomerByEmail($email) {
        $result = $this->db->query("SELECT customer_id FROM " . DB_PREFIX . "customer WHERE LOWER(email) = '" . $this->db->escape(utf8_strtolower($email)) . "' LIMIT 1");

        if ($result->num_rows) {
            return (int) $result->row['customer_id'];
        } else {
            return false;
        }
    }

    public function addCustomer($data) {
    	
    	$this->event->trigger('pre.customer.add', $data);
    	$name = explode(' ', $data['name']);
    	$firstname = $name[0];
    	unset($name[0]);
    	$lastname = implode(' ', $name);
        $this->db->query("INSERT INTO " . DB_PREFIX . "customer SET 
            store_id = '" . (int)$this->config->get('config_store_id') . "', 
            firstname = '" . $this->db->escape($firstname) . "', 
            lastname = '" . $this->db->escape($lastname) . "', 
            email = '" . $this->db->escape($data['email']) . "', 
            password = '" . $this->db->escape(md5($data['password'])) . "', 
            status = '1', 
            date_added = NOW()");

        $customer_id = $this->db->getLastId();

        if (!$this->config->get('config_customer_approval')) {
            $this->db->query("UPDATE " . DB_PREFIX . "customer SET 
                approved = '1' 
                WHERE customer_id = '" . (int)$customer_id . "'");
        }
		$this->load->language('mail/customer');

		$this->event->trigger('post.customer.add', $customer_id);

        return $customer_id;
    }
}