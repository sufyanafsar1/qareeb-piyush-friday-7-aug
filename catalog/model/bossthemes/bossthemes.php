<?php
class ModelBossThemesBossThemes extends Model {
	public function getExtensions($type) {
		$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "extension WHERE `type` = '" . $this->db->escape($type) . "'");

		return $query->rows;
	}
	public function getAllModules() {
		$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "layout_module ORDER BY sort_order");
		
		return $query->rows;
	}
	public function getLayoutModulesByPosition($position) {
		$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "layout_module WHERE position = '" . $this->db->escape($position) . "' ORDER BY sort_order");
		
		return $query->rows;
	}
	public function getLayoutModulesByCode($code) {
		$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "layout_module WHERE code = '" . $this->db->escape($code) . "' ORDER BY sort_order");
		
		return $query->rows;
	}
	public function getLayoutModulesByLayoutId($layout_id, $position) {
		$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "layout_module WHERE (layout_id = '" . (int)$layout_id . "' AND position = '" . $this->db->escape($position) . "') OR (layout_id=9999 AND position = '" . $this->db->escape($position) . "') ORDER BY sort_order");
		
		return $query->rows;
	}
	
}
?>