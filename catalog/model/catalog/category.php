<?php

class ModelCatalogCategory extends Model {

    public function getCategory($category_id) {
        $query = $this->db->query("SELECT DISTINCT * FROM " . DB_PREFIX . "category c LEFT JOIN " . DB_PREFIX . "category_description cd ON (c.category_id = cd.category_id) LEFT JOIN " . DB_PREFIX . "category_to_store c2s ON (c.category_id = c2s.category_id) WHERE c.category_id = '" . (int) $category_id . "' AND cd.language_id = '" . (int) $this->config->get('config_language_id') . "' AND c2s.store_id = '" . (int) $this->config->get('config_store_id') . "' AND c.status = '1'");

        return $query->row;
    }

    public function getCategories($parent_id = 0) {
    
		if(file_exists($_SERVER['DOCUMENT_ROOT'] . "/development/jsonData/catagory.json")){
                
			$data = json_decode(file_get_contents($_SERVER['DOCUMENT_ROOT'] . "/development/jsonData/catagory.json"), true);
			$newArray = array();
			
			for($i=0;$i<count($data);$i++){
				if($data[$i]['parent_id']==(int)$parent_id){		
					array_push($newArray,$data[$i]);
				}
			}
			
			return $newArray;

		}else{

			$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "category c LEFT JOIN " . DB_PREFIX . "category_description cd ON (c.category_id = cd.category_id) LEFT JOIN " . DB_PREFIX . "category_to_store c2s ON (c.category_id = c2s.category_id) WHERE c.parent_id = '" . (int) $parent_id . "' AND cd.language_id = '" . (int) $this->config->get('config_language_id') . "' AND c2s.store_id = '" . (int) $this->config->get('config_store_id') . "'  AND c.status = '1' ORDER BY c.sort_order, LCASE(cd.name)");

			$Newquery = $this->db->query("SELECT * FROM " . DB_PREFIX . "category c LEFT JOIN " . DB_PREFIX . "category_description cd ON (c.category_id = cd.category_id) LEFT JOIN " . DB_PREFIX . "category_to_store c2s ON (c.category_id = c2s.category_id) WHERE cd.language_id = '" . (int) $this->config->get('config_language_id') . "' AND c2s.store_id = '" . (int) $this->config->get('config_store_id') . "'  AND c.status = '1' ORDER BY c.sort_order, LCASE(cd.name)");
			
			file_put_contents($_SERVER['DOCUMENT_ROOT'] . "/development/jsonData/catagory.json", json_encode($Newquery->rows));
			return $query->rows;
		} 
		/* 
       $query = $this->db->query("SELECT * FROM " . DB_PREFIX . "category c LEFT JOIN " . DB_PREFIX . "category_description cd ON (c.category_id = cd.category_id) LEFT JOIN " . DB_PREFIX . "category_to_store c2s ON (c.category_id = c2s.category_id) WHERE c.parent_id = '" . (int) $parent_id . "' AND cd.language_id = '" . (int) $this->config->get('config_language_id') . "' AND c2s.store_id = '" . (int) $this->config->get('config_store_id') . "'  AND c.status = '1' ORDER BY c.sort_order, LCASE(cd.name)");
        return $query->rows;  */
    }

    public function getCategoriess($languageid) {
        $query = $this->db->query("SELECT * FROM " . DB_PREFIX . "category c LEFT JOIN " . DB_PREFIX . "category_description cd ON (c.category_id = cd.category_id) LEFT JOIN " . DB_PREFIX . "category_to_store c2s ON (c.category_id = c2s.category_id) WHERE c.parent_id = '0' AND cd.language_id = '" . (int) $languageid . "' AND c2s.store_id = '" . (int) $this->config->get('config_store_id') . "'  AND c.status = '1' ORDER BY c.sort_order, LCASE(cd.name)");

        return $query->rows;
    }

    public function getCategoriesss($parent_id = 0, $languageid) {
        $query = $this->db->query("SELECT * FROM " . DB_PREFIX . "category c LEFT JOIN " . DB_PREFIX . "category_description cd ON (c.category_id = cd.category_id) LEFT JOIN " . DB_PREFIX . "category_to_store c2s ON (c.category_id = c2s.category_id) WHERE c.parent_id = '" . (int) $parent_id . "' AND cd.language_id = '" . (int) $languageid . "' AND c2s.store_id = '" . (int) $this->config->get('config_store_id') . "'  AND c.status = '1' ORDER BY c.sort_order, LCASE(cd.name)");

        return $query->rows;
    }

    public function getCategoryFilters($category_id) {
        $implode = array();

        $query = $this->db->query("SELECT filter_id FROM " . DB_PREFIX . "category_filter WHERE category_id = '" . (int) $category_id . "'");

        foreach ($query->rows as $result) {
            $implode[] = (int) $result['filter_id'];
        }

        $filter_group_data = array();

        if ($implode) {
            $filter_group_query = $this->db->query("SELECT DISTINCT f.filter_group_id, fgd.name, fg.sort_order FROM " . DB_PREFIX . "filter f LEFT JOIN " . DB_PREFIX . "filter_group fg ON (f.filter_group_id = fg.filter_group_id) LEFT JOIN " . DB_PREFIX . "filter_group_description fgd ON (fg.filter_group_id = fgd.filter_group_id) WHERE f.filter_id IN (" . implode(',', $implode) . ") AND fgd.language_id = '" . (int) $this->config->get('config_language_id') . "' GROUP BY f.filter_group_id ORDER BY fg.sort_order, LCASE(fgd.name)");

            foreach ($filter_group_query->rows as $filter_group) {
                $filter_data = array();

                $filter_query = $this->db->query("SELECT DISTINCT f.filter_id, fd.name FROM " . DB_PREFIX . "filter f LEFT JOIN " . DB_PREFIX . "filter_description fd ON (f.filter_id = fd.filter_id) WHERE f.filter_id IN (" . implode(',', $implode) . ") AND f.filter_group_id = '" . (int) $filter_group['filter_group_id'] . "' AND fd.language_id = '" . (int) $this->config->get('config_language_id') . "' ORDER BY f.sort_order, LCASE(fd.name)");

                foreach ($filter_query->rows as $filter) {
                    $filter_data[] = array(
                        'filter_id' => $filter['filter_id'],
                        'name' => $filter['name']
                    );
                }

                if ($filter_data) {
                    $filter_group_data[] = array(
                        'filter_group_id' => $filter_group['filter_group_id'],
                        'name' => $filter_group['name'],
                        'filter' => $filter_data
                    );
                }
            }
        }

        return $filter_group_data;
    }

    public function getCategoryLayoutId($category_id) {
        $query = $this->db->query("SELECT * FROM " . DB_PREFIX . "category_to_layout WHERE category_id = '" . (int) $category_id . "' AND store_id = '" . (int) $this->config->get('config_store_id') . "'");

        if ($query->num_rows) {
            return $query->row['layout_id'];
        } else {
            return 0;
        }
    }

    public function getTotalCategoriesByCategoryId($parent_id = 0) {
        $query = $this->db->query("SELECT COUNT(*) AS total FROM " . DB_PREFIX . "category c LEFT JOIN " . DB_PREFIX . "category_to_store c2s ON (c.category_id = c2s.category_id) WHERE c.parent_id = '" . (int) $parent_id . "' AND c2s.store_id = '" . (int) $this->config->get('config_store_id') . "' AND c.status = '1'");

        return $query->row['total'];
    }

    public function getLimitedCategories($parent_id = 0, $limit = 0, $lastCategoryId = 0) {
        if ($lastCategoryId != 0) {
            $query = $this->db->query("SELECT * FROM " . DB_PREFIX . "category c LEFT JOIN " . DB_PREFIX . "category_description cd ON (c.category_id = cd.category_id) LEFT JOIN " . DB_PREFIX . "category_to_store c2s ON (c.category_id = c2s.category_id) WHERE c.parent_id = '" . (int) $parent_id . "' AND c.category_id < '" . (int) $lastCategoryId . "' AND cd.language_id = '" . (int) $this->config->get('config_language_id') . "' AND c2s.store_id = '" . (int) $this->config->get('config_store_id') . "'  AND c.status = '1' ORDER BY c.category_id DESC LIMIT " . (int) $limit);
        } else {
            $query = $this->db->query("SELECT * FROM " . DB_PREFIX . "category c LEFT JOIN " . DB_PREFIX . "category_description cd ON (c.category_id = cd.category_id) LEFT JOIN " . DB_PREFIX . "category_to_store c2s ON (c.category_id = c2s.category_id) WHERE c.parent_id = '" . (int) $parent_id . "' AND cd.language_id = '" . (int) $this->config->get('config_language_id') . "' AND c2s.store_id = '" . (int) $this->config->get('config_store_id') . "'  AND c.status = '1' ORDER BY c.category_id DESC LIMIT " . (int) $limit);
        }

        return $query->rows;
    }

    public function getRemainingCategories($parent_id = 0, $lastCategoryId = 0) {

        $query = $this->db->query("SELECT * FROM " . DB_PREFIX . "category c LEFT JOIN " . DB_PREFIX . "category_description cd ON (c.category_id = cd.category_id) LEFT JOIN " . DB_PREFIX . "category_to_store c2s ON (c.category_id = c2s.category_id) WHERE c.parent_id = '" . (int) $parent_id . "' AND c.category_id < '" . (int) $lastCategoryId . "' AND cd.language_id = '" . (int) $this->config->get('config_language_id') . "' AND c2s.store_id = '" . (int) $this->config->get('config_store_id') . "'  AND c.status = '1' ORDER BY c.category_id DESC");

        return $query->rows;
    }

    public function getSellerCategories($parent_id = 0, $seller) {


        $sql = "SELECT c.category_id,c.image,cd.name,cd.description FROM " . DB_PREFIX . "category c LEFT JOIN " . DB_PREFIX . "category_description cd ON (c.category_id = cd.category_id)   
                    WHERE c.category_id IN(SELECT DISTINCT(opc.category_id) FROM " . DB_PREFIX . "product op INNER JOIN " . DB_PREFIX . "product_to_category opc ON op.product_id = opc.product_id  WHERE seller_id=" . $seller . " )
                    AND c.parent_id = '" . $parent_id . "' AND cd.language_id = '" . (int) $this->config->get('config_language_id') . "'   AND c.status = '1' ORDER BY c.sort_order, LCASE(cd.name)";

        $query = $this->db->query($sql);
        return $query->rows;
    }
public function getStoreCategories($seller,$language) {

        $sql = "SELECT c.category_id,c.image,cd.name,cd.description FROM " . DB_PREFIX . "category c LEFT JOIN " . DB_PREFIX . "category_description cd ON (c.category_id = cd.category_id)   
                    WHERE c.category_id IN(SELECT DISTINCT(opc.category_id) FROM " . DB_PREFIX . "product op INNER JOIN " . DB_PREFIX . "product_to_category opc ON op.product_id = opc.product_id  WHERE seller_id=" . $seller . " )
                     AND c.parent_id = '0' AND cd.language_id = '" . (int) $language . "'   AND c.status = '1' ORDER BY c.sort_order, LCASE(cd.name)";


        $query = $this->db->query($sql);
        return $query->rows;
    }
    public function getSellerCategoriesAreaWise($parent_id = 0, $seller, $area_id) {
        $sql = "SELECT c.category_id,c.image,cd.name,cd.description FROM " . DB_PREFIX . "category c LEFT JOIN " . DB_PREFIX . "category_description cd ON (c.category_id = cd.category_id)   
                    WHERE c.category_id IN(SELECT DISTINCT(sp.category_id) FROM " . DB_PREFIX . "saddress sa LEFT JOIN " . DB_PREFIX . "sellercategories sp ON (sa.seller_id=sp.seller_id)". " WHERE sa.seller_id=" . $seller . " AND sa.area_id=" . $area_id . "  ) AND c.parent_id = '" . $parent_id . "' AND cd.language_id = '" . (int) $this->config->get('config_language_id') . "'   AND c.status = '1' ORDER BY c.sort_order, LCASE(cd.name)";
	
        $query = $this->db->query($sql);
        return $query->rows;
    }

    public function getSellerCategoriesRadiusWise($parent_id = 0, $seller, $radiusKm, $lat, $lng) {
        //$sqlRadius = "SELECT DISTINCT(a.category_id) FROM( SELECT p2c.category_id, (6371 * acos( cos( radians(" . $lat . ") ) * cos( radians( sa.latitude ) ) * cos( radians( sa.longitude ) - radians(" . $lng . ") ) + sin( radians(" . $lat . ") ) * sin( radians(sa.latitude ) ) ) ) AS distance FROM  " . DB_PREFIX . "saddress sa LEFT JOIN " . DB_PREFIX . "sellers_products sp ON (sa.seller_id = sp.seller_id)". "LEFT JOIN " . DB_PREFIX . "product_to_category p2c ON (p2c.product_id = sp.product_id)". "WHERE sa.seller_id=" . $seller . " HAVING distance < " . $radiusKm . ") a";
				
		$sqlRadius = "SELECT DISTINCT(a.category_id) FROM( SELECT sp.category_id, (6371 * acos( cos( radians(" . $lat . ") ) * cos( radians( sa.latitude ) ) * cos( radians( sa.longitude ) - radians(" . $lng . ") ) + sin( radians(" . $lat . ") ) * sin( radians(sa.latitude ) ) ) ) AS distance FROM  " . DB_PREFIX . "saddress sa LEFT JOIN " . DB_PREFIX . "sellercategories sp ON (sa.seller_id = sp.seller_id)". "WHERE sa.seller_id=" . $seller . " HAVING distance < " . $radiusKm . ") a";

        $sql = "SELECT c.category_id,c.image,cd.name,cd.description FROM " . DB_PREFIX . "category c LEFT JOIN " . DB_PREFIX . "category_description cd ON (c.category_id = cd.category_id)
                    WHERE c.category_id IN(" . $sqlRadius . ")
                    AND c.parent_id = '" . $parent_id . "' AND cd.language_id = '" . (int) $this->config->get('config_language_id') . "'   AND c.status = '1' ORDER BY c.sort_order, LCASE(cd.name)";

        $query = $this->db->query($sql);
        return $query->rows;
    }
    
    public function getSellerCategoriesAreaWiseV2($parent_id = 0, $seller, $area_id,$language_id) {
		$sql = "SELECT c.category_id,c.image,cd.name,cd.description,(Select COUNT(m.category_id) FROM oc_category m WHERE m.parent_id = c.category_id) AS total_child FROM " . DB_PREFIX . "category c LEFT JOIN " . DB_PREFIX . "category_description cd ON (c.category_id = cd.category_id)
		WHERE c.category_id IN(SELECT DISTINCT(sp.category_id) 
		FROM " . DB_PREFIX . "saddress sa 
		LEFT JOIN " . DB_PREFIX . "sellercategories sp ON (sa.seller_id=sp.seller_id)" . " 
		WHERE sa.seller_id=" . $seller . " 
		AND sa.area_id=" . $area_id . " ) 
		AND c.parent_id = '" . $parent_id . "' 
		AND cd.language_id = '" . (int) $language_id . "'   
		AND c.status = '1' ORDER BY c.sort_order, LCASE(cd.name)";
	
		$query = $this->db->query($sql);
		return $query->rows;
	}
	
	public function getSellerCategoriesRadiusWiseV2($parent_id = 0, $seller, $radiusKm, $lat, $lng,$language_id) {
		//$sqlRadius = "SELECT DISTINCT(a.category_id) FROM( SELECT p2c.category_id, (6371 * acos( cos( radians(" . $lat . ") ) * cos( radians( sa.latitude ) ) * cos( radians( sa.longitude ) - radians(" . $lng . ") ) + sin( radians(" . $lat . ") ) * sin( radians(sa.latitude ) ) ) ) AS distance FROM  " . DB_PREFIX . "saddress sa LEFT JOIN " . DB_PREFIX . "sellers_products sp ON (sa.seller_id = sp.seller_id)". "LEFT JOIN " . DB_PREFIX . "product_to_category p2c ON (p2c.product_id = sp.product_id)". "WHERE sa.seller_id=" . $seller . " HAVING distance < " . $radiusKm . ") a";
					 
		$sqlRadius = "SELECT DISTINCT(a.category_id) FROM( SELECT sp.category_id, (6371 * acos( cos( radians(" . $lat . ") ) * cos( radians( sa.latitude ) ) * cos( radians( sa.longitude ) - radians(" . $lng . ") ) + sin( radians(" . $lat . ") ) * sin( radians(sa.latitude ) ) ) ) AS distance FROM  " . DB_PREFIX . "saddress sa LEFT JOIN " . DB_PREFIX . "sellercategories sp ON (sa.seller_id = sp.seller_id)". "WHERE sa.seller_id=" . $seller . " HAVING distance < " . $radiusKm . ") a";

		$sql = "SELECT c.category_id,c.image,cd.name,cd.description ,(Select COUNT(m.category_id) FROM oc_category m WHERE m.parent_id = c.category_id) AS total_child FROM " . DB_PREFIX . "category c LEFT JOIN " . DB_PREFIX . "category_description cd ON (c.category_id = cd.category_id)
                    WHERE c.category_id IN(" . $sqlRadius . ")
                    AND c.parent_id = '" . $parent_id . "' AND cd.language_id = '" . (int) $language_id . "'   AND c.status = '1' ORDER BY c.sort_order, LCASE(cd.name)";

		$query = $this->db->query($sql);
		return $query->rows;
	}

    public function getAreaWiseCategories($parent_id = 0, $area_id) {
        $areaSql = " SELECT DISTINCT(p2c.category_id) FROM " . DB_PREFIX . "sellers s JOIN " . DB_PREFIX . "saddress a ON s.seller_id = a.seller_id Left JOIN " . DB_PREFIX . "sellers_products sp ON (sp.seller_id=a.seller_id) LEFT JOIN " . DB_PREFIX . "product_to_category p2c ON (p2c.product_id = sp.product_id) WHERE a.area_id = '" . (int) $area_id . "'";

        $sql = "SELECT c.category_id,c.image,cd.name,cd.description FROM " . DB_PREFIX . "category c LEFT JOIN " . DB_PREFIX . "category_description cd ON (c.category_id = cd.category_id)   
                    WHERE c.category_id IN(" . $areaSql . ")
                    AND c.parent_id = '" . $parent_id . "' AND cd.language_id = '" . (int) $this->config->get('config_language_id') . "'   AND c.status = '1' ORDER BY c.sort_order, LCASE(cd.name)";

        $query = $this->db->query($sql);
        return $query->rows;
    }

    public function getRadiusWiseCategories($parent_id = 0, $radiusKm, $lat, $lng) {
        $sqlRadius = "SELECT DISTINCT(a.category_id) FROM( SELECT p2c.category_id, (6371 * acos( cos( radians(" . $lat . ") ) * cos( radians( sa.latitude ) ) * cos( radians( sa.longitude ) - radians(" . $lng . ") ) + sin( radians(" . $lat . ") ) * sin( radians(sa.latitude ) ) ) ) AS distance FROM " . DB_PREFIX . "sellers s JOIN " . DB_PREFIX . "saddress sa ON s.seller_id = sa.seller_id LEFT JOIN " . DB_PREFIX . "sellers_products sp ON (s.seller_id = sp.seller_id) LEFT JOIN " . DB_PREFIX . "product_to_category p2c ON (p2c.product_id = sp.product_id) HAVING distance < " . $radiusKm . ") a";

        $sql = "SELECT c.category_id,c.image,cd.name,cd.description FROM " . DB_PREFIX . "category c LEFT JOIN " . DB_PREFIX . "category_description cd ON (c.category_id = cd.category_id)   
                    WHERE c.category_id IN(" . $sqlRadius . ")
                    AND c.parent_id = '" . $parent_id . "' AND cd.language_id = '" . (int) $this->config->get('config_language_id') . "'   AND c.status = '1' ORDER BY c.sort_order, LCASE(cd.name)";

        $query = $this->db->query($sql);
        return $query->rows;
    }

    public function getSellerAreaWiseCategories($parent_id = 0, $area_id, $seller_id) {

        //$areaSql =  " SELECT DISTINCT(p2c.category_id) FROM " . DB_PREFIX . "sellers s JOIN " . DB_PREFIX . "saddress a ON s.seller_id = a.seller_id Left JOIN " . DB_PREFIX . "sellers_products sp ON (sp.seller_id=a.seller_id) LEFT JOIN " . DB_PREFIX . "product_to_category p2c ON (p2c.product_id = sp.product_id) WHERE a.area_id = '".(int)$area_id."' AND a.seller_id='".(int)$seller_id."'";
        
        //Left JOIN " . DB_PREFIX . "sellers_products sp ON (sp.seller_id=a.seller_id) 
        //WHERE a.area_id = '" . (int) $area_id . "' 
        //var_dump($parent_id);exit;
        $areaSql = " SELECT count(sc.id) as product_count, sc.category_id , c.parent_id,c.image,cd.name,cd.description 
        FROM " . DB_PREFIX . "sellers s 
        JOIN " . DB_PREFIX . "saddress a ON s.seller_id = a.seller_id AND a.area_id = '" . (int) $area_id . "' 
        JOIN " . DB_PREFIX . "sellercategories sc ON (sc.seller_id = s.seller_id) 
        JOIN " . DB_PREFIX . "category c ON (c.category_id = sc.category_id) 
		JOIN " . DB_PREFIX . "product p ON (p.product_id = sc.product_id)
        JOIN " . DB_PREFIX . "category_description cd ON (c.category_id = cd.category_id)   
        WHERE a.seller_id='" . (int) $seller_id . "'
        AND c.parent_id = '" . $parent_id . "'
        AND cd.language_id = '" . (int) $this->config->get('config_language_id') . "'   
        AND c.status = '1'
		AND p.status = '1'
        group by sc.category_id
         ORDER BY c.sort_order, LCASE(cd.name)";//echo $areaSql;exit;
         
         
        $query = $this->db->query($areaSql);
       
      /* {
	   	 $areaSql = " SELECT sc.category_id , c.parent_id,c.image,cd.name,cd.description 
        FROM " . DB_PREFIX . "sellers s 
        JOIN " . DB_PREFIX . "saddress a ON s.seller_id = a.seller_id AND a.area_id = '" . (int) $area_id . "' 
        JOIN " . DB_PREFIX . "sellercategories sc ON (sc.seller_id = s.seller_id) 
        JOIN " . DB_PREFIX . "category c ON (c.category_id = sc.category_id) 
        JOIN " . DB_PREFIX . "category_description cd ON (c.category_id = cd.category_id)   
        WHERE a.seller_id='" . (int) $seller_id . "'
        AND c.parent_id = '" . $parent_id . "'
        AND cd.language_id = '" . (int) $this->config->get('config_language_id') . "'   
        AND c.status = '1' ORDER BY c.sort_order, LCASE(cd.name)";
        $query = $this->db->query($areaSql);
	   }*/
       
//var_dump($query->rows);exit;

        /*$sql = "SELECT c.category_id,c.image,cd.name,cd.description 
        FROM " . DB_PREFIX . "category c 
        LEFT JOIN " . DB_PREFIX . "category_description cd ON (c.category_id = cd.category_id)   
        WHERE c.category_id IN(" . $areaSql . ")
        AND c.parent_id = '" . $parent_id . "' 
        AND cd.language_id = '" . (int) $this->config->get('config_language_id') . "'   
        AND c.status = '1' ORDER BY c.sort_order, LCASE(cd.name)";*/

        //$query = $this->db->query($sql);
        return $query->rows;
    }

    public function getSellerRadiusWiseCategories($parent_id = 0, $radiusKm, $lat, $lng, $seller_id) {
        //$sqlRadius = "SELECT DISTINCT(a.category_id) FROM( SELECT p2c.category_id, (6371 * acos( cos( radians(".$lat.") ) * cos( radians( sa.latitude ) ) * cos( radians( sa.longitude ) - radians(".$lng.") ) + sin( radians(".$lat.") ) * sin( radians(sa.latitude ) ) ) ) AS distance FROM " . DB_PREFIX . "sellers s JOIN " . DB_PREFIX . "saddress sa ON s.seller_id = sa.seller_id LEFT JOIN " . DB_PREFIX . "sellers_products sp ON (s.seller_id = sp.seller_id) LEFT JOIN " . DB_PREFIX . "product_to_category p2c ON (p2c.product_id = sp.product_id) WHERE s.seller_id=".$seller_id." HAVING distance < " . $radiusKm . ") a";
        $sqlRadius = "SELECT DISTINCT(a.category_id) FROM( SELECT sc.category_id, (6371 * acos( cos( radians(" . $lat . ") ) * cos( radians( sa.latitude ) ) * cos( radians( sa.longitude ) - radians(" . $lng . ") ) + sin( radians(" . $lat . ") ) * sin( radians(sa.latitude ) ) ) ) AS distance FROM " . DB_PREFIX . "sellers s JOIN " . DB_PREFIX . "saddress sa ON s.seller_id = sa.seller_id LEFT JOIN " . DB_PREFIX . "sellers_products sp ON (s.seller_id = sp.seller_id) LEFT JOIN " . DB_PREFIX . "sellercategories sc ON (sc.product_id = sp.product_id) WHERE s.seller_id=" . $seller_id . " HAVING distance < " . $radiusKm . ") a";

        $sql = "SELECT c.category_id,c.image,cd.name,cd.description FROM " . DB_PREFIX . "category c LEFT JOIN " . DB_PREFIX . "category_description cd ON (c.category_id = cd.category_id)   
                    WHERE c.category_id IN(" . $sqlRadius . ")
                    AND c.parent_id = '" . $parent_id . "' AND cd.language_id = '" . (int) $this->config->get('config_language_id') . "'   AND c.status = '1' ORDER BY c.sort_order, LCASE(cd.name)";

        $query = $this->db->query($sql);


        return $query->rows;
    }

	public function getAreaS($seller_id) {
        $query = $this->db->query("SELECT city_id, area_id FROM " . DB_PREFIX . "saddress WHERE seller_id = '" . (int) $seller_id . "'");

        return $query->row;
    }

	public function getArea($area_id) {
		
		$languageId = (int)$this->config->get('config_language_id');
		
		if ($languageId == 1) {
			$query = $this->db->query("SELECT name FROM " . DB_PREFIX . "area WHERE area_id = '" . (int) $area_id . "'");
        } else {
            $query = $this->db->query("SELECT name_ar as name FROM " . DB_PREFIX . "area WHERE area_id = '" . (int) $area_id . "'");
        }
		 
		if ($query->num_rows) {
            return $query->row['name'];
        } else {
            return '';
        } 
    }
	
	public function getCity($city_id) {
        $query = $this->db->query("SELECT name FROM " . DB_PREFIX . "city WHERE city_id = '" . (int) $city_id . "'");
		
		if ($query->num_rows) {
            return $query->row['name'];
        } else {
            return '';
        }        
    }
	
	public function getSellerCategoriesProducts($seller_id, $category_id) {
        $sql = "SELECT COUNT(sc.product_id) as total 
				FROM " . DB_PREFIX . "category c
				INNER JOIN " . DB_PREFIX . "sellercategories sc ON (c.category_id = sc.category_id)
				INNER JOIN " . DB_PREFIX . "product p ON (sc.product_id = p.product_id)   
				INNER JOIN " . DB_PREFIX . "sellers_products sp ON (sc.product_id = sp.product_id)   
				WHERE p.status = '1' AND c.status = '1' AND sc.seller_id = '".$seller_id."' AND c.category_id = '".$category_id."' AND c.approve = '1' AND sp.seller_id = '".$seller_id."'";
				
        $query = $this->db->query($sql);
		return $query->row['total'];
    }
}
