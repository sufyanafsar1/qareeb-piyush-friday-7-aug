<?php

class ModelLocalisationArea extends Model {

    public function getArea($area_id) {
        $languageId = (int) $this->config->get('config_language_id');
        if ($languageId == 1) {
            $sql = "SELECT area_id,city_id,name FROM " . DB_PREFIX . "area WHERE area_id = '" . (int) $area_id . "'";
        } else {
            $sql = "SELECT area_id,city_id,name_ar as name FROM " . DB_PREFIX . "area WHERE area_id = '" . (int) $area_id . "'";
        }

        $query = $this->db->query($sql);
        return $query->row;
    }

    public function getAreas($data = array()) {
        $sql = "SELECT *, a.name, a.code, c.name AS city FROM " . DB_PREFIX . "area a LEFT JOIN " . DB_PREFIX . "city c ON (a.city_id = c.city_id)";

        $sort_data = array(
            'c.name',
            'a.name',
            'a.code'
        );

        if (isset($data['sort']) && in_array($data['sort'], $sort_data)) {
            $sql .= " ORDER BY " . $data['sort'];
        } else {
            $sql .= " ORDER BY c.name";
        }

        if (isset($data['order']) && ($data['order'] == 'DESC')) {
            $sql .= " DESC";
        } else {
            $sql .= " ASC";
        }

        if (isset($data['start']) || isset($data['limit'])) {
            if ($data['start'] < 0) {
                $data['start'] = 0;
            }

            if ($data['limit'] < 1) {
                $data['limit'] = 20;
            }

            $sql .= " LIMIT " . (int) $data['start'] . "," . (int) $data['limit'];
        }

        $query = $this->db->query($sql);

        return $query->rows;
    }

    public function getAreasByArrayId($data = array()) {
        $comma_separated = implode(",", $data);

        $sql = "SELECT *, a.name, a.code, c.name AS city FROM " . DB_PREFIX . "area a LEFT JOIN " . DB_PREFIX . "city c ON (a.city_id = c.city_id)";
        if ($comma_separated != "") {
            $sql .= " WHERE area_id IN(" . $comma_separated . ")";
        }

        $query = $this->db->query($sql);

        return $query->rows;
    }

    public function getAreasByIds($data = array()) {

        $city_id = $data['city_id'];
        $area_id = $data['area_id'];

        $sql = "SELECT a.name As areaName, c.name AS cityName  FROM oc_area a INNER JOIN oc_city c ON c.city_id = a.city_id";

        $sql .= " WHERE a.area_id = " . $area_id;

        $query = $this->db->query($sql);

        return $query->row;
    }

    public function getAreasByCityId($city_id) {
        $area_data = $this->cache->get('area.' . (int) $city_id);

        if (!$area_data) {
            $query = $this->db->query("SELECT * FROM " . DB_PREFIX . "area WHERE city_id = '" . (int) $city_id . "' AND status = '1' ORDER BY name");

            $area_data = $query->rows;

            $this->cache->set('area.' . (int) $city_id, $area_data);
        }

        return $area_data;
    }

    public function getAreasByCityIdV2($city_id, $languageId) {

        if ($languageId == 1) {
            $sql = "SELECT area_id,city_id,name,name_ar FROM " . DB_PREFIX . "area WHERE city_id = '" . (int) $city_id . "' AND status = '1' ORDER BY name";
        } else {
            $sql = "SELECT area_id,city_id,name_ar as name FROM " . DB_PREFIX . "area WHERE city_id = '" . (int) $city_id . "' AND status = '1' ORDER BY name";
        }

        $query = $this->db->query($sql);

        $area_data = $query->rows;




        return $area_data;
    }

	public function getLatLngBySellerInfo($lat, $lng) {

		$sql = "SELECT s.seller_id, s.delivery_charges, s.minimum_order, s.image, s.firstname, s.lastname, sa.area_id as areaID, sa.city_id as cityID, c.name as cityName, c.name_ar as cityNameAR, a.name as areaName, a.name_ar as areaNameAR
		FROM " . DB_PREFIX . "saddress sa 
		LEFT JOIN " . DB_PREFIX . "sellers s ON (sa.seller_id = s.seller_id) 
		LEFT JOIN " . DB_PREFIX . "city c ON (sa.city_id = c.city_id) 
		LEFT JOIN " . DB_PREFIX . "area a ON (sa.area_id = a.area_id) 
		WHERE sa.latitude = " .$lat. " AND sa.longitude = ".$lng;
				
        $query = $this->db->query($sql);
		
		if ($query->num_rows) {
			return $query->row;
		} else {
			return false;
		}
    }
	
    public function getTotalAreas() {
        $query = $this->db->query("SELECT COUNT(*) AS total FROM " . DB_PREFIX . "area");

        return $query->row['total'];
    }

    public function getTotalAreasByCityId($city_id) {
        $query = $this->db->query("SELECT COUNT(*) AS total FROM " . DB_PREFIX . "area WHERE city_id = '" . (int) $city_id . "'");

        return $query->row['total'];
    }

}
