<?php
class ModelPaymentStartPayments extends Model {
	public function getMethod($address, $total) {
		$this->language->load('payment/start_payments');

        $method_data = array(
            'code'       => 'start_payments',
            'title'      => $this->language->get('text_title'),
            'terms'      => '',
            'sort_order' => $this->config->get('start_payments_sort_order')
        );

		return $method_data;
	}
}
