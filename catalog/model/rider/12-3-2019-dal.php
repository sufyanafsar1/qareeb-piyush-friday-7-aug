<?php
class ModelRiderDal extends Model {


	public function addRider($data) {

		$riderId =  (int) $data["riderID"];
		//$riderInfo =   $this->db->query("Select * FROM ri_riderinfo WHERE email= '".$data['email']."'");

		if($riderId > 0 )
		{
			$this->db->query("UPDATE ri_riderinfo SET name ='" . $data["name"] . "' , email ='" . $data['email'] . "' , password ='" . $data["password"] . "' , contact_no ='" . $data["contact_no"] . "', address ='" . $data["address"] . "', license ='" . $data["license"] . "' , rider_status ='" . $data["rider_status"] . "' WHERE rider_id ='" . $riderId . "'");
			$rider_id = $riderId;
		}
		else{

			$this->db->query("INSERT INTO ri_riderinfo ( email, name, password, contact_no, address, license, is_active, is_online, rider_status, rider_pic, device_ID) VALUES ('". $data['email']."','". $data['name']."','". $data['password']."','". $data['contact_no']."','". $data['address']."','". $data['license']."','". $data['is_active']."','". $data['is_online']."','". $data['rider_status']."','". $data['rider_pic']."','". $data['device_ID']."')");

			$rider_id = $this->db->getLastId();
		}

		return $rider_id;
	}

	public function deleteRider($data)
	{
		//Delete from riderInfo
		$this->db->query("DELETE FROM ri_riderinfo  WHERE rider_id ='" . $data["UserID"] . "'");

		//Delete from riderInfo Area
		$this->db->query("DELETE FROM ri_riderinfoareas  WHERE rider_id ='" . $data["UserID"] . "'");

		//Delete from rider Availability
		$this->db->query("DELETE FROM ri_rideravailability  WHERE rider_id ='" . $data["UserID"] . "'");
		return "success";
	}

	public function ChangeUserActiveStatus($data) {

		$this->db->query("UPDATE ri_riderinfo SET is_active ='" . $data["status"] . "' WHERE rider_id ='" . $data["UserID"] . "'");
		return "success";
	}
	public function ChangeUserOnlineStatus($data) {

		$this->db->query("UPDATE ri_riderinfo SET is_online ='" . $data["status"] . "' WHERE rider_id ='" . $data["UserID"] . "'");
		return "success";
	}

	public function getPendingMainOrders($data) {
		$query = $this->db->query("SELECT order_id,order_status_id,date_added,total,shipping_address_1,firstname as CustomerName FROM oc_order WHERE order_status_id=1");
		$res=$query->rows;

		return $res;
	}


	public function getMainOrderDetailsByOrderID($data) {
		$query1 = $this->db->query("SELECT order_id,order_status_id,date_added,total,shipping_address_1,firstname as customerName from oc_order WHERE order_id='" . $data["orderID"] . "'");
		$res1=$query1->rows;

		$query2 = $this->db->query("SELECT op.order_id,op.product_id,op.`name`,op.quantity,op.price,op.total,p.upc as productBarCode FROM oc_order_product op INNER JOIN oc_product p on op.product_id=p.product_id WHERE op.order_id='". $data["orderID"] . "'");
		$res2=$query2->rows;

		$resCollec[0]=$res1;
		$resCollec[1]=$res2;
		debugging($resCollec);
		return $resCollec;
	}


	public function AcceptOrder($data) {

		$this->db->query("UPDATE ri_riderorder set order_status=2 WHERE Order_id='" . $data["order_id"] . "'");
		$resultone = $this->db->query("SELECT * FROM `ri_riderorder` WHERE rider_id = (SELECT rider_id FROM ri_riderorder where Order_id = '" . $data["order_id"] . "') AND order_status  = 1");
		$resulttwo = $this->db->query("SELECT * FROM `ri_riderorder` WHERE rider_id = (SELECT rider_id FROM ri_riderorder where Order_id = '" . $data["order_id"] . "') AND order_status  = 2");
		$resultthree = $this->db->query("SELECT * FROM `ri_riderorder` WHERE rider_id = (SELECT rider_id FROM ri_riderorder where Order_id = '" . $data["order_id"] . "') AND order_status  = 3");
		$resultfour = $this->db->query("SELECT * FROM `ri_riderorder` WHERE rider_id = (SELECT rider_id FROM ri_riderorder where Order_id = '" . $data["order_id"] . "') AND order_status  = 4");
		$resultfive = $this->db->query("SELECT * FROM `ri_riderorder` WHERE rider_id = (SELECT rider_id FROM ri_riderorder where Order_id = '" . $data["order_id"] . "') AND order_status  = 5");
		$resultsix = $this->db->query("SELECT * FROM `ri_riderorder` WHERE rider_id = (SELECT rider_id FROM ri_riderorder where Order_id = '" . $data["order_id"] . "') AND order_status  = 6");

		$result = array(
			'Assigned' => $resultone->num_rows,
			'Accepted' => $resulttwo->num_rows,
			'Rejected' => $resultthree->num_rows,
			'Delivered' => $resultfour->num_rows,
			'Assign to rider' => $resultfive->num_rows,
			'Processing' => $resultsix->num_rows
		);


		return array(
			'msg' => 'success',
			'result' => [$result] );

		// //send push notification
		// $dataF =  array
		// (
		// 	'contentTitle' 	=> 'Order Accepted',
		// 	'riderOrderID'		=> $data["order_id"]
		// );


		// $regId  = array('NjE1MmNlOTEtNGM5MC00YmFjLWJmNjUtNTFkYTMxZGVlNzA2');
		// $ids = array( $this->GetShopperDevice_idByOrderID($data['order_id'])['device_ID'] );

		// return $this->sendPushNotification($dataF, $ids, $regId);


//		return "success";
	}

	public function RejectOrder($data) {
		$this->db->query("UPDATE ri_riderorder set order_status=3 WHERE Order_id='" . $data["order_id"] . "'");
		$this->db->query("UPDATE oc_order SET order_status_id=1 where order_id=(SELECT order_fromDB_id from ri_riderorder where order_id='" . $data["order_id"] . "')");

		$dataF =  array
		(
			'contentTitle' 	=> 'Order Rejected',
			'riderOrderID'		=> $data["order_id"]
		);

		$regId  = array('NjE1MmNlOTEtNGM5MC00YmFjLWJmNjUtNTFkYTMxZGVlNzA2');
		$ids = array( $this->GetShopperDevice_idByOrderID($data['order_id'])['device_ID'] );
		$this->sendPushNotification($dataF, $ids, $regId);

		return "success";
	}

	public function OrderDelivered($data) {

		$this->db->query("UPDATE ri_riderorder set order_status=4 WHERE Order_id='" . $data["order_id"] . "'");
		$this->db->query("UPDATE oc_order SET order_status_id=19 where order_id=(SELECT order_fromDB_id from ri_riderorder where order_id='" . $data["order_id"] . "')");

		$query =  $this->db->query("SELECT order_fromDB_id from ri_riderorder where order_id='" . $data["order_id"] . "'");
		$riderOrder = $query->row;


		$this->load->language('rider/rider');


		$langId = 1;

		if($langId==1){
			$data1 =  array("en" => $this->language->get('text_order_delivered'));
		}else{
			$data1 =  array("ar" => $this->language->get('text_order_delivered'));
		}


		$userData = $this->getCustomerDeviceByOrderId($riderOrder['order_fromDB_id']);

		if(!empty($userData) && isset($userData)){
			if(isset($userData['mobile_user_id']) && isset($userData['mobile_reg_id'])){

				$userId = array($userData['mobile_user_id']);
				$regId  = array($userData['mobile_reg_id']);
				$response = $this->sendPushNotification($data1, $userId,$regId);


			}
		}


		$resultone = $this->db->query("SELECT * FROM `ri_riderorder` WHERE rider_id = (SELECT rider_id FROM ri_riderorder where Order_id = '" . $data["order_id"] . "') AND order_status  = 1");
		$resulttwo = $this->db->query("SELECT * FROM `ri_riderorder` WHERE rider_id = (SELECT rider_id FROM ri_riderorder where Order_id = '" . $data["order_id"] . "') AND order_status  = 2");
		$resultthree = $this->db->query("SELECT * FROM `ri_riderorder` WHERE rider_id = (SELECT rider_id FROM ri_riderorder where Order_id = '" . $data["order_id"] . "') AND order_status  = 3");
		$resultfour = $this->db->query("SELECT * FROM `ri_riderorder` WHERE rider_id = (SELECT rider_id FROM ri_riderorder where Order_id = '" . $data["order_id"] . "') AND order_status  = 4");
		$resultfive = $this->db->query("SELECT * FROM `ri_riderorder` WHERE rider_id = (SELECT rider_id FROM ri_riderorder where Order_id = '" . $data["order_id"] . "') AND order_status  = 5");
		$resultsix = $this->db->query("SELECT * FROM `ri_riderorder` WHERE rider_id = (SELECT rider_id FROM ri_riderorder where Order_id = '" . $data["order_id"] . "') AND order_status  = 6");

		$resultstatus = array(
			'Assigned' => $resultone->num_rows,
			'Accepted' => $resulttwo->num_rows,
			'Rejected' => $resultthree->num_rows,
			'Delivered' => $resultfour->num_rows,
			'Assign to rider' => $resultfive->num_rows,
			'Processing' => $resultsix->num_rows
		);


		return array(
			'msg' => 'success',
			'result' => [$resultstatus] );

	}

	public function OrderDeliveredToRider($data) {

		$this->db->query("UPDATE ri_riderorder set order_status=5 WHERE Order_id='" . $data["order_id"] . "'");
		return "success";
	}

	public function RiderOrderStatus($data)
	{

		$resultone = $this->db->query("SELECT * FROM `ri_riderorder` WHERE rider_id =  '" . $data["rider_id"] . "' AND order_status  = 1");
		$resulttwo = $this->db->query("SELECT * FROM `ri_riderorder` WHERE rider_id =  '" . $data["rider_id"] . "' AND order_status  = 2");
		$resultthree = $this->db->query("SELECT * FROM `ri_riderorder` WHERE rider_id =  '" . $data["rider_id"] . "' AND order_status  = 3");
		$resultfour = $this->db->query("SELECT * FROM `ri_riderorder` WHERE rider_id =  '" . $data["rider_id"] . "' AND order_status  = 4");
		$resultfive = $this->db->query("SELECT * FROM `ri_riderorder` WHERE rider_id =  '" . $data["rider_id"] . "' AND order_status  = 5");
		$resultsix = $this->db->query("SELECT * FROM `ri_riderorder` WHERE rider_id =  '" . $data["rider_id"] . "' AND order_status  = 6");

		$resultstatus = array(
			'Assigned' => $resultone->num_rows,
			'Accepted' => $resulttwo->num_rows,
			'Rejected' => $resultthree->num_rows,
			'Delivered' => $resultfour->num_rows,
			'Assign to rider' => $resultfive->num_rows,
			'Processing' => $resultsix->num_rows
		);


		return array(
			'msg' => 'success',
			'result' => [$resultstatus]);
	}

	public function TrackOrder($data) {
		$query = $this->db->query("SELECT o.Order_id,o.delivery_address,rc.latitude as latitude_rider,rc.longitute as longitude_rider,
              o.delivery_latitude,o.delivery_longitude
              from ri_ridercurrentlocations rc INNER JOIN
              ri_riderorder o on rc.riderID=o.rider_id
              WHERE o.Order_id='" . $data["order_id"] . "' ORDER BY rc.dateTime_c DESC LIMIT 1");
		$res=$query->rows;

		return $res;
	}

	public function GetUserOrderByStatus($data) {

		$query = $this->db->query("SELECT * from ri_riderorder WHERE rider_id='" . $data["rider_id"] . "' AND order_status='" . $data["status"] . "'");
		$res=$query->rows;

		return $res;
	}
	public function getAllOrders($data) {
		$this->load->model('catalog/product');
		$this->load->model('account/order');
		$this->load->model('saccount/seller');



		if((int)$data['all'] > 0){

			$query = $this->db->query("SELECT o.*,ro.Order_id as rider_order_id, rs.status_id as rider_order_status_id,rs.status as rider_order_status from ri_riderorder ro LEFT JOIN oc_order o ON(ro.order_fromDB_id=o.order_id) LEFT JOIN  ri_riderorderstatus rs ON(ro.order_status=rs.status_id) WHERE ro.rider_id='" . $data["rider_id"] . "' AND ro.order_status IN(" . $data['status'].")");
		}	else{

			$query = $this->db->query("SELECT o.*,ro.Order_id as rider_order_id, rs.status_id as rider_order_status_id,rs.status as rider_order_status from ri_riderorder ro LEFT JOIN oc_order o ON(ro.order_fromDB_id=o.order_id) LEFT JOIN  ri_riderorderstatus rs ON(ro.order_status=rs.status_id) WHERE rider_id='" . $data["rider_id"] . "' AND o.seller_id>0");
		}

		if($query->rows  > 0){
			$riderOrders =  $query->rows;

			foreach ($riderOrders as $key => $value){

				$product_total = $this->model_account_order->getTotalOrderProductsByOrderId($value['order_id']);

				$riderOrders[$key]['products_total'] = $product_total;


				$deliveryTime = $this->model_account_order->getOrderDeliveryTime($value['order_id']);
				$deliveryTimeArray = explode(" - ",$deliveryTime['delivery_time']);

				$current = strtotime(date("Y-m-d"));
				$date    = strtotime($deliveryTimeArray[0]);
				$datediff = $date - $current;
				$difference = floor($datediff/(60*60*24));
				if($difference==0)
				{
					$deliveryDay = "Today ";
				}else{
					$deliveryDay =  date('l', strtotime($deliveryTimeArray[0]));
				}

				$time1 = date('H:i A', strtotime($deliveryTimeArray[0]));
				$time2 = date('H:i A', strtotime($deliveryTimeArray[1]));
				$riderOrders[$key]['delivery_time'] = $deliveryDay.' '.$time1.' - '.$time2;

				$riderOrders[$key]['seller'] = $this->model_saccount_seller->getSeller($value['seller_id']);


				$riderOrders[$key]['total'] = $this->model_account_order->getOrderRiderSubTotals($value['order_id']);

			}

			return $riderOrders;
		}else{
			return 'failed';
		}



	}

	public function getOrderProducts($data) {

		$this->load->model('catalog/product');
		$this->load->model('account/order');
		$this->load->model('saccount/seller');
		$this->load->model('tool/image');
		// Paramters

		$orderId = $data["order_id"];
		$status   = $data["status"];  // 0 = All , 1= Found, 2= Not Found


		//Get Order Detail

		$orderQuery =  $query = $this->db->query("SELECT * from " . DB_PREFIX . "order  WHERE order_id='" . $orderId . "'");
		$order = $orderQuery->row;

		$deliveryTime = $this->model_account_order->getOrderDeliveryTime($orderId);
		$deliveryTimeArray = explode(" - ",$deliveryTime['delivery_time']);

		$current = strtotime(date("Y-m-d"));
		$date    = strtotime($deliveryTimeArray[0]);
		$datediff = $date - $current;
		$difference = floor($datediff/(60*60*24));
		if($difference==0)
		{
			$deliveryDay = "Today ";
		}else{
			$deliveryDay =  date('l', strtotime($deliveryTimeArray[0]));
		}

		$time1 = date('H:i A', strtotime($deliveryTimeArray[0]));
		$time2 = date('H:i A', strtotime($deliveryTimeArray[1]));
		$order['delivery_time'] = $deliveryDay.' '.$time1.' - '.$time2;

		$result['order'] =$order;

		$this->load->model('account/order');

		$product_total = $this->model_account_order->getTotalOrderProductsByOrderId($order['order_id']);
		$voucher_total = $this->model_account_order->getTotalOrderVouchersByOrderId($order['order_id']);

		$deliveryChargesArr = $this->db->query("Select oc_sellers.delivery_charges, oc_sellers.seller_id, oc_sellers.firstname, oc_sellers.lastname from oc_order_seller_shipping_fee join oc_sellers on oc_sellers.seller_id = oc_order_seller_shipping_fee.seller_id where oc_order_seller_shipping_fee.order_id = '".$order['order_id']."'")->rows;

		$credit = $this->db->query("SELECT amount FROM oc_customer_transaction WHERE customer_id = '".$order['customer_id']."' AND order_id = '".$order['order_id']."'")->row;

		if(isset($credit['amount'])){
			$result['order']['credit'] = $credit['amount'];
		} else {
			$result['order']['credit'] = 0;
		}

		$deliveryCharges = 0;
		foreach( $deliveryChargesArr as $deliveryChargesRow ){
			$result['order']['deliveryCharges']= $deliveryChargesRow['delivery_charges'];
		}


		$Total1 = $this->model_account_order->getOrderTotals1($order['order_id']);
		$Total2 = $this->model_account_order->getOrderTotals2($order['order_id']);

		$date_added = date("Y-m-d", strtotime($order['date_added']));
		$date = '2018-11-02';

		$beforDate = 0;
		$afterDate = 0;
		if($order['date_added'] != '0000-00-00 00:00:00'){
			if($date_added <= $date){
				$result['order']['total'] = number_format($Total1 + $Total2, 2);
			} else {
				$result['order']['total'] = number_format($Total1, 2);
			}
		} else {
			$result['order']['total'] = number_format($Total1 + $Total2, 2);
		}

		$result['products'] = array();
		$products = $this->model_account_order->getOrderProductsByStatus($orderId,$status);

		if(isset($products) && !empty($products)){
			foreach ($products as $key => $value){
				if ($value['image']) {
					$image = $this->model_tool_image->resize($value['image'], 250, 250);
				} else {
					$image = $this->model_tool_image->resize('placeholder.png', 250, 250);
				}

				$products[$key]['image'] = $image;

				$result['order']['found_product_total'] += $value['total'];
			}

		}

		$result['products'] = $products;

		return $result;
	}

	public function getOrderProduct($data) {

		$this->load->model('catalog/product');
		$this->load->model('account/order');
		$this->load->model('saccount/seller');
		$this->load->model('tool/image');
		// Paramters

		$orderId = $data["order_id"];
		$productId   = $data["product_id"];


		//Get Order Detail

		$orderQuery =  $query = $this->db->query("SELECT * from " . DB_PREFIX . "order  WHERE order_id='" . $orderId . "'");
		$order = $orderQuery->row;

		$deliveryTime = $this->model_account_order->getOrderDeliveryTime($orderId);
		$deliveryTimeArray = explode(" - ",$deliveryTime['delivery_time']);

		$current = strtotime(date("Y-m-d"));
		$date    = strtotime($deliveryTimeArray[0]);
		$datediff = $date - $current;
		$difference = floor($datediff/(60*60*24));
		if($difference==0)
		{
			$deliveryDay = "Today ";
		}else{
			$deliveryDay =  date('l', strtotime($deliveryTimeArray[0]));
		}

		$time1 = date('H:i A', strtotime($deliveryTimeArray[0]));
		$time2 = date('H:i A', strtotime($deliveryTimeArray[1]));
		$order['delivery_time'] = $deliveryDay.' '.$time1.' - '.$time2;
		$result['order'] =$order;

		$product = $this->model_account_order->getOrderProductDetail($orderId,$productId,1);

		if ($product['image']) {
			$image = $this->model_tool_image->resize($product['image'], 250, 250);
		} else {
			$image = $this->model_tool_image->resize('placeholder.png', 250, 250);
		}
		$product['image'] = $image;
		$product['options'] = $this->model_account_order->getOrderOptions($orderId,$product['order_product_id']);

		$result['product'] = $product;

		return $result;
	}
	public function updateProductStatus($data){

		// Paramters

		$orderId        = $data["order_id"];
		$productId      = $data["product_id"];
		$status         = $data["status"];  // 0 = All , 1= Found, 2= Not Found

		$pQuery =  $this->db->query("SELECT * FROM " . DB_PREFIX . "order_product  WHERE order_id='" . $orderId . "' AND product_id='" . $productId . "'");

		if($pQuery->num_rows){
			$query = $this->db->query("UPDATE " . DB_PREFIX . "order_product SET product_status='".$status."' WHERE order_id='" . $orderId . "' AND product_id='" . $productId . "'");


			$resultone = $this->db->query("SELECT * FROM " . DB_PREFIX . "order_product WHERE order_id = '" . $orderId . "' AND product_status = 0");
			$resulttwo = $this->db->query("SELECT * FROM " . DB_PREFIX . "order_product WHERE order_id = '" . $orderId . "' AND product_status = 1");
			$resultthree = $this->db->query("SELECT * FROM " . DB_PREFIX . "order_product WHERE order_id = '" . $orderId . "' AND product_status = 2");

			$result = array(
				'All' => $resultone->num_rows,
				'Found' => $resulttwo->num_rows,
				'Not Found' => $resultthree->num_rows,
			);


			/*if($status==2) // If not found
            {
                $this->load->language('rider/rider');
                   $this->load->model('account/order');
                $langId = 1;

                $product = $this->model_account_order->getOrderProductDetail($orderId,$productId,$langId);

                if($langId==1){
                    $data1 =  array("en" => $this->language->get('text_substitute').' '.$product['name']);
                }else{
                    $data1 =  array("ar" => $this->language->get('text_substitute').' '.$product['name']);
                }



                $userData = $this->getCustomerDeviceByOrderId($orderId);

               if(!empty($userData) && isset($userData)){
                    if(isset($userData['mobile_user_id']) && isset($userData['mobile_reg_id'])){

                        $userId = array($userData['mobile_user_id']);
                        $regId  = array($userData['mobile_reg_id']);
                 $response = $this->sendPushNotification($data1, $userId,$regId);


                    }
               }

            }*/





			return array(
				'msg' => 'success',
				'result' => [$result] );

		}else{
			return array(
				'msg' => 'failed',
				'result' => 'failed' );

		}

	}


	public function GetAssignerOrderByStatus($data) {

		$query = $this->db->query("SELECT o.Order_id,o.rider_id,o.order_fromDB_id,o.order_date,o.order_amount,o.order_status,o.delivery_address,
                               o.commission_amount,o.access_datetime,o.delivery_latitude,o.delivery_longitude 
                               FROM ri_shoppertoriderassignment s INNER JOIN ri_riderorder o
                               on s.order_id=o.Order_id
                                where s.shopper_id='" . $data["shopper_id"] . "' AND o.order_status='" . $data["status"] . "'");
		$res=$query->rows;

		return $res;
	}



	public function UpdateProductBarCode($data) {
		$query = $this->db->query("update oc_product set upc='" . $data["barCode"] . "' where product_id='" . $data["product_id"] . "'");


		return "success";
	}


	public function loginRider($data) {

		$query = $this->db->query("SELECT ri.*,a.area_name FROM ri_riderinfo ri LEFT OUTER JOIN ri_riderinfoareas ria on ri.rider_id=ria.rider_id LEFT OUTER JOIN ri_areas a on ria.area_id=a.area_id WHERE ri.email='".$data["email"]."' AND password='".$data["password"]."'");

		$res=$query->row;
		$data = array();
		if($query->num_rows > 0)
		{
			$uOnlineData = array('UserID'=> $res["rider_id"] , 'status'=>1);
			$userStatus =  $this->ChangeUserOnlineStatus($uOnlineData);

			$this->db->query("UPDATE ri_riderinfo SET device_ID = '" . $data["device_ID"] . "' WHERE rider_id = '" . $res["rider_id"] . "'");

			$this->db->query("INSERT into ri_usersession(user_id,loginTime,lastActivityTime)VALUES(".$res["rider_id"].", NOW(), NOW() )");

			//Get Assigned Orders
			$newOrders = $this->GetUserOrderByStatus(array('rider_id'=>$res["rider_id"],'status'=>1));

			//Get Accepted Orders
			$myOrders = $this->GetUserOrderByStatus(array('rider_id'=>$res["rider_id"],'status'=>2));

			//Get All Orders
			$allOrders = $this->getAllOrders(array('rider_id'=>$res["rider_id"]));
			$res['is_online'] =  ($userStatus=='success') ? 1 : 0;
			$data['rider'] = $res;
			$data['new_order'] = count($newOrders);
			$data['my_order'] = count($myOrders);
			$data['history_order'] =  count($allOrders);

		}
		else
		{

			$data['rider'] = 'user not found';
			$data['new_order'] = 0;
			$data['my_order'] = 0;
			$data['history_order'] = 0;

		}

		return $data;

	}

	public function forgotPassword($data){
		$this->load->language('mail/forgotten');
		$query = $this->db->query("select * from ri_riderinfo where email='".$data["email"]."'");

		$rider = $query->row;

		if(isset($rider) && !empty($rider)){
			$subject = 'Rider - Password';

			$message  = 'Dear '.$rider['name'] . "\n\n";
			$message .= 'You Password is: ' ;
			$message .= $rider['password'];

			sendSMS($rider['contact_no'],$message);

			$c_name = $rider['name'];

			$ip_address = $this->request->server['REMOTE_ADDR'];

			$this->load->model('catalog/product');

			$template = $this->model_catalog_product->getEmailTemplate('rider-forgot-password-api-email');

			$tags = array("[RIDER_NAME]", "[PASSWORD]", "[IP_ADDRESS]", "[SUBJECT]");
			$tagsValues = array($c_name, $rider['password'], $ip_address, $template['subject']);

			//get subject
			$subject = html_entity_decode($template['subject'], ENT_QUOTES, 'UTF-8');
			$subject = str_replace($tags, $tagsValues, $subject);
			$subject = html_entity_decode($subject, ENT_QUOTES);

			//get message body
			$msg = html_entity_decode($template['description'], ENT_QUOTES, 'UTF-8');
			$msg = str_replace($tags, $tagsValues, $msg);
			$msg = html_entity_decode($msg, ENT_QUOTES);

			$header = html_entity_decode($this->config->get('config_mail_header'), ENT_QUOTES, 'UTF-8');
			$header = str_replace($tags, $tagsValues, $header);
			$message = html_entity_decode($header, ENT_QUOTES);
			$message .= $msg;
			$message .= html_entity_decode($this->config->get('config_mail_footer'), ENT_QUOTES, 'UTF-8');

			$mail = new Mail();
			$mail->protocol = $this->config->get('config_mail_protocol');
			$mail->parameter = $this->config->get('config_mail_parameter');
			$mail->smtp_hostname = $this->config->get('config_mail_smtp_hostname');
			$mail->smtp_username = $this->config->get('config_mail_smtp_username');
			$mail->smtp_password = html_entity_decode($this->config->get('config_mail_smtp_password'), ENT_QUOTES, 'UTF-8');
			$mail->smtp_port = $this->config->get('config_mail_smtp_port');
			$mail->smtp_timeout = $this->config->get('config_mail_smtp_timeout');

			$mail->setTo($data["email"]);
			$mail->setFrom($this->config->get('config_email'));
			$mail->setSender(html_entity_decode($this->config->get('config_name'), ENT_QUOTES, 'UTF-8'));
			$mail->setSubject($subject);
			//$mail->setText($message);
			$mail->setHtml($message);
			$mail->send();

			return "Success";
		}else{
			return "Failed";
		}
	}
	public function GetRiderBy_ID($data) {
		//$query = $this->db->query("select * from ri_riderinfo where rider_id='".$data["id"]."'");

		$query = $this->db->query("SELECT ri.*,a.area_name FROM ri_riderinfo ri LEFT OUTER JOIN ri_riderinfoareas ria on ri.rider_id=ria.rider_id LEFT OUTER JOIN ri_areas a on ria.area_id=a.area_id WHERE ri.rider_id='".$data["id"]."'");
		$res=$query->row;

		return $res;
	}

	private function getRiderByOrderId($orderId) {


		$query = $this->db->query("SELECT ri.* FROM ri_riderinfo ri LEFT JOIN ri_riderorder ro ON(ri.rider_id=ro.rider_id) WHERE ro.order_fromDB_id='".$orderId."' AND ro.order_status!=3");
		$res=$query->row;

		return $res;
	}
	public function changePassword($data) {

		$rider_id = (int)$data["rider_id"];
		$oldPassword =  $data["old_password"];
		$newPassword =  $data["new_password"];
		$res = array("msg"=>"Failed");
		$query = $this->db->query("select * from ri_riderinfo where rider_id='".$rider_id."' AND password='".$oldPassword."'");
		if(count($query->rows) > 0)
		{
			$this->db->query("UPDATE ri_riderinfo SET password='".$newPassword."' where rider_id='".$rider_id."'");

			$res = array("msg"=>"Success");

		}


		return $res;
	}
	public function updateRider($data) {

		$rider_id = (int)$data["rider_id"];

		$res = array("msg"=>"Failed");
		$query = $this->db->query("select * from ri_riderinfo where rider_id='".$rider_id."'");
		if(count($query->rows) > 0)
		{

			$this->db->query("UPDATE ri_riderinfo SET name ='" . $data["name"] . "' , email ='" . $data['email'] . "' , contact_no ='" . $data["contact_no"] . "', address ='" . $data["address"] . "', license ='" . $data["license"] . "', is_online ='" . $data["is_online"] . "' , rider_status ='" . $data["rider_status"] . "' WHERE rider_id ='" . $rider_id . "'");

			$res = array("msg"=>"Success");

		}


		return $res;
	}


	public function GetRiderByEmail($data) {
		$caseType =  (int)$data['caseType'];

		$query = $this->db->query("select * from ri_riderinfo where email='".$data["email"]."'");

		//Add Case
		if($caseType ==0)
		{
			if(count($query->rows) > 0)
			{
				$res = true;
			}
			else{
				$res = false;
			}
		}
		else if($caseType ==1) // Edit Case
		{
			if(count($query->rows) > 1)
			{
				$res = true;
			}
			else{
				$res = false;
			}
		}

		return $res;
	}

	public function AddRiderCurrentLocation($data) {
		$this->db->query("INSERT INTO ri_ridercurrentlocations ( riderID,latitude,longitute,dateTime_c)  VALUES ('". $data['riderID']."','". $data['latitude']."','". $data['longitute']."','". $data['dateTime_c']."')");
		$res=$this->db->getLastId();

		return $res;

	}

	public function GetRiderLastLocation($data) {
		$query = $this->db->query("select * from ri_ridercurrentlocations where riderID='".$data["riderID"]."' ORDER BY dateTime_c desc LIMIT 1");
		$res=$query->row;

		return $res;

	}

	public function GetUsersByStatus($data) {

		$sqlQuery  = "SELECT ri.*,a.area_name,a.city FROM ri_riderinfo ri INNER JOIN  ri_riderinfoareas ria on ri.rider_id=ria.rider_id INNER JOIN ri_areas a
                      on ria.area_id=a.area_id  WHERE ri.email != ''";

		if(isset($data['is_active']))
		{
			$sqlQuery .= " AND ri.is_active = ".$data['is_active'];
		}

		if(isset($data['is_online'])){
			$sqlQuery .= " AND ri.is_online = ".$data['is_online'];
		}

		if(isset($data['status'])){
			$sqlQuery .= " AND ri.rider_status = ".$data['status'];
		}



		$query = $this->db->query($sqlQuery);
		return $query->rows;
	}
	//public function getRidersByGivenTimeLimit($data)
//    {
//
//        $varTimeIn =  substr($data['day'],0,3)."_time_in";
//        $varTimeOut = substr($data['day'],0,3)."_time_out";
//
//        $timeIn = date("H:m",strtotime($data['timeIn']));
//        $timeOut = date("H:m",strtotime($data['timeOut']));
//
//
//          $sqlquery =  "SELECT ri.rider_id,ri.name , ri.email, a.area_name  FROM ri_rideravailability ra INNER JOIN ri_riderinfo ri
//                      ON ra.rider_id=ri.rider_id  INNER JOIN  ri_riderinfoareas ria on ri.rider_id=ria.rider_id INNER JOIN ri_areas a
//                      on ria.area_id=a.area_id WHERE " . $data['day'] ."= 1 AND CONVERT(".$varTimeIn. ",TIME) <= '".$timeIn."' AND
//                      CONVERT(".$varTimeOut. ",TIME) >= '".$timeIn."' AND ri.is_active=1 AND ri.is_online=1 AND
//                      ri.rider_status in (1,3) and a.city='".$data['city']."'";
//
//           $sqlquery =  "SELECT ri.rider_id,ri.name , ri.email, a.area_name  FROM ri_rideravailability ra INNER JOIN ri_riderinfo ri
//                      ON ra.rider_id=ri.rider_id  INNER JOIN  ri_riderinfoareas ria on ri.rider_id=ria.rider_id INNER JOIN ri_areas a
//                      on ria.area_id=a.area_id WHERE " . $data['day'] ."= 1 AND CONVERT(".$varTimeIn. ",TIME) >= '".$timeIn."' AND
//                      CONVERT(".$varTimeOut. ",TIME) <= '".$timeOut."' AND ri.is_active=1 AND ri.is_online=1 AND
//                      ri.rider_status in (1,3) and a.city='".$data['city']."'";
//
//       die($sqlquery);
//       $query =  $this->db->query($sqlquery);
//        $res=$query->rows;
//
//        return $res;
//    }
	public function getRidersByGivenTimeLimit($data)
	{

		$varTimeIn =  substr($data['day'],0,3)."_time_in";
		$varTimeOut = substr($data['day'],0,3)."_time_out";

		$timeIn = date("H:i",strtotime(str_replace('+',' ',$data['timein'])));

		$sqlquery =  "SELECT ri.rider_id,ri.name , ri.email, a.area_name  FROM ri_rideravailability ra INNER JOIN ri_riderinfo ri
                      ON ra.rider_id=ri.rider_id  INNER JOIN  ri_riderinfoareas ria on ri.rider_id=ria.rider_id INNER JOIN ri_areas a
                      on ria.area_id=a.area_id WHERE " . $data['day'] ."= 1 AND ri.is_active=1 AND ri.is_online=1 AND
                      ri.rider_status in (1,3) and a.city='".$data['city']."' AND CONVERT('".$timeIn. "',TIME) >= CONVERT(".$varTimeIn. ",TIME) AND 
                      CONVERT('".$timeIn."',TIME) <= CONVERT(".$varTimeOut. ",TIME)";





		$query =  $this->db->query($sqlquery);
		$res=$query->rows;

		return $res;
	}
	public function getRidersByGivenTimeLimitV2($data)
	{

		/*$varTimeIn =  substr($data['day'],0,3)."_time_in";
        $varTimeOut = substr($data['day'],0,3)."_time_out";

         $timeIn = date("H:i",strtotime(str_replace('+',' ',$data['timein'])));

         $sqlquery =  "SELECT ri.rider_id,ri.name , ri.email, a.area_name  FROM ri_rideravailability ra INNER JOIN ri_riderinfo ri
                      ON ra.rider_id=ri.rider_id  INNER JOIN  ri_riderinfoareas ria on ri.rider_id=ria.rider_id INNER JOIN ri_areas a
                      on ria.area_id=a.area_id WHERE " . $data['day'] ."= 1 AND ri.is_active=1 AND ri.is_online=1 AND
                      ri.rider_status in (1,3) and a.area_name LIKE '%".$data['city']."%' AND CONVERT('".$timeIn. "',TIME) >= CONVERT(".$varTimeIn. ",TIME) AND 
                      CONVERT('".$timeIn."',TIME) <= CONVERT(".$varTimeOut. ",TIME)";*/

		$sqlquery  = "SELECT rider_id,name , email FROM ri_riderinfo WHERE is_active=1 AND is_online=1";

		$query =  $this->db->query($sqlquery);
		$res=$query->rows;

		return $res;
	}
	public function updateRiderAvailability($data)
	{

		// Check If rider exists
		$riderInfo =  $this->db->query("SELECT * FROM ri_rideravailability WHERE rider_id = ".$data['rider_id']);

		$res = "failed";
		if(count($riderInfo->rows) > 0){
			$updateAvailabilty =  "UPDATE `ri_rideravailability` SET `saturday`=".$data['saturday']." ,`sat_time_in`= '".strToDateTime($data['sat_time_in'])."',`sat_time_out`= '".strToDateTime($data['sat_time_out'])."',
                                    `sunday`= ".$data['sunday'].",`sun_time_in`='".strToDateTime($data['sun_time_in'])."',`sun_time_out`= '".strToDateTime($data['sun_time_out'])."',
                                    `monday`= ".$data['monday'].",`mon_time_in`= '".strToDateTime($data['mon_time_in'])."',`mon_time_out`= '".strToDateTime($data['mon_time_out'])."',
                                     `tuesday`= ".$data['tuesday'].",`tue_time_in`= '".strToDateTime($data['tue_time_in'])."',`tue_time_out`='".strToDateTime($data['tue_time_out'])."',
                                     `wednesday`=".$data['wednesday'].",`wed_time_in`= '".strToDateTime($data['wed_time_in'])."',`wed_time_out`= '".strToDateTime($data['wed_time_out'])."',
                                     `thursday`= ".$data['thursday'].",`thu_time_in`= '".strToDateTime($data['thu_time_in'])."' ,`thu_time_out`= '".strToDateTime($data['thu_time_out'])."',
                                     `friday`= ".$data['friday'].",`fri_time_in`= '".strToDateTime($data['fri_time_in'])."',`fri_time_out`= '".strToDateTime($data['fri_time_out'])."' 
                                       WHERE rider_id = ".$data['rider_id'];


			$this->db->query($updateAvailabilty);
			$res = "success";
		}
		else{
			$insertAvailabilty  =  "INSERT INTO `ri_rideravailability` (`rider_id`, `saturday`, `sat_time_in`, `sat_time_out`, `sunday`, `sun_time_in`, `sun_time_out`,
                                                    `monday`, `mon_time_in`, `mon_time_out`, `tuesday`, `tue_time_in`, `tue_time_out`, `wednesday`, 
                                                    `wed_time_in`, `wed_time_out`, `thursday`, `thu_time_in`, `thu_time_out`, `friday`, `fri_time_in`,
                                                    `fri_time_out`) 
                                                    VALUES ('".$data['rider_id']."', ".$data['saturday'].", '".strToDateTime($data['sat_time_in'])."', '".strToDateTime($data['sat_time_out'])."',
                                                     ".$data['sunday'].",'".strToDateTime($data['sun_time_in'])."', '".strToDateTime($data['sun_time_out'])."', 
                                                     ".$data['monday'].", '".strToDateTime($data['mon_time_in'])."', '".strToDateTime($data['mon_time_out'])."',
                                                      ".$data['tuesday'].", '".strToDateTime($data['tue_time_in'])."', '".strToDateTime($data['tue_time_out'])."', 
                                                      ".$data['wednesday'].", '".strToDateTime($data['wed_time_in'])."','".strToDateTime($data['wed_time_out'])."', 
                                                      ".$data['thursday'].", '".strToDateTime($data['thu_time_in'])."', '".strToDateTime($data['thu_time_out'])."', 
                                                      ".$data['friday'].", '".strToDateTime($data['fri_time_in'])."', '".strToDateTime($data['fri_time_out'])."')";

			/**
			 * $insertAvailabilty  = "INSERT INTO `ri_rideravailability` SET `rider_id`=".$data['rider_id']." , `saturday`=".$data['saturday']." ,`sat_time_in`= '".strToDateTime($data['sat_time_in'])."',`sat_time_out`= '".strToDateTime($data['sat_time_out'])."',
			 *                                     `sunday`= ".$data['sunday'].",`sun_time_in`='".strToDateTime($data['sun_time_in'])."',`sun_time_out`= '".strToDateTime($data['sun_time_out'])."',
			 *                                     `monday`= ".$data['monday'].",`mon_time_in`= '".strToDateTime($data['mon_time_in'])."',`mon_time_out`= '".strToDateTime($data['mon_time_out'])."',
			 *                                      `tuesday`= ".$data['tuesday'].",`tue_time_in`= '".strToDateTime($data['tue_time_in'])."',`tue_time_out`='".strToDateTime($data['tue_time_out'])."',
			 *                                      `wednesday`=".$data['wednesday'].",`wed_time_in`= '".strToDateTime($data['wed_time_in'])."',`wed_time_out`= '".strToDateTime($data['wed_time_out'])."',
			 *                                      `thursday`= ".$data['thursday'].",`thu_time_in`= '".strToDateTime($data['thu_time_in'])."' ,`thu_time_out`= '".strToDateTime($data['thu_time_out'])."',
			 *                                      `friday`= ".$data['friday'].",`fri_time_in`= '".strToDateTime($data['fri_time_in'])."',`fri_time_out`= '".strToDateTime($data['fri_time_out'])."'";
			 *
			 */

			$this->db->query($insertAvailabilty);
			$availability_id = $this->db->getLastId();
			$res = ($availability_id > 0) ? "success" : "failed";

		}
		return $res;
	}

	public function addRiderArea($data)
	{
		$riderinfoareas = $this->db->query("SELECT * FROM ri_riderinfoareas  WHERE  `rider_id`=".$data['rider_id']);
		if(count($riderinfoareas->rows) > 0)
		{
			$this->db->query("Update ri_riderinfoareas SET `area_id`=".$data['area_id']." WHERE  `rider_id`=".$data['rider_id']);
			$res = "success";
		}
		else{
			$query = $this->db->query("INSERT INTO ri_riderinfoareas SET `rider_id`=".$data['rider_id']." , `area_id`=".$data['area_id']);
			$rider_area_id = $this->db->getLastId();
			$res = ($rider_area_id > 0) ? "success" : "failed";

		}

		return $res;
	}
	public function getRiderAreasById($data)
	{

		$query = $this->db->query("SELECT * FROM ri_riderinfoareas ria INNER JOIN ri_areas ra ON ria.area_id = ra.area_id  WHERE  `rider_id`=".$data['rider_id']);
		$res=$query->rows;
		return $res;
	}
	public function getRiderAvailiblityByRiderId($data)
	{

		$query = $this->db->query("SELECT * FROM ri_rideravailability WHERE  `rider_id`=".$data['rider_id']);

		if($query->num_rows > 0)
		{
			$res=$query->rows;
			return $res;
		}
		else{
			return  "Data not found";
		}

	}

	public function addArea($data)
	{

		$query = $this->db->query("INSERT INTO ri_areas SET `area_name`= '".$data['area']."' , `city`= '".$data['city']."'");
		$area_id = $this->db->getLastId();
		$res = ($area_id > 0) ? "success" : "failed";
		return $res;
	}
	public function addAreaV2($data)
	{

		$areaInfo = $this->db->query("SELECT * FROM ri_areas WHERE `area_name`= '".$data['area']."'");
		if($areaInfo->num_rows > 0)
		{

			$res=$areaInfo->row['area_id'];

		}
		else{
			$query = $this->db->query("INSERT INTO ri_areas SET `area_name`= '".$data['area']."'");
			$area_id = $this->db->getLastId();
			$res = ($area_id > 0) ? $area_id: 0;
		}

		return $res;
	}
	public function getAreas()
	{

		$query = $this->db->query("SELECT * FROM ri_areas");
		$res=$query->rows;
		return $res;
	}

	public function assignOrder($data)
	{
		$insertOrderSql = "INSERT INTO `ri_riderorder` (`rider_id`, `order_fromDB_id`, `order_date`, `order_amount`, `order_status`, `delivery_address`, `delivery_longitude`, `delivery_latitude`, `commission_amount`, `access_datetime`) VALUES ('".$data['rider_id']."', ".$data['order_fromDB_id'].", '".$data['order_date']."' , ".$data['order_amount'].", 1, '".$data['delivery_address']."', '".$data['delivery_latitude']."', '".$data['delivery_longitude']."', ".$data['commission_amount'].", NOW())";


		$query = $this->db->query($insertOrderSql);
		$order_id = $this->db->getLastId();

		if($order_id > 0 ){
			//$insertAssigner = "INSERT INTO `ri_shoppertoriderassignment` (`shopper_id`, `rider_id`, `order_id`, `access_datetime`)
			// VALUES (".$data['order_assigner_id'].", ".$data['rider_id'].", ".$order_id.", NOW())";


			//$query = $this->db->query($insertAssigner);
			// $assignerId = $this->db->getLastId();
			$this->db->query("UPDATE oc_order SET order_status_id=2 where order_id='".$data['order_fromDB_id']."'");

			//send push notification
			$dataF =  array
			(
				'contentTitle' 	=> 'New Order Assigned',
				'riderOrderID'		=> $order_id,
				'MainOrderID'	=> $dataF['order_fromDB_id']
			);

			//$regId  = array('NjE1MmNlOTEtNGM5MC00YmFjLWJmNjUtNTFkYTMxZGVlNzA2');
			$regId  = array('NWYxNTkxM2MtNWJhZS00ZTE5LThkMGMtOGQ5YWFhMzZiMTJi');
			

			$ids = array( $this->GetDevice_id($data['rider_id'])['device_ID'] );
			$this->sendPushNotification($dataF, $ids, $regId);


		}
		$res = ($order_id > 0 ) ? $order_id : "failed";
		//$res = ($order_id > 0 && $assignerId > 0 ) ? $order_id : "failed";
		return $res;
	}

	public function assignOrderV2($data)
	{
		$order_id   = $data['rider_order_id'];
		$rider_id   = $data['rider_id'];
		$shoper_id  = $data['shoper_id'];

		//Select record fron riderorder table against shoper and orderId;
		$orderInfo = $this->db->query("SELECT * from ri_riderorder WHERE rider_id='" . $shoper_id . "' AND order_id='" . $order_id . "'");

		if(count($orderInfo->rows) > 0)
		{
			// Update status of shopper to rider
			$this->db->query("Update ri_riderorder SET order_status = 5 WHERE rider_id='" . $shoper_id . "' AND order_id='" . $order_id . "'");



			$insertOrderSql = "INSERT INTO `ri_riderorder` (`rider_id`, `order_fromDB_id`, `order_date`, `order_amount`, `order_status`, `delivery_address`, `delivery_latitude`, `delivery_longitude`, `commission_amount`, `access_datetime`)
                            VALUES ('".$rider_id."', ".$orderInfo->row['order_fromDB_id'].", '".$orderInfo->row['order_date']."' , ".$orderInfo->row['order_amount'].", 1, '".$orderInfo->row['delivery_address']."', '".$orderInfo->row['delivery_latitude']."', '".$orderInfo->row['delivery_longitude']."', ".$orderInfo->row['commission_amount'].", NOW())";


			$query = $this->db->query($insertOrderSql);
			$orderId = $this->db->getLastId();



			if($orderId > 0 ){
				$insertAssigner = "INSERT INTO `ri_shoppertoriderassignment` (`shopper_id`, `rider_id`, `order_id`, `access_datetime`)
                                VALUES (".$shoper_id.", ".$rider_id.", ".$orderId.", NOW())";


				$query = $this->db->query($insertAssigner);
				$assignerId = $this->db->getLastId();
				$this->db->query("UPDATE oc_order SET order_status_id=2 where order_id='".$orderInfo->row['order_fromDB_id']."'");





				//send push notification
				$dataF =  array
				(
					'contentTitle' 	=> 'New Order Assigned',
					'riderOrderID'		=> $orderId,
					'MainOrderID'	=> $dataF['order_fromDB_id']
				);

				$regId  = array('NjE1MmNlOTEtNGM5MC00YmFjLWJmNjUtNTFkYTMxZGVlNzA2');
				$ids = array( $this->GetDevice_id($rider_id)['device_ID'] );
				$this->sendPushNotification($dataF, $ids, $regId);


			}
		}

		$res = ($orderId > 0 && $assignerId > 0 ) ? $orderId : "failed";
		return $res;
	}

	public function logOut($data) {

		$this->db->query("UPDATE ri_usersession SET logoutTime = NOW() WHERE user_id=".$data['userID']);
		return "Success";
	}

	function ptest()
	{
		$data =  array
		(
			'message' 	=> 'here is a message. message',
			'title'		=> 'This is a title. title',
			'subtitle'	=> 'This is a subtitle. subtitle',
			'tickerText'	=> 'Ticker text here...Ticker text here...Ticker text here'

		);

		$ids = array('dZBFgMs_n_A:APA91bHOWpqmxSkraOmj4o3-kCt4HRjCrMAZoZ4sg-I53MJOkRtVEkZgaDUWL4-6bP_d2Sg4l5g1_GwVrXPVZs79lUCulT4jXsPh7DLec0V8EN5368d7PU6EsY3PoQHeOo2UmJ4F5595');
		$this->sendPushNotification($data, $ids);
	}



	function pNotification(){

		$data =  array("en" => 'Testing one singal push notification');

		$userId = array('b13c8f0f-f618-4572-9f1b-a7989898b8d8');
		$regId  = array('NjE1MmNlOTEtNGM5MC00YmFjLWJmNjUtNTFkYTMxZGVlNzA2');
		//$regId  = array('NjE1MmNlOTEtNGM5MC00YmFjLWJmNjUtNTFkYTMxZGVlNzA2');
		$response = $this->sendPushNotification($data, $userId,$regId);
		debugging($response);
	}


	function sendPushNotification($msgContent, $userId,$regId) {

		$content = $msgContent;

		$fields = array(
			'app_id' => "c7e4b4b0-091a-42bd-a879-02b103bc2178",
			//'app_id' => "NjE1MmNlOTEtNGM5MC00YmFjLWJmNjUtNTFkYTMxZGVlNzA2",			
			'include_player_ids' => $userId, //Please UnComment this line for dynamic user
			//'include_player_ids' => array('4358f1a8-34c7-479e-a7d0-b8a852f4639b'),  abf87f54-59a4-451b-8d78-dc5ed02beda9
			'contents' => $content
		);

		$fields = json_encode($fields);


		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, "https://onesignal.com/api/v1/notifications");
		curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json; charset=utf-8',
			'Authorization: Basic NjE1MmNlOTEtNGM5MC00YmFjLWJmNjUtNTFkYTMxZGVlNzA2'));
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
		curl_setopt($ch, CURLOPT_HEADER, FALSE);
		curl_setopt($ch, CURLOPT_POST, TRUE);
		curl_setopt($ch, CURLOPT_POSTFIELDS, $fields);
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);

		$response = curl_exec($ch);
		curl_close($ch);

		return $response;

	}

	function sendPushNotificationOLD($data, $registrationIds) {

		$msg =$data;
		$fields = array
		(
			'registration_ids' 	=> $registrationIds,
			'data'			=> $msg
		);

		$headers = array
		(
			'Authorization: key=AAAAeDIkjlA:APA91bHZDZm47TjbOuUBP6WmfxDh66LlrI1HPmYX3G0nFQKG1FyqX7pGrEUl7QTGe9KPQ6GtnidsFbK39AMZFDkL-K1DHz58oPB1sGtTBxyQ9pc18lkoa3q3fLoHt8907EQ9c42Wrs1l',
			'Content-Type: application/json'
		);

		$ch = curl_init();
		curl_setopt( $ch,CURLOPT_URL, 'https://android.googleapis.com/gcm/send' );
		curl_setopt( $ch,CURLOPT_POST, true );
		curl_setopt( $ch,CURLOPT_HTTPHEADER, $headers );
		curl_setopt( $ch,CURLOPT_RETURNTRANSFER, true );
		curl_setopt( $ch,CURLOPT_SSL_VERIFYPEER, false );
		curl_setopt( $ch,CURLOPT_POSTFIELDS, json_encode( $fields ) );
		$result = curl_exec($ch );
		curl_close( $ch );
		return $result;

	}



	public function GetDevice_id($rider_id) {
		$query = $this->db->query("SELECT device_ID from ri_riderinfo where rider_id=".$rider_id);
		return $query->row;
	}

	public function GetShopperDevice_idByOrderID($order_id) {
		$query = $this->db->query("SELECT r.device_ID from ri_riderinfo r INNER JOIN ri_shoppertoriderassignment s on r.rider_id=s.shopper_id
       WHERE s.order_id=".$order_id);
		return $query->row;
	}

	public function getOrdersHistory($data) {
		$this->load->model('tool/image');


		$rider_id = (int)$data['rider_id'];
		$status_id = (int)$data['status_id'];

		$sql = "SELECT ro.Order_id as rider_order_id,o.order_id, o.date_added,o.total,o.payment_address_1,o.shipping_address_1,o.telephone
                                        ,IFNULL(ot.value,0) as delivery_charges,od.signature_image,od.parcel_image,rs.status,
                                        ro.delivery_longitude,ro.delivery_latitude,CONCAT (o.firstname,' ',o.lastname) AS customer_name   
                                        FROM ri_riderorder ro
                                        LEFT OUTER JOIN " . DB_PREFIX . "order o ON ro.order_fromDB_id = o.order_id
                                        LEFT OUTER JOIN ri_riderorderstatus rs ON ro.order_status = rs.status_id                                        
                                        LEFT OUTER JOIN " . DB_PREFIX . "order_delivered od ON o.order_id = od.order_id
                                        LEFT OUTER JOIN " . DB_PREFIX . "order_total ot ON o.order_id = ot.order_id
                                        AND ot.code = 'low_order_fee' 
                                        WHERE ro.rider_id =".$rider_id;



		if($status_id > 0){
			$sql .= " AND rs.status_id = ". $status_id;
		}

		$sql .= " ORDER BY o.order_id DESC";


		//$orders_query = $this->db->query("SELECT o.order_id, o.date_added,o.order_id,o.total,o.payment_address_1,o.shipping_address_1,o.telephone
//                                        ,os.name as order_status ,IFNULL(ot.value,0) as delivery_charges ,od.signature_type,od.signature_image,od.parcel_type,od.parcel_image  FROM " . DB_PREFIX . "order o
//                                        INNER JOIN " . DB_PREFIX . "order_status os ON o.order_status_id = os.order_status_id
//                                        LEFT OUTER JOIN " . DB_PREFIX . "order_delivered od ON o.order_id = od.order_id
//                                        LEFT OUTER JOIN " . DB_PREFIX . "order_total ot ON o.order_id = ot.order_id
//                                        AND ot.code = 'low_order_fee'
//                                        WHERE os.language_id=1 ORDER BY o.order_id DESC");


		$orders_query = $this->db->query($sql);

		$orders = array();
		if (count($orders_query->rows) > 0) {

			foreach($orders_query->rows as $order)
			{
				$signImage    =  $order['signature_image'];
				$parcelImage  =  $order['parcel_image'];

				if(isset($signImage) && !empty($signImage))
				{
					$order['signature_image'] = $this->model_tool_image->resize($signImage, 470, 350);
				}
				if(isset($parcelImage) && !empty($parcelImage))
				{
					$order['parcel_image'] = $this->model_tool_image->resize($parcelImage, 470, 350);
				}
				$order_product_query = $this->db->query("SELECT op.`name`,op.`quantity`,op.`price`,op.`total`,op.`tax`,p.image,p.product_id,p.upc
                                                        FROM " . DB_PREFIX . "order_product op 
                                                        LEFT JOIN " . DB_PREFIX . "product p ON  op.product_id = p.product_id
                                                        WHERE order_id = '" . (int)$order['order_id'] . "'");

				$allProducts = $order_product_query->rows;


				foreach($allProducts as $key => $value)
				{
					$allProducts[$key]['image'] = $this->model_tool_image->resize($allProducts[$key]['image'], 470, 350);
				}

				$order['items'] = $allProducts;

				$orders[] = $order;

			}

			return $orders ;


		} else {

			return  "No Order found!";
		}
	}
	public function getOrders($data) {
		$this->load->model('tool/image');
		$store_address = nl2br($this->config->get('config_address'));

		$rider_id = (int)$data['rider_id'];
		$status_id = (int)$data['status_id'];

		$sql = "SELECT ro.Order_id as rider_order_id,o.order_id, o.date_added,o.total,o.payment_address_1,o.shipping_address_1,o.telephone
                                        ,IFNULL(ot.value,0) as delivery_charges,od.signature_image,od.parcel_image,rs.status,
                                        ro.delivery_longitude,ro.delivery_latitude,CONCAT (o.firstname,' ',o.lastname) AS customer_name
                                        ,o.store_name,o.payment_method
                                        FROM ri_riderorder ro
                                        LEFT OUTER JOIN " . DB_PREFIX . "order o ON ro.order_fromDB_id = o.order_id
                                        LEFT OUTER JOIN ri_riderorderstatus rs ON ro.order_status = rs.status_id                                        
                                        LEFT OUTER JOIN " . DB_PREFIX . "order_delivered od ON o.order_id = od.order_id
                                        LEFT OUTER JOIN " . DB_PREFIX . "order_total ot ON o.order_id = ot.order_id
                                        AND ot.code = 'low_order_fee' 
                                        WHERE ro.rider_id =".$rider_id." AND ro.access_datetime >= DATE(NOW()) - INTERVAL 2 DAY";



		if($status_id > 0){
			$sql .= " AND rs.status_id = ". $status_id;
		}

		$sql .= " ORDER BY o.order_id DESC";


		//$orders_query = $this->db->query("SELECT o.order_id, o.date_added,o.order_id,o.total,o.payment_address_1,o.shipping_address_1,o.telephone
//                                        ,os.name as order_status ,IFNULL(ot.value,0) as delivery_charges ,od.signature_type,od.signature_image,od.parcel_type,od.parcel_image  FROM " . DB_PREFIX . "order o
//                                        INNER JOIN " . DB_PREFIX . "order_status os ON o.order_status_id = os.order_status_id
//                                        LEFT OUTER JOIN " . DB_PREFIX . "order_delivered od ON o.order_id = od.order_id
//                                        LEFT OUTER JOIN " . DB_PREFIX . "order_total ot ON o.order_id = ot.order_id
//                                        AND ot.code = 'low_order_fee'
//                                        WHERE os.language_id=1 ORDER BY o.order_id DESC");


		$orders_query = $this->db->query($sql);

		$orders = array();
		if (count($orders_query->rows) > 0) {

			foreach($orders_query->rows as $order)
			{
				$order['store_address'] = $store_address;
				$signImage    =  $order['signature_image'];
				$parcelImage  =  $order['parcel_image'];

				if(isset($signImage) && !empty($signImage))
				{
					$order['signature_image'] = $this->model_tool_image->resize($signImage, 470, 350);
				}
				if(isset($parcelImage) && !empty($parcelImage))
				{
					$order['parcel_image'] = $this->model_tool_image->resize($parcelImage, 470, 350);
				}
				$order_product_query = $this->db->query("SELECT op.`name`,op.`quantity`,op.`price`,op.`total`,op.`tax`,p.image,p.product_id,p.upc
                                                        FROM " . DB_PREFIX . "order_product op 
                                                        LEFT JOIN " . DB_PREFIX . "product p ON  op.product_id = p.product_id
                                                        WHERE order_id = '" . (int)$order['order_id'] . "'");

				$allProducts = $order_product_query->rows;


				foreach($allProducts as $key => $value)
				{
					$allProducts[$key]['image'] = $this->model_tool_image->resize($allProducts[$key]['image'], 470, 350);
				}

				$order['items'] = $allProducts;

				$orders[] = $order;

			}

			return $orders ;


		} else {

			return  "No Order found!";
		}
	}

	public function addSignature()
	{
		$data = $this->request->post;

		// Check if already data exist against order and customer
		$orderInfo =  $this->db->query("SELECT * FROM  " . DB_PREFIX . "order_delivered WHERE  order_id ='".$data["order_id"]."' AND customer_id='".$data["customer_id"]."'");

		if($orderInfo->num_rows > 0)
		{
			$imageStatus = $this->uploadImage($data["signature_image"],$data["signature_type"]);

			if($imageStatus != "failed")
			{
				// Update and set values
				$this->db->query(" UPDATE " . DB_PREFIX . "order_delivered SET  signature_image = '".$imageStatus."' WHERE  order_id ='".$data["order_id"]."' AND customer_id='".$data["customer_id"]."'");

				$res = "success";
			}
			else{
				$res = "failed";
			}

		}
		else{
			$imageStatus = $this->uploadImage($data["signature_image"],$data["signature_type"]);

			if($imageStatus != "failed")
			{
				// Insert Values
				$sql = "INSERT INTO  " . DB_PREFIX . "order_delivered(order_id,customer_id,signature_image,date_added) VALUES (".$data["order_id"].",".$data["customer_id"].",'".$imageStatus."' ,NOW())";
				$this->db->query($sql);

				$delivered_id =$this->db->getLastId();
				$res = ($delivered_id > 0) ? "success" : "failed";
			}
			else{
				$res = "failed";
			}
		}

		return $res;
	}
	public function addParcel()
	{
		$data = $this->request->post;

		// Check if already data exist against order and customer
		$orderInfo =  $this->db->query("SELECT * FROM  " . DB_PREFIX . "order_delivered WHERE  order_id ='".$data["order_id"]."' AND customer_id='".$data["customer_id"]."'");

		if($orderInfo->num_rows > 0)
		{
			$imageStatus = $this->uploadImage($data["parcel_image"],$data["parcel_type"]);

			if($imageStatus != "failed")
			{
				// Update and set values
				$this->db->query(" UPDATE " . DB_PREFIX . "order_delivered SET  parcel_image = '".$imageStatus."' WHERE  order_id ='".$data["order_id"]."' AND customer_id='".$data["customer_id"]."'");

				$res = "success";
			}
			else{
				$res = "failed";
			}

		}
		else{
			$imageStatus = $this->uploadImage($data["parcel_image"],$data["parcel_type"]);

			if($imageStatus != "failed")
			{
				// Insert Values
				$sql = "INSERT INTO  " . DB_PREFIX . "order_delivered(order_id,customer_id,parcel_image,date_added) VALUES (".$data["order_id"].",".$data["customer_id"].",'".$imageStatus."' ,NOW())";
				$this->db->query($sql);

				$delivered_id =$this->db->getLastId();
				$res = ($delivered_id > 0) ? "success" : "failed";
			}
			else{
				$res = "failed";
			}
		}

		return $res;
	}



	function uploadImage($image64,$imageType)
	{

		$directory ='rider/order_delivered/';
		DIR_IMAGE .
		//define('UPLOAD_DIR', 'images/');
		$img = $image64;
		$img = str_replace('data:image/'.$imageType.';base64,', '', $img);
		$img = str_replace(' ', '+', $img);
		$data = base64_decode($img);
		$fileName = $directory . uniqid() . '.'.$imageType;
		$file = DIR_IMAGE .$fileName;
		$success = file_put_contents($file, $data);

		return $success ? $fileName : 'failed';

	}
	function uploadAnyImage($image64,$imageType,$directoryPath)
	{

		$directory =  $directoryPath;

		//Check if the directory already exists.
		if(!is_dir(DIR_IMAGE .$directory)){
			//Directory does not exist, so lets create it.
			mkdir(DIR_IMAGE .$directory, 0755, true);
		}

		if(is_dir(DIR_IMAGE .$directory)){
			$img = $image64;
			$img = str_replace('data:image/'.$imageType.';base64,', '', $image64);
			$img = str_replace(' ', '+', $img);
			$data = base64_decode($img);
			$fileName = $directory.'/'.  uniqid() . '.'.$imageType;

			$file = DIR_IMAGE .$fileName;
			$success = file_put_contents($file, $data);
		}


		return $success ? $fileName : 'failed';

	}
	public function addProductInvoice($data)
	{


		$imageStatus = $this->uploadAnyImage($data["invoice_image"],$data["image_type"],'rider/invoices/'.$data['order_id']);

		if($imageStatus != "failed")
		{
			// Insert Values
			$sql = "INSERT INTO  " . DB_PREFIX . "order_product_invoice(order_id,product_id,image,date_added) VALUES (".$data["order_id"].",".$data["product_id"].",'".$imageStatus."' ,NOW())";

			$this->db->query($sql);

			$invoice_id =$this->db->getLastId();
			$res = ($invoice_id > 0) ? "success" : "failed";
		}
		else{
			$res = "failed";
		}


		return $res;
	}

	function startShopping($uData){

		$this->db->query("UPDATE ri_riderorder set order_status=6 WHERE 	order_fromDB_id='" . $uData["order_id"] . "' AND 	rider_id='" . $uData["rider_id"] . "'");

		$this->load->language('rider/rider');
		$langId = 1;
		if($langId==1){
			$data =  array("en" => $this->language->get('text_start_shopping'));
		}else{
			$data =  array("ar" => $this->language->get('text_start_shopping'));
		}


		$userData = $this->getCustomerDeviceByOrderId($uData['order_id']);


		if(!empty($userData) && isset($userData)){
			if(isset($userData['mobile_user_id']) && isset($userData['mobile_reg_id'])){

				$userId = array($userData['mobile_user_id']);
				$regId  = array($userData['mobile_reg_id']);
				$response = $this->sendPushNotification($data, $userId,$regId);


			}
		}

		return "success";

	}

	function outForDelivery($udata){

		$this->load->language('rider/rider');

		$langId = 1;
		if($langId==1){
			$data =  array("en" => $this->language->get('text_ofd'));
		}else{
			$data =  array("ar" => $this->language->get('text_ofd'));
		}


		$userData = $this->getCustomerDeviceByOrderId($udata['order_id']);

		if(!empty($userData) && isset($userData)){
			if(isset($userData['mobile_user_id']) && isset($userData['mobile_reg_id'])){

				$userId = array($userData['mobile_user_id']);
				$regId  = array($userData['mobile_reg_id']);
				$response = $this->sendPushNotification($data, $userId,$regId);


			}
		}



		return "success";

	}


	function askForSubstitution($uData){


		$this->load->language('rider/rider');
		$this->load->model('account/order');
		$langID =  1;
		$product = $this->model_account_order->getOrderProductDetail($uData['order_id'],$uData['product_id'],$langID);


		$riderInfo = $this->getRiderByOrderId($uData['order_id']);

		if($langID==1){
			$data =  array("en" => $this->language->get('text_substitute').' '.$product['name'] .'. '.$this->language->get('text_rider').' \n '.$riderInfo['name'].': '.$riderInfo['contact_no']);
		}else{
			$data =  array("ar" => $this->language->get('text_substitute').' '.$product['name'] .' '.$this->language->get('text_rider').' \n '.$riderInfo['name'].':  '.$riderInfo['contact_no']);
		}


		$userData = $this->getCustomerDeviceByOrderId($uData['order_id']);

		if(!empty($userData) && isset($userData)){
			if(isset($userData['mobile_user_id']) && isset($userData['mobile_reg_id'])){

				$userId = array($userData['mobile_user_id']);
				$regId  = array($userData['mobile_reg_id']);
				$response = $this->sendPushNotification($data, $userId,$regId);


			}
		}

		print_r($response);
		exit;
		return "success";

	}



	function substitutionNotification($data){

		$this->load->language('rider/rider');
		$this->load->model('account/order');

		$product = $this->model_account_order->getOrderProductDetail($orderId,$productId,$data['lang_id']);

		if($data['lang_id']==1){
			$data =  array("en" => $this->language->get('text_substitute').' '.$product['name']);
		}else{
			$data =  array("ar" => $this->language->get('text_substitute').' '.$product['name']);
		}


		$userData = $this->getCustomerDeviceByOrderId($data['order_id']);

		if(!empty($userData) && isset($userData)){
			if(isset($userData['mobile_user_id']) && isset($userData['mobile_reg_id'])){

				$userId = array($userData['mobile_user_id']);
				$regId  = array($userData['mobile_reg_id']);
				$response = $this->sendPushNotification($data, $userId,$regId);

				return "success";
			}
		}



		return "success";

	}

	function addSubstitution($data){
		$this->load->language('rider/rider');
		$langId = 1;
		// Required Parameters
		// 1-  New Product Id
		// 2- Old Product Id
		// 3- Seller Id
		// 4- Choice By customer/Rider  0 = customer , 1= rider
		// 5- Order_Id
		// 6- New Product Quantity
		// is_substitute = 1 True, 0 False and remove item from orderProducts


		// Tables to Update with Columns

		//1- oc_order                   | total
		//2- oc_order_product           | Insert new product and Remove Old Product
		//3- oc_order_total             | Remove Old Heads and Insert New Calculated Heads  e.g Sub_total,Vat,Delivery_charges,Total


		// Steps To Follow
		//0- Create History of oc_order ,  oc_order_product, oc_order_total
		//1- Get New Product and Add Product into Order Products Table
		//2- Remove Old Product
		//3- Get order latest products and Calculate Sub-Total, Vat , Delivery Charges and Total
		//4- Remove OLD heads from oc_order_total and Insert New Heads
		//5- Update total column in oc_order Table

		$newProductId  = $data['new_product_id'];
		$oldProductId  = $data['old_product_id'];
		$sellerId      = $data['seller_id'];
		$productChoice      = $data['product_choice'];
		$orderId       = $data['order_id'];
		$quantity      = $data['quantity'];
		$is_substitute = $data['is_substitute'];



		$this->load->model('catalog/product');
		$this->load->model('account/order');
		$this->load->model('saccount/seller');
		$this->load->model('checkout/order');


		// Order Tables History
		$this->model_checkout_order->createOrderHistory($orderId);

		$isProductAddDelete = false;
		if($is_substitute > 0) // Get And Add Product in OrderProductsTable
		{
			$product =  $this->model_catalog_product->getProductBySellerIdAPI($newProductId,$sellerId,1);

			if($product){
				$order_data['product'] = array();


				$option_data = array();

				foreach ($product['option'] as $option) {
					$option_data[] = array(
						'product_option_id'       => $option['product_option_id'],
						'product_option_value_id' => $option['product_option_value_id'],
						'option_id'               => $option['option_id'],
						'option_value_id'         => $option['option_value_id'],
						'name'                    => $option['name'],
						'value'                   => $option['value'],
						'type'                    => $option['type']
					);
				}

				if ($product['special'] == null) {
					$order_data['product'] = array(
						'product_id' => $product['product_id'],
						'name' => $product['name'],
						'model' => $product['model'],
						'option' => $option_data,
						'download' => array(),
						'quantity' => $quantity,
						'subtract' => $product['subtract'],
						'price' => ($product['price'] * 1),
						'total' => ($product['price'] * (int) $quantity),
						'tax' => $this->tax->getTax($product['price'], $product['tax_class_id']),
						'reward' => $product['reward'],
						'product_choice' => $productChoice,
						'added_later' =>'1'
					);

				} else {
					$order_data['product'] = array(
						'product_id' => $product['product_id'],
						'name' => $product['name'],
						'model' => $product['model'],
						'option' => $option_data,
						'download' => array(),
						'quantity' => $quantity,
						'subtract' => $product['subtract'],
						'price' => ($product['special'] * 1),
						'total' => ($product['special'] * (int) $quantity),
						'tax' =>$this->tax->getTax($product['price'], $product['tax_class_id']),
						'reward' => $product['reward'],
						'product_choice' => $productChoice,
						'added_later' =>'1'
					);

				}

				$orderProductId =  $this->model_checkout_order->addOrderProduct($order_data['product'],$orderId,$oldProductId);

				if(isset($orderProductId)){

					$isProductAddDelete = true;
				}


				if($langId==1){
					$data1 =  array("en" => $this->language->get('text_add_substitute').' '.$product['name']);
				}else{
					$data1 =  array("ar" => $this->language->get('text_add_substitute').' '.$product['name']);
				}


				$userData = $this->getCustomerDeviceByOrderId($orderId);

				if(!empty($userData) && isset($userData)){
					if(isset($userData['mobile_user_id']) && isset($userData['mobile_reg_id'])){

						$userId = array($userData['mobile_user_id']);
						$regId  = array($userData['mobile_reg_id']);
						$response = $this->sendPushNotification($data1, $userId,$regId);


					}
				}

			}
		}else{ // Remove Product

			$isProductDeleted = $this->model_checkout_order->removeOrderProduct($orderId,$oldProductId);
			if(isset($isProductDeleted)){
				$isProductAddDelete = true;
			}
			$product = $this->model_account_order->getOrderProductDetail($orderId,$oldProductId,$langId);


			if($langId==1){
				$data1 =  array("en" => $this->language->get('text_remove_substitute').' '.$product['name']);
			}else{
				$data1 =  array("ar" => $this->language->get('text_remove_substitute').' '.$product['name']);
			}


			$userData = $this->getCustomerDeviceByOrderId($orderId);

			if(!empty($userData) && isset($userData)){
				if(isset($userData['mobile_user_id']) && isset($userData['mobile_reg_id'])){

					$userId = array($userData['mobile_user_id']);
					$regId  = array($userData['mobile_reg_id']);
					$response = $this->sendPushNotification($data1, $userId,$regId);


				}
			}
		}


		if($isProductAddDelete){

			$total =0;

			//if Product Added/Deleted Get ALL New Products and Calculate Total Heads
			$orderProducts = $this->model_checkout_order->getOrderProducts($orderId);

			foreach ($orderProducts as $product){

				$total =  ($total + $product['total']);
			}

			//Get Order Already added Total Heads
			$orderTotals = $this->model_checkout_order->getOrderTotal($orderId);
			if($orderTotals){

				foreach ($orderTotals as $orderTotal){

					if($orderTotal['code'] == 'sub_total'){
						$total_data[0] = array(
							'code' => 'sub_total',
							'title' => 'Sub-Total',
							'text' => 'Sub-Total',
							'value' => $total,
							'sort_order' => 0
						);
					}
					else if($orderTotal['code'] == 'vat'){
						$this->load->language('total/vat');
						$vat =  (float)$this->config->get('config_vat');
						if(isset($vat) && $vat > 0){
							$vat_total = $total * $vat / ($vat+100);
						}
						$total_data[1]   = array(
							'code' => 'vat',
							'title' => $this->language->get('text_vat').'('.$vat.' %)',
							'value' => round($vat_total,2),
							'sort_order' => 1
						);
					}
					else if($orderTotal['code'] == 'dc' || $orderTotal['title'] =='Delivery Charges' ){
						$delivery_charges = $this->model_saccount_seller->getSellerDeliveryCharges($sellerId);

						$dCharges = (isset($delivery_charges)) ? (int)$delivery_charges['delivery_charges'] : 0;
						$total = (float) $total + (float)$dCharges;
						$total_data[2] = array(
							'code'       => 'dc',
							'title'      => 'Delivery Charges',
							'value'      => $dCharges,
							'sort_order' => 2
						);
					}
					else if($orderTotal['code'] == 'total'){

						$total_data[5] = array(
							'code' => 'total',
							'title' => 'Total',
							'value' => $total,
							'sort_order' => 5
						);
					}

				}

			}
			$order_total['totals'] = $total_data;


			$this->model_checkout_order->addOrderTotal($order_total,$orderId);


			//Update Order Table with new ordertotal
			$this->model_checkout_order->updateOrderTotal($orderId,$total);

			return "success";
		}else{
			return "failed";
		}
	}

	public function orderPayment($data){
		$this->db->query("UPDATE " . DB_PREFIX . "order SET payment_status='".$data['payment_status']."',payment_reason='".$data['payment_reason']."' WHERE order_id='".$data['order_id']."'");
		return "success";
	}

	public function  checkProductBarCode($data){
		$getProductBarCode =  $this->db->query("SELECT upc FROM " . DB_PREFIX . "product  WHERE product_id='" . $data["product_id"] . "'");

		$barCode = $getProductBarCode->row;

		if(isset($barCode) && !empty($barCode['upc'])){
			//Case 1: If barcode is same
			//Case 2: If barcode is not same
			//Case 3: If barcode is not found

			//return ($barCode['upc'] == $data["upc"]) ? 'success' : 'failed';
			if($barCode['upc'] == $data["upc"]){

				return 'success';
			}else{

				$this->db->query("INSERT INTO " . DB_PREFIX . "product_upc SET product_ID = '" . $data["product_id"] . "',upc = '" . $data["upc"] . "',rider_id = '" . $data["rider_id"] . "',date_added = NOW()") ;
				$upcId =$this->db->getLastId();

				return ($upcId > 0) ? 'success' : 'failed';
			}

		}else{ //Case 3
			$this->db->query("INSERT INTO " . DB_PREFIX . "product_upc SET product_ID = '" . $data["product_id"] . "',upc = '" . $data["upc"] . "',rider_id = '" . $data["rider_id"] . "',date_added = NOW()") ;
			$upcId =$this->db->getLastId();

			return ($upcId > 0) ? 'success' : 'failed';
		}
	}


	private function getCustomerDeviceByOrderId($orderId){

		$query = $this->db->query("SELECT c.mobile_user_id,c.mobile_reg_id FROM oc_order o INNER JOIN oc_customer c ON(o.customer_id=c.customer_id)
	        WHERE o.order_id='".$orderId."'");

		$res=$query->row;

		return $res;

	}

}
