<?php
class ModelRiderDal extends Model {

	
	public function addRider($data) {
	    
       $riderId =  (int) $data["riderID"];
      //$riderInfo =   $this->db->query("Select * FROM ri_riderinfo WHERE email= '".$data['email']."'");
      
       if($riderId > 0 )
       {
         $this->db->query("UPDATE ri_riderinfo SET name ='" . $data["name"] . "' , email ='" . $data['email'] . "' , password ='" . $data["password"] . "' , contact_no ='" . $data["contact_no"] . "', address ='" . $data["address"] . "', license ='" . $data["license"] . "' , rider_status ='" . $data["rider_status"] . "' WHERE rider_id ='" . $riderId . "'");
         $rider_id = $riderId;
       } 
       else{
        
        $this->db->query("INSERT INTO ri_riderinfo ( email, name, password, contact_no, address, license, is_active, is_online, rider_status, rider_pic, device_ID) VALUES ('". $data['email']."','". $data['name']."','". $data['password']."','". $data['contact_no']."','". $data['address']."','". $data['license']."','". $data['is_active']."','". $data['is_online']."','". $data['rider_status']."','". $data['rider_pic']."','". $data['device_ID']."')");
		
		$rider_id = $this->db->getLastId();
       }

		return $rider_id;
	}
    
    public function deleteRider($data)
    {
        //Delete from riderInfo        
        $this->db->query("DELETE FROM ri_riderinfo  WHERE rider_id ='" . $data["UserID"] . "'");
        
        //Delete from riderInfo Area        
        $this->db->query("DELETE FROM ri_riderinfoareas  WHERE rider_id ='" . $data["UserID"] . "'");
        
        //Delete from rider Availability    
        $this->db->query("DELETE FROM ri_rideravailability  WHERE rider_id ='" . $data["UserID"] . "'");
        return "success";
    }
    
	public function ChangeUserActiveStatus($data) {

		$this->db->query("UPDATE ri_riderinfo SET is_active ='" . $data["status"] . "' WHERE rider_id ='" . $data["UserID"] . "'");
		return "success";
	}
    	public function ChangeUserOnlineStatus($data) {

		$this->db->query("UPDATE ri_riderinfo SET is_online ='" . $data["status"] . "' WHERE rider_id ='" . $data["UserID"] . "'");
		return "success";
	}
    
    public function getPendingMainOrders($data) {
	 $query = $this->db->query("SELECT order_id,order_status_id,date_added,total,shipping_address_1,firstname as CustomerName FROM oc_order WHERE order_status_id=1");
     $res=$query->rows;
     
     return $res;
	}
    
	
       public function getMainOrderDetailsByOrderID($data) {
	 $query1 = $this->db->query("SELECT order_id,order_status_id,date_added,total,shipping_address_1,firstname as customerName from oc_order WHERE order_id='" . $data["orderID"] . "'");
     $res1=$query1->rows;
     
     $query2 = $this->db->query("SELECT op.order_id,op.product_id,op.`name`,op.quantity,op.price,op.total,p.upc as productBarCode FROM oc_order_product op INNER JOIN oc_product p on op.product_id=p.product_id WHERE op.order_id='". $data["orderID"] . "'");
     $res2=$query2->rows;
     
     $resCollec[0]=$res1;
     $resCollec[1]=$res2;
     
     return $resCollec;
	}
    
    
    	public function AcceptOrder($data) {
       
		$this->db->query("UPDATE ri_riderorder set order_status=2 WHERE Order_id='" . $data["order_id"] . "'");
        
        
        
         //send push notification                     
   $dataF =  array
    (
	'contentTitle' 	=> 'Order Accepted',
	'riderOrderID'		=> $data["order_id"]
	
    );
   
     $ids = array( $this->GetShopperDevice_idByOrderID($data['order_id'])['device_ID'] );
     $this->sendPushNotification($dataF, $ids);
        
		return "success";
	    }
        
        	public function RejectOrder($data) {
		$this->db->query("UPDATE ri_riderorder set order_status=3 WHERE Order_id='" . $data["order_id"] . "'");
        $this->db->query("UPDATE oc_order SET order_status_id=1 where order_id=(SELECT order_fromDB_id from ri_riderorder where order_id='" . $data["order_id"] . "')");
        
        $dataF =  array
    (
	'contentTitle' 	=> 'Order Rejected',
	'riderOrderID'		=> $data["order_id"]
	
    );
   
     $ids = array( $this->GetShopperDevice_idByOrderID($data['order_id'])['device_ID'] );
     $this->sendPushNotification($dataF, $ids);
        
        
        
		return "success";
	    }
        
        public function OrderDelivered($data) {
        
		$this->db->query("UPDATE ri_riderorder set order_status=4 WHERE Order_id='" . $data["order_id"] . "'");
        $this->db->query("UPDATE oc_order SET order_status_id=19 where order_id=(SELECT order_fromDB_id from ri_riderorder where order_id='" . $data["order_id"] . "')");
	
    $dataF =  array
    (
	'contentTitle' 	=> 'Order Delivered',
	'riderOrderID'		=> $data["order_id"]
	
    );
   
     $ids = array( $this->GetShopperDevice_idByOrderID($data['order_id'])['device_ID'] );
     $this->sendPushNotification($dataF, $ids);
    
    
    
    	return "success";
	    }
     public function OrderDeliveredToRider($data) {
      
		$this->db->query("UPDATE ri_riderorder set order_status=5 WHERE Order_id='" . $data["order_id"] . "'");
		return "success";
	    }
    
         public function TrackOrder($data) {
	 $query = $this->db->query("SELECT o.Order_id,o.delivery_address,rc.latitude as latitude_rider,rc.longitute as longitude_rider,
              o.delivery_latitude,o.delivery_longitude
              from ri_ridercurrentlocations rc INNER JOIN
              ri_riderorder o on rc.riderID=o.rider_id
              WHERE o.Order_id='" . $data["order_id"] . "' ORDER BY rc.dateTime_c DESC LIMIT 1");
     $res=$query->rows;
     
     return $res;
	}
    
     public function GetUserOrderByStatus($data) {
	 $query = $this->db->query("SELECT * from ri_riderorder WHERE rider_id='" . $data["rider_id"] . "' AND order_status='" . $data["status"] . "'");
     $res=$query->rows;
     
     return $res;
	}
    
     public function GetAssignerOrderByStatus($data) {
	 $query = $this->db->query("SELECT o.Order_id,o.rider_id,o.order_fromDB_id,o.order_date,o.order_amount,o.order_status,o.delivery_address,
                               o.commission_amount,o.access_datetime,o.delivery_latitude,o.delivery_longitude 
                               FROM ri_shoppertoriderassignment s INNER JOIN ri_riderorder o
                               on s.order_id=o.Order_id
                                where s.shopper_id='" . $data["shopper_id"] . "' AND o.order_status='" . $data["status"] . "'");
     $res=$query->rows;
     
     return $res;
	}
    
    
    
    public function UpdateProductBarCode($data) {
	 $query = $this->db->query("update oc_product set upc='" . $data["barCode"] . "' where product_id='" . $data["product_id"] . "'");

     
     return "success";
	}
    
	
	public function loginRider($data) {
	
	 $query = $this->db->query("SELECT ri.*,a.area_name FROM ri_riderinfo ri LEFT OUTER JOIN ri_riderinfoareas ria on ri.rider_id=ria.rider_id LEFT OUTER JOIN ri_areas a on ria.area_id=a.area_id WHERE ri.email='".$data["email"]."' AND password='".$data["password"]."'");
 
     $res=$query->row;
     if($query->num_rows > 0)
     {
        	$this->db->query("UPDATE ri_riderinfo SET device_ID = '" . $data["device_ID"] . "' WHERE rider_id = '" . $res["rider_id"] . "'");
          //  echo "INSERT into ri_usersession(user_id,loginTime,lastActivityTime)VALUES(".$res["rider_id"].", NOW(), NOW() )";
          	$this->db->query("INSERT into ri_usersession(user_id,loginTime,lastActivityTime)VALUES(".$res["rider_id"].", NOW(), NOW() )");
            return $res;
     }
     else
     {
        $res="user not found";
         return $res;
     }
     //return null;
		

	}
    
    	public function GetRiderBy_ID($data) {
	 //$query = $this->db->query("select * from ri_riderinfo where rider_id='".$data["id"]."'");
     
     $query = $this->db->query("SELECT ri.*,a.area_name FROM ri_riderinfo ri LEFT OUTER JOIN ri_riderinfoareas ria on ri.rider_id=ria.rider_id LEFT OUTER JOIN ri_areas a on ria.area_id=a.area_id WHERE ri.rider_id='".$data["id"]."'");
     $res=$query->row;
     
     return $res;
	}
    	public function changePassword($data) {
    	   
        $rider_id = (int)$data["rider_id"];
        $oldPassword =  $data["old_password"];
        $newPassword =  $data["new_password"]; 
        $res = array("msg"=>"Failed"); 
	 $query = $this->db->query("select * from ri_riderinfo where rider_id='".$rider_id."' AND password='".$oldPassword."'");
      if(count($query->rows) > 0)
      {
        $this->db->query("UPDATE ri_riderinfo SET password='".$newPassword."' where rider_id='".$rider_id."'");
        
        $res = array("msg"=>"Success"); 
        
      }
      
     
     return $res;
	}
	public function GetRiderByEmail($data) {
	  $caseType =  (int)$data['caseType']; 
      
       	 $query = $this->db->query("select * from ri_riderinfo where email='".$data["email"]."'");
     
       //Add Case
       if($caseType ==0)
       {
            if(count($query->rows) > 0)
            {
                $res = true;
            }
            else{
                $res = false;
            }
       }
       else if($caseType ==1) // Edit Case
       {
            if(count($query->rows) > 1)
            {
                $res = true;
            }
            else{
                $res = false;
            }
       }
        
     return $res;
	}
    
    	public function AddRiderCurrentLocation($data) {
	  $this->db->query("INSERT INTO ri_ridercurrentlocations ( riderID,latitude,longitute,dateTime_c)  VALUES ('". $data['riderID']."','". $data['latitude']."','". $data['longitute']."','". $data['dateTime_c']."')");
     $res=$this->db->getLastId();
     
     return $res;
		
	}
    
     	public function GetRiderLastLocation($data) {
	  $query = $this->db->query("select * from ri_ridercurrentlocations where riderID='".$data["riderID"]."' ORDER BY dateTime_c desc LIMIT 1");
      $res=$query->row;
     
     return $res;
		
	}
    
    public function GetUsersByStatus($data) {
        
      $sqlQuery  = "SELECT ri.*,a.area_name,a.city FROM ri_riderinfo ri INNER JOIN  ri_riderinfoareas ria on ri.rider_id=ria.rider_id INNER JOIN ri_areas a
                      on ria.area_id=a.area_id  WHERE ri.email != ''";
     
        if(isset($data['is_active']))
        {
           $sqlQuery .= " AND ri.is_active = ".$data['is_active']; 
        }
        
        if(isset($data['is_online'])){
             $sqlQuery .= " AND ri.is_online = ".$data['is_online']; 
        }
        
        if(isset($data['status'])){
             $sqlQuery .= " AND ri.rider_status = ".$data['status']; 
        }
        
        
        
        $query = $this->db->query($sqlQuery);
        return $query->rows;
	}
    //public function getRidersByGivenTimeLimit($data)
//    {
//        
//        $varTimeIn =  substr($data['day'],0,3)."_time_in";
//        $varTimeOut = substr($data['day'],0,3)."_time_out";
//        
//        $timeIn = date("H:m",strtotime($data['timeIn']));
//        $timeOut = date("H:m",strtotime($data['timeOut']));
//            
//        
//          $sqlquery =  "SELECT ri.rider_id,ri.name , ri.email, a.area_name  FROM ri_rideravailability ra INNER JOIN ri_riderinfo ri
//                      ON ra.rider_id=ri.rider_id  INNER JOIN  ri_riderinfoareas ria on ri.rider_id=ria.rider_id INNER JOIN ri_areas a
//                      on ria.area_id=a.area_id WHERE " . $data['day'] ."= 1 AND CONVERT(".$varTimeIn. ",TIME) <= '".$timeIn."' AND 
//                      CONVERT(".$varTimeOut. ",TIME) >= '".$timeIn."' AND ri.is_active=1 AND ri.is_online=1 AND
//                      ri.rider_status in (1,3) and a.city='".$data['city']."'";
//          
//           $sqlquery =  "SELECT ri.rider_id,ri.name , ri.email, a.area_name  FROM ri_rideravailability ra INNER JOIN ri_riderinfo ri
//                      ON ra.rider_id=ri.rider_id  INNER JOIN  ri_riderinfoareas ria on ri.rider_id=ria.rider_id INNER JOIN ri_areas a
//                      on ria.area_id=a.area_id WHERE " . $data['day'] ."= 1 AND CONVERT(".$varTimeIn. ",TIME) >= '".$timeIn."' AND 
//                      CONVERT(".$varTimeOut. ",TIME) <= '".$timeOut."' AND ri.is_active=1 AND ri.is_online=1 AND
//                      ri.rider_status in (1,3) and a.city='".$data['city']."'";
//
//       die($sqlquery);
//       $query =  $this->db->query($sqlquery);
//        $res=$query->rows;
//        
//        return $res;
//    }
    public function getRidersByGivenTimeLimit($data)
    {
        
        $varTimeIn =  substr($data['day'],0,3)."_time_in";
        $varTimeOut = substr($data['day'],0,3)."_time_out";
         
         $timeIn = date("H:i",strtotime(str_replace('+',' ',$data['timein'])));
        
         $sqlquery =  "SELECT ri.rider_id,ri.name , ri.email, a.area_name  FROM ri_rideravailability ra INNER JOIN ri_riderinfo ri
                      ON ra.rider_id=ri.rider_id  INNER JOIN  ri_riderinfoareas ria on ri.rider_id=ria.rider_id INNER JOIN ri_areas a
                      on ria.area_id=a.area_id WHERE " . $data['day'] ."= 1 AND ri.is_active=1 AND ri.is_online=1 AND
                      ri.rider_status in (1,3) and a.city='".$data['city']."' AND CONVERT('".$timeIn. "',TIME) >= CONVERT(".$varTimeIn. ",TIME) AND 
                      CONVERT('".$timeIn."',TIME) <= CONVERT(".$varTimeOut. ",TIME)";


                      
                      

          $query =  $this->db->query($sqlquery);
        $res=$query->rows;
        
        return $res;
    }
      public function getRidersByGivenTimeLimitV2($data)
    {
        
        $varTimeIn =  substr($data['day'],0,3)."_time_in";
        $varTimeOut = substr($data['day'],0,3)."_time_out";
         
         $timeIn = date("H:i",strtotime(str_replace('+',' ',$data['timein'])));
        
         $sqlquery =  "SELECT ri.rider_id,ri.name , ri.email, a.area_name  FROM ri_rideravailability ra INNER JOIN ri_riderinfo ri
                      ON ra.rider_id=ri.rider_id  INNER JOIN  ri_riderinfoareas ria on ri.rider_id=ria.rider_id INNER JOIN ri_areas a
                      on ria.area_id=a.area_id WHERE " . $data['day'] ."= 1 AND ri.is_active=1 AND ri.is_online=1 AND
                      ri.rider_status in (1,3) and a.area_name LIKE '%".$data['city']."%' AND CONVERT('".$timeIn. "',TIME) >= CONVERT(".$varTimeIn. ",TIME) AND 
                      CONVERT('".$timeIn."',TIME) <= CONVERT(".$varTimeOut. ",TIME)";

          $query =  $this->db->query($sqlquery);
        $res=$query->rows;
        
        return $res;
    }
    public function updateRiderAvailability($data)
    {

       // Check If rider exists
        $riderInfo =  $this->db->query("SELECT * FROM ri_rideravailability WHERE rider_id = ".$data['rider_id']);
      
        $res = "failed";
        if(count($riderInfo->rows) > 0){
            $updateAvailabilty =  "UPDATE `ri_rideravailability` SET `saturday`=".$data['saturday']." ,`sat_time_in`= '".strToDateTime($data['sat_time_in'])."',`sat_time_out`= '".strToDateTime($data['sat_time_out'])."',
                                    `sunday`= ".$data['sunday'].",`sun_time_in`='".strToDateTime($data['sun_time_in'])."',`sun_time_out`= '".strToDateTime($data['sun_time_out'])."',
                                    `monday`= ".$data['monday'].",`mon_time_in`= '".strToDateTime($data['mon_time_in'])."',`mon_time_out`= '".strToDateTime($data['mon_time_out'])."',
                                     `tuesday`= ".$data['tuesday'].",`tue_time_in`= '".strToDateTime($data['tue_time_in'])."',`tue_time_out`='".strToDateTime($data['tue_time_out'])."',
                                     `wednesday`=".$data['wednesday'].",`wed_time_in`= '".strToDateTime($data['wed_time_in'])."',`wed_time_out`= '".strToDateTime($data['wed_time_out'])."',
                                     `thursday`= ".$data['thursday'].",`thu_time_in`= '".strToDateTime($data['thu_time_in'])."' ,`thu_time_out`= '".strToDateTime($data['thu_time_out'])."',
                                     `friday`= ".$data['friday'].",`fri_time_in`= '".strToDateTime($data['fri_time_in'])."',`fri_time_out`= '".strToDateTime($data['fri_time_out'])."' 
                                       WHERE rider_id = ".$data['rider_id'];
            
          
            $this->db->query($updateAvailabilty);
            $res = "success";
        }
        else{
           $insertAvailabilty  =  "INSERT INTO `ri_rideravailability` (`rider_id`, `saturday`, `sat_time_in`, `sat_time_out`, `sunday`, `sun_time_in`, `sun_time_out`, 
                                                    `monday`, `mon_time_in`, `mon_time_out`, `tuesday`, `tue_time_in`, `tue_time_out`, `wednesday`, 
                                                    `wed_time_in`, `wed_time_out`, `thursday`, `thu_time_in`, `thu_time_out`, `friday`, `fri_time_in`,
                                                    `fri_time_out`) 
                                                    VALUES ('".$data['rider_id']."', ".$data['saturday'].", '".strToDateTime($data['sat_time_in'])."', '".strToDateTime($data['sat_time_out'])."',
                                                     ".$data['sunday'].",'".strToDateTime($data['sun_time_in'])."', '".strToDateTime($data['sun_time_out'])."', 
                                                     ".$data['monday'].", '".strToDateTime($data['mon_time_in'])."', '".strToDateTime($data['mon_time_out'])."',
                                                      ".$data['tuesday'].", '".strToDateTime($data['tue_time_in'])."', '".strToDateTime($data['tue_time_out'])."', 
                                                      ".$data['wednesday'].", '".strToDateTime($data['wed_time_in'])."','".strToDateTime($data['wed_time_out'])."', 
                                                      ".$data['thursday'].", '".strToDateTime($data['thu_time_in'])."', '".strToDateTime($data['thu_time_out'])."', 
                                                      ".$data['friday'].", '".strToDateTime($data['fri_time_in'])."', '".strToDateTime($data['fri_time_out'])."')";

            /**
 * $insertAvailabilty  = "INSERT INTO `ri_rideravailability` SET `rider_id`=".$data['rider_id']." , `saturday`=".$data['saturday']." ,`sat_time_in`= '".strToDateTime($data['sat_time_in'])."',`sat_time_out`= '".strToDateTime($data['sat_time_out'])."',
 *                                     `sunday`= ".$data['sunday'].",`sun_time_in`='".strToDateTime($data['sun_time_in'])."',`sun_time_out`= '".strToDateTime($data['sun_time_out'])."',
 *                                     `monday`= ".$data['monday'].",`mon_time_in`= '".strToDateTime($data['mon_time_in'])."',`mon_time_out`= '".strToDateTime($data['mon_time_out'])."',
 *                                      `tuesday`= ".$data['tuesday'].",`tue_time_in`= '".strToDateTime($data['tue_time_in'])."',`tue_time_out`='".strToDateTime($data['tue_time_out'])."',
 *                                      `wednesday`=".$data['wednesday'].",`wed_time_in`= '".strToDateTime($data['wed_time_in'])."',`wed_time_out`= '".strToDateTime($data['wed_time_out'])."',
 *                                      `thursday`= ".$data['thursday'].",`thu_time_in`= '".strToDateTime($data['thu_time_in'])."' ,`thu_time_out`= '".strToDateTime($data['thu_time_out'])."',
 *                                      `friday`= ".$data['friday'].",`fri_time_in`= '".strToDateTime($data['fri_time_in'])."',`fri_time_out`= '".strToDateTime($data['fri_time_out'])."'";
 *             
 */
           
            $this->db->query($insertAvailabilty);
            $availability_id = $this->db->getLastId();
            $res = ($availability_id > 0) ? "success" : "failed";
            
        }
         return $res;
    }
    
    public function addRiderArea($data)
    {
        $riderinfoareas = $this->db->query("SELECT * FROM ri_riderinfoareas  WHERE  `rider_id`=".$data['rider_id']);
         if(count($riderinfoareas->rows) > 0)
         {
            $this->db->query("Update ri_riderinfoareas SET `area_id`=".$data['area_id']." WHERE  `rider_id`=".$data['rider_id']);
            $res = "success";
         }
         else{
            $query = $this->db->query("INSERT INTO ri_riderinfoareas SET `rider_id`=".$data['rider_id']." , `area_id`=".$data['area_id']);
        $rider_area_id = $this->db->getLastId();
        $res = ($rider_area_id > 0) ? "success" : "failed";
        
         }
     
        return $res;
    }
    public function getRiderAreasById($data)
    {
     
        $query = $this->db->query("SELECT * FROM ri_riderinfoareas ria INNER JOIN ri_areas ra ON ria.area_id = ra.area_id  WHERE  `rider_id`=".$data['rider_id']);
        $res=$query->rows;
        return $res;
    }
     public function getRiderAvailiblityByRiderId($data)
    {
     
        $query = $this->db->query("SELECT * FROM ri_rideravailability WHERE  `rider_id`=".$data['rider_id']);
        
         if($query->num_rows > 0)
         {
             $res=$query->rows;
        return $res;
         }
         else{
            return  "Data not found";
         }
               
    }
    
     public function addArea($data)
    {
     
        $query = $this->db->query("INSERT INTO ri_areas SET `area_name`= '".$data['area']."' , `city`= '".$data['city']."'");
        $area_id = $this->db->getLastId();
        $res = ($area_id > 0) ? "success" : "failed";
        return $res;
    }
     public function addAreaV2($data)
    {
        
     $areaInfo = $this->db->query("SELECT * FROM ri_areas WHERE `area_name`= '".$data['area']."'");
     if($areaInfo->num_rows > 0)
         {
            
             $res=$areaInfo->row['area_id'];
        
         }
         else{
           $query = $this->db->query("INSERT INTO ri_areas SET `area_name`= '".$data['area']."'");
        $area_id = $this->db->getLastId();
        $res = ($area_id > 0) ? $area_id: 0;
         }
        
        return $res;
    }
    public function getAreas()
    {
     
        $query = $this->db->query("SELECT * FROM ri_areas");
        $res=$query->rows;
        return $res;
    }
    
     public function assignOrder($data)
     {
        $insertOrderSql = "INSERT INTO `ri_riderorder` (`rider_id`, `order_fromDB_id`, `order_date`, `order_amount`, `order_status`, `delivery_address`, `delivery_longitude`, `delivery_latitude`, `commission_amount`, `access_datetime`) 
                            VALUES ('".$data['rider_id']."', ".$data['order_fromDB_id'].", '".$data['order_date']."' , ".$data['order_amount'].", 1, '".$data['delivery_address']."', '".$data['delivery_latitude']."', '".$data['delivery_longitude']."', ".$data['commission_amount'].", NOW())";
        
        
        $query = $this->db->query($insertOrderSql);
        $order_id = $this->db->getLastId();
       
        if($order_id > 0 ){
            $insertAssigner = "INSERT INTO `ri_shoppertoriderassignment` (`shopper_id`, `rider_id`, `order_id`, `access_datetime`) 
                                VALUES (".$data['order_assigner_id'].", ".$data['rider_id'].", ".$order_id.", NOW())";
                                
                            
            $query = $this->db->query($insertAssigner);
            $assignerId = $this->db->getLastId();
			$this->db->query("UPDATE oc_order SET order_status_id=2 where order_id='".$data['order_fromDB_id']."'");
      
            
            
      

    //send push notification                     
   $dataF =  array
    (
	'contentTitle' 	=> 'New Order Assigned',
	'riderOrderID'		=> $order_id,
	'MainOrderID'	=> $dataF['order_fromDB_id']
    );
   
     $ids = array( $this->GetDevice_id($data['rider_id'])['device_ID'] );
     $this->sendPushNotification($dataF, $ids);
          
              
        }
        
        $res = ($order_id > 0 && $assignerId > 0 ) ? $order_id : "failed";
        return $res;
     }
     
          public function assignOrderV2($data)
     {
        $order_id   = $data['rider_order_id'];
        $rider_id   = $data['rider_id'];
        $shoper_id  = $data['shoper_id'];
        
        //Select record fron riderorder table against shoper and orderId;
        $orderInfo = $this->db->query("SELECT * from ri_riderorder WHERE rider_id='" . $shoper_id . "' AND order_id='" . $order_id . "'");
       
          if(count($orderInfo->rows) > 0)
          {
            // Update status of shopper to rider 
            $this->db->query("Update ri_riderorder SET order_status = 5 WHERE rider_id='" . $shoper_id . "' AND order_id='" . $order_id . "'");
            
            
            
            $insertOrderSql = "INSERT INTO `ri_riderorder` (`rider_id`, `order_fromDB_id`, `order_date`, `order_amount`, `order_status`, `delivery_address`, `delivery_latitude`, `delivery_longitude`, `commission_amount`, `access_datetime`) 
                            VALUES ('".$rider_id."', ".$orderInfo->row['order_fromDB_id'].", '".$orderInfo->row['order_date']."' , ".$orderInfo->row['order_amount'].", 1, '".$orderInfo->row['delivery_address']."', '".$orderInfo->row['delivery_latitude']."', '".$orderInfo->row['delivery_longitude']."', ".$orderInfo->row['commission_amount'].", NOW())";
        
        
            $query = $this->db->query($insertOrderSql);
            $orderId = $this->db->getLastId();
            
            
            
                  if($orderId > 0 ){
            $insertAssigner = "INSERT INTO `ri_shoppertoriderassignment` (`shopper_id`, `rider_id`, `order_id`, `access_datetime`) 
                                VALUES (".$shoper_id.", ".$rider_id.", ".$orderId.", NOW())";
                                
                            
            $query = $this->db->query($insertAssigner);
            $assignerId = $this->db->getLastId();
			$this->db->query("UPDATE oc_order SET order_status_id=2 where order_id='".$orderInfo->row['order_fromDB_id']."'");
      
            
            
      

                //send push notification                     
               $dataF =  array
                (
            	'contentTitle' 	=> 'New Order Assigned',
            	'riderOrderID'		=> $orderId,
            	'MainOrderID'	=> $dataF['order_fromDB_id']
                );
               
                 $ids = array( $this->GetDevice_id($rider_id)['device_ID'] );
                 $this->sendPushNotification($dataF, $ids);
                      
                          
                    }
          }
        
        $res = ($orderId > 0 && $assignerId > 0 ) ? $orderId : "failed";
        return $res;
     }
    public function logOut($data) {
        
	  $this->db->query("UPDATE ri_usersession SET logoutTime = NOW() WHERE user_id=".$data['userID']);
     return "Success";	
	}
	
    function ptest()
    {
         $data =  array
    (
	'message' 	=> 'here is a message. message',
	'title'		=> 'This is a title. title',
	'subtitle'	=> 'This is a subtitle. subtitle',
	'tickerText'	=> 'Ticker text here...Ticker text here...Ticker text here'

   );

         $ids = array('dZBFgMs_n_A:APA91bHOWpqmxSkraOmj4o3-kCt4HRjCrMAZoZ4sg-I53MJOkRtVEkZgaDUWL4-6bP_d2Sg4l5g1_GwVrXPVZs79lUCulT4jXsPh7DLec0V8EN5368d7PU6EsY3PoQHeOo2UmJ4F5595');
         $this->sendPushNotification($data, $ids);
    }
   function sendPushNotification($data, $registrationIds) {
  
    $msg =$data;
    $fields = array
  (
	'registration_ids' 	=> $registrationIds,
	'data'			=> $msg
  );
 
  $headers = array
  (
	'Authorization: key=AAAAeDIkjlA:APA91bHZDZm47TjbOuUBP6WmfxDh66LlrI1HPmYX3G0nFQKG1FyqX7pGrEUl7QTGe9KPQ6GtnidsFbK39AMZFDkL-K1DHz58oPB1sGtTBxyQ9pc18lkoa3q3fLoHt8907EQ9c42Wrs1l',
	'Content-Type: application/json'
  );
 
$ch = curl_init();
curl_setopt( $ch,CURLOPT_URL, 'https://android.googleapis.com/gcm/send' );
curl_setopt( $ch,CURLOPT_POST, true );
curl_setopt( $ch,CURLOPT_HTTPHEADER, $headers );
curl_setopt( $ch,CURLOPT_RETURNTRANSFER, true );
curl_setopt( $ch,CURLOPT_SSL_VERIFYPEER, false );
curl_setopt( $ch,CURLOPT_POSTFIELDS, json_encode( $fields ) );
$result = curl_exec($ch );
curl_close( $ch );
return $result;
    }
   
  public function GetDevice_id($rider_id) {
    $query = $this->db->query("SELECT device_ID from ri_riderinfo where rider_id=".$rider_id);
         return $query->row;
    }
    
     public function GetShopperDevice_idByOrderID($order_id) {
    $query = $this->db->query("SELECT r.device_ID from ri_riderinfo r INNER JOIN ri_shoppertoriderassignment s on r.rider_id=s.shopper_id
       WHERE s.order_id=".$order_id);
         return $query->row;
    }
    
        public function getOrdersHistory($data) {
    	   $this->load->model('tool/image');
           
           
           $rider_id = (int)$data['rider_id'];
           $status_id = (int)$data['status_id'];

           $sql = "SELECT ro.Order_id as rider_order_id,o.order_id, o.date_added,o.total,o.payment_address_1,o.shipping_address_1,o.telephone
                                        ,IFNULL(ot.value,0) as delivery_charges,od.signature_image,od.parcel_image,rs.status,
                                        ro.delivery_longitude,ro.delivery_latitude,CONCAT (o.firstname,' ',o.lastname) AS customer_name   
                                        FROM ri_riderorder ro
                                        LEFT OUTER JOIN " . DB_PREFIX . "order o ON ro.order_fromDB_id = o.order_id
                                        LEFT OUTER JOIN ri_riderorderstatus rs ON ro.order_status = rs.status_id                                        
                                        LEFT OUTER JOIN " . DB_PREFIX . "order_delivered od ON o.order_id = od.order_id
                                        LEFT OUTER JOIN " . DB_PREFIX . "order_total ot ON o.order_id = ot.order_id
                                        AND ot.code = 'low_order_fee' 
                                        WHERE ro.rider_id =".$rider_id;
       

          
              if($status_id > 0){
               $sql .= " AND rs.status_id = ". $status_id; 
              }
             
              $sql .= " ORDER BY o.order_id DESC";
      
 
		//$orders_query = $this->db->query("SELECT o.order_id, o.date_added,o.order_id,o.total,o.payment_address_1,o.shipping_address_1,o.telephone
//                                        ,os.name as order_status ,IFNULL(ot.value,0) as delivery_charges ,od.signature_type,od.signature_image,od.parcel_type,od.parcel_image  FROM " . DB_PREFIX . "order o
//                                        INNER JOIN " . DB_PREFIX . "order_status os ON o.order_status_id = os.order_status_id
//                                        LEFT OUTER JOIN " . DB_PREFIX . "order_delivered od ON o.order_id = od.order_id
//                                        LEFT OUTER JOIN " . DB_PREFIX . "order_total ot ON o.order_id = ot.order_id
//                                        AND ot.code = 'low_order_fee' 
//                                        WHERE os.language_id=1 ORDER BY o.order_id DESC");


     $orders_query = $this->db->query($sql);
    
                $orders = array();
		if (count($orders_query->rows) > 0) {
		
              foreach($orders_query->rows as $order) 
              {
                  $signImage    =  $order['signature_image'];
                  $parcelImage  =  $order['parcel_image']; 
              
                 if(isset($signImage) && !empty($signImage))
                 {
                    $order['signature_image'] = $this->model_tool_image->resize($signImage, 470, 350);
                 }
                 if(isset($parcelImage) && !empty($parcelImage))
                 {
                    $order['parcel_image'] = $this->model_tool_image->resize($parcelImage, 470, 350);
                 }
                $order_product_query = $this->db->query("SELECT op.`name`,op.`quantity`,op.`price`,op.`total`,op.`tax`,p.image,p.product_id,p.upc                                                          
                                                        FROM " . DB_PREFIX . "order_product op 
                                                        LEFT JOIN " . DB_PREFIX . "product p ON  op.product_id = p.product_id
                                                        WHERE order_id = '" . (int)$order['order_id'] . "'");
               
                $allProducts = $order_product_query->rows;
               
                
                foreach($allProducts as $key => $value)
          		{
          		  $allProducts[$key]['image'] = $this->model_tool_image->resize($allProducts[$key]['image'], 470, 350);
          		}
	
                $order['items'] = $allProducts;
                 
                $orders[] = $order;
                
              }
             
          return $orders ;

			
		} else {
		 
			return  "No Order found!";
		}
	}
      public function getOrders($data) {
    	   $this->load->model('tool/image');
           
           
           $rider_id = (int)$data['rider_id'];
           $status_id = (int)$data['status_id'];

           $sql = "SELECT ro.Order_id as rider_order_id,o.order_id, o.date_added,o.total,o.payment_address_1,o.shipping_address_1,o.telephone
                                        ,IFNULL(ot.value,0) as delivery_charges,od.signature_image,od.parcel_image,rs.status,
                                        ro.delivery_longitude,ro.delivery_latitude,CONCAT (o.firstname,' ',o.lastname) AS customer_name   
                                        FROM ri_riderorder ro
                                        LEFT OUTER JOIN " . DB_PREFIX . "order o ON ro.order_fromDB_id = o.order_id
                                        LEFT OUTER JOIN ri_riderorderstatus rs ON ro.order_status = rs.status_id                                        
                                        LEFT OUTER JOIN " . DB_PREFIX . "order_delivered od ON o.order_id = od.order_id
                                        LEFT OUTER JOIN " . DB_PREFIX . "order_total ot ON o.order_id = ot.order_id
                                        AND ot.code = 'low_order_fee' 
                                        WHERE ro.rider_id =".$rider_id." AND ro.access_datetime >= DATE(NOW()) - INTERVAL 2 DAY";
       

          
              if($status_id > 0){
               $sql .= " AND rs.status_id = ". $status_id; 
              }
             
              $sql .= " ORDER BY o.order_id DESC";
      
 
		//$orders_query = $this->db->query("SELECT o.order_id, o.date_added,o.order_id,o.total,o.payment_address_1,o.shipping_address_1,o.telephone
//                                        ,os.name as order_status ,IFNULL(ot.value,0) as delivery_charges ,od.signature_type,od.signature_image,od.parcel_type,od.parcel_image  FROM " . DB_PREFIX . "order o
//                                        INNER JOIN " . DB_PREFIX . "order_status os ON o.order_status_id = os.order_status_id
//                                        LEFT OUTER JOIN " . DB_PREFIX . "order_delivered od ON o.order_id = od.order_id
//                                        LEFT OUTER JOIN " . DB_PREFIX . "order_total ot ON o.order_id = ot.order_id
//                                        AND ot.code = 'low_order_fee' 
//                                        WHERE os.language_id=1 ORDER BY o.order_id DESC");


     $orders_query = $this->db->query($sql);
    
                $orders = array();
		if (count($orders_query->rows) > 0) {
		
              foreach($orders_query->rows as $order) 
              {
                  $signImage    =  $order['signature_image'];
                  $parcelImage  =  $order['parcel_image']; 
              
                 if(isset($signImage) && !empty($signImage))
                 {
                    $order['signature_image'] = $this->model_tool_image->resize($signImage, 470, 350);
                 }
                 if(isset($parcelImage) && !empty($parcelImage))
                 {
                    $order['parcel_image'] = $this->model_tool_image->resize($parcelImage, 470, 350);
                 }
                $order_product_query = $this->db->query("SELECT op.`name`,op.`quantity`,op.`price`,op.`total`,op.`tax`,p.image,p.product_id,p.upc                                                          
                                                        FROM " . DB_PREFIX . "order_product op 
                                                        LEFT JOIN " . DB_PREFIX . "product p ON  op.product_id = p.product_id
                                                        WHERE order_id = '" . (int)$order['order_id'] . "'");
               
                $allProducts = $order_product_query->rows;
               
                
                foreach($allProducts as $key => $value)
          		{
          		  $allProducts[$key]['image'] = $this->model_tool_image->resize($allProducts[$key]['image'], 470, 350);
          		}
	
                $order['items'] = $allProducts;
                 
                $orders[] = $order;
                
              }
             
          return $orders ;

			
		} else {
		 
			return  "No Order found!";
		}
	}
    
    public function addSignature()
    {
        $data = $this->request->post;
        
        // Check if already data exist against order and customer       
        $orderInfo =  $this->db->query("SELECT * FROM  " . DB_PREFIX . "order_delivered WHERE  order_id ='".$data["order_id"]."' AND customer_id='".$data["customer_id"]."'");
        
        if($orderInfo->num_rows > 0)
        {
            $imageStatus = $this->uploadImage($data["signature_image"],$data["signature_type"]);
            
            if($imageStatus != "failed")
            {
               // Update and set values
            $this->db->query(" UPDATE " . DB_PREFIX . "order_delivered SET  signature_image = '".$imageStatus."' WHERE  order_id ='".$data["order_id"]."' AND customer_id='".$data["customer_id"]."'");
            
            $res = "success"; 
            }
            else{
                 $res = "failed"; 
            }  
            
        }
        else{
            $imageStatus = $this->uploadImage($data["signature_image"],$data["signature_type"]);
            
            if($imageStatus != "failed")
            {
            // Insert Values
            $sql = "INSERT INTO  " . DB_PREFIX . "order_delivered(order_id,customer_id,signature_image,date_added) VALUES (".$data["order_id"].",".$data["customer_id"].",'".$imageStatus."' ,NOW())";
            $this->db->query($sql);
            
            $delivered_id =$this->db->getLastId();
            $res = ($delivered_id > 0) ? "success" : "failed";
            }
            else{
                $res = "failed"; 
            }
        }
        
        return $res;
    }
        public function addParcel()
    {
        $data = $this->request->post;
        
        // Check if already data exist against order and customer       
        $orderInfo =  $this->db->query("SELECT * FROM  " . DB_PREFIX . "order_delivered WHERE  order_id ='".$data["order_id"]."' AND customer_id='".$data["customer_id"]."'");
        
        if($orderInfo->num_rows > 0)
        {
            $imageStatus = $this->uploadImage($data["parcel_image"],$data["parcel_type"]);
            
            if($imageStatus != "failed")
            {
               // Update and set values
            $this->db->query(" UPDATE " . DB_PREFIX . "order_delivered SET  parcel_image = '".$imageStatus."' WHERE  order_id ='".$data["order_id"]."' AND customer_id='".$data["customer_id"]."'");
            
            $res = "success"; 
            }
            else{
                 $res = "failed"; 
            }  
            
        }
        else{
            $imageStatus = $this->uploadImage($data["parcel_image"],$data["parcel_type"]);
            
            if($imageStatus != "failed")
            {
            // Insert Values
            $sql = "INSERT INTO  " . DB_PREFIX . "order_delivered(order_id,customer_id,parcel_image,date_added) VALUES (".$data["order_id"].",".$data["customer_id"].",'".$imageStatus."' ,NOW())";
            $this->db->query($sql);
            
            $delivered_id =$this->db->getLastId();
            $res = ($delivered_id > 0) ? "success" : "failed";
            }
            else{
                $res = "failed"; 
            }
        }
        
        return $res;
    }
    
    
    
     function uploadImage($image64,$imageType)
        {
            
    $directory ='rider/order_delivered/';
     DIR_IMAGE .  
	//define('UPLOAD_DIR', 'images/');
	$img = $image64;
	$img = str_replace('data:image/'.$imageType.';base64,', '', $img);
	$img = str_replace(' ', '+', $img);
	$data = base64_decode($img);
    $fileName = $directory . uniqid() . '.'.$imageType;
	$file = DIR_IMAGE .$fileName;
	$success = file_put_contents($file, $data);
    
	return $success ? $fileName : 'failed';
	  
        }
    
}
