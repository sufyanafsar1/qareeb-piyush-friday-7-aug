<?php
class ModelSaleOrder extends Model {
	public function getorderdataexport($data = array()) {
		$sql = "SELECT o.order_id, CONCAT(o.firstname, ' ', o.lastname) AS customer, (SELECT os.name FROM " . DB_PREFIX . "order_status os WHERE os.order_status_id = o.order_status_id AND os.language_id = '" . (int)$this->config->get('config_language_id') . "') AS status, o.shipping_code, o.total, o.currency_code, o.currency_value, o.date_added, o.date_modified, o.sequence FROM `" . DB_PREFIX . "order` o  LEFT JOIN " . DB_PREFIX . "customer c ON(c.customer_id = o.customer_id)";

		if (isset($data['filter_order_status'])) {
			$implode = array();

			$order_statuses = explode(',', $data['filter_order_status']);

			foreach ($order_statuses as $order_status_id) {
				$implode[] = "o.order_status_id = '" . (int)$order_status_id . "'";
			}

			if ($implode) {
				$sql .= " WHERE (" . implode(" OR ", $implode) . ")";
			}
		} else {
			$sql .= " WHERE o.order_status_id > '0'";
		}
		

		if (!empty($data['filter_order_id'])) {
			$sql .= " AND o.order_id = '" . (int)$data['filter_order_id'] . "'";
		}

		if (!empty($data['filter_customer'])) {
			$sql .= " AND CONCAT(o.firstname, ' ', o.lastname) LIKE '%" . $this->db->escape($data['filter_customer']) . "%'";
		}

		if (!empty($data['filter_date_added'])) {
			$sql .= " AND DATE(o.date_added) = DATE('" . $this->db->escape($data['filter_date_added']) . "')";
		}

		if (!empty($data['filter_date_modified'])) {
			$sql .= " AND DATE(o.date_modified) = DATE('" . $this->db->escape($data['filter_date_modified']) . "')";
		}
		
		if (!empty($data['filter_seller_name'])) {
			$sql .= " AND seller_id = '" . $this->db->escape($data['filter_seller_name']) . "'";
		}

		if (!empty($data['filter_total'])) {
			$sql .= " AND o.total = '" . (float)$data['filter_total'] . "'";
		}
		
		if (!empty($data['filter_rider'])) {
			$sql .= " AND order_id IN (SELECT order_fromDB_id  FROM `ri_riderorder` WHERE `rider_id` IN (SELECT rider_id  FROM `ri_riderinfo` WHERE `name` LIKE '" . $this->db->escape($data['filter_rider']) . "%'))";
		}
		
		if (!empty($data['filter_city'])) {
			$sql .= " AND o.shipping_city LIKE '".$data['filter_city']."%'";
		}
		
		if (isset($data['filter_customer_type']) && $data['filter_customer_type'] != 0) {
			$sql .= "AND c.store = '" . (int)$data['filter_customer_type'] . "'";
			
		}
		
		if (!empty($data['filter_group'])) {
			if($data['filter_group'] == 'month'){
				$sql .= " AND o.date_added like '". $data['filter_month'] ."%'";
			}elseif($data['filter_group'] == 'year'){	
				$sql .= " AND o.date_added like '". $data['filter_year'] ."%'";
				
			}elseif($data['filter_group'] == 'week'){
				
				$sql .= " AND o.date_added >= '".date('Y-m-d', strtotime('-6 days'))."' AND o.date_added <= '".date('Y-m-d', strtotime('1 days'))."'";
			}elseif($data['filter_group'] == 'day'){
				if($data['filter_date_start'] != ''){
					$sql .= " AND o.date_added >= '".$data['filter_date_start']."'";
				}
				if($data['filter_date_end'] != ''){
					
					$sql .= " AND o.date_added <= '".date('Y-m-d', strtotime($data['filter_date_end']. ' + 1 days'))."'";
				}
			}
		}
		
		$sort_data = array(
			'o.order_id',
			'customer',
			'status',
			'o.date_added',
			'o.date_modified',
			'o.total'
		);

		if (isset($data['sort']) && in_array($data['sort'], $sort_data)) {
			$sql .= " ORDER BY " . $data['sort'];
		} else {
			$sql .= " ORDER BY o.order_id";
		}

		if (isset($data['order']) && ($data['order'] == 'DESC')) {
			$sql .= " DESC";
		} else {
			$sql .= " ASC";
		}

		if (isset($data['start']) || isset($data['limit'])) {
			if ($data['start'] < 0) {
				$data['start'] = 0;
			}

			if ($data['limit'] < 1) {
				$data['limit'] = 3000;
			}

			$sql .= " LIMIT " . (int)$data['start'] . "," . (int)$data['limit'];
		}
		
		$query = $this->db->query($sql);

		return $query->rows;
	}
	public function getOrder($order_id,$customer_id) {
		
		$order_query = $this->db->query("SELECT *, (SELECT CONCAT(c.firstname, ' ', c.lastname) FROM " . DB_PREFIX . "customer c WHERE c.customer_id = o.customer_id) AS customer FROM `" . DB_PREFIX . "order` o WHERE o.order_id = '" . (int)$order_id . "' and customer_id = '".(int)$customer_id."'");
	
		
		// $customer_id
		
		if ($order_query->num_rows) {
			$reward = 0;
			
			$order_product_query = $this->db->query("SELECT * FROM " . DB_PREFIX . "order_product WHERE order_id = '" . (int)$order_id . "'");
			
			$order_customer_store = $this->db->query("SELECT *  FROM `" . DB_PREFIX . "customer` WHERE `store` = 1 AND customer_id = ".$order_query->row['customer_id']."");
			
			if($order_customer_store->num_rows){
				$store = $order_customer_store->row['store'];
				$storename = ' - '.$order_customer_store->row['storename'];
			}else{
				$store = '';
				$storename = '';
			}
			
			/* echo"<pre>";
			echo$storename;
			exit; */
			foreach ($order_product_query->rows as $product) {
				$reward += $product['reward'];
			}

			$country_query = $this->db->query("SELECT * FROM `" . DB_PREFIX . "country` WHERE country_id = '" . (int)$order_query->row['payment_country_id'] . "'");

			if ($country_query->num_rows) {
				$payment_iso_code_2 = $country_query->row['iso_code_2'];
				$payment_iso_code_3 = $country_query->row['iso_code_3'];
			} else {
				$payment_iso_code_2 = '';
				$payment_iso_code_3 = '';
			}

			$zone_query = $this->db->query("SELECT * FROM `" . DB_PREFIX . "zone` WHERE zone_id = '" . (int)$order_query->row['payment_zone_id'] . "'");

			if ($zone_query->num_rows) {
				$payment_zone_code = $zone_query->row['code'];
			} else {
				$payment_zone_code = '';
			}

			$country_query = $this->db->query("SELECT * FROM `" . DB_PREFIX . "country` WHERE country_id = '" . (int)$order_query->row['shipping_country_id'] . "'");

			if ($country_query->num_rows) {
				$shipping_iso_code_2 = $country_query->row['iso_code_2'];
				$shipping_iso_code_3 = $country_query->row['iso_code_3'];
			} else {
				$shipping_iso_code_2 = '';
				$shipping_iso_code_3 = '';
			}

			$zone_query = $this->db->query("SELECT * FROM `" . DB_PREFIX . "zone` WHERE zone_id = '" . (int)$order_query->row['shipping_zone_id'] . "'");

			if ($zone_query->num_rows) {
				$shipping_zone_code = $zone_query->row['code'];
			} else {
				$shipping_zone_code = '';
			}

			if ($order_query->row['affiliate_id']) {
				$affiliate_id = $order_query->row['affiliate_id'];
			} else {
				$affiliate_id = 0;
			}

			/* $this->load->model('sale/affiliate');

			$affiliate_info = $this->model_sale_affiliate->getAffiliate($affiliate_id);

			if ($affiliate_info) {
				$affiliate_firstname = $affiliate_info['firstname'];
				$affiliate_lastname = $affiliate_info['lastname'];
			} else {
				$affiliate_firstname = '';
				$affiliate_lastname = '';
			} */

			$this->load->model('localisation/language');

			$language_info = $this->model_localisation_language->getLanguage($order_query->row['language_id']);

			if ($language_info) {
				$language_code = $language_info['code'];
				$language_directory = $language_info['directory'];
			} else {
				$language_code = '';
				$language_directory = '';
			}


			return array(
				'order_id'                => $order_query->row['order_id'],
				'invoice_no'              => $order_query->row['invoice_no'],
				'invoice_prefix'          => $order_query->row['invoice_prefix'],
				'store_id'                => $order_query->row['store_id'],
				'store_name'              => $order_query->row['store_name'],
				'store_url'               => $order_query->row['store_url'],
				'customer_id'             => $order_query->row['customer_id'],
				'customer'                => $order_query->row['customer'],
				'customer_group_id'       => $order_query->row['customer_group_id'],
				'firstname'               => $order_query->row['firstname'],
				'lastname'                => $order_query->row['lastname'],
				'email'                   => $order_query->row['email'],
				'telephone'               => $order_query->row['telephone'],
				'fax'                     => $order_query->row['fax'],
				'custom_field'            => json_decode($order_query->row['custom_field'], true),
				'payment_firstname'       => $order_query->row['payment_firstname'],
				'payment_lastname'        => $order_query->row['payment_lastname'],
				'payment_company'         => $order_query->row['payment_company'],
				'payment_address_1'       => $order_query->row['payment_address_1'],
				'payment_address_2'       => $order_query->row['payment_address_2'],
				'payment_postcode'        => $order_query->row['payment_postcode'],
				'payment_city'            => $order_query->row['payment_city'],
				'payment_zone_id'         => $order_query->row['payment_zone_id'],
				'payment_zone'            => $order_query->row['payment_zone'],
				'payment_zone_code'       => $payment_zone_code,
				'payment_country_id'      => $order_query->row['payment_country_id'],
				'payment_country'         => $order_query->row['payment_country'],
				'payment_iso_code_2'      => $payment_iso_code_2,
				'payment_iso_code_3'      => $payment_iso_code_3,
				'payment_address_format'  => $order_query->row['payment_address_format'],
				'payment_custom_field'    => json_decode($order_query->row['payment_custom_field'], true),
				'payment_method'          => $order_query->row['payment_method'],
				'payment_code'            => $order_query->row['payment_code'],
				'shipping_firstname'      => $order_query->row['shipping_firstname'],
				'shipping_lastname'       => $order_query->row['shipping_lastname'],
				'shipping_company'        => $order_query->row['shipping_company'],
				'shipping_address_1'      => $order_query->row['shipping_address_1'],
				'shipping_address_2'      => $order_query->row['shipping_address_2'],
				'shipping_postcode'       => $order_query->row['shipping_postcode'],
				'shipping_city'           => $order_query->row['shipping_city'],
				'shipping_zone_id'        => $order_query->row['shipping_zone_id'],
				'shipping_zone'           => $order_query->row['shipping_zone'],
				'shipping_zone_code'      => $shipping_zone_code,
				'shipping_country_id'     => $order_query->row['shipping_country_id'],
				'shipping_country'        => $order_query->row['shipping_country'],
				'shipping_iso_code_2'     => $shipping_iso_code_2,
				'shipping_iso_code_3'     => $shipping_iso_code_3,
				'shipping_address_format' => $order_query->row['shipping_address_format'],
				'shipping_custom_field'   => json_decode($order_query->row['shipping_custom_field'], true),
				'shipping_method'         => $order_query->row['shipping_method'],
				'shipping_code'           => $order_query->row['shipping_code'],
				'comment'                 => $order_query->row['comment'],
				'total'                   => $order_query->row['total'],
				'reward'                  => $reward,
				'order_status_id'         => $order_query->row['order_status_id'],
				'affiliate_id'            => $order_query->row['affiliate_id'],
				'affiliate_firstname'     => $affiliate_firstname,
				'affiliate_lastname'      => $affiliate_lastname,
				'commission'              => $order_query->row['commission'],
				'language_id'             => $order_query->row['language_id'],
				'language_code'           => $language_code,
				'language_directory'      => $language_directory,
				'currency_id'             => $order_query->row['currency_id'],
				'currency_code'           => $order_query->row['currency_code'],
				'currency_value'          => $order_query->row['currency_value'],
				'ip'                      => $order_query->row['ip'],
				'forwarded_ip'            => $order_query->row['forwarded_ip'],
				'user_agent'              => $order_query->row['user_agent'],
				'accept_language'         => $order_query->row['accept_language'],
				'date_added'              => $order_query->row['date_added'],
				'date_modified'           => $order_query->row['date_modified'],
				'store'           		  => $store,
				'storename'           	  => $storename
			);
		
		} else {
			return;
		}
	}

	public function getOrders($data = array()) {
		$sql = "SELECT o.order_id, CONCAT(o.firstname, ' ', o.lastname) AS customer, (SELECT os.name FROM " . DB_PREFIX . "order_status os WHERE os.order_status_id = o.order_status_id AND os.language_id = '" . (int)$this->config->get('config_language_id') . "') AS status, o.shipping_code, o.total, o.currency_code, o.currency_value, o.date_added, o.date_modified, o.sequence FROM `" . DB_PREFIX . "order` o LEFT JOIN " . DB_PREFIX . "customer c ON(c.customer_id = o.customer_id)";

		if (isset($data['filter_order_status'])) {
			$implode = array();

			$order_statuses = explode(',', $data['filter_order_status']);

			foreach ($order_statuses as $order_status_id) {
				$implode[] = "o.order_status_id = '" . (int)$order_status_id . "'";
			}

			if ($implode) {
				$sql .= " WHERE (" . implode(" OR ", $implode) . ")";
			}
		} else {
			$sql .= " WHERE o.order_status_id > '0'";
		}

		if (!empty($data['filter_order_id'])) {
			$sql .= " AND o.order_id = '" . (int)$data['filter_order_id'] . "'";
		}

		if (!empty($data['filter_customer'])) {
			$sql .= " AND CONCAT(o.firstname, ' ', o.lastname) LIKE '%" . $this->db->escape($data['filter_customer']) . "%'";
		}

		if (!empty($data['filter_date_added'])) {
			$sql .= " AND DATE(o.date_added) = DATE('" . $this->db->escape($data['filter_date_added']) . "')";
		}

		if (!empty($data['filter_date_modified'])) {
			$sql .= " AND DATE(o.date_modified) = DATE('" . $this->db->escape($data['filter_date_modified']) . "')";
		}
		
		if (!empty($data['filter_seller_name'])) {
			$sql .= " AND seller_id = '" . $this->db->escape($data['filter_seller_name']) . "'";
		}
		
		if (isset($data['filter_customer_type'])) {
			$sql .= "AND c.store = '" . (int)$data['filter_customer_type'] . "'";
			
		}

		if (!empty($data['filter_total'])) {
			$sql .= " AND o.total = '" . (float)$data['filter_total'] . "'";
		}
		
		if (!empty($data['filter_rider'])) {
			$sql .= " AND order_id IN (SELECT order_fromDB_id  FROM `ri_riderorder` WHERE `rider_id` IN (SELECT rider_id  FROM `ri_riderinfo` WHERE `name` LIKE '" . $this->db->escape($data['filter_rider']) . "%'))";
		}
		
		if (!empty($data['filter_city'])) {
			$sql .= " AND o.shipping_city LIKE '".$data['filter_city']."%'";
		}
		if (!empty($data['filter_group'])) {
			if($data['filter_group'] == 'month'){
				$sql .= " AND o.date_added like '". $data['filter_month'] ."%'";
			}elseif($data['filter_group'] == 'year'){	
				$sql .= " AND o.date_added like '". $data['filter_year'] ."%'";
				
			}elseif($data['filter_group'] == 'week'){
				
				$sql .= " AND o.date_added >= '".date('Y-m-d', strtotime('-6 days'))."' AND o.date_added <= '".date('Y-m-d', strtotime('1 days'))."'";
			}elseif($data['filter_group'] == 'day'){
				if($data['filter_date_start'] != ''){
					$sql .= " AND o.date_added >= '".$data['filter_date_start']."'";
				}
				if($data['filter_date_end'] != ''){
					
					$sql .= " AND o.date_added <= '".date('Y-m-d', strtotime($data['filter_date_end']. ' + 1 days'))."'";
				}
			}
		}

		$sort_data = array(
			'o.order_id',
			'customer',
			'status',
			'o.date_added',
			'o.date_modified',
			'o.total'
		);

		if (isset($data['sort']) && in_array($data['sort'], $sort_data)) {
			$sql .= " ORDER BY " . $data['sort'];
		} else {
			$sql .= " ORDER BY o.order_id";
		}

		if (isset($data['order']) && ($data['order'] == 'DESC')) {
			$sql .= " DESC";
		} else {
			$sql .= " ASC";
		}

		if (isset($data['start']) || isset($data['limit'])) {
			if ($data['start'] < 0) {
				$data['start'] = 0;
			}

			if ($data['limit'] < 1) {
				$data['limit'] = 20;
			}

			$sql .= " LIMIT " . (int)$data['start'] . "," . (int)$data['limit'];
		}

		$query = $this->db->query($sql);

		return $query->rows;
	}
	public function getcitys($data = array()) {
		
		$sql = "SELECT distinct shipping_city  FROM " . DB_PREFIX . "order";
			$sql .= " WHERE order_status_id > '0'";
		
		if (isset($data['filter_city']) && $data['filter_city'] != '') {
			$sql .= " AND shipping_city LIKE '".$data['filter_city']."%'";
		}
	
		$sql .= " LIMIT 0,10";
		$query = $this->db->query($sql);
		return $query->rows;
	}
	public function getOrderProducts($order_id) {
	  
		$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "order_product WHERE order_id = '" . (int)$order_id . "'");
		return $query->rows;
	}

	public function getOrderOptions($order_id, $order_product_id) {
		$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "order_option WHERE order_id = '" . (int)$order_id . "' AND order_product_id = '" . (int)$order_product_id . "'");

		return $query->rows;
	}

	public function getOrderVouchers($order_id) {
		$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "order_voucher WHERE order_id = '" . (int)$order_id . "'");

		return $query->rows;
	}
	public function getOrderSellers($order_id,$seller_id='') {	
		$sql = "SELECT * FROM " . DB_PREFIX . "order_product op LEFT JOIN " . DB_PREFIX . "sellers s ON (op.seller_id=s.seller_id) WHERE op.order_id = '" . (int)$order_id . "'";
		if(!empty($seller_id)){
			$sql .= " AND s.username = '".$this->db->escape($seller_id)."'";
		}
		$query = $this->db->query($sql);
		$sellers = array();
		foreach($query->rows as $row){
			$sellers[$row['firstname']] = $row['firstname'];
		}
		if(count($sellers)>0){
			$sellers = implode(" , ",$sellers);
		}else{$sellers ="";}
		return $sellers;
	}
	
	public function getOrderSellerStatus($order_id,$status_id='') {	
		$sql = "SELECT *,os.name as orderstatus FROM " . DB_PREFIX . "order_product op LEFT JOIN " . DB_PREFIX . "sellers s ON (op.seller_id=s.seller_id) LEFT JOIN " . DB_PREFIX . "order_status os ON (os.order_status_id=op.product_status_id) WHERE op.order_id = '" . (int)$order_id . "' AND language_id='" . (int)$this->config->get('config_language_id') . "'";
		if(!empty($status_id)){
			$sql .= " AND op.product_status_id = '".(int)$status_id."'";
		}
		$query = $this->db->query($sql);
		$sellers = array();
		foreach($query->rows as $row){
			$sellers[$row['firstname']] = $row['firstname']." (".$row['orderstatus'].")";
		}
		if(count($sellers)>0){
			$sellers = implode(" , ",$sellers);
		}else{$sellers ="";}
		return $sellers;
	}
	public function getOrderVoucherByVoucherId($voucher_id) {
		$query = $this->db->query("SELECT * FROM `" . DB_PREFIX . "order_voucher` WHERE voucher_id = '" . (int)$voucher_id . "'");

		return $query->row;
	}

	public function getOrderTotals($order_id) {
		$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "order_total WHERE order_id = '" . (int)$order_id . "' ORDER BY sort_order");

		return $query->rows;
	}
	public function getsellername($order_id) {
		
		$query = $this->db->query("SELECT firstname FROM `oc_sellers` WHERE seller_id = (SELECT seller_id FROM `oc_order` WHERE order_id= '" . (int)$order_id . "')");
		
		return $query->rows;
	}

	public function getOrderTotals1($order_id) {
		$query = $this->db->query("SELECT value FROM " . DB_PREFIX . "order_total WHERE order_id = '" . (int)$order_id . "' AND `code` LIKE 'total'");

		return $query->row['value'];
	}

	public function getOrderTotals2($order_id) {
		$query = $this->db->query("SELECT shipping_charges FROM " . DB_PREFIX . "order_seller_shipping_fee WHERE order_id = '" . (int)$order_id . "'");

		return $query->row['shipping_charges'];
	}

	public function getTotalOrders($data = array()) {
		$sql = "SELECT COUNT(*) AS total FROM `" . DB_PREFIX . "order` o LEFT JOIN " . DB_PREFIX . "customer c ON(c.customer_id = o.customer_id)";
		
		if (isset($data['filter_order_status'])) {
			$implode = array();

			$order_statuses = explode(',', $data['filter_order_status']);

			foreach ($order_statuses as $order_status_id) {
				$implode[] = "o.order_status_id = '" . (int)$order_status_id . "'";
			}

			if ($implode) {
				$sql .= " WHERE (" . implode(" OR ", $implode) . ")";
			}
		} else {
			$sql .= " WHERE o.order_status_id > '0'";
		}

		if (!empty($data['filter_order_id'])) {
			$sql .= " AND o.order_id = '" . (int)$data['filter_order_id'] . "'";
		}

		if (!empty($data['filter_customer'])) {
			$sql .= " AND CONCAT(o.firstname, ' ', o.lastname) LIKE '%" . $this->db->escape($data['filter_customer']) . "%'";
		}
		
		if (!empty($data['filter_date_added'])) {
			$sql .= " AND o.DATE(date_added) = DATE('" . $this->db->escape($data['filter_date_added']) . "')";
		}

		if (!empty($data['filter_date_modified'])) {
			$sql .= " AND o.DATE(date_modified) = DATE('" . $this->db->escape($data['filter_date_modified']) . "')";
		}
		
		if (!empty($data['filter_seller_name'])) {
			$sql .= " AND o.seller_id = '" . $this->db->escape($data['filter_seller_name']) . "'";
		}

		if (!empty($data['filter_total'])) {
			$sql .= " AND o.total = '" . (float)$data['filter_total'] . "'";
		}
		
		if (!empty($data['filter_city'])) {
			$sql .= " AND o.shipping_city LIKE '".$data['filter_city']."%'";
		}
		
		if (isset($data['filter_customer_type'])) {
			$sql .= "AND c.store = '" . (int)$data['filter_customer_type'] . "'";
			
		}
		
		if (!empty($data['filter_rider'])) {
			$sql .= " AND o.order_id IN (SELECT order_fromDB_id  FROM `ri_riderorder` WHERE `rider_id` IN (SELECT rider_id  FROM `ri_riderinfo` WHERE `name` LIKE '" . $this->db->escape($data['filter_rider']) . "%'))";
		}
		if (!empty($data['filter_group'])) {
			if($data['filter_group'] == 'month'){
				$sql .= " AND o.date_added like '". $data['filter_month'] ."%'";
			}elseif($data['filter_group'] == 'year'){	
				$sql .= " AND o.date_added like '". $data['filter_year'] ."%'";
			}elseif($data['filter_group'] == 'week'){
				$sql .= " AND o.date_added >= '".date('Y-m-d', strtotime('-6 days'))."' AND o.date_added <= '".date('Y-m-d', strtotime('1 days'))."'";
			}elseif($data['filter_group'] == 'day'){
				if($data['filter_date_start'] != ''){
					$sql .= " AND o.date_added >= '".$data['filter_date_start']."'";
				}
				if($data['filter_date_end'] != ''){
					$sql .= " AND o.date_added <= '".date('Y-m-d', strtotime($data['filter_date_end']. ' + 1 days'))."'";
				}
			}
		}
			/* echo $sql;
			exit; */
		$query = $this->db->query($sql);
	
		return $query->row['total'];
	}

	public function getTotalOrdersByStoreId($store_id) {
		$query = $this->db->query("SELECT COUNT(*) AS total FROM `" . DB_PREFIX . "order` WHERE store_id = '" . (int)$store_id . "'");

		return $query->row['total'];
	}

	public function getTotalOrdersByOrderStatusId($order_status_id) {
		$query = $this->db->query("SELECT COUNT(*) AS total FROM `" . DB_PREFIX . "order` WHERE order_status_id = '" . (int)$order_status_id . "' AND order_status_id > '0'");

		return $query->row['total'];
	}
	
	public function getOrderSellersnew($order_id,$seller_id='') {	
		$sql = "SELECT * FROM " . DB_PREFIX . "order_product op LEFT JOIN " . DB_PREFIX . "sellers s ON (op.seller_id=s.seller_id) WHERE op.order_id = '" . (int)$order_id . "'";
		if(!empty($seller_id)){
			$sql .= " AND s.username = '".$this->db->escape($seller_id)."'";
		}
		$queryseller = $this->db->query("SELECT * FROM `" . DB_PREFIX . "sellers` WHERE seller_id = (SELECT seller_id FROM `oc_order` WHERE order_id= '" . (int)$order_id . "')");
		
		$seller_id = $queryseller->row['seller_id'];
		
		if(!empty($seller_id)){
			$sql .= " AND s.seller_id= '".(int)$seller_id."'";
		}
		
		$query = $this->db->query($sql);
		$sellers = array();
		foreach($query->rows as $row){
			$sellers[$row['firstname']] = $row['firstname'];
		}
		if(count($sellers)>0){
			$sellers = implode(" , ",$sellers);
		}else{$sellers ="";}
		return $sellers;
	}

	public function getOrderSellerStatusnew($order_id,$status_id='',$seller_id='') {	
		$sql = "SELECT *,os.name as orderstatus FROM " . DB_PREFIX . "order_product op LEFT JOIN " . DB_PREFIX . "sellers s ON (op.seller_id=s.seller_id) LEFT JOIN " . DB_PREFIX . "order_status os ON (os.order_status_id=op.product_status_id) WHERE op.order_id = '" . (int)$order_id . "' AND language_id='" . (int)$this->config->get('config_language_id') . "'";
		if(!empty($status_id)){
			$sql .= " AND op.product_status_id = '".(int)$status_id."'";
		}
		
		$queryseller = $this->db->query("SELECT * FROM `" . DB_PREFIX . "sellers` WHERE seller_id = (SELECT seller_id FROM `oc_order` WHERE order_id= '" . (int)$order_id . "')");
		
		$seller_id = $queryseller->row['seller_id'];
		
		if(!empty($seller_id)){
			$sql .= " AND s.seller_id= '".(int)$seller_id."'";
		}
		
		$query = $this->db->query($sql);
		$sellers = array();
		foreach($query->rows as $row){
			$sellers[$row['firstname']] = $row['firstname']." (".$row['orderstatus'].")";
		}
		if(count($sellers)>0){
			$sellers = implode(" , ",$sellers);
		}else{$sellers ="";}
		return $sellers;
	}

	public function getTotalOrdersByProcessingStatus() {
		$implode = array();

		$order_statuses = $this->config->get('config_processing_status');

		foreach ($order_statuses as $order_status_id) {
			$implode[] = "order_status_id = '" . (int)$order_status_id . "'";
		}

		if ($implode) {
			$query = $this->db->query("SELECT COUNT(*) AS total FROM `" . DB_PREFIX . "order` WHERE " . implode(" OR ", $implode));

			return $query->row['total'];
		} else {
			return 0;
		}
	}

	public function getTotalOrdersByCompleteStatus() {
		$implode = array();

		$order_statuses = $this->config->get('config_complete_status');

		foreach ($order_statuses as $order_status_id) {
			$implode[] = "order_status_id = '" . (int)$order_status_id . "'";
		}

		if ($implode) {
			$query = $this->db->query("SELECT COUNT(*) AS total FROM `" . DB_PREFIX . "order` WHERE " . implode(" OR ", $implode) . "");

			return $query->row['total'];
		} else {
			return 0;
		}
	}

	public function getTotalOrdersByLanguageId($language_id) {
		$query = $this->db->query("SELECT COUNT(*) AS total FROM `" . DB_PREFIX . "order` WHERE language_id = '" . (int)$language_id . "' AND order_status_id > '0'");

		return $query->row['total'];
	}

	public function getTotalOrdersByCurrencyId($currency_id) {
		$query = $this->db->query("SELECT COUNT(*) AS total FROM `" . DB_PREFIX . "order` WHERE currency_id = '" . (int)$currency_id . "' AND order_status_id > '0'");

		return $query->row['total'];
	}

	public function createInvoiceNo($order_id) {
		$order_info = $this->getOrder($order_id);

		if ($order_info && !$order_info['invoice_no']) {
			$query = $this->db->query("SELECT MAX(invoice_no) AS invoice_no FROM `" . DB_PREFIX . "order` WHERE invoice_prefix = '" . $this->db->escape($order_info['invoice_prefix']) . "'");

			if ($query->row['invoice_no']) {
				$invoice_no = $query->row['invoice_no'] + 1;
			} else {
				$invoice_no = 1;
			}

			$this->db->query("UPDATE `" . DB_PREFIX . "order` SET invoice_no = '" . (int)$invoice_no . "', invoice_prefix = '" . $this->db->escape($order_info['invoice_prefix']) . "' WHERE order_id = '" . (int)$order_id . "'");

			return $order_info['invoice_prefix'] . $invoice_no;
		}
	}

	public function getOrderHistories($order_id, $start = 0, $limit = 10) {
		if ($start < 0) {
			$start = 0;
		}

		if ($limit < 1) {
			$limit = 10;
		}

		$query = $this->db->query("SELECT oh.date_added, os.name AS status, oh.comment, oh.notify FROM " . DB_PREFIX . "order_history oh LEFT JOIN " . DB_PREFIX . "order_status os ON oh.order_status_id = os.order_status_id WHERE oh.order_id = '" . (int)$order_id . "' AND os.language_id = '" . (int)$this->config->get('config_language_id') . "' ORDER BY oh.date_added DESC LIMIT " . (int)$start . "," . (int)$limit);

		return $query->rows;
	}

	public function getTotalOrderHistories($order_id) {
		$query = $this->db->query("SELECT COUNT(*) AS total FROM " . DB_PREFIX . "order_history WHERE order_id = '" . (int)$order_id . "'");

		return $query->row['total'];
	}

	public function getTotalOrderHistoriesByOrderStatusId($order_status_id) {
		$query = $this->db->query("SELECT COUNT(*) AS total FROM " . DB_PREFIX . "order_history WHERE order_status_id = '" . (int)$order_status_id . "'");

		return $query->row['total'];
	}

	public function getEmailsByProductsOrdered($products, $start, $end) {
		$implode = array();

		foreach ($products as $product_id) {
			$implode[] = "op.product_id = '" . (int)$product_id . "'";
		}

		$query = $this->db->query("SELECT DISTINCT email FROM `" . DB_PREFIX . "order` o LEFT JOIN " . DB_PREFIX . "order_product op ON (o.order_id = op.order_id) WHERE (" . implode(" OR ", $implode) . ") AND o.order_status_id <> '0' LIMIT " . (int)$start . "," . (int)$end);

		return $query->rows;
	}

	public function getTotalEmailsByProductsOrdered($products) {
		$implode = array();

		foreach ($products as $product_id) {
			$implode[] = "op.product_id = '" . (int)$product_id . "'";
		}

		$query = $this->db->query("SELECT DISTINCT email FROM `" . DB_PREFIX . "order` o LEFT JOIN " . DB_PREFIX . "order_product op ON (o.order_id = op.order_id) WHERE (" . implode(" OR ", $implode) . ") AND o.order_status_id <> '0'");

		return $query->row['total'];
	}
	public function getOrderDeliveryTime($order_id) {
		$order_query = $this->db->query("SELECT * FROM " . DB_PREFIX . "delivery_time WHERE order_id = '" . (int)$order_id . "'");
        if ($order_query->num_rows) {
			
			return array(
				'delivery_time' => $order_query->row['delivery_time'],
				
			);
		} else {
			return;
		}
		
	}
	public function getCity() {
		$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "city");
		return $query->rows;
	}
	public function getarea($city_id) {
		$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "area WHERE `city_id` = ".$city_id."");
		return $query->rows;
	}
	public function getseller($areaId) {
		
		$sql = "SELECT s.seller_id FROM " . DB_PREFIX . "sellers s JOIN " . DB_PREFIX . "saddress a ON s.seller_id = a.seller_id WHERE a.area_id = '" . (int) $areaId . "' AND status=1 AND parent_id = 0";

        if ((int) $defaultStore > 0) {
            $sql .= " OR a.seller_id=" . (int) $defaultStore;
        }
		
		$sellersIds = array();
		$query = $this->db->query($sql);
		foreach ($query->rows as $result) {
			$sellersIds[] = $result['seller_id'];		
		}
		
		//get Virtual Seller
		$subSellers = array();		
		$querySellers = $this->db->query("SELECT s.parent_id,s.seller_id FROM " . DB_PREFIX . "sellers s JOIN " . DB_PREFIX . "saddress a ON s.seller_id = a.seller_id WHERE a.area_id = '" . (int) $areaId . "' AND s.parent_id != '0' AND status=1");		
		foreach ($querySellers->rows as $subResult) {			
			if (!in_array($subResult['parent_id'], $sellersIds)){
				$subSellers[] = $subResult['seller_id'];				
			}
		}
		
		$allSeller = implode(",",array_merge($sellersIds,$subSellers));
				
		$sqlAll = "SELECT s.seller_id, s.firstname, s.lastname, s.email, s.image, s.minimum_order, s.delivery_charges, s.banner, s.package_ready, s.delivery_timegap, s.mon_start_time, s.mon_end_time, s.tue_start_time, s.tue_end_time, s.wed_start_time, s.wed_end_time, s.thu_start_time, s.thu_end_time, s.fri_start_time, s.fri_end_time, s.sat_start_time, s.sat_end_time, s.sun_start_time, s.sun_end_time, s.sortorder FROM " . DB_PREFIX . "sellers s WHERE s.seller_id IN (".$allSeller.") GROUP BY s.seller_id ORDER BY s.sortorder ASC, s.firstname ASC";
		
		$queryAll = $this->db->query($sqlAll);		
		
        return $queryAll->rows;

	}
	
}
