<?php
class ModelSlotTiming extends Model
{
    public function setTables()
    {
        $tbl_slot = "
             CREATE TABLE IF NOT EXISTS " . DB_PREFIX . "slot_setting
            ( `slot_id` int NOT NULL AUTO_INCREMENT , 
             `end_time` varchar(100) , 
             `intervals` int , 
             `delivery_starts` int , 
             `start_time` varchar(100) , 
             `show_slots` int ,
             `order_per_slot` int,
             `time_format` varchar(100) DEFAULT '12 ( AM/PM and O clock )',
             `range_date_from` varchar(100),
             `range_date_to` varchar(100),
             `date_format` varchar(100) DEFAULT 'yy-mm-dd',
             `time_zone` varchar(100) DEFAULT 'Asia/karachi',
             PRIMARY KEY (`slot_id`))";

        $this->db->query($tbl_slot);


        $table_exc = "CREATE TABLE IF NOT EXISTS " . DB_PREFIX . "exclude_dates( `exc_id` int NOT NULL AUTO_INCREMENT , "
            . "`exc_date_time_to` datetime , "
            . "`exc_date_time_from` datetime , PRIMARY KEY (`exc_id`))";

        $table_one_day_slot = "CREATE TABLE IF NOT EXISTS " . DB_PREFIX . "one_year_slot( `one_slot_id` int NOT NULL AUTO_INCREMENT , "
            . "`slot_timing` varchar(1000) , "
            . "`to_date_time` datetime , "
            . "`date` date , "
            . "`max_no` int DEFAULT '2' ,"
            . "`Status` varchar (1000) DEFAULT 'ENABLE',"
            . "`from_date_time` datetime , PRIMARY KEY (`one_slot_id`))";


        $delivery_time = "CREATE TABLE IF NOT EXISTS " . DB_PREFIX . "delivery_time( `id` int NOT NULL AUTO_INCREMENT , `order_id` int , `delivery_time` varchar(1000) , times varchar(1000) , PRIMARY KEY (`id`))";


        $weekly = "CREATE TABLE IF NOT EXISTS " . DB_PREFIX . "weekly_day_status( `w_id` int NOT NULL AUTO_INCREMENT , `w_sat` varchar(100) DEFAULT 'OPEN' , `w_sun` varchar(100) DEFAULT 'OPEN' , `w_mon` varchar(100) DEFAULT 'OPEN' , `w_tue` varchar(100) DEFAULT 'OPEN' , `w_wed` varchar(100) DEFAULT 'OPEN' , `w_thu` varchar(100) DEFAULT 'OPEN' , `w_fri` varchar(100) DEFAULT 'OPEN' , PRIMARY KEY (`w_id`)) ";


        $this->db->query($table_exc);
        $this->db->query($table_one_day_slot);
        $this->db->query($delivery_time);
        $this->db->query($weekly);

        $q = $this->db->query("SELECT * FROM " . DB_PREFIX . "weekly_day_status");

        if ($q->num_rows <= 0) {

            $this->db->query("insert into " . DB_PREFIX . "weekly_day_status(`w_id`,`w_sat`,`w_sun`,`w_mon`,`w_tue`,`w_wed`,`w_thu`,`w_fri`) values ( NULL,'OPEN','OPEN','OPEN','OPEN','OPEN','OPEN','OPEN')");
        }

        $slot = $this->db->query("SELECT * FROM " . DB_PREFIX . "slot_setting");

        if ($slot->num_rows <= 0) {


            $this->db->query("insert into " . DB_PREFIX . "slot_setting(`slot_id`,`end_time`,`intervals`,`delivery_starts`,`start_time`,`show_slots`,`order_per_slot`,`time_format` , `range_date_from` , `range_date_to` , `date_format` ,`time_zone`) values ( NULL,'22:00:00','2','2','01:00:00','10','1','12 ( AM/PM and O clock )','2015-11-04','2015-11-07','yy-mm-dd','Asia/karachi')");
        }
    }


    public function getTime($store_id)
    {


        $sql = "select show_slots,delivery_starts,time_format,date_format,time_zone from " . DB_PREFIX . "slot_setting WHERE store_id =" . $store_id;



        $query = $this->db->query($sql);

        if ($query->num_rows > 0) {

            $limit = $query->row['show_slots'];
            $lead = $query->row['delivery_starts'];
            $time_format = $query->row['time_format'];
            $date_format = $query->row['date_format'];
            $time_zone = $query->row['time_zone'];
        }

        if ($limit == "") {
            $limit = 1;
        }


        $_SESSION['time_zone'] = $time_zone;

        date_default_timezone_set($time_zone);

        $added_lead_time = date('Y-m-d H:i:s', strtotime("+$lead hour"));

        $data = array();

        $sql1 = "SELECT * from " . DB_PREFIX . "one_year_slot WHERE store_id = " . $store_id . " AND from_date_time>='" . $added_lead_time . "' ORDER BY from_date_time asc LIMIT $limit";




        $q = $this->db->query($sql1);

        if ($q->num_rows > 0) {


            $start_id = $q->rows[0]['one_slot_id'];




            foreach ($q->rows as $key => $result) {


                if ($date_format == "dd-mm-yy") {




                    $d = explode("-", $result['date']);

                    $year = $d[0];
                    $month = $d[1];
                    $dat = $d[2];

                    $ress = $dat . "-" . $month . "-" . $year;
                } else {

                    $ress = $result['date'];
                }

                $data1 = $result['one_slot_id'];
                $data[] = $ress;
            }


            if ($data != "") {

                $res = array_unique($data);
                $res['last_id'][] = $data1;
                $res['start_id'][] = $start_id;
                return $res;
            }
        }
    }


    public function countSlot($slot_time, $store_id)
    {

        $times = explode(" - ", $slot_time);

        $date = date_create($times[0]);

        $tm1 =  date_format($date, "y-m-d H:i:s");

        $date1 = date_create($times[1]);

        $tm2 =  date_format($date1, "y-m-d H:i:s");

        $new_time = $tm1 . " - " . $tm2;

        $slot = "SELECT max_no FROM " . DB_PREFIX . "one_year_slot WHERE store_id=" . $store_id . " AND slot_timing='" . $new_time . "'";

        $query = $this->db->query($slot);

        if ($query->num_rows > 0) {

            $conf_order_per_slot = $query->row['max_no'];
        }

        $delivery_time = "SELECT * from " . DB_PREFIX . "delivery_time where store_id=" . $store_id . " AND times LIKE '%" . $new_time . "%'";

        $qu = $this->db->query($delivery_time);

        if ($qu->num_rows >= $conf_order_per_slot) {

            echo "fulled";
        } else {

            $this->session->data['delivery_time'] = $slot_time;
            echo "not fulled";
        }
    }

    public function validateTimeSlot($slot_time, $store_id)
    {

        $times = explode(" - ", $slot_time);

        $date = date_create($times[0]);

        $tm1 =  date_format($date, "y-m-d H:i:s");

        $date1 = date_create($times[1]);

        $tm2 =  date_format($date1, "y-m-d H:i:s");

        $new_time = $tm1 . " - " . $tm2;

        $slot = "SELECT max_no FROM " . DB_PREFIX . "one_year_slot WHERE store_id=" . $store_id . " AND slot_timing='" . $new_time . "'";

        $query = $this->db->query($slot);

        if ($query->num_rows > 0) {

            $conf_order_per_slot = $query->row['max_no'];
        }

        $delivery_time = "SELECT * from " . DB_PREFIX . "delivery_time where times LIKE '%" . $new_time . "%'";

        $qu = $this->db->query($delivery_time);

        if ($qu->num_rows >= $conf_order_per_slot) {
            return "Slot is not available! Try another time slot";
        } else {
            return "Success";
        }
    }


    public function getInstalled($type)
    {



        $extension_data = array();

        $query = $this->db->query("SELECT * FROM " . DB_PREFIX . "extension WHERE `type` = '" . $this->db->escape($type) . "' ORDER BY code");

        foreach ($query->rows as $result) {
            $extension_data[] = $result['code'];
        }

        return $extension_data;
    }

    public function getDeliveryTime($orderId)
    {
        $order_query = $this->db->query("SELECT * FROM " . DB_PREFIX . "delivery_time WHERE order_id = '" . (int) $orderId . "'");

        $dt = array('time' => 'null', 'date' => 'null');

        if ($order_query->num_rows) {

            $delivery_time = trim($order_query->row['delivery_time']);
            $dtArray       = explode(" ", trim($delivery_time));

            if (count($dtArray) == 7) {
                $time = $dtArray[1] . " - " . $dtArray[6];
                $dt   = array('time' => $time, 'date' => $dtArray[0]);
            } else if (count($dtArray) == 5) {
                $time = $dtArray[1] . " - " . $dtArray[4];
                $dt   = array('time' => $time, 'date' => $dtArray[0]);
            } else if (count($dtArray) == 3) {
                $time = $dtArray[1] . " - " . date('h:i', strtotime('+2 hours', strtotime($dtArray[1])));
                $dt   = array('time' => $time, 'date' => $dtArray[0]);
            }
            
        }


        return $dt;
    }



    public function getNextDeliveryTime($store_id)
    {

        $ids = $this->getDates($store_id);

        $today =  date("d-m-Y");

        if ((isset($ids)) && sizeof($ids) >= 2 && isset($store_id)) {

            $times =  $this->getTimeByDate($today, $ids[0], $ids[1], $store_id);
        } else {
            $times = '';
        }
        $times;
        if ($times != "") {
            return "Next Delivery Today: " . $times[0];
        } else {
            return "No Delivery Slot Available.";
        }
    }
    public function getDates($store_id)
    {

        $res = array("msg" => "Failed");

        $sqlSetting = "SELECT show_slots,delivery_starts,time_format,date_format,time_zone FROM " . DB_PREFIX . "slot_setting WHERE store_id =" . $store_id;

        $query = $this->db->query($sqlSetting);

        if ($query->num_rows > 0) {

            $limit = $query->row['show_slots'];
            $lead = $query->row['delivery_starts'];
            $time_format = $query->row['time_format'];
            $date_format = $query->row['date_format'];
            $time_zone = $query->row['time_zone'];
        }

        if ($limit == "") {
            $limit = 1;
        }

        $_SESSION['time_zone'] = $time_zone;

        date_default_timezone_set($time_zone);

        $added_lead_time = date('Y-m-d H:i:s', strtotime("+$lead hour"));

        $data = array();

        $sqlSlots = "SELECT * FROM " . DB_PREFIX . "one_year_slot WHERE store_id = " . $store_id . " AND from_date_time>='" . $added_lead_time . "' ORDER BY from_date_time ASC LIMIT $limit";

        $q = $this->db->query($sqlSlots);

        if ($q->num_rows > 0) {

            $start_id = $q->rows[0]['one_slot_id'];
            foreach ($q->rows as $key => $result) {

                if ($date_format == "dd-mm-yy") {
                    $d = explode("-", $result['date']);

                    $year = $d[0];
                    $month = $d[1];
                    $dat = $d[2];

                    $ress = $dat . "-" . $month . "-" . $year;
                } else {

                    $ress = $result['date'];
                }

                $data1 = $result['one_slot_id'];
                $data[] = $ress;
            }

            if ($data != "") {
                $res = array($start_id, $data1);
            }
        }
        return $res;
    }
    public function getTimeByDate($search_date, $start_id, $last_id, $store_id)
    {


        $sqlSetting = "SELECT show_slots,delivery_starts,time_format,date_format,time_zone FROM " . DB_PREFIX . "slot_setting WHERE store_id=" . $store_id;

        $query = $this->db->query($sqlSetting);

        if ($query->num_rows > 0) {

            $time_format = $query->row['time_format'];
            $date_format = $query->row['date_format'];
            $lead = $query->row['delivery_starts'];
            $time_zone = $query->row['time_zone'];
        }


        date_default_timezone_set($time_zone);

        $SearchTime = "";

        $SearchTime = $search_date;

        if ($SearchTime) {
            if ($date_format == 'dd-mm-yy') {
                $d = explode("-", $SearchTime);
                $year = $d[2];
                $month = $d[1];
                $dat = $d[0];
                $SearchTime = $year . "-" . $month . "-" . $dat;
            }
        }

        $start = $start_id;

        $added_lead_time = date('H:i:s', strtotime("+$lead hour"));

        $lead_time = $SearchTime . " " . $added_lead_time;

        $q = $this->db->query("SELECT * from (SELECT slot_timing,to_date_time,from_date_time, one_slot_id,max_no, (SELECT COUNT(*) FROM " . DB_PREFIX . "delivery_time v WHERE store_id=" . $store_id . " AND times LIKE p.slot_timing ) countSlot FROM " . DB_PREFIX . "one_year_slot p WHERE store_id=" . $store_id . " AND to_date_time like '%" . $SearchTime . "%' AND one_slot_id >= '" . $start . "' AND Status='ENABLE') a WHERE a.countSlot!=a.max_no ORDER BY a.one_slot_id ASC");

        if ($q->num_rows > 0) {

            foreach ($q->rows as $val) :
                if ($last_id >= $val['one_slot_id']) {
                    $to = explode(" ", $val['to_date_time']);
                    $from = explode(" ", $val['from_date_time']);
                    $slot_timing = $val['slot_timing'];
                    if ($from[1] > date('H:i:s')) {
                        if ($time_format == '24 ( Military/Continental Time )') {
                            $time_in_12_hour_format_from  = date("H:i:s", strtotime($from[1]));
                            $time_in_12_hour_format_to  = date("H:i:s", strtotime($to[1]));
                        } else if ($time_format == "12 ( AM/PM and O clock )") {
                            $time_in_12_hour_format_from  = date("g:i:s A", strtotime($from[1]));
                            $time_in_12_hour_format_to  = date("g:i:s A", strtotime($to[1]));
                        }
                        $times[] =  $time_in_12_hour_format_from . " - " . $time_in_12_hour_format_to;
                    } else {
                        if ($time_format == '24 ( Military/Continental Time )') {
                            $time_in_12_hour_format_from  = date("H:i:s", strtotime($from[1]));
                            $time_in_12_hour_format_to  = date("H:i:s", strtotime($to[1]));
                        } else if ($time_format == "12 ( AM/PM and O clock )") {
                            $time_in_12_hour_format_from  = date("g:i:s A", strtotime($from[1]));
                            $time_in_12_hour_format_to  = date("g:i:s A", strtotime($to[1]));
                        }
                        $times[] =  $time_in_12_hour_format_from . " - " . $time_in_12_hour_format_to;
                    }
                }
            endforeach;
            // print_r(json_encode($times));
            return $times;
        } else {
            $res = array("No Time Slot Available.");
            return $res;
            // print_r(json_encode($res));
        }
    }
    public function getDeliverySlots($seller_id)
    {

        $query = $this->db->query("SELECT mon_start_time,mon_end_time, tue_start_time,tue_end_time, wed_start_time,wed_end_time, thu_start_time,thu_end_time, fri_start_time,fri_end_time, sat_start_time,sat_end_time, sun_start_time,sun_end_time FROM  " . DB_PREFIX . "sellers WHERE seller_id=" . $seller_id);

        return $query->row;
    }
}
