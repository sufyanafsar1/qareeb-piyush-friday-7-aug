<?php
class ModelStoreStore extends Model {

    public function getSellerByAreaId($areaId, $defaultStore) {
        /* $sql = "SELECT s.seller_id, s.firstname,s.lastname, s.email, s.image, s.minimum_order, s.delivery_charges FROM " . DB_PREFIX . "sellers s JOIN " . DB_PREFIX . "saddress a ON s.seller_id = a.seller_id WHERE a.area_id = '" . (int) $areaId . "' AND status=1";

        if ((int) $defaultStore > 0) {
            $sql .= " OR a.seller_id=" . (int) $defaultStore;
        }

        $query = $this->db->query($sql);

        return $query->rows; */
		
		//get Main Seller
        $sql = "SELECT s.seller_id, s.firstname,s.lastname, s.email, s.image, s.minimum_order, s.delivery_charges FROM " . DB_PREFIX . "sellers s JOIN " . DB_PREFIX . "saddress a ON s.seller_id = a.seller_id WHERE a.area_id = '" . (int) $areaId . "' AND status=1 AND parent_id = 0 ORDER BY s.sortorder ASC, s.firstname ASC";

        if ((int) $defaultStore > 0) {
            $sql .= " OR a.seller_id=" . (int) $defaultStore;
        }

		$sellers = array();
		$sellersIds = array();
		$query = $this->db->query($sql);
		foreach ($query->rows as $result) {		
			$sellersIds[] = $result['seller_id'];
			$sellers[] = $result;
		}
		
		//get Virtual Seller
		$subSellers = array();		
		$querySellers = $this->db->query("SELECT s.parent_id, s.seller_id, s.firstname,s.lastname, s.email, s.image, s.minimum_order, s.delivery_charges FROM " . DB_PREFIX . "sellers s WHERE status=1 AND s.parent_id != '0' ORDER BY s.sortorder ASC, s.firstname ASC");		
		foreach ($querySellers->rows as $subResult) {			
			if (!in_array($subResult['parent_id'], $sellersIds)){
				$subSellers[] = $subResult;				
			}
		}
		
		return array_merge($sellers,$subSellers); 
    }
	
	public function getStoresByAreaIdbk($areaId) {
        $sql = "SELECT s.seller_id,s.firstname,s.lastname,s.email,s.image,minimum_order,delivery_charges,s.banner  FROM " . DB_PREFIX . "sellers s JOIN " . DB_PREFIX . "saddress a ON s.seller_id = a.seller_id WHERE a.area_id = '" . (int) $areaId . "' AND status=1";

        $query = $this->db->query($sql);

        return $query->rows;
    }
	
	 public function getStoresByAreaId($areaId) {
        /* $sql = "SELECT s.seller_id, s.firstname, s.lastname, s.email, s.image, s.minimum_order, s.delivery_charges, s.banner, s.package_ready, s.delivery_timegap, s.mon_start_time, s.mon_end_time, s.tue_start_time, s.tue_end_time, s.wed_start_time, s.wed_end_time, s.thu_start_time, s.thu_end_time, s.fri_start_time, s.fri_end_time, s.sat_start_time, s.sat_end_time, s.sun_start_time, s.sun_end_time, s.sortorder  FROM " . DB_PREFIX . "sellers s JOIN " . DB_PREFIX . "saddress a ON s.seller_id = a.seller_id WHERE a.area_id = '" . (int) $areaId . "' AND status=1 ORDER BY s.sortorder ASC, s.firstname ASC ";
		
        $query = $this->db->query($sql);

        return $query->rows; */
		
		//get Main Seller
        $sql = "SELECT s.seller_id, s.firstname, s.lastname, s.email, s.image, s.minimum_order, s.delivery_charges, s.banner, s.package_ready, s.delivery_timegap, s.mon_start_time, s.mon_end_time, s.tue_start_time, s.tue_end_time, s.wed_start_time, s.wed_end_time, s.thu_start_time, s.thu_end_time, s.fri_start_time, s.fri_end_time, s.sat_start_time, s.sat_end_time, s.sun_start_time, s.sun_end_time, s.sortorder FROM " . DB_PREFIX . "sellers s JOIN " . DB_PREFIX . "saddress a ON s.seller_id = a.seller_id WHERE a.area_id = '" . (int) $areaId . "' AND status=1 AND parent_id = 0";

        if ((int) $defaultStore > 0) {
            $sql .= " OR a.seller_id=" . (int) $defaultStore;
        }

		$sellers = array();
		$sellersIds = array();
		$query = $this->db->query($sql);
		foreach ($query->rows as $result) {
			$sellersIds[] = $result['seller_id'];
			$sellers[] = $result;
		}
		
		//get Virtual Seller
		$subSellers = array();		
		$querySellers = $this->db->query("SELECT s.parent_id, s.seller_id, s.firstname, s.lastname, s.email, s.image, s.minimum_order, s.delivery_charges, s.banner, s.package_ready, s.delivery_timegap, s.mon_start_time, s.mon_end_time, s.tue_start_time, s.tue_end_time, s.wed_start_time, s.wed_end_time, s.thu_start_time, s.thu_end_time, s.fri_start_time, s.fri_end_time, s.sat_start_time, s.sat_end_time, s.sun_start_time, s.sun_end_time, s.sortorder FROM " . DB_PREFIX . "sellers s WHERE s.parent_id != '0' AND status=1 ORDER BY s.sortorder ASC, s.firstname ASC");		
		foreach ($querySellers->rows as $subResult) {			
			if (!in_array($subResult['parent_id'], $sellersIds)){
				$subSellers[] = $subResult;				
			}
		}
		
		return array_merge($sellers,$subSellers);
    }

    public function getSellerByRadius($radiusKm, $lat, $lng) {
        /* // 6371 Earth distance = 25km  1km =  6371/25 = 254.84 Earth distance , 3959 Earth distance = 25miles
        $sql = "SELECT s.seller_id,s.firstname, s.lastname, s.email, s.image, minimum_order, delivery_charges, s.banner, s.package_ready, s.delivery_timegap, s.mon_start_time, s.mon_end_time, s.tue_start_time, s.tue_end_time, s.wed_start_time, s.wed_end_time, s.thu_start_time, s.thu_end_time, s.fri_start_time, s.fri_end_time, s.sat_start_time, s.sat_end_time, s.sun_start_time, s.sun_end_time, (6371 * acos( cos( radians(" . $lat . ") ) * cos( radians( a.latitude ) ) * cos( radians( a.longitude ) - radians(" . $lng . ") ) + sin( radians(" . $lat . ") ) * sin( radians( a.latitude ) ) ) ) AS distance, a.longitude,  a.latitude, s.sortorder FROM " . DB_PREFIX . "sellers s JOIN " . DB_PREFIX . "saddress a ON s.seller_id = a.seller_id WHERE s.status=1 GROUP BY s.seller_id HAVING distance <= " . $radiusKm . " ORDER BY s.sortorder ASC, s.firstname ASC LIMIT 0 , 20";

        $query = $this->db->query($sql);

        return $query->rows; */
		
		// 6371 Earth distance = 25km  1km =  6371/25 = 254.84 Earth distance , 3959 Earth distance = 25miles
        $sql = "SELECT s.seller_id, s.firstname, s.lastname, s.email, s.image, s.minimum_order, s.delivery_charges, s.banner, s.package_ready, s.delivery_timegap, s.mon_start_time, s.mon_end_time, s.tue_start_time, s.tue_end_time, s.wed_start_time, s.wed_end_time, s.thu_start_time, s.thu_end_time, s.fri_start_time, s.fri_end_time, s.sat_start_time, s.sat_end_time, s.sun_start_time, s.sun_end_time, a.longitude,  a.latitude, s.sortorder, (6371 * acos( cos( radians(" . $lat . ") ) * cos( radians( a.latitude ) ) * cos( radians( a.longitude ) - radians(" . $lng . ") ) + sin( radians(" . $lat . ") ) * sin( radians( a.latitude ) ) ) ) AS distance
		FROM " . DB_PREFIX . "sellers s 
		JOIN " . DB_PREFIX . "saddress a ON s.seller_id = a.seller_id 
		WHERE s.status=1 
		AND parent_id = 0
		GROUP BY s.seller_id 
		HAVING distance <= " . $radiusKm . " 
		ORDER BY s.sortorder ASC, s.firstname ASC";

        $query = $this->db->query($sql);

		$sellers = array();
		$sellersIds = array();
		foreach ($query->rows as $result) {
			$sellersIds[] = $result['seller_id'];			
			$sellers[] = $result;
		}
		
		//get Virtual Seller
		$subSellers = array();		
		$querySellers = $this->db->query("SELECT s.parent_id, s.seller_id, s.firstname, s.lastname, s.email, s.image, s.minimum_order, s.delivery_charges, s.banner, s.package_ready, s.delivery_timegap, s.mon_start_time, s.mon_end_time, s.tue_start_time, s.tue_end_time, s.wed_start_time, s.wed_end_time, s.thu_start_time, s.thu_end_time, s.fri_start_time, s.fri_end_time, s.sat_start_time, s.sat_end_time, s.sun_start_time, s.sun_end_time, a.longitude,  a.latitude, s.sortorder, (6371 * acos( cos( radians(" . $lat . ") ) * cos( radians( a.latitude ) ) * cos( radians( a.longitude ) - radians(" . $lng . ") ) + sin( radians(" . $lat . ") ) * sin( radians( a.latitude ) ) ) ) AS distance
		FROM " . DB_PREFIX . "sellers s 
		JOIN " . DB_PREFIX . "saddress a ON s.seller_id = a.seller_id 
		WHERE s.status=1 AND s.parent_id != '0' GROUP BY s.seller_id ORDER BY s.sortorder ASC, s.firstname ASC");		
		foreach ($querySellers->rows as $subResult) {			
			if (!in_array($subResult['parent_id'], $sellersIds)){
				//$subSellers[] = $subResult['parent_id'];	
				$subSellers[] = $subResult;	
			}
		}
				
		return array_merge($sellers,$subSellers); 
		
    }

    public function getSellerByID($seller_id) {
        $query = $this->db->query("SELECT * FROM " . DB_PREFIX . "sellers WHERE seller_id = '" . (int) $seller_id . "'");

        return $query->row;
    }

    public function getAllSellersInfo() {
        $query = $this->db->query("SELECT * FROM " . DB_PREFIX . "sellers");

        return $query->row;
    }

    public function getTimebhutta($seller_id) {
        $query = $this->db->query("SELECT * FROM " . DB_PREFIX . "sellers WHERE seller_id = '" . (int) $seller_id . "'");

        $sellerData = $query->row;

        if ((date("l") == 'Monday') && $sellerData['mon_end_time'] > date("h:i:sa")) {
            $storeTimeStatusText = 'Same day delivery.';
            $storeTimeStatus = 1;
        } else if ((date("l") == 'Monday') && $sellerData['mon_end_time'] < date("h:i:sa")) {
            $storeTimeStatusText = 'Opps Store Closed! Delivery will be on next working day.';
            $storeTimeStatus = 0;
        }

        if ((date("l") == 'Tuesday') && $sellerData['tue_end_time'] > date("h:i:sa")) {
            $storeTimeStatusText = 'Same day delivery.';
            $storeTimeStatus = 1;
        } else if ((date("l") == 'Tuesday') && $sellerData['tue_end_time'] < date("h:i:sa")) {
            $storeTimeStatusText = 'Opps Store Closed! Delivery will be on next working day.';
            $storeTimeStatus = 0;
        }

        if ((date("l") == 'Wednesday') && $sellerData['wed_end_time'] > date("h:i:sa")) {
            $storeTimeStatusText = 'Same day delivery.';
            $storeTimeStatus = 1;
        } else if ((date("l") == 'Wednesday') && $sellerData['wed_end_time'] < date("h:i:sa")) {
            $storeTimeStatusText = 'Opps Store Closed! Delivery will be on next working day.';
            $storeTimeStatus = 0;
        }

        if ((date("l") == 'Thursday') && $sellerData['thu_end_time'] > date("h:i:sa")) {
            $storeTimeStatusText = 'Same day delivery.';
            $storeTimeStatus = 1;
        } else if ((date("l") == 'Thursday') && $sellerData['thu_end_time'] < date("h:i:sa")) {
            $storeTimeStatusText = 'Opps Store Closed! Delivery will be on next working day.';
            $storeTimeStatus = 0;
        }

        if ((date("l") == 'Friday') && $sellerData['fri_end_time'] > date("h:i:sa")) {
            $storeTimeStatusText = 'Same day delivery.';
            $storeTimeStatus = 1;
        } else if ((date("l") == 'Friday') && $sellerData['fri_end_time'] < date("h:i:sa")) {
            $storeTimeStatusText = 'Opps Store Closed! Delivery will be on next working day.';
            $storeTimeStatus = 0;
        }

        if ((date("l") == 'Saturday') && $sellerData['sat_end_time'] > date("h:i:sa")) {
            $storeTimeStatusText = 'Same day delivery.';
            $storeTimeStatus = 1;
        } else if ((date("l") == 'Saturday') && $sellerData['sat_end_time'] < date("h:i:sa")) {
            $storeTimeStatusText = 'Opps Store Closed! Delivery will be on next working day.';
            $storeTimeStatus = 0;
        }

        if ((date("l") == 'Sunday') && $sellerData['sun_end_time'] > date("h:i:sa")) {
            $storeTimeStatusText = 'Same day delivery.';
            $storeTimeStatus = 1;
        } else if ((date("l") == 'Sunday') && $sellerData['sun_end_time'] < date("h:i:sa")) {
            $storeTimeStatusText = 'Opps Store Closed! Delivery will be on next working day.';
            $storeTimeStatus = 0;
        }

        $storeTimeResult['storeTimeStatusText'] = $storeTimeStatusText;
        $storeTimeResult['storeTimeStatus'] = $storeTimeStatus;
        return $storeTimeResult;
    }

}
