<?php
class ModelTotalCoupon extends Model {
	public function getMobileCouponnew($code,$customer_id,$language_id,$deviceId, $seller_id) {
		$status = true;

		$coupon_query = $this->db->query("SELECT * FROM `" . DB_PREFIX . "coupon` WHERE code = '" . $this->db->escape($code) . "' AND ((date_start = '0000-00-00' OR date_start < NOW()) AND (date_end = '0000-00-00' OR date_end > NOW())) AND status = '1'");

		if ($coupon_query->num_rows) {
		  
			if ($coupon_query->row['total'] > $this->cart->getMobileSubTotal($customer_id,$language_id,$deviceId)) {
				$status = false;
			}

			$coupon_history_query = $this->db->query("SELECT COUNT(*) AS total FROM `" . DB_PREFIX . "coupon_history` ch WHERE ch.coupon_id = '" . (int)$coupon_query->row['coupon_id'] . "'");

			if ($coupon_query->row['uses_total'] > 0 && ($coupon_history_query->row['total'] >= $coupon_query->row['uses_total'])) {
				$status = false;
			}

			if ($coupon_query->row['logged'] && !$customer_id) {
				$status = false;
			}

			if ($customer_id) {
				$coupon_history_query = $this->db->query("SELECT COUNT(*) AS total FROM `" . DB_PREFIX . "coupon_history` ch WHERE ch.coupon_id = '" . (int)$coupon_query->row['coupon_id'] . "' AND ch.customer_id = '" . (int)$customer_id . "'");

				if ($coupon_query->row['uses_customer'] > 0 && ($coupon_history_query->row['total'] >= $coupon_query->row['uses_customer'])) {
					$status = false;
				}
			}
			
			if($coupon_query->row['all_seller'] == 0){
				if($seller_id != $coupon_query->row['sellerid']){
					$status = false;
				}
			}
			
		
			// Products
			$coupon_product_data = array();

			$coupon_product_query = $this->db->query("SELECT * FROM `" . DB_PREFIX . "coupon_product` WHERE coupon_id = '" . (int)$coupon_query->row['coupon_id'] . "'");

			foreach ($coupon_product_query->rows as $product) {
				$coupon_product_data[] = $product['product_id'];
			}

			// Categories
			$coupon_category_data = array();

			$coupon_category_query = $this->db->query("SELECT * FROM `" . DB_PREFIX . "coupon_category` cc LEFT JOIN `" . DB_PREFIX . "category_path` cp ON (cc.category_id = cp.path_id) WHERE cc.coupon_id = '" . (int)$coupon_query->row['coupon_id'] . "'");

			foreach ($coupon_category_query->rows as $category) {
				$coupon_category_data[] = $category['category_id'];
			}

			$product_data = array();

			if ($coupon_product_data || $coupon_category_data) {
				foreach ($this->cart->getMobileProductstest($customer_id,$language_id,$deviceId) as $product) {
					if (in_array($product['product_id'], $coupon_product_data)) {
						$product_data[] = $product['product_id'];

						continue;
					}

					foreach ($coupon_category_data as $category_id) {
						$coupon_category_query = $this->db->query("SELECT COUNT(*) AS total FROM `" . DB_PREFIX . "product_to_category` WHERE `product_id` = '" . (int)$product['product_id'] . "' AND category_id = '" . (int)$category_id . "'");

						if ($coupon_category_query->row['total']) {
							$product_data[] = $product['product_id'];

							continue;
						}
					}
				}

				if (!$product_data) {
					$status = false;
				}
			}
		} else {
			$status = false;
		}

		if ($status) {
			return array(
				'coupon_id'     => $coupon_query->row['coupon_id'],
				'code'          => $coupon_query->row['code'],
				'name'          => $coupon_query->row['name'],
				'type'          => $coupon_query->row['type'],
				'discount'      => $coupon_query->row['discount'],
				'shipping'      => $coupon_query->row['shipping'],
				'total'         => $coupon_query->row['total'],
				'product'       => $product_data,
				'date_start'    => $coupon_query->row['date_start'],
				'date_end'      => $coupon_query->row['date_end'],
				'uses_total'    => $coupon_query->row['uses_total'],
				'uses_customer' => $coupon_query->row['uses_customer'],
				'status'        => $coupon_query->row['status'],
				'date_added'    => $coupon_query->row['date_added']
			);
		}
	}
	
	
	public function getDesktopCouponnew($code,$customer_id,$language_id,$session_id, $sellers) {
		$status = true;
		
		$coupon_query = $this->db->query("SELECT * FROM `" . DB_PREFIX . "coupon` WHERE code = '" . $this->db->escape($code) . "' AND ((date_start = '0000-00-00' OR date_start < NOW()) AND (date_end = '0000-00-00' OR date_end > NOW())) AND status = '1'");

		if ($coupon_query->num_rows) {
			
			if ($coupon_query->row['total'] > $this->cart->getDesktopSubTotal($customer_id,$language_id,$session_id)) {
				$status = false;
			}
			
			$coupon_history_query = $this->db->query("SELECT COUNT(*) AS total FROM `" . DB_PREFIX . "coupon_history` ch WHERE ch.coupon_id = '" . (int)$coupon_query->row['coupon_id'] . "'");

			if ($coupon_query->row['uses_total'] > 0 && ($coupon_history_query->row['total'] >= $coupon_query->row['uses_total'])) {
				$status = false;
			}

			if ($coupon_query->row['logged'] && !$customer_id) {
				$status = false;
			}

			if ($customer_id) {
				$coupon_history_query = $this->db->query("SELECT COUNT(*) AS total FROM `" . DB_PREFIX . "coupon_history` ch WHERE ch.coupon_id = '" . (int)$coupon_query->row['coupon_id'] . "' AND ch.customer_id = '" . (int)$customer_id . "'");

				if ($coupon_query->row['uses_customer'] > 0 && ($coupon_history_query->row['total'] >= $coupon_query->row['uses_customer'])) {
					$status = false;
				}
			}
			// echo $coupon_query->row['multi_sellers'];
			
			$coupon_seller = explode(",", $coupon_query->row['multi_sellers']);
			$seller_status = 0;
			
			if($coupon_query->row['all_seller'] == 0){
				foreach($coupon_seller as $seller){
					
					if(in_array($seller, $sellers)) {
						$seller_status = 1;
					}
				}
				if(!$seller_status){
					$status = false;
				}
			}
			
		
			// Products
			$coupon_product_data = array();

			$coupon_product_query = $this->db->query("SELECT * FROM `" . DB_PREFIX . "coupon_product` WHERE coupon_id = '" . (int)$coupon_query->row['coupon_id'] . "'");

			foreach ($coupon_product_query->rows as $product) {
				$coupon_product_data[] = $product['product_id'];
			}

			// Categories
			$coupon_category_data = array();

			$coupon_category_query = $this->db->query("SELECT * FROM `" . DB_PREFIX . "coupon_category` cc LEFT JOIN `" . DB_PREFIX . "category_path` cp ON (cc.category_id = cp.path_id) WHERE cc.coupon_id = '" . (int)$coupon_query->row['coupon_id'] . "'");

			foreach ($coupon_category_query->rows as $category) {
				$coupon_category_data[] = $category['category_id'];
			}

			$product_data = array();

			if ($coupon_product_data || $coupon_category_data) {
				foreach ($this->cart->getMobileProductstest($customer_id,$language_id,$deviceId) as $product) {
					if (in_array($product['product_id'], $coupon_product_data)) {
						$product_data[] = $product['product_id'];

						continue;
					}

					foreach ($coupon_category_data as $category_id) {
						$coupon_category_query = $this->db->query("SELECT COUNT(*) AS total FROM `" . DB_PREFIX . "product_to_category` WHERE `product_id` = '" . (int)$product['product_id'] . "' AND category_id = '" . (int)$category_id . "'");

						if ($coupon_category_query->row['total']) {
							$product_data[] = $product['product_id'];

							continue;
						}
					}
				}

				if (!$product_data) {
					$status = false;
				}
			}
		} else {
			$status = false;
		}
		
		if ($status) {
			return array(
				'coupon_id'     => $coupon_query->row['coupon_id'],
				'code'          => $coupon_query->row['code'],
				'name'          => $coupon_query->row['name'],
				'type'          => $coupon_query->row['type'],
				'discount'      => $coupon_query->row['discount'],
				'shipping'      => $coupon_query->row['shipping'],
				'total'         => $coupon_query->row['total'],
				'product'       => $product_data,
				'date_start'    => $coupon_query->row['date_start'],
				'date_end'      => $coupon_query->row['date_end'],
				'uses_total'    => $coupon_query->row['uses_total'],
				'uses_customer' => $coupon_query->row['uses_customer'],
				'status'        => $coupon_query->row['status'],
				'date_added'    => $coupon_query->row['date_added']
			);
		}
	}
	
	
	public function getCoupon($code) {
		$status = true;

		$coupon_query = $this->db->query("SELECT * FROM `" . DB_PREFIX . "coupon` WHERE code = '" . $this->db->escape($code) . "' AND ((date_start = '0000-00-00' OR date_start < NOW()) AND (date_end = '0000-00-00' OR date_end > NOW())) AND status = '1'");
	
		if ($coupon_query->num_rows) {
			if ($coupon_query->row['total'] > $this->cart->getSubTotal()) {
				$status = false;
			}

			$coupon_history_query = $this->db->query("SELECT COUNT(*) AS total FROM `" . DB_PREFIX . "coupon_history` ch WHERE ch.coupon_id = '" . (int)$coupon_query->row['coupon_id'] . "'");

			if ($coupon_query->row['uses_total'] > 0 && ($coupon_history_query->row['total'] >= $coupon_query->row['uses_total'])) {
				$status = false;
			}

			if ($coupon_query->row['logged'] && !$this->customer->getId()) {
				$status = false;
			}

			if ($this->customer->getId()) {
				$coupon_history_query = $this->db->query("SELECT COUNT(*) AS total FROM `" . DB_PREFIX . "coupon_history` ch WHERE ch.coupon_id = '" . (int)$coupon_query->row['coupon_id'] . "' AND ch.customer_id = '" . (int)$this->customer->getId() . "'");

				if ($coupon_query->row['uses_customer'] > 0 && ($coupon_history_query->row['total'] >= $coupon_query->row['uses_customer'])) {
					$status = false;
				}
			}

			// Products
			$coupon_product_data = array();

			$coupon_product_query = $this->db->query("SELECT * FROM `" . DB_PREFIX . "coupon_product` WHERE coupon_id = '" . (int)$coupon_query->row['coupon_id'] . "'");

			foreach ($coupon_product_query->rows as $product) {
				$coupon_product_data[] = $product['product_id'];
			}

			// Categories
			$coupon_category_data = array();

			$coupon_category_query = $this->db->query("SELECT * FROM `" . DB_PREFIX . "coupon_category` cc LEFT JOIN `" . DB_PREFIX . "category_path` cp ON (cc.category_id = cp.path_id) WHERE cc.coupon_id = '" . (int)$coupon_query->row['coupon_id'] . "'");

			foreach ($coupon_category_query->rows as $category) {
				$coupon_category_data[] = $category['category_id'];
			}

			$product_data = array();

			if ($coupon_product_data || $coupon_category_data) {
				foreach ($this->cart->getProducts() as $product) {
					if (in_array($product['product_id'], $coupon_product_data)) {
						$product_data[] = $product['product_id'];

						continue;
					}

					foreach ($coupon_category_data as $category_id) {
						$coupon_category_query = $this->db->query("SELECT COUNT(*) AS total FROM `" . DB_PREFIX . "product_to_category` WHERE `product_id` = '" . (int)$product['product_id'] . "' AND category_id = '" . (int)$category_id . "'");

						if ($coupon_category_query->row['total']) {
							$product_data[] = $product['product_id'];

							continue;
						}
					}
				}

				if (!$product_data) {
					$status = false;
				}
			}
		} else {
			$status = false;
		}

		if ($status) {
			return array(
				'coupon_id'     => $coupon_query->row['coupon_id'],
				'code'          => $coupon_query->row['code'],
				'name'          => $coupon_query->row['name'],
				'type'          => $coupon_query->row['type'],
				'discount'      => $coupon_query->row['discount'],
				'multi_sellers' => $coupon_query->row['multi_sellers'],
				'shipping'      => $coupon_query->row['shipping'],
				'total'         => $coupon_query->row['total'],
				'product'       => $product_data,
				'date_start'    => $coupon_query->row['date_start'],
				'date_end'      => $coupon_query->row['date_end'],
				'uses_total'    => $coupon_query->row['uses_total'],
				'uses_customer' => $coupon_query->row['uses_customer'],
				'status'        => $coupon_query->row['status'],
				'date_added'    => $coupon_query->row['date_added']
			);
		}
	}

	public function getTotal(&$total_data, &$total, &$taxes) {
		if (isset($this->session->data['coupon'])) {
			$this->load->language('total/coupon');

			$coupon_info = $this->getCoupon($this->session->data['coupon']);

			if ($coupon_info) {
				
				if($coupon_info['coupon_id'] == 30){
					$discount_total = 0;
					
					/*--------------------------------------------*/
					if((!isset($this->session->data['sellerId'])) || ($this->session->data['sellerId'] == 0) || ($this->session->data['sellerId'] == '')){						
						foreach ($this->cart->getProducts() as $product) {						
							$this->session->data['sellerId'] = $product['seller_id'];				
						}
					}
					/*--------------------------------------------*/
					
					$deliveryChargesArr = $this->db->query("SELECT oc_sellers.delivery_charges FROM oc_sellers  WHERE oc_sellers.seller_id in ( ".$this->session->data['sellerId']." )")->row;
					
					$discount_total = $deliveryChargesArr['delivery_charges'];
					
					$total_data[] = array(
						'code'       => 'coupon',
						'title'      => sprintf($this->language->get('text_coupon'), $this->session->data['coupon']),
						'value'      => -$discount_total,
						'sort_order' => $this->config->get('coupon_sort_order')
					);
					
					$total -= $discount_total;
				} else {
					
					$discount_total = 0;

					if (!$coupon_info['product']) {
						$sub_total = $this->cart->getSubTotal();
					} else {
						$sub_total = 0;

						foreach ($this->cart->getProducts() as $product) {
							if (in_array($product['product_id'], $coupon_info['product'])) {
								$sub_total += $product['total'];
							}
						}
					}

					if ($coupon_info['type'] == 'F') {
						$coupon_info['discount'] = min($coupon_info['discount'], $sub_total);
					}

					foreach ($this->cart->getProducts() as $product) {
						$discount = 0;

						if (!$coupon_info['product']) {
							$status = true;
						} else {
							if (in_array($product['product_id'], $coupon_info['product'])) {
								$status = true;
							} else {
								$status = false;
							}
						}

						if ($status) {
							if ($coupon_info['type'] == 'F') {
								$discount = $coupon_info['discount'] * ($product['total'] / $sub_total);
							} elseif ($coupon_info['type'] == 'P') {
								$discount = $product['total'] / 100 * $coupon_info['discount'];
							}

							if ($product['tax_class_id']) {
								$tax_rates = $this->tax->getRates($product['total'] - ($product['total'] - $discount), $product['tax_class_id']);

								foreach ($tax_rates as $tax_rate) {
									if ($tax_rate['type'] == 'P') {
										$taxes[$tax_rate['tax_rate_id']] -= $tax_rate['amount'];
									}
								}
							}
						}

						$discount_total += $discount;
					}

					if ($coupon_info['shipping'] && isset($this->session->data['shipping_method'])) {
						if (!empty($this->session->data['shipping_method']['tax_class_id'])) {
							$tax_rates = $this->tax->getRates($this->session->data['shipping_method']['cost'], $this->session->data['shipping_method']['tax_class_id']);

							foreach ($tax_rates as $tax_rate) {
								if ($tax_rate['type'] == 'P') {
									$taxes[$tax_rate['tax_rate_id']] -= $tax_rate['amount'];
								}
							}
						}

						$discount_total += $this->session->data['shipping_method']['cost'];
					}

					// If discount greater than total
					if ($discount_total > $total) {
						$discount_total = $total;
					}

					if ($discount_total > 0) {
						$total_data[] = array(
							'code'       => 'coupon',
							'title'      => sprintf($this->language->get('text_coupon'), $this->session->data['coupon']),
							'value'      => -$discount_total,
							'sort_order' => $this->config->get('coupon_sort_order')
						);

						$total -= $discount_total;
					} else {
						unset($this->session->data['coupon']);
					}
				}
			} else {
				unset($this->session->data['coupon']);
			}
		}
	}
    	public function getTotalV2(&$total_data, &$total, &$taxes,$seller_id) {
			$allProducts = $this->cart->getProducts();
						
			$language_id = 1;
			$seller =[];
			$total = 0;
			
			foreach ($allProducts as $key1 => $value1) {
				$seller[$value1['seller_id']]  = $value1['seller_id'];
				$total += $value1['total'];
			}
			
			$this->session->data['product_multi_sellers'] = implode(',', $seller);
			$seller_id = implode(',', $seller);
			
		if (isset($this->session->data['coupon'])) {
			$this->load->language('total/coupon');

			$coupon_info = $this->getCoupon($this->session->data['coupon']);

			if ($coupon_info) {
				
				if($coupon_info['coupon_id'] == 30){
					$discount_total = 0;
					
					$deliveryChargesArr = $this->db->query("SELECT oc_sellers.delivery_charges FROM oc_sellers  WHERE oc_sellers.seller_id in ( ".$this->session->data['product_multi_sellers']." )")->row;
						
					$discount_total = $deliveryChargesArr['delivery_charges'];
					
					$total_data[] = array(
						'code'       => 'coupon',
						'title'      => sprintf($this->language->get('text_coupon'), $this->session->data['coupon']),
						'value'      => -$discount_total,
						'sort_order' => $this->config->get('coupon_sort_order')
					);
					
					$total -= $discount_total;
				} else {
					
					$discount_total = 0;
					
					if (!empty($coupon_info['product'])	) {
						$sub_total = $this->cart->getSellerProductsv2($seller_id);
					} else {
						$sub_total = 0;
						
						foreach ($this->cart->getSellerProducts($seller_id) as $product) {
							if (in_array($product['product_id'], $coupon_info['product'])) {
								$sub_total += $product['total'];
							}
						}
					}
					
					
					if ($coupon_info['type'] == 'F') {
						$coupon_info['discount'] = min($coupon_info['discount'], $total);
					}
					$coupon_seller = explode(',',$coupon_info['multi_sellers']);
					$totals = 0;
					$f_seller= [];						
					foreach ($this->cart->getSellerProducts($seller_id) as $product) {
						
						$discount = 0;	
						if(in_array($product['seller_id'], $coupon_seller)){
							$totals += $product['total']; 
							if (!$coupon_info['product']) {
								$status = true;
							} else {
								if (in_array($product['product_id'], $coupon_info['product'])) {
									$status = true;
								} else {
									$status = false;
								}
							}
							
							if ($status) {
								if($coupon_info['type'] == 'F') {
									if(!in_array($product['seller_id'], $f_seller)){
										$f_seller[] = $product['seller_id'];
										$discount = $coupon_info['discount'];
									}
								} elseif ($coupon_info['type'] == 'P') {
									$discount = $product['total'] / 100 * $coupon_info['discount'];
								}

								if ($product['tax_class_id']) {
									$tax_rates = $this->tax->getRates($product['total'] - ($product['total'] - $discount), $product['tax_class_id']);

									foreach ($tax_rates as $tax_rate) {
										if ($tax_rate['type'] == 'P') {
											$taxes[$tax_rate['tax_rate_id']] -= $tax_rate['amount'];
										}
									}
								}
							}

							$discount_total += $discount;
						}
					}
					
					
					if ($coupon_info['shipping'] && isset($this->session->data['shipping_method'])) {
						if (!empty($this->session->data['shipping_method']['tax_class_id'])) {
							$tax_rates = $this->tax->getRates($this->session->data['shipping_method']['cost'], $this->session->data['shipping_method']['tax_class_id']);

							foreach ($tax_rates as $tax_rate) {
								if ($tax_rate['type'] == 'P') {
									$taxes[$tax_rate['tax_rate_id']] -= $tax_rate['amount'];
								}
							}
						}

						$discount_total += $this->session->data['shipping_method']['cost'];
					}

					// If discount greater than total
					if ($discount_total > $total) {
						$discount_total = $total;
					}
					
					if ($discount_total > 0) {
						$total_data[] = array(
							'code'       => 'coupon',
							'title'      => sprintf($this->language->get('text_coupon'), $this->session->data['coupon']),
							'value'      => -$discount_total,
							'sort_order' => $this->config->get('coupon_sort_order')
						);
						$test[] = array(
							'code'       => 'coupon',
							'title'      => sprintf($this->language->get('text_coupon'), $this->session->data['coupon']),
							'value'      => -$discount_total,
							'sort_order' => $this->config->get('coupon_sort_order')
						);

						$total -= $discount_total;
					} else {
						unset($this->session->data['coupon']);
					}
				
				}
			} else {
				unset($this->session->data['coupon']);
			}
		}
	}

	public function confirm($order_info, $order_total) {
		
		if((isset($order_total['value'])) && ($order_total['value'] > 0) && ($order_total['value'] != '')){
			
			$code = '';

			$start = strpos($order_total['title'], '(') + 1;
			$end = strrpos($order_total['title'], ')');

			if ($start && $end) {
				$code = substr($order_total['title'], $start, $end - $start);
			}

			if ($code) {
				$coupon_info = $this->getCoupon($code);

				if ($coupon_info) {
					$this->db->query("INSERT INTO `" . DB_PREFIX . "coupon_history` SET coupon_id = '" . (int)$coupon_info['coupon_id'] . "', order_id = '" . (int)$order_info['order_id'] . "', customer_id = '" . (int)$order_info['customer_id'] . "', amount = '" . (float)$order_total['value'] . "', date_added = NOW()");
				} else {
					return $this->config->get('config_fraud_status_id');
				}
			}
		}
	}
	
		public function getMobileCoupon($code,$customer_id,$language_id,$deviceId) {
		$status = true;

		$coupon_query = $this->db->query("SELECT * FROM `" . DB_PREFIX . "coupon` WHERE code = '" . $this->db->escape($code) . "' AND ((date_start = '0000-00-00' OR date_start < NOW()) AND (date_end = '0000-00-00' OR date_end > NOW())) AND status = '1'");

		if ($coupon_query->num_rows) {
		  
			if ($coupon_query->row['total'] > $this->cart->getMobileSubTotal($customer_id,$language_id,$deviceId)) {
				$status = false;
			}

			$coupon_history_query = $this->db->query("SELECT COUNT(*) AS total FROM `" . DB_PREFIX . "coupon_history` ch WHERE ch.coupon_id = '" . (int)$coupon_query->row['coupon_id'] . "'");

			if ($coupon_query->row['uses_total'] > 0 && ($coupon_history_query->row['total'] >= $coupon_query->row['uses_total'])) {
				$status = false;
			}

			if ($coupon_query->row['logged'] && !$customer_id) {
				$status = false;
			}

			if ($customer_id) {
				$coupon_history_query = $this->db->query("SELECT COUNT(*) AS total FROM `" . DB_PREFIX . "coupon_history` ch WHERE ch.coupon_id = '" . (int)$coupon_query->row['coupon_id'] . "' AND ch.customer_id = '" . (int)$customer_id . "'");

				if ($coupon_query->row['uses_customer'] > 0 && ($coupon_history_query->row['total'] >= $coupon_query->row['uses_customer'])) {
					$status = false;
				}
			}

			// Products
			$coupon_product_data = array();

			$coupon_product_query = $this->db->query("SELECT * FROM `" . DB_PREFIX . "coupon_product` WHERE coupon_id = '" . (int)$coupon_query->row['coupon_id'] . "'");

			foreach ($coupon_product_query->rows as $product) {
				$coupon_product_data[] = $product['product_id'];
			}

			// Categories
			$coupon_category_data = array();

			$coupon_category_query = $this->db->query("SELECT * FROM `" . DB_PREFIX . "coupon_category` cc LEFT JOIN `" . DB_PREFIX . "category_path` cp ON (cc.category_id = cp.path_id) WHERE cc.coupon_id = '" . (int)$coupon_query->row['coupon_id'] . "'");

			foreach ($coupon_category_query->rows as $category) {
				$coupon_category_data[] = $category['category_id'];
			}

			$product_data = array();

			if ($coupon_product_data || $coupon_category_data) {
				foreach ($this->cart->getMobileProductstest($customer_id,$language_id,$deviceId) as $product) {
					if (in_array($product['product_id'], $coupon_product_data)) {
						$product_data[] = $product['product_id'];

						continue;
					}

					foreach ($coupon_category_data as $category_id) {
						$coupon_category_query = $this->db->query("SELECT COUNT(*) AS total FROM `" . DB_PREFIX . "product_to_category` WHERE `product_id` = '" . (int)$product['product_id'] . "' AND category_id = '" . (int)$category_id . "'");

						if ($coupon_category_query->row['total']) {
							$product_data[] = $product['product_id'];

							continue;
						}
					}
				}

				if (!$product_data) {
					$status = false;
				}
			}
		} else {
			$status = false;
		}

		if ($status) {
			return array(
				'coupon_id'     => $coupon_query->row['coupon_id'],
				'code'          => $coupon_query->row['code'],
				'name'          => $coupon_query->row['name'],
				'type'          => $coupon_query->row['type'],
				'discount'      => $coupon_query->row['discount'],
				'shipping'      => $coupon_query->row['shipping'],
				'total'         => $coupon_query->row['total'],
				'product'       => $product_data,
				'date_start'    => $coupon_query->row['date_start'],
				'date_end'      => $coupon_query->row['date_end'],
				'uses_total'    => $coupon_query->row['uses_total'],
				'uses_customer' => $coupon_query->row['uses_customer'],
				'status'        => $coupon_query->row['status'],
				'date_added'    => $coupon_query->row['date_added']
			);
		}
	}
	
	public function getMobileTotal($coupon,$customer_id,$language_id,$deviceId,$seller_id) {
	
		$this->load->language('total/coupon');
		$total_data = array();
		$total = 0;
		$coupon_info = $this->getMobileCoupon($coupon,$customer_id,$language_id,$deviceId);

		if ($coupon_info) {
			
			if($coupon_info['coupon_id'] == 30){
				$discount_total = 0;
				
				$deliveryChargesArr = $this->db->query("SELECT oc_sellers.delivery_charges FROM oc_sellers  WHERE oc_sellers.seller_id in ( ".$seller_id." )")->row;
				
				$discount_total = $deliveryChargesArr['delivery_charges'];
				
				$total_data = array(
					'code'       => 'coupon',
					'title'      => sprintf($this->language->get('text_coupon'), $coupon),
					'value'      => $discount_total,
					'sort_order' => $this->config->get('coupon_sort_order')
				);
				
				$total -= $discount_total;
			} else {
				
				$discount_total = 0;

				if (!$coupon_info['product']) {
						
					$sub_total = $this->cart->getMobileSubTotal($customer_id,$language_id,$deviceId);
					
				} else {				  
					$sub_total = 0;

					foreach ($this->cart->getMobileProductstest($customer_id,$language_id,$deviceId) as $product) {
						if (in_array($product['product_id'], $coupon_info['product'])) {
							$sub_total += $product['total'];
						}
					}
				}

				if ($coupon_info['type'] == 'F') {
					$coupon_info['discount'] = min($coupon_info['discount'], $sub_total);
				}

				foreach ($this->cart->getMobileProductstest($customer_id,$language_id,$deviceId) as $product) {
					$discount = 0;

					if (!$coupon_info['product']) {
						$status = true;
					} else {
						if (in_array($product['product_id'], $coupon_info['product'])) {
							$status = true;
						} else {
							$status = false;
						}
					}

					if ($status) {
					   
						if ($coupon_info['type'] == 'F') {
							$discount = $coupon_info['discount'] * ($product['total'] / $sub_total);
						} elseif ($coupon_info['type'] == 'P') {
							$discount = $product['total'] / 100 * $coupon_info['discount'];
						}

						if ($product['tax_class_id']) {
							$tax_rates = $this->tax->getRates($product['total'] - ($product['total'] - $discount), $product['tax_class_id']);

							foreach ($tax_rates as $tax_rate) {
								if ($tax_rate['type'] == 'P') {
									$taxes[$tax_rate['tax_rate_id']] -= $tax_rate['amount'];
								}
							}
						}
					}

					$discount_total += $discount;
				}

				if ($coupon_info['shipping'] && isset($this->session->data['shipping_method'])) {
					if (!empty($this->session->data['shipping_method']['tax_class_id'])) {
						$tax_rates = $this->tax->getRates($this->session->data['shipping_method']['cost'], $this->session->data['shipping_method']['tax_class_id']);

						foreach ($tax_rates as $tax_rate) {
							if ($tax_rate['type'] == 'P') {
								$taxes[$tax_rate['tax_rate_id']] -= $tax_rate['amount'];
							}
						}
					}

					$discount_total += $this->session->data['shipping_method']['cost'];
				}
				  $total = $sub_total;
				 
				// If discount greater than total
				if ($discount_total > $total) {
					$discount_total = $total;
				}

				if ($discount_total > 0) {
					
					$total_data = array(
						'code'       => 'coupon',
						'title'      => sprintf($this->language->get('text_coupon'), $coupon),
						'value'      => $discount_total,
						'sort_order' => $this->config->get('coupon_sort_order')
					);
					
					$total -= $discount_total;
				} 
			}
		}
            
		//$data = array($total_data,$total);
		$data = array($total_data);
		
		return $data;
	}
	
	public function getMobileTotalNew($coupon,$customer_id,$language_id,$deviceId,$seller_id) {
	
		$this->load->language('total/coupon');
		$total_data = array();
		$total = 0;
		$coupon_info = $this->getMobileCoupon($coupon,$customer_id,$language_id,$deviceId);

		if ($coupon_info) {
			
			if($coupon_info['coupon_id'] == 30){
				$discount_total = 0;
				
				$deliveryChargesArr = $this->db->query("SELECT oc_sellers.delivery_charges FROM oc_sellers  WHERE oc_sellers.seller_id in ( ".$seller_id." )")->row;
				
				$discount_total = $deliveryChargesArr['delivery_charges'];
				
				$total_data = array(
					'code'       => 'coupon',
					'title'      => sprintf($this->language->get('text_coupon'), $coupon),
					'value'      => (float)number_format($discount_total,2),
					'sort_order' => $this->config->get('coupon_sort_order')
				);
				
				$total -= $discount_total;
			} else {
				
				$discount_total = 0;

				if (!$coupon_info['product']) {
						
					$sub_total = $this->cart->getMobileSubTotal($customer_id,$language_id,$deviceId);
					
				} else {				  
					$sub_total = 0;

					foreach ($this->cart->getMobileProductstest($customer_id,$language_id,$deviceId) as $product) {
						if($seller_id == $product['seller_id']){
							if (in_array($product['product_id'], $coupon_info['product'])) {
								$sub_total += $product['total'];
							}
						}
					}
				}

				if ($coupon_info['type'] == 'F') {
					$coupon_info['discount'] = min($coupon_info['discount'], $sub_total);
				}

				foreach ($this->cart->getMobileProductstest($customer_id,$language_id,$deviceId) as $product) {
					
					if($seller_id == $product['seller_id']){
						$discount = 0;

						if (!$coupon_info['product']) {
							$status = true;
						} else {
							if (in_array($product['product_id'], $coupon_info['product'])) {
								$status = true;
							} else {
								$status = false;
							}
						}

						if ($status) {
						   
							if ($coupon_info['type'] == 'F') {
								$discount = $coupon_info['discount'] * ($product['total'] / $sub_total);
							} elseif ($coupon_info['type'] == 'P') {
								$discount = $product['total'] / 100 * $coupon_info['discount'];
							}

							if ($product['tax_class_id']) {
								$tax_rates = $this->tax->getRates($product['total'] - ($product['total'] - $discount), $product['tax_class_id']);

								foreach ($tax_rates as $tax_rate) {
									if ($tax_rate['type'] == 'P') {
										$taxes[$tax_rate['tax_rate_id']] -= $tax_rate['amount'];
									}
								}
							}
						}

						$discount_total += $discount;
					}
				}

				if ($coupon_info['shipping'] && isset($this->session->data['shipping_method'])) {
					if (!empty($this->session->data['shipping_method']['tax_class_id'])) {
						$tax_rates = $this->tax->getRates($this->session->data['shipping_method']['cost'], $this->session->data['shipping_method']['tax_class_id']);

						foreach ($tax_rates as $tax_rate) {
							if ($tax_rate['type'] == 'P') {
								$taxes[$tax_rate['tax_rate_id']] -= $tax_rate['amount'];
							}
						}
					}

					$discount_total += $this->session->data['shipping_method']['cost'];
				}
				  $total = $sub_total;
				 
				// If discount greater than total
				if ($discount_total > $total) {
					$discount_total = $total;
				}

				if ($discount_total > 0) {
					
					$total_data = array(
						'code'       => 'coupon',
						'title'      => sprintf($this->language->get('text_coupon'), $coupon),
						'value'      => (float)number_format($discount_total,2),
						'sort_order' => $this->config->get('coupon_sort_order')
					);
					
					$total -= $discount_total;
				} 
			}
			
			foreach ($this->cart->getMobileProductstest($customer_id,$language_id,$deviceId) as $product) {
					
				if($seller_id == $product['seller_id']){					
					$cart_id = $product['cart_id'];
					
					$this->db->query("UPDATE " . DB_PREFIX . "cart SET coupon_code = '".$coupon."' WHERE cart_id = '" . (int)$cart_id . "'");
				}
			}
		}
            
		//$data = array($total_data,$total);
		$data = array($total_data);
		
		return $data;
	}
			
	public function couponHistory($couponId,$orderId,$customerId,$discount) {
		
		if((isset($discount)) && ($discount > 0) && ($discount != '')){
	
			if ($couponId) {
				$this->db->query("INSERT INTO `" . DB_PREFIX . "coupon_history` SET coupon_id = '" . (int)$couponId . "', order_id = '" . (int)$orderId . "', customer_id = '" . (int)$customerId . "', amount = '-" . (float)$discount . "', date_added = NOW()");
			} else {
				return $this->config->get('config_fraud_status_id');
			}
		}
		
	}

	public function unconfirm($order_id) {
		$this->db->query("DELETE FROM `" . DB_PREFIX . "coupon_history` WHERE order_id = '" . (int)$order_id . "'");
	}
}
