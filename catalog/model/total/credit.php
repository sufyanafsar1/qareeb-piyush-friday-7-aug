<?php
class ModelTotalCredit extends Model {
	public function getTotal(&$total_data, &$total, &$taxes) {
				
		if ($this->config->get('credit_status')) {
			$this->load->language('total/credit');
			
			foreach ($this->cart->getProducts() as $product) {
				
				$sellerIdArr[] = $product['seller_id'];
				
			}

			$sellerIdArr = array_unique( $sellerIdArr );
			$sellerIdStr =  implode(',', $sellerIdArr);
             
            $deliveryChargesArr = $this->db->query(" select oc_sellers.delivery_charges, oc_sellers.seller_id, oc_sellers.firstname, oc_sellers.lastname from oc_sellers where oc_sellers.seller_id in ( $sellerIdStr )")->rows;
									
			$deliveryCharges = 0;
			foreach( $deliveryChargesArr as $deliveryChargesRow ){
				$deliveryCharges += $deliveryChargesRow['delivery_charges'];				
			}
			
			// $total = $total + $deliveryCharges;
			
			$balance = $this->customer->getBalance();

			if ((float)$balance) {
				if ($balance > $total) {
					$credit = $total;
				} else {
					$credit = $balance;
				}

				//if ($credit > 0) {
					$total_data[] = array(
						'code'       => 'credit',
						'title'      => $this->language->get('text_credit'),
						'value'      => -$credit,
						'sort_order' => $this->config->get('credit_sort_order')
					);

					$total -= $credit;
				//}
			}
		}
	}
	
	public function getTotalNew(&$total_data, &$total, &$taxes, &$order_id) {

		if ($this->config->get('credit_status')) {
			$this->load->language('total/credit');
			
			foreach ($this->cart->getProducts() as $product) {
				
				$sellerIdArr[] = $product['seller_id'];
				
			}

			$sellerIdArr = array_unique( $sellerIdArr );
			$sellerIdStr =  implode(',', $sellerIdArr);
             
            $deliveryChargesArr = $this->db->query(" select oc_sellers.delivery_charges, oc_sellers.seller_id, oc_sellers.firstname, oc_sellers.lastname from oc_sellers where oc_sellers.seller_id in ( $sellerIdStr )")->rows;
									
			$deliveryCharges = 0;
			foreach( $deliveryChargesArr as $deliveryChargesRow ){
				$deliveryCharges += $deliveryChargesRow['delivery_charges'];				
			}
			
			$total = $total + $deliveryCharges;
			
			$deliveryCredit = $this->db->query("SELECT amount FROM oc_customer_transaction WHERE order_id = '".$order_id."'")->row;
			
			$balance = $deliveryCredit['amount'];
						
			if ($balance > 0) {
				
				$balance = abs($deliveryCredit['amount']);
				
				if ($balance > $total) {
					$credit = $total;
				} else {
					$credit = $balance;
				}

				$total_data[] = array(
					'code'       => 'credit',
					'title'      => $this->language->get('text_credit'),
					'value'      => $credit,
					'sort_order' => $this->config->get('credit_sort_order')
				);

				$total += $credit;					
				
			} else if ($balance < 0) {
				
				$balance = abs($deliveryCredit['amount']);
				
				if ($balance > $total) {
					$credit = $total;
				} else {
					$credit = $balance;
				}
				
				$total_data[] = array(
					'code'       => 'credit',
					'title'      => $this->language->get('text_credit'),
					'value'      => -$credit,
					'sort_order' => $this->config->get('credit_sort_order')
				);

				$total -= $credit;
			}
		}
	}

	public function confirm($order_info, $order_total) {
		
		$this->load->language('total/credit');

		if ($order_info['customer_id']) {
			$this->db->query("INSERT INTO " . DB_PREFIX . "customer_transaction SET customer_id = '" . (int)$order_info['customer_id'] . "', order_id = '" . (int)$order_info['order_id'] . "', description = '" . $this->db->escape(sprintf($this->language->get('text_order_id'), (int)$order_info['order_id'])) . "', amount = '" . (float)$order_total['value'] . "', date_added = NOW()");
		}
	}

	public function unconfirm($order_id) {
		$this->db->query("DELETE FROM " . DB_PREFIX . "customer_transaction WHERE order_id = '" . (int)$order_id . "'");
	}
}