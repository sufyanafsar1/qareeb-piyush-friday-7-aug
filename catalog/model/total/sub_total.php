<?php
class ModelTotalSubTotal extends Model
{
	public function getTotal(&$total_data, &$total, &$taxes)
	{
		$this->load->language('total/sub_total');

		$sub_total = $this->cart->getSubTotal();

		if (isset($this->session->data['vouchers']) && $this->session->data['vouchers']) {
			foreach ($this->session->data['vouchers'] as $voucher) {
				$sub_total += $voucher['amount'];
			}
		}

		$total_data[] = array(
			'code'       => 'sub_total',
			'title'      => $this->language->get('text_sub_total'),
			'value'      => $sub_total,
			'sort_order' => $this->config->get('sub_total_sort_order')
		);

		$total += $sub_total;
	}
	public function getTotalV2(&$total_data, &$total, &$taxes, $seller_id)
	{
		$this->load->language('total/sub_total');
		$sub_total = $this->cart->getSellerSubTotal($seller_id);
		// if ($this->customer->getId() == 2014) {
		// 	echo '<pre>';
		// 	print_r($sub_total);
		// 	echo '</pre>';
		// 	exit;
		// }

		if (isset($this->session->data['vouchers']) && $this->session->data['vouchers']) {
			foreach ($this->session->data['vouchers'] as $voucher) {
				$sub_total += $voucher['amount'];
			}
		}

		$total_data[] = array(
			'code'       => 'sub_total',
			'title'      => $this->language->get('text_sub_total'),
			'value'      => $sub_total,
			'sort_order' => $this->config->get('sub_total_sort_order')
		);

		$total += $sub_total;
	}
}
