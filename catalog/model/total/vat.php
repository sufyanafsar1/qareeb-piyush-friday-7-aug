<?php
class ModelTotalVat extends Model
{
    public function getTotal(&$total_data, $vat)
    {
        $this->load->language('total/vat');

        $sub_total = $this->cart->getSubTotal();
		$vat = 15;
        //$vat_total = ($vat / 100) * $sub_total;
        $vat_total = $sub_total * $vat / ($vat+100);

        $new_sub_total = $sub_total - $vat_total;

        $current_total_data = $total_data;
        $new_total_data = array();
$sTotal = false;
        foreach ($current_total_data as $key => $val){

            if($val['code'] == 'sub_total'){
                $sTotal = true;
                $new_total_data[$key] = $val;
                //$new_total_data[$key]['value'] = round($new_sub_total);

                $new_total_data[$key + 1] = array(
                    'code' => 'vat',
                    'title' => $this->language->get('text_vat').'('.$vat.' %)',
                    'value' => round($vat_total,2),
                    'sort_order' => 1
                );

            }else{
                if($sTotal){
                    $new_total_data[$key + 1]  = $val;
                }else{
                    $new_total_data[$key] = $val;
                }
            }



        }
        $total_data = $new_total_data;
    }
	public function getTotalvalue($total_data = 0, $vat){
		return $vat_total = $total_data * $vat / ($vat+100);
	}
	
}