function getURLVar(key) {
	var value = [];

	var query = String(document.location).split('?');

	if (query[1]) {
		var part = query[1].split('&');

		for (i = 0; i < part.length; i++) {
			var data = part[i].split('=');

			if (data[0] && data[1]) {
				value[data[0]] = data[1];
			}
		}

		if (value[key]) {
			return value[key];
		} else {
			return '';
		}
	}
}
function loacation() {
    location.reload();
}

function showLoginPopup()
{
      $('#modal-quicksignIn').appendTo("body");
      $('#modal-quicksignIn').modal('show');
}
function changeQty(id,increase,seller_id) {
    
    var qty = parseInt($('#' + id+'').val());

    if(parseInt(qty) <= 1 )
    {
       qty = increase ? qty+1 : 0; 
        $('#' + id+'').val(qty);
        
     }
    else if ( !isNaN(qty) ) {
        qty = increase ? qty+1 : (qty-1 > 1 ? qty-1:1);
        $('#' + id+'').val(qty);
    }else{
  $('#' + id+'').val(0);
 }

var arr = id.split('select-number');

 update_cart_data_home(arr[1], id,seller_id);
}

function update_cart_data_home(cart_id, id,seller_id) {
	var quantity = $('#'+ id+'').val();
	if (quantity >= 0) {
		$.ajax({
			url: 'index.php?route=checkout/cart/update_by_home',
			type: 'post',
			data: {'cart_id' : cart_id, 'quantity': quantity},
			dataType: 'json',

  success: function(json) {				

		if (json['success']) {
     	addProductNotices('Update Success','abc','Shopping Cart Updated Successfully','success');
     $('.b-cart-total').html(json['total']);
    
     $('.b-cart > ul').load('index.php?route=common/cart/info ul li');
     
   
    }
			}

		});
	}
    
    
    if(quantity==0)
     {
        
        var parentId = $('#'+ id+'').parent().parent().parent().attr('id');
        var parentIdArray =   parentId.split("_");       
        var cartBtn = '<a href="javascript:void(0)" id="custom" onclick="btadd.cart(\''+parentIdArray[3]+'\', \'1\', '+seller_id+');"><button>Add to Cart</button></a>' ;
        $("#"+parentId).html(cartBtn);
     }
    
}
function addProductNotices(title, thumb, text, type) {
	$.jGrowl.defaults.closer = true;
	var tpl = thumb + '<h3>'+text+'</h3>';
	$.jGrowl(tpl, {		
		life: 3000,
		header: title,
		speed: 'slow'
	});
}
function changeCartQty(id,increase) {
      
    var qty = parseInt($('#' + id+'').val());

    if(parseInt(qty) <= 1 )
    {
       qty = increase ? qty+1 : 0; 
        $('#' + id+'').val(qty);
    }
    else if ( !isNaN(qty) ) 
    {
        qty = increase ? qty+1 : (qty-1 > 1 ? qty-1:1);
        $('#' + id+'').val(qty);
    }else
    {
        $('#' + id+'').val(0);
     }
     var arr = id.split('item-number');
     updateCartQty(arr[1], id);
     
 }

function updateCartQty(cart_id, id) {
   
	var quantity = $('#'+ id+'').val();
    
	if (quantity >= 0) {
		$.ajax({
			url: 'index.php?route=checkout/cart/update_by_home',
			type: 'post',
			data: {'cart_id' : cart_id, 'quantity': quantity},
			dataType: 'json',

  success: function(json) {				

		if (json['success']) {
     	addProductQtyNotices('Update Success','abc','Shopping Cart Updated Successfully','success');
     $('.b-cart-total').html(json['total']);
    
     $('.b-cart > ul').load('index.php?route=common/cart/info ul li');
     
    }
	}
   });
	}
    
}
function addProductQtyNotices(title, thumb, text, type) {
	$.jGrowl.defaults.closer = true;
	var tpl = title + '<h3>'+text+'</h3>';
	$.jGrowl(tpl, {		
		life: 3000,
		header: title,
		speed: 'slow'
	});
}
function resendPassword()
{
   var password     = $("#input-passwordSent").val();
   var mobile       = $("#input-telephone").val();
   $('#modal-quicksignIn .modal-body').html("Please wait.....");
    $.ajax({
		url: 'index.php?route=account/simpleregister/resendPassword',
		type: 'post',
			data: { 'mobile': mobile, 'userPassword': password },
		dataType: 'json',
		beforeSend: function() {
			$('#quick-register #resendPassword').button('loading');
            $('#quick-register #verifyPassword').hide(); 
			$('#modal-quicksignIn .alert-danger').remove();
		},
		complete: function() {
			$('#quick-register #resendPassword').button('reset');
		},
		success: function(json) {
			$('#modal-quicksignup .form-group').removeClass('has-error');
						
           
			if (json['error_warning']) {
				$('#modal-quicksignIn .modal-header').after('<div class="alert alert-danger" style="margin:5px;"><i class="fa fa-exclamation-circle"></i> ' + json['error_warning'] + '</div>');
			}
            
			if (json['success']) {
			 var password = json["password_sent"];
             var mobile = json["mobile_registered"];
				$('#modal-quicksignIn .main-heading').html(json['heading_title']);
				success = json['text_resend_password'];
				success += '<div>';
                   success += '<div class="form-group required">';
                   success += '<div>';
                   success += '<input type="hidden" name="telephone" value="'+mobile+'"  id="input-telephone" class="form-control" />';
                   success += '<input type="hidden" name="passwordSent" value="'+password+'"  id="input-passwordSent" class="form-control" />';
                   success += '</div>';
                   success += '</div>';   
                   success += '<div class="form-group required">';
                   success += '<div>';
                   success += '<input type="text" name="password" value="" placeholder="'+ json["text_verify_password"] +'"  id="input-password" class="form-control" />';
                   success += '</div>';
                   success += '</div>';          
                   success += '</div>';
                   success += '<div class="buttons"><div class="text-right"><a onclick="resendPassword()" id="resendPassword" class="btn btn-primary">'+ json['button_resend'] +'</a> <a id="verifyPassword" onclick="verifyPassword()" class="btn btn-primary">'+ json['button_verify'] +'</a></div></div>';
				$('#modal-quicksignIn .modal-body').html(success);
			}
            else{
                alert("Something went wrong. Please click resend button");
            }
		}
	});
}

function verifyPassword()
{
    var userPassword = $("#input-password").val();
    var sentPassword = $("#input-passwordSent").val();
    var mobile       = $("#input-telephone").val();
   
    if(userPassword == sentPassword)
    {
         $('#modal-quicksignIn .modal-body').html("Please wait verification in progress.....");
      $.ajax({
		url: 'index.php?route=account/simpleregister/register',
		type: 'post',
		data: { 'mobile': mobile, 'password': userPassword },
		dataType: 'json',
		beforeSend: function() {
			$('#quick-register #verifyPassword').button('loading');
            $('#quick-register #resendPassword').hide();
			$('#modal-quicksignIn .alert-danger').remove();
		},
		complete: function() {
			$('#quick-register #verifyPassword').button('reset');
		},
		success: function(json) {
			$('#modal-quicksignup .form-group').removeClass('has-error');
			
			if(json['islogged']){
				 window.location.href="index.php?route=account/account";
			}	
           
			if (json['error_warning']) {
				$('#modal-quicksignIn .modal-header').after('<div class="alert alert-danger" style="margin:5px;"><i class="fa fa-exclamation-circle"></i> ' + json['error_warning'] + '</div>');
			}
            
			if (json['success']) {
				$('#modal-quicksignIn .main-heading').html(json['heading_title']);
				success = json['text_verification_message'];
				success += '<div class="buttons"><div class="text-right"><a onclick="loacation();" class="btn btn-primary">'+ json['button_continue'] +'</a></div></div>';
				$('#modal-quicksignIn .modal-body').html(success);
			}
		}
	});
    }
    else{
        alert("Password doesn't match.");
    }
}

$(document).ready(function() {
    
    // Hide SHow Cart Toggle
     $('.btn-dropdown-cart').click(function(event){
        event.stopPropagation();
         $("#cartDropDown").slideToggle("fast");
    });
    
    $("#cartDropDown").on("click", function (event) {
        event.stopPropagation();
    });
    $(document).on("click",'body *', function () {        
    $("#cartDropDown").hide();
    });
  
    $(".bt-item,.product").hover(function() { 
       var divId = this.id;  
      
         $("#heart"+divId).css("display","block");
    });
        
    $(".bt-item,.product").mouseleave(function() { 
        
       var divId = this.id; 
       var clsName =  $("#heart"+divId).attr('class');
         if(clsName =="heart-box-pink")
         {
             $("#heart"+divId).css("display","block");
         }
         else{
             $("#heart"+divId).css("display","none");
         }
        
        });
        
        $(document).delegate('#signinTab,#signupTab', 'click', function(e) {
            $('#modal-quicksignIn .form-control').removeClass('has-error');
                $('#modal-quicksignIn .alert-danger').remove();
            });
    
    $(document).delegate('#signIn', 'click', function(e) {
        
                $('#quick-register')[0].reset();
                $('#quick-signin')[0].reset();
                getLocation();
                $('#modal-quicksignIn').modal('show');
                $('#modal-quicksignIn .form-control').removeClass('has-error');
                $('#modal-quicksignIn .alert-danger').remove();
                
            });
           
          
            
       $('#btnLogin').click(function () {

       
        var email = $("#email").val();
        var password = $("#password").val();
      
        $.ajax({
            type: 'POST',
            dataType: 'json',
            data: { 'email': email, 'password': password },
            url: 'index.php?route=account/login/login',
            beforeSend: function () {
                $('#modal-quicksignIn #btnLogin').button('loading');

            },
            complete: function () {

                $('#modal-quicksignIn #btnLogin').button('reset');
            },
            success: function (json) {
               $('#modal-quicksignIn .form-control').removeClass('has-error');
                if (json['islogged']) {
                    window.location.href = "index.php?route=account/account";
                }
                if (json['error_email']) {
                    $(".alert-danger").remove();
                    $('#modal-quicksignIn .modal-header').after('<div class="alert alert-danger" style="margin:5px;"><i class="fa fa-exclamation-circle"></i> ' + json['error_email'] + '</div>');
                    $('#quick-login #email').addClass('has-error');                  
                    $('#quick-login #email').focus();
                }
                else if (json['error_password']) {
                    $(".alert-danger").remove();
                    $('#modal-quicksignIn .modal-header').after('<div class="alert alert-danger" style="margin:5px;"><i class="fa fa-exclamation-circle"></i> ' + json['error_password'] + '</div>');
                    $('#quick-login #password').addClass('has-error');                  
                    $('#quick-login #password').focus();
                }
                else if (json['error']) {
                    $(".alert-danger").remove();
                    $('#modal-quicksignIn .modal-header').after('<div class="alert alert-danger" style="margin:5px;"><i class="fa fa-exclamation-circle"></i> ' + json['error'] + '</div>');
                    $('#quick-login #email').addClass('has-error');                   
                    $('#quick-login #email').focus();
                }
                if (json['success']) {
                    loacation();
                    $('#modal-quicksignIn').modal('hide');
                }
            }
        });
    });
	
$('#btnSignup').click(function() {

    //Html On Success;
   
   
	$.ajax({
		url: 'index.php?route=account/simpleregister/registerTempCustomer',
		type: 'post',
		data: $('#quick-register input[type=\'text\'],#quick-register input[type=\'tel\'],#quick-register input[type=\'email\'], #quick-register input[type=\'password\'], #quick-register input[type=\'checkbox\']:checked'),
		dataType: 'json',
		beforeSend: function() {
			$('#quick-register #btnSignup').button('loading');
			$('#modal-quicksignIn .alert-danger').remove();
		},
		complete: function() {
			$('#quick-register #btnSignup').button('reset');
		},
		success: function(json) {
			$('#modal-quicksignIn .form-control').removeClass('has-error');
			
			if(json['islogged']){
				 window.location.href="index.php?route=account/account";
			}
			if (json['error_firstname']) {
				$('#quick-register #input-firstname').addClass('has-error');
				$('#quick-register #input-firstname').focus();
                $('#modal-quicksignIn .modal-header').after('<div class="alert alert-danger" style="margin:5px;"><i class="fa fa-exclamation-circle"></i> ' + json['error_firstname'] + '</div>');
                
			}
			
            else if (json['error_lastname']) {
				$('#quick-register #input-lastname').addClass('has-error');
				$('#quick-register #input-lastname').focus();
                $('#modal-quicksignIn .modal-header').after('<div class="alert alert-danger" style="margin:5px;"><i class="fa fa-exclamation-circle"></i> ' + json['error_lastname'] + '</div>');
			}
			else if (json['error_email']) {
				$('#quick-register #input-email').addClass('has-error');
				$('#quick-register #input-email').focus();
                $('#modal-quicksignIn .modal-header').after('<div class="alert alert-danger" style="margin:5px;"><i class="fa fa-exclamation-circle"></i> ' + json['error_email'] + '</div>');
			}
			else if (json['error_telephone']) {
				$('#quick-register #input-telephone').addClass('has-error');
				$('#quick-register #input-telephone').focus();
                $('#modal-quicksignIn .modal-header').after('<div class="alert alert-danger" style="margin:5px;"><i class="fa fa-exclamation-circle"></i> ' + json['error_telephone'] + '</div>');
			}
			else if (json['error_password']) {
				$('#quick-register #input-password').addClass('has-error');
				$('#quick-register #input-password').focus();
                $('#modal-quicksignIn .modal-header').after('<div class="alert alert-danger" style="margin:5px;"><i class="fa fa-exclamation-circle"></i> ' + json['error_password'] + '</div>');
			}
            else if (json['error_confirm']) {
				$('#quick-register #input-confirm').addClass('has-error');
				$('#quick-register #input-confirm').focus();
                $('#modal-quicksignIn .modal-header').after('<div class="alert alert-danger" style="margin:5px;"><i class="fa fa-exclamation-circle"></i> ' + json['error_confirm'] + '</div>');
			}
			else if (json['error_warning']) {
				$('#modal-quicksignIn .modal-header').after('<div class="alert alert-danger" style="margin:5px;"><i class="fa fa-exclamation-circle"></i> ' + json['error_warning'] + '</div>');
			}
            else if (json['error_warning_mobile']) {
				$('#modal-quicksignIn .modal-header').after('<div class="alert alert-danger" style="margin:5px;"><i class="fa fa-exclamation-circle"></i> ' + json['error_warning_mobile'] + '</div>');
			}
            else if (!json['agree']) {
				$('#modal-quicksignIn .modal-header').after('<div class="alert alert-danger" style="margin:5px;"><i class="fa fa-exclamation-circle"></i> ' + json['text_agree'] + '</div>');
			}
			
			if (json['now_login']) {
				$('.quick-login').before('<li class="dropdown"><a href="<?php echo $account; ?>" title="<?php echo $text_account; ?>" class="dropdown-toggle" data-toggle="dropdown"><i class="fa fa-user"></i> <span class="hidden-xs hidden-sm hidden-md"><?php echo $text_account; ?></span> <span class="caret"></span></a><ul class="dropdown-menu dropdown-menu-right"><li><a href="<?php echo $account; ?>"><?php echo $text_account; ?></a></li><li><a href="<?php echo $order; ?>"><?php echo $text_order; ?></a></li><li><a href="<?php echo $transaction; ?>"><?php echo $text_transaction; ?></a></li><li><a href="<?php echo $download; ?>"><?php echo $text_download; ?></a></li><li><a href="<?php echo $logout; ?>"><?php echo $text_logout; ?></a></li></ul></li>');
				
				$('.quick-login').remove();
			}
			if (json['success']) {
			 var password = json["password_sent"];
             var mobile = json["mobile_registered"];
				$('#modal-quicksignIn .main-heading').html(json['heading_title']);
				success = json['text_message'];
				success += '<div>';
                   success += '<div class="form-group required">';
                   success += '<div>';
                   success += '<input type="hidden" name="telephone" value="'+mobile+'"  id="input-telephone" class="form-control" />';
                   success += '<input type="hidden" name="passwordSent" value="'+password+'"  id="input-passwordSent" class="form-control" />';
                   success += '</div>';
                   success += '</div>';   
                   success += '<div class="form-group required">';
                   success += '<div>';
                   success += '<input type="text" name="password" value="" placeholder="'+ json["text_verify_password"] +'"  id="input-password" class="form-control" />';
                   success += '</div>';
                   success += '</div>';          
                   success += '</div>';
                   success += '<div class="buttons"><div class="text-right"><a onclick="resendPassword()" id="resendPassword" class="btn btn-primary">'+ json['button_resend'] +'</a> <a id="verifyPassword" onclick="verifyPassword()" class="btn btn-primary">'+ json['button_verify'] +'</a></div></div>';
				$('#modal-quicksignIn .modal-body').html(success);
			}
		}
	});
});


    
	// Highlight any found errors
	$('.text-danger').each(function() {
		var element = $(this).parent().parent();
		
		if (element.hasClass('form-group')) {
			element.addClass('has-error');
		}
	});
		
	// Currency
	$('#currency .currency-select').on('click', function(e) {
		e.preventDefault();

		$('#currency input[name=\'code\']').attr('value', $(this).attr('name'));

		$('#currency').submit();
	});

	// Language
	$('#language a').on('click', function(e) {
		e.preventDefault();

		$('#language input[name=\'code\']').attr('value', $(this).attr('href'));

		$('#language').submit();
	});

	// Product List
	
	/*	 $('#list-view').click(function() {
	 	   
	 		$('#content .product-layout > .clearfix').remove();

	 		$('#content .product-layout').attr('class', 'product-layout product-list col-xs-12');

	 		localStorage.setItem('display', 'list');
	 	});
	 

	// Product Grid
	$('#grid-view').click(function() {
	    
		$('#content .product-layout > .clearfix').remove();

		// What a shame bootstrap does not take into account dynamically loaded columns
		cols = $('#column-right, #column-left').length;

		if (cols == 2) {
			$('#content .product-layout').attr('class', 'product-layout product-grid col-lg-6 col-md-6 col-sm-12 col-xs-12');
		} else if (cols == 1) {
			$('#content .product-layout').attr('class', 'product-layout product-grid col-lg-4 col-md-4 col-sm-6 col-xs-12');
		} else {
			$('#content .product-layout').attr('class', 'product-layout product-grid col-lg-3 col-md-3 col-sm-6 col-xs-12');
		}

		 localStorage.setItem('display', 'grid');
	});


	 if (localStorage.getItem('display') == 'list') {
	 		$('#list-view').trigger('click');
	 	} else {
	 		$('#grid-view').trigger('click');
	 	}
	 */

	// tooltips on hover
	$('[data-toggle=\'tooltip\']').tooltip({container: 'body'});

	// Makes tooltips work on ajax generated content
	$(document).ajaxStop(function() {
		$('[data-toggle=\'tooltip\']').tooltip({container: 'body'});
	});
});

// Cart add remove functions
var cart = {
	'add': function(product_id, quantity) {

		$.ajax({
			url: 'index.php?route=checkout/cart/add',
			type: 'post',
			data: 'product_id=' + product_id + '&quantity=' + (typeof(quantity) != 'undefined' ? quantity : 1),
			dataType: 'json',
			beforeSend: function() {
				$('#cart > button').button('loading');
			},
			success: function(json) {
				$('.alert, .text-danger').remove();

				$('#cart > button').button('reset');

				if (json['redirect']) {
					location = json['redirect'];
				}

				if (json['success']) {
					$('#content').parent().before('<div class="alert alert-success"><i class="fa fa-check-circle"></i> ' + json['success'] + '<button type="button" class="close" data-dismiss="alert">&times;</button></div>');

					$('#cart-total').html(json['total']);

					$('html, body').animate({ scrollTop: 0 }, 'slow');

					$('#cart > ul').load('index.php?route=common/cart/info ul li');
				}
			}
		});
	},
	'update': function(key, quantity) {
		$.ajax({
			url: 'index.php?route=checkout/cart/edit',
			type: 'post',
			data: 'key=' + key + '&quantity=' + (typeof(quantity) != 'undefined' ? quantity : 1),
			dataType: 'json',
			beforeSend: function() {
				$('#cart > button').button('loading');
			},
			success: function(json) {
				$('#cart > button').button('reset');

				$('#cart-total').html(json['total']);

				if (getURLVar('route') == 'checkout/cart' || getURLVar('route') == 'checkout/checkout') {
					location = 'index.php?route=checkout/cart';
				} else {
					$('#cart > ul').load('index.php?route=common/cart/info ul li');
				}
			}
		});
	},
	'remove': function(key) {	   
 		$.ajax({
 			url: 'index.php?route=checkout/cart/remove',
 			type: 'post',
 			data: 'key=' + key,
 			dataType: 'json',
 			beforeSend: function() {
 				$('.b-cart > button').button('loading');
 			},
 			success: function(json) {
 			    
 				//$('.b-cart > button').button('reset');   
                        
 				$('.b-cart > button').html("<span class='b-cart-total'>" + json['total']+"</span>");

 				if (getURLVar('route') == 'checkout/cart' || getURLVar('route') == 'checkout/checkout') {
 					location = 'index.php?route=checkout/cart';
 				} else {
 				  
 					$('.b-cart > ul').load('index.php?route=common/cart/info ul li');
 				}
 			}
 		});
 
 
 /*	$.ajax({
			url: 'index.php?route=checkout/cart/remove',
			type: 'post',
			data: 'key=' + key,
			dataType: 'json',
			beforeSend: function() {
				$('.cart > button').button('loading');
			},
			success: function(json) {
				$('.cart > button').button('reset');

			//	$('.cart-total').html(json['total']);
                if(json['lang'] =='en')
                    {
                        	$('.b-cart-total').html(json['items']);
                    }
                    else{
                        $('.b-cart-total').html('<img src="image/shopping_bag.png" alt"Shopping Bag"  />');
                    }
                    
                    
                    $('#itemsCount').html(json['total']);
                    if(json['total'] > 0)
                    {
                         $("#itemsCount").attr("class", "cart-full");
                    }
                    else{
                         $("#itemsCount").attr("class", "cart-empty");
                    }

				if (getURLVar('route') == 'checkout/cart' || getURLVar('route') == 'checkout/checkout') {
					location = 'index.php?route=checkout/cart';
				} else {
					$('.cart > ul').load('index.php?route=common/cart/info ul li');
				}
			}
		}); */
	}
}

var voucher = {
	'add': function() {

	},
	'remove': function(key) {
		$.ajax({
			url: 'index.php?route=checkout/cart/remove',
			type: 'post',
			data: 'key=' + key,
			dataType: 'json',
			beforeSend: function() {
				$('#cart > button').button('loading');
			},
			complete: function() {
				$('#cart > button').button('reset');
			},
			success: function(json) {
				$('#cart-total').html(json['total']);

				if (getURLVar('route') == 'checkout/cart' || getURLVar('route') == 'checkout/checkout') {
					location = 'index.php?route=checkout/cart';
				} else {
					$('#cart > ul').load('index.php?route=common/cart/info ul li');
				}
			}
		});
	}
}

var wishlist = {
	'add': function(product_id) {
	   
		$.ajax({
			url: 'index.php?route=account/wishlist/add',
			type: 'post',
			data: 'product_id=' + product_id,
			dataType: 'json',
			success: function(json) {
				$('.alert').remove();

				if (json['success']) {
					$('#content').parent().before('<div class="alert alert-success"><i class="fa fa-check-circle"></i> ' + json['success'] + '<button type="button" class="close" data-dismiss="alert">&times;</button></div>');
				}

				if (json['info']) {
					$('#content').parent().before('<div class="alert alert-info"><i class="fa fa-info-circle"></i> ' + json['info'] + '<button type="button" class="close" data-dismiss="alert">&times;</button></div>');
				}

				$('#wishlist-total').html(json['total']);

				$('html, body').animate({ scrollTop: 0 }, 'slow');
			}
		});
	},
	'remove': function() {
		
	}
}


function wishlist_remove(product_id,seller_id) {
	$.ajax({
		url: 'index.php?route=account/wishlist/wishlist_product_remove',
		type: 'post',
		data: { product_id: product_id, seller_id : seller_id},
		dataType: 'json',
		success: function(json) {
			if (json['success']) {
				addProductNotice(json['title'], json['thumb'], json['success'], 'success');
				$('.wishlist-total').html(json['total']);
				$("#heart"+product_id).attr('class', 'heart-box');
				$("#heart"+product_id+" .heart-label i").css('color', '#000000');
				$("#heart"+product_id).css("display","block");
			}else{
				addProductNotice(json['title'],'', json['info']);
				$('.wishlist-total').html(json['total']);
			}
		}
	});
}

function changeHeart(productId,seller_id)
{
	var product_id_text = "'"+productId+"'";
	if ($("#heart"+productId).attr('class') == 'heart-box-pink') {
		var content = '<div class="heart-label">';
			//content += '<a class="wishlist1" id="wishlist_cat" onclick="btadd.wishlist('+product_id_text+'); changeHeart('+product_id_text+')">';
            content += '<a class="wishlist1" onclick="wishlist_cat_add('+productId+','+seller_id+')">';
	        content += '<i class="fa fa-heart" style="color:#000000"></i>';   
	        content += '</a>';                   
	        content += '</div>';
	    $("#heart"+productId).attr('class', 'heart-box');
	    $("#heart"+productId).html(content);
	}else{
		var content = '<div class="heart-label">';
			content +=	'<a class="wishlist1" onclick="wishlist_remove('+product_id_text+','+seller_id+'); changeHeart('+product_id_text+','+seller_id+')">';
	        content +=	'<i class="fa fa-heart" style="color:#E8114A"></i>';    
	        content += 	'</a>';                    
	        content +=	'</div>';
	    $("#heart"+productId).css("display","block");
	    $("#heart"+productId).attr('class', 'heart-box-pink');
	    $("#heart"+productId).html(content);
	}
}

var compare = {
	'add': function(product_id) {
		$.ajax({
			url: 'index.php?route=product/compare/add',
			type: 'post',
			data: 'product_id=' + product_id,
			dataType: 'json',
			success: function(json) {
				$('.alert').remove();

				if (json['success']) {
					$('#content').parent().before('<div class="alert alert-success"><i class="fa fa-check-circle"></i> ' + json['success'] + '<button type="button" class="close" data-dismiss="alert">&times;</button></div>');

					$('#compare-total').html(json['total']);

					$('html, body').animate({ scrollTop: 0 }, 'slow');
				}
			}
		});
	},
	'remove': function() {

	}
};
var btadd = {
    'cart': function(product_id,quantity,seller_id) {   
    //alert('cxzc')
        $.ajax({
            url: 'index.php?route=bossthemes/boss_add/cart/',
            type: 'post',
           data: 'product_id=' + product_id + '&quantity=' + (typeof(quantity) != 'undefined' ? quantity : 1 )+ '&seller_id=' +seller_id,
            dataType: 'json',
            success: function(json) {
                //if (json['redirect']) {
                //    location = json['redirect'];
                //}

                if (json['success']) {
                    addProductNotices(json['title'], json['thumb'], json['success'] + "<br><br>" + json['msg'], 'success');
                    $('.b-cart-total').html(json['total']);
                
                    $('.b-cart > ul').load('index.php?route=common/cart/info ul li');
                    
                    //Convert Add To cart Into  -, + buttons
                  var  cart_id = json['cartId'];
                  var incDecBtn = "<div style='position:relative'>";
                    incDecBtn += '<a href="javascript:void(0)"><input id="select-number'+cart_id+'" value="'+quantity+'" onchange="update_cart_data_home(\''+cart_id+'\', \'select-number'+cart_id+'\')" disabled/></a>';
                    incDecBtn += '<a style="position:absolute;left: 40px;top: 24px;color: white" onclick="changeQty(\'select-number'+cart_id+'\',0,'+seller_id+'); return false;" class="decrease" href="javascript:void(0)">_</a>';
                    incDecBtn += '<a style="position:absolute;right: 40px;top: 31px;color: white" onclick="changeQty(\'select-number'+cart_id+'\',1,'+seller_id+'); return false;" class="increase" href="javascript:void(0)">+</a>';
                    incDecBtn += '</div>';
                 $("#pro_div_id_"+product_id).html(incDecBtn);
                            
                }
            }
        });
        return false;
    },
    'wishlist': function(product_id) {
        $.ajax({
            url: 'index.php?route=bossthemes/boss_add/wishlist/',
            type: 'post',
            data: 'product_id=' + product_id,
            dataType: 'json',
            success: function(json) {
                if (json['success']) {
                    addProductNotices(json['title'], json['thumb'], json['success'], 'success');
                    $('#myModal .btn-wishlist').attr('onclick', "wishlist_remove('"+product_id+"')");
                    $('.wishlist-total').html(json['total']);
                }else{
                    addProductNotices(json['title'],'', json['info']);
                    $('.wishlist-total').html(json['total']);
                }
            }
        });
    },
	'wishlist_cat': function(data) {
	   
		$.ajax({
			url: 'index.php?route=bossthemes/boss_add/wishlist_cat/',
			type: 'post',
			data: data,
			dataType: 'json',
			success: function(json) {
				if (json['success']) {
					addProductNotice(json['title'], json['thumb'], json['success'], 'success');
					$('.wishlist-total').html(json['total']);
				}else{
					addProductNotice(json['title'],'', json['info']);
					$('.wishlist-total').html(json['total']);
				}
			}
		});
	},
    
    'compare': function(product_id) {
        $.ajax({
            url: 'index.php?route=bossthemes/boss_add/compare',
            type: 'post',
            data: 'product_id=' + product_id,
            dataType: 'json',
            success: function(json) {
                if (json['success']) {
                    addProductNotices(json['title'], json['thumb'], json['success'], 'success');
                    $('#compare-total').html(json['total']);
                }
            }
        });
    }
};

/* Agree to Terms */
$(document).delegate('.agree', 'click', function(e) {
	e.preventDefault();

	$('#modal-agree').remove();

	var element = this;

	$.ajax({
		url: $(element).attr('href'),
		type: 'get',
		dataType: 'html',
		success: function(data) {
			html  = '<div id="modal-agree" class="modal">';
			html += '  <div class="modal-dialog">';
			html += '    <div class="modal-content">';
			html += '      <div class="modal-header">';
			html += '        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>';
			html += '        <h4 class="modal-title">' + $(element).text() + '</h4>';
			html += '      </div>';
			html += '      <div class="modal-body">' + data + '</div>';
			html += '    </div';
			html += '  </div>';
			html += '</div>';

			$('body').append(html);

			$('#modal-agree').modal('show');
		}
	});
});

// Autocomplete */
(function($) {
	$.fn.autocomplete = function(option) {
		return this.each(function() {
			this.timer = null;
			this.items = new Array();
	
			$.extend(this, option);
	
			$(this).attr('autocomplete', 'off');
			
			// Focus
			$(this).on('focus', function() {
				this.request();
			});
			
			// Blur
			$(this).on('blur', function() {
				setTimeout(function(object) {
					object.hide();
				}, 200, this);				
			});
			
			// Keydown
			$(this).on('keydown', function(event) {
				switch(event.keyCode) {
					case 27: // escape
						this.hide();
						break;
					default:
						this.request();
						break;
				}				
			});
			
			// Click
			this.click = function(event) {
				event.preventDefault();
	
				value = $(event.target).parent().attr('data-value');
	
				if (value && this.items[value]) {
					this.select(this.items[value]);
				}
			}
			
			// Show
			this.show = function() {
				var pos = $(this).position();
	
				$(this).siblings('ul.dropdown-menu').css({
					top: pos.top + $(this).outerHeight(),
					left: pos.left
				});
	
				$(this).siblings('ul.dropdown-menu').show();
			}
			
			// Hide
			this.hide = function() {
				$(this).siblings('ul.dropdown-menu').hide();
			}		
			
			// Request
			this.request = function() {
				clearTimeout(this.timer);
		
				this.timer = setTimeout(function(object) {
					object.source($(object).val(), $.proxy(object.response, object));
				}, 200, this);
			}
			
			// Response
			this.response = function(json) {
				html = '';
	
				if (json.length) {
					for (i = 0; i < json.length; i++) {
						this.items[json[i]['value']] = json[i];
					}
	
					for (i = 0; i < json.length; i++) {
						if (!json[i]['category']) {
							html += '<li data-value="' + json[i]['value'] + '"><a href="#">' + json[i]['label'] + '</a></li>';
						}
					}
	
					// Get all the ones with a categories
					var category = new Array();
	
					for (i = 0; i < json.length; i++) {
						if (json[i]['category']) {
							if (!category[json[i]['category']]) {
								category[json[i]['category']] = new Array();
								category[json[i]['category']]['name'] = json[i]['category'];
								category[json[i]['category']]['item'] = new Array();
							}
	
							category[json[i]['category']]['item'].push(json[i]);
						}
					}
	
					for (i in category) {
						html += '<li class="dropdown-header">' + category[i]['name'] + '</li>';
	
						for (j = 0; j < category[i]['item'].length; j++) {
							html += '<li data-value="' + category[i]['item'][j]['value'] + '"><a href="#">&nbsp;&nbsp;&nbsp;' + category[i]['item'][j]['label'] + '</a></li>';
						}
					}
				}
	
				if (html) {
					this.show();
				} else {
					this.hide();
				}
	
				$(this).siblings('ul.dropdown-menu').html(html);
			}
			
			$(this).after('<ul class="dropdown-menu"></ul>');
			$(this).siblings('ul.dropdown-menu').delegate('a', 'click', $.proxy(this.click, this));	
			
		});
	}
})(window.jQuery);

$(document).ready(function() {
    $('#wishlist_cat_btn').click(function () {
        
        btadd.wishlist_cat($('#wishlist_cat_form').serializeArray());
        changeHeart($('#wishlist_product_id').val(),$('#wishlist_seller_id').val());
        $('#modal-wishlist_category').modal('hide');
    });
});

function wishlist_cat_add(product_id,seller_id) {

    $('#wishlist_product_id').val(product_id);
    $('#wishlist_seller_id').val(seller_id);
    $('#modal-wishlist_category').modal('show');
}