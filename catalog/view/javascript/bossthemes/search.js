//show result in divs
function showResult(data, status)
{
    
    //empty the previous results 
    $("#live-search-dropdown ul").empty();

    //display new results
    var filter = $("#search-live").val();
    //this check is for the asynchronous ajax call return 
    //if the input box is empty don't display the result
    
    if(!(filter=="")){
        $("#live-search-dropdown").show();
      for(result in data)
      {
        var price ='';
        var pId = data[result].product_id;
          var sId = data[result].seller_id;
        if(data[result].special)
        {
            price = data[result].special;
        }
        else{
            price = data[result].price;
        }
        $("#live-search-dropdown ul").
            append('<li> <img src="'+ data[result].image + '">'+
                   '<a data-toggle="modal" data-target="#myModal" onclick="getModalContent('+pId+','+sId+');" href="javascript: void(0)">'+ data[result].name + '</a> &nbsp'+ price +
                   '<button id= "' + data[result].product_id +','+data[result].seller_id+'"title="Add to Cart" type="button" class="addcart">'+
                   '<i class="fa fa-plus"></i></button></li>'+
                   '<hr style="margin-top: 1px; margin-bottom: 1px;">'
            );

      }


    }else{
         $("#live-search-dropdown").hide();
    }
    
}
//cart notification window
function showCartnotification(json) 
{
      if (json['redirect']) {
        location = json['redirect'];
      }

      if (json['success']) {
        addProductNotice(json['title'], json['thumb'], json['success'], 'success');
        $('.b-cart-total').html(json['total']);
      
        $('.b-cart > ul').load('index.php?route=common/cart/info ul li');
      }
}

$(document).ready(function(){

  //event for add to cart button
  $("#live-search-dropdown").on('click',function(event){
    
      if(event.target.nodeName == "I")
      {
        //alert("I"); return false;
          var product_id = event.target.parentElement.id;
          var product_data  = {product_id: product_id, quantity: 1};
          //make an ajax request to add the item
          $.ajax({
              url:"index.php?route=bossthemes/boss_add/cart/",
              method:"POST",
              data:product_data,
              success:showCartnotification


          });
          
      }
      else if(event.target.nodeName == "BUTTON")
      { 
         
          var product_seller = event.target.id;
          var product_array =  product_seller.split(",");
          
          var product_id = product_array[0];
          var seller_id  = product_array[1];
         
          var product_data  = {product_id: product_id, quantity: 1, seller_id:seller_id };
          //make an ajax request to add the item
          $.ajax({
              url:"index.php?route=bossthemes/boss_add/cart/",
              method:"POST",
              data:product_data,
              success:showCartnotification


          });
      }
        
  });
  //event for hover on search bar
  $("#search").on({
    mouseenter: function () {
    
        //console.log("Mouse enter");
        
        $("#search-live").show();

        var filter = $("#search-live").val();

        $("#live-search-dropdown").hide();

        
    },
    mouseleave: function () {
        //stuff to do on mouse leave
        //console.log("Mouse leave");

        
        var filter = $("#search-live").val()
        //CHECK IF INPUT VALUE IS EMPTY BEFORE HIDING DIVS

        if(filter=="")
        { 
          $("#search-live").show();
          $("#live-search-dropdown").hide();
        }  
        
    }
  });
	//event for search bar selected
	$("#search-live").on("input",function(){
    var filter = $("#search-live").val()
    var seller_id   = $("#seller_id").val();
    //check if box is empty
    if(!(filter == ""))
    {
        $.ajax({
          dataType: "json",
           url:"index.php?route=product/live_search&filter_name=" + filter +"&seller_id="+seller_id,
          method: "GET",
          success: showResult

      });  
    }
    else
    {
        $("#live-search-dropdown ul").empty();      
    }

		
	});
});