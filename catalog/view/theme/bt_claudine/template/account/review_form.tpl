<?php echo $header; ?>
<div class="container">
  <ul class="breadcrumb">
    <?php foreach ($breadcrumbs as $breadcrumb) { ?>
    <li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
    <?php } ?>
  </ul>
  <?php if ($error_warning) { ?>
  <div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> <?php echo $error_warning; ?></div>
  <?php } ?>
  
   <?php if ($success) { ?>
  <div class="alert alert-success"><i class="fa fa-check-circle"></i> <?php echo $success; ?></div>
  <?php } ?>
  
  <div class="row"><?php echo $column_left; ?>
    <?php if ($column_left && $column_right) { ?>
    <?php $class = 'col-sm-6'; ?>
    <?php } elseif ($column_left || $column_right) { ?>
    <?php $class = 'col-sm-9'; ?>
    <?php } else { ?>
    <?php $class = 'col-sm-12'; ?>
    <?php } ?>
    <div id="content" class="<?php echo $class; ?>"><?php echo $content_top; ?>
      <h1><?php echo $heading_title; ?></h1>
       <form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data" class="form-horizontal" id="form-review">
       
		 <div class="form-group required">
                  <div class="col-sm-12">
                    <label class="control-label" for="input-name">Your Name:</label>
                    <input type="text" name="author" readonly  value="<?php echo $author; ?>" id="input-name" class="form-control" />
                  </div>
                </div>
				
		<div class="form-group required">
                  <div class="col-sm-12">
                    <label class="control-label" for="input-review">Your Reviews:</label>
					 <input type="hidden" name="order_id" value="<?php echo $order_id; ?>" />
	                <input type="hidden" name="seller_id" value="<?php echo $seller_id; ?>" />
                    <textarea name="text" rows="5" id="input-review" class="form-control"><?php echo $text; ?></textarea>
					<?php if ($error_text) { ?>
					<span class="text-danger"><?php echo $error_text; ?></span>
					<?php } ?>
                  </div>
                </div>
				
         <div class="form-group required">
                  <div class="col-sm-12">
                    <label class="control-label"><?php echo $entry_rating; ?></label>
                    <b class="rating"><?php echo $entry_bad; ?></b>&nbsp;
              <?php if ($rating == 1) { ?>
              <input type="radio" name="rating" value="1" checked />
              <?php } else { ?>
              <input type="radio" name="rating" value="1" />
              <?php } ?>
              &nbsp;
              <?php if ($rating == 2) { ?>
              <input type="radio" name="rating" value="2" checked />
              <?php } else { ?>
              <input type="radio" name="rating" value="2" />
              <?php } ?>
              &nbsp;
              <?php if ($rating == 3) { ?>
              <input type="radio" name="rating" value="3" checked />
              <?php } else { ?>
              <input type="radio" name="rating" value="3" />
              <?php } ?>
              &nbsp;
              <?php if ($rating == 4) { ?>
              <input type="radio" name="rating" value="4" checked />
              <?php } else { ?>
              <input type="radio" name="rating" value="4" />
              <?php } ?>
              &nbsp;
              <?php if ($rating == 5) { ?>
              <input type="radio" name="rating" value="5" checked />
              <?php } else { ?>
              <input type="radio" name="rating" value="5" />
              <?php } ?>
              &nbsp; <b class="rating"><?php echo $entry_good; ?></b>
              <?php if ($error_rating) { ?>
              <span class="text-danger"><?php echo $error_rating; ?></span>
              <?php } ?></div>
                </div>
				
				
         <div class="buttons clearfix">
          <div class="pull-left"><a href="<?php echo $cancel; ?>" class="btn btn-default">Cancel</a></div>
          <div class="pull-right">
            <input type="submit" value="Save" class="btn btn-primary" />
          </div>
        </div>
		
    
      </form>
      <?php echo $content_bottom; ?></div>
    <?php echo $column_right; ?></div>
</div>

<?php echo $footer; ?>