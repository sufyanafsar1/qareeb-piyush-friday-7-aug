<div class="container">
    <div class="row">
        <div class="content_bg">
            <div class="row">
                <div class="col-sm-6">
                    <div class="login-content">
                        <!-- SignIn Start -->
                        <div id="modal-quicksignIn" class="modal col-sm-6 custom-margin-3 ">
                            <div class="modal-dialog">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                        
                                    </div>
                                    <div class="modal-body modal-body-custom">
                                        <div class="row">
                                            <div class="col-sm-12" id="quick-login">
                                               
                                               <div class="tabbable"> <!-- Only required for left/right tabs -->
                                            <ul class="nav nav-tabs nav-login">
                                            <li><a href="#signup" id="signupTab" data-toggle="tab"><?php echo $text_register; ?></a></li>
                                            <li class="active"><a id="signinTab" href="#signin" data-toggle="tab"><?php echo $text_login; ?></a></li>
                                            </ul>
                                            <div class="tab-content">
                                            <div class="tab-pane " id="signup">
                                                <?php echo $signup; ?>
                                            </div>
                                            <div class="tab-pane active" id="signin">
              <form id="quick-signin" action="<?php echo $action; ?>" method="post" enctype="multipart/form-data">
              <div class="form-group">
                <label class="control-label" for="input-email"><?php echo $entry_email; ?></label>
                <input type="text" name="email" value="<?php echo $email; ?>" placeholder="<?php echo $entry_email; ?>" id="email" class="form-control" />
              </div>
              <div class="form-group">
                <label class="control-label" for="input-password"><?php echo $entry_password; ?></label>
                <input type="password" name="password" value="<?php echo $password; ?>" placeholder="<?php echo $entry_password; ?>" id="password" class="form-control" />
              </div>
              <a class="forgotten" href="<?php echo $forgotten; ?>"><?php echo $text_forgotten; ?></a>
              <br />
              <div class="col-sm-12 custom-margin-1">
              <input type="button" id="btnLogin" value="<?php echo $button_login; ?>" class="btn button_login btn-primary-custom" />
              </div>
              <?php if ($redirect) { ?>
              <input type="hidden" name="redirect" value="<?php echo $redirect; ?>" />
              <?php } ?>
			  
            </form>
                                            </div>
                                            </div>
                                            </div>
                                            </div>
                                        </div>
                                    </div>
                                    
                                </div>
                            </div>
                        </div>
                        <!-- SignIn END -->


                    </div>
                </div>
            </div>
        </div>

    </div>
</div>
