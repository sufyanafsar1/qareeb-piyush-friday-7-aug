<?php echo $header; ?>
<style type="text/css">
  .wishlist_active a {
    color: #ffffff !important;
}
.wishlist_active {
    background: #8bc42f;
}
.wishlist_cat {
    font-size: 20px;
    padding: 10px;
}
</style>
<div class="bt-breadcrumb">
<div class="container">
  <ul class="breadcrumb">
    <?php foreach ($breadcrumbs as $breadcrumb) { ?>
      <li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
    <?php } ?>
  </ul>
  </div>
</div>
<div class="container">
  <div class="row">
  <?php 
      echo $column_left; 
      echo $column_right;
      if ($column_left && $column_right) {
        $class = 'col-sm-6';
      } elseif ($column_left || $column_right) {
        $class = 'col-sm-9';
      } else {
        $class = 'col-sm-12';
      } 
    ?>
    <div id="content" class="background-white <?php echo $class; ?>"><?php echo $content_top; ?>
		  <?php if ($success) { ?>
		  <div class="alert alert-success"><i class="fa fa-check-circle"></i> <?php echo $success; ?>
			<button type="button" class="close" data-dismiss="alert">&times;</button>
		  </div>
		  <?php } ?>
	<div class="content_bg">	
    <h2><?php echo $heading_title; ?></h2>  
      <?php if ($products) { ?>
	  <div class="wishlist-info">
      <div class="col-md-3">
        <?php $class = null;
        if(!isset($_GET['cat'])){
          $class = 'wishlist_active';
        } ?>
        <div class="wishlist_cat <?php echo $class ?>">
          <a href="/demo/index.php?route=account/wishlist">All</a>
        </div>
        <?php
          foreach($wishlist_cat as $key => $value){
            if($value['category_name'] == $_GET['cat']){
              $class = 'wishlist_active';
            }else{
              $class = null;
            }
            if(!is_null($value['category_name'])){
              echo '<div class="wishlist_cat '.$class.'"><a href="/demo/index.php?route=account/wishlist&cat='.$value['category_name'].'">'.$value['category_name'].'</a></div>';
            }
          }
        ?>
      </div>
      <div class="col-md-9">
        <table class="table">
          <thead>
            <tr>
              <td class="image"><?php echo $column_name; ?></td>
              <td class="model"><?php echo $column_model; ?></td>
              <td class="product_price"><?php echo $column_price; ?></td>
              <td class="stock"><?php echo $column_stock; ?></td>
              <td class="remove"><?php echo $button_remove; ?></td>
              <td class="action"><?php echo $column_action; ?></td>
            </tr>
          </thead>        
          <tbody>   
      <?php if ($products) { ?>
          <?php foreach ($products as $product) { ?>
            <tr>
              <td class="thumb_image"><div class="visible-xs title_column"><?php echo $column_name; ?></div><?php if ($product['thumb']) { ?>
                <div class="image"><a href="<?php echo $product['href']; ?>"><img src="<?php echo $product['thumb']; ?>" alt="<?php echo $product['name']; ?>" title="<?php echo $product['name']; ?>" /></a></div>
                <?php } ?>
              <div class="name"><a href="<?php echo $product['href']; ?>"><?php echo $product['name']; ?></a></div></td>
              <td class="model"><div class="visible-xs title_column"><?php echo $column_model; ?></div><?php echo $product['model']; ?></td>
              <td class="product_price"><div class="visible-xs title_column"><?php echo $column_price; ?></div><?php if ($product['price']) { ?>
                <div class="price">
                  <?php if (!$product['special']) { ?>
                  <?php echo $product['price']; ?>
                  <?php } else { ?>
                  <span class="price-old"><?php echo $product['price']; ?></span><span class="price-new"><?php echo $product['special']; ?></span> 
                  <?php } ?>
                </div>
                <?php } ?></td>
              <td class="stock"><div class="visible-xs title_column"><?php echo $column_stock; ?></div><?php echo $product['stock']; ?></td>
              <td class="remove"><div class="visible-xs title_column"><?php echo $button_remove; ?></div><a href="<?php echo $product['remove']; ?>" data-toggle="tooltip" title="<?php echo $button_remove; ?>" class="button_remove"><i class="fa fa-close"></i></a></td>
           <td class="action"><div class="visible-xs title_column"><?php echo $column_action; ?></div>
              <div class="cart_button">
                <button type="button" onclick="cart.add('<?php echo $product['product_id']; ?>');" title="<?php echo $button_cart; ?>" class="btn button_cart"><?php echo $button_cart; ?></button>
              </div>
            </td> 
                </tr>
            <?php } ?>

      <?php } else { ?>
      <tr>
        <td colspan="7"><?php echo $text_empty; ?></td>
      </tr>
      <?php } ?>
          </tbody>        
        </table>
      </div>
	  </div>
      <div class="buttons clearfix">
        <div class="pull-right"><a href="<?php echo $continue; ?>" class="btn"><?php echo $button_continue; ?></a></div>
      </div>
      </div>
      <?php echo $content_bottom; ?></div>
    </div>
</div>
<?php echo $footer; ?> 