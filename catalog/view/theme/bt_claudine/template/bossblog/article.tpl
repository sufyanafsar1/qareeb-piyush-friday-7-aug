<?php echo $header; ?>
<div class="bt-breadcrumb">
<div class="container">
  <ul class="breadcrumb">
	<li class="b_breadcrumb"><?php echo $heading_title; ?></li>
    <?php foreach ($breadcrumbs as $breadcrumb) { ?>
    <li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
    <?php } ?>
  </ul>
  </div>
</div>
<div class="container">
<div class="row"><?php echo $column_left; ?><?php echo $column_right; ?>
    <?php if ($column_left && $column_right) { ?>
    <?php $class = 'col-sm-6'; ?>
    <?php } elseif ($column_left || $column_right) { ?>
    <?php $class = 'col-sm-9'; ?>
    <?php } else { ?>
    <?php $class = 'col-sm-12'; ?>
    <?php } ?>
<div id="content" class="<?php echo $class; ?>"><?php echo $content_top; ?>
	<h1 class="article-title-boss"><?php echo $heading_title; ?></h1> 
	<div class="date-post">			
			<small class="time-stamp time-article">
				<?php $date = new DateTime($date_modified);?>
				<?php echo $date->format('M d, Y');?>
			,</small> &nbsp;&nbsp;
			<span class="post-by"><?php echo $text_postby;?><span><?php echo $author; ?></span>,</span>&nbsp;&nbsp;
			<span class="comment-count"><span><?php echo $comments; ?></span> <?php echo $text_comments;?></span>
		</div>
	<div class="boss_article-item boss_article-detail">
		<div class="content_bg">
		<div class="article-title">
			<p><?php echo $title;?></p>        
		</div>
		
		<div class="article-content">
			<?php echo $content;?>		
			
		</div>
		<div class="boss_article-action">
			<?php if ($tags && !empty($tags)) { ?>
			  <div class="tags"><span><?php echo $text_tags; ?>: </span>
			  <ul>
				<?php for ($i = 0; $i < count($tags); $i++) { ?>
				<?php if ($i < (count($tags) - 1)) { ?>
				<li class="item"><a href="<?php echo $tags[$i]['href']; ?>"><?php echo $tags[$i]['tag']; ?></a></li>
				<?php } else { ?>
				<li class="item"><a href="<?php echo $tags[$i]['href']; ?>"><?php echo $tags[$i]['tag']; ?></a></li>
				<?php } ?>
				<?php } ?>
			  </ul>
			  </div>
			<?php } ?>
		<div class="article-share"><!-- AddThis Button BEGIN -->
			 <div class="addthis_inline_share_toolbox"></div>
			 <script type="text/javascript" src="//s7.addthis.com/js/300/addthis_widget.js#pubid=ra-592f9ff1f0fd9929"></script> 
			  <!-- AddThis Button END --> 
			</div>	
		</div>
		</div>
		<div class="article-related">
			<h3 class="title_heading"><?php echo $article_related; ?></h3>
			 <div id="boss_min_height">
			 <div id="article_related">
			   <?php foreach ($articles as $article) { ?> 
					<div class="article-items">
					<div class="article-image"><a href="<?php echo $article['href']; ?>"><img src="<?php echo $article['thumb']; ?>" alt="<?php echo $article['name']; ?>" title="<?php echo $article['name']; ?>" class="img-responsive" /></a>
					<span class="time-image">
						<?php $date = new DateTime($article['date_modified']);?>
						<small class="time-date"><?php echo $date->format('d');?></small>
						<small class="time-month"><?php echo $date->format('M');?></small>
					</span>
					</div>
					<div class="article_dt">
						<div class="article-name"><a href="<?php echo $article['href']; ?>"><?php echo $article['name']; ?></a></div>
						<div class="article-footer">					
							<span class="time-stamp">
								<?php $date = new DateTime($article['date_modified']);?>
								<small><?php echo $date->format('M d, Y');?></small>
							</span>
							<span>&nbsp; / &nbsp;</span>
							<span class="post-by"><span><?php echo $text_postby;?> <?php echo $article['author']; ?></span></span>	
							<span>&nbsp; / &nbsp;</span>
							<span class="comment-count"><span><?php echo $article['comment']; ?> </span><a href="<?php echo $article['href']; ?>"><?php echo $text_comments;?></a></span>                 
						</div>
						<div class="article-title">
							<p><?php echo $article['title']; ?></p>                   
						</div>
						<div class="read_more">
						<a class="btn" href="<?php echo $article['href']; ?>" title="<?php echo $article['name']; ?>" ><?php echo $text_readmore;?></a>
						</div>
					</div>
				    </div>
			   <?php } ?>			   
			 </div>
			 <a id="prev_ar_related" class="prev nav_thumb" href="javascript:void(0)" style="display:block;" title="prev"><i title="Previous" class="fa fa-chevron-left">&nbsp;</i></a>
			<a id="next_ar_related" class="next nav_thumb" href="javascript:void(0)" style="display:block;" title="next"><i title="Next" class="fa fa-chevron-right">&nbsp;</i></a>
			 </div>
		</div>
			
	<?php if (!empty($products)) { ?>
	<div id="product-related" class="product-related">
		<h2 class="title_heading"><?php echo $text_product_related; ?> (<?php echo count($products); ?>)</h2>
		<div class="carousel-button">
			<a id="prev_related" class="prev nav_thumb" href="javascript:void(0)" style="display:block;" title="prev"><i title="Previous" class="fa fa-chevron-left">&nbsp;</i></a>
			<a id="next_related" class="next nav_thumb" href="javascript:void(0)" style="display:block;" title="next"><i title="Next" class="fa fa-chevron-right">&nbsp;</i></a>
		</div>
		<div class="list_carousel responsive" >
			<ul id="product_related" class="content-products"><?php foreach ($products as $product) { ?><li>				
			<div class="product-thumb transition">
				<?php if ($product['thumb']) { ?>		
				<div class="image"><a href="<?php echo $product['href']; ?>"><img src="<?php echo $product['thumb']; ?>" alt="<?php echo $product['name']; ?>" title="<?php echo $product['name']; ?>" class="img-responsive" /></a>					
					<div class="button-group">
						<a class="btn-cart" title="<?php echo $button_cart; ?>" onclick="btadd.cart('<?php echo  $product['product_id'];  ?>');"><?php echo $button_cart; ?></a>
						<a class="btn-wishlist" title="<?php echo $text_add_to_compare; ?>" onclick="btadd.compare('<?php echo  $product['product_id'];  ?>');"><?php echo $text_add_to_compare; ?></a>
						<a class="btn-compare" title="<?php echo $text_add_to_wish_list; ?>" onclick="wishlist_cat_add('<?php echo $product['product_id']; ?>')""><?php echo $text_add_to_wish_list; ?></a>
					</div>
				</div>
				<?php } ?>  
				<div class="small_detail">
					<div class="caption">	
						<div class="name"><a href="<?php echo $product['href']; ?>"><?php echo $product['name']; ?></a></div>
						<?php if ($product['price']) { ?>
						<div class="price">
						  <?php if (!$product['special']) { ?>
						  <?php echo $product['price']; ?>
						  <?php } else { ?>
						 <span class="price-new"><?php echo $product['special']; ?></span>  <span class="price-old"><?php echo $product['price']; ?></span>
						  <?php } ?>
						</div><?php } ?>
					</div>		
				</div>
			
			</div>
			</li><?php } ?></ul>        
		</div>
    </div>
	<?php } ?> 
	
	<?php if ($allow_comment!=0) {?>
		<?php if ($comment_status==1||$allow_comment==1) {?>
	<div class="comments">                
		<div id="article-comments"></div> 
		<div class="form-comment-container">
			<span id="new">
				<h3 class="title_heading"><?php echo $text_comment; ?> </h3>
			</span>       
			<div id="0_comment_box" class="form-comment content_bg">
				<div class="row">
				<?php if(!$login){?>
				<div class="col-sm-6 col-xs-12">				  
					  <div class="field" id="username">
						  <label class="" for="name"><?php echo $text_username; ?><em>*</em></label>
						  <div class="input-box">
							  <input type="text" class="form-control required-entry" value="" title="Name" id="name" name="username">
						  </div>
					  </div>                    
					  <div class="field" id="email1">
						  <label class="" for="email"><?php echo $text_email; ?><em>*</em></label>
						  <div class="input-box">
							  <input type="text" class="form-control required-entry" value="" title="Email" id="email1" name="email_blog1">
						  </div>
					  </div>				  
				</div>
				<div class="col-sm-6 col-xs-12">	
				  <div class="input-box-comment">
					  <label class="tt_input" for="comment"><?php echo $entry_comment; ?><em>*</em></label>
					  <textarea rows="2" cols="10" class="required-entry form-control" style="height:122px" title="Comment" id="comment" name="comment_content"></textarea>
				  </div>
				</div>  
				<?php } else{?>
						<div class="col-sm-6 col-xs-12">	
						  <div class="input-box-comment">
							  <label class="tt_input" for="comment"><?php echo $entry_comment; ?><em>*</em></label>
							  <textarea rows="2" cols="10" class="required-entry form-control" style="height:122px" title="Comment" id="comment" name="comment_content"></textarea>
						  </div>
						</div> 
						<input type="hidden" class="form-control required-entry" value="<?php echo $username; ?>" title="Name" id="name" name="username">
						<input type="hidden" class="form-control required-entry validate-email" value="<?php echo $email; ?>" title="Email" id="email" name="email_blog1">
				  <?php } ?>
				</div>  
				  <?php if ($site_key) { ?>
					<div class="form-group">
					  <div class="col-sm-offset-2 col-sm-10">
						<div class="g-recaptcha" data-sitekey="<?php echo $site_key; ?>"></div>
						<?php if ($error_captcha) { ?>
						  <div class="text-danger"><?php echo $error_captcha; ?></div>
						<?php } ?>
					  </div>
					</div>
				  <?php } ?>
				  <div class="submit-button">
					<div class="left"><br/><a id="button-comment" class="btn"><?php echo $button_continue; ?></a></div>
				  </div>
			</div>
		</div>                   
	 </div>
	 <?php } } ?>   
			
        </div>	
  <?php echo $content_bottom; ?></div>
  
  </div>
 </div>
<script type="text/javascript" src="catalog/view/javascript/bossthemes/jquery.touchSwipe.min.js"></script>
<script type="text/javascript" src="catalog/view/javascript/bossthemes/carouFredSel-6.2.1.js"></script> 
<script type="text/javascript"><!--
$('#article-comments').load('index.php?route=bossblog/article/comment&blog_article_id=<?php echo $blog_article_id; ?>');
$('#button-comment').bind('click', function() { 
	$.ajax({
		url: 'index.php?route=bossblog/article/write&blog_article_id=<?php echo $blog_article_id; ?>&need_approval=<?php echo $need_approval; ?>&approval_status=<?php echo $approval_status; ?>',
		type: 'post',
		dataType: 'json',
		data: 'username=' + encodeURIComponent($('input[name=\'username\']').val()) + '&comment_content=' + encodeURIComponent($('textarea[name=\'comment_content\']').val()) + '&email1=' + encodeURIComponent($('input[name=\'email_blog1\']').val()) + '&g-recaptcha-response=' + encodeURIComponent($('textarea[name=\'g-recaptcha-response\']').val()),		
		beforeSend: function() {
			$('.success, .warning').remove();
			$('#button-comment').attr('disabled', true);
			$('#new').after('<div class="attention"><img src="catalog/view/theme/default/image/loading.gif" alt="" /> <?php echo $text_wait; ?></div>');
		},
		complete: function() { 
			$('#button-comment').attr('disabled', false);			
			$('.attention').remove();			
		},		
		success: function(json) { 
			if (json['error']) {
				$('#new').after('<div class="warning">' + json['error'] + '</div>');
			}
			
			if (json['success']) {
				$('#new').after('<div class="success">' + json['success'] + '</div>');
				$('#article-comments').load('index.php?route=bossblog/article/comment&blog_article_id=<?php echo $blog_article_id; ?>');				
				$('input[name=\'username\']').val('');
				$('textarea[name=\'comment_content\']').val('');
				$('input[name=\'email_blog1\']').val('');                
			}
		}
	});
});
$(window).load(function(){
	$('#product_related').carouFredSel({
        auto: false,
        responsive: true,
        width: '100%',
        prev: '#prev_related',
        next: '#next_related',
        swipe: {
        onTouch : false
        },
        items: {
            width: 280,
			height: 'auto',
            visible: {
            min: 1,
            max: 4
            }
        },
        scroll: {
            direction : 'left',    //  The direction of the transition.
            duration  : 1000   //  The duration of the transition.
        }
	});
	
	$('#article_related').carouFredSel({
        auto: false,
        responsive: true,
        width: '100%',
        prev: '#prev_ar_related',
        next: '#next_ar_related',
        swipe: {
        onTouch : false
        },
        items: {
            width: 410,
			/*height: 'auto',*/
            visible: {
            min: 1,
            max: 3
            }
        },
        scroll: {
            direction : 'left',    //  The direction of the transition.
            duration  : 1000   //  The duration of the transition.
        }
	});
//	$('.caroufredsel_wrapper #article_related').css("max-height",getMaxHeight('#article_related .article-items')+2);
//	$('.caroufredsel_wrapper').css("max-height",getMaxHeight('#article_related .article-items')+2);
});
function getMaxHeight($elms) {
	var maxHeight = 0;
	$($elms).each(function () {
	
		var height = $(this).outerHeight();
		if (height > maxHeight) {
			maxHeight = height;
		}
	}); 
	return maxHeight;
};  
//--></script> 
<?php echo $footer; ?>