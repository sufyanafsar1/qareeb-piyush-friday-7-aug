<?php echo $header; ?>
<div class="bt-breadcrumb">
<div class="container">
  <ul class="breadcrumb">
	<li class="b_breadcrumb"><?php echo $heading_title; ?></li>
    <?php foreach ($breadcrumbs as $breadcrumb) { ?>
    <li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
    <?php } ?>
  </ul>
  </div>
</div>
<div class="container">
  <div class="row"><?php echo $column_left; ?><?php echo $column_right; ?>
    <?php if ($column_left && $column_right) { ?>
    <?php $class = 'col-sm-6'; ?>
    <?php } elseif ($column_left || $column_right) { ?>
    <?php $class = 'col-sm-9'; ?>
    <?php } else { ?>
    <?php $class = 'col-sm-12'; ?>
    <?php } ?>
    <div id="content" class="<?php echo $class; ?>"><?php echo $content_top; ?>
	  <?php if ($attention) { ?>
	  <div class="alert alert-info"><i class="fa fa-info-circle"></i> <?php echo $attention; ?>
		<button type="button" class="close" data-dismiss="alert">&times;</button>
	  </div>
	  <?php } ?>
	  <?php if ($success) { ?>
	  <div class="alert alert-success"><i class="fa fa-check"></i> <?php echo $success; ?>
		<button type="button" class="close" data-dismiss="alert">&times;</button>
	  </div>
	  <?php } ?>
	  <?php if ($error_warning) { ?>
	  <div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> <?php echo $error_warning; ?>
		<button type="button" class="close" data-dismiss="alert">&times;</button>
	  </div>
	  <?php } ?>
	  <div class="content_bg" style="padding-bottom:20px;margin-bottom:40px">
      <form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data">
        <div class="table-responsive cart-info">
          <table class="table">
            <thead>
              <tr>
                <td class="image"><?php echo $column_name; ?></td>
                <td class="model"><?php echo $column_model; ?></td>
                <td class="product_price"><?php echo $column_price; ?></td>
                <td class="quantity"><?php echo $column_quantity; ?></td>
                <td class="total"><?php echo $column_total; ?></td>
                <td class="remove"></td>
              </tr>
            </thead>
            <tbody>
              <?php $i=0; foreach ($products as $product) { ?>
              <tr>
                <td class="thumb_image">
					<div class="visible-xs title_column"><?php echo $column_name; ?></div>
				  <?php if ($product['thumb']) { ?>
                  <div class="image"><a href="<?php echo $product['href']; ?>"><img src="<?php echo $product['thumb']; ?>" alt="<?php echo $product['name']; ?>" title="<?php echo $product['name']; ?>" class="" /></a></div>
                  <?php } ?>
                  <div class="name"><a href="<?php echo $product['href']; ?>"><?php echo $product['name']; ?></a>
                  <?php if (!$product['stock']) { ?>
                  <span class="text-danger">***</span>
                  <?php } ?>
                  <?php if ($product['option']) { ?>
                  <?php foreach ($product['option'] as $option) { ?>
                  <br />
                  <small><?php echo $option['name']; ?>: <?php echo $option['value']; ?></small>
                  <?php } ?>
                  <?php } ?>
                  <?php if ($product['reward']) { ?>
                  <br />
                  <small><?php echo $product['reward']; ?></small>
                  <?php } ?>
                  <?php if ($product['recurring']) { ?>
                  <br />
                  <span class="label label-info"><?php echo $text_recurring_item; ?></span> <small><?php echo $product['recurring']; ?></small>
                  <?php } ?>
				  </div>	
				</td>
                <td class="model"><div class="visible-xs title_column"><?php echo $column_model; ?></div><?php echo $product['model']; ?></td>
                <td class="quantity"><div class="visible-xs title_column"><?php echo $column_quantity; ?></div><span class="price"><?php echo $product['price']; ?></span></td>
                <td class="product_price">
					<div class="visible-xs title_column"><?php echo $column_price; ?></div>
					<button onclick="changeQty('<?php echo 'select-number'.$i;?>',0); return false;" class="decrease"><i class="fa fa-minus"></i></button>
                    <input id="select-number<?php echo $i; ?>" type="text" name="quantity[<?php echo $product['cart_id']; ?>]" value="<?php echo $product['quantity']; ?>" size="1" class="form-control " />
					<button onclick="changeQty('<?php echo 'select-number'.$i;?>',1); return false;" class="increase"><i class="fa fa-plus"></i></button>
                    <button type="submit" data-toggle="tooltip" title="<?php echo $button_update; ?>" class="button_update"></button>
                </td>
                <td class="total"><div class="visible-xs title_column"><?php echo $column_total; ?></div><span class="price"><?php echo $product['total']; ?></span></td>
				<td class="remove">
					<button type="button" data-toggle="tooltip" title="<?php echo $button_remove; ?>" class="button_remove" onclick="cart.remove('<?php echo $product['cart_id']; ?>');"><i class="fa fa-close"></i></button>
				</td>
              </tr>
              <?php $i++; } ?>
              <?php foreach ($vouchers as $vouchers) { ?>
              <tr>
                <td class="thumb_image">
					<div class="visible-xs title_column"><?php echo $column_name; ?></div>
					<?php echo $vouchers['description']; ?>
				</td>
                <td class="model"><div class="visible-xs title_column"><?php echo $column_model; ?></div></td>
                <td class="quantity">
					<div class="visible-xs title_column"><?php echo $column_quantity; ?></div>
					<span class="price"><?php echo $vouchers['amount']; ?></span></td>
                <td class="product_price">
					<div class="visible-xs title_column"><?php echo $column_price; ?></div>
                    <input type="text" name="" value="1" size="1" disabled="disabled" class="form-control" />
                </td>
                <td class="total">
					<div class="visible-xs title_column"><?php echo $column_total; ?></div>
					<span class="price"><?php echo $vouchers['amount']; ?></span></td>
				<td class="remove">
					<button type="button" data-toggle="tooltip" title="<?php echo $button_remove; ?>" class="button_remove" onclick="voucher.remove('<?php echo $vouchers['cart_id']; ?>');"><i class="fa fa-close"></i></button>
				</td>	
              </tr>
              <?php } ?>
            </tbody>
          </table>
        </div>
      </form>
	  </div>
	  <div class="content_bg">
      <?php if ($coupon || $voucher || $reward || $shipping) { ?>
      <h2 class="text_next_cart"><?php echo $text_next; ?></h2>
      <p><?php echo $text_next_choice; ?></p>
      <div class="panel-group cart-module" id="accordion"><?php echo $coupon; ?><?php echo $voucher; ?><?php echo $reward; ?><?php echo $shipping; ?></div>
      <?php } ?>
      <div class="cart-total">
          <table>
            <?php foreach ($totals as $total) { ?>
            <tr>
              <td class="left <?php echo ($total==end($totals)? ' last': ''); ?>"><b><?php echo $total['title']; ?>:</b></td>
              <td class="right <?php echo ($total==end($totals)? ' last': ''); ?>"><?php echo $total['text']; ?></td>
            </tr>
            <?php } ?>
          </table>
      </div>
      <div class="buttons cart-buttons">
        <a href="<?php echo $continue; ?>" class="btn"><?php echo $button_shopping; ?></a>
      
        <?php  
                if(isset($_SESSION['default']['customer_id']) && $_SESSION['default']['customer_id'] > 0)
                { ?>
                      <a href="<?php echo $checkout; ?>" class="btn"><?php echo $button_checkout; ?></a>
               <?php }
                else{ ?>
                     <button class="btn" id="chkoutLogin" onclick="showLoginPopup();"> <?php echo $button_checkout; ?></button>
              <?php  }   ?>
       
      </div>
      </div>
      <?php echo $content_bottom; ?></div>
    </div>
</div>

<script type="text/javascript"><!--
function changeQty(id,increase) {
    var qty = parseInt($('#' + id+'').val());
	
    if ( !isNaN(qty) ) {
        qty = increase ? qty+1 : (qty-1 > 1 ? qty-1:1);
        $('#' + id+'').val(qty);
    }else{
		$('#' + id+'').val(1);
	}
	
}
//--></script> 
<?php echo $footer; ?> 