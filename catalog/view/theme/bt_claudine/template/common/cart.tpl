<div class="b-cart dropdown_cart cart">
  <button type="button" data-toggle="collapse" data-target="#cartDropDown" data-loading-text="<?php echo $text_loading; ?>" class="btn-dropdown-cart dropdown-toggle"><span class="b-cart-total"><b><?php echo $text_items; ?></b></span></button>
  <ul class="dropdown-menu pull-right collapse" id="cartDropDown">
    <?php if ($products || $vouchers) { ?>
    <li>
      <table class="table">
        <?php $i=0; foreach ($products as $product) { ?>
        <tr>
          <td class="text-center">
			<div class="image"><?php if ($product['thumb']) { ?>
            <a href="<?php echo $product['href']; ?>"><img src="<?php echo $product['thumb']; ?>" alt="<?php echo $product['name']; ?>" title="<?php echo $product['name']; ?>" class="img-thumbnail" /></a>
            <?php } ?>
				
			</div>
		  </td>
			
          <td class="text-left name"><a href="<?php echo $product['href']; ?>"><?php echo substr($product['name'],0,15)."..." ; ?></a>
           <span class="price price-popup"><?php echo $product['total']; ?></span>
          
            <?php if ($product['option']) { ?>
            <?php foreach ($product['option'] as $option) { ?>
            <br />
            - <small><?php echo $option['name']; ?> <?php echo $option['value']; ?></small>
            <?php } ?>
            <?php } ?>
            <?php if ($product['recurring']) { ?>
            <br />
            - <small><?php echo $text_recurring; ?> <?php echo $product['recurring']; ?></small>
            <?php } ?>
		
            <div class="qty-button">
					<div class="visible-xs title_column"><?php echo $column_price; ?></div>
					<button onclick="changeCartQty('<?php echo 'item-number'.$product['cart_id'];?>',0); return false;" class="decrease"><i class="fa fa-minus"></i></button>
                    <input onchange="updateCartQty('<?php echo $product['cart_id']; ?>', '<?php echo 'item-number'.$product['cart_id'];?>')" id="item-number<?php echo $product['cart_id']; ?>" type="text" name="quantity[<?php echo $product['cart_id']; ?>]" value="<?php echo $product['quantity']; ?>" size="1" class="form-control qty-input" />
					<button onclick="changeCartQty('<?php echo 'item-number'.$product['cart_id'];?>',1); return false;" class="increase"><i class="fa fa-plus"></i></button>
                   
                </div>
              <div class="remove-custom">
					<button type="button" onclick="cart.remove('<?php echo $product['cart_id']; ?>');" title="<?php echo $button_remove; ?>" class="button_remove"><i class="fa fa-trash-o"></i></button>
				</div>		
		  </td>
        </tr>
        <?php $i++; } ?>
        <?php foreach ($vouchers as $voucher) { ?>
        <tr class="voucher">
          <td class="text-center"> 
			<div class="image">
				<div class="remove"><button type="button" onclick="voucher.remove('<?php echo $voucher['cart_id']; ?>');" title="<?php echo $button_remove; ?>" class="btn-danger"><i class="fa fa-times"></i></button></div>
			</div>	
		  </td>
		</tr>
		<tr>	
          <td class="text-left">
			<div class="description"><?php echo $voucher['description']; ?></div>
			<div class="quantity"><span class="price"><?php echo $voucher['amount']; ?> x 1 </div>
		  </td>
        </tr>
        <?php } ?>
      </table>
    </li>
    <li>
      <div class="cart_bottom">
        <table class="minicart_total">
          <?php foreach ($totals as $total) { ?>
          <tr>
            <td class="text-left"><span><?php echo $total['title']; ?></span></td>
            <td class="text-right <?php echo ($total==end($totals) ? ' last' : ''); ?>"><?php echo $total['text']; ?></td>
          </tr>
          <?php } ?>
        </table>
        <table>
            <tr>
                   <td class="free-delivery"> <?php echo  $data['delivery']; ?> </td>     
            </tr>
        </table>
      </div>
      <div class="couponPopup"><?php echo $coupon_popup; ?></div>
	<div class="buttons">
    
		<span class="checkout_bt">
           <?php  
          
                if(isset($_SESSION['default']['customer_id']) && $_SESSION['default']['customer_id'] > 0)
                { ?>
                     <a class="btn btn-primary button_login btn-primary-custom" href="<?php echo $checkout; ?>"><?php echo $text_checkout; ?></a>
               <?php }
                else{ ?>
                     <button class="btn btn-primary button_login btn-primary-custom" id="chkoutLogin" onclick="showLoginPopup();"> <?php echo $text_checkout; ?></button>
              <?php  }   ?>
        
        
        </span>
		<!-- <span class="cart_bt"><a href="<?php echo $cart; ?>"><?php echo $text_cart; ?></a></span> -->
	</div>
    </li>
    <?php } else { ?>
    <li>
      <p class="text-center"><?php echo $text_empty; ?></p>
    </li>
    <?php } ?>
  </ul>
</div>
