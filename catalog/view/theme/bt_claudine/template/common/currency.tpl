<?php if (count($currencies) > 1) { ?>
<form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data" class="currency">
  <div class="btn-group">
    <button class="btn-link dropdown-toggle" data-toggle="dropdown">
    <span><?php echo $text_currency; ?></span>
	<?php foreach ($currencies as $currency) { ?>
    <?php if ($currency['symbol_left'] && $currency['code'] == $code) { ?>
    <strong><?php echo $currency['symbol_left']; ?><b><?php echo $currency['code']; ?></b></strong>
    <?php } elseif ($currency['symbol_right'] && $currency['code'] == $code) { ?>
    <strong><b><?php echo $currency['code']; ?></b><?php echo $currency['symbol_right']; ?></strong>
    <?php } ?>
    <?php } ?>
     <i class="fa fa-angle-down"></i></button>
    <ul class="dropdown-menu">
      <?php foreach ($currencies as $currency) { ?>
      <?php if ($currency['symbol_left']) { ?>
      <li><button class="currency-select" type="button" name="<?php echo $currency['code']; ?>"><?php echo $currency['symbol_left']; ?><b> <?php echo $currency['code']; ?></b></button></li>
      <?php } else { ?>
      <li><button class="currency-select" type="button" name="<?php echo $currency['code']; ?>"><?php echo $currency['symbol_right']; ?><b> <?php echo $currency['code']; ?></b></button></li>
      <?php } ?>
      <?php } ?>
    </ul>
  </div>
  <input type="hidden" name="code" value="" />
  <input type="hidden" name="redirect" value="<?php echo $redirect; ?>" />
</form>
<?php } ?>
