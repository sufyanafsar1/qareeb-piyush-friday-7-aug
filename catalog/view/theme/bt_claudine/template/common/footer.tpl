<?php global $config;?>

<?php

	$boss_manager = array(

		'option' => array(

			'bt_scroll_top' => true,

			'animation' 	=> true,			

		),

		'layout' => array(

			'mode_css'   => 'wide',

			'box_width' 	=> 1200,

			'h_mode_css'   => 'inherit',

			'h_box_width' 	=> 1200,

			'f_mode_css'   => 'inherit',

			'f_box_width' 	=> 1200

		),

		'status' => 1

	);

?>

<?php $footer_about = $config->get('boss_manager_footer_about'); ?>

<?php $footer_social = $config->get('boss_manager_footer_social'); ?>



<?php $footer_powered = $config->get('boss_manager_footer_powered'); ?>

<?php if($config->get('boss_manager')){

		$boss_manager = $config->get('boss_manager'); 

	}else{

		$boss_manager = $boss_manager;

	} ?>

<?php 

	if(!empty($boss_manager)){

		$layout = isset($boss_manager['layout'])?$boss_manager['layout']:''; 

		$footer_link = isset($boss_manager['footer_link'])?$boss_manager['footer_link']:'';

	}

	$f_style = '';

	if($layout['f_mode_css']=='fboxed'){

		$f_mode_css = 'bt-fboxed';

		$f_style = (!empty($layout['f_box_width']))?'style="max-width:'.$layout['f_box_width'].'px"':'';

	}else{

		$f_mode_css = '';

	}

	

?>

<div id="bt_footer_container" class="<?php echo $f_mode_css;?>" <?php echo $f_style;?>>



<footer id="bt_footer">

  <div class="container">

  <div class="row">

	<?php if(isset($footer_about['status']) && $footer_about['status']){ ?> 

	<div class="footer-about">

		<?php if($footer_about['about_title'][$config->get('config_language_id')]) { ?>

			<h3><?php echo html_entity_decode($footer_about['about_title'][$config->get('config_language_id')],ENT_QUOTES, 'UTF-8'); ?></h3>

		<?php } ?>

		<?php if($footer_about['image_status']){ ?><a href="#" title="parallax"><img alt="parallax" src="image/<?php echo $footer_about['image_link']; ?>"></a> <?php } ?>

		<?php echo html_entity_decode($footer_about['about_content'][$config->get('config_language_id')],ENT_QUOTES, 'UTF-8'); ?>

	</div>

	<?php } ?>

	<?php echo isset($btfooter)?$btfooter:''; ?> 

    <div class="footer_column">

    <div class="footer_column_content">

    <div class="row">

	

	<?php if((isset($footer_link['information']) && $footer_link['information']) || (isset($footer_link['my_account']) && $footer_link['my_account'])) { ?>

  <div class="column col-sm-cs5 col-xs-12">

    <h3><?php echo $text_information; ?><i class="fa fa-caret-down"></i><i class="fa fa-caret-up"></i></h3>

    <ul>

	  <?php if ($informations && $footer_link['information']) { ?>

      <?php foreach ($informations as $information) { ?>

      <li><a href="<?php echo $information['href']; ?>"><?php echo $information['title']; ?></a></li>

      <?php } ?>

	  <?php } ?>

    </ul>

  </div>

  <?php } ?>

  <?php if((isset($footer_link['my_account']) && $footer_link['my_account'])|| (isset($footer_link['order_history']) && $footer_link['order_history'])|| (isset($footer_link['wishlist']) && $footer_link['wishlist'])|| (isset($footer_link['newsletter']) && $footer_link['newsletter'])) { ?>

  <div class="column col-sm-cs5 col-xs-12">

    <h3><?php echo $text_account; ?><i class="fa fa-caret-down"></i><i class="fa fa-caret-up"></i></h3>

    <ul>

	  <?php if(isset($footer_link['my_account']) && $footer_link['my_account']){ ?><li><a href="<?php echo $account; ?>"><?php echo $text_account; ?></a></li><?php } if(isset($footer_link['order_history']) && $footer_link['order_history']){ ?>

      <li><a href="<?php echo $order; ?>"><?php echo $text_order; ?></a></li>

	  <?php } if(isset($footer_link['wishlist']) && $footer_link['wishlist']){ ?>

	  <li><a href="<?php echo $wishlist; ?>"><?php echo $text_wishlist; ?></a></li>

	  <?php } ?>

    </ul>

  </div>

  <?php } ?>

  <?php if((isset($footer_link['contact_us']) && $footer_link['contact_us']) || (isset($footer_link['return']) && $footer_link['return'])  || (isset($footer_link['site_map']) && $footer_link['site_map'])) { ?>

  <div class="column col-sm-cs5 col-xs-12">

    <h3><?php echo $text_service; ?><i class="fa fa-caret-down"></i><i class="fa fa-caret-up"></i></h3>

    <ul>

	  <?php if(isset($footer_link['contact_us']) && $footer_link['contact_us']){ ?>

      <li><a href="<?php echo $contact; ?>"><?php echo $text_contact; ?></a></li>

	  <?php } if(isset($footer_link['gift_vouchers']) && $footer_link['gift_vouchers']){ ?>

      <li><a href="<?php echo $voucher; ?>"><?php echo $text_voucher; ?></a></li>

	  <?php } if(isset($footer_link['site_map']) && $footer_link['site_map']){ ?>

      <li><a href="<?php echo $sitemap; ?>"><?php echo $text_sitemap; ?></a></li>

	  <?php } ?>

    </ul>

  </div>

  <?php } ?>

  <?php if((isset($footer_link['brands']) && $footer_link['brands']) || (isset($footer_link['gift_vouchers']) && $footer_link['gift_vouchers']) || (isset($footer_link['affiliates']) && $footer_link['affiliates']) || (isset($footer_link['specials']) && $footer_link['specials']) ) { ?>

  <div class="column col-sm-cs5 col-xs-12">

    <h3><?php echo $text_extra; ?><i class="fa fa-caret-down"></i><i class="fa fa-caret-up"></i></h3>

    <ul>

	  <?php if(isset($footer_link['brands']) && $footer_link['brands']){ ?>

      <li><a href="<?php echo $manufacturer; ?>"><?php echo $text_manufacturer; ?></a></li>

	  <?php } if(isset($footer_link['affiliates']) && $footer_link['affiliates']){ ?>

      <li><a href="<?php echo $affiliate; ?>"><?php echo $text_affiliate; ?></a></li>

	  <?php } if(isset($footer_link['specials']) && $footer_link['specials']){ ?>

      <li><a href="<?php echo $special; ?>"><?php echo $text_special; ?></a></li>

	  <?php } ?>

    </ul>

  </div>

  <?php } ?>

  <div class="column col-sm-cs5 col-xs-12">

	<?php if(isset($footer_social['status']) && $footer_social['status']){ ?>

			<div class="footer-social">

			<h3><?php echo html_entity_decode(isset($footer_social['title'][$config->get('config_language_id')])?$footer_social['title'][$config->get('config_language_id')]:'',ENT_QUOTES, 'UTF-8'); ?><i class="fa fa-caret-down"></i><i class="fa fa-caret-up"></i></h3>

			<ul>

				<?php if(isset($footer_social['face_status']) && $footer_social['face_status']){;?><li class="facebook"><a href="<?php echo isset($footer_social['face_url'])?$footer_social['face_url']:'#'; ?>" data-original-title="Facebook" data-placement="top" data-toggle="tooltip"><i class="fa fa-facebook"></i></a></li><?php } ?>

				<?php if(isset($footer_social['twitter_status']) && $footer_social['twitter_status']){;?><li class="twitter"><a href="<?php echo isset($footer_social['twitter_url'])?$footer_social['twitter_url']:'#'; ?>" data-original-title="Twitter" data-placement="top" data-toggle="tooltip"><i class="fa fa-twitter"></i></a></li><?php } ?>

				<?php if(isset($footer_social['googleplus_status']) && $footer_social['googleplus_status']){;?><li class="google"><a href="<?php echo isset($footer_social['googleplus_url'])?$footer_social['googleplus_url']:'#'; ?>" data-original-title="Googleplus" data-placement="top" data-toggle="tooltip"><i class="fa fa-google-plus"></i></a></li><?php } ?>

				<?php if(isset($footer_social['pinterest_status']) && $footer_social['pinterest_status']){;?><li class="pinterest"><a href="<?php echo isset($footer_social['pinterest_url'])?$footer_social['pinterest_url']:'#'; ?>" data-original-title="Pinterest" data-placement="top" data-toggle="tooltip"><i class="fa fa-pinterest"></i></a></li><?php } ?>

			</ul>

			<ul>	

				<?php if(isset($footer_social['youtube_status']) && $footer_social['youtube_status']){;?><li class="youtube"><a href="<?php echo isset($footer_social['youtube_url'])?$footer_social['youtube_url']:'#'; ?>" data-original-title="Youtube" data-placement="top" data-toggle="tooltip"><i class="fa fa-youtube-play"></i></a></li><?php } ?>

				<?php if(isset($footer_social['instagram_status']) && $footer_social['instagram_status']){;?><li class="instagram"><a href="<?php echo isset($footer_social['instagram_url'])?$footer_social['instagram_url']:'#'; ?>" data-original-title="Instagram" data-placement="top" data-toggle="tooltip"><i class="fa fa-instagram"></i></a></li><?php } ?>

				<?php if(isset($footer_social['in_status']) && $footer_social['in_status']){;?><li class="linkedin"><a href="<?php echo isset($footer_social['in_url'])?$footer_social['in_url']:'#'; ?>" data-original-title="In" data-placement="top" data-toggle="tooltip"><i class="fa fa-linkedin"></i></a></li><?php } ?>

				<?php if(isset($footer_social['rss_status']) && $footer_social['rss_status']){;?><li class="rss"><a href="<?php echo isset($footer_social['rss_url'])?$footer_social['rss_url']:'#'; ?>" data-original-title="RSS" data-placement="top" data-toggle="tooltip"><i class="fa fa-rss"></i></a></li><?php } ?>

				

			</ul>

			</div>

		<?php } ?>

  </div>

	</div>

                               

	</div>

	</div>

	</div>

  </div>

                                <div class="container">

                                       <div class="row">
								 <div class="col-xs-3">
				<a>	<iframe src="https://maroof.sa/Business/GetStamp?bid=11676" style=" width: auto; height: 210px; overflow-y:hidden;  "  frameborder="0" seamless='seamless' scrollable="no"></iframe></a>
									</div>				 
<div class="col-xs-8">
                                     <a href="https://sslanalyzer.comodoca.com/?url=careeb.com" target="_blank" ><img src="image/icons/comodo-secure-icon.png" height="64px" width="auto" alt="Mountain View"></a>
</div>
                                 </div>

                                

                                </div>

                                 <div class="powered-payment">

    <div class="container">

	<div class="row">	

		<div class="powered">

			<?php echo html_entity_decode(isset($footer_powered[$config->get('config_language_id')])?$footer_powered[$config->get('config_language_id')]:'',ENT_QUOTES, 'UTF-8'); ?>

		</div>			

	</div>

    </div>

  </div>

</footer>

</div>

<?php if(isset($boss_manager['option']['bt_scroll_top'])&&($boss_manager['option']['bt_scroll_top'])){ ?>

<div id="back_top" class="back_top"><span><i class="fa fa-caret-up"></i><b>Top</b></span></div>

 <script type="text/javascript">

        $(function(){

			$(window).scroll(function(){

				if($(this).scrollTop()>600){

				  $('#back_top').fadeIn();

				}

				else{

				  $('#back_top').fadeOut();

				}

			});

			$("#back_top").click(function (e) {

				e.preventDefault();

				$('body,html').animate({scrollTop:0},800,'swing');

			});

        });

		$(function () {

		  $('[data-toggle="tooltip"]').tooltip()

		});

		$(function () {

			$("#bt_footer h3").click(function(){

				$(this).toggleClass('opentab');

			});

		});

</script> 

<?php } ?>

<!--

OpenCart is open source software and you are free to remove the powered by OpenCart if you want, but its generally accepted practise to make a small donation.

Please donate via PayPal to donate@opencart.com

//--> 



<!-- Theme created by Welford Media for OpenCart 2.0 www.welfordmedia.co.uk -->

</div> <!-- End .blur -->

</div> <!-- End #bt_container -->

</body></html>