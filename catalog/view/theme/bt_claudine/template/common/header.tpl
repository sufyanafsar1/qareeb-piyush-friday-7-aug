<?php

$user_agent = getenv("HTTP_USER_AGENT");
if(strpos($user_agent, "Win") !== FALSE){
	$os = "Windows";
}elseif(strpos($user_agent, "Mac") !== FALSE){
	$os = "Mac";
}else{
	$os = "other";
}
?>
<?php global $config; global $request; ?>
<?php
	$boss_manager = array(
		'option' => array(
			'bt_scroll_top' => true,
			'animation' 	=> true,			
		),
		'layout' => array(
			'mode_css'   => 'wide',
			'box_width' 	=> 1200,
			'h_mode_css'   => 'inherit',
			'h_box_width' 	=> 1200,
			'f_mode_css'   => 'inherit',
			'f_box_width' 	=> 1200
		),
		'status' => 1
	);
?>
<?php if($config->get('boss_manager')){
		$boss_manager = $config->get('boss_manager'); 
	}else{
		$boss_manager = $boss_manager;
	} 
	$header_link = isset($boss_manager['header_link'])?$boss_manager['header_link']:''; 
	$option = isset($boss_manager['option'])?$boss_manager['option']:''; 
	$loading = isset($boss_manager['option']['loading'])?$boss_manager['option']['loading']:''; 
?>
<!DOCTYPE html>
<!--[if IE]><![endif]-->
<!--[if IE 8 ]><html dir="<?php echo $direction; ?>" lang="<?php echo $lang; ?>" class="ie8"><![endif]-->
<!--[if IE 9 ]><html dir="<?php echo $direction; ?>" lang="<?php echo $lang; ?>" class="ie9"><![endif]-->
<!--[if (gt IE 9)|!(IE)]><!-->
<html dir="<?php echo $direction; ?>" lang="<?php echo $lang; ?>">
<!--<![endif]-->
<head>
<meta charset="UTF-8" />
<meta name="viewport" content="width=device-width, initial-scale=1">
<title><?php echo $title; ?></title>
<base href="<?php echo $base; ?>" />
<?php if ($description) { ?>
<meta name="description" content="<?php echo $description; ?>" />
<?php } ?>
<?php if ($keywords) { ?>
<meta name="keywords" content= "<?php echo $keywords; ?>" />
<?php } ?>
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<?php foreach ($links as $link) { ?>
<link href="<?php echo $link['href']; ?>" rel="<?php echo $link['rel']; ?>" />
<?php } ?>
<script src="catalog/view/javascript/jquery/jquery-2.1.1.min.js" type="text/javascript"></script>
<link href="catalog/view/javascript/bossthemes/bootstrap/css/bootstrap.css" rel="stylesheet" media="screen" />
<link href="catalog/view/theme/bt_claudine/stylesheet/customstyle.css" rel="stylesheet"  />
<script src="catalog/view/javascript/search.js" type="text/javascript"></script>
<script src="catalog/view/javascript/bossthemes/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
<?php if ($os != 'Mac') { ?>
<script type="text/javascript" src="catalog/view/javascript/bossthemes/jquery.smoothscroll.js"></script>
<?php }?>
<script src="catalog/view/javascript/bossthemes/jquery.jgrowl.js" type="text/javascript"></script>
<link href="catalog/view/javascript/bossthemes/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
<link href="//fonts.googleapis.com/css?family=Open+Sans:400,400i,300,700" rel="stylesheet" type="text/css" />
<link href="catalog/view/theme/<?php echo $config->get('config_template'); ?>/stylesheet/stylesheet.css" rel="stylesheet">
<link rel="stylesheet" type="text/css" href="catalog/view/theme/<?php echo $config->get('config_template'); ?>/stylesheet/bossthemes/bt_stylesheet.css" />
<link rel="stylesheet" type="text/css" href="catalog/view/theme/<?php echo $config->get('config_template'); ?>/stylesheet/bossthemes/responsive.css" />
<?php if(isset($boss_manager['option']['animation'])&&($boss_manager['option']['animation'])){ ?>
<link rel="stylesheet" type="text/css" href="catalog/view/theme/<?php echo $config->get('config_template'); ?>/stylesheet/bossthemes/cs.animate.css" />
<?php } ?>
<link rel="stylesheet" type="text/css" href="catalog/view/theme/<?php echo $config->get('config_template'); ?>/stylesheet/bossthemes/jquery.jgrowl.css" />
<link rel="stylesheet" type="text/css" href="catalog/view/theme/<?php echo $config->get('config_template'); ?>/stylesheet/bossthemes/bootstrap-select.css" />
<link rel="stylesheet" type="text/css" href="catalog/view/javascript/bossthemes/video/video.css">
<!--Start Mobile Theme CSS-->
 <link rel="stylesheet" type="text/css" href="catalog/view/theme/bt_claudine/template/common/mobile_theme/style_swipemenu.css">
 <!--End Mobile Theme CSS-->
<?php foreach ($styles as $style) { ?>
<link href="<?php echo $style['href']; ?>" type="text/css" rel="<?php echo $style['rel']; ?>" media="<?php echo $style['media']; ?>" />
<?php } ?>
<script type="text/javascript" src="catalog/view/javascript/bossthemes/cs.bossthemes.js"></script>
<?php foreach ($scripts as $script) { ?>
<script src="<?php echo $script; ?>" type="text/javascript"></script>
<?php } ?>
<script type="text/javascript" src="catalog/view/javascript/bossthemes/getwidthbrowser.js"></script>
<script type="text/javascript" src="catalog/view/javascript/bossthemes/jquery.appear.js"></script>
<script type="text/javascript" src="catalog/view/javascript/bossthemes/bootstrap-select.js"></script>
<script type="text/javascript" src="catalog/view/javascript/bossthemes/parallax/jquery.parallax-1.1.3.js"></script>
<script type="text/javascript" src="catalog/view/javascript/bossthemes/common.js"></script>
<script type="text/javascript" src="catalog/view/javascript/bossthemes/google-map.js"></script>
<script type="text/javascript" src="catalog/view/javascript/bossthemes/ipmapper.js"></script>

<?php if (isset($analytics) && !empty($analytics)) { ?>
<?php foreach ($analytics as $analytic) { ?>
<?php echo $analytic; ?>
<?php } } ?>
<?php if(isset($this->request->get['route'])){$route1 = $this->request->get['route'];}else{$route1 ="";}
	if(isset($route1) && ($route1== "common/home" || $route1=="") && $loading){ ?>
		<script type="text/javascript"><!--
			$(document).ready(function() {
				$(".bt-loading").fadeOut("1500", function () {
					$('#bt_loading').css("display", "none");
				});
			});
		//--></script>
	<?php }else{ ?>
		<script type="text/javascript"><!--
		$(document).ready(function() {
		$('#bt_loading').css("display", "none");
		});
		//--></script>
	<?php } ?>

<style>
	#bt_loading{position:fixed; width:100%; height:100%; z-index:999; background:#fff}
	.bt-loading{
		height: 29px;
		left: 50%;
		margin-left: -120px;
		margin-top: -29px;
		position: absolute;
		top: 50%;
		width: 240px;
		z-index: 9999;
	}
</style>
<?php 	
	if (isset($option['sticky_menu']) && $option['sticky_menu']) { ?>
	
	<script type="text/javascript"><!--
	$(window).scroll(function() {
			var height_header = $('#top').height() + $('header').height();  			
			if($(window).scrollTop() > height_header) {
				
			//	$('header').addClass('boss_scroll');
             //   $('#top').addClass('boss_scroll');
			} else {
				$('header').removeClass('boss_scroll');
                $('#top').removeClass('boss_scroll');
			}
		});
	//-->
	</script>
	<!--[if IE 8]> 
	<script type="text/javascript">
	$(window).scroll(function() {
			var height_header = $('#bt_header').height();  			
			if($('html').scrollTop() > height_header) {				
				$('header').addClass('boss_scroll');
			} else {
				$('header').removeClass('boss_scroll');
			}
		});
	</script>
	<![endif]-->
<?php } ?>
<link href='https://fonts.googleapis.com/css?family=Lato:400,700' rel='stylesheet' type='text/css'>
<link href='https://fonts.googleapis.com/css?family=Josefin+Sans:400,600,700' rel='stylesheet' type='text/css'>
<link href='https://fonts.googleapis.com/css?family=Open+Sans:400,600,700' rel='stylesheet' type='text/css'>
<?php 
	$b_style = '';$h_style = ''; $status=0; $h_mode_css = ''; $h_mode_css = '';
	
	if(isset($boss_manager) && !empty($boss_manager)){
		$layout = $boss_manager['layout'];
		$status = $boss_manager['status']; 
	
		if(isset($layout['mode_css']) && $layout['mode_css']=='wide'){
			$mode_css = 'bt-wide';
		}else{
			$mode_css = 'bt-boxed';
			$b_style = (!empty($layout['box_width']))?'style="max-width:'.$layout['box_width'].'px"':'';
		}
		if(isset($layout['h_mode_css']) && $layout['h_mode_css']=='boxed'){
			$h_mode_css = 'bt-hboxed';
			$h_style = (!empty($layout['h_box_width']))?'style="max-width:'.$layout['h_box_width'].'px"':'';
		}else{
			$h_mode_css = '';
		}
	
	}
?>
<?php
if($status){
	include "catalog/view/theme/".$config->get('config_template')."/template/bossthemes/boss_color_font_setting.php";
} ?>
<?php if($direction=='rtl'){ ?>
<link rel="stylesheet" type="text/css" href="catalog/view/theme/<?php echo $config->get('config_template'); ?>/stylesheet/bossthemes/stylertl.css" />
<?php } ?>
<?php if ($os != 'Mac') { ?>
<style>
::-webkit-scrollbar {width: 8px; height: 8px; border: 3px solid #eee;  }
::-webkit-scrollbar-button:start:decrement, ::-webkit-scrollbar-button:end:increment {display: block; height: 10px; background:  #eee}
::-webkit-scrollbar-track {background: #eee; -webkit-border-radius: 10px; border-radius:10px; -webkit-box-shadow: inset 0 0 4px rgba(0,0,0,.2)}
::-webkit-scrollbar-thumb {height: 50px; width: 50px; background: rgba(0,0,0,.5); -webkit-border-radius: 8px; border-radius: 8px; -webkit-box-shadow: inset 0 0 4px rgba(0,0,0,.1);}
</style>
<?php } ?>
<style type="text/css">
   .mobileShow { display: none;}
   /* Smartphone Portrait and Landscape */
   @media only screen
   and (min-device-width : 320px)
   and (max-device-width : 480px){ .mobileShow { display: inline;}}
</style>
</head>

<?php 
	if(isset($request->get['route'])){
		$route = $request->get['route'];
	}else{
		$route ="";
	}
	if(isset($route) && ($route== "common/home" || $route=="")){
		$home_page='bt-home-page';
	}else{
		$home_page="bt-other-page";
	}
?>
<body class="<?php echo $home_page; ?> <?php if($direction=='rtl') echo 'rtl';?>">

<?php if($loading){ ?>
<div id="bt_loading"><div class="bt-loading">
	<div id="fountainG">
		<div id="fountainG_1" class="fountainG"></div>
		<div id="fountainG_2" class="fountainG"></div>
		<div id="fountainG_3" class="fountainG"></div>
		<div id="fountainG_4" class="fountainG"></div>
		<div id="fountainG_5" class="fountainG"></div>
		<div id="fountainG_6" class="fountainG"></div>
		<div id="fountainG_7" class="fountainG"></div>
		<div id="fountainG_8" class="fountainG"></div>
	</div>
</div></div>
<?php } ?>
<div class="mobileShow">
<pre><p>Download Careeb App   <a href="https://play.google.com/store/apps/details?id=com.rs.careebstore&amp;hl=en" target="_blank"><img src="https://careeb.com/image/catalog/banner/andriod-icon-sm.png"></a>&nbsp;&nbsp;<a href="https://itunes.apple.com/st/app/salatty/id1200691958?mt=8" target="_blank"><img src="https://careeb.com/image/catalog/banner/ios-icon.png"></a></p></pre></div>
<div id="bt_container" class="<?php  echo $mode_css;?>" <?php echo $b_style;?>>
<div id="bt_header" class="<?php echo $h_mode_css;?>" <?php echo $h_style;?>>
<div class="mobile_fixed" id="top_menu">
<nav id="top">
<div class="container">
<div class="row">
	<div class="b_mobile">	
			<a class="open-panel"><i class="fa fa-bars"></i></a>
		<div class="menu_mobile">	
			<a class="close-panel"><i class="fa fa-times-circle"></i></a>
			<?php $header_mobile = $config->get('boss_manager_header_mobile'); ?>
			<?php if(isset($header_mobile['status']) && $header_mobile['status'] && !$logged) { ?>
			<?php echo html_entity_decode(isset($header_mobile['content'][$config->get('config_language_id')])?$header_mobile['content'][$config->get('config_language_id')]:'',ENT_QUOTES, 'UTF-8'); ?>
			<?php } ?>
			<div class="language_currency">
				<?php if(isset($header_link['currency']) && $header_link['currency']){ echo $currency;} ?>
				<?php if(isset($header_link['language']) && $header_link['language']){ echo $language;} ?>
			</div>	
			<div class="top-links">
			  <ul class="list-inline">
				<?php if(isset($header_link['phone']) && $header_link['phone']){ ?>	<li class="contact"><a href="<?php echo $contact; ?>"><i class="fa fa-phone"></i></a> <span class="hidden-sm hidden-md telephoneNo"><?php echo $telephone; ?></span></li> <?php } ?>
				<?php if(isset($header_link['my_account']) && $header_link['my_account']){ ?><li class="dropdown"><a href="<?php echo $account; ?>" title="<?php echo $text_account; ?>" class="dropdown-toggle" data-toggle="dropdown"><i class="fa fa-user"></i><span><?php echo $text_account; ?></span><i class="fa fa-angle-down"></i></a>
				  <ul class="dropdown-menu dropdown-menu-right">
					<?php if ($logged) { ?>
					<li><a href="<?php echo $account; ?>"><?php echo $text_account_deatil; ?></a></li>
                <li><a href="<?php echo $wishlist; ?>"><?php echo $text_wishlist; ?></a></li>			
				
                <li><a href="<?php echo $order; ?>"><?php echo $text_order; ?></a></li>
			    <hr class="custom-hr"/>
				<li><a href="<?php echo $logout; ?>"><?php echo $text_logout; ?></a></li>
					<?php } else { ?>
					<li><a href="<?php echo $register; ?>"><?php echo $text_register; ?></a></li>
					<li><a href="<?php echo $login; ?>"><?php echo $text_login; ?></a></li>
					<?php } ?>
				  </ul>
				</li>
				<?php } ?>
			</ul>
			</div>
			<?php 
				if(isset($option['use_menu']) && $option['use_menu'] == 'megamenu'){?>
			<?php	echo isset($btmainmenu)?$btmainmenu:''; 
			}else{
			?>	
			<?php if ($categories) { ?>	
			  <nav id="menu1" class="navbar">
				<div class="navbar-header">
				  <button type="button" class="btn btn-navbar navbar-toggle" data-toggle="collapse" data-target=".navbar-ex1-collapse"><i class="fa fa-bars"></i></button>
				  <span id="category" class="visible-xs"><?php echo $text_category; ?></span>
				</div>
				<div class="collapse navbar-collapse navbar-ex1-collapse">
				  <ul class="nav navbar-nav">
					<?php foreach ($categories as $category) { ?>
					<?php if ($category['children']) { ?>
					<li class="dropdown"><a href="<?php echo $category['href']; ?>" class="dropdown-toggle" data-toggle="dropdown"><?php echo $category['name']; ?></a>
					  <div class="dropdown-menu">
						<div class="dropdown-inner">
						  <?php foreach (array_chunk($category['children'], ceil(count($category['children']) / $category['column'])) as $children) { ?>
						  <ul class="list-unstyled">
							<?php foreach ($children as $child) { ?>
							<li><a href="<?php echo $child['href']; ?>"><?php echo $child['name']; ?></a></li>
							<?php } ?>
						  </ul>
						  <?php } ?>
						</div>
						<a href="<?php echo $category['href']; ?>" class="see-all"><?php echo $text_all; ?> <?php echo $category['name']; ?></a> </div>
					</li>
					<?php } else { ?>
					<li><a href="<?php echo $category['href']; ?>"><?php echo $category['name']; ?></a></li>
					<?php } ?>
					<?php } ?>
				  </ul>
				</div>
			  </nav>
	
			<?php } } ?>
		</div>
	</div>
	<?php //if(isset($boss_login)){
			//echo $boss_login;
		//}
	?>
    <div class="mobile_logo mobile-app_opp">
     <?php if ($logo) { ?>
		  <a href="<?php echo $home; ?>"><img src="<?php echo $logo; ?>" title="<?php echo $name; ?>" class = "logo_resize" alt="<?php echo $name; ?>" /></a>
		  <?php } else { ?>
		  <h1><a href="<?php echo $home; ?>"><?php echo $name; ?></a></h1>
		  <?php } ?>
    </div>
   <?php if($_SERVER['SERVER_NAME'] =="dammam.careeb.com" || $_SERVER['SERVER_NAME']=="jeddah.careeb.com" || $_SERVER['SERVER_NAME']=="riyadh.careeb.com"){ ?>
    <div class="top_left_location col-sm-4 col-xs-12 ">
        <?php 
         $cityName;
         
          if($_SERVER['SERVER_NAME']  == "dammam.careeb.com")
          {
            $cityName = "Dammam"; 
          }
          else if($_SERVER['SERVER_NAME'] == "jeddah.careeb.com")
          {
            $cityName = "Jeddah"; 
          }
          else if($_SERVER['SERVER_NAME'] == "riyadh.careeb.com")
          {
            $cityName = "Riyadh"; 
          }
         ?>
            <input type="text" style="width:68%; padding:10px 0px 7px 1px" readonly="true" value="Store location , <?php echo $cityName; ?>" placeholder="Store location,Riyadh" />
        
       
           <input id="btnLocation" value="Change" class="btn button_login btn-primary-location" type="button"/>
        
    </div>
    <?php } ?>
	<div class="top_right col-sm-8 col-xs-12 pull-right">
     
		<div class="top-links">
		  <ul class="list-inline">
			<?php if(isset($header_link['phone']) && $header_link['phone']){ ?>	<li class="contact"><a href="<?php echo $contact; ?>"><i class="fa fa-phone"></i></a> <span class="hidden-xs hidden-sm hidden-md telephoneNo"><?php echo $telephone; ?></span></li> <?php } ?>
            <?php if(isset($header_link['language']) && $header_link['language']){ ?>	<li class="contact"><?php echo $language; ?></li> <?php } ?>
			<?php if(isset($header_link['my_account']) && $header_link['my_account']){ if ($logged) { ?>
            <li class="dropdown"><a href="<?php echo $account; ?>" title="<?php echo $text_login; ?>" class="dropdown-toggle" data-toggle="dropdown"><i class="fa fa-user"></i><span><?php echo $text_account;   ?></span><i class="fa fa-angle-down"></i></a>
			 <?php } else{ ?>
             <li class="dropdown"><a href="javascript:void(0);" id="signIn"><?php echo $text_register .'/'.$text_login; ?></a>
             <?php } ?>
              <ul class="dropdown-menu dropdown-menu-right">
				<?php if ($logged) { ?>
				<li><a href="<?php echo $account; ?>"><?php echo $text_account_deatil; ?></a></li>
                <li><a href="<?php echo $wishlist; ?>"><?php echo $text_wishlist; ?></a></li>			
				
                <li><a href="<?php echo $transaction; ?>"><?php echo $text_store_credit; ?></a></li>
                <li><a href="<?php echo $order; ?>"><?php echo $text_order; ?></a></li>
			    <hr class="custom-hr"/>
				<li><a href="<?php echo $logout; ?>"><?php echo $text_logout; ?></a></li>
				<?php } else { ?>
			
                <li><a href="javascript:void(0);" id="signIn"><?php echo $text_register .'/'.$text_login; ?></a></li>
				<?php } ?>
			  </ul>
			</li>
			<?php } ?>
		<?php if(isset($header_link['cart_mini']) && $header_link['cart_mini']){ ?>	<li class="contact"><?php echo $cart; ?></li> <?php } ?>
        
          </ul>
		</div>
    </div>
</div>
</div>
</nav>
</div>
<div class="container table-conf">
	<div class="row">
		<div class="bt-content-menu" style="float: left; width: 100%; clear: both; height: 1px;"></div>
	</div>
</div>
<header>
<div class="container">
<div class="row">
	<div class="header_left col-sm-2 col-xs-12 pull-left mobile-app">
		<?php if(isset($header_link['logo']) && $header_link['logo']){ ?>
		<div id="logo">
		  <?php if ($logo) { ?>
		  <a href="<?php echo $home; ?>"><img src="<?php echo $logo; ?>" title="<?php echo $name; ?>" alt="<?php echo $name; ?>" /></a>
		  <?php } else { ?>
		  <h1><a href="<?php echo $home; ?>"><?php echo $name; ?></a></h1>
		  <?php } ?>
		</div>
		<?php } ?>
	</div>
	<div class="header_right col-sm-10 col-xs-12 pull-right mobile_margin_top">	
		<?php 
			if(isset($header_link['search']) && $header_link['search']){
				echo $search; 
			} 
		?>
		<?php 
		if(isset($option['use_menu']) && $option['use_menu'] == 'megamenu'){
			echo isset($btmainmenu)?$btmainmenu:''; 
		}else{
		?>	
		<?php if ($categories) { ?>	
		  <nav id="menu2" class="navbar">
			<div class="navbar-header">
			  <button type="button" class="btn btn-navbar navbar-toggle" data-toggle="collapse" data-target=".navbar-ex1-collapse"><i class="fa fa-bars"></i></button>
			  <span id="category" class="visible-xs"><?php echo $text_category; ?></span>
			</div>
			<div class="collapse navbar-collapse navbar-ex1-collapse">
			  <ul class="nav navbar-nav">
				<?php foreach ($categories as $category) { ?>
				<?php if ($category['children']) { ?>
				<li class="dropdown"><a href="<?php echo $category['href']; ?>" class="dropdown-toggle" data-toggle="dropdown"><?php echo $category['name']; ?></a>
				  <div class="dropdown-menu">
					<div class="dropdown-inner">
					  <?php foreach (array_chunk($category['children'], ceil(count($category['children']) / $category['column'])) as $children) { ?>
					  <ul class="list-unstyled">
						<?php foreach ($children as $child) { ?>
						<li><a href="<?php echo $child['href']; ?>"><?php echo $child['name']; ?></a></li>
						<?php } ?>
					  </ul>
					  <?php } ?>
					</div>
					<a href="<?php echo $category['href']; ?>" class="see-all"><?php echo $text_all; ?> <?php echo $category['name']; ?></a> </div>
				</li>
				<?php } else { ?>
				<li><a href="<?php echo $category['href']; ?>"><?php echo $category['name']; ?></a></li>
				<?php } ?>
				<?php } ?>
			  </ul>
			</div>
		  </nav>
	
	<?php } } ?>
	</div>	  
	</div>	  
	</div>	 
    
</header>

<div class="boss-new-position">
	<div class="container"><div class="row"><?php echo isset($btslideshow)?$btslideshow:''; ?></div></div>
</div> <!-- End #bt_header -->
</div> <!-- End #bt_header -->

<div class="boss_content_blur">
