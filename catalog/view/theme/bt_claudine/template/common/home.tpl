<?php if(isset($_COOKIE['tracking_city'])){ ?>
  <script type="text/javascript">
    //window.location.assign('http://<?php echo $_COOKIE['tracking_city']; ?>.careeb.com');
  </script>
<?php } ?>
<?php echo $header; ?>
<?php $column_left  = trim($column_left);$column_right  = trim($column_right); ?>

<?php if ($column_left || $column_right) { ?>
<div class="container left-menucategory-fixed"><div class="row">
<?php } ?>

 <?php echo $column_left; ?><?php echo $column_right; ?>
 
 <?php if ($column_left && $column_right) { ?>
 <div id="content" class="col-sm-6">
 <?php }else if ($column_left || $column_right) { ?>
 <div id="content" class="col-sm-9">
 <?php }else { ?>
  <div class="row">
  <div  class="col-sm-1 col-sm-rtl"></div>
 <div id="content" class="col-sm-10 col-sm-rtl">
 <?php } ?>
  <?php echo $content_top; ?>
  
  <?php if ($column_left && $column_right) { ?>
  </div>
 <?php }else if ($column_left || $column_right) { ?>
  </div>
 <?php }else { ?>
  </div> </div> 
 <?php } ?>


<?php if ($column_left || $column_right) { ?>
</div></div>
<?php } ?>
  <?php echo $content_bottom; ?>
<?php   $lang  = $_SESSION['default']['language']; ?>
<div class="modal fade col-sm-10 custom-margin-1" id="myModal">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
    <div class="modal-header">
       <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                        
    </div>
      <div class="modal-body model-body-city">
       <div id="subscribe-top">
            <?php if($lang =="en") { ?>
            
            <h1><?php echo $text_select_city; ?></h1>
            <?php  } else{ ?>
                	<img src="image/city_name.png"  alt"<?php echo $text_select_city; ?>" />
            <?php } ?>

        </div>
        <input type="hidden" id="language" value="<?php echo $_SESSION['default']['language'];?>" />

      <div class="product-thumb product-thumb-custom">
        	<div class="image">
        		<img src="image/dammam.jpg" class="img_city" alt"<?php echo $text_dammam; ?>" onclick="setCity('dammam')"  /> 
                                 
        	</div>
		<div class="caption">
			<div class="name">
            <?php if($lang =="en") { ?>
			<?php echo $text_dammam; ?>
             <?php  } else{ ?>
                	<img src="image/dammam_name.png"  alt"<?php echo $text_dammam; ?>" />
            <?php } ?>
			</div>										
		</div>						
								
        </div>
        <div class="product-thumb product-thumb-custom ">
        	<div class="image">
        		 <img src="image/jeddah.jpg" class="img_city" alt"<?php echo $text_jeddah; ?>" onclick="setCity('jeddah')"  /> 
                                 
        	</div>
		<div class="caption">
			<div class="name">
            <?php if($lang =="en") { ?>
		<?php echo $text_jeddah; ?> 
        <?php  } else{ ?>
                	<img src="image/jeddah_name.png"  alt"<?php echo $text_jeddah; ?>" />
            <?php } ?>
			</div>										
		</div>						
								
        </div>
        <div class="product-thumb product-thumb-custom">
        	<div class="image">
        	<img src="image/riyadh.jpg" class="img_city" alt"<?php echo $text_riyadh; ?>" onclick="setCity('riyadh')"  />  
                                 
        	</div>
		<div class="caption">
			<div class="name">
             <?php if($lang =="en") { ?>
			<?php echo $text_riyadh; ?>
             <?php  } else{ ?>
                	<img src="image/riyadh_name.png"  alt"<?php echo $text_riyadh; ?>" />
            <?php } ?>
			</div>										
		</div>						
								
        </div>
        <div id="error"></div>
        <div class="clear"></div>
      </div>
     <div class="modal-footer">
       
      </div>
    </div>
  </div>
</div>
<script type="text/javascript">
function setCity(cityVal)
{
    var language =  $('#language').val();
    var city = cityVal;
     if (language != "" && city != "") {
      $.ajax({
        method: "POST",
        url: "index.php?route=common/cookies/index",
        data: { city: city, lang: language }
      })
      .done(function( msg ) {
        console.log(msg);
        window.location.reload();
        $('#myModal').modal('hide');
      }); 
    }else{
      $('#error').html('Please select the valid values');
    }
}

$("#btnLocation").click(function(){
  
   
    $("#myModal").modal('show');
  });
 

</script>
<?php  if (($_SERVER['SERVER_NAME'] =="careeb.com" ||$_SERVER['SERVER_NAME'] =="www.careeb.com" ) && !isset($_COOKIE['tracking_city'])){ ?>
<script type="text/javascript">
$(document).ready(function(){
   
    
   // $("#myModal").modal('show');
  });
 

</script>
<?php } ?>
<?php echo $footer; ?>