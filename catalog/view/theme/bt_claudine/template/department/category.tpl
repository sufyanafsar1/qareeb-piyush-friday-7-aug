<?php echo $header; ?>
<?php global $config; ?>



<div class="bt-breadcrumb">

<div class="container">

  <ul class="breadcrumb">

	<li class="b_breadcrumb"><?php echo $heading_title; ?></li>

    <?php foreach ($breadcrumbs as $breadcrumb) { ?>

    <li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>

    <?php } ?>

  </ul>

  </div>

</div>

<div class="container">

  <div class="row">
  
  <div class="categorylist">
    <?php
        
    if(count($categories) > 0) { 
        foreach($categories as $category){ 
            $last_cat_id = $category['id'];
    ?>
    
 <div class="product-layout product-grid col-md-4 col-sm-6 col-xs-12">
  <div class="product-thumb">
    <div class="image">
      
            <img src="<?php echo $category['image']; ?>" title="<?php echo $category['name']; ?>" />
        
	</div>         	
    <div class="small_detail">
        <div class="name">
           <?php echo $category['name']; ?>
        </div>

     

      <div class="button-group">
        	<div  class="cart ">
				<a href="<?php echo $category['href']; ?>" class="btn btn-primary"> <?php echo $view_all ?></a>
            </div>
      </div>
    </div>
  </div>
</div>
    <?php } ?>
   <div class="show_more_main" id="show_more_main<?php echo $last_cat_id; ?>">
         <div class="col-md-6 col-sm-6 col-xs-12"></div>
         <div class="col-md-6 col-sm-6 col-xs-12 ">
        
       
        <input type="button" value="<?php echo $button_load_more ?>" class="btn btn-primary " id="show_more" />
        <input type="hidden" value="<?php echo $last_cat_id; ?>" id="show_more_value" />
        <span class="loding" style="display: none;"><span class="loding_txt">Loading....</span></span>
        </div>
   </div> 
         
         
    
    <?php } ?>
</div>
  </div>

	

</div>
<script>



$(document).ready(function(){
    
    $("#show_more").click(function(){
        var lastCatId = $("#show_more_value").val();
       
         var categories ="";
         var lastCategoryId;
        	$.ajax({
			url: 'index.php?route=department/category/getMoreCategories/',
			type: 'post',
			data: 'lastid='+lastCatId,
			dataType: 'json',
			success: function(json) {
	
			 if (json['success']) {
					$('.show_more').hide();
                    $('.loding').show();
                    
                    $.each(json['categories'], function(index, itemData) {
                        
                        lastCategoryId = itemData['id'];
                     categories +='<div class="product-layout product-grid col-md-4 col-sm-6 col-xs-12">';
                     categories +='<div class="product-thumb">';
                     categories +='<div class="image">';      
                     categories +='<img src="'+itemData['image']+ '" title="'+itemData['name']+ '" />';
                     categories +='</div>';         	
                     categories +=' <div class="small_detail">';
                     categories +=' <div class="name"> ' + itemData['name'] + '</div>';
                     categories +='<div class="button-group">';
                     categories +='<div  class="cart ">';
                     categories +='<a href="'+itemData['href']+'" class="btn btn-primary"> '+ json['view_all'] +'</a> ';
                     categories +='</div>';
                     categories +='</div>';
                     categories +='</div>';
                     categories +='</div>';
                     categories +='</div>';
                           
                    }); //each
                   if(json['total_rows'] > 6 )
                   {
                        categories +='<div class="show_more_main" id="show_more_main'+lastCategoryId+'">';
                        categories +='<div class="col-md-6 col-sm-6 col-xs-12"></div>';
                        categories +='<div class="col-md-6 col-sm-6 col-xs-12">';           
                        categories +=' <input type="button" onclick="getMoreCategories();" value="'+ json['button_load_more'] +'" class="btn btn-primary" id="show_more" />';
                        categories +=' <input type="hidden" value="'+ lastCategoryId +'" id="show_more_value" />';
                        categories +='<span class="loding" style="display: none;"><span class="loding_txt">Loading....</span></span>';
                        categories +='</div>';
                        categories +='</div>';                   
                   } //total rows 
                   
                   $('#show_more_main'+lastCatId).remove();
                   $('.categorylist').append(categories);
              				
 				}//json success
			}//success
		}); // AJax       
        
    }); //click
    
}); //ready


 function getMoreCategories()
 {
    var lastCatId = $("#show_more_value").val();
       
         var categories ="";
         var lastCategoryId;
        	$.ajax({
			url: 'index.php?route=department/category/getMoreCategories/',
			type: 'post',
			data: 'lastid='+lastCatId,
			dataType: 'json',
			success: function(json) {
	
			 if (json['success']) {
					$('.show_more').hide();
                    $('.loding').show();
                    
                    $.each(json['categories'], function(index, itemData) {
                        
                        lastCategoryId = itemData['id'];
                     categories +='<div class="product-layout product-grid col-md-4 col-sm-6 col-xs-12">';
                     categories +='<div class="product-thumb">';
                     categories +='<div class="image">';      
                     categories +='<img src="'+itemData['image']+ '" title="'+itemData['name']+ '" />';
                     categories +='</div>';         	
                     categories +=' <div class="small_detail">';
                     categories +=' <div class="name"> ' + itemData['name'] + '</div>';
                     categories +='<div class="button-group">';
                     categories +='<div  class="cart ">';
                    categories +='<a href="'+itemData['href']+'" class="btn btn-primary"> '+ json['view_all'] +'</a> ';
                     categories +='</div>';
                     categories +='</div>';
                     categories +='</div>';
                     categories +='</div>';
                     categories +='</div>';
                           
                    }); //each
                   if(json['total_rows'] > 6 )
                   {
                        categories +='<div class="show_more_main" id="show_more_main'+lastCategoryId+'">';
                        categories +='<div class="col-md-6 col-sm-6 col-xs-12"></div>';
                        categories +='<div class="col-md-6 col-sm-6 col-xs-12">';           
                        categories +=' <input type="button" onclick="getMoreCategories();" value="'+ json['button_load_more'] +'" class="btn btn-primary" id="show_more" />';
                        categories +=' <input type="hidden" value="'+ lastCategoryId +'" id="show_more_value" />';
                        categories +='<span class="loding" style="display: none;"><span class="loding_txt">Loading....</span></span>';
                        categories +='</div>';
                        categories +='</div>';                   
                   } //total rows 
                   
                   $('#show_more_main'+lastCatId).remove();
                   $('.categorylist').append(categories);
              				
 				}//json success
			}//success
		}); // AJax       
    
    
 }

</script>

<?php echo $footer; ?>