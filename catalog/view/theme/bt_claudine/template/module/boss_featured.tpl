<?php   if(!empty($product_data)){ 
?>

<?php if($show_slider){ $row = $num_row; $sliderclass = 'slider'; }else {$row = 1; $sliderclass = 'nslider';} ?>
<?php (!empty($product_data['mainproduct']))? $prolarge = 'prolarge': $prolarge = 'nprolarge'; ?>
<?php (!empty($column))?$class_css = 'bt-custom-column bt-'.$prolarge.'-'.$sliderclass:$class_css = 'bt-custom-content bt-'.$prolarge.'-'.$sliderclass; ?>
<?php if($column == 'column'){ $direction = 'top';}else{$direction = 'left';} ?>
<div class="boss-custom-pro <?php echo $class_css; ?>" id="boss_min_height_<?php echo $module; ?>">	
	<div class="box-heading"><div class="title"><h3><?php echo $title; ?></h3></div></div>
	<div class="box-content bt-product-content">
	<?php if(!empty($product_data['mainproduct'])){ ?>
	<?php $mainproduct = $product_data['mainproduct'];?>
		<div class="bt-product-large">
		<section class="bt-item-large">
		<div class="product-thumb">
			<?php if ($mainproduct['thumb']) { ?>
			<div class="image">
				<a id="image<?php echo $module;?>" href="<?php echo $mainproduct['href']; ?>" title="<?php echo properName($mainproduct['name']); ?>">
					<img src="<?php echo $mainproduct['thumb']; ?>" alt="<?php echo properName($mainproduct['name']); ?>" />
				</a>
                                 <?php if ($mainproduct['special']) { ?>
                                    
                                    <div class="sale-box">
                                <div class="sale-label">
                                    <b><?php echo $mainproduct['discount'];?></b></div>
                                 </div>
                                 <?php } ?> 

                                 <div class="heart-box">
                                <div class="heart-label">
                                   <a class="wishlist1"  onclick="wishlist_cat_add('<?php echo $product['product_id']; ?>');"><i class="fa fa-heart"></i></a>
                                   </div>
                                 </div>
                                 
			</div>
			<?php } ?>
			<?php if($column!='column' && !empty($mainproduct['images']) && $show_addition_image){ ?>
			<div class="addition_image">
			<div id="addition_image<?php echo $module; ?>">
			<?php foreach ($mainproduct['images'] as $addition_image) { ?>
				<div class="items"><a onclick="loadImage('image<?php echo $module; ?>','<?php echo $addition_image['popup']; ?>');" href="javascript:void(0)" title="<?php echo properName($mainproduct['name']); ?>" ><img src="<?php echo properName($addition_image['thumb']);?>" title="<?php echo properName($mainproduct['name']); ?>" alt="<?php echo properName($mainproduct['name']); ?>" /></a></div>
			<?php } ?>
			</div>
			<a id="add_img_next_<?php echo $module; ?>" class="btn-nav-center prev nav_thumb" href="javascript:void(0)" title="prev">Prev</a>
			<a id="add_img_prev_<?php echo $module; ?>" class="btn-nav-center next nav_thumb" href="javascript:void(0)" title="next">Next</a>
			</div>
			<?php } ?>
			<div class="name"><a class="boss-name" href="javascript:viod(0);" title="<?php echo properName($mainproduct['name']); ?>"><?php echo properName($mainproduct['name']); ?></a></div>
			<?php //if ($mainproduct['rating']) { ?>				
				 <div class="rating">
				  <?php for ($i = 1; $i <= 5; $i++) { ?>
				  <?php if ($mainproduct['rating'] < $i) { ?>
				  <span class="fa fa-stack"><i class="fa fa-star-o fa-stack-2x"></i></span>
				  <?php } else { ?>
				  <span class="fa fa-stack"><i class="fa fa-star fa-stack-2x"></i></span>
				  <?php } ?>
				  <?php } ?>
				</div>
				
			<?php //} ?>		
			<?php if ($mainproduct['price']) { ?>
				<div class="price">
				<?php if (!$mainproduct['special']) { ?>
					<?php echo $mainproduct['price']; ?>
				<?php } else { ?>
					<span class="price-old"><?php echo $mainproduct['price']; ?></span> <span class="price-new"><?php echo $mainproduct['special']; ?></span>
				<?php } ?>
				</div>
			<?php } ?>
			
			<div class="boss-button">
			<div class="cart">
			<?php if (count($data['product_data']['cart_data'])): ?>
				<?php $count = 0; ?>
				<?php foreach ($data['product_data']['cart_data'] as $key => $value): ?>
					<?php if ($value['product_id'] == $mainproduct['product_id']): ?>
						<?php $count = 1; 
							$quantity = $value['quantity'];
							$cart_id = $value['cart_id'];
							break;		 ?>
					<?php endif ?>
				<?php endforeach ?>
				<?php if ($count == 1): ?>
					<button onclick="changeQty('<?php echo 'select-number'.$cart_id;?>',0); return false;" type="button" class="decrease"><i class="fa fa-minus"></i></button>
      				<input id="select-number<?php echo $cart_id; ?>" type="text" name="quantity[<?php echo $cart_id; ?>]" value="<?php echo $quantity; ?>" size="1" class="form-control1" />
					<button onclick="changeQty('<?php echo 'select-number'.$cart_id;?>',1); return false;"  type="button" class="increase"><i class="fa fa-plus"></i></button>
      				<button type="button" data-toggle="tooltip" title="<?php echo $button_update; ?>" onclick="update_cart_data_home('<?php echo $cart_id; ?>','<?php echo 'select-number'.$cart_id;?>');" class="button_update"></button>
				<?php else: ?>
					<input type="button" value="<?php echo $button_cart; ?>" onclick="btadd.cart('<?php echo $mainproduct['product_id']; ?>', '<?php echo $mainproduct['minimum']; ?>');" class="btn btn-primary" />
				<?php endif ?>
			<?php else: ?>
			<input type="button" value="<?php echo $button_cart; ?>" onclick="btadd.cart('<?php echo $mainproduct['product_id']; ?>', '<?php echo $mainproduct['minimum']; ?>');" class="btn btn-primary" />
			<?php endif ?></div>
				<a class="btn-action btn-wishlist" onclick="boss_addToWishList('<?php //echo $mainproduct['product_id']; ?>');"><?php echo $button_wishlist; ?></a>
				<a class="btn-action btn-comapre" onclick="boss_addToCompare('<?php //echo $mainproduct['product_id']; ?>');"><?php echo $button_compare; ?></a>
			</div>
			</div>
		</section>
		</div>
	<?php } ?>
	
	<?php if(!empty($product_data['products'])){ ?>
	<?php $products = $product_data['products'];?>
	<div class="bt-items bt-product-grid">
		<div id="boss_featured_<?php echo $module; ?>">
			<?php $i = 0; 
			$count_product = count($products);
			if($count_product<($per_row*$row)){
				$row = 1;
			} ?>
			<?php while($i< $count_product) { ?>		
			
			<?php //if(($i%$row)==0){ ?> <div class="bt-item-extra element-<?php echo $per_row; ?>"> <?php //} ?>
			<?php  $j=0; ?>
			<?php while($i<$count_product && $j < $row){
					$product = $products[$i];
					
					$j++;
					//echo $product['price'];
             if(isset($_SESSION['cartProducts'])){
                        
                        $itemSelected = (in_array($product['product_id'], $_SESSION['cartProducts'])) ? 'bt-item-selected' : '';
                    }else{
                        
                      $itemSelected = '';  
                    }
              
			?>
			<section class="bt-item <?php echo $itemSelected ?>" id="<?php echo $product['product_id']; ?>">
			<div class="product-thumb">
				<?php if ($product['thumb']) { ?>
				<div class="image">
					<a id="<?php echo $product['href']; ?>" href="javascript:viod(0)" title="<?php echo properName($product['name_hover']); ?>">
						<img src="<?php echo $product['thumb']; ?>" alt="<?php echo properName($product['name']); ?>" />
                                                
					</a>
                                    
                                    <?php if ($product['special']) { ?>
                                    
                                    <div class="sale-box">
                                <div class="sale-label">
                                    <b><?php echo $product['discount'];?></b></div>
                                 </div>
                                 <?php } 
                                 $existInList = false;
									foreach ($_SESSION['default']['wishlist'] as $key => $value) {
										if ($value[0] == $product['product_id']) {
											$existInList = true;
										}
									}
									if ($existInList) {
										unset($this->session->data['wishlist'][$index]);
									}
									if($existInList) { ?>
                                <div class="heart-box-pink" id="heart<?php echo $product['product_id']; ?>">
                                <div class="heart-label"><a class="wishlist1"  onclick="wishlist_remove('<?php echo $product['product_id']; ?>'); changeHeart('<?php echo $product['product_id']; ?>')">
                                   		<i class="fa fa-heart" style="color:#E8114A"></i>
                                   	</a>
                                   </div>
                                 </div>
                            <?php } else{ ?>
                                <div class="heart-box" id="heart<?php echo $product['product_id']; ?>">
                                <div class="heart-label">
                                   <a class="wishlist1" onclick="wishlist_cat_add('<?php echo $product['product_id']; ?>')"><i class="fa fa-heart"></i></a>
                                   </div>
                                 </div>
                 	<?php } ?>	
 
                                
				</div>
				<?php } ?>			
				<div class="caption">
					<div class="name">
						<a class="boss-name" href="javascript:viod(0);" title="<?php echo properName($product['name_hover']); ?>"> <?php echo $product['name'];?></a>
					</div>
					<?php //if ($product['rating']) { ?>						
						 <div class="rating" style="display:none !important">
						  <?php for ($rate = 1; $rate <= 5; $rate++) { ?>
						  <?php if ($product['rating'] < $rate) { ?>
						  <span class="fa fa-stack"><i class="fa fa-star-o fa-stack-2x"></i></span>
						  <?php } else { ?>
						  <span class="fa fa-stack"><i class="fa fa-star fa-stack-2x"></i></span>
						  <?php } ?>
						  <?php } ?>
						</div>						
					<?php //} ?>					
					<?php if ($product['price']) { ?>
					<div class="price">
						<?php if (!$product['special']) { ?>
							<?php echo $product['price']; ?>
						<?php } else { ?>
							<span class="price-old" style="display: -webkit-inline-box !important"><?php echo $product['price']; ?></span> <span class="price-new"><?php echo $product['special']; ?></span>
						<?php } ?>
					</div>
					<?php } ?>
					</div>
					<div class="boss-button">
					<div id="<?php echo "pro_div_id_".$product['product_id']; ?>" class="cart ">
					<?php if (count($data['product_data']['cart_data'])): ?>
						<?php $count = 0; ?>
						<?php foreach ($data['product_data']['cart_data'] as $key => $value): ?>
							<?php if ($value['product_id'] == $product['product_id']): ?>
								<?php $count = 1; 
									$quantity = $value['quantity'];
									$cart_id = $value['cart_id'];
									break; ?>
							<?php endif ?>	
						<?php endforeach ?>
						<?php if ($count == 1): ?>
							<button onclick="changeQty('<?php echo 'select-number'.$cart_id;?>',0); return false;" class="decrease"><i class="fa fa-minus"></i></button>
              				<input id="select-number<?php echo $cart_id; ?>" type="text" name="quantity[<?php echo $cart_id; ?>]" value="<?php echo $quantity; ?>" size="1" class="form-control" style="display:-webkit-inline-box;width: 40px;height: 30px;text-align: center" onchange="update_cart_data_home('<?php echo $cart_id; ?>', '<?php echo 'select-number'.$cart_id;?>');"/>
							<button onclick="changeQty('<?php echo 'select-number'.$cart_id;?>',1); return false;" class="increase"><i class="fa fa-plus"></i></button>
              				
						<?php else: ?>
							<input type="button" value="<?php echo $button_cart; ?>" onclick="btadd.cart('<?php echo $product['product_id']; ?>', '<?php echo $product['minimum']; ?>');" class="btn btn-primary" id="custom" />
						<?php endif ?>
					<?php else: ?>
						<input type="button" value="<?php echo $button_cart; ?>" onclick="btadd.cart('<?php echo $product['product_id']; ?>', '<?php echo $product['minimum']; ?>');" class="btn btn-primary" id="custom"/>
					<?php endif ?>
					</div>					
					
                <!--                        					
					<a class="btn-action btn-comapre" onclick="boss_addToCompare('<?php echo $product['product_id']; ?>');"><?php echo $button_compare; ?></a>
					</div>-->
				</div>
				</div>
			</section>			
			<?php if(($j==$row)||($i==($count_product-1))){ ?> </div> <?php } ?>
			<?php  $i++;} ?>
			<?php } ?>
			
		</div>
		<?php if(($show_slider) && (empty($column))){ ?>
			<a id="bt_next_<?php echo $module; ?>" class="btn-nav-center prev nav_thumb" href="javascript:void(0)" title="prev">Prev</a>
			<a id="bt_prev_<?php echo $module; ?>" class="btn-nav-center next nav_thumb" href="javascript:void(0)" title="next">Next</a>
			<?php if($nav_type){ ?>
			<div id="bt_pag_<?php echo $module; ?>" class="bt-pag"></div>
			<?php } ?>
		<?php } ?>
		
	</div>
		<?php if(($show_slider) && (!empty($column))){ ?>
			<a id="bt_next_<?php echo $module; ?>" class="btn-nav-center prev nav_thumb" href="javascript:void(0)" title="prev">Prev</a>
			<a id="bt_prev_<?php echo $module; ?>" class="btn-nav-center next nav_thumb" href="javascript:void(0)" title="next">Next</a>
			<?php if($nav_type){ ?>
			<div id="bt_pag_<?php echo $module; ?>" class="bt-pag"></div>
			<?php } ?>
		<?php } ?>
	<?php } ?>
  </div>
</div>
<script type="text/javascript"><!--
function loadImage(id,linkimage){
	image = '<img src="'+linkimage+'" />';
	$('#'+id).html(image);
	
	return false;
}
//--></script>
<?php if($show_slider){ ?>
<script type="text/javascript"><!--
$(window).load(function(){
	//var image_width = <?php echo $image_width; ?>;
	if ($(window).width() > 768) {
		var image_width = <?php echo $image_width; ?>;
	}else{
		var image_width = 300;
	}
    $('#boss_featured_<?php echo $module; ?>').carouFredSel({
        auto : {
			<?php if($auto_scroll){ ?> play: true, <?php }else{?> play: false, <?php } ?>
			timeoutDuration: 3000,
		},
        responsive: true,
        width: '100%',
		/*height: 'auto',*/
        prev: '#bt_next_<?php echo $module; ?>',
        next: '#bt_prev_<?php echo $module; ?>',
		pagination: '#bt_pag_<?php echo $module; ?>',
        swipe: {
			onTouch : false
        },
        items: {
            /*width: 200,*/
            height: 'auto',
            visible: {
				min: 1,
				max: <?php echo $per_row; ?>
            }
        },
		direction: '<?php echo $direction;?>',
        scroll: {
            direction : 'left',    //  The direction of the transition.
            duration  : 1000   //  The duration of the transition.
        },
		onCreate: function () {
			$(window).smartresize(function(){
				$('#boss_featured_<?php echo $module; ?> section.bt-item').css("height",'');	
				$('#boss_featured_<?php echo $module; ?> section.bt-item').css("height",getMaxHeight('#boss_featured_<?php echo $module; ?> section.bt-item'));	
								<?php if($direction=='top'){ ?>
				$('#boss_min_height_<?php echo $module; ?> div.caroufredsel_wrapper').css("min-height",getMaxHeight('#boss_featured_<?php echo $module; ?> .bt-item-extra')*<?php echo $per_row; ?>);
				
				$('#boss_min_height_<?php echo $module; ?> div.caroufredsel_wrapper').css("width",'100%');
				$('#boss_min_height_<?php echo $module; ?> div.caroufredsel_wrapper #boss_featured_<?php echo $module; ?>').css("width",'100%');
				<?php } ?>
			});
		}
    });
	
	$('#boss_featured_<?php echo $module; ?> section.bt-item').css("height",getMaxHeight('#boss_featured_<?php echo $module; ?> section.bt-item'));	
		
	<?php if($direction=='top'){ ?>
	$('#boss_min_height_<?php echo $module; ?> div.caroufredsel_wrapper').css("min-height",getMaxHeight('#boss_featured_<?php echo $module; ?> .bt-item-extra')*<?php echo $per_row; ?>);	
	<?php } ?>
	
});
 
function getMaxHeight($elms) {
	var maxHeight = 0;
	$($elms).each(function () {
	
		var height = $(this).outerHeight();
		if (height > maxHeight) {
			maxHeight = height;
		}
	});
	return maxHeight;
};

//--></script>
<?php } ?>
<script type="text/javascript"><!--
$(window).load(function(){	
    $('#addition_image<?php echo $module; ?>').carouFredSel({
        auto: false,
        responsive: true,
        width: '100%',
		/*height: 'auto',*/
        prev: '#add_img_next_<?php echo $module; ?>',
        next: '#add_img_prev_<?php echo $module; ?>',
		swipe: {
			onTouch : false
        },
        items: {
            width: 150,
            height: 'auto',
            visible: {
				min: 1,
				max: 3
            }
        },		
        scroll: {
            direction : 'left',    //  The direction of the transition.
            duration  : 1000   //  The duration of the transition.
        },
		
    });
});
//--></script>
<?php } ?>
<script type="text/javascript"><!--
function changeQty(id,increase) {
    
    var qty = parseInt($('#' + id+'').val());

    if(parseInt(qty) <= 1 )
    {
       qty = increase ? qty+1 : 0; 
        $('#' + id+'').val(qty);
        
     }
    else if ( !isNaN(qty) ) {
        qty = increase ? qty+1 : (qty-1 > 1 ? qty-1:1);
        $('#' + id+'').val(qty);
    }else{
  $('#' + id+'').val(0);
 }

var arr = id.split('select-number');

 update_cart_data_home(arr[1], id);
}

function update_cart_data_home(cart_id, id) {
	var quantity = $('#'+ id+'').val();
	if (quantity >= 0) {
		$.ajax({
			url: 'index.php?route=checkout/cart/update_by_home',
			type: 'post',
			data: {'cart_id' : cart_id, 'quantity': quantity},
			dataType: 'json',

  success: function(json) {				

		if (json['success']) {
     	addProductNotices('Update Success','abc','Shopping Cart Updated Successfully','success');
     $('.b-cart-total').html(json['total']);
    
     $('.b-cart > ul').load('index.php?route=common/cart/info ul li');
     
   
    }
			}

		});
	}
    
    
    if(quantity==0)
     {
        
        var parentId = $('#'+ id+'').parent().attr('id');
        var parentIdArray =   parentId.split("_");       
        
         var cartBtn = '<input type="button" value="Add to cart" onclick="btadd.cart('+parentIdArray[3]+', 1)" class="btn btn-primary" id="custom" />' ;   
        $("#"+parentId).html(cartBtn);
     }
    
}
function addProductNotices(title, thumb, text, type) {
	$.jGrowl.defaults.closer = true;
	var tpl = title + '<h3>'+text+'</h3>';
	$.jGrowl(tpl, {		
		life: 3000,
		header: title,
		speed: 'slow'
	});
}
//--></script>