<?php global $config;?>
<div class="boss_flickr">
	<?php if(isset($heading_title) && $heading_title!=''){ ?>
    <div class="box-heading block-title">
        <span><?php echo $heading_title; ?></span>
    </div>
	<?php } ?>
	<?php 
		require_once( DIR_TEMPLATE.$config->get('config_template')."/template/bossthemes/phpFlickr.php" );
		$user_id = $user_id;
		$f = new phpFlickr($api_key);
		$args = array(
			'per_page' => $limit,
		);
		$image_size = $image_size-1;
		die($image_size);
		$recent = $f->people_getPhotos($user_id,$args);
	?>
	<?php if(isset($recent) && !empty($recent)) { ?>
    <div class="boss_flickr_photo">
        <?php
			foreach ($recent['photos']['photo'] as $photo) {
				$image = $f->photos_getSizes($photo['id']);
		?>
		<a target="_blank" href="https://www.flickr.com/photos/<?php echo $photo['owner'] ?>/<?php echo $photo['id'] ?>/" title="<?php echo $photo['title'];?>"><img  src="<?php echo $image[$image_size]['source'] ?>" title="<?php echo $photo['title'];?>" alt="<?php echo $photo['title'];?>" /></a>
   
		<?php } ?>
    </div>
	<?php } ?>

</div>
