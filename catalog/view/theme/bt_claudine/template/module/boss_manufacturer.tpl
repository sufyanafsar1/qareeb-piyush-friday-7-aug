<div class="bt-box bt-manufacturer">
	<div class="box-heading"><span> <?php echo $heading_title; ?> </span></div>
	<div class="box-content scroll-left">    
		<ul>
		<?php if(isset($manufacturers) && !empty($manufacturers)){ ?>
        <?php foreach ($manufacturers as $manufacturer) { ?>        
		<li><a href="<?php echo $manufacturer['href']; ?>" title="<?php echo $manufacturer['name']; ?>" <?php if(isset($manufacturer_id) && $manufacturer['manufacturer_id'] == $manufacturer_id) {echo 'class="active"';} ?>><?php echo $manufacturer['name']; ?></a></li>        
        <?php } ?>
        <?php } ?>
		</ul>
	</div>
</div>