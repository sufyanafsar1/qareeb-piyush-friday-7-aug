<script>
var config = <?php echo $json_config; ?>;
if(typeof(ga) == "undefined")
   config.general.analytics_event = 0;
</script>
<style>
<?php echo $config['design']['custom_style']; ?>
<?php if($config['design']['only_quickcheckout']){ ?>
body > *{
	display: none
}
body > #d_quickcheckout{
	display: block;
} 
#d_quickcheckout.container #d_logo{
	margin: 20px 0px;
}
<?php } ?>
</style>

       <ul id="order" class="nav nav-tabs nav-justified">
            <li class="disabled active"><a href="#tab-address" data-toggle="tab"><?php echo $title_delivery; ?></a></li>
            <li class="disabled"><a href="#tab-delivery" data-toggle="tab"><?php echo $title_delivery_date_time; ?></a></li>
            <li class="disabled"><a href="#tab-payment" data-toggle="tab"><?php echo $title_payment; ?></a></li>
            <li class="disabled"><a href="#tab-summary" data-toggle="tab"><?php echo $title_summary; ?></a></li>
          </ul>
          <div class="tab-content tab-content-custom" id="cartContent">
            <div class="tab-pane active" id="tab-address">
              
             
		     <?php echo $payment_address; ?>
	          <h2> <?php echo $description_agreement; ?></h2>
             <div class="col-sm-4"></div>
              <div class="col-sm-4">
                <button type="button" id="button-address" data-loading-text="Loading" class="btn btn-primary btn-primary-custom btn-login"><i class="fa fa-arrow-right"></i> <?php echo $button_next; ?></button>
              </div>
              <div class="col-sm-4"></div>
            </div>
            
             <div class="tab-pane" id="tab-delivery">
                    <?php echo $timeslot; ?>
                    <div class="col-sm-4"></div>
              <div class="col-sm-4">
                <button type="button" id="button-interval" data-loading-text="Loading" class="btn btn-primary btn-primary-custom btn-login"><i class="fa fa-arrow-right"></i> <?php echo $button_next; ?></button>
              </div>
              <div class="col-sm-4"></div>
            </div>
            <div class="tab-pane" id="tab-payment">
             <?php echo $payment_method; ?>
               <div class="col-sm-12" id="bank_details">
    
                <h2>Bank Transfer Instructions</h2>
                 <p><b><?php echo $text_bank_transfer; ?></b></p>
                <div class="well well-sm">
                  <p><?php echo $text_bank_details; ?>
                </p>
                  <p><?php echo $text_bank_warning; ?></p>
                </div>                
       
             </div>
              <div class="row">
                 <div class="col-sm-4"></div>
                <div class="col-sm-4">
                  <button type="button" id="button-payment" class="btn btn-primary btn-primary-custom btn-login"><i class="fa fa-arrow-right"></i> <?php echo $button_next; ?></button>
                </div>
                 <div class="col-sm-4"></div>
              </div>
            </div>
                        
            <div class="tab-pane" id="tab-summary">
              <?php echo $cart; ?>
		<?php echo $payment; ?>
		<?php echo $confirm; ?>
              
            </div>
          </div>

<div id="d_quickcheckout">
	<?php echo $field; ?>

</div>
<script type="text/javascript"><!--
$("#button-interval").css("display","none");
// Disable the tabs
$('#order a[data-toggle=\'tab\']').on('click', function(e) {
	return false;
});
$('#button-address').on('click', function() {
	$('a[href=\'#tab-delivery\']').tab('show');
   
});
/*
 $('#button-interval').on('click', function() {
 	    $('a[href=\'#tab-payment\']').tab('show');
      paymentMethod();
      $("#cartContent").attr('class', 'tab-content');
    
 });
 */

$(document).delegate('#button-interval', 'click', function() {

    
    $.ajax({
        url: 'index.php?route=module/timeslot/timeslot/save',
        type: 'post',
        data: {get_slot : $('#get_slots').val(), delivery_slot_time: $('#slots').val()},
        dataType: 'html',
        beforeSend: function() {
        	$('#button-interval').button('loading');
		},
        complete: function() {
			$('#button-interval').button('reset');
        },
        success: function(json) {
            
            if(json=='fulled'){
            
                $("#error_max_slot").css("display","block");
                $("#error_max_slot").css("color","red");
               
                return false;
        }else{
            
            $("#error_max_slot").css("display","none");
            
        }
            $('.alert, .text-danger').remove();

            if (json['redirect']) {
                location = json['redirect'];
            } else if (json['error']) {
                if (json['error']['warning']) {
                    $('#tab-delivery').prepend('<div class="alert alert-warning">' + json['error']['warning'] + '<button type="button" class="close" data-dismiss="alert">&times;</button></div>');
                }
            } else {
                $('a[href=\'#tab-payment\']').tab('show');
                  paymentMethod();
                  $("#cartContent").attr('class', 'tab-content');
            }
        },
        error: function(xhr, ajaxOptions, thrownError) {
            alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
        }
    });
});



/*
 $('#button-address').on('click', function() {
 	$('a[href=\'#tab-payment\']').tab('show');
     paymentMethod();
     $("#cartContent").attr('class', 'tab-content');
 });
 */
$('#button-payment').on('click', function() {
	$('a[href=\'#tab-summary\']').tab('show');
    $("#cartContent").attr('class', 'tab-content tab-content-custom');
});
 

</script>
