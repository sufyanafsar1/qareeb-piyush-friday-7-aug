<div class="bt-featured bt-box">
<div class="box-heading"><span><?php echo $heading_title; ?></span></div>
<div class="box-content bt-product-content">
  <div class="bt-items bt-product-grid">
  <?php foreach ($sellers as $seller) { ?>
  <div class="bt-item-extra element-6">
      <div class="image"><img src="<?php echo $seller['thumb']; ?>" alt="<?php echo $seller['name']; ?>"
      title="<?php echo $seller['name']; ?>" class="img-responsive" /></div>
      <div class="prod-detail">
        <div class="name"><a href="<?php echo $seller['href']; ?>"><?php echo $seller['name']; ?></a></div>
       
      </div>
     
  </div>
  <?php } ?>
</div>
</div>
</div>
