<?php echo $header; ?>

<?php global $config; ?>

<?php 

	$refine_search=1; $category_info=1; $view='both_grid'; $boss_class = 'col-lg-3 col-md-3 col-sm-6 col-xs-12'; $category_icon=1; $category_image=0;

	if($config->get('boss_manager')){

		$boss_manager = $config->get('boss_manager'); 

	}else{

		$boss_manager = '';

	}

	if(!empty($boss_manager)){

		$refine_search = isset($boss_manager['other']['refine_search'])?$boss_manager['other']['refine_search']:1; 		

		$category_info = isset($boss_manager['other']['category_info'])?$boss_manager['other']['category_info']:1; 

		$category_icon = isset($boss_manager['other']['category_icon'])?$boss_manager['other']['category_icon']:1; 

		$category_image = isset($boss_manager['other']['category_image'])?$boss_manager['other']['category_image']:0; 

		$view = isset($boss_manager['other']['view_pro'])?$boss_manager['other']['view_pro']:'both_grid'; 

		$perrrow = isset($boss_manager['other']['perrow'])?$boss_manager['other']['perrow']:3;

	}

	

	if(isset($perrrow) && $perrrow==1){

		$boss_class = 'col-lg-12 col-md-12 col-sm-12 col-xs-12';

	}else if(isset($perrrow) && $perrrow==2){

		$boss_class = 'col-lg-6 col-md-6 col-sm-6 col-xs-12';

	}else if(isset($perrrow) && $perrrow==3){

		$boss_class = 'col-lg-4 col-md-4 col-sm-6 col-xs-12';

	}else if(isset($perrrow) && $perrrow==4){

		$boss_class = 'col-lg-3 col-md-3 col-sm-6 col-xs-12';

	}else if(isset($perrrow) && $perrrow==5){

		$boss_class = 'boss-col-5column col-md-3 col-sm-6 col-xs-12';

	}else if(isset($perrrow) && $perrrow==6){

		$boss_class = 'col-lg-2 col-md-3 col-sm-6 col-xs-12';

	}
    
    


?>

<div class="bt-breadcrumb">

<div class="container">

  <ul class="breadcrumb">

	<li class="b_breadcrumb"><?php echo $heading_title; ?></li>

    <?php foreach ($breadcrumbs as $breadcrumb) { ?>

    <li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>

    <?php } ?>

  </ul>

  </div>

</div>

<div class="container">

  <div class="row"><?php echo $column_left; ?><?php echo $column_right; ?>

    <?php if ($column_left && $column_right) { ?>

    <?php $class = 'col-sm-6'; ?>

    <?php } elseif ($column_left || $column_right) { ?>

    <?php $class = 'col-sm-9'; ?>

    <?php } else { ?>

    <?php $class = 'col-sm-12'; ?>

    <?php } ?>

    <div id="content" class="<?php echo $class; ?>">

	<div id="bulk-load" style="display:none"></div>

	<?php if ($thumb && $category_image) { ?>

	<div data-delay="400" data-animate="fadeInUp" class="block-cate-page fadeInUp animated"><a title="banner category" href="#"><img src="<?php echo $thumb ?>" alt="banner-category"></a></div>

	<?php } ?>

	<?php echo $content_top; ?>

      

	<div class="content_bg"><?php if ($description) { ?>

		<?php if($category_info){ ?>

      <div class="category-info">        

        <h2><?php echo $heading_title; ?></h2>

        <?php if ($description) { ?>

        <div class="info_detail"><?php echo $description; ?></div>

        <?php } ?>

      </div>

      <?php } ?>

      <?php } ?>

	<?php if(isset($boss_manager['other']['refine_search'])&&($boss_manager['other']['refine_search'])){ ?>

      <?php if ($categories && $refine_search) { ?>

      <div class="category-list">

      <h3><?php echo $text_refine; ?></h3>

      <?php if (count($categories) <= 5) { ?>

          <ul>

            <?php foreach ($categories as $category) { ?>

            <li>

				<?php if ($category_icon) { ?> <div class="sub_image"><img src="<?php echo $category['thumb']; ?>" title="<?php echo $category['name']; ?>" alt="<?php echo $category['name']; ?>" /></div>

				<?php } ?>

				<a href="<?php echo $category['href']; ?>"><?php echo $category['name']; ?></a></li>

            <?php } ?>

          </ul>

      <?php } else { ?>

       

          <ul>

            <?php foreach ($categories as $category) { ?>

            <li>

			<?php if ($category_icon) { ?> <div class="sub_image"><img src="<?php echo $category['thumb']; ?>" title="<?php echo $category['name']; ?>" alt="<?php echo $category['name']; ?>" /> </div>

			<?php } ?>

			<a href="<?php echo $category['href']; ?>"><?php echo $category['name']; ?></a></li>

            <?php } ?>

          </ul>

        

      <?php } ?>

      </div>

      <?php } ?>

	<?php } ?>  

	  </div>

      <?php if ($products) { ?>

      <div class="product-filter">

		<!--<div class="compare-total"><a href="<?php echo $compare; ?>" id="compare-total"><?php echo $text_compare; ?></a></div>-->

		<div class="btn-group" <?php if($view == 'grid' || $view =='list')echo 'style="display:none"'; ?>>

			<button type="button" style="display:none" id="grid-view" title="<?php echo $button_grid; ?>"><i class="fa fa-th"></i></button>

			<!--<button type="button" id="list-view" title="<?php echo $button_list; ?>"><i class="fa fa-th-list"></i></button>-->

		</div>

		<!--<div class="sort">

          <label class="control-label" for="input-sort"><?php echo $text_sort; ?></label>

		  <label class="arrow">

          <select id="input-sort" class="form-control selectpicker" onchange="location = this.value;">

            <?php foreach ($sorts as $sorts) { ?>

            <?php if ($sorts['value'] == $sort . '-' . $order) { ?>

            <option value="<?php echo $sorts['href']; ?>" selected="selected"><?php echo $sorts['text']; ?></option>

            <?php } else { ?>

            <option value="<?php echo $sorts['href']; ?>"><?php echo $sorts['text']; ?></option>

            <?php } ?>

            <?php } ?>

          </select>

		  </label>

		</div>

		<div class="limit">	

          <label class="control-label" for="input-limit"><?php echo $text_limit; ?></label>

		  <label class="arrow">

          <select id="input-limit" class="form-control selectpicker" onchange="location = this.value;">

            <?php foreach ($limits as $limits) { ?>

            <?php if ($limits['value'] == $limit) { ?>

            <option value="<?php echo $limits['href']; ?>" selected="selected"><?php echo $limits['text']; ?></option>

            <?php } else { ?>

            <option value="<?php echo $limits['href']; ?>"><?php echo $limits['text']; ?></option>

            <?php } ?>

            <?php } ?>

          </select>

		  </label>

		</div>-->

      </div>

	  <?php 

		global $registry; 

		$loader = new Loader($registry);

		$loader->model('bossthemes/boss_refinesearch');

		$loader->model('tool/image');

		$b_model = $registry->get('model_bossthemes_boss_refinesearch');

		$image_model = $registry->get('model_tool_image');

		$b_f_setting = $config->get('boss_refinesearch_module');

		$b_f_status = $config->get('boss_refinesearch_status');	  

	  ?>

      <div class="row layout-thumb">

        <?php foreach ($products as $product) {?>
        <?php $itemSelected = (in_array($product['product_id'], $_SESSION['cartProducts'])) ? 'bt-item-selected' : '';  ?>

        <div class="product-layout product-grid boss-col-5column col-md-3 col-sm-6 col-xs-12 <?php echo $itemSelected; ?>" id="<?php echo $product['product_id']; ?>">

          <div class="product-thumb">

            <div class="image"><a id="<?php echo $product['href']; ?>" href="javascript:viod(0)"><img src="<?php echo $product['thumb']; ?>" alt="<?php echo properName($product['name']); ?>" title="<?php echo properName($product['name']); ?>"/></a>
        
			</div>
        <?php $existInList = false;
                  foreach ($_SESSION['default']['wishlist'] as $key => $value) {
                    if ($value[0] == $product['product_id']) {
                      $existInList = true;
                    }
                  }
                  if ($existInList) {
                    unset($this->session->data['wishlist'][$index]);
                  }
                  if($existInList) { ?>
                                <div class="heart-box-pink" id="heart<?php echo $product['product_id']; ?>">
                                  <div class="heart-label">
                                    <a class="wishlist1" onclick="wishlist_remove('<?php echo $product['product_id']; ?>'); changeHeart('<?php echo $product['product_id']; ?>')">
                                      <i class="fa fa-heart" style="color:#E8114A"></i>
                                    </a>
                                  </div>
                                </div>
                            <?php } else{ ?>
                                <div class="heart-box" id="heart<?php echo $product['product_id']; ?>">
                                  <div class="heart-label">
                                     <a class="wishlist1" onclick="wishlist_cat_add('<?php echo $product['product_id']; ?>')"><i class="fa fa-heart"></i></a>
                                  </div>
                                </div>
                 	<?php } ?>

            <div class="small_detail">

                <div class="name"><?php echo getItemName($product['name'],30); ?></div>

				

                <?php if ($product['rating']) { ?>

                <div class="rating">

                  <?php for ($i = 1; $i <= 5; $i++) { ?>

                  <?php if ($product['rating'] < $i) { ?>

				  <span class="fa fa-stack"><i class="fa fa-star-o fa-stack-2x"></i></span>

				  <?php } else { ?>

				  <span class="fa fa-stack"><i class="fa fa-star fa-stack-2x"></i></span>

                  <?php } ?>

                  <?php } ?>

                </div>

                <?php } ?>

                <?php if ($product['price']) { ?>

                <p class="price">

                  <?php if (!$product['special']) { ?>

                  <?php echo $product['price']; ?>

                  <?php } else { ?>

                  <span class="price-new"><?php echo $product['special']; ?></span> <span class="price-old"><?php echo $product['price']; ?></span>

                  <?php } ?>

                  <?php if ($product['tax']) { ?>

                  <span class="price-tax"><?php echo $text_tax; ?> <?php echo $product['tax']; ?></span>

                  <?php } ?>

                </p>

                <?php } ?>

                <p class="description"><?php echo $product['description']; ?></p>

              <div class="button-group">
                	<div id="<?php echo "pro_div_id_".$product['product_id']; ?>" class="cart ">
					<?php  if (count($data['product_data']['cart_data'])): ?>
						<?php $count = 0; ?>
						<?php foreach ($data['product_data']['cart_data'] as $key => $value): ?>
							<?php if ($value['product_id'] == $product['product_id']): ?>
								<?php $count = 1; 
									$quantity = $value['quantity'];
									$cart_id = $value['cart_id'];
									break; ?>
							<?php endif ?>	
						<?php endforeach ?>
						<?php if ($count == 1): ?>
							<button onclick="changeQty('<?php echo 'select-number'.$cart_id;?>',0); return false;" class="decrease"><i class="fa fa-minus"></i></button>
              				<input id="select-number<?php echo $cart_id; ?>" type="text" name="quantity[<?php echo $cart_id; ?>]" value="<?php echo $quantity; ?>" size="1" class="form-control" style="display:-webkit-inline-box;width: 40px;height: 30px;text-align: center" onchange="update_cart_data_home('<?php echo $cart_id; ?>', '<?php echo 'select-number'.$cart_id;?>');"/>
							<button onclick="changeQty('<?php echo 'select-number'.$cart_id;?>',1); return false;" class="increase"><i class="fa fa-plus"></i></button>
              				
						<?php else: ?>
							<input type="button" value="<?php echo $button_cart; ?>" onclick="btadd.cart('<?php echo $product['product_id']; ?>', '<?php echo $product['minimum']; ?>');" class="btn btn-primary" id="custom" />
						<?php endif ?>
					<?php else: ?>
						<input type="button" value="<?php echo $button_cart; ?>" onclick="btadd.cart('<?php echo $product['product_id']; ?>', '<?php echo $product['minimum']; ?>');" class="btn btn-primary" id="custom"/>
					<?php endif ?>
					</div>
              <!--  <button type="button" class="btn-cart" onclick="btadd.cart('<?php echo $product['product_id']; ?>', '<?php echo $product['minimum']; ?>');"><?php echo $button_cart; ?></button>

                <button type="button" class="btn-cart" onclick="btadd.cart('<?php echo $product['product_id']; ?>', '<?php echo $product['minimum']; ?>');"><i class="fa fa-cart-plus" aria-hidden="true"></i></button>

                <button type="button" class="btn-wishlist" data-toggle="tooltip" title="<?php echo $button_wishlist; ?>" onclick="btadd.wishlist('<?php echo $product['product_id']; ?>');"><i class="fa fa-bars" aria-hidden="true"></i></button>

                <button type="button" class="btn-compare" data-toggle="tooltip" title="<?php echo $button_compare; ?>" onclick="btadd.compare('<?php echo $product['product_id']; ?>');"><i class="fa fa-retweet"></i></button>-->

              </div>

            </div>

		

	

          </div>

        </div>

        <?php } ?>

      </div>

      <div class="bt_pagination">

        <?php if(!empty($pagination)){?><div class="links"><?php echo $pagination; ?></div> <?php } ?>

        <div class="results"><?php echo $results; ?></div>

	  </div>

      <?php } ?>

      <?php if (!$categories && !$products) { ?>

		<div class="content_bg">

			<p><?php echo $text_empty; ?></p>

			<div class="buttons">

				<div class="pull-left"><a href="<?php echo $continue; ?>" class="btn"><?php echo $button_continue; ?></a></div>

			</div>

		</div>

      <?php } ?>

      <?php echo $content_bottom; ?></div>

    </div>	

</div>

<script type="text/javascript"><!--

// Product List

	$('#list-view').click(function() {
//
	//	$('#content .product-layout > .clearfix').remove();



	//	$('#content .product-layout').attr('class', 'product-layout product-list col-xs-12');



	//	localStorage.setItem('display', 'list');

	});
	// Product Grid

	$('#grid-view').click(function() {

	//	$('#content .product-layout').attr('class', 'product-layout product-grid <?php echo $boss_class; ?>');	

	//	 localStorage.setItem('display', 'grid');

	});

$(document).ready(function(){
   
    
    	if (localStorage.getItem('display') == 'list') {

		$('#list-view').trigger('click');

	} else if (localStorage.getItem('display') == 'grid'){

		$('#grid-view').trigger('click');

	}else {

		<?php if($view == 'grid' || $view == 'both_grid') { ?>

			$('#grid-view').trigger('click');

		<?php } ?>

		<?php if($view == 'list' || $view == 'both_list') { ?>

			$('#list-view').trigger('click');

		<?php } ?>

	}
});


	



//--></script>

<?php echo $footer; ?>