if (
	((function(t, e) {
		'object' == typeof module && 'object' == typeof module.exports
			? (module.exports = t.document
					? e(t, !0)
					: function(t) {
							if (!t.document) throw new Error('jQuery requires a window with a document');
							return e(t);
						})
			: e(t);
	})('undefined' != typeof window ? window : this, function(t, e) {
		var n = [],
			i = n.slice,
			o = n.concat,
			r = n.push,
			s = n.indexOf,
			a = {},
			l = a.toString,
			c = a.hasOwnProperty,
			u = {},
			d = '1.11.3',
			h = function(t, e) {
				return new h.fn.init(t, e);
			},
			p = /^[\s\uFEFF\xA0]+|[\s\uFEFF\xA0]+$/g,
			f = /^-ms-/,
			m = /-([\da-z])/gi,
			g = function(t, e) {
				return e.toUpperCase();
			};
		function v(t) {
			var e = 'length' in t && t.length,
				n = h.type(t);
			return (
				'function' !== n &&
				!h.isWindow(t) &&
				(!(1 !== t.nodeType || !e) || 'array' === n || 0 === e || ('number' == typeof e && 0 < e && e - 1 in t))
			);
		}
		(h.fn = h.prototype = {
			jquery: d,
			constructor: h,
			selector: '',
			length: 0,
			toArray: function() {
				return i.call(this);
			},
			get: function(t) {
				return null != t ? (t < 0 ? this[t + this.length] : this[t]) : i.call(this);
			},
			pushStack: function(t) {
				var e = h.merge(this.constructor(), t);
				return (e.prevObject = this), (e.context = this.context), e;
			},
			each: function(t, e) {
				return h.each(this, t, e);
			},
			map: function(t) {
				return this.pushStack(
					h.map(this, function(e, n) {
						return t.call(e, n, e);
					})
				);
			},
			slice: function() {
				return this.pushStack(i.apply(this, arguments));
			},
			first: function() {
				return this.eq(0);
			},
			last: function() {
				return this.eq(-1);
			},
			eq: function(t) {
				var e = this.length,
					n = +t + (t < 0 ? e : 0);
				return this.pushStack(0 <= n && n < e ? [ this[n] ] : []);
			},
			end: function() {
				return this.prevObject || this.constructor(null);
			},
			push: r,
			sort: n.sort,
			splice: n.splice
		}),
			(h.extend = h.fn.extend = function() {
				var t,
					e,
					n,
					i,
					o,
					r,
					s = arguments[0] || {},
					a = 1,
					l = arguments.length,
					c = !1;
				for (
					'boolean' == typeof s && ((c = s), (s = arguments[a] || {}), a++),
						'object' == typeof s || h.isFunction(s) || (s = {}),
						a === l && ((s = this), a--);
					a < l;
					a++
				)
					if (null != (o = arguments[a]))
						for (i in o)
							(t = s[i]),
								s !== (n = o[i]) &&
									(c && n && (h.isPlainObject(n) || (e = h.isArray(n)))
										? (e
												? ((e = !1), (r = t && h.isArray(t) ? t : []))
												: (r = t && h.isPlainObject(t) ? t : {}),
											(s[i] = h.extend(c, r, n)))
										: void 0 !== n && (s[i] = n));
				return s;
			}),
			h.extend({
				expando: 'jQuery' + (d + Math.random()).replace(/\D/g, ''),
				isReady: !0,
				error: function(t) {
					throw new Error(t);
				},
				noop: function() {},
				isFunction: function(t) {
					return 'function' === h.type(t);
				},
				isArray:
					Array.isArray ||
					function(t) {
						return 'array' === h.type(t);
					},
				isWindow: function(t) {
					return null != t && t == t.window;
				},
				isNumeric: function(t) {
					return !h.isArray(t) && 0 <= t - parseFloat(t) + 1;
				},
				isEmptyObject: function(t) {
					var e;
					for (e in t) return !1;
					return !0;
				},
				isPlainObject: function(t) {
					var e;
					if (!t || 'object' !== h.type(t) || t.nodeType || h.isWindow(t)) return !1;
					try {
						if (
							t.constructor &&
							!c.call(t, 'constructor') &&
							!c.call(t.constructor.prototype, 'isPrototypeOf')
						)
							return !1;
					} catch (t) {
						return !1;
					}
					if (u.ownLast) for (e in t) return c.call(t, e);
					for (e in t);
					return void 0 === e || c.call(t, e);
				},
				type: function(t) {
					return null == t
						? t + ''
						: 'object' == typeof t || 'function' == typeof t ? a[l.call(t)] || 'object' : typeof t;
				},
				globalEval: function(e) {
					e &&
						h.trim(e) &&
						(t.execScript ||
							function(e) {
								t.eval.call(t, e);
							})(e);
				},
				camelCase: function(t) {
					return t.replace(f, 'ms-').replace(m, g);
				},
				nodeName: function(t, e) {
					return t.nodeName && t.nodeName.toLowerCase() === e.toLowerCase();
				},
				each: function(t, e, n) {
					var i = 0,
						o = t.length,
						r = v(t);
					if (n) {
						if (r) for (; i < o && !1 !== e.apply(t[i], n); i++);
						else for (i in t) if (!1 === e.apply(t[i], n)) break;
					} else if (r) for (; i < o && !1 !== e.call(t[i], i, t[i]); i++);
					else for (i in t) if (!1 === e.call(t[i], i, t[i])) break;
					return t;
				},
				trim: function(t) {
					return null == t ? '' : (t + '').replace(p, '');
				},
				makeArray: function(t, e) {
					var n = e || [];
					return null != t && (v(Object(t)) ? h.merge(n, 'string' == typeof t ? [ t ] : t) : r.call(n, t)), n;
				},
				inArray: function(t, e, n) {
					var i;
					if (e) {
						if (s) return s.call(e, t, n);
						for (i = e.length, n = n ? (n < 0 ? Math.max(0, i + n) : n) : 0; n < i; n++)
							if (n in e && e[n] === t) return n;
					}
					return -1;
				},
				merge: function(t, e) {
					for (var n = +e.length, i = 0, o = t.length; i < n; ) t[o++] = e[i++];
					if (n != n) for (; void 0 !== e[i]; ) t[o++] = e[i++];
					return (t.length = o), t;
				},
				grep: function(t, e, n) {
					for (var i = [], o = 0, r = t.length, s = !n; o < r; o++) !e(t[o], o) !== s && i.push(t[o]);
					return i;
				},
				map: function(t, e, n) {
					var i,
						r = 0,
						s = t.length,
						a = [];
					if (v(t)) for (; r < s; r++) null != (i = e(t[r], r, n)) && a.push(i);
					else for (r in t) null != (i = e(t[r], r, n)) && a.push(i);
					return o.apply([], a);
				},
				guid: 1,
				proxy: function(t, e) {
					var n, o, r;
					return (
						'string' == typeof e && ((r = t[e]), (e = t), (t = r)),
						h.isFunction(t)
							? ((n = i.call(arguments, 2)),
								((o = function() {
									return t.apply(e || this, n.concat(i.call(arguments)));
								}).guid = t.guid = t.guid || h.guid++),
								o)
							: void 0
					);
				},
				now: function() {
					return +new Date();
				},
				support: u
			}),
			h.each('Boolean Number String Function Array Date RegExp Object Error'.split(' '), function(t, e) {
				a['[object ' + e + ']'] = e.toLowerCase();
			});
		var b = (function(t) {
			var e,
				n,
				i,
				o,
				r,
				s,
				a,
				l,
				c,
				u,
				d,
				h,
				p,
				f,
				m,
				g,
				v,
				b,
				y,
				w = 'sizzle' + 1 * new Date(),
				x = t.document,
				k = 0,
				$ = 0,
				T = rt(),
				E = rt(),
				C = rt(),
				_ = function(t, e) {
					return t === e && (d = !0), 0;
				},
				S = {}.hasOwnProperty,
				j = [],
				N = j.pop,
				A = j.push,
				I = j.push,
				L = j.slice,
				D = function(t, e) {
					for (var n = 0, i = t.length; n < i; n++) if (t[n] === e) return n;
					return -1;
				},
				O =
					'checked|selected|async|autofocus|autoplay|controls|defer|disabled|hidden|ismap|loop|multiple|open|readonly|required|scoped',
				P = '[\\x20\\t\\r\\n\\f]',
				R = '(?:\\\\.|[\\w-]|[^\\x00-\\xa0])+',
				M = R.replace('w', 'w#'),
				q =
					'\\[' +
					P +
					'*(' +
					R +
					')(?:' +
					P +
					'*([*^$|!~]?=)' +
					P +
					'*(?:\'((?:\\\\.|[^\\\\\'])*)\'|"((?:\\\\.|[^\\\\"])*)"|(' +
					M +
					'))|)' +
					P +
					'*\\]',
				z =
					':(' +
					R +
					')(?:\\(((\'((?:\\\\.|[^\\\\\'])*)\'|"((?:\\\\.|[^\\\\"])*)")|((?:\\\\.|[^\\\\()[\\]]|' +
					q +
					')*)|.*)\\)|)',
				U = new RegExp(P + '+', 'g'),
				H = new RegExp('^' + P + '+|((?:^|[^\\\\])(?:\\\\.)*)' + P + '+$', 'g'),
				G = new RegExp('^' + P + '*,' + P + '*'),
				F = new RegExp('^' + P + '*([>+~]|' + P + ')' + P + '*'),
				B = new RegExp('=' + P + '*([^\\]\'"]*?)' + P + '*\\]', 'g'),
				W = new RegExp(z),
				V = new RegExp('^' + M + '$'),
				Q = {
					ID: new RegExp('^#(' + R + ')'),
					CLASS: new RegExp('^\\.(' + R + ')'),
					TAG: new RegExp('^(' + R.replace('w', 'w*') + ')'),
					ATTR: new RegExp('^' + q),
					PSEUDO: new RegExp('^' + z),
					CHILD: new RegExp(
						'^:(only|first|last|nth|nth-last)-(child|of-type)(?:\\(' +
							P +
							'*(even|odd|(([+-]|)(\\d*)n|)' +
							P +
							'*(?:([+-]|)' +
							P +
							'*(\\d+)|))' +
							P +
							'*\\)|)',
						'i'
					),
					bool: new RegExp('^(?:' + O + ')$', 'i'),
					needsContext: new RegExp(
						'^' +
							P +
							'*[>+~]|:(even|odd|eq|gt|lt|nth|first|last)(?:\\(' +
							P +
							'*((?:-\\d)?\\d*)' +
							P +
							'*\\)|)(?=[^-]|$)',
						'i'
					)
				},
				X = /^(?:input|select|textarea|button)$/i,
				K = /^h\d$/i,
				Y = /^[^{]+\{\s*\[native \w/,
				J = /^(?:#([\w-]+)|(\w+)|\.([\w-]+))$/,
				Z = /[+~]/,
				tt = /'|\\/g,
				et = new RegExp('\\\\([\\da-f]{1,6}' + P + '?|(' + P + ')|.)', 'ig'),
				nt = function(t, e, n) {
					var i = '0x' + e - 65536;
					return i != i || n
						? e
						: i < 0
							? String.fromCharCode(i + 65536)
							: String.fromCharCode((i >> 10) | 55296, (1023 & i) | 56320);
				},
				it = function() {
					h();
				};
			try {
				I.apply((j = L.call(x.childNodes)), x.childNodes), j[x.childNodes.length].nodeType;
			} catch (e) {
				I = {
					apply: j.length
						? function(t, e) {
								A.apply(t, L.call(e));
							}
						: function(t, e) {
								for (var n = t.length, i = 0; (t[n++] = e[i++]); );
								t.length = n - 1;
							}
				};
			}
			function ot(t, e, i, o) {
				var r, a, c, u, d, f, v, b, k, $;
				if (
					((e ? e.ownerDocument || e : x) !== p && h(e),
					(i = i || []),
					(u = (e = e || p).nodeType),
					'string' != typeof t || !t || (1 !== u && 9 !== u && 11 !== u))
				)
					return i;
				if (!o && m) {
					if (11 !== u && (r = J.exec(t)))
						if ((c = r[1])) {
							if (9 === u) {
								if (!(a = e.getElementById(c)) || !a.parentNode) return i;
								if (a.id === c) return i.push(a), i;
							} else if (
								e.ownerDocument &&
								(a = e.ownerDocument.getElementById(c)) &&
								y(e, a) &&
								a.id === c
							)
								return i.push(a), i;
						} else {
							if (r[2]) return I.apply(i, e.getElementsByTagName(t)), i;
							if ((c = r[3]) && n.getElementsByClassName)
								return I.apply(i, e.getElementsByClassName(c)), i;
						}
					if (n.qsa && (!g || !g.test(t))) {
						if (
							((b = v = w), (k = e), ($ = 1 !== u && t), 1 === u && 'object' !== e.nodeName.toLowerCase())
						) {
							for (
								f = s(t),
									(v = e.getAttribute('id')) ? (b = v.replace(tt, '\\$&')) : e.setAttribute('id', b),
									b = "[id='" + b + "'] ",
									d = f.length;
								d--;

							)
								f[d] = b + mt(f[d]);
							(k = (Z.test(t) && pt(e.parentNode)) || e), ($ = f.join(','));
						}
						if ($)
							try {
								return I.apply(i, k.querySelectorAll($)), i;
							} catch (t) {
							} finally {
								v || e.removeAttribute('id');
							}
					}
				}
				return l(t.replace(H, '$1'), e, i, o);
			}
			function rt() {
				var t = [];
				return function e(n, o) {
					return t.push(n + ' ') > i.cacheLength && delete e[t.shift()], (e[n + ' '] = o);
				};
			}
			function st(t) {
				return (t[w] = !0), t;
			}
			function at(t) {
				var e = p.createElement('div');
				try {
					return !!t(e);
				} catch (t) {
					return !1;
				} finally {
					e.parentNode && e.parentNode.removeChild(e), (e = null);
				}
			}
			function lt(t, e) {
				for (var n = t.split('|'), o = t.length; o--; ) i.attrHandle[n[o]] = e;
			}
			function ct(t, e) {
				var n = e && t,
					i =
						n &&
						1 === t.nodeType &&
						1 === e.nodeType &&
						(~e.sourceIndex || 1 << 31) - (~t.sourceIndex || 1 << 31);
				if (i) return i;
				if (n) for (; (n = n.nextSibling); ) if (n === e) return -1;
				return t ? 1 : -1;
			}
			function ut(t) {
				return function(e) {
					return 'input' === e.nodeName.toLowerCase() && e.type === t;
				};
			}
			function dt(t) {
				return function(e) {
					var n = e.nodeName.toLowerCase();
					return ('input' === n || 'button' === n) && e.type === t;
				};
			}
			function ht(t) {
				return st(function(e) {
					return (
						(e = +e),
						st(function(n, i) {
							for (var o, r = t([], n.length, e), s = r.length; s--; )
								n[(o = r[s])] && (n[o] = !(i[o] = n[o]));
						})
					);
				});
			}
			function pt(t) {
				return t && void 0 !== t.getElementsByTagName && t;
			}
			for (e in ((n = ot.support = {}),
			(r = ot.isXML = function(t) {
				var e = t && (t.ownerDocument || t).documentElement;
				return !!e && 'HTML' !== e.nodeName;
			}),
			(h = ot.setDocument = function(t) {
				var e,
					o,
					s = t ? t.ownerDocument || t : x;
				return s !== p && 9 === s.nodeType && s.documentElement
					? ((f = (p = s).documentElement),
						(o = s.defaultView) &&
							o !== o.top &&
							(o.addEventListener
								? o.addEventListener('unload', it, !1)
								: o.attachEvent && o.attachEvent('onunload', it)),
						(m = !r(s)),
						(n.attributes = at(function(t) {
							return (t.className = 'i'), !t.getAttribute('className');
						})),
						(n.getElementsByTagName = at(function(t) {
							return t.appendChild(s.createComment('')), !t.getElementsByTagName('*').length;
						})),
						(n.getElementsByClassName = Y.test(s.getElementsByClassName)),
						(n.getById = at(function(t) {
							return (f.appendChild(t).id = w), !s.getElementsByName || !s.getElementsByName(w).length;
						})),
						n.getById
							? ((i.find.ID = function(t, e) {
									if (void 0 !== e.getElementById && m) {
										var n = e.getElementById(t);
										return n && n.parentNode ? [ n ] : [];
									}
								}),
								(i.filter.ID = function(t) {
									var e = t.replace(et, nt);
									return function(t) {
										return t.getAttribute('id') === e;
									};
								}))
							: (delete i.find.ID,
								(i.filter.ID = function(t) {
									var e = t.replace(et, nt);
									return function(t) {
										var n = void 0 !== t.getAttributeNode && t.getAttributeNode('id');
										return n && n.value === e;
									};
								})),
						(i.find.TAG = n.getElementsByTagName
							? function(t, e) {
									return void 0 !== e.getElementsByTagName
										? e.getElementsByTagName(t)
										: n.qsa ? e.querySelectorAll(t) : void 0;
								}
							: function(t, e) {
									var n,
										i = [],
										o = 0,
										r = e.getElementsByTagName(t);
									if ('*' === t) {
										for (; (n = r[o++]); ) 1 === n.nodeType && i.push(n);
										return i;
									}
									return r;
								}),
						(i.find.CLASS =
							n.getElementsByClassName &&
							function(t, e) {
								return m ? e.getElementsByClassName(t) : void 0;
							}),
						(v = []),
						(g = []),
						(n.qsa = Y.test(s.querySelectorAll)) &&
							(at(function(t) {
								(f.appendChild(t).innerHTML =
									"<a id='" +
									w +
									"'></a><select id='" +
									w +
									"-\f]' msallowcapture=''><option selected=''></option></select>"),
									t.querySelectorAll("[msallowcapture^='']").length &&
										g.push('[*^$]=' + P + '*(?:\'\'|"")'),
									t.querySelectorAll('[selected]').length || g.push('\\[' + P + '*(?:value|' + O + ')'),
									t.querySelectorAll('[id~=' + w + '-]').length || g.push('~='),
									t.querySelectorAll(':checked').length || g.push(':checked'),
									t.querySelectorAll('a#' + w + '+*').length || g.push('.#.+[+~]');
							}),
							at(function(t) {
								var e = s.createElement('input');
								e.setAttribute('type', 'hidden'),
									t.appendChild(e).setAttribute('name', 'D'),
									t.querySelectorAll('[name=d]').length && g.push('name' + P + '*[*^$|!~]?='),
									t.querySelectorAll(':enabled').length || g.push(':enabled', ':disabled'),
									t.querySelectorAll('*,:x'),
									g.push(',.*:');
							})),
						(n.matchesSelector = Y.test(
							(b =
								f.matches ||
								f.webkitMatchesSelector ||
								f.mozMatchesSelector ||
								f.oMatchesSelector ||
								f.msMatchesSelector)
						)) &&
							at(function(t) {
								(n.disconnectedMatch = b.call(t, 'div')), b.call(t, "[s!='']:x"), v.push('!=', z);
							}),
						(g = g.length && new RegExp(g.join('|'))),
						(v = v.length && new RegExp(v.join('|'))),
						(e = Y.test(f.compareDocumentPosition)),
						(y =
							e || Y.test(f.contains)
								? function(t, e) {
										var n = 9 === t.nodeType ? t.documentElement : t,
											i = e && e.parentNode;
										return (
											t === i ||
											!(
												!i ||
												1 !== i.nodeType ||
												!(n.contains
													? n.contains(i)
													: t.compareDocumentPosition && 16 & t.compareDocumentPosition(i))
											)
										);
									}
								: function(t, e) {
										if (e) for (; (e = e.parentNode); ) if (e === t) return !0;
										return !1;
									}),
						(_ = e
							? function(t, e) {
									if (t === e) return (d = !0), 0;
									var i = !t.compareDocumentPosition - !e.compareDocumentPosition;
									return (
										i ||
										(1 &
											(i =
												(t.ownerDocument || t) === (e.ownerDocument || e)
													? t.compareDocumentPosition(e)
													: 1) ||
										(!n.sortDetached && e.compareDocumentPosition(t) === i)
											? t === s || (t.ownerDocument === x && y(x, t))
												? -1
												: e === s || (e.ownerDocument === x && y(x, e))
													? 1
													: u ? D(u, t) - D(u, e) : 0
											: 4 & i ? -1 : 1)
									);
								}
							: function(t, e) {
									if (t === e) return (d = !0), 0;
									var n,
										i = 0,
										o = t.parentNode,
										r = e.parentNode,
										a = [ t ],
										l = [ e ];
									if (!o || !r)
										return t === s ? -1 : e === s ? 1 : o ? -1 : r ? 1 : u ? D(u, t) - D(u, e) : 0;
									if (o === r) return ct(t, e);
									for (n = t; (n = n.parentNode); ) a.unshift(n);
									for (n = e; (n = n.parentNode); ) l.unshift(n);
									for (; a[i] === l[i]; ) i++;
									return i ? ct(a[i], l[i]) : a[i] === x ? -1 : l[i] === x ? 1 : 0;
								}),
						s)
					: p;
			}),
			(ot.matches = function(t, e) {
				return ot(t, null, null, e);
			}),
			(ot.matchesSelector = function(t, e) {
				if (
					((t.ownerDocument || t) !== p && h(t),
					(e = e.replace(B, "='$1']")),
					!(!n.matchesSelector || !m || (v && v.test(e)) || (g && g.test(e))))
				)
					try {
						var i = b.call(t, e);
						if (i || n.disconnectedMatch || (t.document && 11 !== t.document.nodeType)) return i;
					} catch (t) {}
				return 0 < ot(e, p, null, [ t ]).length;
			}),
			(ot.contains = function(t, e) {
				return (t.ownerDocument || t) !== p && h(t), y(t, e);
			}),
			(ot.attr = function(t, e) {
				(t.ownerDocument || t) !== p && h(t);
				var o = i.attrHandle[e.toLowerCase()],
					r = o && S.call(i.attrHandle, e.toLowerCase()) ? o(t, e, !m) : void 0;
				return void 0 !== r
					? r
					: n.attributes || !m
						? t.getAttribute(e)
						: (r = t.getAttributeNode(e)) && r.specified ? r.value : null;
			}),
			(ot.error = function(t) {
				throw new Error('Syntax error, unrecognized expression: ' + t);
			}),
			(ot.uniqueSort = function(t) {
				var e,
					i = [],
					o = 0,
					r = 0;
				if (((d = !n.detectDuplicates), (u = !n.sortStable && t.slice(0)), t.sort(_), d)) {
					for (; (e = t[r++]); ) e === t[r] && (o = i.push(r));
					for (; o--; ) t.splice(i[o], 1);
				}
				return (u = null), t;
			}),
			(o = ot.getText = function(t) {
				var e,
					n = '',
					i = 0,
					r = t.nodeType;
				if (r) {
					if (1 === r || 9 === r || 11 === r) {
						if ('string' == typeof t.textContent) return t.textContent;
						for (t = t.firstChild; t; t = t.nextSibling) n += o(t);
					} else if (3 === r || 4 === r) return t.nodeValue;
				} else for (; (e = t[i++]); ) n += o(e);
				return n;
			}),
			((i = ot.selectors = {
				cacheLength: 50,
				createPseudo: st,
				match: Q,
				attrHandle: {},
				find: {},
				relative: {
					'>': { dir: 'parentNode', first: !0 },
					' ': { dir: 'parentNode' },
					'+': { dir: 'previousSibling', first: !0 },
					'~': { dir: 'previousSibling' }
				},
				preFilter: {
					ATTR: function(t) {
						return (
							(t[1] = t[1].replace(et, nt)),
							(t[3] = (t[3] || t[4] || t[5] || '').replace(et, nt)),
							'~=' === t[2] && (t[3] = ' ' + t[3] + ' '),
							t.slice(0, 4)
						);
					},
					CHILD: function(t) {
						return (
							(t[1] = t[1].toLowerCase()),
							'nth' === t[1].slice(0, 3)
								? (t[3] || ot.error(t[0]),
									(t[4] = +(t[4] ? t[5] + (t[6] || 1) : 2 * ('even' === t[3] || 'odd' === t[3]))),
									(t[5] = +(t[7] + t[8] || 'odd' === t[3])))
								: t[3] && ot.error(t[0]),
							t
						);
					},
					PSEUDO: function(t) {
						var e,
							n = !t[6] && t[2];
						return Q.CHILD.test(t[0])
							? null
							: (t[3]
									? (t[2] = t[4] || t[5] || '')
									: n &&
										W.test(n) &&
										(e = s(n, !0)) &&
										(e = n.indexOf(')', n.length - e) - n.length) &&
										((t[0] = t[0].slice(0, e)), (t[2] = n.slice(0, e))),
								t.slice(0, 3));
					}
				},
				filter: {
					TAG: function(t) {
						var e = t.replace(et, nt).toLowerCase();
						return '*' === t
							? function() {
									return !0;
								}
							: function(t) {
									return t.nodeName && t.nodeName.toLowerCase() === e;
								};
					},
					CLASS: function(t) {
						var e = T[t + ' '];
						return (
							e ||
							((e = new RegExp('(^|' + P + ')' + t + '(' + P + '|$)')) &&
								T(t, function(t) {
									return e.test(
										('string' == typeof t.className && t.className) ||
											(void 0 !== t.getAttribute && t.getAttribute('class')) ||
											''
									);
								}))
						);
					},
					ATTR: function(t, e, n) {
						return function(i) {
							var o = ot.attr(i, t);
							return null == o
								? '!=' === e
								: !e ||
									((o += ''),
									'=' === e
										? o === n
										: '!=' === e
											? o !== n
											: '^=' === e
												? n && 0 === o.indexOf(n)
												: '*=' === e
													? n && -1 < o.indexOf(n)
													: '$=' === e
														? n && o.slice(-n.length) === n
														: '~=' === e
															? -1 < (' ' + o.replace(U, ' ') + ' ').indexOf(n)
															: '|=' === e && (o === n || o.slice(0, n.length + 1) === n + '-'));
						};
					},
					CHILD: function(t, e, n, i, o) {
						var r = 'nth' !== t.slice(0, 3),
							s = 'last' !== t.slice(-4),
							a = 'of-type' === e;
						return 1 === i && 0 === o
							? function(t) {
									return !!t.parentNode;
								}
							: function(e, n, l) {
									var c,
										u,
										d,
										h,
										p,
										f,
										m = r !== s ? 'nextSibling' : 'previousSibling',
										g = e.parentNode,
										v = a && e.nodeName.toLowerCase(),
										b = !l && !a;
									if (g) {
										if (r) {
											for (; m; ) {
												for (d = e; (d = d[m]); )
													if (a ? d.nodeName.toLowerCase() === v : 1 === d.nodeType) return !1;
												f = m = 'only' === t && !f && 'nextSibling';
											}
											return !0;
										}
										if (((f = [ s ? g.firstChild : g.lastChild ]), s && b)) {
											for (
												p = (c = (u = g[w] || (g[w] = {}))[t] || [])[0] === k && c[1],
													h = c[0] === k && c[2],
													d = p && g.childNodes[p];
												(d = (++p && d && d[m]) || (h = p = 0) || f.pop());

											)
												if (1 === d.nodeType && ++h && d === e) {
													u[t] = [ k, p, h ];
													break;
												}
										} else if (b && (c = (e[w] || (e[w] = {}))[t]) && c[0] === k) h = c[1];
										else
											for (
												;
												(d = (++p && d && d[m]) || (h = p = 0) || f.pop()) &&
												((a ? d.nodeName.toLowerCase() !== v : 1 !== d.nodeType) ||
													!++h ||
													(b && ((d[w] || (d[w] = {}))[t] = [ k, h ]), d !== e));

											);
										return (h -= o) === i || (h % i == 0 && 0 <= h / i);
									}
								};
					},
					PSEUDO: function(t, e) {
						var n,
							o = i.pseudos[t] || i.setFilters[t.toLowerCase()] || ot.error('unsupported pseudo: ' + t);
						return o[w]
							? o(e)
							: 1 < o.length
								? ((n = [ t, t, '', e ]),
									i.setFilters.hasOwnProperty(t.toLowerCase())
										? st(function(t, n) {
												for (var i, r = o(t, e), s = r.length; s--; )
													t[(i = D(t, r[s]))] = !(n[i] = r[s]);
											})
										: function(t) {
												return o(t, 0, n);
											})
								: o;
					}
				},
				pseudos: {
					not: st(function(t) {
						var e = [],
							n = [],
							i = a(t.replace(H, '$1'));
						return i[w]
							? st(function(t, e, n, o) {
									for (var r, s = i(t, null, o, []), a = t.length; a--; )
										(r = s[a]) && (t[a] = !(e[a] = r));
								})
							: function(t, o, r) {
									return (e[0] = t), i(e, null, r, n), (e[0] = null), !n.pop();
								};
					}),
					has: st(function(t) {
						return function(e) {
							return 0 < ot(t, e).length;
						};
					}),
					contains: st(function(t) {
						return (
							(t = t.replace(et, nt)),
							function(e) {
								return -1 < (e.textContent || e.innerText || o(e)).indexOf(t);
							}
						);
					}),
					lang: st(function(t) {
						return (
							V.test(t || '') || ot.error('unsupported lang: ' + t),
							(t = t.replace(et, nt).toLowerCase()),
							function(e) {
								var n;
								do {
									if ((n = m ? e.lang : e.getAttribute('xml:lang') || e.getAttribute('lang')))
										return (n = n.toLowerCase()) === t || 0 === n.indexOf(t + '-');
								} while ((e = e.parentNode) && 1 === e.nodeType);
								return !1;
							}
						);
					}),
					target: function(e) {
						var n = t.location && t.location.hash;
						return n && n.slice(1) === e.id;
					},
					root: function(t) {
						return t === f;
					},
					focus: function(t) {
						return (
							t === p.activeElement &&
							(!p.hasFocus || p.hasFocus()) &&
							!!(t.type || t.href || ~t.tabIndex)
						);
					},
					enabled: function(t) {
						return !1 === t.disabled;
					},
					disabled: function(t) {
						return !0 === t.disabled;
					},
					checked: function(t) {
						var e = t.nodeName.toLowerCase();
						return ('input' === e && !!t.checked) || ('option' === e && !!t.selected);
					},
					selected: function(t) {
						return t.parentNode && t.parentNode.selectedIndex, !0 === t.selected;
					},
					empty: function(t) {
						for (t = t.firstChild; t; t = t.nextSibling) if (t.nodeType < 6) return !1;
						return !0;
					},
					parent: function(t) {
						return !i.pseudos.empty(t);
					},
					header: function(t) {
						return K.test(t.nodeName);
					},
					input: function(t) {
						return X.test(t.nodeName);
					},
					button: function(t) {
						var e = t.nodeName.toLowerCase();
						return ('input' === e && 'button' === t.type) || 'button' === e;
					},
					text: function(t) {
						var e;
						return (
							'input' === t.nodeName.toLowerCase() &&
							'text' === t.type &&
							(null == (e = t.getAttribute('type')) || 'text' === e.toLowerCase())
						);
					},
					first: ht(function() {
						return [ 0 ];
					}),
					last: ht(function(t, e) {
						return [ e - 1 ];
					}),
					eq: ht(function(t, e, n) {
						return [ n < 0 ? n + e : n ];
					}),
					even: ht(function(t, e) {
						for (var n = 0; n < e; n += 2) t.push(n);
						return t;
					}),
					odd: ht(function(t, e) {
						for (var n = 1; n < e; n += 2) t.push(n);
						return t;
					}),
					lt: ht(function(t, e, n) {
						for (var i = n < 0 ? n + e : n; 0 <= --i; ) t.push(i);
						return t;
					}),
					gt: ht(function(t, e, n) {
						for (var i = n < 0 ? n + e : n; ++i < e; ) t.push(i);
						return t;
					})
				}
			}).pseudos.nth =
				i.pseudos.eq),
			{ radio: !0, checkbox: !0, file: !0, password: !0, image: !0 }))
				i.pseudos[e] = ut(e);
			for (e in { submit: !0, reset: !0 }) i.pseudos[e] = dt(e);
			function ft() {}
			function mt(t) {
				for (var e = 0, n = t.length, i = ''; e < n; e++) i += t[e].value;
				return i;
			}
			function gt(t, e, n) {
				var i = e.dir,
					o = n && 'parentNode' === i,
					r = $++;
				return e.first
					? function(e, n, r) {
							for (; (e = e[i]); ) if (1 === e.nodeType || o) return t(e, n, r);
						}
					: function(e, n, s) {
							var a,
								l,
								c = [ k, r ];
							if (s) {
								for (; (e = e[i]); ) if ((1 === e.nodeType || o) && t(e, n, s)) return !0;
							} else
								for (; (e = e[i]); )
									if (1 === e.nodeType || o) {
										if ((a = (l = e[w] || (e[w] = {}))[i]) && a[0] === k && a[1] === r)
											return (c[2] = a[2]);
										if (((l[i] = c)[2] = t(e, n, s))) return !0;
									}
						};
			}
			function vt(t) {
				return 1 < t.length
					? function(e, n, i) {
							for (var o = t.length; o--; ) if (!t[o](e, n, i)) return !1;
							return !0;
						}
					: t[0];
			}
			function bt(t, e, n, i, o) {
				for (var r, s = [], a = 0, l = t.length, c = null != e; a < l; a++)
					(r = t[a]) && (!n || n(r, i, o)) && (s.push(r), c && e.push(a));
				return s;
			}
			function yt(t, e, n, i, o, r) {
				return (
					i && !i[w] && (i = yt(i)),
					o && !o[w] && (o = yt(o, r)),
					st(function(r, s, a, l) {
						var c,
							u,
							d,
							h = [],
							p = [],
							f = s.length,
							m =
								r ||
								(function(t, e, n) {
									for (var i = 0, o = e.length; i < o; i++) ot(t, e[i], n);
									return n;
								})(e || '*', a.nodeType ? [ a ] : a, []),
							g = !t || (!r && e) ? m : bt(m, h, t, a, l),
							v = n ? (o || (r ? t : f || i) ? [] : s) : g;
						if ((n && n(g, v, a, l), i))
							for (c = bt(v, p), i(c, [], a, l), u = c.length; u--; )
								(d = c[u]) && (v[p[u]] = !(g[p[u]] = d));
						if (r) {
							if (o || t) {
								if (o) {
									for (c = [], u = v.length; u--; ) (d = v[u]) && c.push((g[u] = d));
									o(null, (v = []), c, l);
								}
								for (u = v.length; u--; )
									(d = v[u]) && -1 < (c = o ? D(r, d) : h[u]) && (r[c] = !(s[c] = d));
							}
						} else (v = bt(v === s ? v.splice(f, v.length) : v)), o ? o(null, s, v, l) : I.apply(s, v);
					})
				);
			}
			function wt(t) {
				for (
					var e,
						n,
						o,
						r = t.length,
						s = i.relative[t[0].type],
						a = s || i.relative[' '],
						l = s ? 1 : 0,
						u = gt(
							function(t) {
								return t === e;
							},
							a,
							!0
						),
						d = gt(
							function(t) {
								return -1 < D(e, t);
							},
							a,
							!0
						),
						h = [
							function(t, n, i) {
								var o = (!s && (i || n !== c)) || ((e = n).nodeType ? u(t, n, i) : d(t, n, i));
								return (e = null), o;
							}
						];
					l < r;
					l++
				)
					if ((n = i.relative[t[l].type])) h = [ gt(vt(h), n) ];
					else {
						if ((n = i.filter[t[l].type].apply(null, t[l].matches))[w]) {
							for (o = ++l; o < r && !i.relative[t[o].type]; o++);
							return yt(
								1 < l && vt(h),
								1 < l &&
									mt(t.slice(0, l - 1).concat({ value: ' ' === t[l - 2].type ? '*' : '' })).replace(
										H,
										'$1'
									),
								n,
								l < o && wt(t.slice(l, o)),
								o < r && wt((t = t.slice(o))),
								o < r && mt(t)
							);
						}
						h.push(n);
					}
				return vt(h);
			}
			return (
				(ft.prototype = i.filters = i.pseudos),
				(i.setFilters = new ft()),
				(s = ot.tokenize = function(t, e) {
					var n,
						o,
						r,
						s,
						a,
						l,
						c,
						u = E[t + ' '];
					if (u) return e ? 0 : u.slice(0);
					for (a = t, l = [], c = i.preFilter; a; ) {
						for (s in ((!n || (o = G.exec(a))) && (o && (a = a.slice(o[0].length) || a), l.push((r = []))),
						(n = !1),
						(o = F.exec(a)) &&
							((n = o.shift()),
							r.push({ value: n, type: o[0].replace(H, ' ') }),
							(a = a.slice(n.length))),
						i.filter))
							!(o = Q[s].exec(a)) ||
								(c[s] && !(o = c[s](o))) ||
								((n = o.shift()), r.push({ value: n, type: s, matches: o }), (a = a.slice(n.length)));
						if (!n) break;
					}
					return e ? a.length : a ? ot.error(t) : E(t, l).slice(0);
				}),
				(a = ot.compile = function(t, e) {
					var n,
						o,
						r,
						a,
						l,
						u,
						d = [],
						h = [],
						f = C[t + ' '];
					if (!f) {
						for (e || (e = s(t)), n = e.length; n--; ) (f = wt(e[n]))[w] ? d.push(f) : h.push(f);
						(f = C(
							t,
							((o = h),
							(a = 0 < (r = d).length),
							(l = 0 < o.length),
							(u = function(t, e, n, s, u) {
								var d,
									h,
									f,
									m = 0,
									g = '0',
									v = t && [],
									b = [],
									y = c,
									w = t || (l && i.find.TAG('*', u)),
									x = (k += null == y ? 1 : Math.random() || 0.1),
									$ = w.length;
								for (u && (c = e !== p && e); g !== $ && null != (d = w[g]); g++) {
									if (l && d) {
										for (h = 0; (f = o[h++]); )
											if (f(d, e, n)) {
												s.push(d);
												break;
											}
										u && (k = x);
									}
									a && ((d = !f && d) && m--, t && v.push(d));
								}
								if (((m += g), a && g !== m)) {
									for (h = 0; (f = r[h++]); ) f(v, b, e, n);
									if (t) {
										if (0 < m) for (; g--; ) v[g] || b[g] || (b[g] = N.call(s));
										b = bt(b);
									}
									I.apply(s, b), u && !t && 0 < b.length && 1 < m + r.length && ot.uniqueSort(s);
								}
								return u && ((k = x), (c = y)), v;
							}),
							a ? st(u) : u)
						)).selector = t;
					}
					return f;
				}),
				(l = ot.select = function(t, e, o, r) {
					var l,
						c,
						u,
						d,
						h,
						p = 'function' == typeof t && t,
						f = !r && s((t = p.selector || t));
					if (((o = o || []), 1 === f.length)) {
						if (
							2 < (c = f[0] = f[0].slice(0)).length &&
							'ID' === (u = c[0]).type &&
							n.getById &&
							9 === e.nodeType &&
							m &&
							i.relative[c[1].type]
						) {
							if (!(e = (i.find.ID(u.matches[0].replace(et, nt), e) || [])[0])) return o;
							p && (e = e.parentNode), (t = t.slice(c.shift().value.length));
						}
						for (
							l = Q.needsContext.test(t) ? 0 : c.length;
							l-- && ((u = c[l]), !i.relative[(d = u.type)]);

						)
							if (
								(h = i.find[d]) &&
								(r = h(u.matches[0].replace(et, nt), (Z.test(c[0].type) && pt(e.parentNode)) || e))
							) {
								if ((c.splice(l, 1), !(t = r.length && mt(c)))) return I.apply(o, r), o;
								break;
							}
					}
					return (p || a(t, f))(r, e, !m, o, (Z.test(t) && pt(e.parentNode)) || e), o;
				}),
				(n.sortStable = w.split('').sort(_).join('') === w),
				(n.detectDuplicates = !!d),
				h(),
				(n.sortDetached = at(function(t) {
					return 1 & t.compareDocumentPosition(p.createElement('div'));
				})),
				at(function(t) {
					return (t.innerHTML = "<a href='#'></a>"), '#' === t.firstChild.getAttribute('href');
				}) ||
					lt('type|href|height|width', function(t, e, n) {
						return n ? void 0 : t.getAttribute(e, 'type' === e.toLowerCase() ? 1 : 2);
					}),
				(n.attributes &&
					at(function(t) {
						return (
							(t.innerHTML = '<input/>'),
							t.firstChild.setAttribute('value', ''),
							'' === t.firstChild.getAttribute('value')
						);
					})) ||
					lt('value', function(t, e, n) {
						return n || 'input' !== t.nodeName.toLowerCase() ? void 0 : t.defaultValue;
					}),
				at(function(t) {
					return null == t.getAttribute('disabled');
				}) ||
					lt(O, function(t, e, n) {
						var i;
						return n
							? void 0
							: !0 === t[e]
								? e.toLowerCase()
								: (i = t.getAttributeNode(e)) && i.specified ? i.value : null;
					}),
				ot
			);
		})(t);
		(h.find = b),
			(h.expr = b.selectors),
			(h.expr[':'] = h.expr.pseudos),
			(h.unique = b.uniqueSort),
			(h.text = b.getText),
			(h.isXMLDoc = b.isXML),
			(h.contains = b.contains);
		var y = h.expr.match.needsContext,
			w = /^<(\w+)\s*\/?>(?:<\/\1>|)$/,
			x = /^.[^:#\[\.,]*$/;
		function k(t, e, n) {
			if (h.isFunction(e))
				return h.grep(t, function(t, i) {
					return !!e.call(t, i, t) !== n;
				});
			if (e.nodeType)
				return h.grep(t, function(t) {
					return (t === e) !== n;
				});
			if ('string' == typeof e) {
				if (x.test(e)) return h.filter(e, t, n);
				e = h.filter(e, t);
			}
			return h.grep(t, function(t) {
				return 0 <= h.inArray(t, e) !== n;
			});
		}
		(h.filter = function(t, e, n) {
			var i = e[0];
			return (
				n && (t = ':not(' + t + ')'),
				1 === e.length && 1 === i.nodeType
					? h.find.matchesSelector(i, t) ? [ i ] : []
					: h.find.matches(
							t,
							h.grep(e, function(t) {
								return 1 === t.nodeType;
							})
						)
			);
		}),
			h.fn.extend({
				find: function(t) {
					var e,
						n = [],
						i = this,
						o = i.length;
					if ('string' != typeof t)
						return this.pushStack(
							h(t).filter(function() {
								for (e = 0; e < o; e++) if (h.contains(i[e], this)) return !0;
							})
						);
					for (e = 0; e < o; e++) h.find(t, i[e], n);
					return (
						((n = this.pushStack(1 < o ? h.unique(n) : n)).selector = this.selector
							? this.selector + ' ' + t
							: t),
						n
					);
				},
				filter: function(t) {
					return this.pushStack(k(this, t || [], !1));
				},
				not: function(t) {
					return this.pushStack(k(this, t || [], !0));
				},
				is: function(t) {
					return !!k(this, 'string' == typeof t && y.test(t) ? h(t) : t || [], !1).length;
				}
			});
		var $,
			T = t.document,
			E = /^(?:\s*(<[\w\W]+>)[^>]*|#([\w-]*))$/;
		((h.fn.init = function(t, e) {
			var n, i;
			if (!t) return this;
			if ('string' == typeof t) {
				if (
					!(n =
						'<' === t.charAt(0) && '>' === t.charAt(t.length - 1) && 3 <= t.length
							? [ null, t, null ]
							: E.exec(t)) ||
					(!n[1] && e)
				)
					return !e || e.jquery ? (e || $).find(t) : this.constructor(e).find(t);
				if (n[1]) {
					if (
						((e = e instanceof h ? e[0] : e),
						h.merge(this, h.parseHTML(n[1], e && e.nodeType ? e.ownerDocument || e : T, !0)),
						w.test(n[1]) && h.isPlainObject(e))
					)
						for (n in e) h.isFunction(this[n]) ? this[n](e[n]) : this.attr(n, e[n]);
					return this;
				}
				if ((i = T.getElementById(n[2])) && i.parentNode) {
					if (i.id !== n[2]) return $.find(t);
					(this.length = 1), (this[0] = i);
				}
				return (this.context = T), (this.selector = t), this;
			}
			return t.nodeType
				? ((this.context = this[0] = t), (this.length = 1), this)
				: h.isFunction(t)
					? void 0 !== $.ready ? $.ready(t) : t(h)
					: (void 0 !== t.selector && ((this.selector = t.selector), (this.context = t.context)),
						h.makeArray(t, this));
		}).prototype =
			h.fn),
			($ = h(T));
		var C = /^(?:parents|prev(?:Until|All))/,
			_ = { children: !0, contents: !0, next: !0, prev: !0 };
		function S(t, e) {
			for (; (t = t[e]) && 1 !== t.nodeType; );
			return t;
		}
		h.extend({
			dir: function(t, e, n) {
				for (var i = [], o = t[e]; o && 9 !== o.nodeType && (void 0 === n || 1 !== o.nodeType || !h(o).is(n)); )
					1 === o.nodeType && i.push(o), (o = o[e]);
				return i;
			},
			sibling: function(t, e) {
				for (var n = []; t; t = t.nextSibling) 1 === t.nodeType && t !== e && n.push(t);
				return n;
			}
		}),
			h.fn.extend({
				has: function(t) {
					var e,
						n = h(t, this),
						i = n.length;
					return this.filter(function() {
						for (e = 0; e < i; e++) if (h.contains(this, n[e])) return !0;
					});
				},
				closest: function(t, e) {
					for (
						var n,
							i = 0,
							o = this.length,
							r = [],
							s = y.test(t) || 'string' != typeof t ? h(t, e || this.context) : 0;
						i < o;
						i++
					)
						for (n = this[i]; n && n !== e; n = n.parentNode)
							if (
								n.nodeType < 11 &&
								(s ? -1 < s.index(n) : 1 === n.nodeType && h.find.matchesSelector(n, t))
							) {
								r.push(n);
								break;
							}
					return this.pushStack(1 < r.length ? h.unique(r) : r);
				},
				index: function(t) {
					return t
						? 'string' == typeof t ? h.inArray(this[0], h(t)) : h.inArray(t.jquery ? t[0] : t, this)
						: this[0] && this[0].parentNode ? this.first().prevAll().length : -1;
				},
				add: function(t, e) {
					return this.pushStack(h.unique(h.merge(this.get(), h(t, e))));
				},
				addBack: function(t) {
					return this.add(null == t ? this.prevObject : this.prevObject.filter(t));
				}
			}),
			h.each(
				{
					parent: function(t) {
						var e = t.parentNode;
						return e && 11 !== e.nodeType ? e : null;
					},
					parents: function(t) {
						return h.dir(t, 'parentNode');
					},
					parentsUntil: function(t, e, n) {
						return h.dir(t, 'parentNode', n);
					},
					next: function(t) {
						return S(t, 'nextSibling');
					},
					prev: function(t) {
						return S(t, 'previousSibling');
					},
					nextAll: function(t) {
						return h.dir(t, 'nextSibling');
					},
					prevAll: function(t) {
						return h.dir(t, 'previousSibling');
					},
					nextUntil: function(t, e, n) {
						return h.dir(t, 'nextSibling', n);
					},
					prevUntil: function(t, e, n) {
						return h.dir(t, 'previousSibling', n);
					},
					siblings: function(t) {
						return h.sibling((t.parentNode || {}).firstChild, t);
					},
					children: function(t) {
						return h.sibling(t.firstChild);
					},
					contents: function(t) {
						return h.nodeName(t, 'iframe')
							? t.contentDocument || t.contentWindow.document
							: h.merge([], t.childNodes);
					}
				},
				function(t, e) {
					h.fn[t] = function(n, i) {
						var o = h.map(this, e, n);
						return (
							'Until' !== t.slice(-5) && (i = n),
							i && 'string' == typeof i && (o = h.filter(i, o)),
							1 < this.length && (_[t] || (o = h.unique(o)), C.test(t) && (o = o.reverse())),
							this.pushStack(o)
						);
					};
				}
			);
		var j,
			N = /\S+/g,
			A = {};
		function I() {
			T.addEventListener
				? (T.removeEventListener('DOMContentLoaded', L, !1), t.removeEventListener('load', L, !1))
				: (T.detachEvent('onreadystatechange', L), t.detachEvent('onload', L));
		}
		function L() {
			(T.addEventListener || 'load' === event.type || 'complete' === T.readyState) && (I(), h.ready());
		}
		(h.Callbacks = function(t) {
			var e, n;
			t =
				'string' == typeof t
					? A[t] ||
						((n = A[(e = t)] = {}),
						h.each(e.match(N) || [], function(t, e) {
							n[e] = !0;
						}),
						n)
					: h.extend({}, t);
			var i,
				o,
				r,
				s,
				a,
				l,
				c = [],
				u = !t.once && [],
				d = function(e) {
					for (o = t.memory && e, r = !0, a = l || 0, l = 0, s = c.length, i = !0; c && a < s; a++)
						if (!1 === c[a].apply(e[0], e[1]) && t.stopOnFalse) {
							o = !1;
							break;
						}
					(i = !1), c && (u ? u.length && d(u.shift()) : o ? (c = []) : p.disable());
				},
				p = {
					add: function() {
						if (c) {
							var e = c.length;
							!(function e(n) {
								h.each(n, function(n, i) {
									var o = h.type(i);
									'function' === o
										? (t.unique && p.has(i)) || c.push(i)
										: i && i.length && 'string' !== o && e(i);
								});
							})(arguments),
								i ? (s = c.length) : o && ((l = e), d(o));
						}
						return this;
					},
					remove: function() {
						return (
							c &&
								h.each(arguments, function(t, e) {
									for (var n; -1 < (n = h.inArray(e, c, n)); )
										c.splice(n, 1), i && (n <= s && s--, n <= a && a--);
								}),
							this
						);
					},
					has: function(t) {
						return t ? -1 < h.inArray(t, c) : !(!c || !c.length);
					},
					empty: function() {
						return (c = []), (s = 0), this;
					},
					disable: function() {
						return (c = u = o = void 0), this;
					},
					disabled: function() {
						return !c;
					},
					lock: function() {
						return (u = void 0), o || p.disable(), this;
					},
					locked: function() {
						return !u;
					},
					fireWith: function(t, e) {
						return (
							!c || (r && !u) || ((e = [ t, (e = e || []).slice ? e.slice() : e ]), i ? u.push(e) : d(e)),
							this
						);
					},
					fire: function() {
						return p.fireWith(this, arguments), this;
					},
					fired: function() {
						return !!r;
					}
				};
			return p;
		}),
			h.extend({
				Deferred: function(t) {
					var e = [
							[ 'resolve', 'done', h.Callbacks('once memory'), 'resolved' ],
							[ 'reject', 'fail', h.Callbacks('once memory'), 'rejected' ],
							[ 'notify', 'progress', h.Callbacks('memory') ]
						],
						n = 'pending',
						i = {
							state: function() {
								return n;
							},
							always: function() {
								return o.done(arguments).fail(arguments), this;
							},
							then: function() {
								var t = arguments;
								return h
									.Deferred(function(n) {
										h.each(e, function(e, r) {
											var s = h.isFunction(t[e]) && t[e];
											o[r[1]](function() {
												var t = s && s.apply(this, arguments);
												t && h.isFunction(t.promise)
													? t.promise().done(n.resolve).fail(n.reject).progress(n.notify)
													: n[r[0] + 'With'](
															this === i ? n.promise() : this,
															s ? [ t ] : arguments
														);
											});
										}),
											(t = null);
									})
									.promise();
							},
							promise: function(t) {
								return null != t ? h.extend(t, i) : i;
							}
						},
						o = {};
					return (
						(i.pipe = i.then),
						h.each(e, function(t, r) {
							var s = r[2],
								a = r[3];
							(i[r[1]] = s.add),
								a &&
									s.add(
										function() {
											n = a;
										},
										e[1 ^ t][2].disable,
										e[2][2].lock
									),
								(o[r[0]] = function() {
									return o[r[0] + 'With'](this === o ? i : this, arguments), this;
								}),
								(o[r[0] + 'With'] = s.fireWith);
						}),
						i.promise(o),
						t && t.call(o, o),
						o
					);
				},
				when: function(t) {
					var e,
						n,
						o,
						r = 0,
						s = i.call(arguments),
						a = s.length,
						l = 1 !== a || (t && h.isFunction(t.promise)) ? a : 0,
						c = 1 === l ? t : h.Deferred(),
						u = function(t, n, o) {
							return function(r) {
								(n[t] = this),
									(o[t] = 1 < arguments.length ? i.call(arguments) : r),
									o === e ? c.notifyWith(n, o) : --l || c.resolveWith(n, o);
							};
						};
					if (1 < a)
						for (e = new Array(a), n = new Array(a), o = new Array(a); r < a; r++)
							s[r] && h.isFunction(s[r].promise)
								? s[r].promise().done(u(r, o, s)).fail(c.reject).progress(u(r, n, e))
								: --l;
					return l || c.resolveWith(o, s), c.promise();
				}
			}),
			(h.fn.ready = function(t) {
				return h.ready.promise().done(t), this;
			}),
			h.extend({
				isReady: !1,
				readyWait: 1,
				holdReady: function(t) {
					t ? h.readyWait++ : h.ready(!0);
				},
				ready: function(t) {
					if (!0 === t ? !--h.readyWait : !h.isReady) {
						if (!T.body) return setTimeout(h.ready);
						((h.isReady = !0) !== t && 0 < --h.readyWait) ||
							(j.resolveWith(T, [ h ]),
							h.fn.triggerHandler && (h(T).triggerHandler('ready'), h(T).off('ready')));
					}
				}
			}),
			(h.ready.promise = function(e) {
				if (!j)
					if (((j = h.Deferred()), 'complete' === T.readyState)) setTimeout(h.ready);
					else if (T.addEventListener)
						T.addEventListener('DOMContentLoaded', L, !1), t.addEventListener('load', L, !1);
					else {
						T.attachEvent('onreadystatechange', L), t.attachEvent('onload', L);
						var n = !1;
						try {
							n = null == t.frameElement && T.documentElement;
						} catch (e) {}
						n &&
							n.doScroll &&
							(function t() {
								if (!h.isReady) {
									try {
										n.doScroll('left');
									} catch (e) {
										return setTimeout(t, 50);
									}
									I(), h.ready();
								}
							})();
					}
				return j.promise(e);
			});
		var D,
			O = 'undefined';
		for (D in h(u)) break;
		(u.ownLast = '0' !== D),
			(u.inlineBlockNeedsLayout = !1),
			h(function() {
				var t, e, n, i;
				(n = T.getElementsByTagName('body')[0]) &&
					n.style &&
					((e = T.createElement('div')),
					((i = T.createElement('div')).style.cssText =
						'position:absolute;border:0;width:0;height:0;top:0;left:-9999px'),
					n.appendChild(i).appendChild(e),
					typeof e.style.zoom !== O &&
						((e.style.cssText = 'display:inline;margin:0;border:0;padding:1px;width:1px;zoom:1'),
						(u.inlineBlockNeedsLayout = t = 3 === e.offsetWidth),
						t && (n.style.zoom = 1)),
					n.removeChild(i));
			}),
			(function() {
				var t = T.createElement('div');
				if (null == u.deleteExpando) {
					u.deleteExpando = !0;
					try {
						delete t.test;
					} catch (t) {
						u.deleteExpando = !1;
					}
				}
				t = null;
			})(),
			(h.acceptData = function(t) {
				var e = h.noData[(t.nodeName + ' ').toLowerCase()],
					n = +t.nodeType || 1;
				return (1 === n || 9 === n) && (!e || (!0 !== e && t.getAttribute('classid') === e));
			});
		var P = /^(?:\{[\w\W]*\}|\[[\w\W]*\])$/,
			R = /([A-Z])/g;
		function M(t, e, n) {
			if (void 0 === n && 1 === t.nodeType) {
				var i = 'data-' + e.replace(R, '-$1').toLowerCase();
				if ('string' == typeof (n = t.getAttribute(i))) {
					try {
						n =
							'true' === n ||
							('false' !== n &&
								('null' === n ? null : +n + '' === n ? +n : P.test(n) ? h.parseJSON(n) : n));
					} catch (t) {}
					h.data(t, e, n);
				} else n = void 0;
			}
			return n;
		}
		function q(t) {
			var e;
			for (e in t) if (('data' !== e || !h.isEmptyObject(t[e])) && 'toJSON' !== e) return !1;
			return !0;
		}
		function z(t, e, i, o) {
			if (h.acceptData(t)) {
				var r,
					s,
					a = h.expando,
					l = t.nodeType,
					c = l ? h.cache : t,
					u = l ? t[a] : t[a] && a;
				if ((u && c[u] && (o || c[u].data)) || void 0 !== i || 'string' != typeof e)
					return (
						u || (u = l ? (t[a] = n.pop() || h.guid++) : a),
						c[u] || (c[u] = l ? {} : { toJSON: h.noop }),
						('object' == typeof e || 'function' == typeof e) &&
							(o ? (c[u] = h.extend(c[u], e)) : (c[u].data = h.extend(c[u].data, e))),
						(s = c[u]),
						o || (s.data || (s.data = {}), (s = s.data)),
						void 0 !== i && (s[h.camelCase(e)] = i),
						'string' == typeof e ? null == (r = s[e]) && (r = s[h.camelCase(e)]) : (r = s),
						r
					);
			}
		}
		function U(t, e, n) {
			if (h.acceptData(t)) {
				var i,
					o,
					r = t.nodeType,
					s = r ? h.cache : t,
					a = r ? t[h.expando] : h.expando;
				if (s[a]) {
					if (e && (i = n ? s[a] : s[a].data)) {
						o = (e = h.isArray(e)
							? e.concat(h.map(e, h.camelCase))
							: e in i ? [ e ] : (e = h.camelCase(e)) in i ? [ e ] : e.split(' ')).length;
						for (; o--; ) delete i[e[o]];
						if (n ? !q(i) : !h.isEmptyObject(i)) return;
					}
					(n || (delete s[a].data, q(s[a]))) &&
						(r ? h.cleanData([ t ], !0) : u.deleteExpando || s != s.window ? delete s[a] : (s[a] = null));
				}
			}
		}
		h.extend({
			cache: {},
			noData: { 'applet ': !0, 'embed ': !0, 'object ': 'clsid:D27CDB6E-AE6D-11cf-96B8-444553540000' },
			hasData: function(t) {
				return !!(t = t.nodeType ? h.cache[t[h.expando]] : t[h.expando]) && !q(t);
			},
			data: function(t, e, n) {
				return z(t, e, n);
			},
			removeData: function(t, e) {
				return U(t, e);
			},
			_data: function(t, e, n) {
				return z(t, e, n, !0);
			},
			_removeData: function(t, e) {
				return U(t, e, !0);
			}
		}),
			h.fn.extend({
				data: function(t, e) {
					var n,
						i,
						o,
						r = this[0],
						s = r && r.attributes;
					if (void 0 === t) {
						if (this.length && ((o = h.data(r)), 1 === r.nodeType && !h._data(r, 'parsedAttrs'))) {
							for (n = s.length; n--; )
								s[n] &&
									0 === (i = s[n].name).indexOf('data-') &&
									M(r, (i = h.camelCase(i.slice(5))), o[i]);
							h._data(r, 'parsedAttrs', !0);
						}
						return o;
					}
					return 'object' == typeof t
						? this.each(function() {
								h.data(this, t);
							})
						: 1 < arguments.length
							? this.each(function() {
									h.data(this, t, e);
								})
							: r ? M(r, t, h.data(r, t)) : void 0;
				},
				removeData: function(t) {
					return this.each(function() {
						h.removeData(this, t);
					});
				}
			}),
			h.extend({
				queue: function(t, e, n) {
					var i;
					return t
						? ((e = (e || 'fx') + 'queue'),
							(i = h._data(t, e)),
							n && (!i || h.isArray(n) ? (i = h._data(t, e, h.makeArray(n))) : i.push(n)),
							i || [])
						: void 0;
				},
				dequeue: function(t, e) {
					e = e || 'fx';
					var n = h.queue(t, e),
						i = n.length,
						o = n.shift(),
						r = h._queueHooks(t, e);
					'inprogress' === o && ((o = n.shift()), i--),
						o &&
							('fx' === e && n.unshift('inprogress'),
							delete r.stop,
							o.call(
								t,
								function() {
									h.dequeue(t, e);
								},
								r
							)),
						!i && r && r.empty.fire();
				},
				_queueHooks: function(t, e) {
					var n = e + 'queueHooks';
					return (
						h._data(t, n) ||
						h._data(t, n, {
							empty: h.Callbacks('once memory').add(function() {
								h._removeData(t, e + 'queue'), h._removeData(t, n);
							})
						})
					);
				}
			}),
			h.fn.extend({
				queue: function(t, e) {
					var n = 2;
					return (
						'string' != typeof t && ((e = t), (t = 'fx'), n--),
						arguments.length < n
							? h.queue(this[0], t)
							: void 0 === e
								? this
								: this.each(function() {
										var n = h.queue(this, t, e);
										h._queueHooks(this, t), 'fx' === t && 'inprogress' !== n[0] && h.dequeue(this, t);
									})
					);
				},
				dequeue: function(t) {
					return this.each(function() {
						h.dequeue(this, t);
					});
				},
				clearQueue: function(t) {
					return this.queue(t || 'fx', []);
				},
				promise: function(t, e) {
					var n,
						i = 1,
						o = h.Deferred(),
						r = this,
						s = this.length,
						a = function() {
							--i || o.resolveWith(r, [ r ]);
						};
					for ('string' != typeof t && ((e = t), (t = void 0)), t = t || 'fx'; s--; )
						(n = h._data(r[s], t + 'queueHooks')) && n.empty && (i++, n.empty.add(a));
					return a(), o.promise(e);
				}
			});
		var H = /[+-]?(?:\d*\.|)\d+(?:[eE][+-]?\d+|)/.source,
			G = [ 'Top', 'Right', 'Bottom', 'Left' ],
			F = function(t, e) {
				return (t = e || t), 'none' === h.css(t, 'display') || !h.contains(t.ownerDocument, t);
			},
			B = (h.access = function(t, e, n, i, o, r, s) {
				var a = 0,
					l = t.length,
					c = null == n;
				if ('object' === h.type(n)) for (a in ((o = !0), n)) h.access(t, e, a, n[a], !0, r, s);
				else if (
					void 0 !== i &&
					((o = !0),
					h.isFunction(i) || (s = !0),
					c &&
						(s
							? (e.call(t, i), (e = null))
							: ((c = e),
								(e = function(t, e, n) {
									return c.call(h(t), n);
								}))),
					e)
				)
					for (; a < l; a++) e(t[a], n, s ? i : i.call(t[a], a, e(t[a], n)));
				return o ? t : c ? e.call(t) : l ? e(t[0], n) : r;
			}),
			W = /^(?:checkbox|radio)$/i;
		!(function() {
			var t = T.createElement('input'),
				e = T.createElement('div'),
				n = T.createDocumentFragment();
			if (
				((e.innerHTML = "  <link/><table></table><a href='/a'>a</a><input type='checkbox'/>"),
				(u.leadingWhitespace = 3 === e.firstChild.nodeType),
				(u.tbody = !e.getElementsByTagName('tbody').length),
				(u.htmlSerialize = !!e.getElementsByTagName('link').length),
				(u.html5Clone = '<:nav></:nav>' !== T.createElement('nav').cloneNode(!0).outerHTML),
				(t.type = 'checkbox'),
				(t.checked = !0),
				n.appendChild(t),
				(u.appendChecked = t.checked),
				(e.innerHTML = '<textarea>x</textarea>'),
				(u.noCloneChecked = !!e.cloneNode(!0).lastChild.defaultValue),
				n.appendChild(e),
				(e.innerHTML = "<input type='radio' checked='checked' name='t'/>"),
				(u.checkClone = e.cloneNode(!0).cloneNode(!0).lastChild.checked),
				(u.noCloneEvent = !0),
				e.attachEvent &&
					(e.attachEvent('onclick', function() {
						u.noCloneEvent = !1;
					}),
					e.cloneNode(!0).click()),
				null == u.deleteExpando)
			) {
				u.deleteExpando = !0;
				try {
					delete e.test;
				} catch (t) {
					u.deleteExpando = !1;
				}
			}
		})(),
			(function() {
				var e,
					n,
					i = T.createElement('div');
				for (e in { submit: !0, change: !0, focusin: !0 })
					(n = 'on' + e),
						(u[e + 'Bubbles'] = n in t) ||
							(i.setAttribute(n, 't'), (u[e + 'Bubbles'] = !1 === i.attributes[n].expando));
				i = null;
			})();
		var V = /^(?:input|select|textarea)$/i,
			Q = /^key/,
			X = /^(?:mouse|pointer|contextmenu)|click/,
			K = /^(?:focusinfocus|focusoutblur)$/,
			Y = /^([^.]*)(?:\.(.+)|)$/;
		function J() {
			return !0;
		}
		function Z() {
			return !1;
		}
		function tt() {
			try {
				return T.activeElement;
			} catch (t) {}
		}
		function et(t) {
			var e = nt.split('|'),
				n = t.createDocumentFragment();
			if (n.createElement) for (; e.length; ) n.createElement(e.pop());
			return n;
		}
		(h.event = {
			global: {},
			add: function(t, e, n, i, o) {
				var r,
					s,
					a,
					l,
					c,
					u,
					d,
					p,
					f,
					m,
					g,
					v = h._data(t);
				if (v) {
					for (
						n.handler && ((n = (l = n).handler), (o = l.selector)),
							n.guid || (n.guid = h.guid++),
							(s = v.events) || (s = v.events = {}),
							(u = v.handle) ||
								((u = v.handle = function(t) {
									return typeof h === O || (t && h.event.triggered === t.type)
										? void 0
										: h.event.dispatch.apply(u.elem, arguments);
								}).elem = t),
							a = (e = (e || '').match(N) || [ '' ]).length;
						a--;

					)
						(f = g = (r = Y.exec(e[a]) || [])[1]),
							(m = (r[2] || '').split('.').sort()),
							f &&
								((c = h.event.special[f] || {}),
								(f = (o ? c.delegateType : c.bindType) || f),
								(c = h.event.special[f] || {}),
								(d = h.extend(
									{
										type: f,
										origType: g,
										data: i,
										handler: n,
										guid: n.guid,
										selector: o,
										needsContext: o && h.expr.match.needsContext.test(o),
										namespace: m.join('.')
									},
									l
								)),
								(p = s[f]) ||
									(((p = s[f] = []).delegateCount = 0),
									(c.setup && !1 !== c.setup.call(t, i, m, u)) ||
										(t.addEventListener
											? t.addEventListener(f, u, !1)
											: t.attachEvent && t.attachEvent('on' + f, u))),
								c.add && (c.add.call(t, d), d.handler.guid || (d.handler.guid = n.guid)),
								o ? p.splice(p.delegateCount++, 0, d) : p.push(d),
								(h.event.global[f] = !0));
					t = null;
				}
			},
			remove: function(t, e, n, i, o) {
				var r,
					s,
					a,
					l,
					c,
					u,
					d,
					p,
					f,
					m,
					g,
					v = h.hasData(t) && h._data(t);
				if (v && (u = v.events)) {
					for (c = (e = (e || '').match(N) || [ '' ]).length; c--; )
						if (((f = g = (a = Y.exec(e[c]) || [])[1]), (m = (a[2] || '').split('.').sort()), f)) {
							for (
								d = h.event.special[f] || {},
									p = u[(f = (i ? d.delegateType : d.bindType) || f)] || [],
									a = a[2] && new RegExp('(^|\\.)' + m.join('\\.(?:.*\\.|)') + '(\\.|$)'),
									l = r = p.length;
								r--;

							)
								(s = p[r]),
									(!o && g !== s.origType) ||
										(n && n.guid !== s.guid) ||
										(a && !a.test(s.namespace)) ||
										(i && i !== s.selector && ('**' !== i || !s.selector)) ||
										(p.splice(r, 1),
										s.selector && p.delegateCount--,
										d.remove && d.remove.call(t, s));
							l &&
								!p.length &&
								((d.teardown && !1 !== d.teardown.call(t, m, v.handle)) ||
									h.removeEvent(t, f, v.handle),
								delete u[f]);
						} else for (f in u) h.event.remove(t, f + e[c], n, i, !0);
					h.isEmptyObject(u) && (delete v.handle, h._removeData(t, 'events'));
				}
			},
			trigger: function(e, n, i, o) {
				var r,
					s,
					a,
					l,
					u,
					d,
					p,
					f = [ i || T ],
					m = c.call(e, 'type') ? e.type : e,
					g = c.call(e, 'namespace') ? e.namespace.split('.') : [];
				if (
					((a = d = i = i || T),
					3 !== i.nodeType &&
						8 !== i.nodeType &&
						!K.test(m + h.event.triggered) &&
						(0 <= m.indexOf('.') && ((m = (g = m.split('.')).shift()), g.sort()),
						(s = m.indexOf(':') < 0 && 'on' + m),
						((e = e[h.expando] ? e : new h.Event(m, 'object' == typeof e && e)).isTrigger = o ? 2 : 3),
						(e.namespace = g.join('.')),
						(e.namespace_re = e.namespace
							? new RegExp('(^|\\.)' + g.join('\\.(?:.*\\.|)') + '(\\.|$)')
							: null),
						(e.result = void 0),
						e.target || (e.target = i),
						(n = null == n ? [ e ] : h.makeArray(n, [ e ])),
						(u = h.event.special[m] || {}),
						o || !u.trigger || !1 !== u.trigger.apply(i, n)))
				) {
					if (!o && !u.noBubble && !h.isWindow(i)) {
						for (l = u.delegateType || m, K.test(l + m) || (a = a.parentNode); a; a = a.parentNode)
							f.push(a), (d = a);
						d === (i.ownerDocument || T) && f.push(d.defaultView || d.parentWindow || t);
					}
					for (p = 0; (a = f[p++]) && !e.isPropagationStopped(); )
						(e.type = 1 < p ? l : u.bindType || m),
							(r = (h._data(a, 'events') || {})[e.type] && h._data(a, 'handle')) && r.apply(a, n),
							(r = s && a[s]) &&
								r.apply &&
								h.acceptData(a) &&
								((e.result = r.apply(a, n)), !1 === e.result && e.preventDefault());
					if (
						((e.type = m),
						!o &&
							!e.isDefaultPrevented() &&
							(!u._default || !1 === u._default.apply(f.pop(), n)) &&
							h.acceptData(i) &&
							s &&
							i[m] &&
							!h.isWindow(i))
					) {
						(d = i[s]) && (i[s] = null), (h.event.triggered = m);
						try {
							i[m]();
						} catch (e) {}
						(h.event.triggered = void 0), d && (i[s] = d);
					}
					return e.result;
				}
			},
			dispatch: function(t) {
				t = h.event.fix(t);
				var e,
					n,
					o,
					r,
					s,
					a = [],
					l = i.call(arguments),
					c = (h._data(this, 'events') || {})[t.type] || [],
					u = h.event.special[t.type] || {};
				if ((((l[0] = t).delegateTarget = this), !u.preDispatch || !1 !== u.preDispatch.call(this, t))) {
					for (a = h.event.handlers.call(this, t, c), e = 0; (r = a[e++]) && !t.isPropagationStopped(); )
						for (
							t.currentTarget = r.elem, s = 0;
							(o = r.handlers[s++]) && !t.isImmediatePropagationStopped();

						)
							(!t.namespace_re || t.namespace_re.test(o.namespace)) &&
								((t.handleObj = o),
								(t.data = o.data),
								void 0 !==
									(n = ((h.event.special[o.origType] || {}).handle || o.handler).apply(r.elem, l)) &&
									!1 === (t.result = n) &&
									(t.preventDefault(), t.stopPropagation()));
					return u.postDispatch && u.postDispatch.call(this, t), t.result;
				}
			},
			handlers: function(t, e) {
				var n,
					i,
					o,
					r,
					s = [],
					a = e.delegateCount,
					l = t.target;
				if (a && l.nodeType && (!t.button || 'click' !== t.type))
					for (; l != this; l = l.parentNode || this)
						if (1 === l.nodeType && (!0 !== l.disabled || 'click' !== t.type)) {
							for (o = [], r = 0; r < a; r++)
								void 0 === o[(n = (i = e[r]).selector + ' ')] &&
									(o[n] = i.needsContext
										? 0 <= h(n, this).index(l)
										: h.find(n, this, null, [ l ]).length),
									o[n] && o.push(i);
							o.length && s.push({ elem: l, handlers: o });
						}
				return a < e.length && s.push({ elem: this, handlers: e.slice(a) }), s;
			},
			fix: function(t) {
				if (t[h.expando]) return t;
				var e,
					n,
					i,
					o = t.type,
					r = t,
					s = this.fixHooks[o];
				for (
					s || (this.fixHooks[o] = s = X.test(o) ? this.mouseHooks : Q.test(o) ? this.keyHooks : {}),
						i = s.props ? this.props.concat(s.props) : this.props,
						t = new h.Event(r),
						e = i.length;
					e--;

				)
					t[(n = i[e])] = r[n];
				return (
					t.target || (t.target = r.srcElement || T),
					3 === t.target.nodeType && (t.target = t.target.parentNode),
					(t.metaKey = !!t.metaKey),
					s.filter ? s.filter(t, r) : t
				);
			},
			props: 'altKey bubbles cancelable ctrlKey currentTarget eventPhase metaKey relatedTarget shiftKey target timeStamp view which'.split(
				' '
			),
			fixHooks: {},
			keyHooks: {
				props: 'char charCode key keyCode'.split(' '),
				filter: function(t, e) {
					return null == t.which && (t.which = null != e.charCode ? e.charCode : e.keyCode), t;
				}
			},
			mouseHooks: {
				props: 'button buttons clientX clientY fromElement offsetX offsetY pageX pageY screenX screenY toElement'.split(
					' '
				),
				filter: function(t, e) {
					var n,
						i,
						o,
						r = e.button,
						s = e.fromElement;
					return (
						null == t.pageX &&
							null != e.clientX &&
							((o = (i = t.target.ownerDocument || T).documentElement),
							(n = i.body),
							(t.pageX =
								e.clientX +
								((o && o.scrollLeft) || (n && n.scrollLeft) || 0) -
								((o && o.clientLeft) || (n && n.clientLeft) || 0)),
							(t.pageY =
								e.clientY +
								((o && o.scrollTop) || (n && n.scrollTop) || 0) -
								((o && o.clientTop) || (n && n.clientTop) || 0))),
						!t.relatedTarget && s && (t.relatedTarget = s === t.target ? e.toElement : s),
						t.which || void 0 === r || (t.which = 1 & r ? 1 : 2 & r ? 3 : 4 & r ? 2 : 0),
						t
					);
				}
			},
			special: {
				load: { noBubble: !0 },
				focus: {
					trigger: function() {
						if (this !== tt() && this.focus)
							try {
								return this.focus(), !1;
							} catch (t) {}
					},
					delegateType: 'focusin'
				},
				blur: {
					trigger: function() {
						return this === tt() && this.blur ? (this.blur(), !1) : void 0;
					},
					delegateType: 'focusout'
				},
				click: {
					trigger: function() {
						return h.nodeName(this, 'input') && 'checkbox' === this.type && this.click
							? (this.click(), !1)
							: void 0;
					},
					_default: function(t) {
						return h.nodeName(t.target, 'a');
					}
				},
				beforeunload: {
					postDispatch: function(t) {
						void 0 !== t.result && t.originalEvent && (t.originalEvent.returnValue = t.result);
					}
				}
			},
			simulate: function(t, e, n, i) {
				var o = h.extend(new h.Event(), n, { type: t, isSimulated: !0, originalEvent: {} });
				i ? h.event.trigger(o, null, e) : h.event.dispatch.call(e, o),
					o.isDefaultPrevented() && n.preventDefault();
			}
		}),
			(h.removeEvent = T.removeEventListener
				? function(t, e, n) {
						t.removeEventListener && t.removeEventListener(e, n, !1);
					}
				: function(t, e, n) {
						var i = 'on' + e;
						t.detachEvent && (typeof t[i] === O && (t[i] = null), t.detachEvent(i, n));
					}),
			(h.Event = function(t, e) {
				return this instanceof h.Event
					? (t && t.type
							? ((this.originalEvent = t),
								(this.type = t.type),
								(this.isDefaultPrevented =
									t.defaultPrevented || (void 0 === t.defaultPrevented && !1 === t.returnValue) ? J : Z))
							: (this.type = t),
						e && h.extend(this, e),
						(this.timeStamp = (t && t.timeStamp) || h.now()),
						void (this[h.expando] = !0))
					: new h.Event(t, e);
			}),
			(h.Event.prototype = {
				isDefaultPrevented: Z,
				isPropagationStopped: Z,
				isImmediatePropagationStopped: Z,
				preventDefault: function() {
					var t = this.originalEvent;
					(this.isDefaultPrevented = J), t && (t.preventDefault ? t.preventDefault() : (t.returnValue = !1));
				},
				stopPropagation: function() {
					var t = this.originalEvent;
					(this.isPropagationStopped = J),
						t && (t.stopPropagation && t.stopPropagation(), (t.cancelBubble = !0));
				},
				stopImmediatePropagation: function() {
					var t = this.originalEvent;
					(this.isImmediatePropagationStopped = J),
						t && t.stopImmediatePropagation && t.stopImmediatePropagation(),
						this.stopPropagation();
				}
			}),
			h.each(
				{
					mouseenter: 'mouseover',
					mouseleave: 'mouseout',
					pointerenter: 'pointerover',
					pointerleave: 'pointerout'
				},
				function(t, e) {
					h.event.special[t] = {
						delegateType: e,
						bindType: e,
						handle: function(t) {
							var n,
								i = t.relatedTarget,
								o = t.handleObj;
							return (
								(!i || (i !== this && !h.contains(this, i))) &&
									((t.type = o.origType), (n = o.handler.apply(this, arguments)), (t.type = e)),
								n
							);
						}
					};
				}
			),
			u.submitBubbles ||
				(h.event.special.submit = {
					setup: function() {
						return (
							!h.nodeName(this, 'form') &&
							void h.event.add(this, 'click._submit keypress._submit', function(t) {
								var e = t.target,
									n = h.nodeName(e, 'input') || h.nodeName(e, 'button') ? e.form : void 0;
								n &&
									!h._data(n, 'submitBubbles') &&
									(h.event.add(n, 'submit._submit', function(t) {
										t._submit_bubble = !0;
									}),
									h._data(n, 'submitBubbles', !0));
							})
						);
					},
					postDispatch: function(t) {
						t._submit_bubble &&
							(delete t._submit_bubble,
							this.parentNode && !t.isTrigger && h.event.simulate('submit', this.parentNode, t, !0));
					},
					teardown: function() {
						return !h.nodeName(this, 'form') && void h.event.remove(this, '._submit');
					}
				}),
			u.changeBubbles ||
				(h.event.special.change = {
					setup: function() {
						return V.test(this.nodeName)
							? (('checkbox' === this.type || 'radio' === this.type) &&
									(h.event.add(this, 'propertychange._change', function(t) {
										'checked' === t.originalEvent.propertyName && (this._just_changed = !0);
									}),
									h.event.add(this, 'click._change', function(t) {
										this._just_changed && !t.isTrigger && (this._just_changed = !1),
											h.event.simulate('change', this, t, !0);
									})),
								!1)
							: void h.event.add(this, 'beforeactivate._change', function(t) {
									var e = t.target;
									V.test(e.nodeName) &&
										!h._data(e, 'changeBubbles') &&
										(h.event.add(e, 'change._change', function(t) {
											!this.parentNode ||
												t.isSimulated ||
												t.isTrigger ||
												h.event.simulate('change', this.parentNode, t, !0);
										}),
										h._data(e, 'changeBubbles', !0));
								});
					},
					handle: function(t) {
						var e = t.target;
						return this !== e ||
						t.isSimulated ||
						t.isTrigger ||
						('radio' !== e.type && 'checkbox' !== e.type)
							? t.handleObj.handler.apply(this, arguments)
							: void 0;
					},
					teardown: function() {
						return h.event.remove(this, '._change'), !V.test(this.nodeName);
					}
				}),
			u.focusinBubbles ||
				h.each({ focus: 'focusin', blur: 'focusout' }, function(t, e) {
					var n = function(t) {
						h.event.simulate(e, t.target, h.event.fix(t), !0);
					};
					h.event.special[e] = {
						setup: function() {
							var i = this.ownerDocument || this,
								o = h._data(i, e);
							o || i.addEventListener(t, n, !0), h._data(i, e, (o || 0) + 1);
						},
						teardown: function() {
							var i = this.ownerDocument || this,
								o = h._data(i, e) - 1;
							o ? h._data(i, e, o) : (i.removeEventListener(t, n, !0), h._removeData(i, e));
						}
					};
				}),
			h.fn.extend({
				on: function(t, e, n, i, o) {
					var r, s;
					if ('object' == typeof t) {
						for (r in ('string' != typeof e && ((n = n || e), (e = void 0)), t)) this.on(r, e, n, t[r], o);
						return this;
					}
					if (
						(null == n && null == i
							? ((i = e), (n = e = void 0))
							: null == i &&
								('string' == typeof e ? ((i = n), (n = void 0)) : ((i = n), (n = e), (e = void 0))),
						!1 === i)
					)
						i = Z;
					else if (!i) return this;
					return (
						1 === o &&
							((s = i),
							((i = function(t) {
								return h().off(t), s.apply(this, arguments);
							}).guid =
								s.guid || (s.guid = h.guid++))),
						this.each(function() {
							h.event.add(this, t, i, n, e);
						})
					);
				},
				one: function(t, e, n, i) {
					return this.on(t, e, n, i, 1);
				},
				off: function(t, e, n) {
					var i, o;
					if (t && t.preventDefault && t.handleObj)
						return (
							(i = t.handleObj),
							h(t.delegateTarget).off(
								i.namespace ? i.origType + '.' + i.namespace : i.origType,
								i.selector,
								i.handler
							),
							this
						);
					if ('object' == typeof t) {
						for (o in t) this.off(o, e, t[o]);
						return this;
					}
					return (
						(!1 === e || 'function' == typeof e) && ((n = e), (e = void 0)),
						!1 === n && (n = Z),
						this.each(function() {
							h.event.remove(this, t, n, e);
						})
					);
				},
				trigger: function(t, e) {
					return this.each(function() {
						h.event.trigger(t, e, this);
					});
				},
				triggerHandler: function(t, e) {
					var n = this[0];
					return n ? h.event.trigger(t, e, n, !0) : void 0;
				}
			});
		var nt =
				'abbr|article|aside|audio|bdi|canvas|data|datalist|details|figcaption|figure|footer|header|hgroup|mark|meter|nav|output|progress|section|summary|time|video',
			it = / jQuery\d+="(?:null|\d+)"/g,
			ot = new RegExp('<(?:' + nt + ')[\\s/>]', 'i'),
			rt = /^\s+/,
			st = /<(?!area|br|col|embed|hr|img|input|link|meta|param)(([\w:]+)[^>]*)\/>/gi,
			at = /<([\w:]+)/,
			lt = /<tbody/i,
			ct = /<|&#?\w+;/,
			ut = /<(?:script|style|link)/i,
			dt = /checked\s*(?:[^=]|=\s*.checked.)/i,
			ht = /^$|\/(?:java|ecma)script/i,
			pt = /^true\/(.*)/,
			ft = /^\s*<!(?:\[CDATA\[|--)|(?:\]\]|--)>\s*$/g,
			mt = {
				option: [ 1, "<select multiple='multiple'>", '</select>' ],
				legend: [ 1, '<fieldset>', '</fieldset>' ],
				area: [ 1, '<map>', '</map>' ],
				param: [ 1, '<object>', '</object>' ],
				thead: [ 1, '<table>', '</table>' ],
				tr: [ 2, '<table><tbody>', '</tbody></table>' ],
				col: [ 2, '<table><tbody></tbody><colgroup>', '</colgroup></table>' ],
				td: [ 3, '<table><tbody><tr>', '</tr></tbody></table>' ],
				_default: u.htmlSerialize ? [ 0, '', '' ] : [ 1, 'X<div>', '</div>' ]
			},
			gt = et(T).appendChild(T.createElement('div'));
		function vt(t, e) {
			var n,
				i,
				o = 0,
				r =
					typeof t.getElementsByTagName !== O
						? t.getElementsByTagName(e || '*')
						: typeof t.querySelectorAll !== O ? t.querySelectorAll(e || '*') : void 0;
			if (!r)
				for (r = [], n = t.childNodes || t; null != (i = n[o]); o++)
					!e || h.nodeName(i, e) ? r.push(i) : h.merge(r, vt(i, e));
			return void 0 === e || (e && h.nodeName(t, e)) ? h.merge([ t ], r) : r;
		}
		function bt(t) {
			W.test(t.type) && (t.defaultChecked = t.checked);
		}
		function yt(t, e) {
			return h.nodeName(t, 'table') && h.nodeName(11 !== e.nodeType ? e : e.firstChild, 'tr')
				? t.getElementsByTagName('tbody')[0] || t.appendChild(t.ownerDocument.createElement('tbody'))
				: t;
		}
		function wt(t) {
			return (t.type = (null !== h.find.attr(t, 'type')) + '/' + t.type), t;
		}
		function xt(t) {
			var e = pt.exec(t.type);
			return e ? (t.type = e[1]) : t.removeAttribute('type'), t;
		}
		function kt(t, e) {
			for (var n, i = 0; null != (n = t[i]); i++) h._data(n, 'globalEval', !e || h._data(e[i], 'globalEval'));
		}
		function $t(t, e) {
			if (1 === e.nodeType && h.hasData(t)) {
				var n,
					i,
					o,
					r = h._data(t),
					s = h._data(e, r),
					a = r.events;
				if (a)
					for (n in (delete s.handle, (s.events = {}), a))
						for (i = 0, o = a[n].length; i < o; i++) h.event.add(e, n, a[n][i]);
				s.data && (s.data = h.extend({}, s.data));
			}
		}
		function Tt(t, e) {
			var n, i, o;
			if (1 === e.nodeType) {
				if (((n = e.nodeName.toLowerCase()), !u.noCloneEvent && e[h.expando])) {
					for (i in (o = h._data(e)).events) h.removeEvent(e, i, o.handle);
					e.removeAttribute(h.expando);
				}
				'script' === n && e.text !== t.text
					? ((wt(e).text = t.text), xt(e))
					: 'object' === n
						? (e.parentNode && (e.outerHTML = t.outerHTML),
							u.html5Clone && t.innerHTML && !h.trim(e.innerHTML) && (e.innerHTML = t.innerHTML))
						: 'input' === n && W.test(t.type)
							? ((e.defaultChecked = e.checked = t.checked), e.value !== t.value && (e.value = t.value))
							: 'option' === n
								? (e.defaultSelected = e.selected = t.defaultSelected)
								: ('input' === n || 'textarea' === n) && (e.defaultValue = t.defaultValue);
			}
		}
		(mt.optgroup = mt.option),
			(mt.tbody = mt.tfoot = mt.colgroup = mt.caption = mt.thead),
			(mt.th = mt.td),
			h.extend({
				clone: function(t, e, n) {
					var i,
						o,
						r,
						s,
						a,
						l = h.contains(t.ownerDocument, t);
					if (
						(u.html5Clone || h.isXMLDoc(t) || !ot.test('<' + t.nodeName + '>')
							? (r = t.cloneNode(!0))
							: ((gt.innerHTML = t.outerHTML), gt.removeChild((r = gt.firstChild))),
						!(
							(u.noCloneEvent && u.noCloneChecked) ||
							(1 !== t.nodeType && 11 !== t.nodeType) ||
							h.isXMLDoc(t)
						))
					)
						for (i = vt(r), a = vt(t), s = 0; null != (o = a[s]); ++s) i[s] && Tt(o, i[s]);
					if (e)
						if (n) for (a = a || vt(t), i = i || vt(r), s = 0; null != (o = a[s]); s++) $t(o, i[s]);
						else $t(t, r);
					return 0 < (i = vt(r, 'script')).length && kt(i, !l && vt(t, 'script')), (i = a = o = null), r;
				},
				buildFragment: function(t, e, n, i) {
					for (var o, r, s, a, l, c, d, p = t.length, f = et(e), m = [], g = 0; g < p; g++)
						if ((r = t[g]) || 0 === r)
							if ('object' === h.type(r)) h.merge(m, r.nodeType ? [ r ] : r);
							else if (ct.test(r)) {
								for (
									a = a || f.appendChild(e.createElement('div')),
										l = (at.exec(r) || [ '', '' ])[1].toLowerCase(),
										d = mt[l] || mt._default,
										a.innerHTML = d[1] + r.replace(st, '<$1></$2>') + d[2],
										o = d[0];
									o--;

								)
									a = a.lastChild;
								if (
									(!u.leadingWhitespace && rt.test(r) && m.push(e.createTextNode(rt.exec(r)[0])),
									!u.tbody)
								)
									for (
										o =
											(r =
												'table' !== l || lt.test(r)
													? '<table>' !== d[1] || lt.test(r) ? 0 : a
													: a.firstChild) && r.childNodes.length;
										o--;

									)
										h.nodeName((c = r.childNodes[o]), 'tbody') &&
											!c.childNodes.length &&
											r.removeChild(c);
								for (h.merge(m, a.childNodes), a.textContent = ''; a.firstChild; )
									a.removeChild(a.firstChild);
								a = f.lastChild;
							} else m.push(e.createTextNode(r));
					for (a && f.removeChild(a), u.appendChecked || h.grep(vt(m, 'input'), bt), g = 0; (r = m[g++]); )
						if (
							(!i || -1 === h.inArray(r, i)) &&
							((s = h.contains(r.ownerDocument, r)), (a = vt(f.appendChild(r), 'script')), s && kt(a), n)
						)
							for (o = 0; (r = a[o++]); ) ht.test(r.type || '') && n.push(r);
					return (a = null), f;
				},
				cleanData: function(t, e) {
					for (
						var i, o, r, s, a = 0, l = h.expando, c = h.cache, d = u.deleteExpando, p = h.event.special;
						null != (i = t[a]);
						a++
					)
						if ((e || h.acceptData(i)) && (s = (r = i[l]) && c[r])) {
							if (s.events)
								for (o in s.events) p[o] ? h.event.remove(i, o) : h.removeEvent(i, o, s.handle);
							c[r] &&
								(delete c[r],
								d ? delete i[l] : typeof i.removeAttribute !== O ? i.removeAttribute(l) : (i[l] = null),
								n.push(r));
						}
				}
			}),
			h.fn.extend({
				text: function(t) {
					return B(
						this,
						function(t) {
							return void 0 === t
								? h.text(this)
								: this.empty().append(((this[0] && this[0].ownerDocument) || T).createTextNode(t));
						},
						null,
						t,
						arguments.length
					);
				},
				append: function() {
					return this.domManip(arguments, function(t) {
						(1 !== this.nodeType && 11 !== this.nodeType && 9 !== this.nodeType) ||
							yt(this, t).appendChild(t);
					});
				},
				prepend: function() {
					return this.domManip(arguments, function(t) {
						if (1 === this.nodeType || 11 === this.nodeType || 9 === this.nodeType) {
							var e = yt(this, t);
							e.insertBefore(t, e.firstChild);
						}
					});
				},
				before: function() {
					return this.domManip(arguments, function(t) {
						this.parentNode && this.parentNode.insertBefore(t, this);
					});
				},
				after: function() {
					return this.domManip(arguments, function(t) {
						this.parentNode && this.parentNode.insertBefore(t, this.nextSibling);
					});
				},
				remove: function(t, e) {
					for (var n, i = t ? h.filter(t, this) : this, o = 0; null != (n = i[o]); o++)
						e || 1 !== n.nodeType || h.cleanData(vt(n)),
							n.parentNode &&
								(e && h.contains(n.ownerDocument, n) && kt(vt(n, 'script')),
								n.parentNode.removeChild(n));
					return this;
				},
				empty: function() {
					for (var t, e = 0; null != (t = this[e]); e++) {
						for (1 === t.nodeType && h.cleanData(vt(t, !1)); t.firstChild; ) t.removeChild(t.firstChild);
						t.options && h.nodeName(t, 'select') && (t.options.length = 0);
					}
					return this;
				},
				clone: function(t, e) {
					return (
						(t = null != t && t),
						(e = null == e ? t : e),
						this.map(function() {
							return h.clone(this, t, e);
						})
					);
				},
				html: function(t) {
					return B(
						this,
						function(t) {
							var e = this[0] || {},
								n = 0,
								i = this.length;
							if (void 0 === t) return 1 === e.nodeType ? e.innerHTML.replace(it, '') : void 0;
							if (
								!(
									'string' != typeof t ||
									ut.test(t) ||
									(!u.htmlSerialize && ot.test(t)) ||
									(!u.leadingWhitespace && rt.test(t)) ||
									mt[(at.exec(t) || [ '', '' ])[1].toLowerCase()]
								)
							) {
								t = t.replace(st, '<$1></$2>');
								try {
									for (; n < i; n++)
										1 === (e = this[n] || {}).nodeType &&
											(h.cleanData(vt(e, !1)), (e.innerHTML = t));
									e = 0;
								} catch (t) {}
							}
							e && this.empty().append(t);
						},
						null,
						t,
						arguments.length
					);
				},
				replaceWith: function() {
					var t = arguments[0];
					return (
						this.domManip(arguments, function(e) {
							(t = this.parentNode), h.cleanData(vt(this)), t && t.replaceChild(e, this);
						}),
						t && (t.length || t.nodeType) ? this : this.remove()
					);
				},
				detach: function(t) {
					return this.remove(t, !0);
				},
				domManip: function(t, e) {
					t = o.apply([], t);
					var n,
						i,
						r,
						s,
						a,
						l,
						c = 0,
						d = this.length,
						p = this,
						f = d - 1,
						m = t[0],
						g = h.isFunction(m);
					if (g || (1 < d && 'string' == typeof m && !u.checkClone && dt.test(m)))
						return this.each(function(n) {
							var i = p.eq(n);
							g && (t[0] = m.call(this, n, i.html())), i.domManip(t, e);
						});
					if (
						d &&
						((n = (l = h.buildFragment(t, this[0].ownerDocument, !1, this)).firstChild),
						1 === l.childNodes.length && (l = n),
						n)
					) {
						for (r = (s = h.map(vt(l, 'script'), wt)).length; c < d; c++)
							(i = l),
								c !== f && ((i = h.clone(i, !0, !0)), r && h.merge(s, vt(i, 'script'))),
								e.call(this[c], i, c);
						if (r)
							for (a = s[s.length - 1].ownerDocument, h.map(s, xt), c = 0; c < r; c++)
								(i = s[c]),
									ht.test(i.type || '') &&
										!h._data(i, 'globalEval') &&
										h.contains(a, i) &&
										(i.src
											? h._evalUrl && h._evalUrl(i.src)
											: h.globalEval(
													(i.text || i.textContent || i.innerHTML || '').replace(ft, '')
												));
						l = n = null;
					}
					return this;
				}
			}),
			h.each(
				{
					appendTo: 'append',
					prependTo: 'prepend',
					insertBefore: 'before',
					insertAfter: 'after',
					replaceAll: 'replaceWith'
				},
				function(t, e) {
					h.fn[t] = function(t) {
						for (var n, i = 0, o = [], s = h(t), a = s.length - 1; i <= a; i++)
							(n = i === a ? this : this.clone(!0)), h(s[i])[e](n), r.apply(o, n.get());
						return this.pushStack(o);
					};
				}
			);
		var Et,
			Ct,
			_t = {};
		function St(e, n) {
			var i,
				o = h(n.createElement(e)).appendTo(n.body),
				r =
					t.getDefaultComputedStyle && (i = t.getDefaultComputedStyle(o[0]))
						? i.display
						: h.css(o[0], 'display');
			return o.detach(), r;
		}
		function jt(t) {
			var e = T,
				n = _t[t];
			return (
				n ||
					(('none' !== (n = St(t, e)) && n) ||
						((e = ((Et = (Et || h("<iframe frameborder='0' width='0' height='0'/>"))
							.appendTo(e.documentElement))[0].contentWindow || Et[0].contentDocument).document).write(),
						e.close(),
						(n = St(t, e)),
						Et.detach()),
					(_t[t] = n)),
				n
			);
		}
		u.shrinkWrapBlocks = function() {
			return null != Ct
				? Ct
				: ((Ct = !1),
					(e = T.getElementsByTagName('body')[0]) && e.style
						? ((t = T.createElement('div')),
							((n = T.createElement('div')).style.cssText =
								'position:absolute;border:0;width:0;height:0;top:0;left:-9999px'),
							e.appendChild(n).appendChild(t),
							typeof t.style.zoom !== O &&
								((t.style.cssText =
									'-webkit-box-sizing:content-box;-moz-box-sizing:content-box;box-sizing:content-box;display:block;margin:0;border:0;padding:1px;width:1px;zoom:1'),
								(t.appendChild(T.createElement('div')).style.width = '5px'),
								(Ct = 3 !== t.offsetWidth)),
							e.removeChild(n),
							Ct)
						: void 0);
			var t, e, n;
		};
		var Nt,
			At,
			It = /^margin/,
			Lt = new RegExp('^(' + H + ')(?!px)[a-z%]+$', 'i'),
			Dt = /^(top|right|bottom|left)$/;
		function Ot(t, e) {
			return {
				get: function() {
					var n = t();
					if (null != n) return n ? void delete this.get : (this.get = e).apply(this, arguments);
				}
			};
		}
		t.getComputedStyle
			? ((Nt = function(e) {
					return e.ownerDocument.defaultView.opener
						? e.ownerDocument.defaultView.getComputedStyle(e, null)
						: t.getComputedStyle(e, null);
				}),
				(At = function(t, e, n) {
					var i,
						o,
						r,
						s,
						a = t.style;
					return (
						(s = (n = n || Nt(t)) ? n.getPropertyValue(e) || n[e] : void 0),
						n &&
							('' !== s || h.contains(t.ownerDocument, t) || (s = h.style(t, e)),
							Lt.test(s) &&
								It.test(e) &&
								((i = a.width),
								(o = a.minWidth),
								(r = a.maxWidth),
								(a.minWidth = a.maxWidth = a.width = s),
								(s = n.width),
								(a.width = i),
								(a.minWidth = o),
								(a.maxWidth = r))),
						void 0 === s ? s : s + ''
					);
				}))
			: T.documentElement.currentStyle &&
				((Nt = function(t) {
					return t.currentStyle;
				}),
				(At = function(t, e, n) {
					var i,
						o,
						r,
						s,
						a = t.style;
					return (
						null == (s = (n = n || Nt(t)) ? n[e] : void 0) && a && a[e] && (s = a[e]),
						Lt.test(s) &&
							!Dt.test(e) &&
							((i = a.left),
							(r = (o = t.runtimeStyle) && o.left) && (o.left = t.currentStyle.left),
							(a.left = 'fontSize' === e ? '1em' : s),
							(s = a.pixelLeft + 'px'),
							(a.left = i),
							r && (o.left = r)),
						void 0 === s ? s : s + '' || 'auto'
					);
				})),
			(function() {
				var e, n, i, o, r, s, a;
				if (
					(((e = T.createElement('div')).innerHTML =
						"  <link/><table></table><a href='/a'>a</a><input type='checkbox'/>"),
					(n = (i = e.getElementsByTagName('a')[0]) && i.style))
				) {
					function l() {
						var e, n, i, l;
						(n = T.getElementsByTagName('body')[0]) &&
							n.style &&
							((e = T.createElement('div')),
							((i = T.createElement('div')).style.cssText =
								'position:absolute;border:0;width:0;height:0;top:0;left:-9999px'),
							n.appendChild(i).appendChild(e),
							(e.style.cssText =
								'-webkit-box-sizing:border-box;-moz-box-sizing:border-box;box-sizing:border-box;display:block;margin-top:1%;top:1%;border:1px;padding:1px;width:4px;position:absolute'),
							(o = r = !1),
							(a = !0),
							t.getComputedStyle &&
								((o = '1%' !== (t.getComputedStyle(e, null) || {}).top),
								(r = '4px' === (t.getComputedStyle(e, null) || { width: '4px' }).width),
								((l = e.appendChild(T.createElement('div'))).style.cssText = e.style.cssText =
									'-webkit-box-sizing:content-box;-moz-box-sizing:content-box;box-sizing:content-box;display:block;margin:0;border:0;padding:0'),
								(l.style.marginRight = l.style.width = '0'),
								(e.style.width = '1px'),
								(a = !parseFloat((t.getComputedStyle(l, null) || {}).marginRight)),
								e.removeChild(l)),
							(e.innerHTML = '<table><tr><td></td><td>t</td></tr></table>'),
							((l = e.getElementsByTagName('td'))[0].style.cssText =
								'margin:0;border:0;padding:0;display:none'),
							(s = 0 === l[0].offsetHeight) &&
								((l[0].style.display = ''),
								(l[1].style.display = 'none'),
								(s = 0 === l[0].offsetHeight)),
							n.removeChild(i));
					}
					(n.cssText = 'float:left;opacity:.5'),
						(u.opacity = '0.5' === n.opacity),
						(u.cssFloat = !!n.cssFloat),
						(e.style.backgroundClip = 'content-box'),
						(e.cloneNode(!0).style.backgroundClip = ''),
						(u.clearCloneStyle = 'content-box' === e.style.backgroundClip),
						(u.boxSizing = '' === n.boxSizing || '' === n.MozBoxSizing || '' === n.WebkitBoxSizing),
						h.extend(u, {
							reliableHiddenOffsets: function() {
								return null == s && l(), s;
							},
							boxSizingReliable: function() {
								return null == r && l(), r;
							},
							pixelPosition: function() {
								return null == o && l(), o;
							},
							reliableMarginRight: function() {
								return null == a && l(), a;
							}
						});
				}
			})(),
			(h.swap = function(t, e, n, i) {
				var o,
					r,
					s = {};
				for (r in e) (s[r] = t.style[r]), (t.style[r] = e[r]);
				for (r in ((o = n.apply(t, i || [])), e)) t.style[r] = s[r];
				return o;
			});
		var Pt = /alpha\([^)]*\)/i,
			Rt = /opacity\s*=\s*([^)]*)/,
			Mt = /^(none|table(?!-c[ea]).+)/,
			qt = new RegExp('^(' + H + ')(.*)$', 'i'),
			zt = new RegExp('^([+-])=(' + H + ')', 'i'),
			Ut = { position: 'absolute', visibility: 'hidden', display: 'block' },
			Ht = { letterSpacing: '0', fontWeight: '400' },
			Gt = [ 'Webkit', 'O', 'Moz', 'ms' ];
		function Ft(t, e) {
			if (e in t) return e;
			for (var n = e.charAt(0).toUpperCase() + e.slice(1), i = e, o = Gt.length; o--; )
				if ((e = Gt[o] + n) in t) return e;
			return i;
		}
		function Bt(t, e) {
			for (var n, i, o, r = [], s = 0, a = t.length; s < a; s++)
				(i = t[s]).style &&
					((r[s] = h._data(i, 'olddisplay')),
					(n = i.style.display),
					e
						? (r[s] || 'none' !== n || (i.style.display = ''),
							'' === i.style.display && F(i) && (r[s] = h._data(i, 'olddisplay', jt(i.nodeName))))
						: ((o = F(i)),
							((n && 'none' !== n) || !o) && h._data(i, 'olddisplay', o ? n : h.css(i, 'display'))));
			for (s = 0; s < a; s++)
				(i = t[s]).style &&
					((e && 'none' !== i.style.display && '' !== i.style.display) ||
						(i.style.display = e ? r[s] || '' : 'none'));
			return t;
		}
		function Wt(t, e, n) {
			var i = qt.exec(e);
			return i ? Math.max(0, i[1] - (n || 0)) + (i[2] || 'px') : e;
		}
		function Vt(t, e, n, i, o) {
			for (var r = n === (i ? 'border' : 'content') ? 4 : 'width' === e ? 1 : 0, s = 0; r < 4; r += 2)
				'margin' === n && (s += h.css(t, n + G[r], !0, o)),
					i
						? ('content' === n && (s -= h.css(t, 'padding' + G[r], !0, o)),
							'margin' !== n && (s -= h.css(t, 'border' + G[r] + 'Width', !0, o)))
						: ((s += h.css(t, 'padding' + G[r], !0, o)),
							'padding' !== n && (s += h.css(t, 'border' + G[r] + 'Width', !0, o)));
			return s;
		}
		function Qt(t, e, n) {
			var i = !0,
				o = 'width' === e ? t.offsetWidth : t.offsetHeight,
				r = Nt(t),
				s = u.boxSizing && 'border-box' === h.css(t, 'boxSizing', !1, r);
			if (o <= 0 || null == o) {
				if ((((o = At(t, e, r)) < 0 || null == o) && (o = t.style[e]), Lt.test(o))) return o;
				(i = s && (u.boxSizingReliable() || o === t.style[e])), (o = parseFloat(o) || 0);
			}
			return o + Vt(t, e, n || (s ? 'border' : 'content'), i, r) + 'px';
		}
		function Xt(t, e, n, i, o) {
			return new Xt.prototype.init(t, e, n, i, o);
		}
		h.extend({
			cssHooks: {
				opacity: {
					get: function(t, e) {
						if (e) {
							var n = At(t, 'opacity');
							return '' === n ? '1' : n;
						}
					}
				}
			},
			cssNumber: {
				columnCount: !0,
				fillOpacity: !0,
				flexGrow: !0,
				flexShrink: !0,
				fontWeight: !0,
				lineHeight: !0,
				opacity: !0,
				order: !0,
				orphans: !0,
				widows: !0,
				zIndex: !0,
				zoom: !0
			},
			cssProps: { float: u.cssFloat ? 'cssFloat' : 'styleFloat' },
			style: function(t, e, n, i) {
				if (t && 3 !== t.nodeType && 8 !== t.nodeType && t.style) {
					var o,
						r,
						s,
						a = h.camelCase(e),
						l = t.style;
					if (
						((e = h.cssProps[a] || (h.cssProps[a] = Ft(l, a))),
						(s = h.cssHooks[e] || h.cssHooks[a]),
						void 0 === n)
					)
						return s && 'get' in s && void 0 !== (o = s.get(t, !1, i)) ? o : l[e];
					if (
						('string' == (r = typeof n) &&
							(o = zt.exec(n)) &&
							((n = (o[1] + 1) * o[2] + parseFloat(h.css(t, e))), (r = 'number')),
						null != n &&
							n == n &&
							('number' !== r || h.cssNumber[a] || (n += 'px'),
							u.clearCloneStyle || '' !== n || 0 !== e.indexOf('background') || (l[e] = 'inherit'),
							!(s && 'set' in s && void 0 === (n = s.set(t, n, i)))))
					)
						try {
							l[e] = n;
						} catch (t) {}
				}
			},
			css: function(t, e, n, i) {
				var o,
					r,
					s,
					a = h.camelCase(e);
				return (
					(e = h.cssProps[a] || (h.cssProps[a] = Ft(t.style, a))),
					(s = h.cssHooks[e] || h.cssHooks[a]) && 'get' in s && (r = s.get(t, !0, n)),
					void 0 === r && (r = At(t, e, i)),
					'normal' === r && e in Ht && (r = Ht[e]),
					'' === n || n ? ((o = parseFloat(r)), !0 === n || h.isNumeric(o) ? o || 0 : r) : r
				);
			}
		}),
			h.each([ 'height', 'width' ], function(t, e) {
				h.cssHooks[e] = {
					get: function(t, n, i) {
						return n
							? Mt.test(h.css(t, 'display')) && 0 === t.offsetWidth
								? h.swap(t, Ut, function() {
										return Qt(t, e, i);
									})
								: Qt(t, e, i)
							: void 0;
					},
					set: function(t, n, i) {
						var o = i && Nt(t);
						return Wt(
							0,
							n,
							i ? Vt(t, e, i, u.boxSizing && 'border-box' === h.css(t, 'boxSizing', !1, o), o) : 0
						);
					}
				};
			}),
			u.opacity ||
				(h.cssHooks.opacity = {
					get: function(t, e) {
						return Rt.test((e && t.currentStyle ? t.currentStyle.filter : t.style.filter) || '')
							? 0.01 * parseFloat(RegExp.$1) + ''
							: e ? '1' : '';
					},
					set: function(t, e) {
						var n = t.style,
							i = t.currentStyle,
							o = h.isNumeric(e) ? 'alpha(opacity=' + 100 * e + ')' : '',
							r = (i && i.filter) || n.filter || '';
						(((n.zoom = 1) <= e || '' === e) &&
							'' === h.trim(r.replace(Pt, '')) &&
							n.removeAttribute &&
							(n.removeAttribute('filter'), '' === e || (i && !i.filter))) ||
							(n.filter = Pt.test(r) ? r.replace(Pt, o) : r + ' ' + o);
					}
				}),
			(h.cssHooks.marginRight = Ot(u.reliableMarginRight, function(t, e) {
				return e ? h.swap(t, { display: 'inline-block' }, At, [ t, 'marginRight' ]) : void 0;
			})),
			h.each({ margin: '', padding: '', border: 'Width' }, function(t, e) {
				(h.cssHooks[t + e] = {
					expand: function(n) {
						for (var i = 0, o = {}, r = 'string' == typeof n ? n.split(' ') : [ n ]; i < 4; i++)
							o[t + G[i] + e] = r[i] || r[i - 2] || r[0];
						return o;
					}
				}),
					It.test(t) || (h.cssHooks[t + e].set = Wt);
			}),
			h.fn.extend({
				css: function(t, e) {
					return B(
						this,
						function(t, e, n) {
							var i,
								o,
								r = {},
								s = 0;
							if (h.isArray(e)) {
								for (i = Nt(t), o = e.length; s < o; s++) r[e[s]] = h.css(t, e[s], !1, i);
								return r;
							}
							return void 0 !== n ? h.style(t, e, n) : h.css(t, e);
						},
						t,
						e,
						1 < arguments.length
					);
				},
				show: function() {
					return Bt(this, !0);
				},
				hide: function() {
					return Bt(this);
				},
				toggle: function(t) {
					return 'boolean' == typeof t
						? t ? this.show() : this.hide()
						: this.each(function() {
								F(this) ? h(this).show() : h(this).hide();
							});
				}
			}),
			(((h.Tween = Xt).prototype = {
				constructor: Xt,
				init: function(t, e, n, i, o, r) {
					(this.elem = t),
						(this.prop = n),
						(this.easing = o || 'swing'),
						(this.options = e),
						(this.start = this.now = this.cur()),
						(this.end = i),
						(this.unit = r || (h.cssNumber[n] ? '' : 'px'));
				},
				cur: function() {
					var t = Xt.propHooks[this.prop];
					return t && t.get ? t.get(this) : Xt.propHooks._default.get(this);
				},
				run: function(t) {
					var e,
						n = Xt.propHooks[this.prop];
					return (
						this.options.duration
							? (this.pos = e = h.easing[this.easing](
									t,
									this.options.duration * t,
									0,
									1,
									this.options.duration
								))
							: (this.pos = e = t),
						(this.now = (this.end - this.start) * e + this.start),
						this.options.step && this.options.step.call(this.elem, this.now, this),
						n && n.set ? n.set(this) : Xt.propHooks._default.set(this),
						this
					);
				}
			}).init.prototype =
				Xt.prototype),
			((Xt.propHooks = {
				_default: {
					get: function(t) {
						var e;
						return null == t.elem[t.prop] || (t.elem.style && null != t.elem.style[t.prop])
							? (e = h.css(t.elem, t.prop, '')) && 'auto' !== e ? e : 0
							: t.elem[t.prop];
					},
					set: function(t) {
						h.fx.step[t.prop]
							? h.fx.step[t.prop](t)
							: t.elem.style && (null != t.elem.style[h.cssProps[t.prop]] || h.cssHooks[t.prop])
								? h.style(t.elem, t.prop, t.now + t.unit)
								: (t.elem[t.prop] = t.now);
					}
				}
			}).scrollTop = Xt.propHooks.scrollLeft = {
				set: function(t) {
					t.elem.nodeType && t.elem.parentNode && (t.elem[t.prop] = t.now);
				}
			}),
			(h.easing = {
				linear: function(t) {
					return t;
				},
				swing: function(t) {
					return 0.5 - Math.cos(t * Math.PI) / 2;
				}
			}),
			(h.fx = Xt.prototype.init),
			(h.fx.step = {});
		var Kt,
			Yt,
			Jt,
			Zt,
			te,
			ee,
			ne,
			ie = /^(?:toggle|show|hide)$/,
			oe = new RegExp('^(?:([+-])=|)(' + H + ')([a-z%]*)$', 'i'),
			re = /queueHooks$/,
			se = [
				function(t, e, n) {
					var i,
						o,
						r,
						s,
						a,
						l,
						c,
						d = this,
						p = {},
						f = t.style,
						m = t.nodeType && F(t),
						g = h._data(t, 'fxshow');
					for (i in (n.queue ||
						(null == (a = h._queueHooks(t, 'fx')).unqueued &&
							((a.unqueued = 0),
							(l = a.empty.fire),
							(a.empty.fire = function() {
								a.unqueued || l();
							})),
						a.unqueued++,
						d.always(function() {
							d.always(function() {
								a.unqueued--, h.queue(t, 'fx').length || a.empty.fire();
							});
						})),
					1 === t.nodeType &&
						('height' in e || 'width' in e) &&
						((n.overflow = [ f.overflow, f.overflowX, f.overflowY ]),
						'inline' ===
							('none' === (c = h.css(t, 'display')) ? h._data(t, 'olddisplay') || jt(t.nodeName) : c) &&
							'none' === h.css(t, 'float') &&
							(u.inlineBlockNeedsLayout && 'inline' !== jt(t.nodeName)
								? (f.zoom = 1)
								: (f.display = 'inline-block'))),
					n.overflow &&
						((f.overflow = 'hidden'),
						u.shrinkWrapBlocks() ||
							d.always(function() {
								(f.overflow = n.overflow[0]),
									(f.overflowX = n.overflow[1]),
									(f.overflowY = n.overflow[2]);
							})),
					e))
						if (((o = e[i]), ie.exec(o))) {
							if ((delete e[i], (r = r || 'toggle' === o), o === (m ? 'hide' : 'show'))) {
								if ('show' !== o || !g || void 0 === g[i]) continue;
								m = !0;
							}
							p[i] = (g && g[i]) || h.style(t, i);
						} else c = void 0;
					if (h.isEmptyObject(p)) 'inline' === ('none' === c ? jt(t.nodeName) : c) && (f.display = c);
					else
						for (i in (g ? 'hidden' in g && (m = g.hidden) : (g = h._data(t, 'fxshow', {})),
						r && (g.hidden = !m),
						m
							? h(t).show()
							: d.done(function() {
									h(t).hide();
								}),
						d.done(function() {
							var e;
							for (e in (h._removeData(t, 'fxshow'), p)) h.style(t, e, p[e]);
						}),
						p))
							(s = ue(m ? g[i] : 0, i, d)),
								i in g ||
									((g[i] = s.start),
									m && ((s.end = s.start), (s.start = 'width' === i || 'height' === i ? 1 : 0)));
				}
			],
			ae = {
				'*': [
					function(t, e) {
						var n = this.createTween(t, e),
							i = n.cur(),
							o = oe.exec(e),
							r = (o && o[3]) || (h.cssNumber[t] ? '' : 'px'),
							s = (h.cssNumber[t] || ('px' !== r && +i)) && oe.exec(h.css(n.elem, t)),
							a = 1,
							l = 20;
						if (s && s[3] !== r)
							for (
								r = r || s[3], o = o || [], s = +i || 1;
								(s /= a = a || '.5'),
									h.style(n.elem, t, s + r),
									a !== (a = n.cur() / i) && 1 !== a && --l;

							);
						return (
							o &&
								((s = n.start = +s || +i || 0),
								(n.unit = r),
								(n.end = o[1] ? s + (o[1] + 1) * o[2] : +o[2])),
							n
						);
					}
				]
			};
		function le() {
			return (
				setTimeout(function() {
					Kt = void 0;
				}),
				(Kt = h.now())
			);
		}
		function ce(t, e) {
			var n,
				i = { height: t },
				o = 0;
			for (e = e ? 1 : 0; o < 4; o += 2 - e) i['margin' + (n = G[o])] = i['padding' + n] = t;
			return e && (i.opacity = i.width = t), i;
		}
		function ue(t, e, n) {
			for (var i, o = (ae[e] || []).concat(ae['*']), r = 0, s = o.length; r < s; r++)
				if ((i = o[r].call(n, e, t))) return i;
		}
		function de(t, e, n) {
			var i,
				o,
				r = 0,
				s = se.length,
				a = h.Deferred().always(function() {
					delete l.elem;
				}),
				l = function() {
					if (o) return !1;
					for (
						var e = Kt || le(),
							n = Math.max(0, c.startTime + c.duration - e),
							i = 1 - (n / c.duration || 0),
							r = 0,
							s = c.tweens.length;
						r < s;
						r++
					)
						c.tweens[r].run(i);
					return a.notifyWith(t, [ c, i, n ]), i < 1 && s ? n : (a.resolveWith(t, [ c ]), !1);
				},
				c = a.promise({
					elem: t,
					props: h.extend({}, e),
					opts: h.extend(!0, { specialEasing: {} }, n),
					originalProperties: e,
					originalOptions: n,
					startTime: Kt || le(),
					duration: n.duration,
					tweens: [],
					createTween: function(e, n) {
						var i = h.Tween(t, c.opts, e, n, c.opts.specialEasing[e] || c.opts.easing);
						return c.tweens.push(i), i;
					},
					stop: function(e) {
						var n = 0,
							i = e ? c.tweens.length : 0;
						if (o) return this;
						for (o = !0; n < i; n++) c.tweens[n].run(1);
						return e ? a.resolveWith(t, [ c, e ]) : a.rejectWith(t, [ c, e ]), this;
					}
				}),
				u = c.props;
			for (
				(function(t, e) {
					var n, i, o, r, s;
					for (n in t)
						if (
							((o = e[(i = h.camelCase(n))]),
							(r = t[n]),
							h.isArray(r) && ((o = r[1]), (r = t[n] = r[0])),
							n !== i && ((t[i] = r), delete t[n]),
							(s = h.cssHooks[i]) && ('expand' in s))
						)
							for (n in ((r = s.expand(r)), delete t[i], r)) (n in t) || ((t[n] = r[n]), (e[n] = o));
						else e[i] = o;
				})(u, c.opts.specialEasing);
				r < s;
				r++
			)
				if ((i = se[r].call(c, t, u, c.opts))) return i;
			return (
				h.map(u, ue, c),
				h.isFunction(c.opts.start) && c.opts.start.call(t, c),
				h.fx.timer(h.extend(l, { elem: t, anim: c, queue: c.opts.queue })),
				c.progress(c.opts.progress).done(c.opts.done, c.opts.complete).fail(c.opts.fail).always(c.opts.always)
			);
		}
		(h.Animation = h.extend(de, {
			tweener: function(t, e) {
				h.isFunction(t) ? ((e = t), (t = [ '*' ])) : (t = t.split(' '));
				for (var n, i = 0, o = t.length; i < o; i++) (n = t[i]), (ae[n] = ae[n] || []), ae[n].unshift(e);
			},
			prefilter: function(t, e) {
				e ? se.unshift(t) : se.push(t);
			}
		})),
			(h.speed = function(t, e, n) {
				var i =
					t && 'object' == typeof t
						? h.extend({}, t)
						: {
								complete: n || (!n && e) || (h.isFunction(t) && t),
								duration: t,
								easing: (n && e) || (e && !h.isFunction(e) && e)
							};
				return (
					(i.duration = h.fx.off
						? 0
						: 'number' == typeof i.duration
							? i.duration
							: i.duration in h.fx.speeds ? h.fx.speeds[i.duration] : h.fx.speeds._default),
					(null == i.queue || !0 === i.queue) && (i.queue = 'fx'),
					(i.old = i.complete),
					(i.complete = function() {
						h.isFunction(i.old) && i.old.call(this), i.queue && h.dequeue(this, i.queue);
					}),
					i
				);
			}),
			h.fn.extend({
				fadeTo: function(t, e, n, i) {
					return this.filter(F).css('opacity', 0).show().end().animate({ opacity: e }, t, n, i);
				},
				animate: function(t, e, n, i) {
					var o = h.isEmptyObject(t),
						r = h.speed(e, n, i),
						s = function() {
							var e = de(this, h.extend({}, t), r);
							(o || h._data(this, 'finish')) && e.stop(!0);
						};
					return (s.finish = s), o || !1 === r.queue ? this.each(s) : this.queue(r.queue, s);
				},
				stop: function(t, e, n) {
					var i = function(t) {
						var e = t.stop;
						delete t.stop, e(n);
					};
					return (
						'string' != typeof t && ((n = e), (e = t), (t = void 0)),
						e && !1 !== t && this.queue(t || 'fx', []),
						this.each(function() {
							var e = !0,
								o = null != t && t + 'queueHooks',
								r = h.timers,
								s = h._data(this);
							if (o) s[o] && s[o].stop && i(s[o]);
							else for (o in s) s[o] && s[o].stop && re.test(o) && i(s[o]);
							for (o = r.length; o--; )
								r[o].elem !== this ||
									(null != t && r[o].queue !== t) ||
									(r[o].anim.stop(n), (e = !1), r.splice(o, 1));
							(e || !n) && h.dequeue(this, t);
						})
					);
				},
				finish: function(t) {
					return (
						!1 !== t && (t = t || 'fx'),
						this.each(function() {
							var e,
								n = h._data(this),
								i = n[t + 'queue'],
								o = n[t + 'queueHooks'],
								r = h.timers,
								s = i ? i.length : 0;
							for (
								n.finish = !0, h.queue(this, t, []), o && o.stop && o.stop.call(this, !0), e = r.length;
								e--;

							)
								r[e].elem === this && r[e].queue === t && (r[e].anim.stop(!0), r.splice(e, 1));
							for (e = 0; e < s; e++) i[e] && i[e].finish && i[e].finish.call(this);
							delete n.finish;
						})
					);
				}
			}),
			h.each([ 'toggle', 'show', 'hide' ], function(t, e) {
				var n = h.fn[e];
				h.fn[e] = function(t, i, o) {
					return null == t || 'boolean' == typeof t
						? n.apply(this, arguments)
						: this.animate(ce(e, !0), t, i, o);
				};
			}),
			h.each(
				{
					slideDown: ce('show'),
					slideUp: ce('hide'),
					slideToggle: ce('toggle'),
					fadeIn: { opacity: 'show' },
					fadeOut: { opacity: 'hide' },
					fadeToggle: { opacity: 'toggle' }
				},
				function(t, e) {
					h.fn[t] = function(t, n, i) {
						return this.animate(e, t, n, i);
					};
				}
			),
			(h.timers = []),
			(h.fx.tick = function() {
				var t,
					e = h.timers,
					n = 0;
				for (Kt = h.now(); n < e.length; n++) (t = e[n])() || e[n] !== t || e.splice(n--, 1);
				e.length || h.fx.stop(), (Kt = void 0);
			}),
			(h.fx.timer = function(t) {
				h.timers.push(t), t() ? h.fx.start() : h.timers.pop();
			}),
			(h.fx.interval = 13),
			(h.fx.start = function() {
				Yt || (Yt = setInterval(h.fx.tick, h.fx.interval));
			}),
			(h.fx.stop = function() {
				clearInterval(Yt), (Yt = null);
			}),
			(h.fx.speeds = { slow: 600, fast: 200, _default: 400 }),
			(h.fn.delay = function(t, e) {
				return (
					(t = (h.fx && h.fx.speeds[t]) || t),
					(e = e || 'fx'),
					this.queue(e, function(e, n) {
						var i = setTimeout(e, t);
						n.stop = function() {
							clearTimeout(i);
						};
					})
				);
			}),
			(Zt = T.createElement('div')).setAttribute('className', 't'),
			(Zt.innerHTML = "  <link/><table></table><a href='/a'>a</a><input type='checkbox'/>"),
			(ee = Zt.getElementsByTagName('a')[0]),
			(ne = (te = T.createElement('select')).appendChild(T.createElement('option'))),
			(Jt = Zt.getElementsByTagName('input')[0]),
			(ee.style.cssText = 'top:1px'),
			(u.getSetAttribute = 't' !== Zt.className),
			(u.style = /top/.test(ee.getAttribute('style'))),
			(u.hrefNormalized = '/a' === ee.getAttribute('href')),
			(u.checkOn = !!Jt.value),
			(u.optSelected = ne.selected),
			(u.enctype = !!T.createElement('form').enctype),
			(te.disabled = !0),
			(u.optDisabled = !ne.disabled),
			(Jt = T.createElement('input')).setAttribute('value', ''),
			(u.input = '' === Jt.getAttribute('value')),
			(Jt.value = 't'),
			Jt.setAttribute('type', 'radio'),
			(u.radioValue = 't' === Jt.value);
		var he = /\r/g;
		h.fn.extend({
			val: function(t) {
				var e,
					n,
					i,
					o = this[0];
				return arguments.length
					? ((i = h.isFunction(t)),
						this.each(function(n) {
							var o;
							1 === this.nodeType &&
								(null == (o = i ? t.call(this, n, h(this).val()) : t)
									? (o = '')
									: 'number' == typeof o
										? (o += '')
										: h.isArray(o) &&
											(o = h.map(o, function(t) {
												return null == t ? '' : t + '';
											})),
								((e = h.valHooks[this.type] || h.valHooks[this.nodeName.toLowerCase()]) &&
									'set' in e &&
									void 0 !== e.set(this, o, 'value')) ||
									(this.value = o));
						}))
					: o
						? (e = h.valHooks[o.type] || h.valHooks[o.nodeName.toLowerCase()]) &&
							'get' in e &&
							void 0 !== (n = e.get(o, 'value'))
							? n
							: 'string' == typeof (n = o.value) ? n.replace(he, '') : null == n ? '' : n
						: void 0;
			}
		}),
			h.extend({
				valHooks: {
					option: {
						get: function(t) {
							var e = h.find.attr(t, 'value');
							return null != e ? e : h.trim(h.text(t));
						}
					},
					select: {
						get: function(t) {
							for (
								var e,
									n,
									i = t.options,
									o = t.selectedIndex,
									r = 'select-one' === t.type || o < 0,
									s = r ? null : [],
									a = r ? o + 1 : i.length,
									l = o < 0 ? a : r ? o : 0;
								l < a;
								l++
							)
								if (
									!(
										(!(n = i[l]).selected && l !== o) ||
										(u.optDisabled ? n.disabled : null !== n.getAttribute('disabled')) ||
										(n.parentNode.disabled && h.nodeName(n.parentNode, 'optgroup'))
									)
								) {
									if (((e = h(n).val()), r)) return e;
									s.push(e);
								}
							return s;
						},
						set: function(t, e) {
							for (var n, i, o = t.options, r = h.makeArray(e), s = o.length; s--; )
								if (((i = o[s]), 0 <= h.inArray(h.valHooks.option.get(i), r)))
									try {
										i.selected = n = !0;
									} catch (t) {
										i.scrollHeight;
									}
								else i.selected = !1;
							return n || (t.selectedIndex = -1), o;
						}
					}
				}
			}),
			h.each([ 'radio', 'checkbox' ], function() {
				(h.valHooks[this] = {
					set: function(t, e) {
						return h.isArray(e) ? (t.checked = 0 <= h.inArray(h(t).val(), e)) : void 0;
					}
				}),
					u.checkOn ||
						(h.valHooks[this].get = function(t) {
							return null === t.getAttribute('value') ? 'on' : t.value;
						});
			});
		var pe,
			fe,
			me = h.expr.attrHandle,
			ge = /^(?:checked|selected)$/i,
			ve = u.getSetAttribute,
			be = u.input;
		h.fn.extend({
			attr: function(t, e) {
				return B(this, h.attr, t, e, 1 < arguments.length);
			},
			removeAttr: function(t) {
				return this.each(function() {
					h.removeAttr(this, t);
				});
			}
		}),
			h.extend({
				attr: function(t, e, n) {
					var i,
						o,
						r = t.nodeType;
					if (t && 3 !== r && 8 !== r && 2 !== r)
						return typeof t.getAttribute === O
							? h.prop(t, e, n)
							: ((1 === r && h.isXMLDoc(t)) ||
									((e = e.toLowerCase()),
									(i = h.attrHooks[e] || (h.expr.match.bool.test(e) ? fe : pe))),
								void 0 === n
									? i && 'get' in i && null !== (o = i.get(t, e))
										? o
										: null == (o = h.find.attr(t, e)) ? void 0 : o
									: null !== n
										? i && 'set' in i && void 0 !== (o = i.set(t, n, e))
											? o
											: (t.setAttribute(e, n + ''), n)
										: void h.removeAttr(t, e));
				},
				removeAttr: function(t, e) {
					var n,
						i,
						o = 0,
						r = e && e.match(N);
					if (r && 1 === t.nodeType)
						for (; (n = r[o++]); )
							(i = h.propFix[n] || n),
								h.expr.match.bool.test(n)
									? (be && ve) || !ge.test(n)
										? (t[i] = !1)
										: (t[h.camelCase('default-' + n)] = t[i] = !1)
									: h.attr(t, n, ''),
								t.removeAttribute(ve ? n : i);
				},
				attrHooks: {
					type: {
						set: function(t, e) {
							if (!u.radioValue && 'radio' === e && h.nodeName(t, 'input')) {
								var n = t.value;
								return t.setAttribute('type', e), n && (t.value = n), e;
							}
						}
					}
				}
			}),
			(fe = {
				set: function(t, e, n) {
					return (
						!1 === e
							? h.removeAttr(t, n)
							: (be && ve) || !ge.test(n)
								? t.setAttribute((!ve && h.propFix[n]) || n, n)
								: (t[h.camelCase('default-' + n)] = t[n] = !0),
						n
					);
				}
			}),
			h.each(h.expr.match.bool.source.match(/\w+/g), function(t, e) {
				var n = me[e] || h.find.attr;
				me[e] =
					(be && ve) || !ge.test(e)
						? function(t, e, i) {
								var o, r;
								return (
									i ||
										((r = me[e]),
										(me[e] = o),
										(o = null != n(t, e, i) ? e.toLowerCase() : null),
										(me[e] = r)),
									o
								);
							}
						: function(t, e, n) {
								return n ? void 0 : t[h.camelCase('default-' + e)] ? e.toLowerCase() : null;
							};
			}),
			(be && ve) ||
				(h.attrHooks.value = {
					set: function(t, e, n) {
						return h.nodeName(t, 'input') ? void (t.defaultValue = e) : pe && pe.set(t, e, n);
					}
				}),
			ve ||
				((pe = {
					set: function(t, e, n) {
						var i = t.getAttributeNode(n);
						return (
							i || t.setAttributeNode((i = t.ownerDocument.createAttribute(n))),
							(i.value = e += ''),
							'value' === n || e === t.getAttribute(n) ? e : void 0
						);
					}
				}),
				(me.id = me.name = me.coords = function(t, e, n) {
					var i;
					return n ? void 0 : (i = t.getAttributeNode(e)) && '' !== i.value ? i.value : null;
				}),
				(h.valHooks.button = {
					get: function(t, e) {
						var n = t.getAttributeNode(e);
						return n && n.specified ? n.value : void 0;
					},
					set: pe.set
				}),
				(h.attrHooks.contenteditable = {
					set: function(t, e, n) {
						pe.set(t, '' !== e && e, n);
					}
				}),
				h.each([ 'width', 'height' ], function(t, e) {
					h.attrHooks[e] = {
						set: function(t, n) {
							return '' === n ? (t.setAttribute(e, 'auto'), n) : void 0;
						}
					};
				})),
			u.style ||
				(h.attrHooks.style = {
					get: function(t) {
						return t.style.cssText || void 0;
					},
					set: function(t, e) {
						return (t.style.cssText = e + '');
					}
				});
		var ye = /^(?:input|select|textarea|button|object)$/i,
			we = /^(?:a|area)$/i;
		h.fn.extend({
			prop: function(t, e) {
				return B(this, h.prop, t, e, 1 < arguments.length);
			},
			removeProp: function(t) {
				return (
					(t = h.propFix[t] || t),
					this.each(function() {
						try {
							(this[t] = void 0), delete this[t];
						} catch (t) {}
					})
				);
			}
		}),
			h.extend({
				propFix: { for: 'htmlFor', class: 'className' },
				prop: function(t, e, n) {
					var i,
						o,
						r = t.nodeType;
					if (t && 3 !== r && 8 !== r && 2 !== r)
						return (
							(1 !== r || !h.isXMLDoc(t)) && ((e = h.propFix[e] || e), (o = h.propHooks[e])),
							void 0 !== n
								? o && 'set' in o && void 0 !== (i = o.set(t, n, e)) ? i : (t[e] = n)
								: o && 'get' in o && null !== (i = o.get(t, e)) ? i : t[e]
						);
				},
				propHooks: {
					tabIndex: {
						get: function(t) {
							var e = h.find.attr(t, 'tabindex');
							return e
								? parseInt(e, 10)
								: ye.test(t.nodeName) || (we.test(t.nodeName) && t.href) ? 0 : -1;
						}
					}
				}
			}),
			u.hrefNormalized ||
				h.each([ 'href', 'src' ], function(t, e) {
					h.propHooks[e] = {
						get: function(t) {
							return t.getAttribute(e, 4);
						}
					};
				}),
			u.optSelected ||
				(h.propHooks.selected = {
					get: function(t) {
						var e = t.parentNode;
						return e && (e.selectedIndex, e.parentNode && e.parentNode.selectedIndex), null;
					}
				}),
			h.each(
				[
					'tabIndex',
					'readOnly',
					'maxLength',
					'cellSpacing',
					'cellPadding',
					'rowSpan',
					'colSpan',
					'useMap',
					'frameBorder',
					'contentEditable'
				],
				function() {
					h.propFix[this.toLowerCase()] = this;
				}
			),
			u.enctype || (h.propFix.enctype = 'encoding');
		var xe = /[\t\r\n\f]/g;
		h.fn.extend({
			addClass: function(t) {
				var e,
					n,
					i,
					o,
					r,
					s,
					a = 0,
					l = this.length,
					c = 'string' == typeof t && t;
				if (h.isFunction(t))
					return this.each(function(e) {
						h(this).addClass(t.call(this, e, this.className));
					});
				if (c)
					for (e = (t || '').match(N) || []; a < l; a++)
						if (
							(i =
								1 === (n = this[a]).nodeType &&
								(n.className ? (' ' + n.className + ' ').replace(xe, ' ') : ' '))
						) {
							for (r = 0; (o = e[r++]); ) i.indexOf(' ' + o + ' ') < 0 && (i += o + ' ');
							(s = h.trim(i)), n.className !== s && (n.className = s);
						}
				return this;
			},
			removeClass: function(t) {
				var e,
					n,
					i,
					o,
					r,
					s,
					a = 0,
					l = this.length,
					c = 0 === arguments.length || ('string' == typeof t && t);
				if (h.isFunction(t))
					return this.each(function(e) {
						h(this).removeClass(t.call(this, e, this.className));
					});
				if (c)
					for (e = (t || '').match(N) || []; a < l; a++)
						if (
							(i =
								1 === (n = this[a]).nodeType &&
								(n.className ? (' ' + n.className + ' ').replace(xe, ' ') : ''))
						) {
							for (r = 0; (o = e[r++]); )
								for (; 0 <= i.indexOf(' ' + o + ' '); ) i = i.replace(' ' + o + ' ', ' ');
							(s = t ? h.trim(i) : ''), n.className !== s && (n.className = s);
						}
				return this;
			},
			toggleClass: function(t, e) {
				var n = typeof t;
				return 'boolean' == typeof e && 'string' === n
					? e ? this.addClass(t) : this.removeClass(t)
					: this.each(
							h.isFunction(t)
								? function(n) {
										h(this).toggleClass(t.call(this, n, this.className, e), e);
									}
								: function() {
										if ('string' === n)
											for (var e, i = 0, o = h(this), r = t.match(N) || []; (e = r[i++]); )
												o.hasClass(e) ? o.removeClass(e) : o.addClass(e);
										else
											(n === O || 'boolean' === n) &&
												(this.className && h._data(this, '__className__', this.className),
												(this.className =
													this.className || !1 === t
														? ''
														: h._data(this, '__className__') || ''));
									}
						);
			},
			hasClass: function(t) {
				for (var e = ' ' + t + ' ', n = 0, i = this.length; n < i; n++)
					if (1 === this[n].nodeType && 0 <= (' ' + this[n].className + ' ').replace(xe, ' ').indexOf(e))
						return !0;
				return !1;
			}
		}),
			h.each(
				'blur focus focusin focusout load resize scroll unload click dblclick mousedown mouseup mousemove mouseover mouseout mouseenter mouseleave change select submit keydown keypress keyup error contextmenu'.split(
					' '
				),
				function(t, e) {
					h.fn[e] = function(t, n) {
						return 0 < arguments.length ? this.on(e, null, t, n) : this.trigger(e);
					};
				}
			),
			h.fn.extend({
				hover: function(t, e) {
					return this.mouseenter(t).mouseleave(e || t);
				},
				bind: function(t, e, n) {
					return this.on(t, null, e, n);
				},
				unbind: function(t, e) {
					return this.off(t, null, e);
				},
				delegate: function(t, e, n, i) {
					return this.on(e, t, n, i);
				},
				undelegate: function(t, e, n) {
					return 1 === arguments.length ? this.off(t, '**') : this.off(e, t || '**', n);
				}
			});
		var ke = h.now(),
			$e = /\?/,
			Te = /(,)|(\[|{)|(}|])|"(?:[^"\\\r\n]|\\["\\\/bfnrt]|\\u[\da-fA-F]{4})*"\s*:?|true|false|null|-?(?!0\d)\d+(?:\.\d+|)(?:[eE][+-]?\d+|)/g;
		(h.parseJSON = function(e) {
			if (t.JSON && t.JSON.parse) return t.JSON.parse(e + '');
			var n,
				i = null,
				o = h.trim(e + '');
			return o &&
			!h.trim(
				o.replace(Te, function(t, e, o, r) {
					return n && e && (i = 0), 0 === i ? t : ((n = o || e), (i += !r - !o), '');
				})
			)
				? Function('return ' + o)()
				: h.error('Invalid JSON: ' + e);
		}),
			(h.parseXML = function(e) {
				var n;
				if (!e || 'string' != typeof e) return null;
				try {
					t.DOMParser
						? (n = new DOMParser().parseFromString(e, 'text/xml'))
						: (((n = new ActiveXObject('Microsoft.XMLDOM')).async = 'false'), n.loadXML(e));
				} catch (e) {
					n = void 0;
				}
				return (
					(n && n.documentElement && !n.getElementsByTagName('parsererror').length) ||
						h.error('Invalid XML: ' + e),
					n
				);
			});
		var Ee,
			Ce,
			_e = /#.*$/,
			Se = /([?&])_=[^&]*/,
			je = /^(.*?):[ \t]*([^\r\n]*)\r?$/gm,
			Ne = /^(?:GET|HEAD)$/,
			Ae = /^\/\//,
			Ie = /^([\w.+-]+:)(?:\/\/(?:[^\/?#]*@|)([^\/?#:]*)(?::(\d+)|)|)/,
			Le = {},
			De = {},
			Oe = '*/'.concat('*');
		try {
			Ce = location.href;
		} catch (e) {
			((Ce = T.createElement('a')).href = ''), (Ce = Ce.href);
		}
		function Pe(t) {
			return function(e, n) {
				'string' != typeof e && ((n = e), (e = '*'));
				var i,
					o = 0,
					r = e.toLowerCase().match(N) || [];
				if (h.isFunction(n))
					for (; (i = r[o++]); )
						'+' === i.charAt(0)
							? ((i = i.slice(1) || '*'), (t[i] = t[i] || []).unshift(n))
							: (t[i] = t[i] || []).push(n);
			};
		}
		function Re(t, e, n, i) {
			var o = {},
				r = t === De;
			function s(a) {
				var l;
				return (
					(o[a] = !0),
					h.each(t[a] || [], function(t, a) {
						var c = a(e, n, i);
						return 'string' != typeof c || r || o[c]
							? r ? !(l = c) : void 0
							: (e.dataTypes.unshift(c), s(c), !1);
					}),
					l
				);
			}
			return s(e.dataTypes[0]) || (!o['*'] && s('*'));
		}
		function Me(t, e) {
			var n,
				i,
				o = h.ajaxSettings.flatOptions || {};
			for (i in e) void 0 !== e[i] && ((o[i] ? t : n || (n = {}))[i] = e[i]);
			return n && h.extend(!0, t, n), t;
		}
		(Ee = Ie.exec(Ce.toLowerCase()) || []),
			h.extend({
				active: 0,
				lastModified: {},
				etag: {},
				ajaxSettings: {
					url: Ce,
					type: 'GET',
					isLocal: /^(?:about|app|app-storage|.+-extension|file|res|widget):$/.test(Ee[1]),
					global: !0,
					processData: !0,
					async: !0,
					contentType: 'application/x-www-form-urlencoded; charset=UTF-8',
					accepts: {
						'*': Oe,
						text: 'text/plain',
						html: 'text/html',
						xml: 'application/xml, text/xml',
						json: 'application/json, text/javascript'
					},
					contents: { xml: /xml/, html: /html/, json: /json/ },
					responseFields: { xml: 'responseXML', text: 'responseText', json: 'responseJSON' },
					converters: { '* text': String, 'text html': !0, 'text json': h.parseJSON, 'text xml': h.parseXML },
					flatOptions: { url: !0, context: !0 }
				},
				ajaxSetup: function(t, e) {
					return e ? Me(Me(t, h.ajaxSettings), e) : Me(h.ajaxSettings, t);
				},
				ajaxPrefilter: Pe(Le),
				ajaxTransport: Pe(De),
				ajax: function(t, e) {
					'object' == typeof t && ((e = t), (t = void 0)), (e = e || {});
					var n,
						i,
						o,
						r,
						s,
						a,
						l,
						c,
						u = h.ajaxSetup({}, e),
						d = u.context || u,
						p = u.context && (d.nodeType || d.jquery) ? h(d) : h.event,
						f = h.Deferred(),
						m = h.Callbacks('once memory'),
						g = u.statusCode || {},
						v = {},
						b = {},
						y = 0,
						w = 'canceled',
						x = {
							readyState: 0,
							getResponseHeader: function(t) {
								var e;
								if (2 === y) {
									if (!c) for (c = {}; (e = je.exec(r)); ) c[e[1].toLowerCase()] = e[2];
									e = c[t.toLowerCase()];
								}
								return null == e ? null : e;
							},
							getAllResponseHeaders: function() {
								return 2 === y ? r : null;
							},
							setRequestHeader: function(t, e) {
								var n = t.toLowerCase();
								return y || ((t = b[n] = b[n] || t), (v[t] = e)), this;
							},
							overrideMimeType: function(t) {
								return y || (u.mimeType = t), this;
							},
							statusCode: function(t) {
								var e;
								if (t)
									if (y < 2) for (e in t) g[e] = [ g[e], t[e] ];
									else x.always(t[x.status]);
								return this;
							},
							abort: function(t) {
								var e = t || w;
								return l && l.abort(e), k(0, e), this;
							}
						};
					if (
						((f.promise(x).complete = m.add),
						(x.success = x.done),
						(x.error = x.fail),
						(u.url = ((t || u.url || Ce) + '').replace(_e, '').replace(Ae, Ee[1] + '//')),
						(u.type = e.method || e.type || u.method || u.type),
						(u.dataTypes = h.trim(u.dataType || '*').toLowerCase().match(N) || [ '' ]),
						null == u.crossDomain &&
							((n = Ie.exec(u.url.toLowerCase())),
							(u.crossDomain = !(
								!n ||
								(n[1] === Ee[1] &&
									n[2] === Ee[2] &&
									(n[3] || ('http:' === n[1] ? '80' : '443')) ===
										(Ee[3] || ('http:' === Ee[1] ? '80' : '443')))
							))),
						u.data &&
							u.processData &&
							'string' != typeof u.data &&
							(u.data = h.param(u.data, u.traditional)),
						Re(Le, u, e, x),
						2 === y)
					)
						return x;
					for (i in ((a = h.event && u.global) && 0 == h.active++ && h.event.trigger('ajaxStart'),
					(u.type = u.type.toUpperCase()),
					(u.hasContent = !Ne.test(u.type)),
					(o = u.url),
					u.hasContent ||
						(u.data && ((o = u.url += ($e.test(o) ? '&' : '?') + u.data), delete u.data),
						!1 === u.cache &&
							(u.url = Se.test(o)
								? o.replace(Se, '$1_=' + ke++)
								: o + ($e.test(o) ? '&' : '?') + '_=' + ke++)),
					u.ifModified &&
						(h.lastModified[o] && x.setRequestHeader('If-Modified-Since', h.lastModified[o]),
						h.etag[o] && x.setRequestHeader('If-None-Match', h.etag[o])),
					((u.data && u.hasContent && !1 !== u.contentType) || e.contentType) &&
						x.setRequestHeader('Content-Type', u.contentType),
					x.setRequestHeader(
						'Accept',
						u.dataTypes[0] && u.accepts[u.dataTypes[0]]
							? u.accepts[u.dataTypes[0]] + ('*' !== u.dataTypes[0] ? ', ' + Oe + '; q=0.01' : '')
							: u.accepts['*']
					),
					u.headers))
						x.setRequestHeader(i, u.headers[i]);
					if (u.beforeSend && (!1 === u.beforeSend.call(d, x, u) || 2 === y)) return x.abort();
					for (i in ((w = 'abort'), { success: 1, error: 1, complete: 1 })) x[i](u[i]);
					if ((l = Re(De, u, e, x))) {
						(x.readyState = 1),
							a && p.trigger('ajaxSend', [ x, u ]),
							u.async &&
								0 < u.timeout &&
								(s = setTimeout(function() {
									x.abort('timeout');
								}, u.timeout));
						try {
							(y = 1), l.send(v, k);
						} catch (t) {
							if (!(y < 2)) throw t;
							k(-1, t);
						}
					} else k(-1, 'No Transport');
					function k(t, e, n, i) {
						var c,
							v,
							b,
							w,
							k,
							$ = e;
						2 !== y &&
							((y = 2),
							s && clearTimeout(s),
							(l = void 0),
							(r = i || ''),
							(x.readyState = 0 < t ? 4 : 0),
							(c = (200 <= t && t < 300) || 304 === t),
							n &&
								(w = (function(t, e, n) {
									for (var i, o, r, s, a = t.contents, l = t.dataTypes; '*' === l[0]; )
										l.shift(),
											void 0 === o && (o = t.mimeType || e.getResponseHeader('Content-Type'));
									if (o)
										for (s in a)
											if (a[s] && a[s].test(o)) {
												l.unshift(s);
												break;
											}
									if (l[0] in n) r = l[0];
									else {
										for (s in n) {
											if (!l[0] || t.converters[s + ' ' + l[0]]) {
												r = s;
												break;
											}
											i || (i = s);
										}
										r = r || i;
									}
									return r ? (r !== l[0] && l.unshift(r), n[r]) : void 0;
								})(u, x, n)),
							(w = (function(t, e, n, i) {
								var o,
									r,
									s,
									a,
									l,
									c = {},
									u = t.dataTypes.slice();
								if (u[1]) for (s in t.converters) c[s.toLowerCase()] = t.converters[s];
								for (r = u.shift(); r; )
									if (
										(t.responseFields[r] && (n[t.responseFields[r]] = e),
										!l && i && t.dataFilter && (e = t.dataFilter(e, t.dataType)),
										(l = r),
										(r = u.shift()))
									)
										if ('*' === r) r = l;
										else if ('*' !== l && l !== r) {
											if (!(s = c[l + ' ' + r] || c['* ' + r]))
												for (o in c)
													if (
														(a = o.split(' '))[1] === r &&
														(s = c[l + ' ' + a[0]] || c['* ' + a[0]])
													) {
														!0 === s
															? (s = c[o])
															: !0 !== c[o] && ((r = a[0]), u.unshift(a[1]));
														break;
													}
											if (!0 !== s)
												if (s && t.throws) e = s(e);
												else
													try {
														e = s(e);
													} catch (t) {
														return {
															state: 'parsererror',
															error: s ? t : 'No conversion from ' + l + ' to ' + r
														};
													}
										}
								return { state: 'success', data: e };
							})(u, w, x, c)),
							c
								? (u.ifModified &&
										((k = x.getResponseHeader('Last-Modified')) && (h.lastModified[o] = k),
										(k = x.getResponseHeader('etag')) && (h.etag[o] = k)),
									204 === t || 'HEAD' === u.type
										? ($ = 'nocontent')
										: 304 === t
											? ($ = 'notmodified')
											: (($ = w.state), (v = w.data), (c = !(b = w.error))))
								: ((b = $), (t || !$) && (($ = 'error'), t < 0 && (t = 0))),
							(x.status = t),
							(x.statusText = (e || $) + ''),
							c ? f.resolveWith(d, [ v, $, x ]) : f.rejectWith(d, [ x, $, b ]),
							x.statusCode(g),
							(g = void 0),
							a && p.trigger(c ? 'ajaxSuccess' : 'ajaxError', [ x, u, c ? v : b ]),
							m.fireWith(d, [ x, $ ]),
							a && (p.trigger('ajaxComplete', [ x, u ]), --h.active || h.event.trigger('ajaxStop')));
					}
					return x;
				},
				getJSON: function(t, e, n) {
					return h.get(t, e, n, 'json');
				},
				getScript: function(t, e) {
					return h.get(t, void 0, e, 'script');
				}
			}),
			h.each([ 'get', 'post' ], function(t, e) {
				h[e] = function(t, n, i, o) {
					return (
						h.isFunction(n) && ((o = o || i), (i = n), (n = void 0)),
						h.ajax({ url: t, type: e, dataType: o, data: n, success: i })
					);
				};
			}),
			(h._evalUrl = function(t) {
				return h.ajax({ url: t, type: 'GET', dataType: 'script', async: !1, global: !1, throws: !0 });
			}),
			h.fn.extend({
				wrapAll: function(t) {
					if (h.isFunction(t))
						return this.each(function(e) {
							h(this).wrapAll(t.call(this, e));
						});
					if (this[0]) {
						var e = h(t, this[0].ownerDocument).eq(0).clone(!0);
						this[0].parentNode && e.insertBefore(this[0]),
							e
								.map(function() {
									for (var t = this; t.firstChild && 1 === t.firstChild.nodeType; ) t = t.firstChild;
									return t;
								})
								.append(this);
					}
					return this;
				},
				wrapInner: function(t) {
					return this.each(
						h.isFunction(t)
							? function(e) {
									h(this).wrapInner(t.call(this, e));
								}
							: function() {
									var e = h(this),
										n = e.contents();
									n.length ? n.wrapAll(t) : e.append(t);
								}
					);
				},
				wrap: function(t) {
					var e = h.isFunction(t);
					return this.each(function(n) {
						h(this).wrapAll(e ? t.call(this, n) : t);
					});
				},
				unwrap: function() {
					return this.parent()
						.each(function() {
							h.nodeName(this, 'body') || h(this).replaceWith(this.childNodes);
						})
						.end();
				}
			}),
			(h.expr.filters.hidden = function(t) {
				return (
					(t.offsetWidth <= 0 && t.offsetHeight <= 0) ||
					(!u.reliableHiddenOffsets() && 'none' === ((t.style && t.style.display) || h.css(t, 'display')))
				);
			}),
			(h.expr.filters.visible = function(t) {
				return !h.expr.filters.hidden(t);
			});
		var qe = /%20/g,
			ze = /\[\]$/,
			Ue = /\r?\n/g,
			He = /^(?:submit|button|image|reset|file)$/i,
			Ge = /^(?:input|select|textarea|keygen)/i;
		function Fe(t, e, n, i) {
			var o;
			if (h.isArray(e))
				h.each(e, function(e, o) {
					n || ze.test(t) ? i(t, o) : Fe(t + '[' + ('object' == typeof o ? e : '') + ']', o, n, i);
				});
			else if (n || 'object' !== h.type(e)) i(t, e);
			else for (o in e) Fe(t + '[' + o + ']', e[o], n, i);
		}
		(h.param = function(t, e) {
			var n,
				i = [],
				o = function(t, e) {
					(e = h.isFunction(e) ? e() : null == e ? '' : e),
						(i[i.length] = encodeURIComponent(t) + '=' + encodeURIComponent(e));
				};
			if (
				(void 0 === e && (e = h.ajaxSettings && h.ajaxSettings.traditional),
				h.isArray(t) || (t.jquery && !h.isPlainObject(t)))
			)
				h.each(t, function() {
					o(this.name, this.value);
				});
			else for (n in t) Fe(n, t[n], e, o);
			return i.join('&').replace(qe, '+');
		}),
			h.fn.extend({
				serialize: function() {
					return h.param(this.serializeArray());
				},
				serializeArray: function() {
					return this.map(function() {
						var t = h.prop(this, 'elements');
						return t ? h.makeArray(t) : this;
					})
						.filter(function() {
							var t = this.type;
							return (
								this.name &&
								!h(this).is(':disabled') &&
								Ge.test(this.nodeName) &&
								!He.test(t) &&
								(this.checked || !W.test(t))
							);
						})
						.map(function(t, e) {
							var n = h(this).val();
							return null == n
								? null
								: h.isArray(n)
									? h.map(n, function(t) {
											return { name: e.name, value: t.replace(Ue, '\r\n') };
										})
									: { name: e.name, value: n.replace(Ue, '\r\n') };
						})
						.get();
				}
			}),
			(h.ajaxSettings.xhr =
				void 0 !== t.ActiveXObject
					? function() {
							return (
								(!this.isLocal && /^(get|post|head|put|delete|options)$/i.test(this.type) && Qe()) ||
								(function() {
									try {
										return new t.ActiveXObject('Microsoft.XMLHTTP');
									} catch (t) {}
								})()
							);
						}
					: Qe);
		var Be = 0,
			We = {},
			Ve = h.ajaxSettings.xhr();
		function Qe() {
			try {
				return new t.XMLHttpRequest();
			} catch (t) {}
		}
		t.attachEvent &&
			t.attachEvent('onunload', function() {
				for (var t in We) We[t](void 0, !0);
			}),
			(u.cors = !!Ve && 'withCredentials' in Ve),
			(Ve = u.ajax = !!Ve) &&
				h.ajaxTransport(function(t) {
					var e;
					if (!t.crossDomain || u.cors)
						return {
							send: function(n, i) {
								var o,
									r = t.xhr(),
									s = ++Be;
								if ((r.open(t.type, t.url, t.async, t.username, t.password), t.xhrFields))
									for (o in t.xhrFields) r[o] = t.xhrFields[o];
								for (o in (t.mimeType && r.overrideMimeType && r.overrideMimeType(t.mimeType),
								t.crossDomain || n['X-Requested-With'] || (n['X-Requested-With'] = 'XMLHttpRequest'),
								n))
									void 0 !== n[o] && r.setRequestHeader(o, n[o] + '');
								r.send((t.hasContent && t.data) || null),
									(e = function(n, o) {
										var a, l, c;
										if (e && (o || 4 === r.readyState))
											if ((delete We[s], (e = void 0), (r.onreadystatechange = h.noop), o))
												4 !== r.readyState && r.abort();
											else {
												(c = {}),
													(a = r.status),
													'string' == typeof r.responseText && (c.text = r.responseText);
												try {
													l = r.statusText;
												} catch (n) {
													l = '';
												}
												a || !t.isLocal || t.crossDomain
													? 1223 === a && (a = 204)
													: (a = c.text ? 200 : 404);
											}
										c && i(a, l, c, r.getAllResponseHeaders());
									}),
									t.async
										? 4 === r.readyState ? setTimeout(e) : (r.onreadystatechange = We[s] = e)
										: e();
							},
							abort: function() {
								e && e(void 0, !0);
							}
						};
				}),
			h.ajaxSetup({
				accepts: {
					script: 'text/javascript, application/javascript, application/ecmascript, application/x-ecmascript'
				},
				contents: { script: /(?:java|ecma)script/ },
				converters: {
					'text script': function(t) {
						return h.globalEval(t), t;
					}
				}
			}),
			h.ajaxPrefilter('script', function(t) {
				void 0 === t.cache && (t.cache = !1), t.crossDomain && ((t.type = 'GET'), (t.global = !1));
			}),
			h.ajaxTransport('script', function(t) {
				if (t.crossDomain) {
					var e,
						n = T.head || h('head')[0] || T.documentElement;
					return {
						send: function(i, o) {
							((e = T.createElement('script')).async = !0),
								t.scriptCharset && (e.charset = t.scriptCharset),
								(e.src = t.url),
								(e.onload = e.onreadystatechange = function(t, n) {
									(n || !e.readyState || /loaded|complete/.test(e.readyState)) &&
										((e.onload = e.onreadystatechange = null),
										e.parentNode && e.parentNode.removeChild(e),
										(e = null),
										n || o(200, 'success'));
								}),
								n.insertBefore(e, n.firstChild);
						},
						abort: function() {
							e && e.onload(void 0, !0);
						}
					};
				}
			});
		var Xe = [],
			Ke = /(=)\?(?=&|$)|\?\?/;
		h.ajaxSetup({
			jsonp: 'callback',
			jsonpCallback: function() {
				var t = Xe.pop() || h.expando + '_' + ke++;
				return (this[t] = !0), t;
			}
		}),
			h.ajaxPrefilter('json jsonp', function(e, n, i) {
				var o,
					r,
					s,
					a =
						!1 !== e.jsonp &&
						(Ke.test(e.url)
							? 'url'
							: 'string' == typeof e.data &&
								!(e.contentType || '').indexOf('application/x-www-form-urlencoded') &&
								Ke.test(e.data) &&
								'data');
				return a || 'jsonp' === e.dataTypes[0]
					? ((o = e.jsonpCallback = h.isFunction(e.jsonpCallback) ? e.jsonpCallback() : e.jsonpCallback),
						a
							? (e[a] = e[a].replace(Ke, '$1' + o))
							: !1 !== e.jsonp && (e.url += ($e.test(e.url) ? '&' : '?') + e.jsonp + '=' + o),
						(e.converters['script json'] = function() {
							return s || h.error(o + ' was not called'), s[0];
						}),
						(e.dataTypes[0] = 'json'),
						(r = t[o]),
						(t[o] = function() {
							s = arguments;
						}),
						i.always(function() {
							(t[o] = r),
								e[o] && ((e.jsonpCallback = n.jsonpCallback), Xe.push(o)),
								s && h.isFunction(r) && r(s[0]),
								(s = r = void 0);
						}),
						'script')
					: void 0;
			}),
			(h.parseHTML = function(t, e, n) {
				if (!t || 'string' != typeof t) return null;
				'boolean' == typeof e && ((n = e), (e = !1)), (e = e || T);
				var i = w.exec(t),
					o = !n && [];
				return i
					? [ e.createElement(i[1]) ]
					: ((i = h.buildFragment([ t ], e, o)), o && o.length && h(o).remove(), h.merge([], i.childNodes));
			});
		var Ye = h.fn.load;
		(h.fn.load = function(t, e, n) {
			if ('string' != typeof t && Ye) return Ye.apply(this, arguments);
			var i,
				o,
				r,
				s = this,
				a = t.indexOf(' ');
			return (
				0 <= a && ((i = h.trim(t.slice(a, t.length))), (t = t.slice(0, a))),
				h.isFunction(e) ? ((n = e), (e = void 0)) : e && 'object' == typeof e && (r = 'POST'),
				0 < s.length &&
					h
						.ajax({ url: t, type: r, dataType: 'html', data: e })
						.done(function(t) {
							(o = arguments), s.html(i ? h('<div>').append(h.parseHTML(t)).find(i) : t);
						})
						.complete(
							n &&
								function(t, e) {
									s.each(n, o || [ t.responseText, e, t ]);
								}
						),
				this
			);
		}),
			h.each([ 'ajaxStart', 'ajaxStop', 'ajaxComplete', 'ajaxError', 'ajaxSuccess', 'ajaxSend' ], function(t, e) {
				h.fn[e] = function(t) {
					return this.on(e, t);
				};
			}),
			(h.expr.filters.animated = function(t) {
				return h.grep(h.timers, function(e) {
					return t === e.elem;
				}).length;
			});
		var Je = t.document.documentElement;
		function Ze(t) {
			return h.isWindow(t) ? t : 9 === t.nodeType && (t.defaultView || t.parentWindow);
		}
		(h.offset = {
			setOffset: function(t, e, n) {
				var i,
					o,
					r,
					s,
					a,
					l,
					c = h.css(t, 'position'),
					u = h(t),
					d = {};
				'static' === c && (t.style.position = 'relative'),
					(a = u.offset()),
					(r = h.css(t, 'top')),
					(l = h.css(t, 'left')),
					('absolute' === c || 'fixed' === c) && -1 < h.inArray('auto', [ r, l ])
						? ((s = (i = u.position()).top), (o = i.left))
						: ((s = parseFloat(r) || 0), (o = parseFloat(l) || 0)),
					h.isFunction(e) && (e = e.call(t, n, a)),
					null != e.top && (d.top = e.top - a.top + s),
					null != e.left && (d.left = e.left - a.left + o),
					'using' in e ? e.using.call(t, d) : u.css(d);
			}
		}),
			h.fn.extend({
				offset: function(t) {
					if (arguments.length)
						return void 0 === t
							? this
							: this.each(function(e) {
									h.offset.setOffset(this, t, e);
								});
					var e,
						n,
						i = { top: 0, left: 0 },
						o = this[0],
						r = o && o.ownerDocument;
					return r
						? ((e = r.documentElement),
							h.contains(e, o)
								? (typeof o.getBoundingClientRect !== O && (i = o.getBoundingClientRect()),
									(n = Ze(r)),
									{
										top: i.top + (n.pageYOffset || e.scrollTop) - (e.clientTop || 0),
										left: i.left + (n.pageXOffset || e.scrollLeft) - (e.clientLeft || 0)
									})
								: i)
						: void 0;
				},
				position: function() {
					if (this[0]) {
						var t,
							e,
							n = { top: 0, left: 0 },
							i = this[0];
						return (
							'fixed' === h.css(i, 'position')
								? (e = i.getBoundingClientRect())
								: ((t = this.offsetParent()),
									(e = this.offset()),
									h.nodeName(t[0], 'html') || (n = t.offset()),
									(n.top += h.css(t[0], 'borderTopWidth', !0)),
									(n.left += h.css(t[0], 'borderLeftWidth', !0))),
							{
								top: e.top - n.top - h.css(i, 'marginTop', !0),
								left: e.left - n.left - h.css(i, 'marginLeft', !0)
							}
						);
					}
				},
				offsetParent: function() {
					return this.map(function() {
						for (
							var t = this.offsetParent || Je;
							t && !h.nodeName(t, 'html') && 'static' === h.css(t, 'position');

						)
							t = t.offsetParent;
						return t || Je;
					});
				}
			}),
			h.each({ scrollLeft: 'pageXOffset', scrollTop: 'pageYOffset' }, function(t, e) {
				var n = /Y/.test(e);
				h.fn[t] = function(i) {
					return B(
						this,
						function(t, i, o) {
							var r = Ze(t);
							return void 0 === o
								? r ? (e in r ? r[e] : r.document.documentElement[i]) : t[i]
								: void (r
										? r.scrollTo(n ? h(r).scrollLeft() : o, n ? o : h(r).scrollTop())
										: (t[i] = o));
						},
						t,
						i,
						arguments.length,
						null
					);
				};
			}),
			h.each([ 'top', 'left' ], function(t, e) {
				h.cssHooks[e] = Ot(u.pixelPosition, function(t, n) {
					return n ? ((n = At(t, e)), Lt.test(n) ? h(t).position()[e] + 'px' : n) : void 0;
				});
			}),
			h.each({ Height: 'height', Width: 'width' }, function(t, e) {
				h.each({ padding: 'inner' + t, content: e, '': 'outer' + t }, function(n, i) {
					h.fn[i] = function(i, o) {
						var r = arguments.length && (n || 'boolean' != typeof i),
							s = n || (!0 === i || !0 === o ? 'margin' : 'border');
						return B(
							this,
							function(e, n, i) {
								var o;
								return h.isWindow(e)
									? e.document.documentElement['client' + t]
									: 9 === e.nodeType
										? ((o = e.documentElement),
											Math.max(
												e.body['scroll' + t],
												o['scroll' + t],
												e.body['offset' + t],
												o['offset' + t],
												o['client' + t]
											))
										: void 0 === i ? h.css(e, n, s) : h.style(e, n, i, s);
							},
							e,
							r ? i : void 0,
							r,
							null
						);
					};
				});
			}),
			(h.fn.size = function() {
				return this.length;
			}),
			(h.fn.andSelf = h.fn.addBack),
			'function' == typeof define &&
				define.amd &&
				define('jquery', [], function() {
					return h;
				});
		var tn = t.jQuery,
			en = t.$;
		return (
			(h.noConflict = function(e) {
				return t.$ === h && (t.$ = en), e && t.jQuery === h && (t.jQuery = tn), h;
			}),
			typeof e === O && (t.jQuery = t.$ = h),
			h
		);
	}),
	'undefined' == typeof jQuery)
)
	throw new Error("Bootstrap's JavaScript requires jQuery");
function showResult(t, e) {
	if (($('#live-search-dropdown ul').empty(), '' != $('#search-live').val()))
		for (result in ($('#live-search-dropdown').show(), t)) {
			var n,
				i = t[result].product_id,
				o = t[result].seller_id;
			(n = t[result].special ? t[result].special : t[result].price),
				$('#live-search-dropdown ul').append(
					'<li> <img src="' +
						t[result].image +
						'"><a data-toggle="modal" data-target="#myModal" onclick="getModalContent(' +
						i +
						',' +
						o +
						',1);" href="javascript: void(0)">' +
						t[result].name +
						'</a> &nbsp' +
						n +
						'<button id= "' +
						t[result].product_id +
						',' +
						t[result].seller_id +
						'"title="Add to Cart" type="button" class="addcart"><i class="fa fa-plus"></i></button></li><hr style="margin-top: 1px; margin-bottom: 1px;">'
				);
		}
	else $('#live-search-dropdown').hide();
}
function showCartnotification(t) {
	t.redirect && (location = t.redirect),
		t.success &&
			(addProductNotice(t.title, t.thumb, t.success, 'success'),
			$('.b-cart-total').html(t.total),
			$('.b-cart > ul').load('index.php?route=common/cart/info ul li'));
}
function ssc_init() {
	if (document.body) {
		var t = document.body,
			e = document.documentElement,
			n = window.innerHeight,
			i = t.scrollHeight;
		if (
			((ssc_root = 0 <= document.compatMode.indexOf('CSS') ? e : t),
			(ssc_activeElement = t),
			(ssc_initdone = !0),
			top != self)
		)
			ssc_frame = !0;
		else if (
			n < i &&
			(t.offsetHeight <= n || e.offsetHeight <= n) &&
			((ssc_root.style.height = 'auto'), ssc_root.offsetHeight <= n)
		) {
			var o = document.createElement('div');
			(o.style.clear = 'both'), t.appendChild(o);
		}
		ssc_fixedback || ((t.style.backgroundAttachment = 'scroll'), (e.style.backgroundAttachment = 'scroll')),
			ssc_keyboardsupport && ssc_addEvent('keydown', ssc_keydown);
	}
}
function ssc_scrollArray(t, e, n, i) {
	if (
		(i || (i = 1e3),
		ssc_directionCheck(e, n),
		ssc_que.push({ x: e, y: n, lastX: e < 0 ? 0.99 : -0.99, lastY: n < 0 ? 0.99 : -0.99, start: +new Date() }),
		!ssc_pending)
	) {
		var o = function() {
			for (var r = +new Date(), s = 0, a = 0, l = 0; l < ssc_que.length; l++) {
				var c = ssc_que[l],
					u = r - c.start,
					d = ssc_animtime <= u,
					h = d ? 1 : u / ssc_animtime;
				ssc_pulseAlgorithm && (h = ssc_pulse(h));
				var p = (c.x * h - c.lastX) >> 0,
					f = (c.y * h - c.lastY) >> 0;
				(s += p), (a += f), (c.lastX += p), (c.lastY += f), d && (ssc_que.splice(l, 1), l--);
			}
			if (e) {
				var m = t.scrollLeft;
				(t.scrollLeft += s), s && t.scrollLeft === m && (e = 0);
			}
			if (n) {
				var g = t.scrollTop;
				(t.scrollTop += a), a && t.scrollTop === g && (n = 0);
			}
			e || n || (ssc_que = []), ssc_que.length ? setTimeout(o, i / ssc_framerate + 1) : (ssc_pending = !1);
		};
		setTimeout(o, 0), (ssc_pending = !0);
	}
}
function ssc_wheel(t) {
	ssc_initdone || ssc_init();
	var e = t.target,
		n = ssc_overflowingAncestor(e);
	if (
		!n ||
		t.defaultPrevented ||
		ssc_isNodeName(ssc_activeElement, 'embed') ||
		(ssc_isNodeName(e, 'embed') && /\.pdf/i.test(e.src))
	)
		return !0;
	var i = t.wheelDeltaX || 0,
		o = t.wheelDeltaY || 0;
	i || o || (o = t.wheelDelta || 0),
		1.2 < Math.abs(i) && (i *= ssc_stepsize / 120),
		1.2 < Math.abs(o) && (o *= ssc_stepsize / 120),
		ssc_scrollArray(n, -i, -o),
		t.preventDefault();
}
function ssc_keydown(t) {
	var e = t.target,
		n = t.ctrlKey || t.altKey || t.metaKey;
	if (/input|textarea|embed/i.test(e.nodeName) || e.isContentEditable || t.defaultPrevented || n) return !0;
	if (ssc_isNodeName(e, 'button') && t.keyCode === ssc_key.spacebar) return !0;
	var i = 0,
		o = 0,
		r = ssc_overflowingAncestor(ssc_activeElement),
		s = r.clientHeight;
	switch ((r == document.body && (s = window.innerHeight), t.keyCode)) {
		case ssc_key.up:
			o = -ssc_arrowscroll;
			break;
		case ssc_key.down:
			o = ssc_arrowscroll;
			break;
		case ssc_key.spacebar:
			o = -(t.shiftKey ? 1 : -1) * s * 0.9;
			break;
		case ssc_key.pageup:
			o = 0.9 * -s;
			break;
		case ssc_key.pagedown:
			o = 0.9 * s;
			break;
		case ssc_key.home:
			o = -r.scrollTop;
			break;
		case ssc_key.end:
			var a = r.scrollHeight - r.scrollTop - s;
			o = 0 < a ? a + 10 : 0;
			break;
		case ssc_key.left:
			i = -ssc_arrowscroll;
			break;
		case ssc_key.right:
			i = ssc_arrowscroll;
			break;
		default:
			return !0;
	}
	ssc_scrollArray(r, i, o), t.preventDefault();
}
function ssc_mousedown(t) {
	ssc_activeElement = t.target;
}
function ssc_setCache(t, e) {
	for (var n = t.length; n--; ) ssc_cache[ssc_uniqueID(t[n])] = e;
	return e;
}
function ssc_overflowingAncestor(t) {
	var e = [],
		n = ssc_root.scrollHeight;
	do {
		var i = ssc_cache[ssc_uniqueID(t)];
		if (i) return ssc_setCache(e, i);
		if ((e.push(t), n === t.scrollHeight)) {
			if (!ssc_frame || ssc_root.clientHeight + 10 < n) return ssc_setCache(e, document.body);
		} else if (
			t.clientHeight + 10 < t.scrollHeight &&
			((overflow = getComputedStyle(t, '').getPropertyValue('overflow')),
			'scroll' === overflow || 'auto' === overflow)
		)
			return ssc_setCache(e, t);
	} while ((t = t.parentNode));
}
function ssc_addEvent(t, e, n) {
	window.addEventListener(t, e, n || !1);
}
function ssc_removeEvent(t, e, n) {
	window.removeEventListener(t, e, n || !1);
}
function ssc_isNodeName(t, e) {
	return t.nodeName.toLowerCase() === e.toLowerCase();
}
function ssc_directionCheck(t, e) {
	(t = 0 < t ? 1 : -1),
		(e = 0 < e ? 1 : -1),
		(ssc_direction.x === t && ssc_direction.y === e) ||
			((ssc_direction.x = t), (ssc_direction.y = e), (ssc_que = []));
}
function ssc_pulse_(t) {
	var e, n;
	return (
		(t *= ssc_pulseScale) < 1
			? (e = t - (1 - Math.exp(-t)))
			: ((t -= 1), (e = (n = Math.exp(-1)) + (1 - Math.exp(-t)) * (1 - n))),
		e * ssc_pulseNormalize
	);
}
function ssc_pulse(t) {
	return 1 <= t ? 1 : t <= 0 ? 0 : (1 == ssc_pulseNormalize && (ssc_pulseNormalize /= ssc_pulse_(1)), ssc_pulse_(t));
}
!(function(t) {
	'use strict';
	var e = jQuery.fn.jquery.split(' ')[0].split('.');
	if ((e[0] < 2 && e[1] < 9) || (1 == e[0] && 9 == e[1] && e[2] < 1) || 2 < e[0])
		throw new Error("Bootstrap's JavaScript requires jQuery version 1.9.1 or higher, but lower than version 3");
})(),
	(function(t) {
		'use strict';
		(t.fn.emulateTransitionEnd = function(e) {
			var n = !1,
				i = this;
			return (
				t(this).one('bsTransitionEnd', function() {
					n = !0;
				}),
				setTimeout(function() {
					n || t(i).trigger(t.support.transition.end);
				}, e),
				this
			);
		}),
			t(function() {
				(t.support.transition = (function() {
					var t = document.createElement('bootstrap'),
						e = {
							WebkitTransition: 'webkitTransitionEnd',
							MozTransition: 'transitionend',
							OTransition: 'oTransitionEnd otransitionend',
							transition: 'transitionend'
						};
					for (var n in e) if (void 0 !== t.style[n]) return { end: e[n] };
					return !1;
				})()),
					t.support.transition &&
						(t.event.special.bsTransitionEnd = {
							bindType: t.support.transition.end,
							delegateType: t.support.transition.end,
							handle: function(e) {
								return t(e.target).is(this) ? e.handleObj.handler.apply(this, arguments) : void 0;
							}
						});
			});
	})(jQuery),
	(function(t) {
		'use strict';
		var e = '[data-dismiss="alert"]',
			n = function(n) {
				t(n).on('click', e, this.close);
			};
		(n.VERSION = '3.3.6'),
			(n.TRANSITION_DURATION = 150),
			(n.prototype.close = function(e) {
				function i() {
					s.detach().trigger('closed.bs.alert').remove();
				}
				var o = t(this),
					r = o.attr('data-target');
				r || (r = (r = o.attr('href')) && r.replace(/.*(?=#[^\s]*$)/, ''));
				var s = t(r);
				e && e.preventDefault(),
					s.length || (s = o.closest('.alert')),
					s.trigger((e = t.Event('close.bs.alert'))),
					e.isDefaultPrevented() ||
						(s.removeClass('in'),
						t.support.transition && s.hasClass('fade')
							? s.one('bsTransitionEnd', i).emulateTransitionEnd(n.TRANSITION_DURATION)
							: i());
			});
		var i = t.fn.alert;
		(t.fn.alert = function(e) {
			return this.each(function() {
				var i = t(this),
					o = i.data('bs.alert');
				o || i.data('bs.alert', (o = new n(this))), 'string' == typeof e && o[e].call(i);
			});
		}),
			(t.fn.alert.Constructor = n),
			(t.fn.alert.noConflict = function() {
				return (t.fn.alert = i), this;
			}),
			t(document).on('click.bs.alert.data-api', e, n.prototype.close);
	})(jQuery),
	(function(t) {
		'use strict';
		function e(e) {
			return this.each(function() {
				var i = t(this),
					o = i.data('bs.button'),
					r = 'object' == typeof e && e;
				o || i.data('bs.button', (o = new n(this, r))), 'toggle' == e ? o.toggle() : e && o.setState(e);
			});
		}
		var n = function(e, i) {
			(this.$element = t(e)), (this.options = t.extend({}, n.DEFAULTS, i)), (this.isLoading = !1);
		};
		(n.VERSION = '3.3.6'),
			(n.DEFAULTS = { loadingText: 'loading...' }),
			(n.prototype.setState = function(e) {
				var n = 'disabled',
					i = this.$element,
					o = i.is('input') ? 'val' : 'html',
					r = i.data();
				(e += 'Text'),
					null == r.resetText && i.data('resetText', i[o]()),
					setTimeout(
						t.proxy(function() {
							i[o](null == r[e] ? this.options[e] : r[e]),
								'loadingText' == e
									? ((this.isLoading = !0), i.addClass(n).attr(n, n))
									: this.isLoading && ((this.isLoading = !1), i.removeClass(n).removeAttr(n));
						}, this),
						0
					);
			}),
			(n.prototype.toggle = function() {
				var t = !0,
					e = this.$element.closest('[data-toggle="buttons"]');
				if (e.length) {
					var n = this.$element.find('input');
					'radio' == n.prop('type')
						? (n.prop('checked') && (t = !1),
							e.find('.active').removeClass('active'),
							this.$element.addClass('active'))
						: 'checkbox' == n.prop('type') &&
							(n.prop('checked') !== this.$element.hasClass('active') && (t = !1),
							this.$element.toggleClass('active')),
						n.prop('checked', this.$element.hasClass('active')),
						t && n.trigger('change');
				} else
					this.$element.attr('aria-pressed', !this.$element.hasClass('active')),
						this.$element.toggleClass('active');
			});
		var i = t.fn.button;
		(t.fn.button = e),
			(t.fn.button.Constructor = n),
			(t.fn.button.noConflict = function() {
				return (t.fn.button = i), this;
			}),
			t(document)
				.on('click.bs.button.data-api', '[data-toggle^="button"]', function(n) {
					var i = t(n.target);
					i.hasClass('btn') || (i = i.closest('.btn')),
						e.call(i, 'toggle'),
						t(n.target).is('input[type="radio"]') ||
							t(n.target).is('input[type="checkbox"]') ||
							n.preventDefault();
				})
				.on('focus.bs.button.data-api blur.bs.button.data-api', '[data-toggle^="button"]', function(e) {
					t(e.target).closest('.btn').toggleClass('focus', /^focus(in)?$/.test(e.type));
				});
	})(jQuery),
	(function(t) {
		'use strict';
		function e(e) {
			return this.each(function() {
				var i = t(this),
					o = i.data('bs.carousel'),
					r = t.extend({}, n.DEFAULTS, i.data(), 'object' == typeof e && e),
					s = 'string' == typeof e ? e : r.slide;
				o || i.data('bs.carousel', (o = new n(this, r))),
					'number' == typeof e ? o.to(e) : s ? o[s]() : r.interval && o.pause().cycle();
			});
		}
		var n = function(e, n) {
			(this.$element = t(e)),
				(this.$indicators = this.$element.find('.carousel-indicators')),
				(this.options = n),
				(this.paused = null),
				(this.sliding = null),
				(this.interval = null),
				(this.$active = null),
				(this.$items = null),
				this.options.keyboard && this.$element.on('keydown.bs.carousel', t.proxy(this.keydown, this)),
				'hover' == this.options.pause &&
					!('ontouchstart' in document.documentElement) &&
					this.$element
						.on('mouseenter.bs.carousel', t.proxy(this.pause, this))
						.on('mouseleave.bs.carousel', t.proxy(this.cycle, this));
		};
		(n.VERSION = '3.3.6'),
			(n.TRANSITION_DURATION = 600),
			(n.DEFAULTS = { interval: 5e3, pause: 'hover', wrap: !0, keyboard: !0 }),
			(n.prototype.keydown = function(t) {
				if (!/input|textarea/i.test(t.target.tagName)) {
					switch (t.which) {
						case 37:
							this.prev();
							break;
						case 39:
							this.next();
							break;
						default:
							return;
					}
					t.preventDefault();
				}
			}),
			(n.prototype.cycle = function(e) {
				return (
					e || (this.paused = !1),
					this.interval && clearInterval(this.interval),
					this.options.interval &&
						!this.paused &&
						(this.interval = setInterval(t.proxy(this.next, this), this.options.interval)),
					this
				);
			}),
			(n.prototype.getItemIndex = function(t) {
				return (this.$items = t.parent().children('.item')), this.$items.index(t || this.$active);
			}),
			(n.prototype.getItemForDirection = function(t, e) {
				var n = this.getItemIndex(e);
				if ((('prev' == t && 0 === n) || ('next' == t && n == this.$items.length - 1)) && !this.options.wrap)
					return e;
				var i = (n + ('prev' == t ? -1 : 1)) % this.$items.length;
				return this.$items.eq(i);
			}),
			(n.prototype.to = function(t) {
				var e = this,
					n = this.getItemIndex((this.$active = this.$element.find('.item.active')));
				return t > this.$items.length - 1 || t < 0
					? void 0
					: this.sliding
						? this.$element.one('slid.bs.carousel', function() {
								e.to(t);
							})
						: n == t ? this.pause().cycle() : this.slide(n < t ? 'next' : 'prev', this.$items.eq(t));
			}),
			(n.prototype.pause = function(e) {
				return (
					e || (this.paused = !0),
					this.$element.find('.next, .prev').length &&
						t.support.transition &&
						(this.$element.trigger(t.support.transition.end), this.cycle(!0)),
					(this.interval = clearInterval(this.interval)),
					this
				);
			}),
			(n.prototype.next = function() {
				return this.sliding ? void 0 : this.slide('next');
			}),
			(n.prototype.prev = function() {
				return this.sliding ? void 0 : this.slide('prev');
			}),
			(n.prototype.slide = function(e, i) {
				var o = this.$element.find('.item.active'),
					r = i || this.getItemForDirection(e, o),
					s = this.interval,
					a = 'next' == e ? 'left' : 'right',
					l = this;
				if (r.hasClass('active')) return (this.sliding = !1);
				var c = r[0],
					u = t.Event('slide.bs.carousel', { relatedTarget: c, direction: a });
				if ((this.$element.trigger(u), !u.isDefaultPrevented())) {
					if (((this.sliding = !0), s && this.pause(), this.$indicators.length)) {
						this.$indicators.find('.active').removeClass('active');
						var d = t(this.$indicators.children()[this.getItemIndex(r)]);
						d && d.addClass('active');
					}
					var h = t.Event('slid.bs.carousel', { relatedTarget: c, direction: a });
					return (
						t.support.transition && this.$element.hasClass('slide')
							? (r.addClass(e),
								r[0].offsetWidth,
								o.addClass(a),
								r.addClass(a),
								o
									.one('bsTransitionEnd', function() {
										r.removeClass([ e, a ].join(' ')).addClass('active'),
											o.removeClass([ 'active', a ].join(' ')),
											(l.sliding = !1),
											setTimeout(function() {
												l.$element.trigger(h);
											}, 0);
									})
									.emulateTransitionEnd(n.TRANSITION_DURATION))
							: (o.removeClass('active'),
								r.addClass('active'),
								(this.sliding = !1),
								this.$element.trigger(h)),
						s && this.cycle(),
						this
					);
				}
			});
		var i = t.fn.carousel;
		(t.fn.carousel = e),
			(t.fn.carousel.Constructor = n),
			(t.fn.carousel.noConflict = function() {
				return (t.fn.carousel = i), this;
			});
		var o = function(n) {
			var i,
				o = t(this),
				r = t(o.attr('data-target') || ((i = o.attr('href')) && i.replace(/.*(?=#[^\s]+$)/, '')));
			if (r.hasClass('carousel')) {
				var s = t.extend({}, r.data(), o.data()),
					a = o.attr('data-slide-to');
				a && (s.interval = !1), e.call(r, s), a && r.data('bs.carousel').to(a), n.preventDefault();
			}
		};
		t(document)
			.on('click.bs.carousel.data-api', '[data-slide]', o)
			.on('click.bs.carousel.data-api', '[data-slide-to]', o),
			t(window).on('load', function() {
				t('[data-ride="carousel"]').each(function() {
					var n = t(this);
					e.call(n, n.data());
				});
			});
	})(jQuery),
	(function(t) {
		'use strict';
		function e(e) {
			var n,
				i = e.attr('data-target') || ((n = e.attr('href')) && n.replace(/.*(?=#[^\s]+$)/, ''));
			return t(i);
		}
		function n(e) {
			return this.each(function() {
				var n = t(this),
					o = n.data('bs.collapse'),
					r = t.extend({}, i.DEFAULTS, n.data(), 'object' == typeof e && e);
				!o && r.toggle && /show|hide/.test(e) && (r.toggle = !1),
					o || n.data('bs.collapse', (o = new i(this, r))),
					'string' == typeof e && o[e]();
			});
		}
		var i = function(e, n) {
			(this.$element = t(e)),
				(this.options = t.extend({}, i.DEFAULTS, n)),
				(this.$trigger = t(
					'[data-toggle="collapse"][href="#' +
						e.id +
						'"],[data-toggle="collapse"][data-target="#' +
						e.id +
						'"]'
				)),
				(this.transitioning = null),
				this.options.parent
					? (this.$parent = this.getParent())
					: this.addAriaAndCollapsedClass(this.$element, this.$trigger),
				this.options.toggle && this.toggle();
		};
		(i.VERSION = '3.3.6'),
			(i.TRANSITION_DURATION = 350),
			(i.DEFAULTS = { toggle: !0 }),
			(i.prototype.dimension = function() {
				return this.$element.hasClass('width') ? 'width' : 'height';
			}),
			(i.prototype.show = function() {
				if (!this.transitioning && !this.$element.hasClass('in')) {
					var e,
						o = this.$parent && this.$parent.children('.panel').children('.in, .collapsing');
					if (!(o && o.length && (e = o.data('bs.collapse')) && e.transitioning)) {
						var r = t.Event('show.bs.collapse');
						if ((this.$element.trigger(r), !r.isDefaultPrevented())) {
							o && o.length && (n.call(o, 'hide'), e || o.data('bs.collapse', null));
							var s = this.dimension();
							this.$element
								.removeClass('collapse')
								.addClass('collapsing')
								[s](0)
								.attr('aria-expanded', !0),
								this.$trigger.removeClass('collapsed').attr('aria-expanded', !0),
								(this.transitioning = 1);
							var a = function() {
								this.$element.removeClass('collapsing').addClass('collapse in')[s](''),
									(this.transitioning = 0),
									this.$element.trigger('shown.bs.collapse');
							};
							if (!t.support.transition) return a.call(this);
							var l = t.camelCase([ 'scroll', s ].join('-'));
							this.$element
								.one('bsTransitionEnd', t.proxy(a, this))
								.emulateTransitionEnd(i.TRANSITION_DURATION)
								[s](this.$element[0][l]);
						}
					}
				}
			}),
			(i.prototype.hide = function() {
				if (!this.transitioning && this.$element.hasClass('in')) {
					var e = t.Event('hide.bs.collapse');
					if ((this.$element.trigger(e), !e.isDefaultPrevented())) {
						var n = this.dimension();
						this.$element[n](this.$element[n]())[0].offsetHeight,
							this.$element.addClass('collapsing').removeClass('collapse in').attr('aria-expanded', !1),
							this.$trigger.addClass('collapsed').attr('aria-expanded', !1),
							(this.transitioning = 1);
						var o = function() {
							(this.transitioning = 0),
								this.$element
									.removeClass('collapsing')
									.addClass('collapse')
									.trigger('hidden.bs.collapse');
						};
						return t.support.transition
							? void this.$element
									[n](0)
									.one('bsTransitionEnd', t.proxy(o, this))
									.emulateTransitionEnd(i.TRANSITION_DURATION)
							: o.call(this);
					}
				}
			}),
			(i.prototype.toggle = function() {
				this[this.$element.hasClass('in') ? 'hide' : 'show']();
			}),
			(i.prototype.getParent = function() {
				return t(this.options.parent)
					.find('[data-toggle="collapse"][data-parent="' + this.options.parent + '"]')
					.each(
						t.proxy(function(n, i) {
							var o = t(i);
							this.addAriaAndCollapsedClass(e(o), o);
						}, this)
					)
					.end();
			}),
			(i.prototype.addAriaAndCollapsedClass = function(t, e) {
				var n = t.hasClass('in');
				t.attr('aria-expanded', n), e.toggleClass('collapsed', !n).attr('aria-expanded', n);
			});
		var o = t.fn.collapse;
		(t.fn.collapse = n),
			(t.fn.collapse.Constructor = i),
			(t.fn.collapse.noConflict = function() {
				return (t.fn.collapse = o), this;
			}),
			t(document).on('click.bs.collapse.data-api', '[data-toggle="collapse"]', function(i) {
				var o = t(this);
				o.attr('data-target') || i.preventDefault();
				var r = e(o),
					s = r.data('bs.collapse') ? 'toggle' : o.data();
				n.call(r, s);
			});
	})(jQuery),
	(function(t) {
		'use strict';
		function e(e) {
			var n = e.attr('data-target');
			n || (n = (n = e.attr('href')) && /#[A-Za-z]/.test(n) && n.replace(/.*(?=#[^\s]*$)/, ''));
			var i = n && t(n);
			return i && i.length ? i : e.parent();
		}
		function n(n) {
			(n && 3 === n.which) ||
				(t('.dropdown-backdrop').remove(),
				t(i).each(function() {
					var i = t(this),
						o = e(i),
						r = { relatedTarget: this };
					o.hasClass('open') &&
						((n &&
							'click' == n.type &&
							/input|textarea/i.test(n.target.tagName) &&
							t.contains(o[0], n.target)) ||
							(o.trigger((n = t.Event('hide.bs.dropdown', r))),
							n.isDefaultPrevented() ||
								(i.attr('aria-expanded', 'false'),
								o.removeClass('open').trigger(t.Event('hidden.bs.dropdown', r)))));
				}));
		}
		var i = '[data-toggle="dropdown"]',
			o = function(e) {
				t(e).on('click.bs.dropdown', this.toggle);
			};
		(o.VERSION = '3.3.6'),
			(o.prototype.toggle = function(i) {
				var o = t(this);
				if (!o.is('.disabled, :disabled')) {
					var r = e(o),
						s = r.hasClass('open');
					if ((n(), !s)) {
						'ontouchstart' in document.documentElement &&
							!r.closest('.navbar-nav').length &&
							t(document.createElement('div'))
								.addClass('dropdown-backdrop')
								.insertAfter(t(this))
								.on('click', n);
						var a = { relatedTarget: this };
						if ((r.trigger((i = t.Event('show.bs.dropdown', a))), i.isDefaultPrevented())) return;
						o.trigger('focus').attr('aria-expanded', 'true'),
							r.toggleClass('open').trigger(t.Event('shown.bs.dropdown', a));
					}
					return !1;
				}
			}),
			(o.prototype.keydown = function(n) {
				if (/(38|40|27|32)/.test(n.which) && !/input|textarea/i.test(n.target.tagName)) {
					var o = t(this);
					if ((n.preventDefault(), n.stopPropagation(), !o.is('.disabled, :disabled'))) {
						var r = e(o),
							s = r.hasClass('open');
						if ((!s && 27 != n.which) || (s && 27 == n.which))
							return 27 == n.which && r.find(i).trigger('focus'), o.trigger('click');
						var a = r.find('.dropdown-menu li:not(.disabled):visible a');
						if (a.length) {
							var l = a.index(n.target);
							38 == n.which && 0 < l && l--,
								40 == n.which && l < a.length - 1 && l++,
								~l || (l = 0),
								a.eq(l).trigger('focus');
						}
					}
				}
			});
		var r = t.fn.dropdown;
		(t.fn.dropdown = function(e) {
			return this.each(function() {
				var n = t(this),
					i = n.data('bs.dropdown');
				i || n.data('bs.dropdown', (i = new o(this))), 'string' == typeof e && i[e].call(n);
			});
		}),
			(t.fn.dropdown.Constructor = o),
			(t.fn.dropdown.noConflict = function() {
				return (t.fn.dropdown = r), this;
			}),
			t(document)
				.on('click.bs.dropdown.data-api', n)
				.on('click.bs.dropdown.data-api', '.dropdown form', function(t) {
					t.stopPropagation();
				})
				.on('click.bs.dropdown.data-api', i, o.prototype.toggle)
				.on('keydown.bs.dropdown.data-api', i, o.prototype.keydown)
				.on('keydown.bs.dropdown.data-api', '.dropdown-menu', o.prototype.keydown);
	})(jQuery),
	(function(t) {
		'use strict';
		function e(e, i) {
			return this.each(function() {
				var o = t(this),
					r = o.data('bs.modal'),
					s = t.extend({}, n.DEFAULTS, o.data(), 'object' == typeof e && e);
				r || o.data('bs.modal', (r = new n(this, s))), 'string' == typeof e ? r[e](i) : s.show && r.show(i);
			});
		}
		var n = function(e, n) {
			(this.options = n),
				(this.$body = t(document.body)),
				(this.$element = t(e)),
				(this.$dialog = this.$element.find('.modal-dialog')),
				(this.$backdrop = null),
				(this.isShown = null),
				(this.originalBodyPad = null),
				(this.scrollbarWidth = 0),
				(this.ignoreBackdropClick = !1),
				this.options.remote &&
					this.$element.find('.modal-content').load(
						this.options.remote,
						t.proxy(function() {
							this.$element.trigger('loaded.bs.modal');
						}, this)
					);
		};
		(n.VERSION = '3.3.6'),
			(n.TRANSITION_DURATION = 300),
			(n.BACKDROP_TRANSITION_DURATION = 150),
			(n.DEFAULTS = { backdrop: !0, keyboard: !0, show: !0 }),
			(n.prototype.toggle = function(t) {
				return this.isShown ? this.hide() : this.show(t);
			}),
			(n.prototype.show = function(e) {
				var i = this,
					o = t.Event('show.bs.modal', { relatedTarget: e });
				this.$element.trigger(o),
					this.isShown ||
						o.isDefaultPrevented() ||
						((this.isShown = !0),
						this.checkScrollbar(),
						this.setScrollbar(),
						this.$body.addClass('modal-open'),
						this.escape(),
						this.resize(),
						this.$element.on('click.dismiss.bs.modal', '[data-dismiss="modal"]', t.proxy(this.hide, this)),
						this.$dialog.on('mousedown.dismiss.bs.modal', function() {
							i.$element.one('mouseup.dismiss.bs.modal', function(e) {
								t(e.target).is(i.$element) && (i.ignoreBackdropClick = !0);
							});
						}),
						this.backdrop(function() {
							var o = t.support.transition && i.$element.hasClass('fade');
							i.$element.parent().length || i.$element.appendTo(i.$body),
								i.$element.show().scrollTop(0),
								i.adjustDialog(),
								o && i.$element[0].offsetWidth,
								i.$element.addClass('in'),
								i.enforceFocus();
							var r = t.Event('shown.bs.modal', { relatedTarget: e });
							o
								? i.$dialog
										.one('bsTransitionEnd', function() {
											i.$element.trigger('focus').trigger(r);
										})
										.emulateTransitionEnd(n.TRANSITION_DURATION)
								: i.$element.trigger('focus').trigger(r);
						}));
			}),
			(n.prototype.hide = function(e) {
				e && e.preventDefault(),
					(e = t.Event('hide.bs.modal')),
					this.$element.trigger(e),
					this.isShown &&
						!e.isDefaultPrevented() &&
						((this.isShown = !1),
						this.escape(),
						this.resize(),
						t(document).off('focusin.bs.modal'),
						this.$element.removeClass('in').off('click.dismiss.bs.modal').off('mouseup.dismiss.bs.modal'),
						this.$dialog.off('mousedown.dismiss.bs.modal'),
						t.support.transition && this.$element.hasClass('fade')
							? this.$element
									.one('bsTransitionEnd', t.proxy(this.hideModal, this))
									.emulateTransitionEnd(n.TRANSITION_DURATION)
							: this.hideModal());
			}),
			(n.prototype.enforceFocus = function() {
				t(document).off('focusin.bs.modal').on(
					'focusin.bs.modal',
					t.proxy(function(t) {
						this.$element[0] === t.target ||
							this.$element.has(t.target).length ||
							this.$element.trigger('focus');
					}, this)
				);
			}),
			(n.prototype.escape = function() {
				this.isShown && this.options.keyboard
					? this.$element.on(
							'keydown.dismiss.bs.modal',
							t.proxy(function(t) {
								27 == t.which && this.hide();
							}, this)
						)
					: this.isShown || this.$element.off('keydown.dismiss.bs.modal');
			}),
			(n.prototype.resize = function() {
				this.isShown
					? t(window).on('resize.bs.modal', t.proxy(this.handleUpdate, this))
					: t(window).off('resize.bs.modal');
			}),
			(n.prototype.hideModal = function() {
				var t = this;
				this.$element.hide(),
					this.backdrop(function() {
						t.$body.removeClass('modal-open'),
							t.resetAdjustments(),
							t.resetScrollbar(),
							t.$element.trigger('hidden.bs.modal');
					});
			}),
			(n.prototype.removeBackdrop = function() {
				this.$backdrop && this.$backdrop.remove(), (this.$backdrop = null);
			}),
			(n.prototype.backdrop = function(e) {
				var i = this,
					o = this.$element.hasClass('fade') ? 'fade' : '';
				if (this.isShown && this.options.backdrop) {
					var r = t.support.transition && o;
					if (
						((this.$backdrop = t(document.createElement('div'))
							.addClass('modal-backdrop ' + o)
							.appendTo(this.$body)),
						this.$element.on(
							'click.dismiss.bs.modal',
							t.proxy(function(t) {
								return this.ignoreBackdropClick
									? void (this.ignoreBackdropClick = !1)
									: void (
											t.target === t.currentTarget &&
											('static' == this.options.backdrop ? this.$element[0].focus() : this.hide())
										);
							}, this)
						),
						r && this.$backdrop[0].offsetWidth,
						this.$backdrop.addClass('in'),
						!e)
					)
						return;
					r
						? this.$backdrop.one('bsTransitionEnd', e).emulateTransitionEnd(n.BACKDROP_TRANSITION_DURATION)
						: e();
				} else if (!this.isShown && this.$backdrop) {
					this.$backdrop.removeClass('in');
					var s = function() {
						i.removeBackdrop(), e && e();
					};
					t.support.transition && this.$element.hasClass('fade')
						? this.$backdrop.one('bsTransitionEnd', s).emulateTransitionEnd(n.BACKDROP_TRANSITION_DURATION)
						: s();
				} else e && e();
			}),
			(n.prototype.handleUpdate = function() {
				this.adjustDialog();
			}),
			(n.prototype.adjustDialog = function() {
				var t = this.$element[0].scrollHeight > document.documentElement.clientHeight;
				this.$element.css({
					paddingLeft: !this.bodyIsOverflowing && t ? this.scrollbarWidth : '',
					paddingRight: this.bodyIsOverflowing && !t ? this.scrollbarWidth : ''
				});
			}),
			(n.prototype.resetAdjustments = function() {
				this.$element.css({ paddingLeft: '', paddingRight: '' });
			}),
			(n.prototype.checkScrollbar = function() {
				var t = window.innerWidth;
				if (!t) {
					var e = document.documentElement.getBoundingClientRect();
					t = e.right - Math.abs(e.left);
				}
				(this.bodyIsOverflowing = document.body.clientWidth < t),
					(this.scrollbarWidth = this.measureScrollbar());
			}),
			(n.prototype.setScrollbar = function() {
				var t = parseInt(this.$body.css('padding-right') || 0, 10);
				(this.originalBodyPad = document.body.style.paddingRight || ''),
					this.bodyIsOverflowing && this.$body.css('padding-right', t + this.scrollbarWidth);
			}),
			(n.prototype.resetScrollbar = function() {
				this.$body.css('padding-right', this.originalBodyPad);
			}),
			(n.prototype.measureScrollbar = function() {
				var t = document.createElement('div');
				(t.className = 'modal-scrollbar-measure'), this.$body.append(t);
				var e = t.offsetWidth - t.clientWidth;
				return this.$body[0].removeChild(t), e;
			});
		var i = t.fn.modal;
		(t.fn.modal = e),
			(t.fn.modal.Constructor = n),
			(t.fn.modal.noConflict = function() {
				return (t.fn.modal = i), this;
			}),
			t(document).on('click.bs.modal.data-api', '[data-toggle="modal"]', function(n) {
				var i = t(this),
					o = i.attr('href'),
					r = t(i.attr('data-target') || (o && o.replace(/.*(?=#[^\s]+$)/, ''))),
					s = r.data('bs.modal') ? 'toggle' : t.extend({ remote: !/#/.test(o) && o }, r.data(), i.data());
				i.is('a') && n.preventDefault(),
					r.one('show.bs.modal', function(t) {
						t.isDefaultPrevented() ||
							r.one('hidden.bs.modal', function() {
								i.is(':visible') && i.trigger('focus');
							});
					}),
					e.call(r, s, this);
			});
	})(jQuery),
	(function(t) {
		'use strict';
		var e = function(t, e) {
			(this.type = null),
				(this.options = null),
				(this.enabled = null),
				(this.timeout = null),
				(this.hoverState = null),
				(this.$element = null),
				(this.inState = null),
				this.init('tooltip', t, e);
		};
		(e.VERSION = '3.3.6'),
			(e.TRANSITION_DURATION = 150),
			(e.DEFAULTS = {
				animation: !0,
				placement: 'top',
				selector: !1,
				template:
					'<div class="tooltip" role="tooltip"><div class="tooltip-arrow"></div><div class="tooltip-inner"></div></div>',
				trigger: 'hover focus',
				title: '',
				delay: 0,
				html: !1,
				container: !1,
				viewport: { selector: 'body', padding: 0 }
			}),
			(e.prototype.init = function(e, n, i) {
				if (
					((this.enabled = !0),
					(this.type = e),
					(this.$element = t(n)),
					(this.options = this.getOptions(i)),
					(this.$viewport =
						this.options.viewport &&
						t(
							t.isFunction(this.options.viewport)
								? this.options.viewport.call(this, this.$element)
								: this.options.viewport.selector || this.options.viewport
						)),
					(this.inState = { click: !1, hover: !1, focus: !1 }),
					this.$element[0] instanceof document.constructor && !this.options.selector)
				)
					throw new Error(
						'`selector` option must be specified when initializing ' +
							this.type +
							' on the window.document object!'
					);
				for (var o = this.options.trigger.split(' '), r = o.length; r--; ) {
					var s = o[r];
					if ('click' == s)
						this.$element.on('click.' + this.type, this.options.selector, t.proxy(this.toggle, this));
					else if ('manual' != s) {
						var a = 'hover' == s ? 'mouseenter' : 'focusin',
							l = 'hover' == s ? 'mouseleave' : 'focusout';
						this.$element.on(a + '.' + this.type, this.options.selector, t.proxy(this.enter, this)),
							this.$element.on(l + '.' + this.type, this.options.selector, t.proxy(this.leave, this));
					}
				}
				this.options.selector
					? (this._options = t.extend({}, this.options, { trigger: 'manual', selector: '' }))
					: this.fixTitle();
			}),
			(e.prototype.getDefaults = function() {
				return e.DEFAULTS;
			}),
			(e.prototype.getOptions = function(e) {
				return (
					(e = t.extend({}, this.getDefaults(), this.$element.data(), e)).delay &&
						'number' == typeof e.delay &&
						(e.delay = { show: e.delay, hide: e.delay }),
					e
				);
			}),
			(e.prototype.getDelegateOptions = function() {
				var e = {},
					n = this.getDefaults();
				return (
					this._options &&
						t.each(this._options, function(t, i) {
							n[t] != i && (e[t] = i);
						}),
					e
				);
			}),
			(e.prototype.enter = function(e) {
				var n = e instanceof this.constructor ? e : t(e.currentTarget).data('bs.' + this.type);
				return (
					n ||
						((n = new this.constructor(e.currentTarget, this.getDelegateOptions())),
						t(e.currentTarget).data('bs.' + this.type, n)),
					e instanceof t.Event && (n.inState['focusin' == e.type ? 'focus' : 'hover'] = !0),
					n.tip().hasClass('in') || 'in' == n.hoverState
						? void (n.hoverState = 'in')
						: (clearTimeout(n.timeout),
							(n.hoverState = 'in'),
							n.options.delay && n.options.delay.show
								? void (n.timeout = setTimeout(function() {
										'in' == n.hoverState && n.show();
									}, n.options.delay.show))
								: n.show())
				);
			}),
			(e.prototype.isInStateTrue = function() {
				for (var t in this.inState) if (this.inState[t]) return !0;
				return !1;
			}),
			(e.prototype.leave = function(e) {
				var n = e instanceof this.constructor ? e : t(e.currentTarget).data('bs.' + this.type);
				return (
					n ||
						((n = new this.constructor(e.currentTarget, this.getDelegateOptions())),
						t(e.currentTarget).data('bs.' + this.type, n)),
					e instanceof t.Event && (n.inState['focusout' == e.type ? 'focus' : 'hover'] = !1),
					n.isInStateTrue()
						? void 0
						: (clearTimeout(n.timeout),
							(n.hoverState = 'out'),
							n.options.delay && n.options.delay.hide
								? void (n.timeout = setTimeout(function() {
										'out' == n.hoverState && n.hide();
									}, n.options.delay.hide))
								: n.hide())
				);
			}),
			(e.prototype.show = function() {
				var n = t.Event('show.bs.' + this.type);
				if (this.hasContent() && this.enabled) {
					this.$element.trigger(n);
					var i = t.contains(this.$element[0].ownerDocument.documentElement, this.$element[0]);
					if (n.isDefaultPrevented() || !i) return;
					var o = this,
						r = this.tip(),
						s = this.getUID(this.type);
					this.setContent(),
						r.attr('id', s),
						this.$element.attr('aria-describedby', s),
						this.options.animation && r.addClass('fade');
					var a =
							'function' == typeof this.options.placement
								? this.options.placement.call(this, r[0], this.$element[0])
								: this.options.placement,
						l = /\s?auto?\s?/i,
						c = l.test(a);
					c && (a = a.replace(l, '') || 'top'),
						r.detach().css({ top: 0, left: 0, display: 'block' }).addClass(a).data('bs.' + this.type, this),
						this.options.container ? r.appendTo(this.options.container) : r.insertAfter(this.$element),
						this.$element.trigger('inserted.bs.' + this.type);
					var u = this.getPosition(),
						d = r[0].offsetWidth,
						h = r[0].offsetHeight;
					if (c) {
						var p = a,
							f = this.getPosition(this.$viewport);
						(a =
							'bottom' == a && u.bottom + h > f.bottom
								? 'top'
								: 'top' == a && u.top - h < f.top
									? 'bottom'
									: 'right' == a && u.right + d > f.width
										? 'left'
										: 'left' == a && u.left - d < f.left ? 'right' : a),
							r.removeClass(p).addClass(a);
					}
					var m = this.getCalculatedOffset(a, u, d, h);
					this.applyPlacement(m, a);
					var g = function() {
						var t = o.hoverState;
						o.$element.trigger('shown.bs.' + o.type), (o.hoverState = null), 'out' == t && o.leave(o);
					};
					t.support.transition && this.$tip.hasClass('fade')
						? r.one('bsTransitionEnd', g).emulateTransitionEnd(e.TRANSITION_DURATION)
						: g();
				}
			}),
			(e.prototype.applyPlacement = function(e, n) {
				var i = this.tip(),
					o = i[0].offsetWidth,
					r = i[0].offsetHeight,
					s = parseInt(i.css('margin-top'), 10),
					a = parseInt(i.css('margin-left'), 10);
				isNaN(s) && (s = 0),
					isNaN(a) && (a = 0),
					(e.top += s),
					(e.left += a),
					t.offset.setOffset(
						i[0],
						t.extend(
							{
								using: function(t) {
									i.css({ top: Math.round(t.top), left: Math.round(t.left) });
								}
							},
							e
						),
						0
					),
					i.addClass('in');
				var l = i[0].offsetWidth,
					c = i[0].offsetHeight;
				'top' == n && c != r && (e.top = e.top + r - c);
				var u = this.getViewportAdjustedDelta(n, e, l, c);
				u.left ? (e.left += u.left) : (e.top += u.top);
				var d = /top|bottom/.test(n),
					h = d ? 2 * u.left - o + l : 2 * u.top - r + c,
					p = d ? 'offsetWidth' : 'offsetHeight';
				i.offset(e), this.replaceArrow(h, i[0][p], d);
			}),
			(e.prototype.replaceArrow = function(t, e, n) {
				this.arrow().css(n ? 'left' : 'top', 50 * (1 - t / e) + '%').css(n ? 'top' : 'left', '');
			}),
			(e.prototype.setContent = function() {
				var t = this.tip(),
					e = this.getTitle();
				t.find('.tooltip-inner')[this.options.html ? 'html' : 'text'](e),
					t.removeClass('fade in top bottom left right');
			}),
			(e.prototype.hide = function(n) {
				function i() {
					'in' != o.hoverState && r.detach(),
						o.$element.removeAttr('aria-describedby').trigger('hidden.bs.' + o.type),
						n && n();
				}
				var o = this,
					r = t(this.$tip),
					s = t.Event('hide.bs.' + this.type);
				return (
					this.$element.trigger(s),
					s.isDefaultPrevented()
						? void 0
						: (r.removeClass('in'),
							t.support.transition && r.hasClass('fade')
								? r.one('bsTransitionEnd', i).emulateTransitionEnd(e.TRANSITION_DURATION)
								: i(),
							(this.hoverState = null),
							this)
				);
			}),
			(e.prototype.fixTitle = function() {
				var t = this.$element;
				(t.attr('title') || 'string' != typeof t.attr('data-original-title')) &&
					t.attr('data-original-title', t.attr('title') || '').attr('title', '');
			}),
			(e.prototype.hasContent = function() {
				return this.getTitle();
			}),
			(e.prototype.getPosition = function(e) {
				var n = (e = e || this.$element)[0],
					i = 'BODY' == n.tagName,
					o = n.getBoundingClientRect();
				null == o.width && (o = t.extend({}, o, { width: o.right - o.left, height: o.bottom - o.top }));
				var r = i ? { top: 0, left: 0 } : e.offset(),
					s = { scroll: i ? document.documentElement.scrollTop || document.body.scrollTop : e.scrollTop() },
					a = i ? { width: t(window).width(), height: t(window).height() } : null;
				return t.extend({}, o, s, a, r);
			}),
			(e.prototype.getCalculatedOffset = function(t, e, n, i) {
				return 'bottom' == t
					? { top: e.top + e.height, left: e.left + e.width / 2 - n / 2 }
					: 'top' == t
						? { top: e.top - i, left: e.left + e.width / 2 - n / 2 }
						: 'left' == t
							? { top: e.top + e.height / 2 - i / 2, left: e.left - n }
							: { top: e.top + e.height / 2 - i / 2, left: e.left + e.width };
			}),
			(e.prototype.getViewportAdjustedDelta = function(t, e, n, i) {
				var o = { top: 0, left: 0 };
				if (!this.$viewport) return o;
				var r = (this.options.viewport && this.options.viewport.padding) || 0,
					s = this.getPosition(this.$viewport);
				if (/right|left/.test(t)) {
					var a = e.top - r - s.scroll,
						l = e.top + r - s.scroll + i;
					a < s.top ? (o.top = s.top - a) : l > s.top + s.height && (o.top = s.top + s.height - l);
				} else {
					var c = e.left - r,
						u = e.left + r + n;
					c < s.left ? (o.left = s.left - c) : u > s.right && (o.left = s.left + s.width - u);
				}
				return o;
			}),
			(e.prototype.getTitle = function() {
				var t = this.$element,
					e = this.options;
				return t.attr('data-original-title') || ('function' == typeof e.title ? e.title.call(t[0]) : e.title);
			}),
			(e.prototype.getUID = function(t) {
				for (; (t += ~~(1e6 * Math.random())), document.getElementById(t); );
				return t;
			}),
			(e.prototype.tip = function() {
				if (!this.$tip && ((this.$tip = t(this.options.template)), 1 != this.$tip.length))
					throw new Error(this.type + ' `template` option must consist of exactly 1 top-level element!');
				return this.$tip;
			}),
			(e.prototype.arrow = function() {
				return (this.$arrow = this.$arrow || this.tip().find('.tooltip-arrow'));
			}),
			(e.prototype.enable = function() {
				this.enabled = !0;
			}),
			(e.prototype.disable = function() {
				this.enabled = !1;
			}),
			(e.prototype.toggleEnabled = function() {
				this.enabled = !this.enabled;
			}),
			(e.prototype.toggle = function(e) {
				var n = this;
				e &&
					((n = t(e.currentTarget).data('bs.' + this.type)) ||
						((n = new this.constructor(e.currentTarget, this.getDelegateOptions())),
						t(e.currentTarget).data('bs.' + this.type, n))),
					e
						? ((n.inState.click = !n.inState.click), n.isInStateTrue() ? n.enter(n) : n.leave(n))
						: n.tip().hasClass('in') ? n.leave(n) : n.enter(n);
			}),
			(e.prototype.destroy = function() {
				var t = this;
				clearTimeout(this.timeout),
					this.hide(function() {
						t.$element.off('.' + t.type).removeData('bs.' + t.type),
							t.$tip && t.$tip.detach(),
							(t.$tip = null),
							(t.$arrow = null),
							(t.$viewport = null);
					});
			});
		var n = t.fn.tooltip;
		(t.fn.tooltip = function(n) {
			return this.each(function() {
				var i = t(this),
					o = i.data('bs.tooltip'),
					r = 'object' == typeof n && n;
				(o || !/destroy|hide/.test(n)) &&
					(o || i.data('bs.tooltip', (o = new e(this, r))), 'string' == typeof n && o[n]());
			});
		}),
			(t.fn.tooltip.Constructor = e),
			(t.fn.tooltip.noConflict = function() {
				return (t.fn.tooltip = n), this;
			});
	})(jQuery),
	(function(t) {
		'use strict';
		var e = function(t, e) {
			this.init('popover', t, e);
		};
		if (!t.fn.tooltip) throw new Error('Popover requires tooltip.js');
		(e.VERSION = '3.3.6'),
			(e.DEFAULTS = t.extend({}, t.fn.tooltip.Constructor.DEFAULTS, {
				placement: 'right',
				trigger: 'click',
				content: '',
				template:
					'<div class="popover" role="tooltip"><div class="arrow"></div><h3 class="popover-title"></h3><div class="popover-content"></div></div>'
			})),
			(((e.prototype = t.extend(
				{},
				t.fn.tooltip.Constructor.prototype
			)).constructor = e).prototype.getDefaults = function() {
				return e.DEFAULTS;
			}),
			(e.prototype.setContent = function() {
				var t = this.tip(),
					e = this.getTitle(),
					n = this.getContent();
				t.find('.popover-title')[this.options.html ? 'html' : 'text'](e),
					t
						.find('.popover-content')
						.children()
						.detach()
						.end()
						[this.options.html ? ('string' == typeof n ? 'html' : 'append') : 'text'](n),
					t.removeClass('fade top bottom left right in'),
					t.find('.popover-title').html() || t.find('.popover-title').hide();
			}),
			(e.prototype.hasContent = function() {
				return this.getTitle() || this.getContent();
			}),
			(e.prototype.getContent = function() {
				var t = this.$element,
					e = this.options;
				return t.attr('data-content') || ('function' == typeof e.content ? e.content.call(t[0]) : e.content);
			}),
			(e.prototype.arrow = function() {
				return (this.$arrow = this.$arrow || this.tip().find('.arrow'));
			});
		var n = t.fn.popover;
		(t.fn.popover = function(n) {
			return this.each(function() {
				var i = t(this),
					o = i.data('bs.popover'),
					r = 'object' == typeof n && n;
				(o || !/destroy|hide/.test(n)) &&
					(o || i.data('bs.popover', (o = new e(this, r))), 'string' == typeof n && o[n]());
			});
		}),
			(t.fn.popover.Constructor = e),
			(t.fn.popover.noConflict = function() {
				return (t.fn.popover = n), this;
			});
	})(jQuery),
	(function(t) {
		'use strict';
		function e(n, i) {
			(this.$body = t(document.body)),
				(this.$scrollElement = t(t(n).is(document.body) ? window : n)),
				(this.options = t.extend({}, e.DEFAULTS, i)),
				(this.selector = (this.options.target || '') + ' .nav li > a'),
				(this.offsets = []),
				(this.targets = []),
				(this.activeTarget = null),
				(this.scrollHeight = 0),
				this.$scrollElement.on('scroll.bs.scrollspy', t.proxy(this.process, this)),
				this.refresh(),
				this.process();
		}
		function n(n) {
			return this.each(function() {
				var i = t(this),
					o = i.data('bs.scrollspy'),
					r = 'object' == typeof n && n;
				o || i.data('bs.scrollspy', (o = new e(this, r))), 'string' == typeof n && o[n]();
			});
		}
		(e.VERSION = '3.3.6'),
			(e.DEFAULTS = { offset: 10 }),
			(e.prototype.getScrollHeight = function() {
				return (
					this.$scrollElement[0].scrollHeight ||
					Math.max(this.$body[0].scrollHeight, document.documentElement.scrollHeight)
				);
			}),
			(e.prototype.refresh = function() {
				var e = this,
					n = 'offset',
					i = 0;
				(this.offsets = []),
					(this.targets = []),
					(this.scrollHeight = this.getScrollHeight()),
					t.isWindow(this.$scrollElement[0]) || ((n = 'position'), (i = this.$scrollElement.scrollTop())),
					this.$body
						.find(this.selector)
						.map(function() {
							var e = t(this),
								o = e.data('target') || e.attr('href'),
								r = /^#./.test(o) && t(o);
							return (r && r.length && r.is(':visible') && [ [ r[n]().top + i, o ] ]) || null;
						})
						.sort(function(t, e) {
							return t[0] - e[0];
						})
						.each(function() {
							e.offsets.push(this[0]), e.targets.push(this[1]);
						});
			}),
			(e.prototype.process = function() {
				var t,
					e = this.$scrollElement.scrollTop() + this.options.offset,
					n = this.getScrollHeight(),
					i = this.options.offset + n - this.$scrollElement.height(),
					o = this.offsets,
					r = this.targets,
					s = this.activeTarget;
				if ((this.scrollHeight != n && this.refresh(), i <= e))
					return s != (t = r[r.length - 1]) && this.activate(t);
				if (s && e < o[0]) return (this.activeTarget = null), this.clear();
				for (t = o.length; t--; )
					s != r[t] && e >= o[t] && (void 0 === o[t + 1] || e < o[t + 1]) && this.activate(r[t]);
			}),
			(e.prototype.activate = function(e) {
				(this.activeTarget = e), this.clear();
				var n = this.selector + '[data-target="' + e + '"],' + this.selector + '[href="' + e + '"]',
					i = t(n).parents('li').addClass('active');
				i.parent('.dropdown-menu').length && (i = i.closest('li.dropdown').addClass('active')),
					i.trigger('activate.bs.scrollspy');
			}),
			(e.prototype.clear = function() {
				t(this.selector).parentsUntil(this.options.target, '.active').removeClass('active');
			});
		var i = t.fn.scrollspy;
		(t.fn.scrollspy = n),
			(t.fn.scrollspy.Constructor = e),
			(t.fn.scrollspy.noConflict = function() {
				return (t.fn.scrollspy = i), this;
			}),
			t(window).on('load.bs.scrollspy.data-api', function() {
				t('[data-spy="scroll"]').each(function() {
					var e = t(this);
					n.call(e, e.data());
				});
			});
	})(jQuery),
	(function(t) {
		'use strict';
		function e(e) {
			return this.each(function() {
				var i = t(this),
					o = i.data('bs.tab');
				o || i.data('bs.tab', (o = new n(this))), 'string' == typeof e && o[e]();
			});
		}
		var n = function(e) {
			this.element = t(e);
		};
		(n.VERSION = '3.3.6'),
			(n.TRANSITION_DURATION = 150),
			(n.prototype.show = function() {
				var e = this.element,
					n = e.closest('ul:not(.dropdown-menu)'),
					i = e.data('target');
				if (
					(i || (i = (i = e.attr('href')) && i.replace(/.*(?=#[^\s]*$)/, '')),
					!e.parent('li').hasClass('active'))
				) {
					var o = n.find('.active:last a'),
						r = t.Event('hide.bs.tab', { relatedTarget: e[0] }),
						s = t.Event('show.bs.tab', { relatedTarget: o[0] });
					if ((o.trigger(r), e.trigger(s), !s.isDefaultPrevented() && !r.isDefaultPrevented())) {
						var a = t(i);
						this.activate(e.closest('li'), n),
							this.activate(a, a.parent(), function() {
								o.trigger({ type: 'hidden.bs.tab', relatedTarget: e[0] }),
									e.trigger({ type: 'shown.bs.tab', relatedTarget: o[0] });
							});
					}
				}
			}),
			(n.prototype.activate = function(e, i, o) {
				function r() {
					s
						.removeClass('active')
						.find('> .dropdown-menu > .active')
						.removeClass('active')
						.end()
						.find('[data-toggle="tab"]')
						.attr('aria-expanded', !1),
						e.addClass('active').find('[data-toggle="tab"]').attr('aria-expanded', !0),
						a ? (e[0].offsetWidth, e.addClass('in')) : e.removeClass('fade'),
						e.parent('.dropdown-menu').length &&
							e
								.closest('li.dropdown')
								.addClass('active')
								.end()
								.find('[data-toggle="tab"]')
								.attr('aria-expanded', !0),
						o && o();
				}
				var s = i.find('> .active'),
					a = o && t.support.transition && ((s.length && s.hasClass('fade')) || !!i.find('> .fade').length);
				s.length && a ? s.one('bsTransitionEnd', r).emulateTransitionEnd(n.TRANSITION_DURATION) : r(),
					s.removeClass('in');
			});
		var i = t.fn.tab;
		(t.fn.tab = e),
			(t.fn.tab.Constructor = n),
			(t.fn.tab.noConflict = function() {
				return (t.fn.tab = i), this;
			});
		var o = function(n) {
			n.preventDefault(), e.call(t(this), 'show');
		};
		t(document)
			.on('click.bs.tab.data-api', '[data-toggle="tab"]', o)
			.on('click.bs.tab.data-api', '[data-toggle="pill"]', o);
	})(jQuery),
	(function(t) {
		'use strict';
		function e(e) {
			return this.each(function() {
				var i = t(this),
					o = i.data('bs.affix'),
					r = 'object' == typeof e && e;
				o || i.data('bs.affix', (o = new n(this, r))), 'string' == typeof e && o[e]();
			});
		}
		var n = function(e, i) {
			(this.options = t.extend({}, n.DEFAULTS, i)),
				(this.$target = t(this.options.target)
					.on('scroll.bs.affix.data-api', t.proxy(this.checkPosition, this))
					.on('click.bs.affix.data-api', t.proxy(this.checkPositionWithEventLoop, this))),
				(this.$element = t(e)),
				(this.affixed = null),
				(this.unpin = null),
				(this.pinnedOffset = null),
				this.checkPosition();
		};
		(n.VERSION = '3.3.6'),
			(n.RESET = 'affix affix-top affix-bottom'),
			(n.DEFAULTS = { offset: 0, target: window }),
			(n.prototype.getState = function(t, e, n, i) {
				var o = this.$target.scrollTop(),
					r = this.$element.offset(),
					s = this.$target.height();
				if (null != n && 'top' == this.affixed) return o < n && 'top';
				if ('bottom' == this.affixed)
					return null != n ? !(o + this.unpin <= r.top) && 'bottom' : !(o + s <= t - i) && 'bottom';
				var a = null == this.affixed,
					l = a ? o : r.top;
				return null != n && o <= n ? 'top' : null != i && t - i <= l + (a ? s : e) && 'bottom';
			}),
			(n.prototype.getPinnedOffset = function() {
				if (this.pinnedOffset) return this.pinnedOffset;
				this.$element.removeClass(n.RESET).addClass('affix');
				var t = this.$target.scrollTop(),
					e = this.$element.offset();
				return (this.pinnedOffset = e.top - t);
			}),
			(n.prototype.checkPositionWithEventLoop = function() {
				setTimeout(t.proxy(this.checkPosition, this), 1);
			}),
			(n.prototype.checkPosition = function() {
				if (this.$element.is(':visible')) {
					var e = this.$element.height(),
						i = this.options.offset,
						o = i.top,
						r = i.bottom,
						s = Math.max(t(document).height(), t(document.body).height());
					'object' != typeof i && (r = o = i),
						'function' == typeof o && (o = i.top(this.$element)),
						'function' == typeof r && (r = i.bottom(this.$element));
					var a = this.getState(s, e, o, r);
					if (this.affixed != a) {
						null != this.unpin && this.$element.css('top', '');
						var l = 'affix' + (a ? '-' + a : ''),
							c = t.Event(l + '.bs.affix');
						if ((this.$element.trigger(c), c.isDefaultPrevented())) return;
						(this.affixed = a),
							(this.unpin = 'bottom' == a ? this.getPinnedOffset() : null),
							this.$element
								.removeClass(n.RESET)
								.addClass(l)
								.trigger(l.replace('affix', 'affixed') + '.bs.affix');
					}
					'bottom' == a && this.$element.offset({ top: s - e - r });
				}
			});
		var i = t.fn.affix;
		(t.fn.affix = e),
			(t.fn.affix.Constructor = n),
			(t.fn.affix.noConflict = function() {
				return (t.fn.affix = i), this;
			}),
			t(window).on('load', function() {
				t('[data-spy="affix"]').each(function() {
					var n = t(this),
						i = n.data();
					(i.offset = i.offset || {}),
						null != i.offsetBottom && (i.offset.bottom = i.offsetBottom),
						null != i.offsetTop && (i.offset.top = i.offsetTop),
						e.call(n, i);
				});
			});
	})(jQuery),
	$(function() {
		'use strict';
		$('.select').click(function() {
			$(this).addClass('active').sebling.removeClass('active');
		}),
			$('.arrowTopBottomTwo').click(function() {
				$('.hideAndShowTwo').fadeToggle();
			}),
			$('.arrowTopBottomOne').click(function() {
				$('.hideAndShowOne').fadeToggle();
			});
		var t = 0,
			e = 0,
			n = 0,
			i = 0;
		$('.typeLink').click(function() {
			$('.Typeform').slideToggle(),
				0 == t
					? ($('.typeLink img').css('transform', 'rotate(0)'), (t = 1))
					: ($('.typeLink img').css('transform', 'rotate(-90deg)'), (t = 0));
		}),
			$('.Nutritionlink').click(function() {
				$('.Nutritionform').slideToggle(),
					0 == e
						? ($('.Nutritionlink img').css('transform', 'rotate(0)'), (e = 1))
						: ($('.Nutritionlink img').css('transform', 'rotate(-90deg)'), (e = 0));
			}),
			$('.Brandlink').click(function() {
				$('.Brandform').slideToggle(),
					1 == n
						? ($('.Brandlink img').css('transform', 'rotate(0)'), (n = 0))
						: ($('.Brandlink img').css('transform', 'rotate(-90deg)'), (n = 1));
			}),
			$('.Packinglink').click(function() {
				$('.Packingform').slideToggle(),
					0 == i
						? ($('.Packinglink img').css('transform', 'rotate(0)'), (i = 1))
						: ($('.Packinglink img').css('transform', 'rotate(-90deg)'), (i = 0));
			}),
			$('.filter').height($('.height').height() - 75),
			(function t() {
				$('.testimonials .active').each(function() {
					$(this).is(':last-child')
						? $(this).delay(3e3).fadeOut(1e3, function() {
								$(this).removeClass('active'),
									$('.testimonials .opinion').eq(0).addClass('active').fadeIn(),
									t();
							})
						: $(this).delay(3e3).fadeOut(1e3, function() {
								$(this).removeClass('active').next().addClass('active').fadeIn(), t();
							});
				});
			})();
	}),
	$(document).ready(function() {
		$('#live-search-dropdown').on('click', function(t) {
			if ('I' == t.target.nodeName) {
				var e = { product_id: t.target.parentElement.id, quantity: 1 };
				$.ajax({
					url: 'index.php?route=bossthemes/boss_add/cart/',
					method: 'POST',
					data: e,
					success: showCartnotification
				});
			} else if ('BUTTON' == t.target.nodeName) {
				var n = t.target.id.split(',');
				(e = { product_id: n[0], quantity: 1, seller_id: n[1] }),
					$.ajax({
						url: 'index.php?route=bossthemes/boss_add/cart/',
						method: 'POST',
						data: e,
						success: showCartnotification
					});
			}
		}),
			$('#search').on({
				mouseenter: function() {
					$('#search-live').show(), $('#search-live').val(), $('#live-search-dropdown').hide();
				},
				mouseleave: function() {
					'' == $('#search-live').val() && ($('#search-live').show(), $('#live-search-dropdown').hide());
				}
			}),
			$('#search-live').on('input', function() {
				var t = $('#search-live').val(),
					e = $('#seller_id').val();
				'' != t
					? $.ajax({
							dataType: 'json',
							url: 'index.php?route=product/live_search&filter_name=' + t + '&seller_id=' + e,
							method: 'GET',
							success: showResult
						})
					: $('#live-search-dropdown ul').empty();
			});
	});
var ssc_activeElement,
	ssc_framerate = 150,
	ssc_animtime = 500,
	ssc_stepsize = 150,
	ssc_pulseAlgorithm = !0,
	ssc_pulseScale = 6,
	ssc_pulseNormalize = 1,
	ssc_keyboardsupport = !0,
	ssc_arrowscroll = 50,
	ssc_frame = !1,
	ssc_direction = { x: 0, y: 0 },
	ssc_initdone = !1,
	ssc_fixedback = !0,
	ssc_root = document.documentElement,
	ssc_key = { left: 37, up: 38, right: 39, down: 40, spacebar: 32, pageup: 33, pagedown: 34, end: 35, home: 36 },
	ssc_que = [],
	ssc_pending = !1,
	ssc_cache = {};
setInterval(function() {
	ssc_cache = {};
}, 1e4);
var ssc_uniqueID = (function() {
		var t = 0;
		return function(e) {
			return e.ssc_uniqueID || (e.ssc_uniqueID = t++);
		};
	})(),
	ischrome = /chrome/.test(navigator.userAgent.toLowerCase());
ischrome &&
	(ssc_addEvent('mousedown', ssc_mousedown), ssc_addEvent('mousewheel', ssc_wheel), ssc_addEvent('load', ssc_init));
var touch = !1;
function home_parallax_sroll(t) {
	jQuery(t).each(function(t, e) {
		var n;
		(n = jQuery(window).scrollTop() - jQuery(e).offset().top),
			jQuery(e).css({ 'background-position': '50%' + n + 'px' });
	});
}
function home_parallax(t) {
	home_parallax_sroll(t),
		jQuery(window).scroll(function() {
			home_parallax_sroll(t);
		});
}
function dataAnimate() {
	$('[data-animate]').each(function() {
		var t,
			e = $(this),
			n = $(this).attr('data-delay');
		if (((t = n ? Number(n) : 200), !e.hasClass('animated'))) {
			e.addClass('not-animated');
			var i = e.attr('data-animate');
			e.appear(
				function() {
					setTimeout(function() {
						e.removeClass('not-animated').addClass(i + ' animated');
					}, t);
				},
				{ accX: 0, accY: -80 },
				'easeInCubic'
			);
		}
	});
}
jQuery(document).ready(function(t) {
	home_parallax('.bt-video .bt_video_banner'),
		t('#b_parallax').parallax('50%', 0),
		t('.selectpicker').selectpicker(),
		t('.currency .currency-select').on('click', function(e) {
			e.preventDefault(),
				t(".currency input[name='code']").attr('value', t(this).attr('name')),
				t('.currency').submit();
		}),
		t('.language a').on('click', function(e) {
			e.preventDefault(),
				t(".language input[name='code']").attr('value', t(this).attr('href')),
				t('.language').submit();
		}),
		t('#menu1 .dropdown-menu').each(function() {
			var e = t('#menu1').offset(),
				n = t(this).parent().offset().left + t(this).outerWidth() - (e.left + t('#menu1').outerWidth());
			0 < n && t(this).css('margin-left', '-' + (n + 5) + 'px');
		}),
		t('#menu2 .dropdown-menu').each(function() {
			var e = t('#menu2').offset(),
				n = t(this).parent().offset().left + t(this).outerWidth() - (e.left + t('#menu2').outerWidth());
			0 < n && t(this).css('margin-left', '-' + (n + 5) + 'px');
		}),
		t("#search input[name='search']").parent().find('button').on('click', function() {
			url = t('base').attr('href') + 'index.php?route=product/search';
			var e = t("#search input[name='search']").val(),
				n = t("#search input[name='seller_id']").val();
			e && (url += '&search=' + encodeURIComponent(e)),
				n && (url += '&seller_id=' + encodeURIComponent(n)),
				(location = url);
		}),
		t("#search input[name='search']").on('keydown', function(e) {
			13 == e.keyCode && t("#search input[name='search']").parent().find('button').trigger('click');
		});
}),
	jQuery(document).ready(function(t) {
		(t.support.touch = 'ontouchend' in document),
			t.support.touch ? ((touch = !0), t('body').addClass('touch')) : t('body').addClass('notouch'),
			t('#list-view').click(function() {
				t('#list-view').addClass('active'), t('#grid-view').removeClass('active');
			}),
			t('#grid-view').click(function() {
				t('#grid-view').addClass('active'), t('#list-view').removeClass('active');
			}),
			'list' == localStorage.getItem('display')
				? t('#list-view').trigger('click')
				: t('#grid-view').trigger('click'),
			t('.open-panel,.close-panel').click(function() {
				t('body').toggleClass('openNav');
			}),
			t('.nav-pills li.parent > p').click(function() {
				'+' == t(this).text()
					? (t(this).parent('li').children('.dropdown').slideDown(300), t(this).text('-'))
					: (t(this).parent('li').children('.dropdown').slideUp(300), t(this).text('+'));
			});
	}),
	(function(t, e) {
		jQuery.fn[e] = function(t) {
			return t
				? this.bind(
						'resize',
						((n = t),
						function() {
							var t = this,
								e = arguments;
							i && clearTimeout(i),
								(i = setTimeout(function() {
									n.apply(t, e), (i = null);
								}, 100));
						})
					)
				: this.trigger(e);
			var n, i;
		};
	})(jQuery, 'smartresize');
var TO = !1;
function handleMenu() {
	var t = $(this).scrollTop(),
		e = $('#header').outerHeight(),
		n = $('#bt_menu').outerHeight(),
		i = e + n;
	1024 < getWidthBrowser() &&
		(i < t
			? $('#bt_menu').hasClass('show') ||
				($('<div style="min-height:' + n + 'px"></div>').insertBefore('#bt_menu'),
				$('#bt_menu').addClass('show').addClass('fadeInDown animated'))
			: $('#bt_menu').hasClass('show') &&
				($('#bt_menu').prev().remove(), $('#bt_menu').removeClass('show').removeClass('fadeInDown animated')));
}
function resizeWidth() {
	var t = $('.bt-content-menu').outerWidth();
	$('.mega-menu ul > li.parent > div.dropdown').each(function(e, n) {
		var o = $('.bt-content-menu').offset(),
			r = $(this).parent().offset();
		(i = r.left + $(this).outerWidth() - (o.left + t)),
			0 < i ? $(this).css('margin-left', '-' + i + 'px') : $(this).css('margin-left', '0px');
	});
}
$(window).smartresize(function() {
	!1 !== TO && clearTimeout(TO), (TO = setTimeout(resizeWidth, 400));
}),
	$(document).ready(function() {
		resizeWidth();
	}),
	($.fn.bttabs = function() {
		var t = this;
		this.each(function() {
			var e = $(this);
			$(e.attr('href')).hide(),
				e.click(function() {
					$(t).removeClass('selected'), $(this).addClass('selected'), $($(this).attr('href')).fadeIn();
					var e = $(this).attr('data-crs');
					return (
						loadslider(e),
						$(t).not(this).each(function(t, e) {
							$($(e).attr('href')).hide();
						}),
						!1
					);
				});
		}),
			$(this).show(),
			$(this).first().click();
	});
var btadd = {
	cart: function(t, e, n) {
		// console.log(t);
		$.ajax({
			url: 'index.php?route=bossthemes/boss_add/cart/',
			type: 'post',
			data: 'product_id=' + t + '&quantity=' + (void 0 !== e ? e : '1&seller_id=' + n),
			dataType: 'json',
			success: function(n) {
				if ((n.redirect && (location = n.redirect), n.success)) {
					addProductNotice(n.title, n.thumb, n.success + '<br><br>' + n.msg, 'success'),
						$('.b-cart-total').html(n.total),
						$('.b-cart > ul').load('index.php?route=common/cart/info ul li');
					var i = n.cartId,
						o =
							'<button onclick="changeQty(\'select-number' +
							i +
							'\',0); return false;" class="decrease"><i class="fa fa-minus"></i></button>';
					(o +=
						'<input id="select-number' +
						i +
						'" type="text" name="quantity[' +
						i +
						']" value="' +
						e +
						'" size="1" class="form-control" style="display:-webkit-inline-box;width: 40px;height: 30px;text-align: center" onchange="update_cart_data_home(\'' +
						i +
						"', 'select-number" +
						i +
						'\')"/>'),
						(o +=
							'<button onclick="changeQty(\'select-number' +
							i +
							'\',1); return false;" class="increase"><i class="fa fa-plus"></i></button>'),
						$('#pro_div_id_' + t).html(o);
				}
			}
		});
	},
	wishlist: function(t) {
		return alert('here'), !1;
	},
	wishlist_cat: function(t) {
		alert('yes'),
			$.ajax({
				url: 'index.php?route=bossthemes/boss_add/wishlist_cat/',
				type: 'post',
				data: t,
				dataType: 'json',
				success: function(t) {
					t.success
						? addProductNotice(t.title, t.thumb, t.success, 'success')
						: addProductNotice(t.title, '', t.info),
						$('.wishlist-total').html(t.total);
				}
			});
	},
	compare: function(t) {
		$.ajax({
			url: 'index.php?route=bossthemes/boss_add/compare',
			type: 'post',
			data: 'product_id=' + t,
			dataType: 'json',
			success: function(t) {
				t.success &&
					(addProductNotice(t.title, t.thumb, t.success, 'success'), $('#compare-total').html(t.total));
			}
		});
	}
};
function addProductNotice(t, e, n, i) {
	$.jGrowl.defaults.closer = !0;
	var o = e + '<h3>' + n + '</h3>';
	$.jGrowl(o, { life: 3e3, header: t, speed: 'slow' });
}
function getURLVar(t) {
	var e = [],
		n = String(document.location).split('?');
	if (n[1]) {
		var o = n[1].split('&');
		for (i = 0; i < o.length; i++) {
			var r = o[i].split('=');
			r[0] && r[1] && (e[r[0]] = r[1]);
		}
		return e[t] ? e[t] : '';
	}
}
var cart = {
		add: function(t, e) {
			$.ajax({
				url: 'index.php?route=checkout/cart/add',
				type: 'post',
				data: 'product_id=' + t + '&quantity=' + (void 0 !== e ? e : 1),
				dataType: 'json',
				beforeSend: function() {
					$('.b-cart > button').button('loading');
				},
				success: function(t) {
					$('.alert, .text-danger').remove(),
						$('.b-cart > button').button('reset'),
						t.redirect && (location = t.redirect),
						t.success &&
							($('#content')
								.parent()
								.before(
									'<div class="alert alert-success"><i class="fa fa-check-circle"></i> ' +
										t.success +
										'<button type="button" class="close" data-dismiss="alert">&times;</button></div>'
								),
							$('.b-cart-total').html(t.total),
							$('html, body').animate({ scrollTop: 0 }, 'slow'),
							$('.b-cart > ul').load('index.php?route=common/cart/info ul li'));
				}
			});
		},
		update: function(t, e) {
			$.ajax({
				url: 'index.php?route=checkout/cart/edit',
				type: 'post',
				data: 'key=' + t + '&quantity=' + (void 0 !== e ? e : 1),
				dataType: 'json',
				beforeSend: function() {
					$('.b-cart > button').button('loading');
				},
				success: function(t) {
					$('.b-cart > button').button('reset'),
						$('.b-cart-total').html(t.total),
						'checkout/cart' == getURLVar('route') || 'checkout/checkout' == getURLVar('route')
							? (location = 'index.php?route=checkout/cart')
							: $('.b-cart > ul').load('index.php?route=common/cart/info ul li');
				}
			});
		},
		remove: function(t) {
			$.ajax({
				url: 'index.php?route=checkout/cart/remove',
				type: 'post',
				data: 'key=' + t,
				dataType: 'json',
				beforeSend: function() {
					$('.b-cart > button').button('loading');
				},
				success: function(t) {
					$('.b-cart > button').button('reset'),
						$('.b-cart-total').html(t.total),
						'checkout/cart' == getURLVar('route') || 'checkout/checkout' == getURLVar('route')
							? (location = 'index.php?route=checkout/cart')
							: $('.b-cart > ul').load('index.php?route=common/cart/info ul li');
				}
			});
		}
	},
	voucher = {
		add: function() {},
		remove: function(t) {
			$.ajax({
				url: 'index.php?route=checkout/cart/remove',
				type: 'post',
				data: 'key=' + t,
				dataType: 'json',
				beforeSend: function() {
					$('.b-cart > button').button('loading');
				},
				complete: function() {
					$('.b-cart > button').button('reset');
				},
				success: function(t) {
					$('.b-cart-total').html(t.total),
						'checkout/cart' == getURLVar('route') || 'checkout/checkout' == getURLVar('route')
							? (location = 'index.php?route=checkout/cart')
							: $('.b-cart > ul').load('index.php?route=common/cart/info ul li');
				}
			});
		}
	},
	wishlist = {
		add: function(t) {
			$.ajax({
				url: 'index.php?route=account/wishlist/add',
				type: 'post',
				data: 'product_id=' + t,
				dataType: 'json',
				success: function(t) {
					$('.alert').remove(),
						t.success &&
							$('#content')
								.parent()
								.before(
									'<div class="alert alert-success"><i class="fa fa-check-circle"></i> ' +
										t.success +
										'<button type="button" class="close" data-dismiss="alert">&times;</button></div>'
								),
						t.info &&
							$('#content')
								.parent()
								.before(
									'<div class="alert alert-info"><i class="fa fa-info-circle"></i> ' +
										t.info +
										'<button type="button" class="close" data-dismiss="alert">&times;</button></div>'
								),
						$('.wishlist-total').html(t.total),
						$('html, body').animate({ scrollTop: 0 }, 'slow');
				}
			});
		},
		remove: function() {}
	},
	compare = {
		add: function(t) {
			$.ajax({
				url: 'index.php?route=product/compare/add',
				type: 'post',
				data: 'product_id=' + t,
				dataType: 'json',
				success: function(t) {
					$('.alert').remove(),
						t.success &&
							($('#content')
								.parent()
								.before(
									'<div class="alert alert-success"><i class="fa fa-check-circle"></i> ' +
										t.success +
										'<button type="button" class="close" data-dismiss="alert">&times;</button></div>'
								),
							$('#compare-total').html(t.total),
							$('html, body').animate({ scrollTop: 0 }, 'slow'));
				}
			});
		},
		remove: function() {}
	};
function getURLVar(t) {
	var e = [],
		n = String(document.location).split('?');
	if (n[1]) {
		var o = n[1].split('&');
		for (i = 0; i < o.length; i++) {
			var r = o[i].split('=');
			r[0] && r[1] && (e[r[0]] = r[1]);
		}
		return e[t] ? e[t] : '';
	}
}
function loacation() {
	location.reload();
}
function showLoginPopup() {
	$('#modal-quicksignIn').appendTo('body'),
		$('#bs-example-navbar-collapse-1').removeClass('in'),
		$('#modal-quicksignIn').modal('show');
}
function changeQty(t, e, n) {
	var i = parseInt($('#' + t).val());
	parseInt(i) <= 1
		? ((i = e ? i + 1 : 0), $('#' + t).val(i))
		: isNaN(i) ? $('#' + t).val(0) : ((i = e ? i + 1 : 1 < i - 1 ? i - 1 : 1), $('#' + t).val(i)),
		update_cart_data_home(t.split('select-number')[1], t, n);
}
function update_cart_data_home(t, e, n) {
	var i = $('#' + e).val();
	if (
		(0 <= i &&
			$.ajax({
				url: 'index.php?route=checkout/cart/update_by_home',
				type: 'post',
				data: { cart_id: t, quantity: i },
				dataType: 'json',
				success: function(t) {
					0 == parseInt(t.total)
						? $('li.retailer-option a').removeAttr('onclick')
						: $('li.retailer-option a').attr(
								'onclick',
								"return confirm('Your cart items will be removed while changing to other store. Are you sure to move to this store?')"
							),
						t.success &&
							(addProductNotices(t.text_update_success, '', t.text_update_cart, 'success'),
							$('.b-cart-total').html(t.total),
							$('.b-cart > ul').load('index.php?route=common/cart/info ul li'));
				}
			}),
		0 == i)
	) {
		var o = $('#' + e).parent().parent().parent().attr('id'),
			r =
				'<a href="javascript:void(0)" id="custom" onclick="btadd.cart(\'' +
				o.split('_')[3] +
				"', '1', " +
				n +
				');"><button>Add to Cart</button></a>';
		$('#' + o).html(r);
	}
}
function addProductNotices(t, e, n, i) {
	$.jGrowl.defaults.closer = !0;
	var o = e + '<h3>' + n + '</h3>';
	$.jGrowl(o, { life: 3e3, header: t, speed: 'slow' });
}
function changeCartQty(t, e) {
	var n = parseInt($('#' + t).val());
	parseInt(n) <= 1
		? ((n = e ? n + 1 : 0), $('#' + t).val(n))
		: isNaN(n) ? $('#' + t).val(0) : ((n = e ? n + 1 : 1 < n - 1 ? n - 1 : 1), $('#' + t).val(n)),
		updateCartQty(t.split('item-number')[1], t);
}
function updateCartQty(t, e) {
	var n = $('#' + e).val();
	0 <= n &&
		$.ajax({
			url: 'index.php?route=checkout/cart/update_by_home',
			type: 'post',
			data: { cart_id: t, quantity: n },
			dataType: 'json',
			success: function(t) {
				t.success &&
					(addProductQtyNotices(t.text_update_success, '', t.text_update_cart, 'success'),
					$('.b-cart-total').html(t.total),
					$('.b-cart > ul').load('index.php?route=common/cart/info ul li'));
			}
		});
}
function addProductQtyNotices(t, e, n, i) {
	$.jGrowl.defaults.closer = !0;
	var o = t + '<h3>' + n + '</h3>';
	$.jGrowl(o, { life: 3e3, header: t, speed: 'slow' });
}
function resendPassword() {
	var t = $('#input-passwordSent').val(),
		e = $('#input-telephone').val();
	$('#modal-quicksignIn .modal-body').html('Please wait.....'),
		$.ajax({
			url: 'index.php?route=account/simpleregister/resendPassword',
			type: 'post',
			data: { mobile: e, userPassword: t },
			dataType: 'json',
			beforeSend: function() {
				$('#quick-register #resendPassword').button('loading'),
					$('#quick-register #verifyPassword').hide(),
					$('#modal-quicksignIn .alert-danger').remove();
			},
			complete: function() {
				$('#quick-register #resendPassword').button('reset');
			},
			success: function(t) {
				if (
					($('#modal-quicksignup .form-group').removeClass('has-error'),
					t.error_warning &&
						$('#modal-quicksignIn .modal-header').after(
							'<div class="alert alert-danger" style="margin:5px;"><i class="fa fa-exclamation-circle"></i> ' +
								t.error_warning +
								'</div>'
						),
					t.success)
				) {
					var e = t.password_sent,
						n = t.mobile_registered;
					$('#modal-quicksignIn .main-heading').html(t.heading_title),
						(success = t.text_resend_password),
						(success += '<div>'),
						(success += '<div class="form-group required">'),
						(success += '<div>'),
						(success +=
							'<input type="hidden" name="telephone" value="' +
							n +
							'"  id="input-telephone" class="form-control" />'),
						(success +=
							'<input type="hidden" name="passwordSent" value="' +
							e +
							'"  id="input-passwordSent" class="form-control" />'),
						(success += '</div>'),
						(success += '</div>'),
						(success += '<div class="form-group required">'),
						(success += '<div>'),
						(success +=
							'<input type="text" name="password" value="" placeholder="' +
							t.text_verify_password +
							'"  id="input-password" class="form-control" />'),
						(success += '</div>'),
						(success += '</div>'),
						(success += '</div>'),
						(success +=
							'<div class="buttons"><div class="text-right"><a onclick="verifyPassword()" id="verifyPassword" class="btn btn-primary">' +
							t.button_verify +
							'</a> <a id="resendPassword" onclick="resendPassword()" class="btn btn-primary">' +
							t.button_resend +
							'</a></div></div>'),
						$('#modal-quicksignIn .modal-body').html(success);
				} else alert('Something went wrong. Please click resend button');
			}
		});
}
function verifyPassword() {
	var t = $('#input-password').val(),
		e = $('#input-passwordSent').val(),
		n = $('#input-telephone').val();
	t == e
		? ($('#modal-quicksignIn .modal-body').html('Please wait verification in progress.....'),
			$.ajax({
				url: 'index.php?route=account/simpleregister/register',
				type: 'post',
				data: { mobile: n, password: t },
				dataType: 'json',
				beforeSend: function() {
					$('#quick-register #verifyPassword').button('loading'),
						$('#quick-register #resendPassword').hide(),
						$('#modal-quicksignIn .alert-danger').remove();
				},
				complete: function() {
					$('#quick-register #verifyPassword').button('reset');
				},
				success: function(t) {
					$('#modal-quicksignup .form-group').removeClass('has-error'),
						t.islogged && (window.location.href = 'index.php?route=account/account'),
						t.error_warning &&
							$('#modal-quicksignIn .modal-header').after(
								'<div class="alert alert-danger" style="margin:5px;"><i class="fa fa-exclamation-circle"></i> ' +
									t.error_warning +
									'</div>'
							),
						t.success &&
							($('#modal-quicksignIn .main-heading').html(t.heading_title),
							(success = t.text_verification_message),
							(success +=
								'<div class="buttons"><div class="text-right"><a onclick="loacation();" class="btn btn-primary">' +
								t.button_continue +
								'</a></div></div>'),
							$('#modal-quicksignIn .modal-body').html(success));
				}
			}))
		: alert('Please enter valid verification code');
}
function wishlist_remove(t, e) {
	$.ajax({
		url: 'index.php?route=account/wishlist/wishlist_product_remove',
		type: 'post',
		data: { product_id: t, seller_id: e },
		dataType: 'json',
		success: function(e) {
			e.success
				? (addProductNotice(e.title, e.thumb, e.success, 'success'),
					$('.wishlist-total').html(e.total),
					$('#heart' + t).attr('class', 'heart-box'),
					$('#heart' + t + ' .heart-label i').css('color', '#000000'),
					$('#heart' + t).css('display', 'block'))
				: (addProductNotice(e.title, '', e.info), $('.wishlist-total').html(e.total));
		}
	});
}
function changeHeart(t, e) {
	var n = "'" + t + "'";
	if ('heart-box-pink' == $('#heart' + t).attr('class')) {
		var i = '<div class="heart-label">';
		(i += '<a class="wishlist1" onclick="wishlist_cat_add(' + t + ',' + e + ')">'),
			(i += '<i class="fa fa-heart" style="color:#000000"></i>'),
			(i += '</a>'),
			(i += '</div>'),
			$('#heart' + t).attr('class', 'heart-box'),
			$('#heart' + t).html(i);
	} else
		(i = '<div class="heart-label">'),
			(i +=
				'<a class="wishlist1" onclick="wishlist_remove(' +
				n +
				',' +
				e +
				'); changeHeart(' +
				n +
				',' +
				e +
				')">'),
			(i += '<i class="fa fa-heart" style="color:#E8114A"></i>'),
			(i += '</a>'),
			(i += '</div>'),
			$('#heart' + t).css('display', 'block'),
			$('#heart' + t).attr('class', 'heart-box-pink'),
			$('#heart' + t).html(i);
}
function wishlist_cat_add(t, e) {
	$('#wishlist_product_id').val(t), $('#wishlist_seller_id').val(e), $('#modal-wishlist_category').modal('show');
}
$(document).delegate('.agree', 'click', function(t) {
	t.preventDefault(), $('#modal-agree').remove();
	var e = this;
	$.ajax({
		url: $(e).attr('href'),
		type: 'get',
		dataType: 'html',
		success: function(t) {
			(html = '<div id="modal-agree" class="modal">'),
				(html += '  <div class="modal-dialog">'),
				(html += '    <div class="modal-content">'),
				(html += '      <div class="modal-header">'),
				(html +=
					'        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>'),
				(html += '        <h4 class="modal-title">' + $(e).text() + '</h4>'),
				(html += '      </div>'),
				(html += '      <div class="modal-body">' + t + '</div>'),
				(html += '    </div'),
				(html += '  </div>'),
				(html += '</div>'),
				$('body').append(html),
				$('#modal-agree').modal('show');
		}
	});
}),
	(function(t) {
		t.fn.autocomplete = function(e) {
			return this.each(function() {
				(this.timer = null),
					(this.items = new Array()),
					t.extend(this, e),
					t(this).attr('autocomplete', 'off'),
					t(this).on('focus', function() {
						this.request();
					}),
					t(this).on('blur', function() {
						setTimeout(
							function(t) {
								t.hide();
							},
							200,
							this
						);
					}),
					t(this).on('keydown', function(t) {
						switch (t.keyCode) {
							case 27:
								this.hide();
								break;
							default:
								this.request();
						}
					}),
					(this.click = function(e) {
						e.preventDefault(),
							(value = t(e.target).parent().attr('data-value')),
							value && this.items[value] && this.select(this.items[value]);
					}),
					(this.show = function() {
						var e = t(this).position();
						t(this).siblings('ul.dropdown-menu').css({ top: e.top + t(this).outerHeight(), left: e.left }),
							t(this).siblings('ul.dropdown-menu').show();
					}),
					(this.hide = function() {
						t(this).siblings('ul.dropdown-menu').hide();
					}),
					(this.request = function() {
						clearTimeout(this.timer),
							(this.timer = setTimeout(
								function(e) {
									e.source(t(e).val(), t.proxy(e.response, e));
								},
								200,
								this
							));
					}),
					(this.response = function(e) {
						if (((html = ''), e.length)) {
							for (i = 0; i < e.length; i++) this.items[e[i].value] = e[i];
							for (i = 0; i < e.length; i++)
								e[i].category ||
									(html +=
										'<li data-value="' + e[i].value + '"><a href="#">' + e[i].label + '</a></li>');
							var n = new Array();
							for (i = 0; i < e.length; i++)
								e[i].category &&
									(n[e[i].category] ||
										((n[e[i].category] = new Array()),
										(n[e[i].category].name = e[i].category),
										(n[e[i].category].item = new Array())),
									n[e[i].category].item.push(e[i]));
							for (i in n)
								for (
									html += '<li class="dropdown-header">' + n[i].name + '</li>', j = 0;
									j < n[i].item.length;
									j++
								)
									html +=
										'<li data-value="' +
										n[i].item[j].value +
										'"><a href="#">&nbsp;&nbsp;&nbsp;' +
										n[i].item[j].label +
										'</a></li>';
						}
						html ? this.show() : this.hide(), t(this).siblings('ul.dropdown-menu').html(html);
					}),
					t(this).after('<ul class="dropdown-menu"></ul>'),
					t(this).siblings('ul.dropdown-menu').delegate('a', 'click', t.proxy(this.click, this));
			});
		};
	})(window.jQuery),
	(function(t) {
		var e = !1 === t.support.boxModel && t.support.objectAll && t.support.leadingWhitespace;
		(t.jGrowl = function(e, n) {
			0 == t('#jGrowl').size() &&
				t('<div id="jGrowl"></div>')
					.addClass(n && n.position ? n.position : t.jGrowl.defaults.position)
					.appendTo('body'),
				t('#jGrowl').jGrowl(e, n);
		}),
			(t.fn.jGrowl = function(e, n) {
				if (t.isFunction(this.each)) {
					var i = arguments;
					return this.each(function() {
						null == t(this).data('jGrowl.instance') &&
							(t(this).data(
								'jGrowl.instance',
								t.extend(new t.fn.jGrowl(), { notifications: [], element: null, interval: null })
							),
							t(this).data('jGrowl.instance').startup(this)),
							t.isFunction(t(this).data('jGrowl.instance')[e])
								? t(this)
										.data('jGrowl.instance')
										[e].apply(t(this).data('jGrowl.instance'), t.makeArray(i).slice(1))
								: t(this).data('jGrowl.instance').create(e, n);
					});
				}
			}),
			t.extend(t.fn.jGrowl.prototype, {
				defaults: {
					pool: 0,
					header: '',
					group: '',
					sticky: !1,
					position: 'top-right',
					glue: 'after',
					theme: 'default',
					themeState: 'highlight',
					corners: '10px',
					check: 250,
					life: 3e3,
					closeDuration: 'normal',
					openDuration: 'normal',
					easing: 'swing',
					closer: !0,
					closeTemplate: '&times;',
					closerTemplate: '<div>[ close all ]</div>',
					log: function() {},
					beforeOpen: function() {},
					afterOpen: function() {},
					open: function() {},
					beforeClose: function() {},
					close: function() {},
					animateOpen: { opacity: 'show' },
					animateClose: { opacity: 'hide' }
				},
				notifications: [],
				element: null,
				interval: null,
				create: function(e, n) {
					void 0 !== (n = t.extend({}, this.defaults, n)).speed &&
						((n.openDuration = n.speed), (n.closeDuration = n.speed)),
						this.notifications.push({ message: e, options: n }),
						n.log.apply(this.element, [ this.element, e, n ]);
				},
				render: function(e) {
					var n = this,
						i = e.message,
						o = e.options;
					(o.themeState = '' == o.themeState ? '' : 'ui-state-' + o.themeState),
						(e = t('<div/>')
							.addClass(
								'jGrowl-notification ' +
									o.themeState +
									' ui-corner-all' +
									(null != o.group && '' != o.group ? ' ' + o.group : '')
							)
							.append(t('<div/>').addClass('jGrowl-close').html(o.closeTemplate))
							.append(t('<div/>').addClass('jGrowl-header').html(o.header))
							.append(t('<div/>').addClass('jGrowl-message').html(i))
							.data('jGrowl', o)
							.addClass(o.theme)
							.children('div.jGrowl-close')
							.bind('click.jGrowl', function() {
								t(this).parent().trigger('jGrowl.beforeClose');
							})
							.parent()),
						t(e)
							.bind('mouseover.jGrowl', function() {
								t('div.jGrowl-notification', n.element).data('jGrowl.pause', !0);
							})
							.bind('mouseout.jGrowl', function() {
								t('div.jGrowl-notification', n.element).data('jGrowl.pause', !1);
							})
							.bind('jGrowl.beforeOpen', function() {
								!1 !== o.beforeOpen.apply(e, [ e, i, o, n.element ]) && t(this).trigger('jGrowl.open');
							})
							.bind('jGrowl.open', function() {
								!1 !== o.open.apply(e, [ e, i, o, n.element ]) &&
									('after' == o.glue
										? t('div.jGrowl-notification:last', n.element).after(e)
										: t('div.jGrowl-notification:first', n.element).before(e),
									t(this).animate(o.animateOpen, o.openDuration, o.easing, function() {
										!1 === t.support.opacity && this.style.removeAttribute('filter'),
											null !== t(this).data('jGrowl') &&
												(t(this).data('jGrowl').created = new Date()),
											t(this).trigger('jGrowl.afterOpen');
									}));
							})
							.bind('jGrowl.afterOpen', function() {
								o.afterOpen.apply(e, [ e, i, o, n.element ]);
							})
							.bind('jGrowl.beforeClose', function() {
								!1 !== o.beforeClose.apply(e, [ e, i, o, n.element ]) &&
									t(this).trigger('jGrowl.close');
							})
							.bind('jGrowl.close', function() {
								t(this).data('jGrowl.pause', !0),
									t(this).animate(o.animateClose, o.closeDuration, o.easing, function() {
										t.isFunction(o.close)
											? !1 !== o.close.apply(e, [ e, i, o, n.element ]) && t(this).remove()
											: t(this).remove();
									});
							})
							.trigger('jGrowl.beforeOpen'),
						'' != o.corners && null != t.fn.corner && t(e).corner(o.corners),
						1 < t('div.jGrowl-notification:parent', n.element).size() &&
							0 == t('div.jGrowl-closer', n.element).size() &&
							!1 !== this.defaults.closer &&
							t(this.defaults.closerTemplate)
								.addClass('jGrowl-closer ' + this.defaults.themeState + ' ui-corner-all')
								.addClass(this.defaults.theme)
								.prependTo(n.element)
								.animate(this.defaults.animateOpen, this.defaults.speed, this.defaults.easing)
								.bind('click.jGrowl', function() {
									t(this).siblings().trigger('jGrowl.beforeClose'),
										t.isFunction(n.defaults.closer) &&
											n.defaults.closer.apply(t(this).parent()[0], [ t(this).parent()[0] ]);
								});
				},
				update: function() {
					t(this.element).find('div.jGrowl-notification:parent').each(function() {
						null != t(this).data('jGrowl') &&
							void 0 !== t(this).data('jGrowl').created &&
							t(this).data('jGrowl').created.getTime() + parseInt(t(this).data('jGrowl').life) <
								new Date().getTime() &&
							!0 !== t(this).data('jGrowl').sticky &&
							(null == t(this).data('jGrowl.pause') || !0 !== t(this).data('jGrowl.pause')) &&
							t(this).trigger('jGrowl.beforeClose');
					}),
						0 < this.notifications.length &&
							(0 == this.defaults.pool ||
								t(this.element).find('div.jGrowl-notification:parent').size() < this.defaults.pool) &&
							this.render(this.notifications.shift()),
						t(this.element).find('div.jGrowl-notification:parent').size() < 2 &&
							t(this.element)
								.find('div.jGrowl-closer')
								.animate(
									this.defaults.animateClose,
									this.defaults.speed,
									this.defaults.easing,
									function() {
										t(this).remove();
									}
								);
				},
				startup: function(n) {
					(this.element = t(n).addClass('jGrowl').append('<div class="jGrowl-notification"></div>')),
						(this.interval = setInterval(function() {
							t(n).data('jGrowl.instance').update();
						}, parseInt(this.defaults.check))),
						e && t(this.element).addClass('ie6');
				},
				shutdown: function() {
					t(this.element)
						.removeClass('jGrowl')
						.find('div.jGrowl-notification')
						.trigger('jGrowl.close')
						.parent()
						.empty(),
						clearInterval(this.interval);
				},
				close: function() {
					t(this.element).find('div.jGrowl-notification').each(function() {
						t(this).trigger('jGrowl.beforeClose');
					});
				}
			}),
			(t.jGrowl.defaults = t.fn.jGrowl.prototype.defaults);
	})(jQuery),
	$(document).ready(function() {
		$('#chkoutLogin').click(function() {
			$('#cartDropDown').hide(),
				console.log($('#bs-example-navbar-collapse-1').attr('class')),
				$('#bs-example-navbar-collapse-1').removeClass(),
				$('#bs-example-navbar-collapse-1').addClass('navbar-collapse collapse'),
				console.log($('#bs-example-navbar-collapse-1').attr('class')),
				event.stopPropagation(),
				$('#modal-quicksignIn').modal('show');
		}),
			$('.dropdown-toggle').click(function() {
				console.log($('#bs-example-navbar-collapse-1').attr('class')),
					$('#bs-example-navbar-collapse-1').removeClass(),
					$('#bs-example-navbar-collapse-1').addClass('navbar-collapse collapse'),
					console.log($('#bs-example-navbar-collapse-1').attr('class'));
			}),
			$('.bt-item,.product').hover(function() {
				var t = this.id;
				$('#heart' + t).css('display', 'block');
			}),
			$('.dropdown-toggle2').click(function() {
				console.log($('#bs-example-navbar-collapse-1').attr('class')),
					$('#bs-example-navbar-collapse-1').removeClass(),
					$('#bs-example-navbar-collapse-1').addClass('navbar-collapse collapse'),
					console.log($('#bs-example-navbar-collapse-1').attr('class'));
			}),
			$('.bt-item,.product').mouseleave(function() {
				var t = this.id;
				'heart-box-pink' == $('#heart' + t).attr('class')
					? $('#heart' + t).css('display', 'block')
					: $('#heart' + t).css('display', 'none');
			}),
			$(document).delegate('#signinTab,#signupTab', 'click', function(t) {
				$('#modal-quicksignIn .form-control').removeClass('has-error'),
					$('#modal-quicksignIn .alert-danger').remove();
			}),
			$(document).delegate('#signIn', 'click', function(t) {
				console.log($('#bs-example-navbar-collapse-1').attr('class')),
					$('#bs-example-navbar-collapse-1').removeClass(),
					$('#bs-example-navbar-collapse-1').addClass('navbar-collapse collapse'),
					console.log($('#bs-example-navbar-collapse-1').attr('class')),
					console.log($("#quick-register")),
					// $("#quick-register")[0].reset(),
					// $("#quick-signin")[0].reset(),
					//  document.forms.namedItem("quick-register").reset();
					// $('#quick-register').trigger('reset'),
					// $('#quick-signin').trigger('reset'),
					// getLocation(),
					$('#modal-quicksignIn').modal('show'),
					$('#modal-quicksignIn .form-control').removeClass('has-error'),
					$('#modal-quicksignIn .alert-danger').remove();
			}),
			$('.btn-dropdown-cart').click(function(t) {
				$('#cartDropDown').slideToggle('fast');
			}),
			$('#cartBtn').click(function() {
				$('#cartDropDown').show(),
					console.log($('#bs-example-navbar-collapse-1').attr('class')),
					$('#bs-example-navbar-collapse-1').removeClass(),
					$('#bs-example-navbar-collapse-1').addClass('navbar-collapse collapse in'),
					console.log($('#bs-example-navbar-collapse-1').attr('class')),
					event.stopPropagation();
			}),
			$('#btnLogin').click(function() {
				var t = $('#email').val(),
					e = $('#password').val();
				$.ajax({
					type: 'POST',
					dataType: 'json',
					data: { email: t, password: e },
					url: 'index.php?route=account/login/login',
					beforeSend: function() {
						$('#modal-quicksignIn #btnLogin').button('loading');
					},
					complete: function() {
						$('#modal-quicksignIn #btnLogin').button('reset');
					},
					success: function(t) {
						$('#modal-quicksignIn .form-control').removeClass('has-error'),
							t.islogged && (window.location.href = 'index.php?route=account/account'),
							t.error_email
								? ($('.alert-danger').remove(),
									$('#modal-quicksignIn .modal-header').after(
										'<div class="alert alert-danger" style="margin:5px;"><i class="fa fa-exclamation-circle"></i> ' +
											t.error_email +
											'</div>'
									),
									$('#quick-login #email').addClass('has-error'),
									$('#quick-login #email').focus())
								: t.error_password
									? ($('.alert-danger').remove(),
										$('#modal-quicksignIn .modal-header').after(
											'<div class="alert alert-danger" style="margin:5px;"><i class="fa fa-exclamation-circle"></i> ' +
												t.error_password +
												'</div>'
										),
										$('#quick-login #password').addClass('has-error'),
										$('#quick-login #password').focus())
									: t.error &&
										($('.alert-danger').remove(),
										$('#modal-quicksignIn .modal-header').after(
											'<div class="alert alert-danger" style="margin:5px;"><i class="fa fa-exclamation-circle"></i> ' +
												t.error +
												'</div>'
										),
										$('#quick-login #email').addClass('has-error'),
										$('#quick-login #email').focus()),
							t.success && (loacation(), $('#modal-quicksignIn').modal('hide'));
					}
				});
			}),
			$('#btnSignup').click(function() {
				$.ajax({
					url: 'index.php?route=account/simpleregister/registerTempCustomer',
					type: 'post',
					data: $(
						"#quick-register input[type='text'],#quick-register input[type='tel'],#quick-register input[type='email'], #quick-register input[type='password'], #quick-register input[type='checkbox']:checked"
					),
					dataType: 'json',
					beforeSend: function() {
						$('#quick-register #btnSignup').button('loading'),
							$('#modal-quicksignIn .alert-danger').remove();
					},
					complete: function() {
						$('#quick-register #btnSignup').button('reset');
					},
					success: function(t) {
						if (
							($('#modal-quicksignIn .form-control').removeClass('has-error'),
							t.islogged && (window.location.href = 'index.php?route=account/account'),
							t.error_firstname
								? ($('#quick-register #input-firstname').addClass('has-error'),
									$('#quick-register #input-firstname').focus(),
									$('#modal-quicksignIn .modal-header').after(
										'<div class="alert alert-danger" style="margin:5px;"><i class="fa fa-exclamation-circle"></i> ' +
											t.error_firstname +
											'</div>'
									))
								: t.error_lastname
									? ($('#quick-register #input-lastname').addClass('has-error'),
										$('#quick-register #input-lastname').focus(),
										$('#modal-quicksignIn .modal-header').after(
											'<div class="alert alert-danger" style="margin:5px;"><i class="fa fa-exclamation-circle"></i> ' +
												t.error_lastname +
												'</div>'
										))
									: t.error_email
										? ($('#quick-register #input-email').addClass('has-error'),
											$('#quick-register #input-email').focus(),
											$('#modal-quicksignIn .modal-header').after(
												'<div class="alert alert-danger" style="margin:5px;"><i class="fa fa-exclamation-circle"></i> ' +
													t.error_email +
													'</div>'
											))
										: t.error_telephone
											? ($('#quick-register #input-telephone').addClass('has-error'),
												$('#quick-register #input-telephone').focus(),
												$('#modal-quicksignIn .modal-header').after(
													'<div class="alert alert-danger" style="margin:5px;"><i class="fa fa-exclamation-circle"></i> ' +
														t.error_telephone +
														'</div>'
												))
											: t.error_password
												? ($('#quick-register #input-password').addClass('has-error'),
													$('#quick-register #input-password').focus(),
													$('#modal-quicksignIn .modal-header').after(
														'<div class="alert alert-danger" style="margin:5px;"><i class="fa fa-exclamation-circle"></i> ' +
															t.error_password +
															'</div>'
													))
												: t.error_confirm
													? ($('#quick-register #input-confirm').addClass('has-error'),
														$('#quick-register #input-confirm').focus(),
														$('#modal-quicksignIn .modal-header').after(
															'<div class="alert alert-danger" style="margin:5px;"><i class="fa fa-exclamation-circle"></i> ' +
																t.error_confirm +
																'</div>'
														))
													: t.error_warning
														? $('#modal-quicksignIn .modal-header').after(
																'<div class="alert alert-danger" style="margin:5px;"><i class="fa fa-exclamation-circle"></i> ' +
																	t.error_warning +
																	'</div>'
															)
														: t.error_warning_mobile
															? $('#modal-quicksignIn .modal-header').after(
																	'<div class="alert alert-danger" style="margin:5px;"><i class="fa fa-exclamation-circle"></i> ' +
																		t.error_warning_mobile +
																		'</div>'
																)
															: t.agree ||
																$('#modal-quicksignIn .modal-header').after(
																	'<div class="alert alert-danger" style="margin:5px;"><i class="fa fa-exclamation-circle"></i> ' +
																		t.text_agree +
																		'</div>'
																),
							t.now_login &&
								($('.quick-login').before(
									'<li class="dropdown"><a href="<?php echo $account; ?>" title="<?php echo $text_account; ?>" class="dropdown-toggle" data-toggle="dropdown"><i class="fa fa-user"></i> <span class="hidden-xs hidden-sm hidden-md"><?php echo $text_account; ?></span> <span class="caret"></span></a><ul class="dropdown-menu dropdown-menu-right"><li><a href="<?php echo $account; ?>"><?php echo $text_account; ?></a></li><li><a href="<?php echo $order; ?>"><?php echo $text_order; ?></a></li><li><a href="<?php echo $transaction; ?>"><?php echo $text_transaction; ?></a></li><li><a href="<?php echo $download; ?>"><?php echo $text_download; ?></a></li><li><a href="<?php echo $logout; ?>"><?php echo $text_logout; ?></a></li></ul></li>'
								),
								$('.quick-login').remove()),
							t.success)
						) {
							var e = t.password_sent,
								n = t.mobile_registered;
							$('#modal-quicksignIn .main-heading').html(t.heading_title),
								(success = t.text_message),
								(success += '<div>'),
								(success += '<div class="form-group required">'),
								(success += '<div>'),
								(success +=
									'<input type="hidden" name="telephone" value="' +
									n +
									'"  id="input-telephone" class="form-control" />'),
								(success +=
									'<input type="hidden" name="passwordSent" value="' +
									e +
									'"  id="input-passwordSent" class="form-control" />'),
								(success += '</div>'),
								(success += '</div>'),
								(success += '<div class="form-group required">'),
								(success += '<div>'),
								(success +=
									'<input type="text" name="password" value="" placeholder="' +
									t.text_verify_password +
									'"  id="input-password" class="form-control" />'),
								(success += '</div>'),
								(success += '</div>'),
								(success += '</div>'),
								(success +=
									'<div class="buttons"><div class="text-right"><a onclick="verifyPassword()" id="verifyPassword" class="btn btn-primary">' +
									t.button_verify +
									'</a> <a id="resendPassword" onclick="resendPassword()" class="btn btn-primary">' +
									t.button_resend +
									'</a></div></div>'),
								$('#modal-quicksignIn .modal-body').html(success);
						}
					}
				});
			}),
			$('.text-danger').each(function() {
				var t = $(this).parent().parent();
				t.hasClass('form-group') && t.addClass('has-error');
			}),
			$('#currency .currency-select').on('click', function(t) {
				t.preventDefault(),
					$("#currency input[name='code']").attr('value', $(this).attr('name')),
					$('#currency').submit();
			}),
			$('#language a').on('click', function(t) {
				t.preventDefault(),
					$("#language input[name='code']").attr('value', $(this).attr('href')),
					$('#language').submit();
			}),
			$("[data-toggle='tooltip']").tooltip({ container: 'body' }),
			$(document).ajaxStop(function() {
				$("[data-toggle='tooltip']").tooltip({ container: 'body' });
			});
	}),
	(cart = {
		add: function(t, e) {
			$.ajax({
				url: 'index.php?route=checkout/cart/add',
				type: 'post',
				data: 'product_id=' + t + '&quantity=' + (void 0 !== e ? e : 1),
				dataType: 'json',
				beforeSend: function() {
					$('#cart > button').button('loading');
				},
				success: function(t) {
					$('.alert, .text-danger').remove(),
						$('#cart > button').button('reset'),
						t.redirect && (location = t.redirect),
						t.success &&
							($('#content')
								.parent()
								.before(
									'<div class="alert alert-success"><i class="fa fa-check-circle"></i> ' +
										t.success +
										'<button type="button" class="close" data-dismiss="alert">&times;</button></div>'
								),
							$('#cart-total').html(t.total),
							$('html, body').animate({ scrollTop: 0 }, 'slow'),
							$('#cart > ul').load('index.php?route=common/cart/info ul li'));
				}
			});
		},
		update: function(t, e) {
			$.ajax({
				url: 'index.php?route=checkout/cart/edit',
				type: 'post',
				data: 'key=' + t + '&quantity=' + (void 0 !== e ? e : 1),
				dataType: 'json',
				beforeSend: function() {
					$('#cart > button').button('loading');
				},
				success: function(t) {
					$('#cart > button').button('reset'),
						$('#cart-total').html(t.total),
						'checkout/cart' == getURLVar('route') || 'checkout/checkout' == getURLVar('route')
							? (location = 'index.php?route=checkout/cart')
							: $('#cart > ul').load('index.php?route=common/cart/info ul li');
				}
			});
		},
		remove: function(t) {
			$.ajax({
				url: 'index.php?route=checkout/cart/remove',
				type: 'post',
				data: 'key=' + t,
				dataType: 'json',
				beforeSend: function() {
					$('.b-cart > button').button('loading');
				},
				success: function(t) {
					$('.b-cart > button').html("<span class='b-cart-total'>" + t.total + '</span>'),
						'checkout/cart' == getURLVar('route') || 'checkout/checkout' == getURLVar('route')
							? (location = 'index.php?route=checkout/cart')
							: $('.b-cart > ul').load('index.php?route=common/cart/info ul li');
				}
			});
		}
	}),
	(voucher = {
		add: function() {},
		remove: function(t) {
			$.ajax({
				url: 'index.php?route=checkout/cart/remove',
				type: 'post',
				data: 'key=' + t,
				dataType: 'json',
				beforeSend: function() {
					$('#cart > button').button('loading');
				},
				complete: function() {
					$('#cart > button').button('reset');
				},
				success: function(t) {
					$('#cart-total').html(t.total),
						'checkout/cart' == getURLVar('route') || 'checkout/checkout' == getURLVar('route')
							? (location = 'index.php?route=checkout/cart')
							: $('#cart > ul').load('index.php?route=common/cart/info ul li');
				}
			});
		}
	}),
	(wishlist = {
		add: function(t) {
			$.ajax({
				url: 'index.php?route=account/wishlist/add',
				type: 'post',
				data: 'product_id=' + t,
				dataType: 'json',
				success: function(t) {
					$('.alert').remove(),
						t.success &&
							$('#content')
								.parent()
								.before(
									'<div class="alert alert-success"><i class="fa fa-check-circle"></i> ' +
										t.success +
										'<button type="button" class="close" data-dismiss="alert">&times;</button></div>'
								),
						t.info &&
							$('#content')
								.parent()
								.before(
									'<div class="alert alert-info"><i class="fa fa-info-circle"></i> ' +
										t.info +
										'<button type="button" class="close" data-dismiss="alert">&times;</button></div>'
								),
						$('#wishlist-total').html(t.total),
						$('html, body').animate({ scrollTop: 0 }, 'slow');
				}
			});
		},
		remove: function() {}
	}),
	(compare = {
		add: function(t) {
			$.ajax({
				url: 'index.php?route=product/compare/add',
				type: 'post',
				data: 'product_id=' + t,
				dataType: 'json',
				success: function(t) {
					$('.alert').remove(),
						t.success &&
							($('#content')
								.parent()
								.before(
									'<div class="alert alert-success"><i class="fa fa-check-circle"></i> ' +
										t.success +
										'<button type="button" class="close" data-dismiss="alert">&times;</button></div>'
								),
							$('#compare-total').html(t.total),
							$('html, body').animate({ scrollTop: 0 }, 'slow'));
				}
			});
		},
		remove: function() {}
	}),
	(btadd = {
		cart: function(t, e, n, sellerAreaId = '') {
			// alert(sellerAreaId);
			return (
				$.ajax({
					url: 'index.php?route=bossthemes/boss_add/cart/',
					type: 'post',
					data: 'product_id=' + t + '&quantity=' + (void 0 !== e ? e : 1) + '&seller_id=' + n + '&seller_area_id=' + sellerAreaId,
					dataType: 'json',
					success: function(i) {
						if (
							(0 == parseInt(i.total)
								? $('li.retailer-option a').removeAttr('onclick')
								: $('li.retailer-option a').attr(
										'onclick',
										"return confirm('Your cart items will be removed while changing to other store. Are you sure to move to this store?')"
									),
							i.success)
						) {
							addProductNotices(i.title, i.thumb, i.success + '<br><br>' + i.msg, 'success'),
								$('.b-cart-total').html(i.total),
								$('.b-cart > ul').load('index.php?route=common/cart/info ul li');
							var o = i.cartId,
								r = "<div style='position:relative'>";
							(r +=
								'<a href="javascript:void(0)"><input id="select-number' +
								o +
								'" value="' +
								e +
								'" onchange="update_cart_data_home(\'' +
								o +
								"', 'select-number" +
								o +
								'\')" disabled/></a>'),
								(r +=
									'<a style="position:absolute;left: 40px;top: 24px;color: white" onclick="changeQty(\'select-number' +
									o +
									"',0," +
									n +
									'); return false;" class="decrease" href="javascript:void(0)">_</a>'),
								(r +=
									'<a style="position:absolute;right: 40px;top: 31px;color: white" onclick="changeQty(\'select-number' +
									o +
									"',1," +
									n +
									'); return false;" class="increase" href="javascript:void(0)">+</a>'),
								(r += '</div>'),
								$('#pro_div_id_' + t).html(r);
						}
					}
				}),
				!1
			);
		},
		wishlist: function(t) {
			$.ajax({
				url: 'index.php?route=bossthemes/boss_add/wishlist/',
				type: 'post',
				data: 'product_id=' + t,
				dataType: 'json',
				success: function(e) {
					e.success
						? (addProductNotices(e.title, e.thumb, e.success, 'success'),
							$('#myModal .btn-wishlist').attr('onclick', "wishlist_remove('" + t + "')"))
						: addProductNotices(e.title, '', e.info),
						$('.wishlist-total').html(e.total);
				}
			});
		},
		wishlist_cat: function(t) {
			$.ajax({
				url: 'index.php?route=bossthemes/boss_add/wishlist_cat/',
				type: 'post',
				data: t,
				dataType: 'json',
				success: function(t) {
					t.success
						? addProductNotice(t.title, t.thumb, t.success, 'success')
						: addProductNotice(t.title, '', t.info),
						$('.wishlist-total').html(t.total);
				}
			});
		},
		compare: function(t) {
			$.ajax({
				url: 'index.php?route=bossthemes/boss_add/compare',
				type: 'post',
				data: 'product_id=' + t,
				dataType: 'json',
				success: function(t) {
					t.success &&
						(addProductNotices(t.title, t.thumb, t.success, 'success'), $('#compare-total').html(t.total));
				}
			});
		}
	}),
	$(document).delegate('.agree', 'click', function(t) {
		t.preventDefault(), $('#modal-agree').remove();
		var e = this;
		$.ajax({
			url: $(e).attr('href'),
			type: 'get',
			dataType: 'html',
			success: function(t) {
				(html = '<div id="modal-agree" class="modal">'),
					(html += '  <div class="modal-dialog">'),
					(html += '    <div class="modal-content">'),
					(html += '      <div class="modal-header">'),
					(html +=
						'        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>'),
					(html += '        <h4 class="modal-title">' + $(e).text() + '</h4>'),
					(html += '      </div>'),
					(html += '      <div class="modal-body">' + t + '</div>'),
					(html += '    </div'),
					(html += '  </div>'),
					(html += '</div>'),
					$('body').append(html),
					$('#modal-agree').modal('show');
			}
		});
	}),
	(function(t) {
		t.fn.autocomplete = function(e) {
			return this.each(function() {
				(this.timer = null),
					(this.items = new Array()),
					t.extend(this, e),
					t(this).attr('autocomplete', 'off'),
					t(this).on('focus', function() {
						this.request();
					}),
					t(this).on('blur', function() {
						setTimeout(
							function(t) {
								t.hide();
							},
							200,
							this
						);
					}),
					t(this).on('keydown', function(t) {
						switch (t.keyCode) {
							case 27:
								this.hide();
								break;
							default:
								this.request();
						}
					}),
					(this.click = function(e) {
						e.preventDefault(),
							(value = t(e.target).parent().attr('data-value')),
							value && this.items[value] && this.select(this.items[value]);
					}),
					(this.show = function() {
						var e = t(this).position();
						t(this).siblings('ul.dropdown-menu').css({ top: e.top + t(this).outerHeight(), left: e.left }),
							t(this).siblings('ul.dropdown-menu').show();
					}),
					(this.hide = function() {
						t(this).siblings('ul.dropdown-menu').hide();
					}),
					(this.request = function() {
						clearTimeout(this.timer),
							(this.timer = setTimeout(
								function(e) {
									e.source(t(e).val(), t.proxy(e.response, e));
								},
								200,
								this
							));
					}),
					(this.response = function(e) {
						if (((html = ''), e.length)) {
							for (i = 0; i < e.length; i++) this.items[e[i].value] = e[i];
							for (i = 0; i < e.length; i++)
								e[i].category ||
									(html +=
										'<li data-value="' + e[i].value + '"><a href="#">' + e[i].label + '</a></li>');
							var n = new Array();
							for (i = 0; i < e.length; i++)
								e[i].category &&
									(n[e[i].category] ||
										((n[e[i].category] = new Array()),
										(n[e[i].category].name = e[i].category),
										(n[e[i].category].item = new Array())),
									n[e[i].category].item.push(e[i]));
							for (i in n)
								for (
									html += '<li class="dropdown-header">' + n[i].name + '</li>', j = 0;
									j < n[i].item.length;
									j++
								)
									html +=
										'<li data-value="' +
										n[i].item[j].value +
										'"><a href="#">&nbsp;&nbsp;&nbsp;' +
										n[i].item[j].label +
										'</a></li>';
						}
						html ? this.show() : this.hide(), t(this).siblings('ul.dropdown-menu').html(html);
					}),
					t(this).after('<ul class="dropdown-menu"></ul>'),
					t(this).siblings('ul.dropdown-menu').delegate('a', 'click', t.proxy(this.click, this));
			});
		};
	})(window.jQuery),
	$(document).ready(function() {
		$('#wishlist_cat_btn').click(function() {
			btadd.wishlist_cat($('#wishlist_cat_form').serializeArray()),
				changeHeart($('#wishlist_product_id').val(), $('#wishlist_seller_id').val()),
				$('#modal-wishlist_category').modal('hide');
		});
	}),
	(window.google = window.google || {}),
	(google.maps = google.maps || {}),
	(function() {
		var t = (google.maps.modules = {});
		(google.maps.__gjsload__ = function(e, n) {
			t[e] = n;
		}),
			(google.maps.Load = function(t) {
				delete google.maps.Load,
					t(
						[
							0.009999999776482582,
							[
								null,
								[
									[
										'http://khm0.googleapis.com/kh?v=721&hl=en-US&',
										'http://khm1.googleapis.com/kh?v=721&hl=en-US&'
									],
									null,
									null,
									null,
									1,
									'721',
									[
										'https://khms0.google.com/kh?v=721&hl=en-US&',
										'https://khms1.google.com/kh?v=721&hl=en-US&'
									]
								],
								null,
								null,
								null,
								null,
								[ [ 'http://cbk0.googleapis.com/cbk?', 'http://cbk1.googleapis.com/cbk?' ] ],
								[
									[
										'http://khm0.googleapis.com/kh?v=103&hl=en-US&',
										'http://khm1.googleapis.com/kh?v=103&hl=en-US&'
									],
									null,
									null,
									null,
									null,
									'103',
									[
										'https://khms0.google.com/kh?v=103&hl=en-US&',
										'https://khms1.google.com/kh?v=103&hl=en-US&'
									]
								],
								[
									[
										'http://mt0.googleapis.com/mapslt?hl=en-US&',
										'http://mt1.googleapis.com/mapslt?hl=en-US&'
									]
								],
								null,
								null,
								null,
								[
									[
										'https://mts0.googleapis.com/mapslt?hl=en-US&',
										'https://mts1.googleapis.com/mapslt?hl=en-US&'
									]
								]
							],
							[
								'en-US',
								'US',
								null,
								0,
								null,
								null,
								'https://maps.gstatic.com/mapfiles/',
								'https://csi.gstatic.com',
								'https://maps.googleapis.com',
								'https://maps.googleapis.com',
								null,
								'https://maps.google.com',
								'https://gg.google.com',
								'https://maps.gstatic.com/maps-api-v3/api/images/',
								'https://www.google.com/maps',
								0,
								'https://www.google.com'
							],
							[ 'https://maps.google.com/maps-api-v3/api/js/28/8', '3.28.8' ],
							[ 499533890 ],
							1,
							null,
							null,
							null,
							null,
							null,
							'',
							null,
							null,
							0,
							'http://khm.googleapis.com/mz?v=721&',
							null,
							'https://earthbuilder.googleapis.com',
							'https://earthbuilder.googleapis.com',
							null,
							'http://mt.googleapis.com/maps/vt/icon',
							[
								[ 'http://maps.google.com/maps/vt' ],
								[ 'https://maps.google.com/maps/vt' ],
								null,
								null,
								null,
								null,
								null,
								null,
								null,
								null,
								null,
								null,
								[ 'https://www.google.com/maps/vt' ],
								'/maps/vt',
								378e6,
								378
							],
							2,
							500,
							[
								null,
								null,
								null,
								null,
								'http://www.google.com/maps/preview/log204',
								'',
								'http://static.panoramio.com.storage.googleapis.com/photos/',
								[
									'http://geo0.ggpht.com/cbk',
									'http://geo1.ggpht.com/cbk',
									'http://geo2.ggpht.com/cbk',
									'http://geo3.ggpht.com/cbk'
								],
								'https://maps.googleapis.com/maps/api/js/GeoPhotoService.GetMetadata',
								'https://maps.googleapis.com/maps/api/js/GeoPhotoService.SingleImageSearch',
								[
									'http://lh3.ggpht.com/',
									'http://lh4.ggpht.com/',
									'http://lh5.ggpht.com/',
									'http://lh6.ggpht.com/'
								]
							],
							[
								'https://www.google.com/maps/api/js/master?pb=!1m2!1u28!2s8!2sen-US!3sUS!4s28/8',
								'https://www.google.com/maps/api/js/widget?pb=!1m2!1u28!2s8!2sen-US'
							],
							null,
							0,
							null,
							'/maps/api/js/ApplicationService.GetEntityDetails',
							0,
							null,
							null,
							[ null, null, null, null, null, null, null, null, null, [ 0, 0 ] ],
							null,
							[],
							[ '28.8' ]
						],
						e
					);
			});
		var e = new Date().getTime();
	})(),
	function(t) {
		var e,
			n,
			i,
			o,
			r,
			s,
			a,
			l,
			c,
			u,
			d,
			h,
			p,
			f,
			m,
			g,
			v,
			b,
			y,
			w,
			x,
			k,
			$,
			T,
			E,
			C,
			_,
			S,
			j,
			N,
			A,
			I,
			L,
			D,
			O,
			P,
			R,
			M,
			q,
			z,
			U,
			H,
			G,
			F,
			B,
			W,
			V,
			Q,
			X,
			K,
			Y,
			J,
			Z,
			tt,
			et,
			nt,
			it,
			ot,
			rt,
			st,
			at,
			lt,
			ct,
			ut,
			dt,
			ht,
			pt,
			ft,
			mt,
			gt,
			vt,
			bt,
			yt,
			wt,
			xt,
			kt,
			$t,
			Tt,
			Et,
			Ct,
			_t,
			St,
			jt,
			Nt,
			At,
			It,
			Lt,
			Dt,
			Ot,
			Pt,
			Rt,
			Mt,
			qt,
			zt,
			Ut,
			Ht,
			Gt,
			Ft,
			Bt,
			Wt,
			Vt,
			Qt,
			Xt,
			Kt,
			Yt,
			Jt,
			Zt,
			te,
			ee,
			ne,
			ie,
			oe,
			re,
			se,
			ae,
			le,
			ce,
			ue,
			de,
			he,
			pe,
			fe,
			me,
			ge,
			ve,
			be,
			ye,
			we,
			xe,
			ke,
			$e;
		for (
			t.aa = 'ERROR',
				t.ba = 'INVALID_REQUEST',
				t.ca = 'MAX_DIMENSIONS_EXCEEDED',
				t.da = 'MAX_ELEMENTS_EXCEEDED',
				t.ea = 'MAX_WAYPOINTS_EXCEEDED',
				t.fa = 'NOT_FOUND',
				t.ga = 'OK',
				t.ha = 'OVER_QUERY_LIMIT',
				t.ia = 'REQUEST_DENIED',
				t.ja = 'UNKNOWN_ERROR',
				t.ka = 'ZERO_RESULTS',
				t.la = function() {
					return function(t) {
						return t;
					};
				},
				t.ma = function() {
					return function() {};
				},
				t.na = function(t) {
					return function(e) {
						this[t] = e;
					};
				},
				t.oa = function(t) {
					return function() {
						return this[t];
					};
				},
				t.pa = function(t) {
					return function() {
						return t;
					};
				},
				t.ra = function(e) {
					return function() {
						return t.qa[e].apply(this, arguments);
					};
				},
				e = function() {
					(e = t.ma()), ve.Symbol || (ve.Symbol = n);
				},
				n = function(t) {
					return 'jscomp_symbol_' + (t || '') + be++;
				},
				t.za = function() {
					e();
					var n = ve.Symbol.iterator;
					n || (n = ve.Symbol.iterator = ve.Symbol('iterator')),
						'function' != typeof Array.prototype[n] &&
							ge(Array.prototype, n, {
								configurable: !0,
								writable: !0,
								value: function() {
									return t.ya(this);
								}
							}),
						(t.za = t.ma());
				},
				t.ya = function(t) {
					var e = 0;
					return i(function() {
						return e < t.length ? { done: !1, value: t[e++] } : { done: !0 };
					});
				},
				i = function(e) {
					return (
						(0, t.za)(),
						((e = { next: e })[ve.Symbol.iterator] = function() {
							return this;
						}),
						e
					);
				},
				t.m = function(t) {
					return void 0 !== t;
				},
				t.Ba = t.ma(),
				t.Ca = function(t) {
					var e = typeof t;
					if ('object' == e) {
						if (!t) return 'null';
						if (t instanceof Array) return 'array';
						if (t instanceof Object) return e;
						var n = Object.prototype.toString.call(t);
						if ('[object Window]' == n) return 'object';
						if (
							'[object Array]' == n ||
							('number' == typeof t.length &&
								void 0 !== t.splice &&
								void 0 !== t.propertyIsEnumerable &&
								!t.propertyIsEnumerable('splice'))
						)
							return 'array';
						if (
							'[object Function]' == n ||
							(void 0 !== t.call && void 0 !== t.propertyIsEnumerable && !t.propertyIsEnumerable('call'))
						)
							return 'function';
					} else if ('function' == e && void 0 === t.call) return 'object';
					return e;
				},
				t.Ea = function(e) {
					return 'array' == t.Ca(e);
				},
				t.Fa = function(e) {
					var n = t.Ca(e);
					return 'array' == n || ('object' == n && 'number' == typeof e.length);
				},
				t.Ga = function(t) {
					return 'string' == typeof t;
				},
				t.Ha = function(t) {
					return 'number' == typeof t;
				},
				t.Ia = function(e) {
					return 'function' == t.Ca(e);
				},
				t.Ja = function(t) {
					var e = typeof t;
					return ('object' == e && null != t) || 'function' == e;
				},
				t.Ma = function(t) {
					return t[ke] || (t[ke] = ++$e);
				},
				o = function(t, e, n) {
					return t.call.apply(t.bind, arguments);
				},
				r = function(t, e, n) {
					if (!t) throw Error();
					if (2 < arguments.length) {
						var i = Array.prototype.slice.call(arguments, 2);
						return function() {
							var n = Array.prototype.slice.call(arguments);
							return Array.prototype.unshift.apply(n, i), t.apply(e, n);
						};
					}
					return function() {
						return t.apply(e, arguments);
					};
				},
				t.p = function(e, n, i) {
					return (
						Function.prototype.bind && -1 != Function.prototype.bind.toString().indexOf('native code')
							? (t.p = o)
							: (t.p = r),
						t.p.apply(null, arguments)
					);
				},
				t.Pa = function() {
					return +new Date();
				},
				t.t = function(t, e) {
					function n() {}
					(n.prototype = e.prototype),
						(t.fb = e.prototype),
						(t.prototype = new n()),
						((t.prototype.constructor = t).Ge = function(t, n, i) {
							for (var o = Array(arguments.length - 2), r = 2; r < arguments.length; r++)
								o[r - 2] = arguments[r];
							e.prototype[n].apply(t, o);
						});
				},
				t.Qa = function(t) {
					return t.replace(/^[\s\xa0]+|[\s\xa0]+$/g, '');
				},
				t.Sa = function() {
					return -1 != t.Ra.toLowerCase().indexOf('webkit');
				},
				t.Ua = function(e, n) {
					var i = 0;
					(e = t.Qa(String(e)).split('.')), (n = t.Qa(String(n)).split('.'));
					for (var o = Math.max(e.length, n.length), r = 0; 0 == i && r < o; r++) {
						var a = e[r] || '',
							l = n[r] || '';
						do {
							if (
								((a = /(\d*)(\D*)(.*)/.exec(a) || [ '', '', '', '' ]),
								(l = /(\d*)(\D*)(.*)/.exec(l) || [ '', '', '', '' ]),
								0 == a[0].length && 0 == l[0].length)
							)
								break;
							(i =
								s(
									0 == a[1].length ? 0 : (0, window.parseInt)(a[1], 10),
									0 == l[1].length ? 0 : (0, window.parseInt)(l[1], 10)
								) ||
								s(0 == a[2].length, 0 == l[2].length) ||
								s(a[2], l[2])),
								(a = a[3]),
								(l = l[3]);
						} while (0 == i);
					}
					return i;
				},
				s = function(t, e) {
					return t < e ? -1 : e < t ? 1 : 0;
				},
				t.Wa = function(e, n, i) {
					if (((i = null == i ? 0 : i < 0 ? Math.max(0, e.length + i) : i), t.Ga(e)))
						return t.Ga(n) && 1 == n.length ? e.indexOf(n, i) : -1;
					for (; i < e.length; i++) if ((i in e) && e[i] === n) return i;
					return -1;
				},
				t.v = function(e, n, i) {
					for (var o = e.length, r = t.Ga(e) ? e.split('') : e, s = 0; s < o; s++)
						(s in r) && n.call(i, r[s], s, e);
				},
				a = function(e, n) {
					for (var i = e.length, o = t.Ga(e) ? e.split('') : e, r = 0; r < i; r++)
						if ((r in o) && n.call(void 0, o[r], r, e)) return r;
					return -1;
				},
				t.Za = function(e, n) {
					var i;
					return (i = 0 <= (n = t.Wa(e, n))) && t.Ya(e, n), i;
				},
				t.Ya = function(t, e) {
					Array.prototype.splice.call(t, e, 1);
				},
				t.$a = function(t, e, n) {
					return arguments.length <= 2
						? Array.prototype.slice.call(t, e)
						: Array.prototype.slice.call(t, e, n);
				},
				t.ab = function(e) {
					return '' + (t.Ja(e) ? t.Ma(e) : e);
				},
				t.w = function(t) {
					return t ? t.length : 0;
				},
				t.cb = function(e, n) {
					t.bb(n, function(t) {
						e[t] = n[t];
					});
				},
				t.db = function(t) {
					for (var e in t) return !1;
					return !0;
				},
				t.eb = function(t, e, n) {
					return null != e && (t = Math.max(t, e)), null != n && (t = Math.min(t, n)), t;
				},
				t.fb = function(t, e, n) {
					return ((t - e) % (n -= e) + n) % n + e;
				},
				t.gb = function(t, e, n) {
					return Math.abs(t - e) <= (n || 1e-9);
				},
				t.hb = function(e, n) {
					for (var i = [], o = t.w(e), r = 0; r < o; ++r) i.push(n(e[r], r));
					return i;
				},
				t.jb = function(e, n) {
					for (var i = t.ib(void 0, t.w(n)), o = t.ib(void 0, 0); o < i; ++o) e.push(n[o]);
				},
				t.kb = function(t) {
					return 'number' == typeof t;
				},
				t.lb = function(t) {
					return 'object' == typeof t;
				},
				t.ib = function(t, e) {
					return null == t ? e : t;
				},
				t.mb = function(t) {
					return 'string' == typeof t;
				},
				t.nb = function(t) {
					return t === !!t;
				},
				t.bb = function(t, e) {
					for (var n in t) e(n, t[n]);
				},
				t.pb = function(e) {
					return function() {
						var n = this,
							i = arguments;
						t.ob(function() {
							e.apply(n, i);
						});
					};
				},
				t.ob = function(t) {
					return window.setTimeout(t, 0);
				},
				l = function(t, e) {
					if (Object.prototype.hasOwnProperty.call(t, e)) return t[e];
				},
				t.rb = function(t) {},
				t.sb = function(t) {
					(t.cancelBubble = !0), t.stopPropagation && t.stopPropagation();
				},
				t.tb = function(e) {
					e.preventDefault && t.m(e.defaultPrevented) ? e.preventDefault() : (e.returnValue = !1);
				},
				t.ub = function(e) {
					(e = e || window.event), t.sb(e), t.tb(e);
				},
				t.vb = function(e) {
					(e.handled = !0), t.m(e.bubbles) || (e.returnValue = 'handled');
				},
				c = function(t, e) {
					return t.__e3_ || (t.__e3_ = {}), (t = t.__e3_)[e] || (t[e] = {}), t[e];
				},
				u = function(e, n) {
					var i = e.__e3_ || {};
					if (n) e = i[n] || {};
					else for (n in ((e = {}), i)) t.cb(e, i[n]);
					return e;
				},
				d = function(t, e) {
					return function(n) {
						return e.call(t, n, this);
					};
				},
				h = function(e, n, i) {
					return function(o) {
						var r = [ n, e ];
						t.jb(r, arguments), t.x.trigger.apply(this, r), i && t.vb.apply(null, arguments);
					};
				},
				p = function(t, e, n, i) {
					(this.f = t),
						(this.j = e),
						(this.b = n),
						(this.l = null),
						(this.m = i),
						(this.id = ++Ne),
						(c(t, e)[this.id] = this),
						Ee && ('tagName' in t) && (Ce[this.id] = this);
				},
				f = function(t) {
					return (t.l = function(e) {
						if ((e || (e = window.event), e && !e.target))
							try {
								e.target = e.srcElement;
							} catch (e) {}
						var n;
						return (
							(n = t.b.apply(t.f, [ e ])),
							(!e ||
								'click' != e.type ||
								!(e = e.srcElement) ||
								'A' != e.tagName ||
								'javascript:void(0)' != e.href) &&
								n
						);
					});
				},
				t.z = t.ma(),
				m = function(e, n) {
					e[(o = n + '_changed')] ? e[o]() : e.changed(n);
					var i,
						o = v(e, n);
					for (i in o) {
						var r = o[i];
						m(r.zc, r.Ya);
					}
					t.x.trigger(e, n.toLowerCase() + '_changed');
				},
				t.Jb = function(t) {
					return Ae[t] || (Ae[t] = t.substr(0, 1).toUpperCase() + t.substr(1));
				},
				g = function(t) {
					return t.gm_accessors_ || (t.gm_accessors_ = {}), t.gm_accessors_;
				},
				v = function(t, e) {
					return (
						t.gm_bindings_ || (t.gm_bindings_ = {}),
						t.gm_bindings_.hasOwnProperty(e) || (t.gm_bindings_[e] = {}),
						t.gm_bindings_[e]
					);
				},
				b = function(t, e, n) {
					(this.l = n), (this.j = t), (this.m = e), (this.f = 0), (this.b = null);
				},
				t.Mb = t.la(),
				t.Nb = function(t) {
					var e,
						n = !1;
					return function() {
						return n || ((e = t()), (n = !0)), e;
					};
				},
				t.Ob = function(t, e, n) {
					for (var i in t) e.call(n, t[i], i, t);
				},
				t.Pb = function(e) {
					return t.Pb[' '](e), e;
				},
				y = function(t, e) {
					var n = ze;
					return Object.prototype.hasOwnProperty.call(n, t) ? n[t] : (n[t] = e(t));
				},
				t.Sb = function(e) {
					return -1 != t.Ra.indexOf(e);
				},
				t.Tb = function(t, e) {
					(this.f = t || 0), (this.j = e || 0);
				},
				t.Ub = function(e, n) {
					return e
						? function() {
								--e || n();
							}
						: (n(), t.Ba);
				},
				t.Vb = function(t, e, n) {
					var i = t.getElementsByTagName('head')[0];
					return (
						((t = t.createElement('script')).type = 'text/javascript'),
						(t.charset = 'UTF-8'),
						(t.src = e),
						n && (t.onerror = n),
						i.appendChild(t),
						t
					);
				},
				w = function(t) {
					for (var e = '', n = 0, i = arguments.length; n < i; ++n) {
						var o = arguments[n];
						o.length && '/' == o[0] ? (e = o) : (e && '/' != e[e.length - 1] && (e += '/'), (e += o));
					}
					return e;
				},
				x = function() {
					this.f = this.b = null;
				},
				k = function() {
					this.next = this.b = this.wc = null;
				},
				t.$b = function() {
					return t.Sb('iPhone') && !t.Sb('iPod') && !t.Sb('iPad');
				},
				$ = function(t) {
					(this.j = window.document), (this.b = {}), (this.f = t);
				},
				T = function(e) {
					var n = e;
					if (e instanceof Array) (n = Array(e.length)), t.bc(n, e);
					else if (e instanceof Object) {
						var i,
							o = (n = {});
						for (i in e) e.hasOwnProperty(i) && (o[i] = T(e[i]));
					}
					return n;
				},
				t.bc = function(t, e) {
					for (var n = 0; n < e.length; ++n) e.hasOwnProperty(n) && (t[n] = T(e[n]));
				},
				t.dc = function(t, e) {
					return t[e] || (t[e] = []), t[e];
				},
				t.fc = function(t, e) {
					if (null == t || null == e) return (null == t) == (null == e);
					if (t.constructor != Array && t.constructor != Object)
						throw Error('Invalid object type passed into jsproto.areObjectsEqual()');
					if (t === e) return !0;
					if (t.constructor != e.constructor) return !1;
					for (var n in t) if (!((n in e) && E(t[n], e[n]))) return !1;
					for (var i in e) if (!(i in t)) return !1;
					return !0;
				},
				E = function(e, n) {
					return (
						e === n ||
						!((!0 !== e && 1 !== e) || (!0 !== n && 1 !== n)) ||
						!((!1 !== e && 0 !== e) || (!1 !== n && 0 !== n)) ||
						(e instanceof Object && n instanceof Object && !!t.fc(e, n))
					);
				},
				t.gc = function(t, e, n, i) {
					(this.type = t), (this.label = e), (this.rk = n), (this.uc = i);
				},
				C = function(t) {
					switch (t) {
						case 'd':
						case 'f':
						case 'i':
						case 'j':
						case 'u':
						case 'v':
						case 'x':
						case 'y':
						case 'g':
						case 'h':
						case 'n':
						case 'o':
						case 'e':
							return 0;
						case 's':
						case 'z':
						case 'B':
							return '';
						case 'b':
							return !1;
						default:
							return null;
					}
				},
				t.ic = function(e, n, i) {
					return new t.gc(e, 1, t.m(n) ? n : C(e), i);
				},
				t.jc = function(e, n, i) {
					return new t.gc(e, 2, t.m(n) ? n : C(e), i);
				},
				t.nc = function(e) {
					return t.ic('i', e);
				},
				t.oc = function(e) {
					return t.ic('v', e);
				},
				t.pc = function(e) {
					return t.ic('b', e);
				},
				t.qc = function(e) {
					return t.ic('e', e);
				},
				t.D = function(e, n) {
					return t.ic('m', e, n);
				},
				t.rc = function() {
					return t.Sb('Trident') || t.Sb('MSIE');
				},
				t.tc = function() {
					return (
						t.Sb('Safari') &&
						!(_() || t.Sb('Coast') || t.Sb('Opera') || t.Sb('Edge') || t.Sb('Silk') || t.Sb('Android'))
					);
				},
				_ = function() {
					return (t.Sb('Chrome') || t.Sb('CriOS')) && !t.Sb('Edge');
				},
				t.uc = function(t) {
					return t * Math.PI / 180;
				},
				t.vc = function(t) {
					return 180 * t / Math.PI;
				},
				t.wc = t.na('b'),
				S = function() {
					(this.l = {}), (this.f = {}), (this.m = {}), (this.b = {}), (this.j = new A());
				},
				j = function(e, n) {
					e.l[n] ||
						((e.l[n] = !0),
						I(e.j, function(i) {
							for (var o = i.b[n], r = o ? o.length : 0, s = 0; s < r; ++s) {
								var a = o[s];
								e.b[a] || j(e, a);
							}
							(i = i.j).b[n] || t.Vb(i.j, w(i.f, n) + '.js');
						}));
				},
				N = function(t, e) {
					var n = Xe;
					for (var i in ((this.j = t), (t = {}), (this.b = n)))
						for (var o = n[i], r = 0, s = o.length; r < s; ++r) {
							var a = o[r];
							t[a] || (t[a] = []), t[a].push(i);
						}
					(this.l = t), (this.f = e);
				},
				A = function() {
					this.b = [];
				},
				I = function(t, e) {
					t.f ? e(t.f) : t.b.push(e);
				},
				L = t.ma(),
				D = function(t, e, n) {
					for (var i = 1; i < e.A.length; ++i) {
						var o = e.A[i],
							r = t[i + e.b];
						if (o && null != r)
							if (3 == o.label) for (var s = 0; s < r.length; ++s) O(r[s], i, o, n);
							else O(r, i, o, n);
					}
				},
				O = function(t, e, n, i) {
					if ('m' == n.type) {
						var o = i.length;
						D(t, n.uc, i), i.splice(o, 0, [ e, 'm', i.length - o ].join(''));
					} else
						'b' == n.type && (t = t ? '1' : '0'),
							i.push([ e, n.type, (0, window.encodeURIComponent)(t) ].join(''));
				},
				P = function(e) {
					t.Kc.setTimeout(function() {
						throw e;
					}, 0);
				},
				R = function() {
					var e = t.Mc.f;
					(e = He(e)),
						!t.Ia(t.Kc.setImmediate) ||
						(t.Kc.Window &&
							t.Kc.Window.prototype &&
							!t.Sb('Edge') &&
							t.Kc.Window.prototype.setImmediate == t.Kc.setImmediate)
							? (Me || (Me = M()), Me(e))
							: t.Kc.setImmediate(e);
				},
				M = function() {
					var e = t.Kc.MessageChannel;
					if (
						(void 0 === e &&
							'undefined' != typeof window &&
							window.postMessage &&
							window.addEventListener &&
							!t.Sb('Presto') &&
							(e = function() {
								((o = window.document.createElement('IFRAME')).style.display = 'none'),
									(o.src = ''),
									window.document.documentElement.appendChild(o);
								var e = o.contentWindow;
								(o = e.document).open(), o.write(''), o.close();
								var n = 'callImmediate' + Math.random(),
									i =
										'file:' == e.location.protocol
											? '*'
											: e.location.protocol + '//' + e.location.host,
									o = (0, t.p)(function(t) {
										('*' != i && t.origin != i) || t.data != n || this.port1.onmessage();
									}, this);
								e.addEventListener('message', o, !1),
									(this.port1 = {}),
									(this.port2 = {
										postMessage: function() {
											e.postMessage(n, i);
										}
									});
							}),
						void 0 !== e && !t.rc())
					) {
						var n = new e(),
							i = {},
							o = i;
						return (
							(n.port1.onmessage = function() {
								if (t.m(i.next)) {
									var e = (i = i.next).og;
									(i.og = null), e();
								}
							}),
							function(t) {
								(o.next = { og: t }), (o = o.next), n.port2.postMessage(0);
							}
						);
					}
					return void 0 !== window.document &&
					('onreadystatechange' in window.document.createElement('SCRIPT'))
						? function(t) {
								var e = window.document.createElement('SCRIPT');
								(e.onreadystatechange = function() {
									(e.onreadystatechange = null),
										e.parentNode.removeChild(e),
										(e = null),
										t(),
										(t = null);
								}),
									window.document.documentElement.appendChild(e);
							}
						: function(e) {
								t.Kc.setTimeout(e, 0);
							};
				},
				q = function() {
					var e = t.Kc.document;
					return e ? e.documentMode : void 0;
				},
				t.Tc = function(e) {
					return y(e, function() {
						return 0 <= t.Ua(t.Sc, e);
					});
				},
				z = function(t, e) {
					-180 == t && 180 != e && (t = 180), -180 == e && 180 != t && (e = 180), (this.b = t), (this.f = e);
				},
				t.Vc = function(t) {
					return t.b > t.f;
				},
				t.Xc = function(e, n) {
					return Math.abs(n.b - e.b) % 360 + Math.abs(t.Wc(n) - t.Wc(e)) <= 1e-9;
				},
				t.Yc = function(t, e) {
					var n = e - t;
					return 0 <= n ? n : e + 180 - (t - 180);
				},
				t.Wc = function(e) {
					return e.isEmpty() ? 0 : t.Vc(e) ? 360 - (e.b - e.f) : e.f - e.b;
				},
				U = function(t, e) {
					(this.f = t), (this.b = e);
				},
				t.$c = function(t) {
					return t.isEmpty() ? 0 : t.b - t.f;
				},
				H = function(t) {
					(this.message = t), (this.name = 'InvalidValueError'), (this.stack = Error().stack);
				},
				t.bd = function(t, e) {
					var n = '';
					if (null != e) {
						if (!(e instanceof H)) return e;
						n = ': ' + e.message;
					}
					return new H(t + n);
				},
				t.cd = function(e) {
					if (!(e instanceof H)) throw e;
					t.rb(e.name + ': ' + e.message);
				},
				t.G = function(t, e, n) {
					var i = S.b();
					(t = '' + t), i.b[t] ? e(i.b[t]) : ((i.f[t] = i.f[t] || []).push(e), n || j(i, t));
				},
				t.dd = function(t, e) {
					S.b().b['' + t] = e;
				},
				G = function(e, n, i) {
					var o = [],
						r = t.Ub(e.length, function() {
							n.apply(null, o);
						});
					t.v(e, function(e, n) {
						t.G(
							e,
							function(t) {
								(o[n] = t), r();
							},
							i
						);
					});
				},
				t.Mc = function(e, n) {
					t.Mc.b || t.Mc.m(), t.Mc.j || (t.Mc.b(), (t.Mc.j = !0)), t.Mc.l.add(e, n);
				},
				t.hd = function(e, n) {
					var i;
					return (
						(i = i ? i + ': ' : ''),
						function(o) {
							if (!o || !t.lb(o)) throw t.bd(i + 'not an Object');
							var r,
								s = {};
							for (r in o) if (((s[r] = o[r]), !n && !e[r])) throw t.bd(i + 'unknown property ' + r);
							for (r in e)
								try {
									var a = e[r](s[r]);
									(t.m(a) || Object.prototype.hasOwnProperty.call(o, r)) && (s[r] = e[r](s[r]));
								} catch (o) {
									throw t.bd(i + 'in property ' + r, o);
								}
							return s;
						}
					);
				},
				F = function(t) {
					try {
						return !!t.cloneNode;
					} catch (t) {
						return !1;
					}
				},
				t.jd = function(e, n, i) {
					return i
						? function(i) {
								if (i instanceof e) return i;
								try {
									return new e(i);
								} catch (i) {
									throw t.bd('when calling new ' + n, i);
								}
							}
						: function(i) {
								if (i instanceof e) return i;
								throw t.bd('not an instance of ' + n);
							};
				},
				t.kd = function(e) {
					return function(n) {
						for (var i in e) if (e[i] == n) return n;
						throw t.bd(n);
					};
				},
				t.ld = function(e) {
					return function(n) {
						if (!t.Ea(n)) throw t.bd('not an Array');
						return t.hb(n, function(n, i) {
							try {
								return e(n);
							} catch (n) {
								throw t.bd('at index ' + i, n);
							}
						});
					};
				},
				t.md = function(e, n) {
					return function(i) {
						if (e(i)) return i;
						throw t.bd(n || '' + i);
					};
				},
				t.nd = function(e) {
					return function(n) {
						for (var i = [], o = 0, r = e.length; o < r; ++o) {
							var s = e[o];
							try {
								(s.If || s)(n);
							} catch (n) {
								if (!(n instanceof H)) throw n;
								i.push(n.message);
								continue;
							}
							return (s.then || s)(n);
						}
						throw t.bd(i.join('; and '));
					};
				},
				t.od = function(t, e) {
					return function(n) {
						return e(t(n));
					};
				},
				t.pd = function(t) {
					return function(e) {
						return null == e ? e : t(e);
					};
				},
				B = function(e) {
					return function(n) {
						if (n && null != n[e]) return n;
						throw t.bd('no ' + e + ' property');
					};
				},
				t.H = function(t, e) {
					(this.x = t), (this.y = e);
				},
				W = function(e) {
					if (e instanceof t.H) return e;
					try {
						t.hd({ x: t.rd, y: t.rd }, !0)(e);
					} catch (e) {
						throw t.bd('not a Point', e);
					}
					return new t.H(e.x, e.y);
				},
				t.I = function(t, e, n, i) {
					(this.width = t), (this.height = e), (this.j = n || 'px'), (this.f = i || 'px');
				},
				V = function(e) {
					if (e instanceof t.I) return e;
					try {
						t.hd({ height: t.rd, width: t.rd }, !0)(e);
					} catch (e) {
						throw t.bd('not a Size', e);
					}
					return new t.I(e.width, e.height);
				},
				Q = function(t) {
					var e = Nn,
						n = S.b().j;
					(t = n.f = new N(new $(t), e)), (e = 0);
					for (var i = n.b.length; e < i; ++e) n.b[e](t);
					n.b.length = 0;
				},
				t.yd = function(e) {
					(this.j = e || t.ab), (this.f = {});
				},
				t.zd = function(e, n) {
					var i = e.f,
						o = e.j(n);
					i[o] || ((i[o] = n), t.x.trigger(e, 'insert', n), e.b && e.b(n));
				},
				t.Ad = function(e, n, i) {
					(this.heading = e), (this.pitch = t.eb(n, -90, 90)), (this.zoom = Math.max(0, i));
				},
				X = function(e) {
					(this.P = []), (this.b = (e && e.ad) || t.Ba), (this.f = (e && e.bd) || t.Ba);
				},
				t.Dd = function(e, n, i, o) {
					function r() {
						t.v(s, function(e) {
							n.call(i || null, function(n) {
								if (e.once) {
									if (e.once.mg) return;
									(e.once.mg = !0), t.Za(a.P, e), a.P.length || a.b();
								}
								e.wc.call(e.context, n);
							});
						});
					}
					var s = e.P.slice(0),
						a = e;
					o && o.xn ? r() : nn(r);
				},
				K = function(t, e) {
					return function(n) {
						return n.wc == t && n.context == (e || null);
					};
				},
				t.Fd = function(e) {
					(this.J = this.I = window.Infinity),
						(this.M = this.L = -window.Infinity),
						t.v(e || [], this.extend, this);
				},
				t.Gd = function(e, n, i, o) {
					var r = new t.Fd();
					return (r.I = e), (r.J = n), (r.L = i), (r.M = o), r;
				},
				t.K = function(e, n, i) {
					if (e && (void 0 !== e.lat || void 0 !== e.lng))
						try {
							Ke(e), (n = e.lng), (e = e.lat), (i = !1);
						} catch (e) {
							t.cd(e);
						}
					(e -= 0),
						(n -= 0),
						i || ((e = t.eb(e, -90, 90)), 180 != n && (n = t.fb(n, -180, 180))),
						(this.lat = function() {
							return e;
						}),
						(this.lng = function() {
							return n;
						});
				},
				t.Id = function(e) {
					return t.uc(e.lat());
				},
				t.Jd = function(e) {
					return t.uc(e.lng());
				},
				Y = function(t, e) {
					return (e = Math.pow(10, e)), Math.round(t * e) / e;
				},
				t.Ld = function() {
					this.P = new X({ ad: (0, t.p)(this.ad, this), bd: (0, t.p)(this.bd, this) });
				},
				t.L = function(t) {
					this.data = t || [];
				},
				t.Md = function(t, e, n) {
					return null != (t = t.data[e]) ? t : n;
				},
				t.M = function(e, n, i) {
					return t.Md(e, n, i || 0);
				},
				t.N = function(e, n, i) {
					return t.Md(e, n, i || '');
				},
				t.O = function(t, e) {
					var n = t.data[e];
					return n || (n = t.data[e] = []), n;
				},
				t.Nd = function(e, n) {
					return t.dc(e.data, n);
				},
				t.Od = function(e, n, i) {
					return t.Nd(e, n)[i];
				},
				t.Pd = function(t, e) {
					return t.data[e] ? t.data[e].length : 0;
				},
				J = t.ma(),
				t.Ud = function(e) {
					try {
						return e instanceof t.K ? e : ((e = Ke(e)), new t.K(e.lat, e.lng));
					} catch (e) {
						throw t.bd('not a LatLng or LatLngLiteral', e);
					}
				},
				t.Vd = function(t) {
					return function() {
						return this.get(t);
					};
				},
				t.Wd = function(e, n) {
					return n
						? function(i) {
								try {
									this.set(e, n(i));
								} catch (i) {
									t.cd(t.bd('set' + t.Jb(e), i));
								}
							}
						: function(t) {
								this.set(e, t);
							};
				},
				t.Xd = function(e, n) {
					t.bb(n, function(n, i) {
						var o = t.Vd(n);
						(e['get' + t.Jb(n)] = o), i && ((i = t.Wd(n, i)), (e['set' + t.Jb(n)] = i));
					});
				},
				Z = t.ma(),
				t.Zd = function() {
					t.Ld.call(this);
				},
				t.ae = function(t) {
					return new tt(t);
				},
				tt = function(e) {
					t.Ld.call(this), (this.b = e);
				},
				t.ce = function(e) {
					this.b = (0, t.be)(e);
				},
				t.de = function(e) {
					this.b = (0, t.be)(e);
				},
				t.ee = function(e) {
					this.b = (0, t.be)(e);
				},
				t.fe = function(e) {
					this.b = t.Ud(e);
				},
				t.ge = function(e, n) {
					if (((e = e && t.Ud(e)), (n = n && t.Ud(n)), e)) {
						n = n || e;
						var i = t.eb(e.lat(), -90, 90),
							o = t.eb(n.lat(), -90, 90);
						(this.f = new U(i, o)),
							(e = e.lng()),
							360 <= (n = n.lng()) - e
								? (this.b = new z(-180, 180))
								: ((e = t.fb(e, -180, 180)), (n = t.fb(n, -180, 180)), (this.b = new z(e, n)));
					} else (this.f = new U(1, -1)), (this.b = new z(180, -180));
				},
				t.he = function(e, n, i, o) {
					return new t.ge(new t.K(e, n, !0), new t.K(i, o, !0));
				},
				t.je = function(e) {
					if (e instanceof t.ge) return e;
					try {
						return (e = sn(e)), t.he(e.south, e.west, e.north, e.east);
					} catch (e) {
						throw t.bd('not a LatLngBounds or LatLngBoundsLiteral', e);
					}
				},
				t.le = function(t) {
					(this.b = t || []), et(this);
				},
				et = function(t) {
					t.set('length', t.b.length);
				},
				nt = function(e) {
					if (e instanceof J) return e;
					try {
						return new t.fe(t.Ud(e));
					} catch (e) {}
					throw t.bd('not a Geometry or LatLng or LatLngLiteral object');
				},
				t.oe = function(t) {
					this.b = on(t);
				},
				t.re = function(t) {
					this.b = rn(t);
				},
				t.se = function(e) {
					(e = e || {}), (this.j = e.id), (this.b = null);
					try {
						this.b = e.geometry ? nt(e.geometry) : null;
					} catch (e) {
						t.cd(e);
					}
					this.f = e.properties || {};
				},
				t.ue = function(e) {
					this.b = [];
					try {
						this.b = ln(e);
					} catch (e) {
						t.cd(e);
					}
				},
				t.we = function(t) {
					this.b = cn(t);
				},
				t.xe = function() {
					(this.__gm = new t.z()), (this.l = null);
				},
				it = function() {
					this.b = {};
				},
				ot = function() {
					(this.b = {}), (this.j = {}), (this.f = {});
				},
				rt = t.ma(),
				st = t.ma(),
				at = function(e) {
					return ((e = e || {}).visible = t.ib(e.visible, !0)), e;
				},
				t.De = function(t) {
					return (t && t.radius) || 6378137;
				},
				lt = function(e) {
					return e instanceof t.le ? dn(e) : new t.le((0, t.be)(e));
				},
				ct = function(e) {
					var n;
					return (
						t.Ea(e) || e instanceof t.le
							? 0 == t.w(e)
								? (n = !0)
								: ((n = e instanceof t.le ? e.getAt(0) : e[0]), (n = t.Ea(n) || n instanceof t.le))
							: (n = !1),
						n ? (e instanceof t.le ? ut(dn)(e) : new t.le(t.ld(lt)(e))) : new t.le([ lt(e) ])
					);
				},
				ut = function(e) {
					return function(n) {
						if (!(n instanceof t.le)) throw t.bd('not an MVCArray');
						return (
							n.forEach(function(n, i) {
								try {
									e(n);
								} catch (n) {
									throw t.bd('at index ' + i, n);
								}
							}),
							n
						);
					};
				},
				t.Ie = function(e, n, i) {
					function o(e) {
						if (!e) throw t.bd('not a Feature');
						if ('Feature' != e.type) throw t.bd('type != "Feature"');
						var n = e.geometry;
						try {
							n = null == n ? null : r(n);
						} catch (e) {
							throw t.bd('in property "geometry"', e);
						}
						var o = e.properties || {};
						if (!t.lb(o)) throw t.bd('properties is not an Object');
						var s = i.idPropertyName;
						if (null != (e = s ? o[s] : e.id) && !t.kb(e) && !t.mb(e))
							throw t.bd((s || 'id') + ' is not a string or number');
						return { id: e, geometry: n, properties: o };
					}
					function r(e) {
						if (null == e) throw t.bd('is null');
						var n = (e.type + '').toLowerCase(),
							i = e.coordinates;
						try {
							switch (n) {
								case 'point':
									return new t.fe(l(i));
								case 'multipoint':
									return new t.ee(u(i));
								case 'linestring':
									return a(i);
								case 'multilinestring':
									return new t.oe(d(i));
								case 'polygon':
									return s(i);
								case 'multipolygon':
									return new t.we(p(i));
							}
						} catch (e) {
							throw t.bd('in property "coordinates"', e);
						}
						if ('geometrycollection' == n)
							try {
								return new t.ue(f(e.geometries));
							} catch (e) {
								throw t.bd('in property "geometries"', e);
							}
						throw t.bd('invalid type');
					}
					function s(e) {
						return new t.re(h(e));
					}
					function a(e) {
						return new t.ce(u(e));
					}
					function l(e) {
						return (e = c(e)), t.Ud({ lat: e[1], lng: e[0] });
					}
					if (!n) return [];
					i = i || {};
					var c = t.ld(t.rd),
						u = t.ld(l),
						d = t.ld(a),
						h = t.ld(function(e) {
							if (!(e = u(e)).length) throw t.bd('contains no elements');
							if (!e[0].b(e[e.length - 1])) throw t.bd('first and last positions are not equal');
							return new t.de(e.slice(0, -1));
						}),
						p = t.ld(s),
						f = t.ld(r),
						m = t.ld(o);
					if ('FeatureCollection' == n.type) {
						n = n.features;
						try {
							return t.hb(m(n), function(t) {
								return e.add(t);
							});
						} catch (n) {
							throw t.bd('in property "features"', n);
						}
					}
					if ('Feature' == n.type) return [ e.add(o(n)) ];
					throw t.bd('not a Feature or FeatureCollection');
				},
				t.Je = t.na('__gm'),
				dt = function(e) {
					this.b = new it();
					var n = this;
					t.x.addListenerOnce(e, 'addfeature', function() {
						t.G('data', function(t) {
							t.b(n, e, n.b);
						});
					});
				},
				ht = function(e) {
					((e = e || {}).clickable = t.ib(e.clickable, !0)),
						(e.visible = t.ib(e.visible, !0)),
						this.setValues(e),
						t.G('marker', t.Ba);
				},
				pt = function(e) {
					this.set('latLngs', new t.le([ new t.le() ])), this.setValues(at(e)), t.G('poly', t.Ba);
				},
				t.Ne = function(t) {
					(this.__gm = { set: null, Hd: null, Eb: { map: null, Yd: null } }), ht.call(this, t);
				},
				t.Oe = function(t) {
					pt.call(this, t);
				},
				t.Pe = function(t) {
					pt.call(this, t);
				},
				ft = function(e) {
					var n = this;
					(e = e || {}),
						this.setValues(e),
						(this.b = new ot()),
						t.x.forward(this.b, 'addfeature', this),
						t.x.forward(this.b, 'removefeature', this),
						t.x.forward(this.b, 'setgeometry', this),
						t.x.forward(this.b, 'setproperty', this),
						t.x.forward(this.b, 'removeproperty', this),
						(this.f = new dt(this.b)),
						this.f.bindTo('map', this),
						this.f.bindTo('style', this),
						t.v(t.Qe, function(e) {
							t.x.forward(n.f, e, n);
						}),
						(this.j = !1);
				},
				mt = function(e) {
					e.j ||
						((e.j = !0),
						t.G('drawing_impl', function(t) {
							t.ml(e);
						}));
				},
				gt = function(e) {
					return e
						? (t.Ga(e)
								? (((n = window.document.createElement('div')).style.overflow = 'auto'),
									(n.innerHTML = e))
								: 3 == e.nodeType ? (n = window.document.createElement('div')).appendChild(e) : (n = e),
							n)
						: null;
					var n;
				},
				t.Ve = function(e) {
					t.Ue && e && t.Ue.push(e);
				},
				vt = function(e, n) {
					(this.b = e),
						(this.f = n),
						e.addListener('map_changed', (0, t.p)(this.im, this)),
						this.bindTo('map', e),
						this.bindTo('disableAutoPan', e),
						this.bindTo('maxWidth', e),
						this.bindTo('position', e),
						this.bindTo('zIndex', e),
						this.bindTo('internalAnchor', e, 'anchor'),
						this.bindTo('internalContent', e, 'content'),
						this.bindTo('internalPixelOffset', e, 'pixelOffset');
				},
				bt = function(t, e, n, i) {
					n ? t.bindTo(e, n, i) : (t.unbind(e), t.set(e, void 0));
				},
				t.af = function(e) {
					function n() {
						r ||
							((r = !0),
							t.G('infowindow', function(t) {
								t.Qj(o);
							}));
					}
					window.setTimeout(function() {
						t.G('infowindow', t.Ba);
					}, 100);
					var i = !!(e = e || {}).b;
					delete e.b;
					var o = new vt(this, i),
						r = !1;
					t.x.addListenerOnce(this, 'anchor_changed', n),
						t.x.addListenerOnce(this, 'map_changed', n),
						this.setValues(e);
				},
				yt = function(t) {
					this.setValues(t);
				},
				wt = t.ma(),
				xt = t.ma(),
				kt = t.ma(),
				t.ff = function() {
					t.G('geocoder', t.Ba);
				},
				t.gf = function(e, n, i) {
					(this.H = null), this.set('url', e), this.set('bounds', t.pd(t.je)(n)), this.setValues(i);
				},
				$t = function(e, n) {
					t.mb(e) ? (this.set('url', e), this.setValues(n)) : this.setValues(e);
				},
				t.jf = function() {
					var e = this;
					t.G('layers', function(t) {
						t.b(e);
					});
				},
				Tt = function(e) {
					this.setValues(e);
					var n = this;
					t.G('layers', function(t) {
						t.f(n);
					});
				},
				Et = function() {
					var e = this;
					t.G('layers', function(t) {
						t.j(e);
					});
				},
				t.mf = function() {
					this.b = '';
				},
				t.nf = function(e) {
					var n = new t.mf();
					return (n.b = e), n;
				},
				t.of = t.ma(),
				t.qf = function() {
					(this.We = ''), (this.hj = t.pf), (this.b = null);
				},
				t.rf = function(e, n) {
					var i = new t.qf();
					return (i.We = e), (i.b = n), i;
				},
				t.sf = function(t, e) {
					e.parentNode && e.parentNode.insertBefore(t, e.nextSibling);
				},
				t.tf = function(t) {
					t && t.parentNode && t.parentNode.removeChild(t);
				},
				t.uf = function(t, e) {
					((t = t.style).width = e.width + e.j), (t.height = e.height + e.f);
				},
				t.vf = function(e) {
					return new t.I(e.offsetWidth, e.offsetHeight);
				},
				t.wf = function() {
					this.P = new X();
				},
				Ct = function(t, e, n, i, o) {
					(this.b = !!e),
						(this.node = null),
						(this.f = 0),
						(this.j = !1),
						(this.l = !n),
						t && this.setPosition(t, i),
						(this.depth = null != o ? o : this.f || 0),
						this.b && (this.depth *= -1);
				},
				t.yf = function(e) {
					(this.ki = e || 0), t.x.bind(this, 'forceredraw', this, this.C);
				},
				_t = function(t) {
					this.data = t || [];
				},
				St = function(t) {
					this.data = t || [];
				},
				jt = function(t) {
					this.data = t || [];
				},
				Nt = function(t) {
					this.data = t || [];
				},
				At = function(t, e, n, i) {
					Ct.call(this, t, e, n, null, i);
				},
				It = function(t) {
					this.data = t || [];
				},
				t.Ff = function(t) {
					this.data = t || [];
				},
				Lt = function(t) {
					this.data = t || [];
				},
				Dt = function(t) {
					this.data = t || [];
				},
				Ot = function(t) {
					this.data = t || [];
				},
				t.Jf = function(e) {
					return t.N(e, 0);
				},
				t.Kf = function(e) {
					return t.N(e, 1);
				},
				t.Tf = function(t) {
					return new It(t.data[2]);
				},
				t.Uf = function() {
					(this.b = new t.H(128, 128)), (this.j = 256 / 360), (this.l = 256 / (2 * Math.PI)), (this.f = !0);
				},
				Pt = function(t) {
					this.data = t || [];
				},
				Rt = function(t) {
					this.data = t || [];
				},
				Mt = function(t) {
					this.data = t || [];
				},
				qt = function(t) {
					this.data = t || [];
				},
				t.Zf = function(t, e, n) {
					return (t = t.fromLatLngToPoint(e)) && ((n = Math.pow(2, n)), (t.x *= n), (t.y *= n)), t;
				},
				t.ag = function(e) {
					for (var n; (n = e.firstChild); ) t.$f(n), e.removeChild(n);
				},
				t.$f = function(e) {
					e = new At(e);
					try {
						for (;;) t.x.clearInstanceListeners(e.next());
					} catch (e) {
						if (e !== t.bg) throw e;
					}
				},
				zt = function(t) {
					this.data = t || [];
				},
				t.dg = function(e, n) {
					var i = e.lat() + t.vc(n);
					90 < i && (i = 90);
					var o = e.lat() - t.vc(n);
					o < -90 && (o = -90), (n = Math.sin(n));
					var r = Math.cos(t.uc(e.lat()));
					return 90 == i || -90 == o || r < 1e-6
						? new t.ge(new t.K(o, -180), new t.K(i, 180))
						: ((n = t.vc(Math.asin(n / r))), new t.ge(new t.K(o, e.lng() - n), new t.K(i, e.lng() + n)));
				},
				Ut = function(e, n, i, o, r) {
					var s = t.N(t.Tf(t.Q), 7);
					(this.b = e), (this.f = o), (this.j = t.m(r) ? r : t.Pa());
					var a = s + '/csi?v=2&s=mapsapi3&v3v=' + t.N(new Ot(t.Q.data[36]), 0) + '&action=' + e;
					t.Ob(i, function(t, e) {
						a += '&' + (0, window.encodeURIComponent)(e) + '=' + (0, window.encodeURIComponent)(t);
					}),
						n && (a += '&e=' + n),
						(this.l = a);
				},
				t.gg = function(e, n) {
					var i = {};
					(i[n] = void 0), t.fg(e, i);
				},
				t.fg = function(e, n) {
					var i = '';
					t.Ob(
						n,
						function(e, n) {
							var o = (null != e ? e : t.Pa()) - this.j;
							i && (i += ','),
								(i += n + '.' + Math.round(o)),
								null == e &&
									window.performance &&
									window.performance.mark &&
									window.performance.mark('mapsapi:' + this.b + ':' + n);
						},
						e
					),
						(n = e.l + '&rt=' + i),
						(e.f.createElement('img').src = n),
						(e = t.Kc.__gm_captureCSI) && e(n);
				},
				t.hg = function(e, n) {
					var i = (n = n || {}).Cm || {};
					(o = t.Nd(t.Q, 12).join(',')) && (i.libraries = o);
					var o = t.N(t.Q, 6),
						r = new _t(t.Q.data[33]),
						s = [];
					return (
						o && s.push(o),
						t.v(r.data, function(e, n) {
							e &&
								t.v(e, function(t, e) {
									null != t && s.push(n + 1 + '_' + (e + 1) + '_' + t);
								});
						}),
						n.Fk && (s = s.concat(n.Fk)),
						new Ut(e, s.join(','), i, n.document || window.document, n.startTime)
					);
				},
				Ht = function(e, n) {
					t.xe.call(this),
						t.Ve(e),
						(this.__gm = new t.z()),
						(this.f = null),
						n && n.client && (this.f = t.ig[n.client] || null);
					var i = (this.controls = []);
					t.bb(t.jg, function(e, n) {
						i[n] = new t.le();
					}),
						(this.j = !0),
						(this.b = e),
						(this.m = !1),
						(this.__gm.ba = (n && n.ba) || new t.yd()),
						this.set('standAlone', !0),
						this.setPov(new t.Ad(0, 0, 1)),
						n && n.fd && !t.kb(n.fd.zoom) && (n.fd.zoom = t.kb(n.zoom) ? n.zoom : 1),
						this.setValues(n),
						null == this.getVisible() && this.setVisible(!0),
						t.x.addListenerOnce(
							this,
							'pano_changed',
							t.pb(function() {
								t.G(
									'marker',
									(0, t.p)(function(t) {
										t.b(this.__gm.ba, this);
									}, this)
								);
							})
						);
				},
				t.lg = function() {
					(this.l = []), (this.j = this.b = this.f = null);
				},
				Gt = function() {
					(this.f = t.hg('apiboot2', { startTime: t.mg })), t.gg(this.f, 'main'), (this.b = !1);
				},
				Ft = function() {
					var e = kn;
					e.b || ((e.b = !0), t.gg(e.f, 'firstmap'));
				},
				Bt = function(e, n, i, o) {
					t.yf.call(this),
						(this.m = n),
						(this.l = new t.Uf()),
						(this.B = i + '/maps/api/js/StaticMapService.GetMapImage'),
						(this.f = this.b = null),
						(this.j = o),
						this.set('div', e),
						this.set('loading', !0);
				},
				Wt = function(e) {
					var n = e.get('tilt') || t.w(e.get('styles'));
					return (e = e.get('mapTypeId')), n ? null : En[e];
				},
				Vt = function(t) {
					t.parentNode && t.parentNode.removeChild(t);
				},
				Qt = function(e, n) {
					var i = e.f;
					(i.onload = null),
						(i.onerror = null),
						e.get('size') &&
							(n &&
								(i.parentNode || e.b.appendChild(i),
								t.uf(i, e.get('size')),
								t.x.trigger(e, 'staticmaploaded'),
								e.j.set(t.Pa())),
							e.set('loading', !1));
				},
				Xt = function(t, e) {
					var n = t.f;
					e != n.src
						? (Vt(n),
							(n.onload = function() {
								Qt(t, !0);
							}),
							(n.onerror = function() {
								Qt(t, !1);
							}),
							(n.src = e))
						: !n.parentNode && e && t.b.appendChild(n);
				},
				Kt = function(e, n, i) {
					(this.R = n),
						(this.b = t.ae(new t.wc([]))),
						(this.B = new t.yd()),
						new t.le(),
						(this.D = new t.yd()),
						(this.F = new t.yd()),
						(this.l = new t.yd());
					var o = (this.ba = new t.yd());
					(o.b = function() {
						delete o.b,
							t.G(
								'marker',
								t.pb(function(t) {
									t.b(o, e);
								})
							);
					}),
						(this.j = new Ht(i, { visible: !1, enableCloseButton: !0, ba: o })),
						this.j.bindTo('reportErrorControl', e),
						(this.j.j = !1),
						(this.f = new t.lg()),
						(this.overlayLayer = null);
				},
				Yt = function(e, n) {
					var i = t.Pa();
					kn && Ft();
					var o = new t.wf(),
						r = n || {};
					r.noClear || t.ag(e);
					var s = void 0 === window.document ? null : window.document.createElement('div');
					s && e.appendChild && (e.appendChild(s), (s.style.width = s.style.height = '100%')),
						t.Je.call(this, new Kt(this, e, s)),
						t.m(r.mapTypeId) || (r.mapTypeId = 'roadmap'),
						this.setValues(r),
						(this.U = t.xg[15] && r.noControlsOrLogging),
						(this.mapTypes = new st()),
						(this.features = new t.z()),
						t.Ve(s),
						this.notify('streetView'),
						(e = t.vf(s));
					var a = null;
					t.Q &&
						Jt(r.useStaticMap, e) &&
						((a = new Bt(s, t.zg, t.N(t.Tf(t.Q), 9), new tt(null))),
						t.x.forward(a, 'staticmaploaded', this),
						a.set('size', e),
						a.bindTo('center', this),
						a.bindTo('zoom', this),
						a.bindTo('mapTypeId', this),
						a.bindTo('styles', this)),
						(this.overlayMapTypes = new t.le());
					var l = (this.controls = []);
					t.bb(t.jg, function(e, n) {
						l[n] = new t.le();
					});
					var c = this,
						u = !0;
					t.G('map', function(t) {
						c.getDiv() && s && t.f(c, r, s, a, u, i, o);
					}),
						(u = !1),
						(this.data = new ft({ map: this }));
				},
				Jt = function(e, n) {
					return t.m(e) ? !!e : (e = n.width) * (n = n.height) <= 384e3 && e <= 800 && n <= 800;
				},
				Zt = function() {
					t.G('maxzoom', t.Ba);
				},
				te = function(e, n) {
					!e || t.mb(e) || t.kb(e) ? (this.set('tableId', e), this.setValues(n)) : this.setValues(e);
				},
				t.Dg = t.ma(),
				t.Eg = function(e) {
					this.setValues(at(e)), t.G('poly', t.Ba);
				},
				t.Fg = function(e) {
					this.setValues(at(e)), t.G('poly', t.Ba);
				},
				ee = function() {
					this.b = null;
				},
				t.Hg = function() {
					this.b = null;
				},
				ne = function(t, e) {
					(this.b = t), (this.f = e || 0);
				},
				ie = function() {
					var t = window.navigator.userAgent;
					(this.l = t),
						(this.b = this.type = 0),
						(this.version = new ne(0)),
						(this.m = new ne(0)),
						(t = t.toLowerCase());
					for (var e = 1; e < 8; ++e) {
						var n = $n[e];
						if (-1 != t.indexOf(n)) {
							this.type = e;
							var i = new RegExp(n + '[ /]?([0-9]+).?([0-9]+)?').exec(t);
							i &&
								(this.version = new ne(
									(0, window.parseInt)(i[1], 10),
									(0, window.parseInt)(i[2] || '0', 10)
								));
							break;
						}
					}
					for (
						7 == this.type &&
							(i = (e = /^Mozilla\/.*Gecko\/.*[Minefield|Shiretoko][ \/]?([0-9]+).?([0-9]+)?/).exec(
								this.l
							)) &&
							((this.type = 5),
							(this.version = new ne(
								(0, window.parseInt)(i[1], 10),
								(0, window.parseInt)(i[2] || '0', 10)
							))),
							6 == this.type &&
								(e = (e = /rv:([0-9]{2,}.?[0-9]+)/).exec(this.l)) &&
								((this.type = 1), (this.version = new ne((0, window.parseInt)(e[1], 10)))),
							e = 1;
						e < 7;
						++e
					)
						if (((n = Tn[e]), -1 != t.indexOf(n))) {
							this.b = e;
							break;
						}
					(5 != this.b && 6 != this.b && 2 != this.b) ||
						((e = /OS (?:X )?(\d+)[_.]?(\d+)/.exec(this.l)) &&
							(this.m = new ne((0, window.parseInt)(e[1], 10), (0, window.parseInt)(e[2] || '0', 10)))),
						4 == this.b &&
							(e = /Android (\d+)\.?(\d+)?/.exec(this.l)) &&
							(this.m = new ne((0, window.parseInt)(e[1], 10), (0, window.parseInt)(e[2] || '0', 10))),
						(this.j = 5 == this.type || 7 == this.type),
						(this.f = 4 == this.type || 3 == this.type),
						(this.D = 0),
						this.j && (i = /\brv:\s*(\d+\.\d+)/.exec(t)) && (this.D = (0, window.parseFloat)(i[1])),
						(this.B = window.document.compatMode || ''),
						(this.C =
							1 == this.b || 2 == this.b || (3 == this.b && -1 == t.toLowerCase().indexOf('mobile')));
				},
				oe = t.na('b'),
				re = function() {
					var e,
						n = window.document;
					(this.f = t.R),
						(this.b = se(n, [ 'transform', 'WebkitTransform', 'MozTransform', 'msTransform' ])),
						(this.C = se(n, [ 'WebkitUserSelect', 'MozUserSelect', 'msUserSelect' ])),
						(this.m = se(n, [
							'transition',
							'WebkitTransition',
							'MozTransition',
							'OTransition',
							'msTransition'
						]));
					t: {
						for (
							var i,
								o = [
									'-webkit-linear-gradient',
									'-moz-linear-gradient',
									'-o-linear-gradient',
									'-ms-linear-gradient'
								],
								r = n.createElement('div'),
								s = 0;
							(i = o[s]);
							++s
						)
							try {
								if (
									((r.style.background = i + '(left, #000, #fff)'),
									-1 != r.style.background.indexOf(i))
								) {
									e = i;
									break t;
								}
							} catch (e) {}
						e = null;
					}
					(this.B = e),
						(this.l = 'string' == typeof n.documentElement.style.opacity),
						(e = (n = window.document.getElementsByTagName('script')[0]).style.color),
						(n.style.color = '');
					try {
						n.style.color = 'rgba(0, 0, 0, 0.5)';
					} catch (e) {}
					(o = n.style.color != e), (n.style.color = e), (this.j = o);
				},
				se = function(t, e) {
					for (var n, i = 0; (n = e[i]); ++i) if ('string' == typeof t.documentElement.style[n]) return n;
					return null;
				},
				t.Tg = function(t, e) {
					(this.size = new Z()), (this.b = t), (this.heading = e);
				},
				t.Ug = function(e) {
					var n = this;
					(this.tileSize = e.tileSize || new t.I(256, 256)),
						(this.name = e.name),
						(this.alt = e.alt),
						(this.minZoom = e.minZoom),
						(this.maxZoom = e.maxZoom),
						(this.j = (0, t.p)(e.getTileUrl, e)),
						(this.b = new t.yd()),
						(this.f = null),
						this.set('opacity', e.opacity),
						t.G('map', function(e) {
							var i = (n.f = e.b),
								o = n.tileSize || new t.I(256, 256);
							n.b.forEach(function(e) {
								var r = e.__gmimt,
									s = r.Y,
									a = r.zoom,
									l = n.j(s, a);
								r.Hb = i(s, a, o, e, l, function() {
									return t.x.trigger(e, 'load');
								});
							});
						});
				},
				ae = function(t, e) {
					null != t.style.opacity
						? (t.style.opacity = e)
						: (t.style.filter = e && 'alpha(opacity=' + Math.round(100 * e) + ')');
				},
				le = function(t) {
					return 'number' == typeof (t = t.get('opacity')) ? t : 1;
				},
				t.Xg = function() {
					t.Xg.Ge(this, 'constructor');
				},
				t.Yg = function(e, n) {
					t.Yg.Ge(this, 'constructor'),
						this.set('styles', e),
						(e = n || {}),
						(this.f = e.baseMapTypeId || 'roadmap'),
						(this.minZoom = e.minZoom),
						(this.maxZoom = e.maxZoom || 20),
						(this.name = e.name),
						(this.alt = e.alt),
						(this.projection = null),
						(this.tileSize = new t.I(256, 256));
				},
				t.Zg = function(e, n) {
					t.md(F, 'container is not a Node')(e),
						this.setValues(n),
						t.G(
							'controls',
							(0, t.p)(function(t) {
								t.Bl(this, e);
							}, this)
						);
				},
				ce = t.na('b'),
				ue = function(t, e, n) {
					for (var i = Array(e.length), o = 0, r = e.length; o < r; ++o) i[o] = e.charCodeAt(o);
					for (i.unshift(n), t = t.b, n = e = 0, o = i.length; n < o; ++n) (e *= 1729), (e += i[n]), (e %= t);
					return e;
				},
				de = function() {
					var e = t.M(new Lt(t.Q.data[4]), 0),
						n = new ce(131071),
						i = (0, window.unescape)('%26%74%6F%6B%65%6E%3D');
					return function(t) {
						var o = (t = t.replace(jn, '%27')) + i;
						return Sn || (Sn = /(?:https?:\/\/[^\/]+)?(.*)/), (t = Sn.exec(t)), o + ue(n, t && t[1], e);
					};
				},
				he = function() {
					var t = new ce(2147483647);
					return function(e) {
						return ue(t, e, 0);
					};
				},
				pe = function(e) {
					for (var n = e.split('.'), i = window, o = window, r = 0; r < n.length; r++)
						if (!(i = (o = i)[n[r]])) throw t.bd(e + ' is not a function');
					return function() {
						i.apply(o);
					};
				},
				fe = function() {
					for (var t in Object.prototype)
						window.console &&
							window.console.error(
								'This site adds property <' +
									t +
									'> to Object.prototype. Extending Object.prototype breaks JavaScript for..in loops, which are used heavily in Google Maps API v3.'
							);
				},
				me = function(t) {
					return (
						(t = ('version' in t)) &&
							window.console &&
							window.console.error(
								'You have included the Google Maps API multiple times on this page. This may cause unexpected errors.'
							),
						t
					);
				},
				t.qa = [],
				ge =
					'function' == typeof Object.defineProperties
						? Object.defineProperty
						: function(t, e, n) {
								if (n.get || n.set) throw new TypeError('ES3 does not support getters and setters.');
								t != Array.prototype && t != Object.prototype && (t[e] = n.value);
							},
				ye = ve =
					'undefined' != typeof window && window === this
						? this
						: void 0 !== window.global && null != window.global ? window.global : this,
				we = [ 'Array', 'prototype', 'fill' ],
				xe = be = 0;
			xe < we.length - 1;
			xe++
		) {
			var Te = we[xe];
			Te in ye || (ye[Te] = {}), (ye = ye[Te]);
		}
		var Ee,
			Ce,
			_e = we[we.length - 1],
			Se = ye[_e],
			je =
				Se ||
				function(t, e, n) {
					var i = this.length || 0;
					for (
						e < 0 && (e = Math.max(0, i + e)),
							(null == n || i < n) && (n = i),
							(n = Number(n)) < 0 && (n = Math.max(0, i + n)),
							e = Number(e || 0);
						e < n;
						e++
					)
						this[e] = t;
					return this;
				};
		je != Se && null != je && ge(ye, _e, { configurable: !0, writable: !0, value: je }),
			(t.Kc = this),
			(ke = 'closure_uid_' + ((1e9 * Math.random()) >>> 0)),
			($e = 0),
			(t.x = {}),
			(Ee = void 0 !== window.navigator && -1 != window.navigator.userAgent.toLowerCase().indexOf('msie')),
			(Ce = {}),
			(t.x.addListener = function(t, e, n) {
				return new p(t, e, n, 0);
			}),
			(t.x.hasListeners = function(e, n) {
				return !!(n = (e = e.__e3_) && e[n]) && !t.db(n);
			}),
			(t.x.removeListener = function(t) {
				t && t.remove();
			}),
			(t.x.clearListeners = function(e, n) {
				t.bb(u(e, n), function(t, e) {
					e && e.remove();
				});
			}),
			(t.x.clearInstanceListeners = function(e) {
				t.bb(u(e), function(t, e) {
					e && e.remove();
				});
			}),
			(t.x.trigger = function(e, n, i) {
				if (t.x.hasListeners(e, n)) {
					var o,
						r = t.$a(arguments, 2),
						s = u(e, n);
					for (o in s) {
						var a = s[o];
						a && a.b.apply(a.f, r);
					}
				}
			}),
			(t.x.addDomListener = function(t, e, n, i) {
				if (t.addEventListener) {
					var o = i ? 4 : 1;
					t.addEventListener(e, n, i), (n = new p(t, e, n, o));
				} else
					t.attachEvent
						? ((n = new p(t, e, n, 2)), t.attachEvent('on' + e, f(n)))
						: ((t['on' + e] = n), (n = new p(t, e, n, 3)));
				return n;
			}),
			(t.x.addDomListenerOnce = function(e, n, i, o) {
				var r = t.x.addDomListener(
					e,
					n,
					function() {
						return r.remove(), i.apply(this, arguments);
					},
					o
				);
				return r;
			}),
			(t.x.T = function(e, n, i, o) {
				return t.x.addDomListener(e, n, d(i, o));
			}),
			(t.x.bind = function(e, n, i, o) {
				return t.x.addListener(e, n, (0, t.p)(o, i));
			}),
			(t.x.addListenerOnce = function(e, n, i) {
				var o = t.x.addListener(e, n, function() {
					return o.remove(), i.apply(this, arguments);
				});
				return o;
			}),
			(t.x.forward = function(e, n, i) {
				return t.x.addListener(e, n, h(n, i));
			}),
			(t.x.Ha = function(e, n, i, o) {
				return t.x.addDomListener(e, n, h(n, i, !o));
			}),
			(t.x.Xh = function() {
				var e,
					n = Ce;
				for (e in n) n[e].remove();
				(Ce = {}), (n = t.Kc.CollectGarbage) && n();
			}),
			(t.x.Qm = function() {
				Ee && t.x.addDomListener(window, 'unload', t.x.Xh);
			});
		var Ne = 0;
		(p.prototype.remove = function() {
			if (this.f) {
				switch (this.m) {
					case 1:
						this.f.removeEventListener(this.j, this.b, !1);
						break;
					case 4:
						this.f.removeEventListener(this.j, this.b, !0);
						break;
					case 2:
						this.f.detachEvent('on' + this.j, this.l);
						break;
					case 3:
						this.f['on' + this.j] = null;
				}
				delete c(this.f, this.j)[this.id], (this.l = this.b = this.f = null), delete Ce[this.id];
			}
		}),
			(t.k = t.z.prototype),
			(t.k.get = function(e) {
				var n = g(this);
				if (((n = l(n, (e += ''))), t.m(n))) {
					if (n) {
						(e = n.Ya), (n = n.zc);
						var i = 'get' + t.Jb(e);
						return n[i] ? n[i]() : n.get(e);
					}
					return this[e];
				}
			}),
			(t.k.set = function(e, n) {
				var i = g(this),
					o = l(i, (e += ''));
				o
					? ((e = o.Ya), (o = o.zc)[(i = 'set' + t.Jb(e))] ? o[i](n) : o.set(e, n))
					: ((this[e] = n), (i[e] = null), m(this, e));
			}),
			(t.k.notify = function(t) {
				var e = g(this);
				(e = l(e, (t += ''))) ? e.zc.notify(e.Ya) : m(this, t);
			}),
			(t.k.setValues = function(e) {
				for (var n in e) {
					var i = e[n],
						o = 'set' + t.Jb(n);
					this[o] ? this[o](i) : this.set(n, i);
				}
			}),
			(t.k.setOptions = t.z.prototype.setValues),
			(t.k.changed = t.ma());
		var Ae = {};
		(t.z.prototype.bindTo = function(e, n, i, o) {
			(e += ''), (i = (i || e) + ''), this.unbind(e);
			var r = { zc: this, Ya: e },
				s = { zc: n, Ya: i, kg: r };
			(g(this)[e] = s), (v(n, i)[t.ab(r)] = r), o || m(this, e);
		}),
			(t.z.prototype.unbind = function(e) {
				var n = g(this),
					i = n[e];
				i && (i.kg && delete v(i.zc, i.Ya)[t.ab(i.kg)], (this[e] = this.get(e)), (n[e] = null));
			}),
			(t.z.prototype.unbindAll = function() {
				var e,
					n = (0, t.p)(this.unbind, this),
					i = g(this);
				for (e in i) n(e);
			}),
			(t.z.prototype.addListener = function(e, n) {
				return t.x.addListener(this, e, n);
			}),
			(t.ph = { ROADMAP: 'roadmap', SATELLITE: 'satellite', HYBRID: 'hybrid', TERRAIN: 'terrain' }),
			(t.jg = {
				TOP_LEFT: 1,
				TOP_CENTER: 2,
				TOP: 2,
				TOP_RIGHT: 3,
				LEFT_CENTER: 4,
				LEFT_TOP: 5,
				LEFT: 5,
				LEFT_BOTTOM: 6,
				RIGHT_TOP: 7,
				RIGHT: 7,
				RIGHT_CENTER: 8,
				RIGHT_BOTTOM: 9,
				BOTTOM_LEFT: 10,
				BOTTOM_CENTER: 11,
				BOTTOM: 11,
				BOTTOM_RIGHT: 12,
				CENTER: 13
			}),
			(b.prototype.get = function() {
				var t;
				return 0 < this.f ? (this.f--, (t = this.b), (this.b = t.next), (t.next = null)) : (t = this.j()), t;
			});
		t.Pb[' '] = t.Ba;
		t: {
			var Ie = t.Kc.navigator;
			if (Ie) {
				var Le = Ie.userAgent;
				if (Le) {
					t.Ra = Le;
					break t;
				}
			}
			t.Ra = '';
		}
		var De = { mo: 'Point', ko: 'LineString', POLYGON: 'Polygon' };
		(t.Tb.prototype.heading = t.oa('f')),
			(t.Tb.prototype.b = t.oa('j')),
			(t.Tb.prototype.toString = function() {
				return this.f + ',' + this.j;
			}),
			(t.uh = new t.Tb());
		var Oe,
			Pe = {
				CIRCLE: 0,
				FORWARD_CLOSED_ARROW: 1,
				FORWARD_OPEN_ARROW: 2,
				BACKWARD_CLOSED_ARROW: 3,
				BACKWARD_OPEN_ARROW: 4
			},
			Re = new b(
				function() {
					return new k();
				},
				function(t) {
					t.reset();
				},
				100
			);
		(x.prototype.add = function(t, e) {
			var n = Re.get();
			n.set(t, e), this.f ? (this.f.next = n) : (this.b = n), (this.f = n);
		}),
			(x.prototype.remove = function() {
				var t = null;
				return this.b && ((t = this.b), (this.b = this.b.next), this.b || (this.f = null), (t.next = null)), t;
			}),
			(k.prototype.set = function(t, e) {
				(this.wc = t), (this.b = e), (this.next = null);
			}),
			(k.prototype.reset = function() {
				this.next = this.b = this.wc = null;
			}),
			(t.xh = t.ic('d', void 0)),
			(t.yh = t.ic('f', void 0)),
			(t.S = t.nc()),
			(t.zh = t.jc('i', void 0)),
			(t.Ah = new t.gc('i', 3, void 0, void 0)),
			(t.Bh = new t.gc('j', 3, '', void 0)),
			(t.Ch = t.ic('u', void 0)),
			(t.Dh = t.jc('u', void 0)),
			(t.Eh = new t.gc('u', 3, void 0, void 0)),
			(t.Fh = t.oc()),
			(t.T = t.pc()),
			(t.U = t.qc()),
			(t.Gh = new t.gc('e', 3, void 0, void 0)),
			(t.V = t.ic('s', void 0)),
			(t.Hh = t.jc('s', void 0)),
			(t.Ih = new t.gc('s', 3, void 0, void 0)),
			(t.Jh = t.ic('x', void 0)),
			(t.Kh = t.jc('x', void 0)),
			(t.Lh = new t.gc('x', 3, void 0, void 0)),
			(t.Mh = new t.gc('y', 3, void 0, void 0)),
			(t.wc.prototype.Sa = t.ra(0)),
			(t.wc.prototype.forEach = function(e, n) {
				t.v(this.b, function(t, i) {
					e.call(n, t, i);
				});
			}),
			(S.f = void 0),
			(S.b = function() {
				return S.f ? S.f : (S.f = new S());
			}),
			(S.prototype.Za = function(e, n) {
				var i = this,
					o = i.m;
				I(i.j, function(r) {
					for (
						var s = r.b[e] || [],
							a = r.l[e] || [],
							l = (o[e] = t.Ub(s.length, function() {
								delete o[e], n(r.f);
								for (var t = i.f[e], s = t ? t.length : 0, l = 0; l < s; ++l) t[l](i.b[e]);
								for (delete i.f[e], l = 0, t = a.length; l < t; ++l) (s = a[l]), o[s] && o[s]();
							})),
							c = 0,
							u = s.length;
						c < u;
						++c
					)
						i.b[s[c]] && l();
				});
			}),
			(t.Nh = new L()),
			(Oe = /'/g),
			(L.prototype.b = function(t, e) {
				var n = [];
				return D(t, e, n), n.join('&').replace(Oe, '%27');
			});
		var Me,
			qe,
			ze,
			Ue,
			He = t.Mb;
		(t.Ph = t.Sb('Opera')),
			(t.Qh = t.rc()),
			(t.Rh = t.Sb('Edge')),
			(t.Sh = t.Sb('Gecko') && !(t.Sa() && !t.Sb('Edge')) && !(t.Sb('Trident') || t.Sb('MSIE')) && !t.Sb('Edge')),
			(t.Th = t.Sa() && !t.Sb('Edge')),
			(t.Uh = t.Sb('Macintosh')),
			(t.Vh = t.Sb('Windows')),
			(t.Wh = t.Sb('Linux') || t.Sb('CrOS')),
			(t.Xh = t.Sb('Android')),
			(t.Yh = t.$b()),
			(t.Zh = t.Sb('iPad')),
			(t.$h = t.Sb('iPod'));
		t: {
			var Ge = '',
				Fe = ((Ue = t.Ra),
				t.Sh
					? /rv\:([^\);]+)(\)|;)/.exec(Ue)
					: t.Rh
						? /Edge\/([\d\.]+)/.exec(Ue)
						: t.Qh
							? /\b(?:MSIE|rv)[: ]([^\);]+)(\)|;)/.exec(Ue)
							: t.Th ? /WebKit\/(\S+)/.exec(Ue) : t.Ph ? /(?:Version)[ \/]?(\S+)/.exec(Ue) : void 0);
			if ((Fe && (Ge = Fe ? Fe[1] : ''), t.Qh)) {
				var Be = q();
				if (null != Be && Be > (0, window.parseFloat)(Ge)) {
					qe = String(Be);
					break t;
				}
			}
			qe = Ge;
		}
		(t.Sc = qe), (ze = {});
		var We = t.Kc.document;
		(t.ei = We && t.Qh ? q() || ('CSS1Compat' == We.compatMode ? (0, window.parseInt)(t.Sc, 10) : 5) : void 0),
			(t.k = z.prototype),
			(t.k.isEmpty = function() {
				return 360 == this.b - this.f;
			}),
			(t.k.intersects = function(e) {
				var n = this.b,
					i = this.f;
				return (
					!this.isEmpty() &&
					!e.isEmpty() &&
					(t.Vc(this)
						? t.Vc(e) || e.b <= this.f || e.f >= n
						: t.Vc(e) ? e.b <= i || e.f >= n : e.b <= i && e.f >= n)
				);
			}),
			(t.k.contains = function(e) {
				-180 == e && (e = 180);
				var n = this.b,
					i = this.f;
				return t.Vc(this) ? (n <= e || e <= i) && !this.isEmpty() : n <= e && e <= i;
			}),
			(t.k.extend = function(e) {
				this.contains(e) ||
					(this.isEmpty()
						? (this.b = this.f = e)
						: t.Yc(e, this.b) < t.Yc(this.f, e) ? (this.b = e) : (this.f = e));
			}),
			(t.k.ub = function() {
				var e = (this.b + this.f) / 2;
				return t.Vc(this) && (e = t.fb(e + 180, -180, 180)), e;
			}),
			(t.k = U.prototype),
			(t.k.isEmpty = function() {
				return this.f > this.b;
			}),
			(t.k.intersects = function(t) {
				var e = this.f,
					n = this.b;
				return e <= t.f ? t.f <= n && t.f <= t.b : e <= t.b && e <= n;
			}),
			(t.k.contains = function(t) {
				return t >= this.f && t <= this.b;
			}),
			(t.k.extend = function(t) {
				this.isEmpty() ? (this.b = this.f = t) : t < this.f ? (this.f = t) : t > this.b && (this.b = t);
			}),
			(t.k.ub = function() {
				return (this.b + this.f) / 2;
			}),
			t.t(H, Error),
			(t.Mc.m = function() {
				if (-1 != String(t.Kc.Promise).indexOf('[native code]')) {
					var e = t.Kc.Promise.resolve(void 0);
					t.Mc.b = function() {
						e.then(t.Mc.f);
					};
				} else
					t.Mc.b = function() {
						R();
					};
			}),
			(t.Mc.B = function(e) {
				t.Mc.b = function() {
					R(), e && e(t.Mc.f);
				};
			}),
			(t.Mc.j = !1),
			(t.Mc.l = new x()),
			(t.Mc.f = function() {
				for (var e; (e = t.Mc.l.remove()); ) {
					try {
						e.wc.call(e.b);
					} catch (e) {
						P(e);
					}
					var n = Re;
					n.m(e), n.f < n.l && (n.f++, (e.next = n.b), (n.b = e));
				}
				t.Mc.j = !1;
			}),
			(t.gi = t.Sb('Firefox')),
			(t.hi = t.$b() || t.Sb('iPod')),
			(t.ii = t.Sb('iPad')),
			(t.ji = t.Sb('Android') && !(_() || t.Sb('Firefox') || t.Sb('Opera') || t.Sb('Silk'))),
			(t.ki = _()),
			(t.li = t.tc() && !(t.$b() || t.Sb('iPad') || t.Sb('iPod')));
		var Ve,
			Qe,
			Xe = {
				main: [],
				common: [ 'main' ],
				util: [ 'common' ],
				adsense: [ 'main' ],
				controls: [ 'util' ],
				data: [ 'util' ],
				directions: [ 'util', 'geometry' ],
				distance_matrix: [ 'util' ],
				drawing: [ 'main' ],
				drawing_impl: [ 'controls' ],
				elevation: [ 'util', 'geometry' ],
				geocoder: [ 'util' ],
				geojson: [ 'main' ],
				imagery_viewer: [ 'main' ],
				geometry: [ 'main' ],
				infowindow: [ 'util' ],
				kml: [ 'onion', 'util', 'map' ],
				layers: [ 'map' ],
				map: [ 'common' ],
				marker: [ 'util' ],
				maxzoom: [ 'util' ],
				onion: [ 'util', 'map' ],
				overlay: [ 'common' ],
				panoramio: [ 'main' ],
				places: [ 'main' ],
				places_impl: [ 'controls' ],
				poly: [ 'util', 'map', 'geometry' ],
				search: [ 'main' ],
				search_impl: [ 'onion' ],
				stats: [ 'util' ],
				streetview: [ 'util', 'geometry' ],
				usage: [ 'util' ],
				visualization: [ 'main' ],
				visualization_impl: [ 'onion' ],
				weather: [ 'main' ],
				zombie: [ 'main' ]
			};
		(t.rd = t.md(t.kb, 'not a number')),
			(Ve = t.od(t.rd, function(e) {
				if ((0, window.isNaN)(e)) throw t.bd('NaN is not an accepted value');
				return e;
			})),
			(t.ni = t.md(t.mb, 'not a string')),
			(Qe = t.md(t.nb, 'not a boolean')),
			(t.pi = t.pd(t.rd)),
			(t.qi = t.pd(t.ni)),
			(t.ri = t.pd(Qe));
		var Ke = t.hd({ lat: t.rd, lng: t.rd }, !0);
		(t.si = new t.H(0, 0)),
			(t.H.prototype.toString = function() {
				return '(' + this.x + ', ' + this.y + ')';
			}),
			(t.H.prototype.b = function(t) {
				return !!t && t.x == this.x && t.y == this.y;
			}),
			(t.H.prototype.equals = t.H.prototype.b),
			(t.H.prototype.round = function() {
				(this.x = Math.round(this.x)), (this.y = Math.round(this.y));
			}),
			(t.H.prototype.Kd = t.ra(1)),
			(t.ti = new t.I(0, 0)),
			(t.I.prototype.toString = function() {
				return '(' + this.width + ', ' + this.height + ')';
			}),
			(t.I.prototype.b = function(t) {
				return !!t && t.width == this.width && t.height == this.height;
			}),
			(t.I.prototype.equals = t.I.prototype.b);
		var Ye = t.Kc.google.maps,
			Je = S.b(),
			Ze = (0, t.p)(Je.Za, Je);
		(Ye.__gjsload__ = Ze),
			t.bb(Ye.modules, Ze),
			delete Ye.modules,
			(t.yd.prototype.remove = function(e) {
				var n = this.f,
					i = this.j(e);
				n[i] && (delete n[i], t.x.trigger(this, 'remove', e), this.onRemove && this.onRemove(e));
			}),
			(t.yd.prototype.contains = function(t) {
				return !!this.f[this.j(t)];
			}),
			(t.yd.prototype.forEach = function(t) {
				var e,
					n = this.f;
				for (e in n) t.call(this, n[e]);
			});
		var tn = t.hd({ zoom: t.pd(Ve), heading: Ve, pitch: Ve }),
			en = t.hd({ source: t.ni, webUrl: t.qi, iosDeepLinkId: t.qi });
		(X.prototype.addListener = function(e, n, i) {
			i = i ? { mg: !1 } : null;
			var o,
				r = !this.P.length;
			o = this.P;
			var s = a(o, K(e, n));
			return (
				(o = s < 0 ? null : t.Ga(o) ? o.charAt(s) : o[s])
					? (o.once = o.once && i)
					: this.P.push({ wc: e, context: n || null, once: i }),
				r && this.f(),
				e
			);
		}),
			(X.prototype.addListenerOnce = function(t, e) {
				return this.addListener(t, e, !0), t;
			}),
			(X.prototype.removeListener = function(e, n) {
				if (this.P.length) {
					var i = this.P;
					0 <= (e = a(i, K(e, n))) && t.Ya(i, e), this.P.length || this.b();
				}
			});
		var nn = t.Mc;
		(t.Fd.prototype.isEmpty = function() {
			return !(this.I < this.L && this.J < this.M);
		}),
			(t.Fd.prototype.extend = function(t) {
				t &&
					((this.I = Math.min(this.I, t.x)),
					(this.L = Math.max(this.L, t.x)),
					(this.J = Math.min(this.J, t.y)),
					(this.M = Math.max(this.M, t.y)));
			}),
			(t.Fd.prototype.getCenter = function() {
				return new t.H((this.I + this.L) / 2, (this.J + this.M) / 2);
			}),
			(t.Ji = t.Gd(-window.Infinity, -window.Infinity, window.Infinity, window.Infinity)),
			(t.Ki = t.Gd(0, 0, 0, 0)),
			(t.K.prototype.toString = function() {
				return '(' + this.lat() + ', ' + this.lng() + ')';
			}),
			(t.K.prototype.toJSON = function() {
				return { lat: this.lat(), lng: this.lng() };
			}),
			(t.K.prototype.b = function(e) {
				return !!e && t.gb(this.lat(), e.lat()) && t.gb(this.lng(), e.lng());
			}),
			(t.K.prototype.equals = t.K.prototype.b),
			(t.K.prototype.toUrlValue = function(e) {
				return (e = t.m(e) ? e : 6), Y(this.lat(), e) + ',' + Y(this.lng(), e);
			}),
			(t.k = t.Ld.prototype),
			(t.k.bd = t.ma()),
			(t.k.ad = t.ma()),
			(t.k.addListener = function(t, e) {
				return this.P.addListener(t, e);
			}),
			(t.k.addListenerOnce = function(t, e) {
				return this.P.addListenerOnce(t, e);
			}),
			(t.k.removeListener = function(t, e) {
				return this.P.removeListener(t, e);
			}),
			(t.k.notify = function(e) {
				t.Dd(
					this.P,
					function(t) {
						t(this.get());
					},
					this,
					e
				);
			}),
			(t.L.prototype.Uh = t.ra(2)),
			(t.be = t.ld(t.Ud)),
			t.t(t.Zd, t.Ld),
			(t.Zd.prototype.set = function(t) {
				this.Ih(t), this.notify();
			}),
			t.t(tt, t.Zd),
			(tt.prototype.get = t.oa('b')),
			(tt.prototype.Ih = t.na('b')),
			t.t(t.ce, J),
			(t.k = t.ce.prototype),
			(t.k.getType = t.pa('LineString')),
			(t.k.getLength = function() {
				return this.b.length;
			}),
			(t.k.getAt = function(t) {
				return this.b[t];
			}),
			(t.k.getArray = function() {
				return this.b.slice();
			}),
			(t.k.forEachLatLng = function(t) {
				this.b.forEach(t);
			});
		var on = t.ld(t.jd(t.ce, 'google.maps.Data.LineString', !0));
		t.t(t.de, J),
			(t.k = t.de.prototype),
			(t.k.getType = t.pa('LinearRing')),
			(t.k.getLength = function() {
				return this.b.length;
			}),
			(t.k.getAt = function(t) {
				return this.b[t];
			}),
			(t.k.getArray = function() {
				return this.b.slice();
			}),
			(t.k.forEachLatLng = function(t) {
				this.b.forEach(t);
			});
		var rn = t.ld(t.jd(t.de, 'google.maps.Data.LinearRing', !0));
		t.t(t.ee, J),
			(t.k = t.ee.prototype),
			(t.k.getType = t.pa('MultiPoint')),
			(t.k.getLength = function() {
				return this.b.length;
			}),
			(t.k.getAt = function(t) {
				return this.b[t];
			}),
			(t.k.getArray = function() {
				return this.b.slice();
			}),
			(t.k.forEachLatLng = function(t) {
				this.b.forEach(t);
			}),
			t.t(t.fe, J),
			(t.fe.prototype.getType = t.pa('Point')),
			(t.fe.prototype.forEachLatLng = function(t) {
				t(this.b);
			}),
			(t.fe.prototype.get = t.oa('b')),
			(t.k = t.ge.prototype),
			(t.k.getCenter = function() {
				return new t.K(this.f.ub(), this.b.ub());
			}),
			(t.k.toString = function() {
				return '(' + this.getSouthWest() + ', ' + this.getNorthEast() + ')';
			}),
			(t.k.toJSON = function() {
				return { south: this.f.f, west: this.b.b, north: this.f.b, east: this.b.f };
			}),
			(t.k.toUrlValue = function(t) {
				var e = this.getSouthWest(),
					n = this.getNorthEast();
				return [ e.toUrlValue(t), n.toUrlValue(t) ].join();
			}),
			(t.k.Ei = function(e) {
				if (!e) return !1;
				e = t.je(e);
				var n = this.f,
					i = e.f;
				return (
					(n.isEmpty() ? i.isEmpty() : Math.abs(i.f - n.f) + Math.abs(n.b - i.b) <= 1e-9) && t.Xc(this.b, e.b)
				);
			}),
			(t.ge.prototype.equals = t.ge.prototype.Ei),
			(t.k = t.ge.prototype),
			(t.k.contains = function(e) {
				return (e = t.Ud(e)), this.f.contains(e.lat()) && this.b.contains(e.lng());
			}),
			(t.k.intersects = function(e) {
				return (e = t.je(e)), this.f.intersects(e.f) && this.b.intersects(e.b);
			}),
			(t.k.extend = function(e) {
				return (e = t.Ud(e)), this.f.extend(e.lat()), this.b.extend(e.lng()), this;
			}),
			(t.k.union = function(e) {
				return (
					!(e = t.je(e)) || e.isEmpty() || (this.extend(e.getSouthWest()), this.extend(e.getNorthEast())),
					this
				);
			}),
			(t.k.getSouthWest = function() {
				return new t.K(this.f.f, this.b.b, !0);
			}),
			(t.k.getNorthEast = function() {
				return new t.K(this.f.b, this.b.f, !0);
			}),
			(t.k.toSpan = function() {
				return new t.K(t.$c(this.f), t.Wc(this.b), !0);
			}),
			(t.k.isEmpty = function() {
				return this.f.isEmpty() || this.b.isEmpty();
			});
		var sn = t.hd({ south: t.rd, west: t.rd, north: t.rd, east: t.rd }, !1);
		t.t(t.le, t.z),
			(t.k = t.le.prototype),
			(t.k.getAt = function(t) {
				return this.b[t];
			}),
			(t.k.indexOf = function(t) {
				for (var e = 0, n = this.b.length; e < n; ++e) if (t === this.b[e]) return e;
				return -1;
			}),
			(t.k.forEach = function(t) {
				for (var e = 0, n = this.b.length; e < n; ++e) t(this.b[e], e);
			}),
			(t.k.setAt = function(e, n) {
				var i = this.b[e],
					o = this.b.length;
				if (e < o) (this.b[e] = n), t.x.trigger(this, 'set_at', e, i), this.l && this.l(e, i);
				else {
					for (i = o; i < e; ++i) this.insertAt(i, void 0);
					this.insertAt(e, n);
				}
			}),
			(t.k.insertAt = function(e, n) {
				this.b.splice(e, 0, n), et(this), t.x.trigger(this, 'insert_at', e), this.f && this.f(e);
			}),
			(t.k.removeAt = function(e) {
				var n = this.b[e];
				return this.b.splice(e, 1), et(this), t.x.trigger(this, 'remove_at', e, n), this.j && this.j(e, n), n;
			}),
			(t.k.push = function(t) {
				return this.insertAt(this.b.length, t), this.b.length;
			}),
			(t.k.pop = function() {
				return this.removeAt(this.b.length - 1);
			}),
			(t.k.getArray = t.oa('b')),
			(t.k.clear = function() {
				for (; this.get('length'); ) this.pop();
			}),
			t.Xd(t.le.prototype, { length: null });
		var an = t.od(t.hd({ placeId: t.qi, query: t.qi, location: t.Ud }), function(e) {
				if (e.placeId && e.query) throw t.bd('cannot set both placeId and query');
				if (!e.placeId && !e.query) throw t.bd('must set one of placeId or query');
				return e;
			}),
			ln = t.ld(nt);
		t.t(t.oe, J),
			(t.k = t.oe.prototype),
			(t.k.getType = t.pa('MultiLineString')),
			(t.k.getLength = function() {
				return this.b.length;
			}),
			(t.k.getAt = function(t) {
				return this.b[t];
			}),
			(t.k.getArray = function() {
				return this.b.slice();
			}),
			(t.k.forEachLatLng = function(t) {
				this.b.forEach(function(e) {
					e.forEachLatLng(t);
				});
			}),
			t.t(t.re, J),
			(t.k = t.re.prototype),
			(t.k.getType = t.pa('Polygon')),
			(t.k.getLength = function() {
				return this.b.length;
			}),
			(t.k.getAt = function(t) {
				return this.b[t];
			}),
			(t.k.getArray = function() {
				return this.b.slice();
			}),
			(t.k.forEachLatLng = function(t) {
				this.b.forEach(function(e) {
					e.forEachLatLng(t);
				});
			});
		var cn = t.ld(t.jd(t.re, 'google.maps.Data.Polygon', !0));
		(t.k = t.se.prototype),
			(t.k.getId = t.oa('j')),
			(t.k.getGeometry = t.oa('b')),
			(t.k.setGeometry = function(e) {
				var n = this.b;
				try {
					this.b = e ? nt(e) : null;
				} catch (e) {
					return void t.cd(e);
				}
				t.x.trigger(this, 'setgeometry', { feature: this, newGeometry: this.b, oldGeometry: n });
			}),
			(t.k.getProperty = function(t) {
				return l(this.f, t);
			}),
			(t.k.setProperty = function(e, n) {
				if (void 0 === n) this.removeProperty(e);
				else {
					var i = this.getProperty(e);
					(this.f[e] = n),
						t.x.trigger(this, 'setproperty', { feature: this, name: e, newValue: n, oldValue: i });
				}
			}),
			(t.k.removeProperty = function(e) {
				var n = this.getProperty(e);
				delete this.f[e], t.x.trigger(this, 'removeproperty', { feature: this, name: e, oldValue: n });
			}),
			(t.k.forEachProperty = function(t) {
				for (var e in this.f) t(this.getProperty(e), e);
			}),
			(t.k.toGeoJson = function(e) {
				var n = this;
				t.G('data', function(t) {
					t.f(n, e);
				});
			}),
			t.t(t.ue, J),
			(t.k = t.ue.prototype),
			(t.k.getType = t.pa('GeometryCollection')),
			(t.k.getLength = function() {
				return this.b.length;
			}),
			(t.k.getAt = function(t) {
				return this.b[t];
			}),
			(t.k.getArray = function() {
				return this.b.slice();
			}),
			(t.k.forEachLatLng = function(t) {
				this.b.forEach(function(e) {
					e.forEachLatLng(t);
				});
			}),
			t.t(t.we, J),
			(t.k = t.we.prototype),
			(t.k.getType = t.pa('MultiPolygon')),
			(t.k.getLength = function() {
				return this.b.length;
			}),
			(t.k.getAt = function(t) {
				return this.b[t];
			}),
			(t.k.getArray = function() {
				return this.b.slice();
			}),
			(t.k.forEachLatLng = function(t) {
				this.b.forEach(function(e) {
					e.forEachLatLng(t);
				});
			}),
			t.t(t.xe, t.z),
			(it.prototype.get = function(t) {
				return this.b[t];
			}),
			(it.prototype.set = function(e, n) {
				var i = this.b;
				i[e] || (i[e] = {}), t.cb(i[e], n), t.x.trigger(this, 'changed', e);
			}),
			(it.prototype.reset = function(e) {
				delete this.b[e], t.x.trigger(this, 'changed', e);
			}),
			(it.prototype.forEach = function(e) {
				t.bb(this.b, e);
			});
		var un = t.pd(t.jd(t.xe, 'StreetViewPanorama'));
		(t.k = ot.prototype),
			(t.k.contains = function(e) {
				return this.b.hasOwnProperty(t.ab(e));
			}),
			(t.k.getFeatureById = function(t) {
				return l(this.f, t);
			}),
			(t.k.add = function(e) {
				if (((e = (e = e || {}) instanceof t.se ? e : new t.se(e)), !this.contains(e))) {
					var n = e.getId();
					if (n) {
						var i = this.getFeatureById(n);
						i && this.remove(i);
					}
					(i = t.ab(e)), (this.b[i] = e), n && (this.f[n] = e);
					var o = t.x.forward(e, 'setgeometry', this),
						r = t.x.forward(e, 'setproperty', this),
						s = t.x.forward(e, 'removeproperty', this);
					(this.j[i] = function() {
						t.x.removeListener(o), t.x.removeListener(r), t.x.removeListener(s);
					}),
						t.x.trigger(this, 'addfeature', { feature: e });
				}
				return e;
			}),
			(t.k.remove = function(e) {
				var n = t.ab(e),
					i = e.getId();
				this.b[n] &&
					(delete this.b[n],
					i && delete this.f[i],
					(i = this.j[n]) && (delete this.j[n], i()),
					t.x.trigger(this, 'removefeature', { feature: e }));
			}),
			(t.k.forEach = function(t) {
				for (var e in this.b) t(this.b[e]);
			}),
			t.t(rt, t.z),
			t.t(st, t.z),
			(st.prototype.set = function(e, n) {
				if (
					null != n &&
					!(
						n &&
						t.kb(n.maxZoom) &&
						n.tileSize &&
						n.tileSize.width &&
						n.tileSize.height &&
						n.getTile &&
						n.getTile.apply
					)
				)
					throw Error('Expected value implementing google.maps.MapType');
				return t.z.prototype.set.apply(this, arguments);
			});
		var dn = ut(t.jd(t.K, 'LatLng'));
		t.t(t.Je, t.z),
			t.t(dt, t.z),
			(dt.prototype.overrideStyle = function(e, n) {
				this.b.set(t.ab(e), n);
			}),
			(dt.prototype.revertStyle = function(e) {
				e ? this.b.reset(t.ab(e)) : this.b.forEach((0, t.p)(this.b.reset, this.b));
			}),
			(t.Ni = t.pd(t.jd(t.Je, 'Map'))),
			t.t(ht, t.z),
			t.Xd(ht.prototype, {
				position: t.pd(t.Ud),
				title: t.qi,
				icon: t.pd(
					t.nd([
						t.ni,
						{
							If: B('url'),
							then: t.hd(
								{
									url: t.ni,
									scaledSize: t.pd(V),
									size: t.pd(V),
									origin: t.pd(W),
									anchor: t.pd(W),
									labelOrigin: t.pd(W),
									path: t.md(function(t) {
										return null == t;
									})
								},
								!0
							)
						},
						{
							If: B('path'),
							then: t.hd(
								{
									path: t.nd([ t.ni, t.kd(Pe) ]),
									anchor: t.pd(W),
									labelOrigin: t.pd(W),
									fillColor: t.qi,
									fillOpacity: t.pi,
									rotation: t.pi,
									scale: t.pi,
									strokeColor: t.qi,
									strokeOpacity: t.pi,
									strokeWeight: t.pi,
									url: t.md(function(t) {
										return null == t;
									})
								},
								!0
							)
						}
					])
				),
				label: t.pd(
					t.nd([
						t.ni,
						{
							If: B('text'),
							then: t.hd({ text: t.ni, fontSize: t.qi, fontWeight: t.qi, fontFamily: t.qi }, !0)
						}
					])
				),
				shadow: t.Mb,
				shape: t.Mb,
				cursor: t.qi,
				clickable: t.ri,
				animation: t.Mb,
				draggable: t.ri,
				visible: t.ri,
				flat: t.Mb,
				zIndex: t.pi,
				opacity: t.pi,
				place: t.pd(an),
				attribution: t.pd(en)
			}),
			t.t(pt, t.z),
			(pt.prototype.map_changed = pt.prototype.visible_changed = function() {
				var e = this;
				t.G('poly', function(t) {
					t.f(e);
				});
			}),
			(pt.prototype.getPath = function() {
				return this.get('latLngs').getAt(0);
			}),
			(pt.prototype.setPath = function(e) {
				try {
					this.get('latLngs').setAt(0, lt(e));
				} catch (e) {
					t.cd(e);
				}
			}),
			t.Xd(pt.prototype, { draggable: t.ri, editable: t.ri, map: t.Ni, visible: t.ri }),
			t.t(t.Ne, ht),
			(t.Ne.prototype.map_changed = function() {
				this.__gm.set && this.__gm.set.remove(this);
				var e = this.get('map');
				(this.__gm.set = e && e.__gm.ba), this.__gm.set && t.zd(this.__gm.set, this);
			}),
			(t.Ne.MAX_ZINDEX = 1e6),
			t.Xd(t.Ne.prototype, { map: t.nd([ t.Ni, un ]) }),
			t.t(t.Oe, pt),
			(t.Oe.prototype.Aa = !0),
			(t.Oe.prototype.getPaths = function() {
				return this.get('latLngs');
			}),
			(t.Oe.prototype.setPaths = function(t) {
				this.set('latLngs', ct(t));
			}),
			t.t(t.Pe, pt),
			(t.Pe.prototype.Aa = !1),
			(t.Qe = 'click dblclick mousedown mousemove mouseout mouseover mouseup rightclick'.split(' ')),
			t.t(ft, t.z),
			(t.k = ft.prototype),
			(t.k.contains = function(t) {
				return this.b.contains(t);
			}),
			(t.k.getFeatureById = function(t) {
				return this.b.getFeatureById(t);
			}),
			(t.k.add = function(t) {
				return this.b.add(t);
			}),
			(t.k.remove = function(t) {
				this.b.remove(t);
			}),
			(t.k.forEach = function(t) {
				this.b.forEach(t);
			}),
			(t.k.addGeoJson = function(e, n) {
				return t.Ie(this.b, e, n);
			}),
			(t.k.loadGeoJson = function(e, n, i) {
				var o = this.b;
				t.G('data', function(t) {
					t.Ik(o, e, n, i);
				});
			}),
			(t.k.toGeoJson = function(e) {
				var n = this.b;
				t.G('data', function(t) {
					t.Ek(n, e);
				});
			}),
			(t.k.overrideStyle = function(t, e) {
				this.f.overrideStyle(t, e);
			}),
			(t.k.revertStyle = function(t) {
				this.f.revertStyle(t);
			}),
			(t.k.controls_changed = function() {
				this.get('controls') && mt(this);
			}),
			(t.k.drawingMode_changed = function() {
				this.get('drawingMode') && mt(this);
			}),
			t.Xd(ft.prototype, {
				map: t.Ni,
				style: t.Mb,
				controls: t.pd(t.ld(t.kd(De))),
				controlPosition: t.pd(t.kd(t.jg)),
				drawingMode: t.pd(t.kd(De))
			}),
			(t.Oi = { METRIC: 0, IMPERIAL: 1 }),
			(t.Pi = { DRIVING: 'DRIVING', WALKING: 'WALKING', BICYCLING: 'BICYCLING', TRANSIT: 'TRANSIT' }),
			(t.Qi = { BEST_GUESS: 'bestguess', OPTIMISTIC: 'optimistic', PESSIMISTIC: 'pessimistic' }),
			(t.Ri = { BUS: 'BUS', RAIL: 'RAIL', SUBWAY: 'SUBWAY', TRAIN: 'TRAIN', TRAM: 'TRAM' }),
			(t.Si = { LESS_WALKING: 'LESS_WALKING', FEWER_TRANSFERS: 'FEWER_TRANSFERS' });
		var hn,
			pn,
			fn,
			mn,
			gn,
			vn,
			bn,
			yn,
			wn,
			xn = t.hd({ routes: t.ld(t.md(t.lb)) }, !0);
		(t.Ue = []),
			t.t(vt, t.z),
			(t.k = vt.prototype),
			(t.k.internalAnchor_changed = function() {
				var e = this.get('internalAnchor');
				bt(this, 'attribution', e),
					bt(this, 'place', e),
					bt(this, 'internalAnchorMap', e, 'map'),
					bt(this, 'internalAnchorPoint', e, 'anchorPoint'),
					e instanceof t.Ne
						? bt(this, 'internalAnchorPosition', e, 'internalPosition')
						: bt(this, 'internalAnchorPosition', e, 'position');
			}),
			(t.k.internalAnchorPoint_changed = vt.prototype.internalPixelOffset_changed = function() {
				var e = this.get('internalAnchorPoint') || t.si,
					n = this.get('internalPixelOffset') || t.ti;
				this.set('pixelOffset', new t.I(n.width + Math.round(e.x), n.height + Math.round(e.y)));
			}),
			(t.k.internalAnchorPosition_changed = function() {
				var t = this.get('internalAnchorPosition');
				t && this.set('position', t);
			}),
			(t.k.internalAnchorMap_changed = function() {
				this.get('internalAnchor') && this.b.set('map', this.get('internalAnchorMap'));
			}),
			(t.k.im = function() {
				var t = this.get('internalAnchor');
				!this.b.get('map') && t && t.get('map') && this.set('internalAnchor', null);
			}),
			(t.k.internalContent_changed = function() {
				this.set('content', gt(this.get('internalContent')));
			}),
			(t.k.trigger = function(e) {
				t.x.trigger(this.b, e);
			}),
			(t.k.close = function() {
				this.b.set('map', null);
			}),
			t.t(t.af, t.z),
			t.Xd(t.af.prototype, {
				content: t.nd([ t.qi, t.md(F) ]),
				position: t.pd(t.Ud),
				size: t.pd(V),
				map: t.nd([ t.Ni, un ]),
				anchor: t.pd(t.jd(t.z, 'MVCObject')),
				zIndex: t.pi
			}),
			(t.af.prototype.open = function(t, e) {
				this.set('anchor', e), e ? !this.get('map') && t && this.set('map', t) : this.set('map', t);
			}),
			(t.af.prototype.close = function() {
				this.set('map', null);
			}),
			t.t(yt, t.z),
			(yt.prototype.changed = function(e) {
				if ('map' == e || 'panel' == e) {
					var n = this;
					t.G('directions', function(t) {
						t.nl(n, e);
					});
				}
				'panel' == e && t.Ve(this.getPanel());
			}),
			t.Xd(yt.prototype, { directions: xn, map: t.Ni, panel: t.pd(t.md(F)), routeIndex: t.pi }),
			(wt.prototype.route = function(e, n) {
				t.G('directions', function(t) {
					t.Gh(e, n, !0);
				});
			}),
			(xt.prototype.getDistanceMatrix = function(e, n) {
				t.G('distance_matrix', function(t) {
					t.b(e, n);
				});
			}),
			(kt.prototype.getElevationAlongPath = function(e, n) {
				t.G('elevation', function(t) {
					t.getElevationAlongPath(e, n);
				});
			}),
			(kt.prototype.getElevationForLocations = function(e, n) {
				t.G('elevation', function(t) {
					t.getElevationForLocations(e, n);
				});
			}),
			(t.Vi = t.jd(t.ge, 'LatLngBounds')),
			(t.ff.prototype.geocode = function(e, n) {
				t.G('geocoder', function(t) {
					t.geocode(e, n);
				});
			}),
			t.t(t.gf, t.z),
			(t.gf.prototype.map_changed = function() {
				var e = this;
				t.G('kml', function(t) {
					t.b(e);
				});
			}),
			t.Xd(t.gf.prototype, { map: t.Ni, url: null, bounds: null, opacity: t.pi }),
			(t.Wi = {
				UNKNOWN: 'UNKNOWN',
				OK: t.ga,
				INVALID_REQUEST: t.ba,
				DOCUMENT_NOT_FOUND: 'DOCUMENT_NOT_FOUND',
				FETCH_ERROR: 'FETCH_ERROR',
				INVALID_DOCUMENT: 'INVALID_DOCUMENT',
				DOCUMENT_TOO_LARGE: 'DOCUMENT_TOO_LARGE',
				LIMITS_EXCEEDED: 'LIMITS_EXECEEDED',
				TIMED_OUT: 'TIMED_OUT'
			}),
			t.t($t, t.z),
			(t.k = $t.prototype),
			(t.k.sd = function() {
				var e = this;
				t.G('kml', function(t) {
					t.f(e);
				});
			}),
			(t.k.url_changed = $t.prototype.sd),
			(t.k.driveFileId_changed = $t.prototype.sd),
			(t.k.map_changed = $t.prototype.sd),
			(t.k.zIndex_changed = $t.prototype.sd),
			t.Xd($t.prototype, {
				map: t.Ni,
				defaultViewport: null,
				metadata: null,
				status: null,
				url: t.qi,
				screenOverlays: t.ri,
				zIndex: t.pi
			}),
			t.t(t.jf, t.z),
			t.Xd(t.jf.prototype, { map: t.Ni }),
			t.t(Tt, t.z),
			t.Xd(Tt.prototype, { map: t.Ni }),
			t.t(Et, t.z),
			t.Xd(Et.prototype, { map: t.Ni }),
			(t.Xi = { NEAREST: 'nearest', BEST: 'best' }),
			(t.Yi = { DEFAULT: 'default', OUTDOOR: 'outdoor' }),
			(t.ig = { japan_prequake: 20, japan_postquake2010: 24 }),
			(t.mf.prototype.af = !0),
			(t.mf.prototype.wb = t.ra(4)),
			(t.mf.prototype.Xg = !0),
			(t.mf.prototype.Ed = t.ra(6)),
			t.nf('about:blank'),
			(t.bg = 'StopIteration' in t.Kc ? t.Kc.StopIteration : { message: 'StopIteration', stack: '' }),
			(t.of.prototype.next = function() {
				throw t.bg;
			}),
			(t.of.prototype.Ce = function() {
				return this;
			}),
			(!t.Sh && !t.Qh) || (t.Qh && 9 <= Number(t.ei)) || (t.Sh && t.Tc('1.9.1')),
			t.Qh && t.Tc('9'),
			(t.qf.prototype.Xg = !0),
			(t.qf.prototype.Ed = t.ra(5)),
			(t.qf.prototype.af = !0),
			(t.qf.prototype.wb = t.ra(3)),
			(t.pf = {}),
			t.rf('<!DOCTYPE html>', 0),
			t.rf('', 0),
			t.rf('<br>', 0),
			(t.wf.prototype.addListener = function(t, e) {
				this.P.addListener(t, e);
			}),
			(t.wf.prototype.addListenerOnce = function(t, e) {
				this.P.addListenerOnce(t, e);
			}),
			(t.wf.prototype.removeListener = function(t, e) {
				this.P.removeListener(t, e);
			}),
			(t.wf.prototype.b = t.ra(7)),
			t.t(Ct, t.of),
			(Ct.prototype.setPosition = function(e, n, i) {
				(this.node = e) && (this.f = t.Ha(n) ? n : 1 != this.node.nodeType ? 0 : this.b ? -1 : 1),
					t.Ha(i) && (this.depth = i);
			}),
			(Ct.prototype.next = function() {
				var e;
				if (this.j) {
					if (!this.node || (this.l && 0 == this.depth)) throw t.bg;
					e = this.node;
					var n = this.b ? -1 : 1;
					if (this.f == n) {
						var i = this.b ? e.lastChild : e.firstChild;
						i ? this.setPosition(i) : this.setPosition(e, -1 * n);
					} else
						(i = this.b ? e.previousSibling : e.nextSibling)
							? this.setPosition(i)
							: this.setPosition(e.parentNode, -1 * n);
					this.depth += this.f * (this.b ? -1 : 1);
				} else this.j = !0;
				if (((e = this.node), !this.node)) throw t.bg;
				return e;
			}),
			(Ct.prototype.splice = function(e) {
				var n = this.node,
					i = this.b ? 1 : -1;
				this.f == i && ((this.f = -1 * i), (this.depth += this.f * (this.b ? -1 : 1))),
					(this.b = !this.b),
					Ct.prototype.next.call(this),
					(this.b = !this.b);
				for (var o = (i = t.Fa(e) ? e : arguments).length - 1; 0 <= o; o--) t.sf(i[o], n);
				t.tf(n);
			}),
			t.t(t.yf, t.z),
			(t.yf.prototype.K = function() {
				var t = this;
				t.D ||
					(t.D = window.setTimeout(function() {
						(t.D = void 0), t.Z();
					}, t.ki));
			}),
			(t.yf.prototype.C = function() {
				this.D && window.clearTimeout(this.D), (this.D = void 0), this.Z();
			}),
			t.t(_t, t.L),
			t.t(St, t.L),
			t.t(jt, t.L),
			t.t(Nt, t.L),
			t.t(At, Ct),
			(At.prototype.next = function() {
				for (; At.fb.next.call(this), -1 == this.f; );
				return this.node;
			}),
			t.t(It, t.L),
			t.t(t.Ff, t.L),
			t.t(Lt, t.L),
			t.t(Dt, t.L),
			t.t(Ot, t.L),
			(t.Uf.prototype.fromLatLngToPoint = function(e, n) {
				n = n || new t.H(0, 0);
				var i = this.b;
				return (
					(n.x = i.x + e.lng() * this.j),
					(e = t.eb(Math.sin(t.uc(e.lat())), -(1 - 1e-15), 1 - 1e-15)),
					(n.y = i.y + 0.5 * Math.log((1 + e) / (1 - e)) * -this.l),
					n
				);
			}),
			(t.Uf.prototype.fromPointToLatLng = function(e, n) {
				var i = this.b;
				return new t.K(
					t.vc(2 * Math.atan(Math.exp((e.y - i.y) / -this.l)) - Math.PI / 2),
					(e.x - i.x) / this.j,
					n
				);
			}),
			t.t(Pt, t.L),
			t.t(Rt, t.L),
			t.t(Mt, t.L),
			t.t(qt, t.L),
			(t.xg = {}),
			t.t(zt, t.L),
			(zt.prototype.getZoom = function() {
				return t.M(this, 2);
			}),
			(zt.prototype.setZoom = function(t) {
				this.data[2] = t;
			}),
			t.t(Ht, t.xe),
			(Ht.prototype.visible_changed = function() {
				var e = this;
				!e.m &&
					e.getVisible() &&
					((e.m = !0),
					t.G('streetview', function(t) {
						var n;
						e.f && (n = e.f), t.Am(e, n);
					}));
			}),
			t.Xd(Ht.prototype, {
				visible: t.ri,
				pano: t.qi,
				position: t.pd(t.Ud),
				pov: t.pd(tn),
				motionTracking: Qe,
				photographerPov: null,
				location: null,
				links: t.ld(t.md(t.lb)),
				status: null,
				zoom: t.pi,
				enableCloseButton: t.ri
			}),
			(Ht.prototype.registerPanoProvider = function(t, e) {
				this.set('panoProvider', { yh: t, options: e || {} });
			}),
			(t.k = t.lg.prototype),
			(t.k.wg = t.ra(8)),
			(t.k.Lb = t.ra(9)),
			(t.k.Nf = t.ra(10)),
			(t.k.Mf = t.ra(11)),
			(t.k.Lf = t.ra(12)),
			t.t(Bt, t.yf);
		var kn,
			$n,
			Tn,
			En = { roadmap: 0, satellite: 2, hybrid: 3, terrain: 4 },
			Cn = { 0: 1, 2: 2, 3: 2, 4: 2 };
		(t.k = Bt.prototype),
			(t.k.Kg = t.Vd('center')),
			(t.k.$f = t.Vd('zoom')),
			(t.k.changed = function() {
				var t = this.Kg(),
					e = this.$f(),
					n = Wt(this);
				((t && !t.b(this.G)) || this.F != e || this.O != n) &&
					(Vt(this.f), this.K(), (this.F = e), (this.O = n)),
					(this.G = t);
			}),
			(t.k.Z = function() {
				var e = '',
					n = this.Kg(),
					i = this.$f(),
					o = Wt(this),
					r = this.get('size');
				if (r) {
					var s;
					if (
						n &&
						(0, window.isFinite)(n.lat()) &&
						(0, window.isFinite)(n.lng()) &&
						1 < i &&
						null != o &&
						r &&
						r.width &&
						r.height &&
						this.b &&
						(t.uf(this.b, r),
						(n = t.Zf(this.l, n, i))
							? (((s = new t.Fd()).I = Math.round(n.x - r.width / 2)),
								(s.L = s.I + r.width),
								(s.J = Math.round(n.y - r.height / 2)),
								(s.M = s.J + r.height))
							: (s = null),
						(n = Cn[o]),
						s)
					) {
						e = new zt();
						var a = new Mt(t.O(e, 0));
						if (
							((a.data[0] = s.I),
							(a.data[1] = s.J),
							(e.data[1] = n),
							e.setZoom(i),
							((i = new qt(t.O(e, 3))).data[0] = s.L - s.I),
							(i.data[1] = s.M - s.J),
							((i = new Rt(t.O(e, 4))).data[0] = o),
							(i.data[4] = t.Jf(t.Tf(t.Q))),
							(i.data[5] = t.Kf(t.Tf(t.Q)).toLowerCase()),
							(i.data[9] = !0),
							(i.data[11] = !0),
							(o = this.B + (0, window.unescape)('%3F')),
							!wn)
						) {
							if (
								((i = wn = { b: -1, A: [] }),
								(n = new Mt([])),
								bn || (bn = { b: -1, A: [ , t.S, t.S ] }),
								(n = t.D(n, bn)),
								(s = new qt([])),
								yn || ((yn = { b: -1, A: [] }).A = [ , t.Ch, t.Ch, t.qc(1) ]),
								(s = t.D(s, yn)),
								(a = new Rt([])),
								!vn)
							) {
								var l = [];
								(vn = { b: -1, A: l }),
									(l[1] = t.U),
									(l[2] = t.T),
									(l[3] = t.T),
									(l[5] = t.V),
									(l[6] = t.V);
								var c = new Pt([]);
								gn || (gn = { b: -1, A: [ , t.Gh, t.T ] }),
									(l[9] = t.D(c, gn)),
									(l[10] = t.T),
									(l[11] = t.T),
									(l[12] = t.T),
									(l[13] = t.Gh),
									(l[100] = t.T);
							}
							if (((a = t.D(a, vn)), (l = new _t([])), !hn)) {
								c = hn = { b: -1, A: [] };
								var u = new St([]);
								pn || (pn = { b: -1, A: [ , t.T ] }), (u = t.D(u, pn));
								var d = new Nt([]);
								mn || (mn = { b: -1, A: [ , t.T, t.T ] }), (d = t.D(d, mn));
								var h = new jt([]);
								fn || (fn = { b: -1, A: [ , t.T ] }), (c.A = [ , u, , , , , , , , , d, , t.D(h, fn) ]);
							}
							i.A = [ , n, t.U, t.Ch, s, a, t.D(l, hn) ];
						}
						(e = t.Nh.b(e.data, wn)), (e = this.m(o + e));
					}
					this.f && (t.uf(this.f, r), Xt(this, e));
				}
			}),
			(t.k.div_changed = function() {
				var e = this.get('div'),
					n = this.b;
				if (e)
					if (n) e.appendChild(n);
					else {
						(n = this.b = window.document.createElement('div')).style.overflow = 'hidden';
						var i = (this.f = window.document.createElement('img'));
						t.x.addDomListener(n, 'contextmenu', function(e) {
							t.tb(e), t.vb(e);
						}),
							(i.ontouchstart = i.ontouchmove = i.ontouchend = i.ontouchcancel = function(e) {
								t.ub(e), t.vb(e);
							}),
							t.uf(i, t.ti),
							e.appendChild(n),
							this.Z();
					}
				else n && (Vt(n), (this.b = null));
			}),
			t.t(Kt, rt),
			t.t(Yt, t.Je),
			(t.k = Yt.prototype),
			(t.k.streetView_changed = function() {
				var t = this.get('streetView');
				t ? t.set('standAlone', !1) : this.set('streetView', this.__gm.j);
			}),
			(t.k.getDiv = function() {
				return this.__gm.R;
			}),
			(t.k.panBy = function(e, n) {
				var i = this.__gm;
				t.G('map', function() {
					t.x.trigger(i, 'panby', e, n);
				});
			}),
			(t.k.panTo = function(e) {
				var n = this.__gm;
				(e = t.Ud(e)),
					t.G('map', function() {
						t.x.trigger(n, 'panto', e);
					});
			}),
			(t.k.panToBounds = function(e) {
				var n = this.__gm,
					i = t.je(e);
				t.G('map', function() {
					t.x.trigger(n, 'pantolatlngbounds', i);
				});
			}),
			(t.k.fitBounds = function(e) {
				var n = this;
				(e = t.je(e)),
					t.G('map', function(t) {
						t.fitBounds(n, e);
					});
			}),
			t.Xd(Yt.prototype, {
				bounds: null,
				streetView: un,
				center: t.pd(t.Ud),
				zoom: t.pi,
				mapTypeId: t.qi,
				projection: null,
				heading: t.pi,
				tilt: t.pi,
				clickableIcons: Qe
			}),
			(Zt.prototype.getMaxZoomAtLatLng = function(e, n) {
				t.G('maxzoom', function(t) {
					t.getMaxZoomAtLatLng(e, n);
				});
			}),
			t.t(te, t.z),
			(te.prototype.changed = function(e) {
				if ('suppressInfoWindows' != e && 'clickable' != e) {
					var n = this;
					t.G('onion', function(t) {
						t.b(n);
					});
				}
			}),
			t.Xd(te.prototype, { map: t.Ni, tableId: t.pi, query: t.pd(t.nd([ t.ni, t.md(t.lb, 'not an Object') ])) }),
			t.t(t.Dg, t.z),
			(t.Dg.prototype.map_changed = function() {
				var e = this;
				t.G('overlay', function(t) {
					t.Sj(e);
				});
			}),
			t.Xd(t.Dg.prototype, { panes: null, projection: null, map: t.nd([ t.Ni, un ]) }),
			t.t(t.Eg, t.z),
			(t.Eg.prototype.map_changed = t.Eg.prototype.visible_changed = function() {
				var e = this;
				t.G('poly', function(t) {
					t.b(e);
				});
			}),
			(t.Eg.prototype.center_changed = function() {
				t.x.trigger(this, 'bounds_changed');
			}),
			(t.Eg.prototype.radius_changed = t.Eg.prototype.center_changed),
			(t.Eg.prototype.getBounds = function() {
				var e = this.get('radius'),
					n = this.get('center');
				if (n && t.kb(e)) {
					var i = (i = this.get('map')) && i.__gm.get('baseMapType');
					return t.dg(n, e / t.De(i));
				}
				return null;
			}),
			t.Xd(t.Eg.prototype, {
				center: t.pd(t.Ud),
				draggable: t.ri,
				editable: t.ri,
				map: t.Ni,
				radius: t.pi,
				visible: t.ri
			}),
			t.t(t.Fg, t.z),
			(t.Fg.prototype.map_changed = t.Fg.prototype.visible_changed = function() {
				var e = this;
				t.G('poly', function(t) {
					t.j(e);
				});
			}),
			t.Xd(t.Fg.prototype, { draggable: t.ri, editable: t.ri, bounds: t.pd(t.je), map: t.Ni, visible: t.ri }),
			t.t(ee, t.z),
			(ee.prototype.map_changed = function() {
				var e = this;
				t.G('streetview', function(t) {
					t.Rj(e);
				});
			}),
			t.Xd(ee.prototype, { map: t.Ni }),
			(t.Hg.prototype.getPanorama = function(e, n) {
				var i = this.b || void 0;
				t.G('streetview', function(o) {
					t.G('geometry', function(t) {
						o.Ok(e, n, t.computeHeading, t.computeOffset, i);
					});
				});
			}),
			(t.Hg.prototype.getPanoramaByLocation = function(t, e, n) {
				this.getPanorama({ location: t, radius: e, preference: (e || 0) < 50 ? 'best' : 'nearest' }, n);
			}),
			(t.Hg.prototype.getPanoramaById = function(t, e) {
				this.getPanorama({ pano: t }, e);
			}),
			($n = {
				0: '',
				1: 'msie',
				3: 'chrome',
				4: 'applewebkit',
				5: 'firefox',
				6: 'trident',
				7: 'mozilla',
				2: 'edge'
			}),
			(Tn = { 0: '', 1: 'x11', 2: 'macintosh', 3: 'windows', 4: 'android', 5: 'iphone', 6: 'ipad' }),
			(t.R = null),
			void 0 !== window.navigator && (t.R = new ie()),
			(oe.prototype.j = t.Nb(function() {
				var e = new window.Image();
				return t.m(e.crossOrigin);
			})),
			(oe.prototype.l = t.Nb(function() {
				return t.m(window.document.createElement('span').draggable);
			})),
			(oe.prototype.f = t.Nb(function() {
				try {
					var t = window.document.createElement('canvas').getContext('2d'),
						e = t.getImageData(0, 0, 1, 1);
					return (
						(e.data[0] = e.data[1] = e.data[2] = 100),
						(e.data[3] = 64),
						t.putImageData(e, 0, 0),
						(e = t.getImageData(0, 0, 1, 1)).data[0] < 95 || 105 < e.data[0]
					);
				} catch (t) {
					return !1;
				}
			})),
			(t.jj = t.R ? new oe(t.R) : null),
			(t.kj = t.R ? new re() : null),
			(t.lj = new t.Tg(0, 0)),
			t.t(t.Ug, t.z),
			(t.k = t.Ug.prototype),
			(t.k.getTile = function(e, n, i) {
				if (!e || !i) return null;
				var o = i.createElement('div');
				if (
					((i = { Y: e, zoom: n, Hb: null }),
					(o.__gmimt = i),
					t.zd(this.b, o),
					1 != (r = le(this)) && ae(o, r),
					this.f)
				) {
					var r = this.tileSize || new t.I(256, 256),
						s = this.j(e, n);
					i.Hb = this.f(e, n, r, o, s, function() {
						t.x.trigger(o, 'load');
					});
				}
				return o;
			}),
			(t.k.releaseTile = function(t) {
				t && this.b.contains(t) && (this.b.remove(t), (t = t.__gmimt.Hb) && t.release());
			}),
			(t.k.Re = t.ra(13)),
			(t.k.opacity_changed = function() {
				var t = le(this);
				this.b.forEach(function(e) {
					return ae(e, t);
				});
			}),
			(t.k.nd = !0),
			t.Xd(t.Ug.prototype, { opacity: t.pi }),
			t.t(t.Xg, t.z),
			(t.Xg.prototype.getTile = function() {
				return null;
			}),
			(t.Xg.prototype.tileSize = new t.I(256, 256)),
			(t.Xg.prototype.nd = !0),
			t.t(t.Yg, t.Xg),
			t.t(t.Zg, t.z),
			t.Xd(t.Zg.prototype, { attribution: t.pd(en), place: t.pd(an) });
		var _n = {
			Animation: { BOUNCE: 1, DROP: 2, no: 3, lo: 4 },
			Circle: t.Eg,
			ControlPosition: t.jg,
			Data: ft,
			GroundOverlay: t.gf,
			ImageMapType: t.Ug,
			InfoWindow: t.af,
			LatLng: t.K,
			LatLngBounds: t.ge,
			MVCArray: t.le,
			MVCObject: t.z,
			Map: Yt,
			MapTypeControlStyle: { DEFAULT: 0, HORIZONTAL_BAR: 1, DROPDOWN_MENU: 2, INSET: 3, INSET_LARGE: 4 },
			MapTypeId: t.ph,
			MapTypeRegistry: st,
			Marker: t.Ne,
			MarkerImage: function(t, e, n, i, o) {
				(this.url = t),
					(this.size = e || o),
					(this.origin = n),
					(this.anchor = i),
					(this.scaledSize = o),
					(this.labelOrigin = null);
			},
			NavigationControlStyle: { DEFAULT: 0, SMALL: 1, ANDROID: 2, ZOOM_PAN: 3, oo: 4, Bj: 5 },
			OverlayView: t.Dg,
			Point: t.H,
			Polygon: t.Oe,
			Polyline: t.Pe,
			Rectangle: t.Fg,
			ScaleControlStyle: { DEFAULT: 0 },
			Size: t.I,
			StreetViewPreference: t.Xi,
			StreetViewSource: t.Yi,
			StrokePosition: { CENTER: 0, INSIDE: 1, OUTSIDE: 2 },
			SymbolPath: Pe,
			ZoomControlStyle: { DEFAULT: 0, SMALL: 1, LARGE: 2, Bj: 3 },
			event: t.x
		};
		t.cb(_n, {
			BicyclingLayer: t.jf,
			DirectionsRenderer: yt,
			DirectionsService: wt,
			DirectionsStatus: {
				OK: t.ga,
				UNKNOWN_ERROR: t.ja,
				OVER_QUERY_LIMIT: t.ha,
				REQUEST_DENIED: t.ia,
				INVALID_REQUEST: t.ba,
				ZERO_RESULTS: t.ka,
				MAX_WAYPOINTS_EXCEEDED: t.ea,
				NOT_FOUND: t.fa
			},
			DirectionsTravelMode: t.Pi,
			DirectionsUnitSystem: t.Oi,
			DistanceMatrixService: xt,
			DistanceMatrixStatus: {
				OK: t.ga,
				INVALID_REQUEST: t.ba,
				OVER_QUERY_LIMIT: t.ha,
				REQUEST_DENIED: t.ia,
				UNKNOWN_ERROR: t.ja,
				MAX_ELEMENTS_EXCEEDED: t.da,
				MAX_DIMENSIONS_EXCEEDED: t.ca
			},
			DistanceMatrixElementStatus: { OK: t.ga, NOT_FOUND: t.fa, ZERO_RESULTS: t.ka },
			ElevationService: kt,
			ElevationStatus: {
				OK: t.ga,
				UNKNOWN_ERROR: t.ja,
				OVER_QUERY_LIMIT: t.ha,
				REQUEST_DENIED: t.ia,
				INVALID_REQUEST: t.ba,
				ho: 'DATA_NOT_AVAILABLE'
			},
			FusionTablesLayer: te,
			Geocoder: t.ff,
			GeocoderLocationType: {
				ROOFTOP: 'ROOFTOP',
				RANGE_INTERPOLATED: 'RANGE_INTERPOLATED',
				GEOMETRIC_CENTER: 'GEOMETRIC_CENTER',
				APPROXIMATE: 'APPROXIMATE'
			},
			GeocoderStatus: {
				OK: t.ga,
				UNKNOWN_ERROR: t.ja,
				OVER_QUERY_LIMIT: t.ha,
				REQUEST_DENIED: t.ia,
				INVALID_REQUEST: t.ba,
				ZERO_RESULTS: t.ka,
				ERROR: t.aa
			},
			KmlLayer: $t,
			KmlLayerStatus: t.Wi,
			MaxZoomService: Zt,
			MaxZoomStatus: { OK: t.ga, ERROR: t.aa },
			SaveWidget: t.Zg,
			StreetViewCoverageLayer: ee,
			StreetViewPanorama: Ht,
			StreetViewService: t.Hg,
			StreetViewStatus: { OK: t.ga, UNKNOWN_ERROR: t.ja, ZERO_RESULTS: t.ka },
			StyledMapType: t.Yg,
			TrafficLayer: Tt,
			TrafficModel: t.Qi,
			TransitLayer: Et,
			TransitMode: t.Ri,
			TransitRoutePreference: t.Si,
			TravelMode: t.Pi,
			UnitSystem: t.Oi
		}),
			t.cb(ft, {
				Feature: t.se,
				Geometry: J,
				GeometryCollection: t.ue,
				LineString: t.ce,
				LinearRing: t.de,
				MultiLineString: t.oe,
				MultiPoint: t.ee,
				MultiPolygon: t.we,
				Point: t.fe,
				Polygon: t.re
			}),
			t.dd('main', {});
		var Sn,
			jn = /'/g,
			Nn = t;
		window.google.maps.Load(function(e, n) {
			var i = window.google.maps;
			fe();
			var o = me(i);
			for (
				t.Q = new Dt(e),
					t.nj = Math.random() < t.M(t.Q, 0, 1),
					t.oj = Math.round(1e15 * Math.random()).toString(36),
					t.zg = de(),
					t.Ui = he(),
					t.ij = new t.le(),
					t.mg = n,
					e = 0;
				e < t.Pd(t.Q, 8);
				++e
			)
				t.xg[t.Od(t.Q, 8, e)] = !0;
			(e = new t.Ff(t.Q.data[3])),
				Q(t.N(e, 0)),
				t.bb(_n, function(t, e) {
					i[t] = e;
				}),
				(i.version = t.N(e, 1)),
				window.setTimeout(function() {
					G([ 'util', 'stats' ], function(e, n) {
						e.f.b(), e.j(), o && n.b.b({ ev: 'api_alreadyloaded', client: t.N(t.Q, 6), key: t.N(t.Q, 16) });
					});
				}, 5e3),
				t.x.Qm(),
				(kn = new Gt()),
				(e = t.N(t.Q, 11)) && G(t.Nd(t.Q, 12), pe(e), !0);
		});
	}.call(this, {});
var IPMapper = {
	map: null,
	mapTypeId: google.maps.MapTypeId.ROADMAP,
	latlngbound: null,
	infowindow: null,
	baseUrl: 'http://freegeoip.net/json/',
	addIPMarker: function(t) {
		if (
			((ipRegex = /^(([0-9]|[1-9][0-9]|1[0-9]{2}|2[0-4][0-9]|25[0-5])\.){3}([0-9]|[1-9][0-9]|1[0-9]{2}|2[0-4][0-9]|25[0-5])$/),
			'' != $.trim(t) && ipRegex.test(t))
		) {
			var e = encodeURI(IPMapper.baseUrl + t + '?callback=?');
			$.getJSON(e, function(t) {
				'' == $.trim(t.latitude) || '0' == t.latitude || isNaN(t.latitude)
					? (IPMapper.logError('IP Address geocoding failed!'), $.error('IP Address geocoding failed!'))
					: (t.region_name, t.country_name);
			});
		} else IPMapper.logError('Invalid IP Address!'), $.error('Invalid IP Address!');
	}
};
function getUserLocation() {
	navigator.geolocation
		? navigator.geolocation.getCurrentPosition(showUserPosition)
		: alert('Geolocation is not supported by this browser.');
}
function showUserPosition(t) {
	var e = t.coords.latitude,
		n = t.coords.longitude,
		i = encodeURI(
			'https://maps.googleapis.com/maps/api/geocode/json?latlng=' +
				e +
				',' +
				n +
				'&key=AIzaSyBlpCdiNIP0fJnV1TFhaNwyEXbDNCrqz9E'
		);
	$.getJSON(i, function(t) {
		if ('' != t) {
			t.results[0].formatted_address.toLowerCase();
			for (var e = t.results[0], n = '', i = '', o = 0, r = e.address_components.length; o < r; o++) {
				var s = e.address_components[o];
				0 <= s.types.indexOf('locality') && (n = s.long_name),
					0 <= s.types.indexOf('route') && (i = s.long_name);
			}
			'' != n &&
				($('select#city_id option').each(function() {
					this.selected = this.text == n;
				}),
				$('select#city_id').change()),
				'' != n &&
					'' != i &&
					setTimeout(function() {
						$('select#area_id option').each(function() {
							this.selected = this.text == i;
						});
					}, 1e3);
		}
	});
}
function getLocation() {
	navigator.geolocation
		? navigator.geolocation.getCurrentPosition(showPosition)
		: alert('Geolocation is not supported by this browser.');
}
function showPosition(t) {
	var e = t.coords.latitude,
		n = t.coords.longitude,
		i = encodeURI(
			'https://maps.googleapis.com/maps/api/geocode/json?latlng=' +
				e +
				',' +
				n +
				'&key=AIzaSyBlpCdiNIP0fJnV1TFhaNwyEXbDNCrqz9E'
		);
	$.getJSON(i, function(t) {
		if ('' != t) {
			for (
				var e = t.results[0].formatted_address.toLowerCase(),
					n = t.results[0],
					i = '',
					o = 0,
					r = n.address_components.length;
				o < r;
				o++
			) {
				var s = n.address_components[o];
				if (0 <= s.types.indexOf('locality') && (i = s.long_name)) var a = s.long_name;
			}
			var l = 5,
				c = 2879;
			'dammam' == i
				? ((l = 2), (c = 4226))
				: 'jeddah' == i
					? ((l = 3), (c = 4224))
					: 'riyadh' == i ? ((l = 1), (c = 2879)) : ((l = 5), (c = 4230), (i = '')),
				'' == i && (i = ''),
				$('#input-address-1').val(e.toUpperCase()),
				$('#input-city').val(a),
				$('#input-group-id').val(l),
				$('#input-zone-id').val(c);
		}
	});
}
!(function(t) {
	var e = t(window),
		n = e.height();
	e.resize(function() {
		n = e.height();
	}),
		(t.fn.parallax = function(i, o, r) {
			var s,
				a,
				l = t(this);
			function c() {
				var r = e.scrollTop();
				l.each(function() {
					var e = t(this),
						c = e.offset().top;
					c + s(e) < r || r + n < c || l.css('backgroundPosition', i + ' ' + Math.round((a - r) * o) + 'px');
				});
			}
			l.each(function() {
				a = l.offset().top;
			}),
				(s = r
					? function(t) {
							return t.outerHeight(!0);
						}
					: function(t) {
							return t.height();
						}),
				(arguments.length < 1 || null === i) && (i = '50%'),
				(arguments.length < 2 || null === o) && (o = 0.1),
				(arguments.length < 3 || null === r) && (r = !0),
				e.bind('scroll', c).resize(c),
				c();
		});
})(jQuery),
	(function(t) {
		'use strict';
		t.expr[':'].icontains = function(e, n, i) {
			return 0 <= t(e).text().toUpperCase().indexOf(i[3].toUpperCase());
		};
		var e = function(n, i, o) {
			o && (o.stopPropagation(), o.preventDefault()),
				(this.$element = t(n)),
				(this.$newElement = null),
				(this.$button = null),
				(this.$menu = null),
				(this.$lis = null),
				(this.options = i),
				null === this.options.title && (this.options.title = this.$element.attr('title')),
				(this.val = e.prototype.val),
				(this.render = e.prototype.render),
				(this.refresh = e.prototype.refresh),
				(this.setStyle = e.prototype.setStyle),
				(this.selectAll = e.prototype.selectAll),
				(this.deselectAll = e.prototype.deselectAll),
				(this.destroy = e.prototype.remove),
				(this.remove = e.prototype.remove),
				(this.show = e.prototype.show),
				(this.hide = e.prototype.hide),
				this.init();
		};
		(e.VERSION = '1.6.2'),
			(e.DEFAULTS = {
				noneSelectedText: 'Nothing selected',
				noneResultsText: 'No results match',
				countSelectedText: '{0} of {1} selected',
				maxOptionsText: [
					'Limit reached ({n} {var} max)',
					'Group limit reached ({n} {var} max)',
					[ 'items', 'item' ]
				],
				multipleSeparator: ', ',
				style: 'btn-default',
				size: 'auto',
				title: null,
				selectedTextFormat: 'values',
				width: !1,
				container: !1,
				hideDisabled: !1,
				showSubtext: !1,
				showIcon: !0,
				showContent: !0,
				dropupAuto: !0,
				header: !1,
				liveSearch: !1,
				actionsBox: !1,
				iconBase: 'glyphicon',
				tickIcon: 'glyphicon-ok',
				maxOptions: !1,
				mobile: !1,
				selectOnTab: !1,
				dropdownAlignRight: !1
			}),
			(e.prototype = {
				constructor: e,
				init: function() {
					var e = this,
						n = this.$element.attr('id');
					this.$element.hide(),
						(this.multiple = this.$element.prop('multiple')),
						(this.autofocus = this.$element.prop('autofocus')),
						(this.$newElement = this.createView()),
						this.$element.after(this.$newElement),
						(this.$menu = this.$newElement.find('> .dropdown-menu')),
						(this.$button = this.$newElement.find('> button')),
						(this.$searchbox = this.$newElement.find('input')),
						this.options.dropdownAlignRight && this.$menu.addClass('pull-right'),
						void 0 !== n &&
							(this.$button.attr('data-id', n),
							t('label[for="' + n + '"]').click(function(t) {
								t.preventDefault(), e.$button.focus();
							})),
						this.checkDisabled(),
						this.clickListener(),
						this.options.liveSearch && this.liveSearchListener(),
						this.render(),
						this.liHeight(),
						this.setStyle(),
						this.setWidth(),
						this.options.container && this.selectPosition(),
						this.$menu.data('this', this),
						this.$newElement.data('this', this),
						this.options.mobile && this.mobile();
				},
				createDropdown: function() {
					var e = this.multiple ? ' show-tick' : '',
						n = this.$element.parent().hasClass('input-group') ? ' input-group-btn' : '',
						i = this.autofocus ? ' autofocus' : '',
						o = this.options.header
							? '<div class="popover-title"><button type="button" class="close" aria-hidden="true">&times;</button>' +
								this.options.header +
								'</div>'
							: '',
						r = this.options.liveSearch
							? '<div class="bootstrap-select-searchbox"><input type="text" class="input-block-level form-control" autocomplete="off" /></div>'
							: '',
						s = this.options.actionsBox
							? '<div class="bs-actionsbox"><div class="btn-group btn-block"><button class="actions-btn bs-select-all btn btn-sm btn-default">Select All</button><button class="actions-btn bs-deselect-all btn btn-sm btn-default">Deselect All</button></div></div>'
							: '';
					return t(
						'<div class="btn-group bootstrap-select' +
							e +
							n +
							'"><button type="button" class="btn1 dropdown-toggle selectpicker" data-toggle="dropdown"' +
							i +
							'><span class="filter-option pull-left"></span>&nbsp;<span class="caret"></span></button><div class="dropdown-menu open">' +
							o +
							r +
							s +
							'<ul class="dropdown-menu inner selectpicker" role="menu"></ul></div></div>'
					);
				},
				createView: function() {
					var t = this.createDropdown(),
						e = this.createLi();
					return t.find('ul').append(e), t;
				},
				reloadLi: function() {
					this.destroyLi();
					var t = this.createLi();
					this.$menu.find('ul').append(t);
				},
				destroyLi: function() {
					this.$menu.find('li').remove();
				},
				createLi: function() {
					var e = this,
						n = [],
						i = '',
						o = 0;
					return (
						this.$element.find('option').each(function() {
							var i = t(this),
								r = i.attr('class') || '',
								s = i.attr('style') || '',
								a = i.data('content') ? i.data('content') : i.html(),
								l =
									void 0 !== i.data('subtext')
										? '<small class="muted text-muted">' + i.data('subtext') + '</small>'
										: '',
								c =
									void 0 !== i.data('icon')
										? '<i class="' + e.options.iconBase + ' ' + i.data('icon') + '"></i> '
										: '';
							if (
								('' !== c &&
									(i.is(':disabled') || i.parent().is(':disabled')) &&
									(c = '<span>' + c + '</span>'),
								i.data('content') || (a = c + '<span class="text">' + a + l + '</span>'),
								e.options.hideDisabled && (i.is(':disabled') || i.parent().is(':disabled')))
							)
								n.push('<a style="min-height: 0; padding: 0"></a>');
							else if (i.parent().is('optgroup') && !0 !== i.data('divider'))
								if (0 === i.index()) {
									var u = i.parent().attr('label'),
										d =
											void 0 !== i.parent().data('subtext')
												? '<small class="muted text-muted">' +
													i.parent().data('subtext') +
													'</small>'
												: '';
									(u =
										(i.parent().data('icon')
											? '<i class="' +
												e.options.iconBase +
												' ' +
												i.parent().data('icon') +
												'"></i> '
											: '') +
										'<span class="text">' +
										u +
										d +
										'</span>'),
										(o += 1),
										0 !== i[0].index
											? n.push(
													'<div class="div-contain"><div class="divider"></div></div><dt>' +
														u +
														'</dt>' +
														e.createA(a, 'opt ' + r, s, o)
												)
											: n.push('<dt>' + u + '</dt>' + e.createA(a, 'opt ' + r, s, o));
								} else n.push(e.createA(a, 'opt ' + r, s, o));
							else
								!0 === i.data('divider')
									? n.push('<div class="div-contain"><div class="divider"></div></div>')
									: !0 === t(this).data('hidden') ? n.push('<a></a>') : n.push(e.createA(a, r, s));
						}),
						t.each(n, function(t, e) {
							i +=
								'<li rel="' +
								t +
								'" data-original-index="' +
								t +
								'"' +
								('<a></a>' === e ? ' class="hide is-hidden"' : '') +
								'>' +
								e +
								'</li>';
						}),
						this.multiple ||
							0 !== this.$element.find('option:selected').length ||
							this.options.title ||
							this.$element.find('option').eq(0).prop('selected', !0).attr('selected', 'selected'),
						t(i)
					);
				},
				createA: function(t, e, n, i) {
					return (
						'<a tabindex="0" class="' +
						e +
						'" style="' +
						n +
						'"' +
						(void 0 !== i ? 'data-optgroup="' + i + '"' : '') +
						'>' +
						t +
						'<i class="' +
						this.options.iconBase +
						' ' +
						this.options.tickIcon +
						' icon-ok check-mark"></i></a>'
					);
				},
				render: function(e) {
					var n = this;
					!1 !== e &&
						this.$element.find('option').each(function(e) {
							n.setDisabled(e, t(this).is(':disabled') || t(this).parent().is(':disabled')),
								n.setSelected(e, t(this).is(':selected'));
						}),
						this.tabIndex();
					var i = this.$element
							.find('option:selected')
							.map(function() {
								var e,
									i = t(this),
									o =
										i.data('icon') && n.options.showIcon
											? '<i class="' + n.options.iconBase + ' ' + i.data('icon') + '"></i> '
											: '';
								return (
									(e =
										n.options.showSubtext && i.attr('data-subtext') && !n.multiple
											? ' <small class="muted text-muted">' + i.data('subtext') + '</small>'
											: ''),
									i.data('content') && n.options.showContent
										? i.data('content')
										: void 0 !== i.attr('title') ? i.attr('title') : o + i.html() + e
								);
							})
							.toArray(),
						o = this.multiple ? i.join(this.options.multipleSeparator) : i[0];
					if (this.multiple && -1 < this.options.selectedTextFormat.indexOf('count')) {
						var r = this.options.selectedTextFormat.split('>'),
							s = this.options.hideDisabled ? ':not([disabled])' : '';
						((1 < r.length && i.length > r[1]) || (1 == r.length && 2 <= i.length)) &&
							(o = this.options.countSelectedText
								.replace('{0}', i.length)
								.replace(
									'{1}',
									this.$element.find('option:not([data-divider="true"], [data-hidden="true"])' + s)
										.length
								));
					}
					(this.options.title = this.$element.attr('title')),
						'static' == this.options.selectedTextFormat && (o = this.options.title),
						o || (o = void 0 !== this.options.title ? this.options.title : this.options.noneSelectedText),
						this.$button.attr('title', t.trim(t('<div/>').html(o).text()).replace(/\s\s+/g, ' ')),
						this.$newElement.find('.filter-option').html(o);
				},
				setStyle: function(t, e) {
					this.$element.attr('class') &&
						this.$newElement.addClass(
							this.$element.attr('class').replace(/selectpicker|mobile-device|validate\[.*\]/gi, '')
						);
					var n = t || this.options.style;
					'add' == e
						? this.$button.addClass(n)
						: 'remove' == e
							? this.$button.removeClass(n)
							: (this.$button.removeClass(this.options.style), this.$button.addClass(n));
				},
				liHeight: function() {
					if (!1 !== this.options.size) {
						var t = this.$menu
								.parent()
								.clone()
								.find('> .dropdown-toggle')
								.prop('autofocus', !1)
								.end()
								.appendTo('body'),
							e = t.addClass('open').find('> .dropdown-menu'),
							n = e.find('li > a').outerHeight(),
							i = this.options.header ? e.find('.popover-title').outerHeight() : 0,
							o = this.options.liveSearch ? e.find('.bootstrap-select-searchbox').outerHeight() : 0,
							r = this.options.actionsBox ? e.find('.bs-actionsbox').outerHeight() : 0;
						t.remove(),
							this.$newElement
								.data('liHeight', n)
								.data('headerHeight', i)
								.data('searchHeight', o)
								.data('actionsHeight', r);
					}
				},
				setSize: function() {
					var e,
						n,
						i,
						o = this,
						r = this.$menu,
						s = r.find('.inner'),
						a = this.$newElement.outerHeight(),
						l = this.$newElement.data('liHeight'),
						c = this.$newElement.data('headerHeight'),
						u = this.$newElement.data('searchHeight'),
						d = this.$newElement.data('actionsHeight'),
						h = r.find('li .divider').outerHeight(!0),
						p =
							parseInt(r.css('padding-top')) +
							parseInt(r.css('padding-bottom')) +
							parseInt(r.css('border-top-width')) +
							parseInt(r.css('border-bottom-width')),
						f = this.options.hideDisabled ? ':not(.disabled)' : '',
						m = t(window),
						g = p + parseInt(r.css('margin-top')) + parseInt(r.css('margin-bottom')) + 2,
						v = function() {
							(n = o.$newElement.offset().top - m.scrollTop()), (i = m.height() - n - a);
						};
					if ((v(), this.options.header && r.css('padding-top', 0), 'auto' == this.options.size)) {
						var b = function() {
							var t,
								a = o.$lis.not('.hide');
							v(),
								(e = i - g),
								o.options.dropupAuto &&
									o.$newElement.toggleClass('dropup', i < n && e - g < r.height()),
								o.$newElement.hasClass('dropup') && (e = n - g),
								(t = 3 < a.length + a.find('dt').length ? 3 * l + g - 2 : 0),
								r.css({
									'max-height': e + 'px',
									overflow: 'hidden',
									'min-height': t + c + u + d + 'px'
								}),
								s.css({
									'max-height': e - c - u - d - p + 'px',
									'overflow-y': 'auto',
									'min-height': Math.max(t - p, 0) + 'px'
								});
						};
						b(),
							this.$searchbox
								.off('input.getSize propertychange.getSize')
								.on('input.getSize propertychange.getSize', b),
							t(window).off('resize.getSize').on('resize.getSize', b),
							t(window).off('scroll.getSize').on('scroll.getSize', b);
					} else if (
						this.options.size &&
						'auto' != this.options.size &&
						r.find('li' + f).length > this.options.size
					) {
						var y = r
								.find('li' + f + ' > *')
								.not('.div-contain')
								.slice(0, this.options.size)
								.last()
								.parent()
								.index(),
							w = r.find('li').slice(0, y + 1).find('.div-contain').length;
						(e = l * this.options.size + w * h + p),
							o.options.dropupAuto && this.$newElement.toggleClass('dropup', i < n && e < r.height()),
							r.css({ 'max-height': e + c + u + d + 'px', overflow: 'hidden' }),
							s.css({ 'max-height': e - p + 'px', 'overflow-y': 'auto' });
					}
				},
				setWidth: function() {
					if ('auto' == this.options.width) {
						this.$menu.css('min-width', '0');
						var t = this.$newElement.clone().appendTo('body'),
							e = t.find('> .dropdown-menu').css('width'),
							n = t.css('width', 'auto').find('> button').css('width');
						t.remove(), this.$newElement.css('width', Math.max(parseInt(e), parseInt(n)) + 'px');
					} else
						'fit' == this.options.width
							? (this.$menu.css('min-width', ''), this.$newElement.css('width', '').addClass('fit-width'))
							: this.options.width
								? (this.$menu.css('min-width', ''), this.$newElement.css('width', this.options.width))
								: (this.$menu.css('min-width', ''), this.$newElement.css('width', ''));
					this.$newElement.hasClass('fit-width') &&
						'fit' !== this.options.width &&
						this.$newElement.removeClass('fit-width');
				},
				selectPosition: function() {
					var e,
						n,
						i = this,
						o = t('<div />'),
						r = function(t) {
							o
								.addClass(t.attr('class').replace(/form-control/gi, ''))
								.toggleClass('dropup', t.hasClass('dropup')),
								(e = t.offset()),
								(n = t.hasClass('dropup') ? 0 : t[0].offsetHeight),
								o.css({ top: e.top + n, left: e.left, width: t[0].offsetWidth, position: 'absolute' });
						};
					this.$newElement.on('click', function() {
						i.isDisabled() ||
							(r(t(this)),
							o.appendTo(i.options.container),
							o.toggleClass('open', !t(this).hasClass('open')),
							o.append(i.$menu));
					}),
						t(window).resize(function() {
							r(i.$newElement);
						}),
						t(window).on('scroll', function() {
							r(i.$newElement);
						}),
						t('html').on('click', function(e) {
							t(e.target).closest(i.$newElement).length < 1 && o.removeClass('open');
						});
				},
				setSelected: function(e, n) {
					null == this.$lis && (this.$lis = this.$menu.find('li')),
						t(this.$lis[e]).toggleClass('selected', n);
				},
				setDisabled: function(e, n) {
					null == this.$lis && (this.$lis = this.$menu.find('li')),
						n
							? t(this.$lis[e]).addClass('disabled').find('a').attr('href', '#').attr('tabindex', -1)
							: t(this.$lis[e]).removeClass('disabled').find('a').removeAttr('href').attr('tabindex', 0);
				},
				isDisabled: function() {
					return this.$element.is(':disabled');
				},
				checkDisabled: function() {
					var t = this;
					this.isDisabled()
						? this.$button.addClass('disabled').attr('tabindex', -1)
						: (this.$button.hasClass('disabled') && this.$button.removeClass('disabled'),
							-1 == this.$button.attr('tabindex') &&
								(this.$element.data('tabindex') || this.$button.removeAttr('tabindex'))),
						this.$button.click(function() {
							return !t.isDisabled();
						});
				},
				tabIndex: function() {
					this.$element.is('[tabindex]') &&
						(this.$element.data('tabindex', this.$element.attr('tabindex')),
						this.$button.attr('tabindex', this.$element.data('tabindex')));
				},
				clickListener: function() {
					var e = this;
					this.$newElement.on('touchstart.dropdown', '.dropdown-menu', function(t) {
						t.stopPropagation();
					}),
						this.$newElement.on('click', function() {
							e.setSize(),
								e.options.liveSearch ||
									e.multiple ||
									setTimeout(function() {
										e.$menu.find('.selected a').focus();
									}, 10);
						}),
						this.$menu.on('click', 'li a', function(n) {
							var i = t(this).parent().data('originalIndex'),
								o = e.$element.val(),
								r = e.$element.prop('selectedIndex');
							if (
								(e.multiple && n.stopPropagation(),
								n.preventDefault(),
								!e.isDisabled() && !t(this).parent().hasClass('disabled'))
							) {
								var s = e.$element.find('option'),
									a = s.eq(i),
									l = a.prop('selected'),
									c = a.parent('optgroup'),
									u = e.options.maxOptions,
									d = c.data('maxOptions') || !1;
								if (e.multiple) {
									if (
										(a.prop('selected', !l),
										e.setSelected(i, !l),
										t(this).blur(),
										!1 !== u || !1 !== d)
									) {
										var h = u < s.filter(':selected').length,
											p = d < c.find('option:selected').length,
											f = e.options.maxOptionsText,
											m = f[0].replace('{n}', u),
											g = f[1].replace('{n}', d),
											v = t('<div class="notify"></div>');
										if ((u && h) || (d && p))
											if (u && 1 == u)
												s.prop('selected', !1),
													a.prop('selected', !0),
													e.$menu.find('.selected').removeClass('selected'),
													e.setSelected(i, !0);
											else if (d && 1 == d) {
												c.find('option:selected').prop('selected', !1), a.prop('selected', !0);
												var b = t(this).data('optgroup');
												e.$menu
													.find('.selected')
													.has('a[data-optgroup="' + b + '"]')
													.removeClass('selected'),
													e.setSelected(i, !0);
											} else
												f[2] &&
													((m = m.replace('{var}', f[2][1 < u ? 0 : 1])),
													(g = g.replace('{var}', f[2][1 < d ? 0 : 1]))),
													a.prop('selected', !1),
													e.$menu.append(v),
													u &&
														h &&
														(v.append(t('<div>' + m + '</div>')),
														e.$element.trigger('maxReached.bs.select')),
													d &&
														p &&
														(v.append(t('<div>' + g + '</div>')),
														e.$element.trigger('maxReachedGrp.bs.select')),
													setTimeout(function() {
														e.setSelected(i, !1);
													}, 10),
													v.delay(750).fadeOut(300, function() {
														t(this).remove();
													});
									}
								} else
									s.prop('selected', !1),
										a.prop('selected', !0),
										e.$menu.find('.selected').removeClass('selected'),
										e.setSelected(i, !0);
								e.multiple ? e.options.liveSearch && e.$searchbox.focus() : e.$button.focus(),
									((o != e.$element.val() && e.multiple) ||
										(r != e.$element.prop('selectedIndex') && !e.multiple)) &&
										e.$element.change();
							}
						}),
						this.$menu.on(
							'click',
							'li.disabled a, li dt, li .div-contain, .popover-title, .popover-title :not(.close)',
							function(t) {
								t.target == this &&
									(t.preventDefault(),
									t.stopPropagation(),
									e.options.liveSearch ? e.$searchbox.focus() : e.$button.focus());
							}
						),
						this.$menu.on('click', '.popover-title .close', function() {
							e.$button.focus();
						}),
						this.$searchbox.on('click', function(t) {
							t.stopPropagation();
						}),
						this.$menu.on('click', '.actions-btn', function(n) {
							e.options.liveSearch ? e.$searchbox.focus() : e.$button.focus(),
								n.preventDefault(),
								n.stopPropagation(),
								t(this).is('.bs-select-all') ? e.selectAll() : e.deselectAll(),
								e.$element.change();
						}),
						this.$element.change(function() {
							e.render(!1);
						});
				},
				liveSearchListener: function() {
					var e = this,
						n = t('<li class="no-results"></li>');
					this.$newElement.on('click.dropdown.data-api', function() {
						e.$menu.find('.active').removeClass('active'),
							e.$searchbox.val() &&
								(e.$searchbox.val(''),
								e.$lis.not('.is-hidden').removeClass('hide'),
								n.parent().length && n.remove()),
							e.multiple || e.$menu.find('.selected').addClass('active'),
							setTimeout(function() {
								e.$searchbox.focus();
							}, 10);
					}),
						this.$searchbox.on('input propertychange', function() {
							e.$searchbox.val()
								? (e.$lis
										.not('.is-hidden')
										.removeClass('hide')
										.find('a')
										.not(':icontains(' + e.$searchbox.val() + ')')
										.parent()
										.addClass('hide'),
									e.$menu.find('li').filter(':visible:not(.no-results)').length
										? n.parent().length && n.remove()
										: (n.parent().length && n.remove(),
											n.html(e.options.noneResultsText + ' "' + e.$searchbox.val() + '"').show(),
											e.$menu.find('li').last().after(n)))
								: (e.$lis.not('.is-hidden').removeClass('hide'), n.parent().length && n.remove()),
								e.$menu.find('li.active').removeClass('active'),
								e.$menu
									.find('li')
									.filter(':visible:not(.divider)')
									.eq(0)
									.addClass('active')
									.find('a')
									.focus(),
								t(this).focus();
						}),
						this.$menu.on('mouseenter', 'a', function(n) {
							e.$menu.find('.active').removeClass('active'),
								t(n.currentTarget).parent().not('.disabled').addClass('active');
						}),
						this.$menu.on('mouseleave', 'a', function() {
							e.$menu.find('.active').removeClass('active');
						});
				},
				val: function(t) {
					return void 0 !== t ? (this.$element.val(t), this.render(), this.$element) : this.$element.val();
				},
				selectAll: function() {
					null == this.$lis && (this.$lis = this.$menu.find('li')),
						this.$element.find('option:enabled').prop('selected', !0),
						t(this.$lis).not('.disabled').addClass('selected'),
						this.render(!1);
				},
				deselectAll: function() {
					null == this.$lis && (this.$lis = this.$menu.find('li')),
						this.$element.find('option:enabled').prop('selected', !1),
						t(this.$lis).not('.disabled').removeClass('selected'),
						this.render(!1);
				},
				keydown: function(e) {
					var n,
						i,
						o,
						r,
						s,
						a,
						l,
						c,
						u,
						d,
						h,
						p,
						f = {
							32: ' ',
							48: '0',
							49: '1',
							50: '2',
							51: '3',
							52: '4',
							53: '5',
							54: '6',
							55: '7',
							56: '8',
							57: '9',
							59: ';',
							65: 'a',
							66: 'b',
							67: 'c',
							68: 'd',
							69: 'e',
							70: 'f',
							71: 'g',
							72: 'h',
							73: 'i',
							74: 'j',
							75: 'k',
							76: 'l',
							77: 'm',
							78: 'n',
							79: 'o',
							80: 'p',
							81: 'q',
							82: 'r',
							83: 's',
							84: 't',
							85: 'u',
							86: 'v',
							87: 'w',
							88: 'x',
							89: 'y',
							90: 'z',
							96: '0',
							97: '1',
							98: '2',
							99: '3',
							100: '4',
							101: '5',
							102: '6',
							103: '7',
							104: '8',
							105: '9'
						};
					if (
						((o = (n = t(this)).parent()),
						n.is('input') && (o = n.parent().parent()),
						(d = o.data('this')).options.liveSearch && (o = n.parent().parent()),
						d.options.container && (o = d.$menu),
						(i = t('[role=menu] li:not(.divider) a', o)),
						!(p = d.$menu.parent().hasClass('open')) &&
							/([0-9]|[A-z])/.test(String.fromCharCode(e.keyCode)) &&
							(d.options.container
								? d.$newElement.trigger('click')
								: (d.setSize(), d.$menu.parent().addClass('open'), (p = !0)),
							d.$searchbox.focus()),
						d.options.liveSearch &&
							(/(^9$|27)/.test(e.keyCode.toString(10)) &&
								p &&
								0 === d.$menu.find('.active').length &&
								(e.preventDefault(), d.$menu.parent().removeClass('open'), d.$button.focus()),
							(i = t('[role=menu] li:not(.divider):visible', o)),
							n.val() ||
								/(38|40)/.test(e.keyCode.toString(10)) ||
								(0 === i.filter('.active').length &&
									(i = d.$newElement.find('li').filter(':icontains(' + f[e.keyCode] + ')')))),
						i.length)
					) {
						if (/(38|40)/.test(e.keyCode.toString(10)))
							(r = i.index(i.filter(':focus'))),
								(a = i.parent(':not(.disabled):visible').first().index()),
								(l = i.parent(':not(.disabled):visible').last().index()),
								(s = i.eq(r).parent().nextAll(':not(.disabled):visible').eq(0).index()),
								(c = i.eq(r).parent().prevAll(':not(.disabled):visible').eq(0).index()),
								(u = i.eq(s).parent().prevAll(':not(.disabled):visible').eq(0).index()),
								d.options.liveSearch &&
									(i.each(function(e) {
										t(this).is(':not(.disabled)') && t(this).data('index', e);
									}),
									(r = i.index(i.filter('.active'))),
									(a = i.filter(':not(.disabled):visible').first().data('index')),
									(l = i.filter(':not(.disabled):visible').last().data('index')),
									(s = i.eq(r).nextAll(':not(.disabled):visible').eq(0).data('index')),
									(c = i.eq(r).prevAll(':not(.disabled):visible').eq(0).data('index')),
									(u = i.eq(s).prevAll(':not(.disabled):visible').eq(0).data('index'))),
								(h = n.data('prevIndex')),
								38 == e.keyCode &&
									(d.options.liveSearch && (r -= 1),
									r != u && c < r && (r = c),
									r < a && (r = a),
									r == h && (r = l)),
								40 == e.keyCode &&
									(d.options.liveSearch && (r += 1),
									-1 == r && (r = 0),
									r != u && r < s && (r = s),
									l < r && (r = l),
									r == h && (r = a)),
								n.data('prevIndex', r),
								d.options.liveSearch
									? (e.preventDefault(),
										n.is('.dropdown-toggle') ||
											(i.removeClass('active'),
											i.eq(r).addClass('active').find('a').focus(),
											n.focus()))
									: i.eq(r).focus();
						else if (!n.is('input')) {
							var m,
								g = [];
							i.each(function() {
								t(this).parent().is(':not(.disabled)') &&
									t.trim(t(this).text().toLowerCase()).substring(0, 1) == f[e.keyCode] &&
									g.push(t(this).parent().index());
							}),
								(m = t(document).data('keycount')),
								m++,
								t(document).data('keycount', m),
								t.trim(t(':focus').text().toLowerCase()).substring(0, 1) != f[e.keyCode]
									? ((m = 1), t(document).data('keycount', m))
									: m >= g.length && (t(document).data('keycount', 0), m > g.length && (m = 1)),
								i.eq(g[m - 1]).focus();
						}
						(/(13|32)/.test(e.keyCode.toString(10)) ||
							(d.options.selectOnTab && /(^9$)/.test(e.keyCode.toString(10)))) &&
							p &&
							(/(32)/.test(e.keyCode.toString(10)) || e.preventDefault(),
							d.options.liveSearch
								? /(32)/.test(e.keyCode.toString(10)) || (d.$menu.find('.active a').click(), n.focus())
								: t(':focus').click(),
							t(document).data('keycount', 0)),
							((/(^9$|27)/.test(e.keyCode.toString(10)) && p && (d.multiple || d.options.liveSearch)) ||
								(/(27)/.test(e.keyCode.toString(10)) && !p)) &&
								(d.$menu.parent().removeClass('open'), d.$button.focus());
					}
				},
				mobile: function() {
					this.$element.addClass('mobile-device').appendTo(this.$newElement),
						this.options.container && this.$menu.hide();
				},
				refresh: function() {
					(this.$lis = null),
						this.reloadLi(),
						this.render(),
						this.setWidth(),
						this.setStyle(),
						this.checkDisabled(),
						this.liHeight();
				},
				update: function() {
					this.reloadLi(), this.setWidth(), this.setStyle(), this.checkDisabled(), this.liHeight();
				},
				hide: function() {
					this.$newElement.hide();
				},
				show: function() {
					this.$newElement.show();
				},
				remove: function() {
					this.$newElement.remove(), this.$element.remove();
				}
			});
		var n = t.fn.selectpicker;
		(t.fn.selectpicker = function(n, i) {
			var o,
				r = arguments;
			(n = r[0]), (i = r[1]), [].shift.apply(r);
			var s = this.each(function() {
				var s = t(this);
				if (s.is('select')) {
					var a = s.data('selectpicker'),
						l = 'object' == typeof n && n;
					if (a) {
						if (l) for (var c in l) l.hasOwnProperty(c) && (a.options[c] = l[c]);
					} else {
						var u = t.extend({}, e.DEFAULTS, t.fn.selectpicker.defaults, s.data(), l);
						s.data('selectpicker', (a = new e(this, u, i)));
					}
					'string' == typeof n && (o = a[n] instanceof Function ? a[n].apply(a, r) : a.options[n]);
				}
			});
			return void 0 !== o ? o : s;
		}),
			(t.fn.selectpicker.Constructor = e),
			(t.fn.selectpicker.noConflict = function() {
				return (t.fn.selectpicker = n), this;
			}),
			t(document)
				.data('keycount', 0)
				.on(
					'keydown',
					'.bootstrap-select [data-toggle=dropdown], .bootstrap-select [role=menu], .bootstrap-select-searchbox input',
					e.prototype.keydown
				)
				.on(
					'focusin.modal',
					'.bootstrap-select [data-toggle=dropdown], .bootstrap-select [role=menu], .bootstrap-select-searchbox input',
					function(t) {
						t.stopPropagation();
					}
				);
	})(jQuery);
