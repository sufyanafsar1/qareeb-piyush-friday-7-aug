$(function () {
    'use strict';

    $('.select').click(function(){
        $(this).addClass('active').sebling.removeClass('active');
    })
   

    $('.arrowTopBottomTwo').click(function(){
        $('.hideAndShowTwo').fadeToggle();
    })

    $('.arrowTopBottomOne').click(function(){
        $('.hideAndShowOne').fadeToggle();
    })



    var counter1 = 0;
    var counter2 = 0;
    var counter3 = 0;
    var counter4 = 0;



    $('.typeLink').click(function(){
        
        $('.Typeform').slideToggle();

        if(counter1 == 0)
        {
            $('.typeLink img').css('transform','rotate(0)');
            counter1 = 1;
        }
        else
        {
            $('.typeLink img').css('transform','rotate(-90deg)');
            counter1 = 0;
        }

    })
    $('.Nutritionlink').click(function(){
        
        $('.Nutritionform').slideToggle();

        if(counter2 == 0)
        {
            $('.Nutritionlink img').css('transform','rotate(0)');
            counter2 = 1;
        }
        else
        {
            $('.Nutritionlink img').css('transform','rotate(-90deg)');
            counter2 = 0;
        }

    })
    $('.Brandlink').click(function(){
        
        $('.Brandform').slideToggle();

        if(counter3 == 1)
        {
            $('.Brandlink img').css('transform','rotate(0)');
            counter3 = 0;
        }
        else
        {
            $('.Brandlink img').css('transform','rotate(-90deg)');
            counter3 = 1;
        }
    })
    $('.Packinglink').click(function(){
        
        $('.Packingform').slideToggle();

        if(counter4 == 0)
        {
            $('.Packinglink img').css('transform','rotate(0)');
            counter4 = 1;
        }
        else
        {
            $('.Packinglink img').css('transform','rotate(-90deg)');
            counter4 = 0;
        }
    })

    $('.filter').height($('.height').height() - 75);
    



    (function autoSlider(){

        $('.testimonials .active').each(function(){

            if(!$(this).is(':last-child'))
            {
                $(this).delay(3000).fadeOut(1000, function(){
                    $(this).removeClass('active').next().addClass('active').fadeIn();

                    autoSlider();
                })
            }

            else
            {
                $(this).delay(3000).fadeOut(1000, function(){
                    $(this).removeClass('active');
                    $('.testimonials .opinion').eq(0).addClass('active').fadeIn();
                    autoSlider();
                })
            }

        })

    }());




})
