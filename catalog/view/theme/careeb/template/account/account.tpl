<?php echo $header; ?>
<div class="bt-breadcrumb">
<div class="container-fluid container">
  <ul class="breadcrumb">
    <?php foreach ($breadcrumbs as $breadcrumb) { ?>
      <li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
    <?php } ?>
  </ul>
  </div>
</div>
<div class="container-fluid container">
  <div class="row">
  <?php 
      echo $column_left; 
      echo $column_right;
      if ($column_left && $column_right) {
        $class = 'col-sm-6';
      } elseif ($column_left || $column_right) {
        $class = 'col-sm-9';
      } else {
        $class = 'col-sm-12';
      } 
    ?>
    <div id="content" class="background-white <?php echo $class; ?>"><?php echo $content_top; ?>
		<div class="content_bg">
	  <?php if ($success) { ?>
	  <div class="alert alert-success"><i class="fa fa-check-circle"></i> <?php echo $success; ?></div>
	  <?php } ?>
	  <div class="row">
	  <div class="col-sm-4 col-xs-12">
      <h2 class="title_border"><?php echo $text_my_account; ?></h2>
      <ul class="list-unstyled myaccount">
        <li><a href="<?php echo $edit; ?>"><?php echo $text_edit; ?></a></li>
        <li><a href="<?php echo $password; ?>"><?php echo $text_password; ?></a></li>
        <li><a href="<?php echo $address; ?>"><?php echo $text_address; ?></a></li>
        <li><a href="<?php echo $wishlist; ?>"><?php echo $text_wishlist; ?></a></li>
        <li><a href="javascript:void(0);" class="email_open"><?php echo $text_invite; ?></a></li>
      </ul>
	  </div>
	  <div class="col-sm-4 col-xs-12">
      <h2 class="title_border"><?php echo $text_my_orders; ?></h2>
      <ul class="list-unstyled myaccount">
        <li><a href="<?php echo $order; ?>"><?php echo $text_order; ?></a></li>
        <li><a href="<?php echo $download; ?>"><?php echo $text_download; ?></a></li>
        <?php if ($reward) { ?>
        <li><a href="<?php echo $reward; ?>"><?php echo $text_reward; ?></a></li>
        <?php } ?>
        <li><a href="<?php echo $return; ?>"><?php echo $text_return; ?></a></li>
        <li><a href="<?php echo $transaction; ?>"><?php echo $text_transaction; ?></a></li>
        <li><a href="<?php echo $recurring; ?>"><?php echo $text_recurring; ?></a></li>
      </ul>
	  </div>
	  <div class="col-sm-4 col-xs-12">
      <h2 class="title_border"><?php echo $text_my_newsletter; ?></h2>
      <ul class="list-unstyled myaccount">
        <li><a href="<?php echo $newsletter; ?>"><?php echo $text_newsletter; ?></a></li>
      </ul>
	  </div>
	  </div>
      <?php echo $content_bottom; ?></div></div>
	</div>
</div>
<style type="text/css">
  .model_div {
    z-index: 300;
    background: #ffffff;
    position: absolute;
    padding: 30px;
    width: 350px;
    border-radius: 5px;
    top: 20%;
    left:40%;
  }
  #divLoading.show {
    display: block;
    position: fixed;
    z-index: 100;
    background-color: rgba(0,0,0,0.5);
    left: 0;
    bottom: 0;
    right: 0;
    top: 0;
  }
  #divLoading, .divLoading {
    display: none; 
  }
  #send_email {
    width: 95% !important;
  }
  .close {
    float: right;
}
</style>
<div class="model_div" style="display: none;">
  <button class="close">X</button>
  <h3>Enter Email Address</h3>
  <input type="text" id="send_email" class="form-control" style="margin-bottom: 10px;">
  <div class="boss-button">
    <a href="javascript:void(0)" id="custom"><button class="button email_invite">Send Invite</button></a>
  </div>
</div>
<div id="divLoading" class="hide"></div>
<?php echo $footer; ?>
<script type="text/javascript">
$(document).ready(function(){
  $('.model_div .close').click(function () {
    $("#divLoading").removeClass('show');
    $("#divLoading").addClass('hide');
    $('.model_div').hide();
  });
      
  $('.email_open').click(function () {
    $("#divLoading").removeClass('hide');
    $("#divLoading").addClass('show');
    $('.model_div').show();
  });
  
  $('.email_invite').click(function () {
    $.ajax({
      url: 'index.php?route=account/account/sendInvite',
      type: 'post',
      dataType: 'json',
      data: 'inviteEmail=' + encodeURIComponent($('#send_email').val()),
      beforeSend: function() {
			$('.email_invite').button('loading');
		},
		complete: function() {
			$('.email_invite').button('reset');
		},
      success: function(json) {
        if (json['success']) {
          $("#divLoading").removeClass('show');
          $("#divLoading").addClass('hide');
          $('.model_div').hide();
          alert(json['success']);
        }else{
          alert('Invalid email Entered');
        }
      }
    });
  });
});
</script>