<?php echo $header; ?>
<div class="bt-breadcrumb">
	<div class="container">
		<ul class="breadcrumb">
			<?php foreach ($breadcrumbs as $breadcrumb) { ?>
			<li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
			<?php } ?>
		</ul>
	</div>
</div>

<div class="container-fluid container">
	<div class="">
		<?php echo $column_left; ?>
		
		<?php if ($column_left && $column_right) { ?>
			<?php $class = ' '; ?>
		<?php } elseif ($column_left || $column_right) { ?>
			<?php $class = 'col-sm-9'; ?>
		<?php } else { ?>
			<?php $class = 'col-sm-12'; ?>
		<?php } ?>
		
		<div id="content" class="background-white <?php echo $class; ?>">
		
			<?php echo $content_top; ?>
			
			<div class="content_bg">
			
				<form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data" class="form-horizontal">
				
					<h2 class="accountTitle"><?php echo $heading_title; ?></h2> 
					
					<fieldset  class="accountDiv">
					
						<div class="required col-sm-4">
							<?php if($address_type == 'edit'){ ?>
								<label>EDIT ADDRESS INFORMATION</label>
							<?php } else { ?>
								<label>ADD ADDRESS INFORMATION</label>
							<?php } ?>
						</div>
						<div class="required col-sm-8">
						
							<div class="required col-sm-6">
								<label class="control-label" for="input-firstname" style="display:none;"><?php echo $entry_firstname; ?></label>
								<div>
									<input type="text" name="firstname" value="<?php echo $firstname; ?>" placeholder="<?php echo $entry_firstname; ?>" id="input-firstname" class="form-control" />
									<?php if ($error_firstname) { ?>
										<div class="text-danger"><?php echo $error_firstname; ?></div>
									<?php } ?>
								</div>
							</div>
							
							<div class="required col-sm-6">
								<label class="control-label" for="input-lastname" style="display:none;"><?php echo $entry_lastname; ?></label>
								<div>
									<input type="text" name="lastname" value="<?php echo $lastname; ?>" placeholder="<?php echo $entry_lastname; ?>" id="input-lastname" class="form-control" />
									<?php if ($error_lastname) { ?>
										<div class="text-danger"><?php echo $error_lastname; ?></div>
									<?php } ?>
								</div>
							</div>
							
							<div class="col-sm-12">
								<label class="control-label" for="input-company" style="display:none;"><?php echo $entry_company; ?></label>
								<div>
									<input type="text" name="company" value="<?php echo $company; ?>" placeholder="<?php echo $entry_company; ?>" id="input-company" class="form-control" />
								</div>
							</div>
							
							<div class="required col-sm-12">
								<label class="control-label" for="input-address-1" style="display:none;"><?php echo $entry_address_1; ?></label>
								<div>									
									<textarea type="text" name="address_1" placeholder="<?php echo $entry_address_1; ?>" id="input-address-1" rows="3" class="form-control" /><?php echo $address_1; ?></textarea>
									<?php if ($error_address_1) { ?>
										<div class="text-danger"><?php echo $error_address_1; ?></div>
									<?php } ?>
								</div>
							</div>
					  <!-- Citc Region -->
							<div class="required col-sm-6">
							    <label class="control-label" for="input-zone" style="display:none;">Citc Region</label>
							    <div>
									<select name="citc_region_id" id="select_citc_region" class="form-control">
										<option selected disabled >Select Region</option>
										<?php foreach($citc_regions as $region) { 
											if($region['region_id'] == $citc_region_id) { ?>
												<option selected value="<?php echo $region['region_id'] ?>"><?php echo $region['regionName'] ?></option>
											<?php } else { ?>
												<option value="<?php echo $region['region_id'] ?>"><?php echo $region['regionName'] ?></option>
											<?php } ?>
										<?php } ?>
							        </select>
									<?php if ($error_citc_region_id) { ?>
										<div class="text-danger"><?php echo $error_citc_region_id; ?></div>
									<?php } ?>
							    </div>
							</div>
							<!-- Citc City -->
							<div class="required col-sm-6">
							    <label class="control-label" for="input-zone" style="display:none;">Citc City</label>
							    <div>
							        <select name="citc_city_id" id="select_citc_city" class="form-control">
							            <option selected disabled>Select City</option>
							        </select>
									<?php if ($error_citc_city_id) { ?>
										<div class="text-danger"><?php echo $error_citc_city_id; ?></div>
									<?php } ?>
							    </div>
							</div>
					  
					  		<!-- City & Zone  -->
							<!-- <div class="required col-sm-6">
								<label class="control-label" for="input-city" style="display:none;"><?php echo $entry_city; ?></label>
								<div>
									<input type="text" name="city" value="<?php echo $city; ?>" placeholder="<?php echo $entry_city; ?>" id="input-city" class="form-control" />
									<?php if ($error_city) { ?>
										<div class="text-danger"><?php echo $error_city; ?></div>
									<?php } ?>
								</div>
							</div>
							  
							<div class="required col-sm-6">
								<label class="control-label" for="input-zone" style="display:none;"><?php echo $entry_zone; ?></label>
								<div>
									<select name="zone_id" id="input-zone" class="form-control">
									</select>
									<?php if ($error_zone) { ?>
										<div class="text-danger"><?php echo $error_zone; ?></div>
									<?php } ?>
								</div>
							</div> -->
							
							<!-- Country Id -->
							<!-- <div class="required col-sm-12" style="opacity: 0;height: 0;">
								<label class="control-label" for="input-country" style="display:none;"><?php echo $entry_country; ?></label>
								<div>
									<select name="country_id" id="input-country" class="form-control">
										<option value=""><?php echo $text_select; ?></option>
										<?php foreach ($countries as $country) { ?>
											<?php if ($country['country_id'] == $country_id) { ?>
												<option value="<?php echo $country['country_id']; ?>" selected="selected"><?php echo $country['name']; ?></option>
											<?php } else { ?>
												<option value="<?php echo $country['country_id']; ?>"><?php echo $country['name']; ?></option>
											<?php } ?>
										<?php } ?>
									</select>
									<?php if ($error_country) { ?>
									  <div class="text-danger"><?php echo $error_country; ?></div>
									<?php } ?>
								</div>
							</div> -->
							<!-- Country Id hidden input field -->
							<input type="hidden" name="country_id" value="184" />

						  <?php foreach ($custom_fields as $custom_field) {  ?>
						  <?php if ($custom_field['location'] == 'address') { ?>
						  <?php if ($custom_field['type'] == 'select') { ?>
						  <div class="form-group<?php echo ($custom_field['required'] ? ' required' : ''); ?> custom-field" data-sort="<?php echo $custom_field['sort_order']; ?>">
							<label class="control-label" for="input-custom-field<?php echo $custom_field['custom_field_id']; ?>"><?php echo $custom_field['name']; ?></label>
							<div>
							  <select name="custom_field[<?php echo $custom_field['custom_field_id']; ?>]" id="input-custom-field<?php echo $custom_field['custom_field_id']; ?>" class="form-control">
								<option value=""><?php echo $text_select; ?></option>
								<?php foreach ($custom_field['custom_field_value'] as $custom_field_value) { ?>
								<?php if (isset($address_custom_field[$custom_field['custom_field_id']]) && $custom_field_value['custom_field_value_id'] == $address_custom_field[$custom_field['custom_field_id']]) { ?>
								<option value="<?php echo $custom_field_value['custom_field_value_id']; ?>" selected="selected"><?php echo $custom_field_value['name']; ?></option>
								<?php } else { ?>
								<option value="<?php echo $custom_field_value['custom_field_value_id']; ?>"><?php echo $custom_field_value['name']; ?></option>
								<?php } ?>
								<?php } ?>
							  </select>
							  <?php if (isset($error_custom_field[$custom_field['custom_field_id']])) { ?>
							  <div class="text-danger"><?php echo $error_custom_field[$custom_field['custom_field_id']]; ?></div>
							  <?php } ?>
							</div>
						  </div>
						  <?php } ?>
						  <?php if ($custom_field['type'] == 'radio') { ?>
						  <div class="form-group<?php echo ($custom_field['required'] ? ' required' : ''); ?> custom-field" data-sort="<?php echo $custom_field['sort_order']; ?>">
							<label class="control-label"><?php echo $custom_field['name']; ?></label>
							<div>
							  <div>
								<?php foreach ($custom_field['custom_field_value'] as $custom_field_value) { ?>
								<div class="radio">
								  <?php if (isset($address_custom_field[$custom_field['custom_field_id']]) && $custom_field_value['custom_field_value_id'] == $address_custom_field[$custom_field['custom_field_id']]) { ?>
								  <label>
									<input type="radio" name="custom_field[<?php echo $custom_field['custom_field_id']; ?>]" value="<?php echo $custom_field_value['custom_field_value_id']; ?>" checked="checked" />
									<?php echo $custom_field_value['name']; ?></label>
								  <?php } else { ?>
								  <label>
									<input type="radio" name="custom_field[<?php echo $custom_field['custom_field_id']; ?>]" value="<?php echo $custom_field_value['custom_field_value_id']; ?>" />
									<?php echo $custom_field_value['name']; ?></label>
								  <?php } ?>
								</div>
								<?php } ?>
							  </div>
							  <?php if (isset($error_custom_field[$custom_field['custom_field_id']])) { ?>
							  <div class="text-danger"><?php echo $error_custom_field[$custom_field['custom_field_id']]; ?></div>
							  <?php } ?>
							</div>
						  </div>
						  <?php } ?>
						  <?php if ($custom_field['type'] == 'checkbox') { ?>
						  <div class="form-group<?php echo ($custom_field['required'] ? ' required' : ''); ?> custom-field" data-sort="<?php echo $custom_field['sort_order']; ?>">
							<label class="control-label"><?php echo $custom_field['name']; ?></label>
							<div>
							  <div>
								<?php foreach ($custom_field['custom_field_value'] as $custom_field_value) { ?>
								<div class="checkbox">
								  <?php if (isset($address_custom_field[$custom_field['custom_field_id']]) && in_array($custom_field_value['custom_field_value_id'], $address_custom_field[$custom_field['custom_field_id']])) { ?>
								  <label>
									<input type="checkbox" name="custom_field[<?php echo $custom_field['custom_field_id']; ?>][]" value="<?php echo $custom_field_value['custom_field_value_id']; ?>" checked="checked" />
									<?php echo $custom_field_value['name']; ?></label>
								  <?php } else { ?>
								  <label>
									<input type="checkbox" name="custom_field[<?php echo $custom_field['custom_field_id']; ?>][]" value="<?php echo $custom_field_value['custom_field_value_id']; ?>" />
									<?php echo $custom_field_value['name']; ?></label>
								  <?php } ?>
								</div>
								<?php } ?>
							  </div>
							  <?php if (isset($error_custom_field[$custom_field['custom_field_id']])) { ?>
							  <div class="text-danger"><?php echo $error_custom_field[$custom_field['custom_field_id']]; ?></div>
							  <?php } ?>
							</div>
						  </div>
						  <?php } ?>
						  <?php if ($custom_field['type'] == 'text') { ?>
						  <div class="form-group<?php echo ($custom_field['required'] ? ' required' : ''); ?> custom-field" data-sort="<?php echo $custom_field['sort_order']; ?>">
							<label class="control-label" for="input-custom-field<?php echo $custom_field['custom_field_id']; ?>"><?php echo $custom_field['name']; ?></label>
							<div>
							  <input type="text" name="custom_field[<?php echo $custom_field['custom_field_id']; ?>]" value="<?php echo (isset($address_custom_field[$custom_field['custom_field_id']]) ? $address_custom_field[$custom_field['custom_field_id']] : $custom_field['value']); ?>" placeholder="<?php echo $custom_field['name']; ?>" id="input-custom-field<?php echo $custom_field['custom_field_id']; ?>" class="form-control" />
							  <?php if (isset($error_custom_field[$custom_field['custom_field_id']])) { ?>
							  <div class="text-danger"><?php echo $error_custom_field[$custom_field['custom_field_id']]; ?></div>
							  <?php } ?>
							</div>
						  </div>
						  <?php } ?>
						  <?php if ($custom_field['type'] == 'textarea') { ?>
						  <div class="form-group<?php echo ($custom_field['required'] ? ' required' : ''); ?> custom-field" data-sort="<?php echo $custom_field['sort_order']; ?>">
							<label class="control-label" for="input-custom-field<?php echo $custom_field['custom_field_id']; ?>"><?php echo $custom_field['name']; ?></label>
							<div>
							  <textarea name="custom_field[<?php echo $custom_field['custom_field_id']; ?>]" rows="5" placeholder="<?php echo $custom_field['name']; ?>" id="input-custom-field<?php echo $custom_field['custom_field_id']; ?>" class="form-control"><?php echo (isset($address_custom_field[$custom_field['custom_field_id']]) ? $address_custom_field[$custom_field['custom_field_id']] : $custom_field['value']); ?></textarea>
							  <?php if (isset($error_custom_field[$custom_field['custom_field_id']])) { ?>
							  <div class="text-danger"><?php echo $error_custom_field[$custom_field['custom_field_id']]; ?></div>
							  <?php } ?>
							</div>
						  </div>
						  <?php } ?>
						  <?php if ($custom_field['type'] == 'file') { ?>
						  <div class="form-group<?php echo ($custom_field['required'] ? ' required' : ''); ?> custom-field" data-sort="<?php echo $custom_field['sort_order']; ?>">
							<label class="control-label"><?php echo $custom_field['name']; ?></label>
							<div>
							  <button type="button" id="button-custom-field<?php echo $custom_field['custom_field_id']; ?>" data-loading-text="<?php echo $text_loading; ?>" class="btn btn-default"><i class="fa fa-upload"></i> <?php echo $button_upload; ?></button>
							  <input type="hidden" name="custom_field[<?php echo $custom_field['custom_field_id']; ?>]" value="<?php echo (isset($address_custom_field[$custom_field['custom_field_id']]) ? $address_custom_field[$custom_field['custom_field_id']] : ''); ?>" />
							  <?php if (isset($error_custom_field[$custom_field['custom_field_id']])) { ?>
							  <div class="text-danger"><?php echo $error_custom_field[$custom_field['custom_field_id']]; ?></div>
							  <?php } ?>
							</div>
						  </div>
						  <?php } ?>
						  <?php if ($custom_field['type'] == 'date') { ?>
						  <div class="form-group<?php echo ($custom_field['required'] ? ' required' : ''); ?> custom-field" data-sort="<?php echo $custom_field['sort_order']; ?>">
							<label class="control-label" for="input-custom-field<?php echo $custom_field['custom_field_id']; ?>"><?php echo $custom_field['name']; ?></label>
							<div>
							  <div class="input-group date">
							   <input type="text" name="custom_field[<?php echo $custom_field['custom_field_id']; ?>]" value="<?php echo (isset($address_custom_field[$custom_field['custom_field_id']]) ? $address_custom_field[$custom_field['custom_field_id']] : $custom_field['value']); ?>" placeholder="<?php echo $custom_field['name']; ?>" data-date-format="YYYY-MM-DD" id="input-custom-field<?php echo $custom_field['custom_field_id']; ?>" class="form-control" />
								<span class="input-group-btn">
								<button type="button" class="btn btn-default"><i class="fa fa-calendar"></i></button>
								</span></div>
							  <?php if (isset($error_custom_field[$custom_field['custom_field_id']])) { ?>
							  <div class="text-danger"><?php echo $error_custom_field[$custom_field['custom_field_id']]; ?></div>
							  <?php } ?>
							</div>
						  </div>
						  <?php } ?>
						  <?php if ($custom_field['type'] == 'time') { ?>
						  <div class="form-group<?php echo ($custom_field['required'] ? ' required' : ''); ?> custom-field" data-sort="<?php echo $custom_field['sort_order']; ?>">
							<label class="control-label" for="input-custom-field<?php echo $custom_field['custom_field_id']; ?>"><?php echo $custom_field['name']; ?></label>
							<div>
							  <div class="input-group time">
								<input type="text" name="custom_field[<?php echo $custom_field['custom_field_id']; ?>]" value="<?php echo (isset($address_custom_field[$custom_field['custom_field_id']]) ? $address_custom_field[$custom_field['custom_field_id']] : $custom_field['value']); ?>" placeholder="<?php echo $custom_field['name']; ?>" data-date-format="HH:mm" id="input-custom-field<?php echo $custom_field['custom_field_id']; ?>" class="form-control" />
								<span class="input-group-btn">
								<button type="button" class="btn btn-default"><i class="fa fa-calendar"></i></button>
								</span></div>
							  <?php if (isset($error_custom_field[$custom_field['custom_field_id']])) { ?>
							  <div class="text-danger"><?php echo $error_custom_field[$custom_field['custom_field_id']]; ?></div>
							  <?php } ?>
							</div>
						  </div>
						  <?php } ?>
						  <?php if ($custom_field['type'] == 'datetime') { ?>
						  <div class="form-group<?php echo ($custom_field['required'] ? ' required' : ''); ?> custom-field" data-sort="<?php echo $custom_field['sort_order']; ?>">
							<label class="control-label" for="input-custom-field<?php echo $custom_field['custom_field_id']; ?>"><?php echo $custom_field['name']; ?></label>
							<div>
							  <div class="input-group datetime">
								<input type="text" name="custom_field[<?php echo $custom_field['custom_field_id']; ?>]" value="<?php echo (isset($address_custom_field[$custom_field['custom_field_id']]) ? $address_custom_field[$custom_field['custom_field_id']] : $custom_field['value']); ?>" placeholder="<?php echo $custom_field['name']; ?>" data-date-format="YYYY-MM-DD HH:mm" id="input-custom-field<?php echo $custom_field['custom_field_id']; ?>" class="form-control" />
								<span class="input-group-btn">
								<button type="button" class="btn btn-default"><i class="fa fa-calendar"></i></button>
								</span></div>
							  <?php if (isset($error_custom_field[$custom_field['custom_field_id']])) { ?>
							  <div class="text-danger"><?php echo $error_custom_field[$custom_field['custom_field_id']]; ?></div>
							  <?php } ?>
							</div>
						  </div>
						  <?php } ?>
						  <?php } ?>
						  <?php } ?>
						  
							<div style=" margin-bottom: 10px; margin-top: 6px;float: left;width: 100%;">
								<div class="col-sm-3">
									<label class="control-label"><?php echo $entry_default; ?></label>
								</div>
								<div class="col-sm-9">
									<div>
										<?php if ($default) { ?>
											<label class="radio-inline">
											<input type="radio" name="default" value="1" checked="checked" />
											<?php echo $text_yes; ?></label>
											<label class="radio-inline">
											<input type="radio" name="default" value="0" />
											<?php echo $text_no; ?></label>
										<?php } else { ?>
											<label class="radio-inline">
											<input type="radio" name="default" value="1" />
											<?php echo $text_yes; ?></label>
											<label class="radio-inline">
											<input type="radio" name="default" value="0" checked="checked" />
											<?php echo $text_no; ?></label>
									  <?php } ?>
									</div>
								</div>
							</div>
							
							<div class="buttons clearfix  col-sm-12">								
								<div class="pull-right">
									<input type="submit" value="<?php echo $button_continue; ?>" class="btn btn-shopping" />
									<a href="<?php echo $back; ?>" class="btn"><?php echo $button_back; ?></a>
								</div>
							</div>

						</div>
					</fieldset>
				</form>
			</div>
			<?php echo $content_bottom; ?>
		</div>
	  <?php echo $column_right; ?>
	</div>
</div>
<script type="text/javascript">
$(document).ready(() => {
	let citc_region_id = $('#select_citc_region').val();
	// console.log(citc_region_id);
	// Set city on page load if Region id value is selected
	if(citc_region_id) {
		set_citc_city(citc_region_id);
	}
	// Set city on Region change
	$('#select_citc_region').on('change', function(e) {
		let citc_region_id = $('#select_citc_region').val();
		// console.log(citc_region_id);
		set_citc_city(citc_region_id);
	});
});
function set_citc_city(citc_region_id) {
	$.ajax({
		url: 'index.php?route=account/register/get_citc_city',
		type: 'post',
		data: {citc_region_id: citc_region_id},
		dataType: 'json',
		success: (json) => {
			let output = [];
			console.log('<?php echo $citc_city_id ?>');

			if(!'<?php echo $citc_city_id ?>') {
				output.push(`<option selected disabled > Select City</option>`);
			}
			json.data.forEach((city) => {
				if (city.city_id == '<?php echo $citc_city_id ?>') {
					output.push(`<option selected value="${city.city_id}|${city.city_name}"> ${city.city_name} </option>`);
				} else {
					output.push(`<option value="${city.city_id}|${city.city_name}"> ${city.city_name} </option>`);
				}
			});
			$('#select_citc_city').html(output.join(''));
		},
		error: (error) => {
			console.log(`error: ${error}`)
		}
	});
}
</script>
<script type="text/javascript"><!--
// Sort the custom fields
$('.form-group[data-sort]').detach().each(function() {
	if ($(this).attr('data-sort') >= 0 && $(this).attr('data-sort') <= $('.form-group').length) {
		$('.form-group').eq($(this).attr('data-sort')).before(this);
	} 
	
	if ($(this).attr('data-sort') > $('.form-group').length) {
		$('.form-group:last').after(this);
	}
		
	if ($(this).attr('data-sort') < -$('.form-group').length) {
		$('.form-group:first').before(this);
	}
});
//--></script>
<script type="text/javascript"><!--
$('button[id^=\'button-custom-field\']').on('click', function() {
	var node = this;

	$('#form-upload').remove();

	$('body').prepend('<form enctype="multipart/form-data" id="form-upload" style="display: none;"><input type="file" name="file" /></form>');

	$('#form-upload input[name=\'file\']').trigger('click');
	if (typeof timer != 'undefined') {
    	clearInterval(timer);
	}
	timer = setInterval(function() {
		if ($('#form-upload input[name=\'file\']').val() != '') {
			clearInterval(timer);
		
			$.ajax({
				url: 'index.php?route=tool/upload',
				type: 'post',
				dataType: 'json',
				data: new FormData($('#form-upload')[0]),
				cache: false,
				contentType: false,
				processData: false,
				beforeSend: function() {
					$(node).button('loading');
				},
				complete: function() {
					$(node).button('reset');
				},
				success: function(json) {
					$(node).parent().find('.text-danger').remove();
					
					if (json['error']) {
						$(node).parent().find('input').after('<div class="text-danger">' + json['error'] + '</div>');
					}
	
					if (json['success']) {
						alert(json['success']);
	
						$(node).parent().find('input').attr('value', json['code']);
					}
				},
				error: function(xhr, ajaxOptions, thrownError) {
					alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
				}
			});
		}
	}, 500);
});
//--></script>
<script type="text/javascript"><!--
$('.date').datetimepicker({
	pickTime: false
});

$('.datetime').datetimepicker({
	pickDate: true,
	pickTime: true
});

$('.time').datetimepicker({
	pickDate: false
});
//--></script>
<script type="text/javascript"><!--
$('select[name=\'country_id\']').on('change', function() {
	$.ajax({
		url: 'index.php?route=account/account/country&country_id=' + this.value,
		dataType: 'json',
		beforeSend: function() {
			$('select[name=\'country_id\']').after(' <i class="fa fa-circle-o-notch fa-spin"></i>');
		},
		complete: function() {
			$('.fa-spin').remove();
		},
		success: function(json) {
			if (json['postcode_required'] == '1') {
				$('input[name=\'postcode\']').parent().parent().addClass('required');
			} else {
				$('input[name=\'postcode\']').parent().parent().removeClass('required');
			}
	
			html = '<option value=""><?php echo $text_select; ?></option>';
	
			if (json['zone'] && json['zone'] != '') {
				for (i = 0; i < json['zone'].length; i++) {
					html += '<option value="' + json['zone'][i]['zone_id'] + '"';
	
					if (json['zone'][i]['zone_id'] == '<?php echo $zone_id; ?>') {
						html += ' selected="selected"';
			  		}
	
			  		html += '>' + json['zone'][i]['name'] + '</option>';
				}
			} else {
				html += '<option value="0" selected="selected"><?php echo $text_none; ?></option>';
			}
	
			$('select[name=\'zone_id\']').html(html);
		},
		error: function(xhr, ajaxOptions, thrownError) {
			alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
		}
	});
});

$('select[name=\'country_id\']').trigger('change');
//--></script>
<?php echo $footer; ?>