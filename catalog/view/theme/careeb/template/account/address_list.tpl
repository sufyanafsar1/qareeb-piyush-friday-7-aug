<?php echo $header; ?>
<div class="bt-breadcrumb">
	<div class="container">
		<ul class="breadcrumb">
			<?php foreach ($breadcrumbs as $breadcrumb) { ?>
			<li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
			<?php } ?>
		</ul>
	</div>
</div>

<div class="container-fluid container">

	<div class="">
	
		<?php echo $column_left; ?>
		
		<?php if ($column_left && $column_right) { ?>
			<?php $class = ' '; ?>
		<?php } elseif ($column_left || $column_right) { ?>
			<?php $class = 'col-sm-9'; ?>
		<?php } else { ?>
			<?php $class = 'col-sm-12'; ?>
		<?php } ?>
		
		<div id="content" class="background-white form-horizontal <?php echo $class; ?>">
		
			<?php echo $content_top; ?>
			
			<h2 class="accountTitle">
				<?php echo $text_address_book; ?>						
			</h2>
			
			<div class="content_bg">
			
				<?php if ($success) { ?>
					<div class="alert alert-success"><i class="fa fa-check-circle"></i> <?php echo $success; ?></div>
				<?php } ?>
				
				<?php if ($error_warning) { ?>
					<div class="alert alert-warning"><i class="fa fa-exclamation-circle"></i> <?php echo $error_warning; ?></div>
				<?php } ?>
				
				<fieldset class="accountDiv">
				
					<div class="required col-sm-4">
						<label>MANAGE ADDRESSES</label>
					</div>
					<div class="required col-sm-8" style="border: 1px solid #f2f2f2;">
					
						<?php if ($addresses) { ?>
							<?php foreach ($addresses as $result) { ?>
								<div class="my-address">
									<h4>
										<?php echo $result['name']; ?>
										<span class="pull-right">
											<a href="<?php echo $result['update']; ?>" class="btn btn-primary">
												<i class="fa fa-pencil-square-o" style="font-size: 20px;"></i>
											</a> &nbsp; 
											<a href="<?php echo $result['delete']; ?>" class="btn btn-primary">
												<i class="fa fa-trash-o" style="font-size: 20px;"></i>
											</a>
										</span>
									</h4>
									<div class="addressD">
										<?php echo $result['address']; ?>
									</div>
								</div>
							<?php } ?>	
							
						<?php } else { ?>
							<p><?php echo $text_empty; ?></p>
						<?php } ?>
						
					</div>
				</fieldset>
			</div>
			</br>
			<div class="pull-right"><a href="<?php echo $add; ?>" class="btn btn-shopping"><?php echo $button_new_address; ?></a></div>		
			<?php echo $content_bottom; ?>
		</div>
		<?php echo $column_right; ?>
    </div>
</div>
<?php echo $footer; ?>