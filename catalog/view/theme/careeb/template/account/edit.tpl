<?php echo $header; ?>
<div class="bt-breadcrumb">
	<div class="container-fluid container">
		<ul class="breadcrumb">
			<?php foreach ($breadcrumbs as $breadcrumb) { ?>
			<li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
			<?php } ?>
		</ul>
	</div>
</div>

<div class="container-fluid container">
	<div class="">
		<?php echo $column_left; ?>
		
		<?php if ($column_left && $column_right) { ?>
			<?php $class = ' '; ?>
		<?php } elseif ($column_left || $column_right) { ?>
			<?php $class = 'col-sm-9'; ?>
		<?php } else { ?>
			<?php $class = 'col-sm-12'; ?>
		<?php } ?>
		
		<div id="content" class="background-white <?php echo $class; ?>">
		
			<?php echo $content_top; ?>
			
			<div class="content_bg">
			
				<?php if ($error_warning) { ?>
					<div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> <?php echo $error_warning; ?></div>
				<?php } ?>
				
				<form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data" class="form-horizontal account_form">
							
					<h2 class="accountTitle"><?php echo $text_your_details; ?></h2>
					
					<fieldset class="accountDiv">
						
						<div class="required col-sm-4">
							<label>ACCOUNT INFORMATION</label>
						</div>
						<div class="required col-sm-8">
							<div class="required col-sm-6">
								<label class="control-label" for="input-firstname" style="display:none;"><?php echo $entry_firstname; ?> </label>
								<div>
									<input type="text" name="firstname" value="<?php echo $firstname; ?>" placeholder="<?php echo $entry_firstname; ?>" id="input-firstname" class="form-control" />
									<?php if ($error_firstname) { ?>
										<div class="text-danger"><?php echo $error_firstname; ?></div>
									<?php } ?>
								</div>
							</div>
							
							<div class="required col-sm-6">
								<label class="control-label" for="input-lastname" style="display:none;"><?php echo $entry_lastname; ?></label>
								<div>
									<input type="text" name="lastname" value="<?php echo $lastname; ?>" placeholder="<?php echo $entry_lastname; ?>" id="input-lastname" class="form-control" />
									<?php if ($error_lastname) { ?>
										<div class="text-danger"><?php echo $error_lastname; ?></div>
									<?php } ?>
								</div>
							</div>
								 
							<div class="required col-sm-12">
								<label class="control-label" for="input-email" style="display:none;"><?php echo $entry_email; ?></label>
								<div>
									<input type="email" name="email" value="<?php echo $email; ?>" placeholder="<?php echo $entry_email; ?>" id="input-email" class="form-control" />
									<?php if ($error_email) { ?>
										<div class="text-danger"><?php echo $error_email; ?></div>
									<?php } ?>
								</div>
							</div>
							
							<div class="required col-sm-12">
								<label class="control-label" for="input-telephone" style="display:none;"><?php echo $entry_telephone; ?></label>
								<div>
									<input type="tel" name="telephone" value="<?php echo $telephone; ?>" placeholder="<?php echo $entry_telephone; ?>" id="input-telephone" class="form-control"  pattern="[0-9]{12}"/>
									<?php if ($error_telephone) { ?>
										<div class="text-danger"><?php echo $error_telephone; ?></div>
									<?php } ?>
								</div>
							</div>
								
							<div class="" style="display:none;">
								<label class="control-label" for="input-fax"><?php echo $entry_fax; ?></label>
								<div>
									<input type="text" name="fax" value="<?php echo $fax; ?>" placeholder="<?php echo $entry_fax; ?>" id="input-fax" class="form-control" pattern="[0-9]{12}"/>
								</div>
							</div>
							
							<?php foreach ($custom_fields as $custom_field) { ?>
								<?php if ($custom_field['location'] == 'account') { ?>
								<?php if ($custom_field['type'] == 'select') { ?>
								  <div class="form-group<?php echo ($custom_field['required'] ? ' required' : ''); ?> custom-field" data-sort="<?php echo $custom_field['sort_order']; ?>">
									<label class="control-label" for="input-custom-field<?php echo $custom_field['custom_field_id']; ?>"><?php echo $custom_field['name']; ?></label>
									<div>
									  <select name="custom_field[<?php echo $custom_field['custom_field_id']; ?>]" id="input-custom-field<?php echo $custom_field['custom_field_id']; ?>" class="form-control">
										<option value=""><?php echo $text_select; ?></option>
										<?php foreach ($custom_field['custom_field_value'] as $custom_field_value) { ?>
										<?php if (isset($account_custom_field[$custom_field['custom_field_id']]) && $custom_field_value['custom_field_value_id'] == $account_custom_field[$custom_field['custom_field_id']]) { ?>
										<option value="<?php echo $custom_field_value['custom_field_value_id']; ?>" selected="selected"><?php echo $custom_field_value['name']; ?></option>
										<?php } else { ?>
										<option value="<?php echo $custom_field_value['custom_field_value_id']; ?>"><?php echo $custom_field_value['name']; ?></option>
										<?php } ?>
										<?php } ?>
									  </select>
									  <?php if (isset($error_custom_field[$custom_field['custom_field_id']])) { ?>
									  <div class="text-danger"><?php echo $error_custom_field[$custom_field['custom_field_id']]; ?></div>
									  <?php } ?>
									</div>
								  </div>
								  <?php } ?>
								  <?php if ($custom_field['type'] == 'radio') { ?>
								  <div class="form-group<?php echo ($custom_field['required'] ? ' required' : ''); ?> custom-field" data-sort="<?php echo $custom_field['sort_order']; ?>">
									<label class="control-label"><?php echo $custom_field['name']; ?></label>
									<div>
									  <div>
										<?php foreach ($custom_field['custom_field_value'] as $custom_field_value) { ?>
										<div class="radio">
										  <?php if (isset($account_custom_field[$custom_field['custom_field_id']]) && $custom_field_value['custom_field_value_id'] == $account_custom_field[$custom_field['custom_field_id']]) { ?>
										  <label>
											<input type="radio" name="custom_field[<?php echo $custom_field['custom_field_id']; ?>]" value="<?php echo $custom_field_value['custom_field_value_id']; ?>" checked="checked" />
											<?php echo $custom_field_value['name']; ?></label>
										  <?php } else { ?>
										  <label>
											<input type="radio" name="custom_field[<?php echo $custom_field['custom_field_id']; ?>]" value="<?php echo $custom_field_value['custom_field_value_id']; ?>" />
											<?php echo $custom_field_value['name']; ?></label>
										  <?php } ?>
										</div>
										<?php } ?>
									  </div>
									  <?php if (isset($error_custom_field[$custom_field['custom_field_id']])) { ?>
									  <div class="text-danger"><?php echo $error_custom_field[$custom_field['custom_field_id']]; ?></div>
									  <?php } ?>
									</div>
								  </div>
								  <?php } ?>
								  <?php if ($custom_field['type'] == 'checkbox') { ?>
								  <div class="form-group<?php echo ($custom_field['required'] ? ' required' : ''); ?> custom-field" data-sort="<?php echo $custom_field['sort_order']; ?>">
									<label class="control-label"><?php echo $custom_field['name']; ?></label>
									<div>
									  <div>
										<?php foreach ($custom_field['custom_field_value'] as $custom_field_value) { ?>
										<div class="checkbox">
										  <?php if (isset($account_custom_field[$custom_field['custom_field_id']]) && in_array($custom_field_value['custom_field_value_id'], $account_custom_field[$custom_field['custom_field_id']])) { ?>
										  <label>
											<input type="checkbox" name="custom_field[<?php echo $custom_field['custom_field_id']; ?>][]" value="<?php echo $custom_field_value['custom_field_value_id']; ?>" checked="checked" />
											<?php echo $custom_field_value['name']; ?></label>
										  <?php } else { ?>
										  <label>
											<input type="checkbox" name="custom_field[<?php echo $custom_field['custom_field_id']; ?>][]" value="<?php echo $custom_field_value['custom_field_value_id']; ?>" />
											<?php echo $custom_field_value['name']; ?></label>
										  <?php } ?>
										</div>
										<?php } ?>
									  </div>
									  <?php if (isset($error_custom_field[$custom_field['custom_field_id']])) { ?>
									  <div class="text-danger"><?php echo $error_custom_field[$custom_field['custom_field_id']]; ?></div>
									  <?php } ?>
									</div>
								  </div>
								  <?php } ?>
								  <?php if ($custom_field['type'] == 'text') { ?>
								  <div class="form-group<?php echo ($custom_field['required'] ? ' required' : ''); ?> custom-field" data-sort="<?php echo $custom_field['sort_order']; ?>">
									<label class="control-label" for="input-custom-field<?php echo $custom_field['custom_field_id']; ?>"><?php echo $custom_field['name']; ?></label>
									<div>
									  <input type="text" name="custom_field[<?php echo $custom_field['custom_field_id']; ?>]" value="<?php echo (isset($account_custom_field[$custom_field['custom_field_id']]) ? $account_custom_field[$custom_field['custom_field_id']] : $custom_field['value']); ?>" placeholder="<?php echo $custom_field['name']; ?>" id="input-custom-field<?php echo $custom_field['custom_field_id']; ?>" class="form-control" />
									  <?php if (isset($error_custom_field[$custom_field['custom_field_id']])) { ?>
									  <div class="text-danger"><?php echo $error_custom_field[$custom_field['custom_field_id']]; ?></div>
									  <?php } ?>
									</div>
								  </div>
								  <?php } ?>
								  <?php if ($custom_field['type'] == 'textarea') { ?>
								  <div class="form-group<?php echo ($custom_field['required'] ? ' required' : ''); ?> custom-field" data-sort="<?php echo $custom_field['sort_order']; ?>">
									<label class="control-label" for="input-custom-field<?php echo $custom_field['custom_field_id']; ?>"><?php echo $custom_field['name']; ?></label>
									<div>
									  <textarea name="custom_field[<?php echo $custom_field['custom_field_id']; ?>]" rows="5" placeholder="<?php echo $custom_field['name']; ?>" id="input-custom-field<?php echo $custom_field['custom_field_id']; ?>" class="form-control"><?php echo (isset($account_custom_field[$custom_field['custom_field_id']]) ? $account_custom_field[$custom_field['custom_field_id']] : $custom_field['value']); ?></textarea>
									  <?php if (isset($error_custom_field[$custom_field['custom_field_id']])) { ?>
									  <div class="text-danger"><?php echo $error_custom_field[$custom_field['custom_field_id']]; ?></div>
									  <?php } ?>
									</div>
								  </div>
								  <?php } ?>
								  <?php if ($custom_field['type'] == 'file') { ?>
								  <div class="form-group<?php echo ($custom_field['required'] ? ' required' : ''); ?> custom-field" data-sort="<?php echo $custom_field['sort_order']; ?>">
									<label class="control-label"><?php echo $custom_field['name']; ?></label>
									<div>
									  <button type="button" id="button-custom-field<?php echo $custom_field['custom_field_id']; ?>" data-loading-text="<?php echo $text_loading; ?>" class="btn btn-default"><i class="fa fa-upload"></i> <?php echo $button_upload; ?></button>
									  <input type="hidden" name="custom_field[<?php echo $custom_field['custom_field_id']; ?>]" value="<?php echo (isset($account_custom_field[$custom_field['custom_field_id']]) ? $account_custom_field[$custom_field['custom_field_id']] : ''); ?>" />
									  <?php if (isset($error_custom_field[$custom_field['custom_field_id']])) { ?>
									  <div class="text-danger"><?php echo $error_custom_field[$custom_field['custom_field_id']]; ?></div>
									  <?php } ?>
									</div>
								  </div>
								  <?php } ?>
								  <?php if ($custom_field['type'] == 'date') { ?>
								  <div class="form-group<?php echo ($custom_field['required'] ? ' required' : ''); ?> custom-field" data-sort="<?php echo $custom_field['sort_order']; ?>">
									<label class="control-label" for="input-custom-field<?php echo $custom_field['custom_field_id']; ?>"><?php echo $custom_field['name']; ?></label>
									<div>
									  <div class="input-group date">
										<input type="text" name="custom_field[<?php echo $custom_field['custom_field_id']; ?>]" value="<?php echo (isset($account_custom_field[$custom_field['custom_field_id']]) ? $account_custom_field[$custom_field['custom_field_id']] : $custom_field['value']); ?>" placeholder="<?php echo $custom_field['name']; ?>" data-date-format="YYYY-MM-DD" id="input-custom-field<?php echo $custom_field['custom_field_id']; ?>" class="form-control" />
										<span class="input-group-btn">
										<button type="button" class="btn btn-default"><i class="fa fa-calendar"></i></button>
										</span></div>
									  <?php if (isset($error_custom_field[$custom_field['custom_field_id']])) { ?>
									  <div class="text-danger"><?php echo $error_custom_field[$custom_field['custom_field_id']]; ?></div>
									  <?php } ?>
									</div>
								  </div>
								  <?php } ?>
								  <?php if ($custom_field['type'] == 'time') { ?>
								  <div class="form-group<?php echo ($custom_field['required'] ? ' required' : ''); ?> custom-field" data-sort="<?php echo $custom_field['sort_order']; ?>">
									<label class="control-label" for="input-custom-field<?php echo $custom_field['custom_field_id']; ?>"><?php echo $custom_field['name']; ?></label>
									<div>
									  <div class="input-group time">
										<input type="text" name="custom_field[<?php echo $custom_field['custom_field_id']; ?>]" value="<?php echo (isset($account_custom_field[$custom_field['custom_field_id']]) ? $account_custom_field[$custom_field['custom_field_id']] : $custom_field['value']); ?>" placeholder="<?php echo $custom_field['name']; ?>" data-date-format="HH:mm" id="input-custom-field<?php echo $custom_field['custom_field_id']; ?>" class="form-control" />
										<span class="input-group-btn">
										<button type="button" class="btn btn-default"><i class="fa fa-calendar"></i></button>
										</span></div>
									  <?php if (isset($error_custom_field[$custom_field['custom_field_id']])) { ?>
									  <div class="text-danger"><?php echo $error_custom_field[$custom_field['custom_field_id']]; ?></div>
									  <?php } ?>
									</div>
								  </div>
								  <?php } ?>
								  <?php if ($custom_field['type'] == 'datetime') { ?>
								  <div class="form-group<?php echo ($custom_field['required'] ? ' required' : ''); ?> custom-field" data-sort="<?php echo $custom_field['sort_order']; ?>">
									<label class="control-label" for="input-custom-field<?php echo $custom_field['custom_field_id']; ?>"><?php echo $custom_field['name']; ?></label>
									<div>
									  <div class="input-group datetime">
										<input type="text" name="custom_field[<?php echo $custom_field['custom_field_id']; ?>]" value="<?php echo (isset($account_custom_field[$custom_field['custom_field_id']]) ? $account_custom_field[$custom_field['custom_field_id']] : $custom_field['value']); ?>" placeholder="<?php echo $custom_field['name']; ?>" data-date-format="YYYY-MM-DD HH:mm" id="input-custom-field<?php echo $custom_field['custom_field_id']; ?>" class="form-control" />
										<span class="input-group-btn">
										<button type="button" class="btn btn-default"><i class="fa fa-calendar"></i></button>
										</span></div>
									  <?php if (isset($error_custom_field[$custom_field['custom_field_id']])) { ?>
									  <div class="text-danger"><?php echo $error_custom_field[$custom_field['custom_field_id']]; ?></div>
									  <?php } ?>
									</div>
								  </div>
								  <?php } ?>
								  <?php } ?>
							<?php } ?>
							
							<div class="buttons clearfix col-sm-12">							
								<div class="pull-right">
									<input type="submit" value="<?php echo $button_continue; ?>" class="btn btn-shopping" />
									<a href="<?php echo $back; ?>" class="btn"><?php echo $button_back; ?></a>
								</div>
							</div>
						</div>
						
					</fieldset>					
				</form>
			</div>  
		</div>  
		<?php echo $content_bottom; ?>
	</div>
	<?php echo $column_right; ?>
</div>
<script type="text/javascript"><!--
// Sort the custom fields
$('.form-group[data-sort]').detach().each(function() {
	if ($(this).attr('data-sort') >= 0 && $(this).attr('data-sort') <= $('.form-group').length) {
		$('.form-group').eq($(this).attr('data-sort')).before(this);
	} 
	
	if ($(this).attr('data-sort') > $('.form-group').length) {
		$('.form-group:last').after(this);
	}
		
	if ($(this).attr('data-sort') < -$('.form-group').length) {
		$('.form-group:first').before(this);
	}
});
//--></script> 
<script type="text/javascript"><!--
$('button[id^=\'button-custom-field\']').on('click', function() {
	var node = this;
	
	$('#form-upload').remove();
	
	$('body').prepend('<form enctype="multipart/form-data" id="form-upload" style="display: none;"><input type="file" name="file" /></form>');

	$('#form-upload input[name=\'file\']').trigger('click');
	if (typeof timer != 'undefined') {
    	clearInterval(timer);
	}
	timer = setInterval(function() {
		if ($('#form-upload input[name=\'file\']').val() != '') {
			clearInterval(timer);
			
			$.ajax({
				url: 'index.php?route=tool/upload',
				type: 'post',		
				dataType: 'json',
				data: new FormData($('#form-upload')[0]),
				cache: false,
				contentType: false,
				processData: false,		
				beforeSend: function() {
					$(node).button('loading');
				},
				complete: function() {
					$(node).button('reset');			
				},		
				success: function(json) {
					$(node).parent().find('.text-danger').remove();
					
					if (json['error']) {
						$(node).parent().find('input').after('<div class="text-danger">' + json['error'] + '</div>');
					}
								
					if (json['success']) {
						alert(json['success']);
						
						$(node).parent().find('input').attr('value', json['code']);
					}
				},			
				error: function(xhr, ajaxOptions, thrownError) {
					alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
				}
			});
		}
	}, 500);
});
//--></script>
<script type="text/javascript"><!--
$('.date').datetimepicker({
	pickTime: false
});

$('.datetime').datetimepicker({
	pickDate: true,
	pickTime: true
});

$('.time').datetimepicker({
	pickDate: false
});
//--></script> 
<?php echo $footer; ?>