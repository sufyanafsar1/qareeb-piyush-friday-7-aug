<?php echo $header; ?>
<div class="bt-breadcrumb">
	<div class="container">
		<ul class="breadcrumb">
			<?php foreach ($breadcrumbs as $breadcrumb) { ?>
			<li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
			<?php } ?>
		</ul>
	</div>
</div>

<div class="container-fluid container">
	<div class="">
		<?php echo $column_left; ?>
			
		<?php if ($column_left && $column_right) { ?>
			<?php $class = ' '; ?>
		<?php } elseif ($column_left || $column_right) { ?>
			<?php $class = 'col-sm-9'; ?>
		<?php } else { ?>
			<?php $class = 'col-sm-12'; ?>
		<?php } ?>
	
		<div id="content" class="background-white <?php echo $class; ?>">
			<?php echo $content_top; ?>
			
			<div class="content_bg">
			
				<form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data" class="form-horizontal">
				
					<h2 class="accountTitle"><?php echo $text_password; ?></h2>
					
					<fieldset class="accountDiv">
						<div class="required col-sm-4">
							<label>CHANGE PASSWORD</label>
						</div>
						<div class="required col-sm-8">
							<div class="required col-sm-6">
								<label class="control-label" for="input-password" style="display:none;"><?php echo $entry_password; ?></label>
								<div>
									<input type="password" name="password" value="<?php echo $password; ?>" placeholder="<?php echo $entry_password; ?>" id="input-password" class="form-control" />
									<?php if ($error_password) { ?>
										<div class="text-danger"><?php echo $error_password; ?></div>
									<?php } ?>
								</div>
							</div>
							
							<div class="required col-sm-6">
								<label class="control-label" for="input-confirm" style="display:none;"><?php echo $entry_confirm; ?></label>
								<div>
									<input type="password" name="confirm" value="<?php echo $confirm; ?>" placeholder="<?php echo $entry_confirm; ?>" id="input-confirm" class="form-control" />
									<?php if ($error_confirm) { ?>
										<div class="text-danger"><?php echo $error_confirm; ?></div>
									<?php } ?>
								</div>
							</div>
							
							<div class="buttons clearfix col-sm-12">								
								<div class="pull-right">
									<input type="submit" value="<?php echo $button_continue; ?>" class="btn btn-shopping" />
									<a href="<?php echo $back; ?>" class="btn btn-default"><?php echo $button_back; ?></a>
								</div>
							</div>
						</div>
					</fieldset>
					
					
				</form>
			</div>
			<?php echo $content_bottom; ?>
		</div>
	  <?php echo $column_right; ?>
    </div>
</div>
<?php echo $footer; ?> 