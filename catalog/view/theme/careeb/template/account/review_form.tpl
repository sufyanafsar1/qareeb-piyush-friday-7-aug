<style>
	.accountDiv ul{margin:0;padding:0;}
	.accountDiv li{cursor:pointer;list-style-type: none;display: inline-block;color: #F0F0F0;text-shadow: 0 0 1px #666666;font-size:20px;}
	.accountDiv .highlight, .accountDiv .selected {color:#F4B30A;text-shadow: 0 0 1px #F48F0A;}
</style>

<?php if ($error_warning) { ?>
	<div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> <?php echo $error_warning; ?></div>
<?php } ?>
  
<?php if ($success) { ?>
	<div class="alert alert-success"><i class="fa fa-check-circle"></i> <?php echo $success; ?></div>
<?php } ?>		

<form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data" class="form-horizontal" id="form-review">

	<fieldset class="accountDiv">		
		
		<div class="required col-sm-12">
		
			<label class="control-label" for="input-rating">Rate of service: </label>
									
			<div>	
				<div id="tutorial-rating">
					<input type="hidden" name="rating" id="rating" value="<?php echo $rating; ?>" />
					<ul onMouseOut="resetRating('rating');">
					  <?php  
						$i=0;
						for($i=1;$i<=5;$i++) { 
							$selected = "";
							if(!empty($rating) && $i<=$rating) {
								$selected = "selected";
							}											
						?>
							<li class='<?php echo $selected; ?>' onmouseover="highlightStar(this,'rating');" onmouseout="removeHighlight('rating');" onClick="addRating(this,'rating');">&#9733;</li>  
					  <?php }  ?>
					<ul>
					<?php if ($error_rating) { ?>
						<span class="text-danger"><?php echo $error_rating; ?></span>
					<?php } ?>
				</div>								
			</div>						
		</div>
	
		<div class="required col-sm-12">							
			<label class="control-label" for="input-rate_rider">Rate of rider: </label>
			<div>	
				<div id="tutorial-rate_rider">
					<input type="hidden" name="rate_rider" id="rate_rider" value="<?php echo $rate_rider; ?>" />
					<ul onMouseOut="resetRating('rate_rider');">
					  <?php  
						$i=0;
						for($i=1;$i<=5;$i++) { 
							$selected = "";
							if(!empty($rate_rider) && $i<=$rate_rider) {
								$selected = "selected";
							}											
						?>
							<li class='<?php echo $selected; ?>' onmouseover="highlightStar(this,'rate_rider');" onmouseout="removeHighlight('rate_rider');" onClick="addRating(this,'rate_rider');">&#9733;</li>  
					  <?php }  ?>
					<ul>
					<?php if ($error_rate_rider) { ?>
						<span class="text-danger"><?php echo $error_rate_rider; ?></span>
					<?php } ?>
				</div>								
			</div>				
		</div>								
									
		<div class="required col-sm-12">
			<label class="control-label" for="input-overall_rating">Overall rating: </label>
									
			<div>	
				<div id="tutorial-overall_rating">
					<input type="hidden" name="overall_rating" id="overall_rating" value="<?php echo $overall_rating; ?>" />
					<ul onMouseOut="resetRating('overall_rating');">
					  <?php  
						$i=0;
						for($i=1;$i<=5;$i++) {
							$selected = "";
							if(!empty($overall_rating) && $i<=$overall_rating) {
								$selected = "selected";
							}
						?>
							<li class='<?php echo $selected; ?>' onmouseover="highlightStar(this,'overall_rating');"  onmouseout="removeHighlight('overall_rating');" onClick="addRating(this,'overall_rating');" >&#9733;</li>  
					  <?php }  ?>
					<ul>
					<?php if ($error_overall_rating) { ?>
						<span class="text-danger"><?php echo $error_overall_rating; ?></span>
					<?php } ?>
				</div>								
			</div>						
		</div>
	
		
		<div class="required col-sm-12"  style="display:none;">
			<label class="control-label" for="input-firstname" style="display:none;">Your Name: </label>
			<div>								
				<input type="text" name="author" id="author" readonly  value="<?php echo $author; ?>" id="input-name" class="form-control" />								
			</div>
		</div>
		
		<div class="required col-sm-12">
			<label class="control-label" for="input-firstname" >Your reviews: </label>
			<div>								
				<input type="hidden" name="order_id" id="order_id" value="<?php echo $order_id; ?>" />
				<input type="hidden" name="seller_id" id="seller_id_new" value="<?php echo $seller_id; ?>" />
				<textarea name="text" rows="5" id="input-review" class="form-control"><?php echo $text; ?></textarea>
				<?php if ($error_text) { ?>
				<span class="text-danger"><?php echo $error_text; ?></span>
				<?php } ?>								
			</div>
		</div>
		
		<div class="buttons clearfix col-sm-12">							
			<div class="pull-left">
				<input type="button" id="btnReview" onClick="reviewFunction(this.value);" value="Save" class="btn btn-primary" />				
			</div>
		</div>       
								
	</fieldset>	

</form>											
                       
                                            
<script>
function highlightStar(obj,id) {
	removeHighlight(id);		
	$('.accountDiv #tutorial-'+id+' li').each(function(index) {
		$(this).addClass('highlight');
		if(index == $('.accountDiv #tutorial-'+id+' li').index(obj)) {
			return false;	
		}
	});
}

function removeHighlight(id) {
	$('.accountDiv #tutorial-'+id+' li').removeClass('selected');
	$('.accountDiv #tutorial-'+id+' li').removeClass('highlight');
}

function addRating(obj,id) {
	$('.accountDiv #tutorial-'+id+' li').each(function(index) {
		$(this).addClass('selected');
		$('#tutorial-'+id+' #'+id).val((index+1));
		if(index == $('.accountDiv #tutorial-'+id+' li').index(obj)) {
			return false;	
		}
	});	
}

function resetRating(id) {
	if($('#tutorial-'+id+' #'+id).val() != 0) {
		$('.accountDiv #tutorial-'+id+' li').each(function(index) {
			$(this).addClass('selected');
			if((index+1) == $('#tutorial-'+id+' #'+id).val()) {
				return false;	
			}
		});
	}
} 

function reviewFunction(val){
	
	var rating = $("#rating").val();
	var rate_rider = $("#rate_rider").val();
	var overall_rating = $("#overall_rating").val();
	var author = $("#author").val();
	var order_id = $("#order_id").val();
	var seller_id = $("#seller_id_new").val();
	var text = $("#input-review").val();		
		
	$.ajax({
		type: 'POST',
		dataType: 'json',
		data: { 'rating': rating, 'rate_rider': rate_rider, 'overall_rating': overall_rating, 'author': author, 'order_id': order_id, 'seller_id': seller_id, 'text': text },
		url: 'index.php?route=account/review/review&seller_id='+seller_id+'&order_id='+order_id,
		beforeSend: function () {
			$('#modal-ReviewForm #btnReview').button('loading');
		},
		complete: function () {
			$('#modal-ReviewForm #btnReview').button('reset');
		},
		success: function (json) {
			$('#modal-ReviewForm .form-control').removeClass('has-error');				
			if (json['error_author']) {
				$(".alert-danger").remove();
				$('#modal-ReviewForm .modal-header').after('<div class="alert alert-danger" style="margin:5px;"><i class="fa fa-exclamation-circle"></i> ' + json['error_author'] + '</div>');
				//$('#quick-login #email').addClass('has-error'); 			
			} else if (json['error_rating']) {
				$(".alert-danger").remove();
				$('#modal-ReviewForm .modal-header').after('<div class="alert alert-danger" style="margin:5px;"><i class="fa fa-exclamation-circle"></i> ' + json['error_rating'] + '</div>');
				//$('#quick-login #email').addClass('has-error'); 			
			} else if (json['error_rate_rider']) {
				$(".alert-danger").remove();
				$('#modal-ReviewForm .modal-header').after('<div class="alert alert-danger" style="margin:5px;"><i class="fa fa-exclamation-circle"></i> ' + json['error_rate_rider'] + '</div>');
				//$('#quick-login #email').addClass('has-error'); 			
			} else if (json['error_overall_rating']) {
				$(".alert-danger").remove();
				$('#modal-ReviewForm .modal-header').after('<div class="alert alert-danger" style="margin:5px;"><i class="fa fa-exclamation-circle"></i> ' + json['error_overall_rating'] + '</div>');
				//$('#quick-login #email').addClass('has-error'); 			
			} else if (json['error_text']) {
				$(".alert-danger").remove();
				$('#modal-ReviewForm .modal-header').after('<div class="alert alert-danger" style="margin:5px;"><i class="fa fa-exclamation-circle"></i> ' + json['error_text'] + '</div>');
				//$('#quick-login #email').addClass('has-error'); 			
			} else if (json['error']) {
				$(".alert-danger").remove();
				$('#modal-ReviewForm .modal-header').after('<div class="alert alert-danger" style="margin:5px;"><i class="fa fa-exclamation-circle"></i> ' + json['error'] + '</div>');
				$('#quick-login #email').addClass('has-error');                   
				$('#quick-login #email').focus();
			}
			if (json['success']) {
				$("#orderId"+order_id).val(1);
				$(".alert-danger").remove();
				$('#quick-review').html(json['success']);				
				$('#modal-ReviewForm .modal-header').html('<h2 class="accountTitle" style="display: contents;">Reviews</h2> <a href="index.php?route=account/order" class="close">×</a>');
			} 
		}
	});
}
</script>
<?php //echo $footer; ?>