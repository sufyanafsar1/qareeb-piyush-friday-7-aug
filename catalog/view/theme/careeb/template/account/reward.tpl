<?php echo $header; ?>
<div class="bt-breadcrumb">
<div class="container">
  <ul class="breadcrumb">
    <?php foreach ($breadcrumbs as $breadcrumb) { ?>
      <li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
    <?php } ?>
  </ul>
  </div>
</div>
<div class="container-fluid container">
	<div class="">
		<?php 
		  echo $column_left; 
		  echo $column_right;
		  if ($column_left && $column_right) {
			$class = 'col-sm-6';
		  } elseif ($column_left || $column_right) {
			$class = 'col-sm-9';
		  } else {
			$class = 'col-sm-12';
		  } 
		?>
		
		<div id="content" class="background-white <?php echo $class; ?>">
		
			<?php echo $content_top; ?>
			
			<h2 class="accountTitle">
				<?php echo $heading_title; ?>						
			</h2>
			
			<div class="content_bg">
				<p><?php echo $text_total; ?> <b><?php echo $total; ?></b>.</p>
				
				<fieldset class="accountDiv">
				
					<div class="table-responsive">
						<table class="table table-bordered table-hover">
							<thead>
								<tr>
									<td class="text-left"><?php echo $column_date_added; ?></td>
									<td class="text-left"><?php echo $column_description; ?></td>
									<td class="text-right"><?php echo $column_points; ?></td>
								</tr>
							</thead>
							<tbody>
								<?php 
								if ($rewards) { ?>
									<?php 
									foreach ($rewards  as $reward) { ?>
										<tr>
											<td class="text-left"><?php echo $reward['date_added']; ?></td>
											<td class="text-left"><?php if ($reward['order_id']) { ?>
											<a href="<?php echo $reward['href']; ?>"><?php echo $reward['description']; ?></a>
											<?php } else { ?>
											<?php echo $reward['description']; ?>
											<?php } ?></td>
											<td class="text-right"><?php echo $reward['points']; ?></td>
										</tr>
									<?php 
									} ?>
								<?php 
								} else { ?>
								<tr>
									<td class="text-center" colspan="3"><?php echo $text_empty; ?></td>
								</tr>
								<?php 
								} ?>
							</tbody>
						</table>
					</div>
				</fieldset>
				<div class="row">
					<div class="col-sm-6 text-left"><?php echo $pagination; ?></div>
					<div class="col-sm-6" style="text-align:right;"><?php echo $results; ?></div>
				</div>
				</br>
				<div class="buttons clearfix">
					<div class="pull-right"><a href="<?php echo $continue; ?>" class="btn btn-primary"><?php echo $button_continue; ?></a></div>
				</div>
			</div>
			<?php echo $content_bottom; ?>
		</div>
    </div>
</div>
<?php echo $footer; ?>