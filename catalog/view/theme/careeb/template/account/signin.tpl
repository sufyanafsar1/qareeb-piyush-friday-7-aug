
<div class="container">
    <div class="row">
        <div class="content_bg">
            <div class="row">
                <div class="col-sm-6">
                    <div class="login-content">
<!-- SignIn Start -->
<div id="modal-quicksignIn" class="modal custom-margin-3 ">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header" style="display:none;">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
			</div>
			<div class="modal-body modal-body-custom">
				<div class="row">
					<div class="col-sm-12" id="quick-login">
					   
						<div class="tabbable"> <!-- Only required for left/right tabs -->
							<ul class="nav nav-tabs nav-login" style="display:none;">
								<li><a href="#signup" id="signupTab" data-toggle="tab"><?php echo $text_register; ?></a></li>
								<li class="active"><a id="signinTab" href="#signin" data-toggle="tab"><?php echo $text_login; ?></a></li>								
							</ul>
							
							<div class="tab-content">
								<button type="button" class="close" data-dismiss="modal" aria-hidden="true">
									<img src="catalog/view/theme/careeb/img/x.svg" />
								</button>
								<div class="tab-pane " id="signup">
									<?php echo $signup; ?>
								</div>
								<div class="tab-pane active" id="signin">
									<h3><?php echo $text_log_into_your_account; ?></h3>
									
									<form id="quick-signin" action="<?php echo $action; ?>" method="post" enctype="multipart/form-data" class="form-horizontal">
									
										<div class="accountdetails">
											<fieldset>
												<legend><?php echo $entry_email; ?></legend>
												<input type="text" name="email" value="<?php echo $email; ?>" placeholder="user@gmail.com" id="email" />
											</fieldset>
											
											<fieldset>
												<legend><?php echo $entry_password; ?></legend>						
												<input type="password" name="password" value="<?php echo $password; ?>" placeholder="**********************" id="password"  />
												<a href="javascript:void(0);" id="AIPassword">
													<img class="firstImg" src="catalog/view/theme/careeb/img/show-password-inactive.svg" />
												</a>														
											</fieldset>
											
											<span class="Keep-me-logged-in">
												<input type="checkbox" name="logged" /><?php echo $text_keep_me_login_in; ?>
											</span>
											<span class="Forgot-password">
												<!-- <a class="forgotten" href="<?php echo $forgotten; ?>"><?php echo $text_forgotten; ?></a> -->
												<a class="forgotten" href="javascript:void(0);" id="forgot" title="forgot"><?php echo $text_forgotten; ?></a>
											</span>		
											<script>
												$(document).delegate('#forgot', 'click', function(e) {
													$('#modal-quicksignIn').modal('hide');
													$('#modal-forgot').modal('show');
												});
											</script>

											
										
											<div class="Or-Login-with">
												<span><?php echo $text_or_login_with; ?></span>
											</div>
											
											<div class="SocialIconImage">
												<img src="catalog/view/theme/careeb/img/facebook-login.svg" class="facebook-login">
												<img src="catalog/view/theme/careeb/img/twitter-login.svg" class="twitter-login">
											</div>											
										</div>
										
										<div class="custom-margin-1">
											<input type="button" id="btnLogin" value="<?php echo $button_login; ?>" class="btn button_login btn-primary-custom" />
											<div class="buttons" style="display:none;">
												<div class=""> 
													<a class="forgotten" href="<?php echo $forgotten; ?>"><?php echo $text_forgotten; ?></a>
												</div>	
											</div>
										</div>
										
										<?php 
										if ($redirect) { ?>
											<input type="hidden" name="redirect" value="<?php echo $redirect; ?>" />
										<?php 
										} ?>
									</form>	
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>					
		</div>		
		<p class="text-center signupText"><a href="<?php echo $register; ?>"><?php echo $text_signup; ?></a></p>
	</div>
</div>
<!-- SignIn END -->

<script>
	var clicks = 0;

	$('#AIPassword').click(function() {		
		if (clicks == 0){
			var x = document.getElementById("password");
			x.type = "text";
			$(".firstImg").attr("src","catalog/view/theme/careeb/img/show-password-active.svg");
			clicks = 1;
		} else{
			var x = document.getElementById("password");
			x.type = "password";
			$(".firstImg").attr("src","catalog/view/theme/careeb/img/show-password-inactive.svg");
			clicks = 0;
		}		
	});
</script>

                    </div>
                </div>
            </div>
        </div>

    </div>
</div>
