<script type="text/javascript">
	getLocation();
</script>

<form action="<?php echo $action; ?>" id="quick-register" method="post" enctype="multipart/form-data" class="form-horizontal register">

	<fieldset>
		
		<div class="col-sm-6 col-xs-12">
			
			<div class="form-group required">

				<label class="control-label" for="input-firstname"><?php echo $entry_firstname; ?></label>

				<div>

					<input type="text" name="firstname" value="<?php echo $firstname; ?>" placeholder="<?php echo $entry_firstname; ?>" id="input-firstname" class="form-control" />

					<?php if ($error_firstname) { ?>

						<div class="text-danger"><?php echo $error_firstname; ?></div>

					<?php } ?>

				</div>

			</div>

		</div>

		<div class="col-sm-6 col-xs-12">

			<div class="form-group required">

				<label class="control-label" for="input-lastname"><?php echo $entry_lastname; ?></label>

				<div>

					<input type="text" name="lastname" value="<?php echo $lastname; ?>" placeholder="<?php echo $entry_lastname; ?>" id="input-lastname" class="form-control" />

					<?php if ($error_lastname) { ?>

						<div class="text-danger"><?php echo $error_lastname; ?></div>

					<?php } ?>

				</div>

			</div>

		</div>

		<div class="col-sm-6 col-xs-12">

			<div class="form-group required">

				<label class="control-label" for="input-email"><?php echo $entry_email; ?></label>

				<div>

					<input type="email" name="email" value="<?php echo $email; ?>" placeholder="<?php echo $entry_email; ?>" class="form-control" />

					<?php if ($error_email) { ?>

						<div class="text-danger"><?php echo $error_email; ?></div>

					<?php } ?>

				</div>

			</div>

		</div>

		<div class="col-sm-6 col-xs-12">

			<div class="form-group required">

				<label class="control-label" for="input-telephone"><?php echo $entry_telephone; ?></label>

				<div>
		
					<input type="tel" name="telephone" value="<?php echo $telephone; ?>" placeholder="<?php echo $mobile_format; ?>"  id="input-telephone" class="form-control" />

					<?php if ($error_telephone) { ?>

						<div class="text-danger"><?php echo $error_telephone; ?></div>

					<?php } ?>
		
				</div>

			</div>        

		</div>

		<div class="col-sm-12 col-xs-12">          

			<div class="form-group required">

				<label class="control-label" for="input-address-1"><?php echo $entry_address_1; ?></label>

				<div>

					<input type="text" name="address_1" value="<?php echo $address_1; ?>" placeholder="<?php echo $entry_address_1; ?>" id="input-address-1" class="form-control" />

					<?php if ($error_address_1) { ?>

						<div class="text-danger"><?php echo $error_address_1; ?></div>

					<?php } ?>

				</div>

			</div>               

		</div>

		<div class="col-sm-6 col-xs-12" style="display:none">          

			<div class="form-group required">

				<label class="control-label" for="input-city"><?php echo $entry_city; ?></label>

				<div>

					<input type="text" name="city" value="" placeholder="$entry_city" id="input-city" class="form-control" />

				</div>

			</div>               

		</div>

		<div class="col-sm-6 col-xs-12" style="display:none">          

			<div class="form-group required">

				<label class="control-label" for="input-group">Group ID</label>

				<div>

					<input type="text" name="customer_group_id" value="5"  id="input-group-id" class="form-control" />

				</div>

			</div>               

		</div>

		<div class="col-sm-6 col-xs-12" style="display:none">          

			<div class="form-group required">

				<label class="control-label" for="input-zone">Zone ID</label>

				<div>

					<input type="text" name="zone_id" value="2879"  id="input-zone-id" class="form-control" />

				</div>

			</div>               

		</div>

         
	</fieldset>


	<?php if ($text_agree) { ?>

        <div class="buttons">

			<div class="pull-left">

				<?php if ($agree) { ?>

				<input type="checkbox" name="agree" value="1" checked="checked" />

				<?php } else { ?>

				<input type="checkbox" name="agree" value="1" />

				<?php } ?>

				&nbsp;<?php echo $text_agree; ?> <br/><br/>

			</div>

        </div>

	<?php } else { ?>

        <div class="buttons">

			<div class="pull-left">

			</div>

        </div>

	<?php } ?>

	<div class="custom-margin-1">

		<input type="button" id="btnSignup" value="<?php echo $button_continue; ?>" class="btn btn-primary-custom" />

	</div>

</form>