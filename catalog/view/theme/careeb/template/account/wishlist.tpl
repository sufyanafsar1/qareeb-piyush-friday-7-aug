<?php echo $header; ?>
<style type="text/css">
	.wishlist_active a {color: #ffffff !important;}
	.wishlist_active {background: #8bc42f;}
	.wishlist_cat { font-size: 20px; padding: 10px;}
</style>

<div class="bt-breadcrumb">
	<div class="container-fluid container">
		<ul class="breadcrumb">
			<?php foreach ($breadcrumbs as $breadcrumb) { ?>
			<li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
			<?php } ?>
		</ul>
	</div>
</div>

<div class="container-fluid container">
	<div class="">
		<?php echo $column_left; ?>
		
		<?php if ($column_left && $column_right) { ?>
			<?php $class = ' '; ?>
		<?php } elseif ($column_left || $column_right) { ?>
			<?php $class = 'col-sm-9'; ?>
		<?php } else { ?>
			<?php $class = 'col-sm-12'; ?>
		<?php } ?>
		
		<div id="content" class="background-white form-horizontal <?php echo $class; ?>">
		
			<?php echo $content_top; ?>
			
			<h2 class="accountTitle">
				<?php echo $heading_title; ?>							
			</h2>
			
			<div class="content_bg">
			
				<?php if ($success) { ?>
					<div class="alert alert-success"><i class="fa fa-check-circle"></i> <?php echo $success; ?>
						<button type="button" class="close" data-dismiss="alert">&times;</button>
					</div>
				<?php } ?>
				
				<fieldset class="accountDiv">
				
				<?php 
				if ($products) { ?>
					<div class="wishlist-info" style="border: 1px solid #f2f2f2;">
						<table class="table" style="width: 100%;">
							<thead>
								<tr>
									<td class="image"><?php echo $column_name; ?></td>
									<td class="model"><?php echo $column_model; ?></td>
									<td class="product_price text-right"><?php echo $column_price; ?></td>
									<td class="stock text-center"><?php echo $column_stock; ?></td>
									<td class="remove text-center"><?php echo $button_remove; ?></td>
									<td class="action text-center"><?php echo $column_action; ?></td>
								</tr>
							</thead>        						
							<tbody>
							<?php 
							foreach ($products as $product) { ?>
								<tr>
									<td class="thumb_image">
										<div class="visible-xs title_column"><?php echo $column_name; ?></div>
										<?php if ($product['thumb']) { ?>
											<div class="image">
												<a href="<?php echo $product['href']; ?>">
													<img src="<?php echo $product['thumb']; ?>" alt="<?php echo $product['name']; ?>" title="<?php echo $product['name']; ?>" />
												</a>
											</div>
										<?php } ?>
										<div class="name">
											<a href="<?php echo $product['href']; ?>">
												<b><?php echo $product['name']; ?></b>
											</a> 
											<br /> 
											<b><?php echo $text_seller; ?></b> <?php echo $product['seller_name']; ?>
											<br />
											<b><?php echo $text_category; ?></b> <?php echo $product['category_name']; ?>
										</div>
									</td>
									
									<td class="model">
										<div class="visible-xs title_column"><?php echo $column_model; ?></div>
										<?php echo $product['model']; ?>
									</td>
									
									<td class="product_price">
										<div class="visible-xs title_column"><?php echo $column_price; ?></div>
										<?php if ($product['price']) { ?>
											<div class="price">
												<?php if (!$product['special']) { ?>
													<?php echo $product['price']; ?>
												<?php } else { ?>
													<span class="price-old"><?php echo $product['price']; ?></span>
													</br>
													<span class="price-new"><?php echo $product['special']; ?></span> 
												<?php } ?>
											</div>
										<?php } ?>
									</td>
									
									<td class="stock text-center">
										<div class="visible-xs title_column"><?php echo $column_stock; ?></div>
										<?php echo $product['stock']; ?>
									</td>
									
									<td class="remove text-center">
										<div class="visible-xs title_column"><?php echo $button_remove; ?></div>
										<a href="<?php echo $product['remove']; ?>" data-toggle="tooltip" title="<?php echo $button_remove; ?>" class="button_remove"><i class="fa fa-close"></i></a>
									</td>
									
									<td class="action">
										<div class="visible-xs title_column"><?php echo $column_action; ?></div>
										<!--<div class="cart_button" id="pro_div_id_<?php echo $product['product_id']; ?>">
										<button type="button" onclick="btadd.cart('<?php echo $product['product_id']; ?>',1,<?php echo $product['seller_id']; ?>);" title="<?php echo $button_cart; ?>" class="btn button_cart"><?php echo $button_cart; ?></button>
										</div> -->
										<div class="text-center">
							   
											<div id="pro_div_id_<?php echo $product['product_id']?>" class="add_to_cart_btn" style="margin: 0;">
												<?php  
												if (count($data['product_data']['cart_data'])):
													$count = 0;
													foreach ($data['product_data']['cart_data'] as $key => $value): 
														if (($value['product_id'] == $product['product_id']) && ($value['seller_id'] ==  $product['seller_id'])):
															$count = 1; 
															$quantity = $value['quantity'];
															$cart_id = $value['cart_id'];
															break;
														endif;
													endforeach;
													
													if ($count == 1): ?>
														<div style='position:relative'>
															<a href="javascript:void(0)"><input  onchange="update_cart_data_home('<?php echo $cart_id; ?>', '<?php echo 'select-number'.$cart_id;?>');" id="select-number<?php echo $cart_id; ?>" disabled value="<?php echo $quantity; ?>"/></a>
															  
															<a style='position:absolute;left: 40px;top: 24px;color: white' onclick="changeQty('<?php echo 'select-number'.$cart_id;?>',0,'<?php echo $product['seller_id']; ?>'); return false;" class="decrease" href="javascript:void(0)">_</a>
															  
															<a style='position:absolute;right: 40px;top: 31px;color: white' onclick="changeQty('<?php echo 'select-number'.$cart_id;?>',1,'<?php echo $product['seller_id']; ?>'); return false;" class="increase" href="javascript:void(0)">+</a>
														</div>
													<?php else: ?>
														<a href="javascript:void(0)" id="custom" onclick="btadd.cart('<?php echo $product['product_id']; ?>', '<?php echo $product['minimum']; ?>','<?php echo $product['seller_id']; ?>');"><button ><?php echo $button_cart; ?></button></a>  
													<?php endif ?>
													
												<?php else: ?>
													<a href="javascript:void(0)" id="custom" onclick="btadd.cart('<?php echo $product['product_id']; ?>', '<?php echo $product['minimum']; ?>','<?php echo $product['seller_id']; ?>');"><button ><?php echo $button_cart; ?></button></a> 
												<?php endif ?>
											</div>                      
									   </div>
									</td>	
								</tr>
							<?php 
							} ?>
							</tbody>        
						</table>
					</div>
				<?php 
				} else { ?>
					<p><?php echo $text_empty; ?></p>
				<?php 
				} ?>
				</div>
				</br>
				<div class="buttons clearfix">
					<div class="pull-left"><a href="<?php echo $continue; ?>" class="btn"><?php echo $button_continue; ?></a></div>
				</div>
			</div>
			<?php echo $content_bottom; ?>
		</div>
    </div>
</div>
<?php echo $footer; ?> 