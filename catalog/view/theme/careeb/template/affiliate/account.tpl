<?php echo $header; ?>
<div class="bt-breadcrumb">
<div class="container">
  <ul class="breadcrumb">
    <?php foreach ($breadcrumbs as $breadcrumb) { ?>
      <li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
    <?php } ?>
  </ul>
  </div>
</div>
<div class="container">

  <div class="row"><?php 
      echo $column_left; 
      echo $column_right;
      if ($column_left && $column_right) {
        $class = 'col-sm-6';
      } elseif ($column_left || $column_right) {
        $class = 'col-sm-9';
      } else {
        $class = 'col-sm-12';
      } 
    ?>

    <div id="content" class="background-white <?php echo $class; ?>"><?php echo $content_top; ?>

	<div class="content_bg">

      <h2 class="title_border"><?php echo $text_my_account; ?></h2>

  <?php if ($success) { ?>

  <div class="alert alert-success"><i class="fa fa-check-circle"></i> <?php echo $success; ?></div>

  <?php } ?>

      <ul class="list-unstyled myaccount">

        <li><a href="<?php echo $edit; ?>"><?php echo $text_edit; ?></a></li>

        <li><a href="<?php echo $password; ?>"><?php echo $text_password; ?></a></li>

        <li><a href="<?php echo $payment; ?>"><?php echo $text_payment; ?></a></li>

      </ul>

      <h2 class="title_border"><?php echo $text_my_tracking; ?></h2>

      <ul class="list-unstyled myaccount">

        <li><a href="<?php echo $tracking; ?>"><?php echo $text_tracking; ?></a></li>

      </ul>

      <h2 class="title_border"><?php echo $text_my_transactions; ?></h2>

      <ul class="list-unstyled myaccount">

        <li><a href="<?php echo $transaction; ?>"><?php echo $text_transaction; ?></a></li>

      </ul>

      <?php echo $content_bottom; ?></div></div>

    </div>

</div>

<?php echo $footer; ?>