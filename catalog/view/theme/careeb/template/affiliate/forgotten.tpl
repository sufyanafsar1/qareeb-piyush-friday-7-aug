<?php echo $header; ?>

<div class="container">

  <div class="row"><?php 
      echo $column_left; 
      echo $column_right;
      if ($column_left && $column_right) {
        $class = 'col-sm-6';
      } elseif ($column_left || $column_right) {
        $class = 'col-sm-9';
      } else {
        $class = 'col-sm-12';
      } 
    ?>

    <div id="content" class="background-white <?php echo $class; ?>"><?php echo $content_top; ?>

  <?php if ($error_warning) { ?>

  <div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> <?php echo $error_warning; ?></div>

  <?php } ?>

  <div class="content_bg">

      <p><?php echo $text_email; ?></p>

      <form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data" class="form-horizontal">

        <fieldset>

          <h2><?php echo $text_your_email; ?></h2>

          <div>

            <label class="control-label" for="input-email"><?php echo $entry_email; ?></label>

            <div>

              <input type="text" name="email" value="" placeholder="<?php echo $entry_email; ?>" id="input-email" class="form-control" />

            </div>

          </div>

        </fieldset>

        <div class="buttons clearfix">

          <div class="pull-left"><a href="<?php echo $back; ?>" class="btn"><?php echo $button_back; ?></a></div>

          <div class="pull-right">

            <input type="submit" value="<?php echo $button_continue; ?>" class="btn btn-shopping" />

          </div>

        </div>

      </form>

      <?php echo $content_bottom; ?></div></div>

    </div>

</div>

<?php echo $footer; ?>