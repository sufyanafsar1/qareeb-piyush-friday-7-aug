<?php echo $header; ?>
<div class="container">

  <div class="row"><?php 
      echo $column_left; 
      echo $column_right;
      if ($column_left && $column_right) {
        $class = 'col-sm-6';
      } elseif ($column_left || $column_right) {
        $class = 'col-sm-9';
      } else {
        $class = 'col-sm-12';
      } 
    ?>
    <div id="content" class="background-white <?php echo $class; ?>"><?php echo $content_top; ?>

	  <div class="content_bg">

      <form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data" class="form-horizontal">

        <fieldset>

          <h2><?php echo $text_password; ?></h2>

          <div>

            <label class="control-label" for="input-password"><?php echo $entry_password; ?></label>

            <div>

              <input type="password" name="password" value="<?php echo $password; ?>" placeholder="<?php echo $entry_password; ?>" id="input-password" class="form-control" />

              <?php if ($error_password) { ?>

              <div class="text-danger"><?php echo $error_password; ?></div>

              <?php } ?>

            </div>

          </div>

          <div>

            <label class="control-label" for="input-confirm"><?php echo $entry_confirm; ?></label>

            <div>

              <input type="password" name="confirm" value="<?php echo $confirm; ?>" placeholder="<?php echo $entry_confirm; ?>" id="input-confirm" class="form-control" />

              <?php if ($error_confirm) { ?>

              <div class="text-danger"><?php echo $error_confirm; ?></div>

              <?php } ?>

            </div>

          </div>

        </fieldset>

        <div class="buttons clearfix">

          <div class="pull-left"><a href="<?php echo $back; ?>" class="btn btn-default"><?php echo $button_back; ?></a></div>

          <div class="pull-right">

            <input type="submit" value="<?php echo $button_continue; ?>" class="btn btn-shopping" />

          </div>

        </div>

      </form>

      <?php echo $content_bottom; ?></div></div>

    </div>

</div>

<?php echo $footer; ?>