<?php echo $header; ?> 

<?php global $config; ?>
<div class="bt-breadcrumb">
<div class="container">
  <ul class="breadcrumb">
    <?php foreach ($breadcrumbs as $breadcrumb) { ?>
      <li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
    <?php } ?>
  </ul>
  </div>
</div>
<div class="container">

<div class="row"><?php 
      echo $column_left; 
      echo $column_right;
      if ($column_left && $column_right) {
        $class = 'col-sm-6';
      } elseif ($column_left || $column_right) {
        $class = 'col-sm-9';
      } else {
        $class = 'col-sm-12';
      } 
    ?>

	<div id="content" class="background-white <?php echo $class; ?>"><?php echo $content_top; ?>

	  <?php if ($thumb || $description) { ?>

		<div class="content_bg">

			<?php if ($thumb) { ?>

			<div class="img_thumb"><img src="<?php echo $thumb; ?>" alt="<?php echo $heading_title; ?>" title="<?php echo $heading_title; ?>" /></div>

			<?php } ?>

			<?php if ($description) { ?>

			<div class="description"><?php echo $description; ?></div>

			<?php } ?>

		</div>

      <?php } ?>

      <?php if ($blogcategories) { ?>

	  <div class="content_bg category-list">

      <h3><?php echo $text_sub_category; ?></h3>

      <?php if (count($blogcategories) <= 5) { ?>

          <ul>

            <?php foreach ($blogcategories as $category) { ?>

            <li><a href="<?php echo $category['href']; ?>"><?php echo $category['name']; ?></a></li>

            <?php } ?>

          </ul>

      <?php } else { ?>

        <?php foreach (array_chunk($blogcategories, ceil(count($blogcategories) / 4)) as $blogcategories) { ?>

          <ul>

            <?php foreach ($blogcategories as $category) { ?>

            <li><a href="<?php echo $category['href']; ?>"><?php echo $category['name']; ?></a></li>

            <?php } ?>

          </ul>

        <?php } ?>

      <?php } ?>

	  </div>

      <?php } ?>

	  <?php if ($articles) { ?>      

      <div class="product-filter col-md-12">

		<div class="rss-feed">

			<a href="<?php echo $link_rss; ?>" title='RSS'><img src='image/catalog/bossblog/rss.png' alt='Rss' /></a>

		</div>	

        <div class="sortedBy sort pull-right">

			<label class="control-label" for="input-sort"><?php echo $text_sort; ?></label>

			  <select id="input-sort" onchange="location = this.value;">

				<?php foreach ($sorts as $sorts) { ?>

				<?php if ($sorts['value'] == $sort . '-' . $order) { ?>

				<option value="<?php echo $sorts['href']; ?>" selected="selected"><?php echo $sorts['text']; ?></option>

				<?php } else { ?>

				<option value="<?php echo $sorts['href']; ?>"><?php echo $sorts['text']; ?></option>

				<?php } ?>

				<?php } ?>

			  </select>

        </div>

        <div class="sortedBy pull-left limit">

			<label class="control-label" for="input-limit"><?php echo $text_limit; ?></label>

			  <select id="input-limit" onchange="location = this.value;">

				<?php foreach ($limits as $limits) { ?>

				<?php if ($limits['value'] == $limit) { ?>

				<option value="<?php echo $limits['href']; ?>" selected="selected"><?php echo $limits['text']; ?></option>

				<?php } else { ?>

				<option value="<?php echo $limits['href']; ?>"><?php echo $limits['text']; ?></option>

				<?php } ?>

				<?php } ?>

			  </select>

        </div>

      </div>

	  <div class="row article-layout">

        <?php foreach ($articles as $article) { ?>

        <div class="product-layout product-grid col-lg-4 col-md-4 col-sm-6 col-xs-12">

          <div class="content_bg">

            <div class="article-image"><a href="<?php echo $article['href']; ?>"><img src="<?php echo $article['thumb']; ?>" alt="<?php echo $article['name']; ?>" title="<?php echo $article['name']; ?>" class="img-responsive" /></a>

			<span class="time-image">

				<?php $date = new DateTime($article['date_modified']);?>

				<small class="time-date"><?php echo $date->format('d');?></small>

				<small class="time-month"><?php echo $date->format('M');?></small>

			</span>

			</div>

            <div class="article_dt">

				<div class="article-name"><a href="<?php echo $article['href']; ?>"><?php echo $article['name']; ?></a></div>

				<div class="article-footer">					

					<span class="time-stamp">

						<?php $date = new DateTime($article['date_modified']);?>

						<small><?php echo $date->format('M d, Y');?></small>

					</span>

					<span>&nbsp; / &nbsp;</span>

					<span class="post-by"><span><?php echo $text_postby;?> <?php echo $article['author']; ?></span></span>	

					<span>&nbsp; / &nbsp;</span>

					<span class="comment-count"><span><?php echo $article['comment']; ?> </span><a href="<?php echo $article['href']; ?>"><?php echo $text_comments;?></a></span>                 

				</div>

				<div class="article-title">

					<p><?php echo $article['title']; ?></p>                   

				</div>

				<div class="read_more">

				<a class="btn" href="<?php echo $article['href']; ?>" title="<?php echo $article['name']; ?>" ><?php echo $text_readmore;?></a>

				</div>

            </div>

          </div>

        </div>

        <?php } ?>

      </div>

      <div class="bt_pagination">

        <?php if(!empty($pagination)){?><div class="links"><?php echo $pagination; ?></div> <?php } ?>

        <div class="results"><?php echo $results; ?></div>

      </div>

      <?php }else{ ?>

	  <p><?php echo $text_empty; ?></p>

	  <?php } ?>

	<?php echo $content_bottom; ?></div>	

    

</div>

</div>        

<?php echo $footer; ?>