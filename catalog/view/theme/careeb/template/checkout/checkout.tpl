<?php echo $header; ?>
<div class="bt-breadcrumb">
	<div class="container-fluid container">
		<ul class="breadcrumb">
			<?php foreach ($breadcrumbs as $breadcrumb) { ?>
				<li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
			<?php } ?>
		</ul>
	</div>
</div>
<div class="container-fluid container wizardDiv"> 
    <?php if ($error_warning) { ?>
        <div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> <?php echo $error_warning; ?>
            <button type="button" class="close" data-dismiss="alert">&times;</button>
        </div>
    <?php } ?>
    <div class=""><?php echo $column_left; ?>
        <?php if ($column_left && $column_right) { ?>
            <?php $class = 'col-sm-6'; ?>
        <?php } elseif ($column_left || $column_right) { ?>
            <?php $class = 'col-sm-9'; ?>
        <?php } else { ?>
            <?php $class = 'col-sm-12'; ?>
        <?php } ?>
        <div id="content" class="<?php echo $class; ?>"><?php echo $content_top; ?>
            <div class="">
                <div class="">
                    <section>
                        <div class="wizard">
                            <div class="col-md-12 checkoutTitle">
                                <h3> Checkout
								<?php // echo $seller_name; ?> </h3>
                            </div>
                            <div class="wizard-inner">
                                <div class="connecting-line"></div>
                                <ul class="nav nav-tabs" role="tablist">
                                    <li role="presentation" class="disabled <?php
                                    if (!$logged && $account != 'guest') {
                                        echo 'active';
                                    }
                                    ?>">
                                        <a href="#step1" data-toggle="tab" aria-controls="step1" role="tab" title="Step 1">
                                            <span class="round-tab">
                                                <i class="fa fa-user"></i>
                                            </span>
                                        </a>
                                    </li>

                                    <!-- <li role="presentation" class="disabled <?php
                                    //if ($logged) {
                                       // echo 'active';
                                    //}
                                    ?>">
                                        <a href="#step2" data-toggle="tab" aria-controls="step2" role="tab" title="Step 2">
                                            <span class="round-tab">
                                                <i class="fa fa-location-arrow"></i>
                                            </span>
                                        </a>
                                    </li> -->
                                    <li role="presentation" class="disabled <?php
                                    if ($logged) {
                                        echo 'active';
                                    }
                                    ?>">
                                        <a href="#step3" data-toggle="tab" aria-controls="step3" role="tab" title="Step 2">
                                            <span class="round-tab">
                                                <i class="fa fa-truck"></i>
                                            </span>
                                        </a>
                                    </li>
                                    <!-- <li role="presentation" class="disabled">
                                        <a href="#step4" data-toggle="tab" aria-controls="step4" role="tab" title="Step 3">
                                            <span class="round-tab">
                                                <i class="fa fa-shirtsinbulk"></i>
                                            </span>
                                        </a>
                                    </li> -->
                                    <li role="presentation" class="disabled">
                                        <a href="#step5" data-toggle="tab" aria-controls="step5" role="tab" title="Step 4">
                                            <span class="round-tab">
                                                <i class="fa fa-money"></i>
                                            </span>
                                        </a>
                                    </li>
                                    <li role="presentation" class="disabled">
                                        <a href="#step6" data-toggle="tab" aria-controls="step6" role="tab" title="Step 5">
                                            <span class="round-tab">
                                                <i class="fa fa-clock-o"></i>
                                            </span>
                                        </a>
                                    </li>
                                    <li role="presentation" class="disabled">
                                        <a href="#step7" data-toggle="tab" aria-controls="step7" role="tab" title="Step 5">
                                            <span class="round-tab">
                                                <i class="fa fa-eye"></i>
                                            </span>
                                        </a>
                                    </li>

                                    <!--<li role="presentation" class="disabled">
										<a href="#complete" data-toggle="tab" aria-controls="complete" role="tab" title="Complete">
											<span class="round-tab">
												<i class="glyphicon glyphicon-ok"></i>
											</span>
										</a>
									</li> -->
                                </ul>
                            </div>
							<div class="tab-content">
								<div class="tab-pane <?php
									if (!$logged && $account != 'guest') {
                                        echo 'active';
                                    }
                                    ?>" role="tabpanel" id="step1">
									<?php // echo $text_checkout_option; ?>
									<div class="panel-collapse" id="collapse-checkout-option">
										<div class="panel-body"></div>
									</div>
								</div>
								<!-- <div class="tab-pane <?php
									//if ($logged) {
                                       // echo 'active';
                                    //}
                                    ?>" role="tabpanel" id="step2">
									<?php //if (!$logged && $account != 'guest') { ?>
										<h3><?php //echo $text_checkout_account; ?></h3>
										<div class="panel-collapse" id="collapse-payment-address">
											<div class="panel-body"></div>
										</div>
									<?php //} else { ?>
										<h3><?php //echo $text_checkout_payment_address; ?></h3>
										<div class="panel-collapse" id="collapse-payment-address">
											<div class="panel-body"></div>
										</div>
									<?php //} ?>
								</div> -->

								<div class="tab-pane <?php
								if ($logged) {
									echo 'active';
								}
								?>" role="tabpanel" id="step3">

									<?php if ($shipping_required) { ?>
										<h3><?php echo $text_checkout_shipping_address; ?></h3>
										<div class="panel-collapse" id="collapse-shipping-address">
											<div class="panel-body"></div>
										</div>    
									<?php } ?>
								</div>

								<!-- <div class="tab-pane" role="tabpanel" id="step5">
									<?php //if ($shipping_required) { ?>
										<h3><?php //echo $text_checkout_shipping_method; ?></h3>
										<div class="panel-collapse" id="collapse-shipping-method">
											<div class="panel-body"></div>
										</div>
									<?php //} ?>
								</div> -->

								<div class="tab-pane" role="tabpanel" id="step5">
									<h3><?php echo $text_checkout_payment_method; ?></h3>
									<div class="panel-collapse" id="collapse-payment-method">
										<div class="panel-body"></div>
									</div>
								</div>

								<div class="tab-pane" role="tabpanel" id="step6">
									<h3><?php echo $text_checkout_datetimeslot; ?></h3>
									<div class="panel-collapse" id="collapse-checkout-datetimeslot">
										<div class="panel-body"></div>
									</div>
								</div>

								<style>
								div.table-responsive .table{width:100%}
								@media screen and (max-width: 767px) {
									div.table-responsive {width: 100%;}
								}
								</style>
								<div class="tab-pane" role="tabpanel" id="step7">
									<h3><?php echo $text_checkout_confirm; ?></h3>
									</br>
									<div class="panel-collapse" id="collapse-checkout-confirm">
										<div class="panel-body"></div>
									</div>
								</div>
								<div class="clearfix"></div>
							</div>
                        </div>
                    </section>
                </div>
            </div>
            <?php echo $content_bottom; ?>
        </div>
        <?php echo $column_right; ?>
    </div>
</div>
<style>
.wizard {
	margin: 20px auto;
	background: #fff;
}

.wizard .nav-tabs {
	position: relative;
	margin: 40px auto;
	margin-bottom: 0;
	border-bottom-color: #e0e0e0;
}

.wizard > div.wizard-inner {
	position: relative;
}

.connecting-line {
	height: 2px;
	background: #e0e0e0;
	position: absolute;
	width: 80%;
	margin: 0 auto;
	left: 0;
	right: 0;
	top: 70%;
	z-index: 1;
}

.wizard .nav-tabs > li.active > a, .wizard .nav-tabs > li.active > a:hover, .wizard .nav-tabs > li.active > a:focus {
	color: #555555;
	cursor: default;
	border: 0;
	border-bottom-color: transparent;
}

span.round-tab {
	width: 70px;
	height: 70px;
	line-height: 70px;
	display: inline-block;
	border-radius: 100px;
	background: #fff;
	border: 2px solid #e0e0e0;
	z-index: 2;
	position: absolute;
	left: 0;
	text-align: center;
	font-size: 25px;
}
span.round-tab i{
	color:#555555;
	font-size: 24px;
	position: absolute;
	width: 30px;
	height: 24px;
	padding: 22px;
	left: 0;
	right: auto;
	top: 0;
}

.wizard li.active span.round-tab i{
	color: #fff;
}

span.round-tab:hover {
	color: #333;
	border: 2px solid #333;
}

.wizard .nav-tabs > li {
	width: 16.6%;
}

.wizard li:after {
	content: " ";
	position: absolute;
	left: 46%;
	opacity: 0;
	margin: 0 auto;
	bottom: 0px;
	border: 5px solid transparent;
	border-bottom-color: #207f00;
	transition: 0.1s ease-in-out;
}

.wizard li.active:after {
	/* content: " ";
	position: absolute;
	left: 46%;
	opacity: 1;
	margin: 0 auto;
	bottom: 0px;
	border: 10px solid transparent;
	border-bottom-color: #207f00; */
}

.wizard .nav-tabs > li a {
	width: 70px;
	height: 70px;
	margin: 20px auto;
	border-radius: 100%;
	padding: 0;
}

.wizard .nav-tabs > li a:hover {
	background: transparent;
}

.wizard h3 {
	margin-top: 0;
}
.wizard ul.nav-tabs{
	 border: none;
}
.tab-content{
	border: none;
	width: 100%;
	padding-top: 0;
}

@media( max-width : 585px ) {

	.wizard {
		width: 90%;
		height: auto !important;
	}

	span.round-tab {
		font-size: 16px;
		width: 50px;
		height: 50px;
		line-height: 50px;
	}

	.wizard .nav-tabs > li a {
		width: 50px;
		height: 50px;
		line-height: 50px;
	}

	.wizard li.active:after {
		content: " ";
		position: absolute;
		left: 35%;
	}
}
</style>  
<script>
    $(document).ready(function () {
        //Initialize tooltips
        $('.nav-tabs > li a[title]').tooltip();

        //Wizard
        $('a[data-toggle="tab"]').on('show.bs.tab', function (e) {

            var $target = $(e.target);

            if ($target.parent().hasClass('disabled')) {
                return false;
            }
            $target.parent().removeClass('disabled');
        });

        $(".next-step").click(function (e) {

            var $active = $('.wizard .nav-tabs li.active');
            $active.next().removeClass('disabled');
            nextTab($active);

        });
        $(".prev-step").click(function (e) {

            var $active = $('.wizard .nav-tabs li.active');
            prevTab($active);

        });
    });

    function nextTab(elem) {
        $(elem).next().find('a[data-toggle="tab"]').click();
    }
    function prevTab(elem) {
        $(elem).prev().find('a[data-toggle="tab"]').click();
    }
</script>

    <script type="text/javascript"><!--
        $(document).on('change', 'input[name=\'account\']', function () {
            if ($('#collapse-payment-address').parent().find('.panel-heading .panel-title > *').is('a')) {
                if (this.value == 'register') {
                    $('#collapse-payment-address').parent().find('.panel-heading .panel-title').html('<a href="#collapse-payment-address" data-toggle="collapse" data-parent="#accordion" class="accordion-toggle"><?php echo $text_checkout_account; ?> <i class="fa fa-caret-down"></i></a>');
                } else {
                    $('#collapse-payment-address').parent().find('.panel-heading .panel-title').html('<a href="#collapse-payment-address" data-toggle="collapse" data-parent="#accordion" class="accordion-toggle"><?php echo $text_checkout_payment_address; ?> <i class="fa fa-caret-down"></i></a>');
                }
            } else {
                if (this.value == 'register') {
                    $('#collapse-payment-address').parent().find('.panel-heading .panel-title').html('<?php echo $text_checkout_account; ?>');
                } else {
                    $('#collapse-payment-address').parent().find('.panel-heading .panel-title').html('<?php echo $text_checkout_payment_address; ?>');
                }
            }
        });

        <?php if (!$logged) { ?>
            $(document).ready(function () {
                $.ajax({
                    url: 'index.php?route=checkout/login',
                    dataType: 'html',
                    success: function (html) {
                        $('#collapse-checkout-option .panel-body').html(html);

                        $('#collapse-checkout-option').parent().find('.panel-heading .panel-title').html('<a href="#collapse-checkout-option" data-toggle="collapse" data-parent="#accordion" class="accordion-toggle"><?php echo $text_checkout_option; ?> <i class="fa fa-caret-down"></i></a>');

                        $('a[href=\'#collapse-checkout-option\']').trigger('click');
                        $('a[href=\'#step3\']').trigger('click');
                    },
                    error: function (xhr, ajaxOptions, thrownError) {
                        alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
                    }
                });
            });
        <?php } else { ?>
            $(document).ready(function () {
                $.ajax({
                    url: 'index.php?route=checkout/shipping_address',
                    dataType: 'html',
                    success: function (html) {
                        $('#collapse-shipping-address').html(html);
                        $('a[href=\'#step3\']').parent().removeClass('disabled');
                        // console.log(html)
                        // $('#collapse-payment-address').parent().find('.panel-heading .panel-title').html('<a href="#collapse-payment-address" data-toggle="collapse" data-parent="#accordion" class="accordion-toggle"><?php echo $text_checkout_payment_address; ?> <i class="fa fa-caret-down"></i></a>');

                        //$('a[href=\'#collapse-payment-address\']').trigger('click');
                    },
                    error: function (xhr, ajaxOptions, thrownError) {
                        alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
                    }
                });
            });
        <?php } ?>

        // Checkout
        $(document).delegate('#button-account', 'click', function () {
            $.ajax({
                url: 'index.php?route=checkout/' + $('input[name=\'account\']:checked').val(),
                dataType: 'html',
                beforeSend: function () {
                    $('#button-account').button('loading');
                },
                complete: function () {
                    $('#button-account').button('reset');
                },
                success: function (html) {
                    $('.alert, .text-danger').remove();

                    $('#collapse-payment-address .panel-body').html(html);
                    $('a[href=\'#step1\']').parent().removeClass('disabled');
                    $('a[href=\'#step3\']').parent().removeClass('disabled');
                    $('a[href=\'#step3\']').trigger('click');

                    if ($('input[name=\'account\']:checked').val() == 'register') {
                        $('#collapse-payment-address').parent().find('.panel-heading .panel-title').html('<a href="#collapse-payment-address" data-toggle="collapse" data-parent="#accordion" class="accordion-toggle"><?php echo $text_checkout_account; ?> <i class="fa fa-caret-down"></i></a>');
                    } else {
                        $('#collapse-payment-address').parent().find('.panel-heading .panel-title').html('<a href="#collapse-payment-address" data-toggle="collapse" data-parent="#accordion" class="accordion-toggle"><?php echo $text_checkout_payment_address; ?> <i class="fa fa-caret-down"></i></a>');
                    }

                    $('a[href=\'#collapse-payment-address\']').trigger('click');
                },
                error: function (xhr, ajaxOptions, thrownError) {
                    alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
                }
            });
        });

        // Login
        $(document).delegate('#button-login', 'click', function () {
            $.ajax({
                url: 'index.php?route=checkout/login/save',
                type: 'post',
                data: $('#collapse-checkout-option :input'),
                dataType: 'json',
                beforeSend: function () {
                    $('#button-login').button('loading');
                },
                complete: function () {
                    $('#button-login').button('reset');
                },
                success: function (json) {
                    $('.alert, .text-danger').remove();
                    $('.form-group').removeClass('has-error');

                    if (json['redirect']) {
                        location = json['redirect'];
                    } else if (json['error']) {
                        $('#collapse-checkout-option .panel-body').prepend('<div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> ' + json['error']['warning'] + '<button type="button" class="close" data-dismiss="alert">&times;</button></div>');

                        // Highlight any found errors
                        $('input[name=\'email\']').parent().addClass('has-error');
                        $('input[name=\'password\']').parent().addClass('has-error');
                    }
                },
                error: function (xhr, ajaxOptions, thrownError) {
                    alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
                }
            });
        });

// Register
        $(document).delegate('#button-register', 'click', function () {
            $.ajax({
                url: 'index.php?route=checkout/register/save',
                type: 'post',
                data: $('#collapse-payment-address input[type=\'text\'], #collapse-payment-address input[type=\'date\'], #collapse-payment-address input[type=\'datetime-local\'], #collapse-payment-address input[type=\'time\'], #collapse-payment-address input[type=\'password\'], #collapse-payment-address input[type=\'hidden\'], #collapse-payment-address input[type=\'checkbox\']:checked, #collapse-payment-address input[type=\'radio\']:checked, #collapse-payment-address textarea, #collapse-payment-address select'),
                dataType: 'json',
                beforeSend: function () {
                    $('#button-register').button('loading');
                },
                success: function (json) {
                    $('.alert, .text-danger').remove();
                    $('.form-group').removeClass('has-error');

                    if (json['redirect']) {
                        location = json['redirect'];
                    } else if (json['error']) {
                        $('#button-register').button('reset');

                        if (json['error']['warning']) {
                            $('#collapse-payment-address .panel-body').prepend('<div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> ' + json['error']['warning'] + '<button type="button" class="close" data-dismiss="alert">&times;</button></div>');
                        }

                        for (i in json['error']) {
                            var element = $('#input-payment-' + i.replace('_', '-'));

                            if ($(element).parent().hasClass('input-group')) {
                                $(element).parent().after('<div class="text-danger">' + json['error'][i] + '</div>');
                            } else {
                                $(element).after('<div class="text-danger">' + json['error'][i] + '</div>');
                            }
                        }

                        // Highlight any found errors
                        $('.text-danger').parent().addClass('has-error');
                    } else {
                        <?php if ($shipping_required) { ?>
                            var shipping_address = $('#payment-address input[name=\'shipping_address\']:checked').prop('value');

                            if (shipping_address) {
                                $.ajax({
                                    url: 'index.php?route=checkout/shipping_method',
                                    dataType: 'html',
                                    success: function (html) {
                                        // Add the shipping address
                                        $.ajax({
                                            url: 'index.php?route=checkout/shipping_address',
                                            dataType: 'html',
                                            success: function (html) {
                                                $('#collapse-shipping-address .panel-body').html(html);

                                                $('#collapse-shipping-address').parent().find('.panel-heading .panel-title').html('<a href="#collapse-shipping-address" data-toggle="collapse" data-parent="#accordion" class="accordion-toggle"><?php echo $text_checkout_shipping_address; ?> <i class="fa fa-caret-down"></i></a>');
                                            },
                                            error: function (xhr, ajaxOptions, thrownError) {
                                                alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
                                            }
                                        });

                                        $('#collapse-shipping-method .panel-body').html(html);

                                        $('#collapse-shipping-method').parent().find('.panel-heading .panel-title').html('<a href="#collapse-shipping-method" data-toggle="collapse" data-parent="#accordion" class="accordion-toggle"><?php echo $text_checkout_shipping_method; ?> <i class="fa fa-caret-down"></i></a>');

                                        $('a[href=\'#collapse-shipping-method\']').trigger('click');

                                        $('#collapse-shipping-method').parent().find('.panel-heading .panel-title').html('<?php echo $text_checkout_shipping_method; ?>');
                                        $('#collapse-payment-method').parent().find('.panel-heading .panel-title').html('<?php echo $text_checkout_payment_method; ?>');
                                        $('#collapse-checkout-datetimeslot').parent().find('.panel-heading .panel-title').html('<?php echo $text_checkout_datetimeslot; ?>');
                                        $('#collapse-checkout-confirm').parent().find('.panel-heading .panel-title').html('<?php echo $text_checkout_confirm; ?>');
                                    },
                                    error: function (xhr, ajaxOptions, thrownError) {
                                        alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
                                    }
                                });
                            } else {
                                $.ajax({
                                    url: 'index.php?route=checkout/shipping_address',
                                    dataType: 'html',
                                    success: function (html) {
                                        $('#collapse-shipping-address .panel-body').html(html);

                                        $('#collapse-shipping-address').parent().find('.panel-heading .panel-title').html('<a href="#collapse-shipping-address" data-toggle="collapse" data-parent="#accordion" class="accordion-toggle"><?php echo $text_checkout_shipping_address; ?> <i class="fa fa-caret-down"></i></a>');

                                        $('a[href=\'#collapse-shipping-address\']').trigger('click');

                                        $('#collapse-shipping-method').parent().find('.panel-heading .panel-title').html('<?php echo $text_checkout_shipping_method; ?>');
                                        $('#collapse-payment-method').parent().find('.panel-heading .panel-title').html('<?php echo $text_checkout_payment_method; ?>');
                                        $('#collapse-checkout-datetimeslot').parent().find('.panel-heading .panel-title').html('<?php echo $text_checkout_datetimeslot; ?>');
                                        $('#collapse-checkout-confirm').parent().find('.panel-heading .panel-title').html('<?php echo $text_checkout_confirm; ?>');
                                    },
                                    error: function (xhr, ajaxOptions, thrownError) {
                                        alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
                                    }
                                });
                            }
                        <?php } else { ?>
                            $.ajax({
                                url: 'index.php?route=checkout/payment_method',
                                dataType: 'html',
                                success: function (html) {
                                    $('#collapse-payment-method .panel-body').html(html);

                                    $('#collapse-payment-method').parent().find('.panel-heading .panel-title').html('<a href="#collapse-payment-method" data-toggle="collapse" data-parent="#accordion" class="accordion-toggle"><?php echo $text_checkout_payment_method; ?> <i class="fa fa-caret-down"></i></a>');

                                    $('a[href=\'#collapse-payment-method\']').trigger('click');
                                    $('#collapse-checkout-datetimeslot').parent().find('.panel-heading .panel-title').html('<?php echo $text_checkout_datetimeslot; ?>');
                                    $('#collapse-checkout-confirm').parent().find('.panel-heading .panel-title').html('<?php echo $text_checkout_confirm; ?>');
                                },
                                error: function (xhr, ajaxOptions, thrownError) {
                                    alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
                                }
                            });
                        <?php } ?>

                        $.ajax({
                            url: 'index.php?route=checkout/payment_address',
                            dataType: 'html',
                            complete: function () {
                                $('#button-register').button('reset');
                            },
                            success: function (html) {
                                $('#collapse-payment-address .panel-body').html(html);

                                $('#collapse-payment-address').parent().find('.panel-heading .panel-title').html('<a href="#collapse-payment-address" data-toggle="collapse" data-parent="#accordion" class="accordion-toggle"><?php echo $text_checkout_payment_address; ?> <i class="fa fa-caret-down"></i></a>');
                            },
                            error: function (xhr, ajaxOptions, thrownError) {
                                alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
                            }
                        });
                    }
                },
                error: function (xhr, ajaxOptions, thrownError) {
                    alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
                }
            });
        });

// Payment Address
        $(document).delegate('#button-payment-address', 'click', function () {
            $.ajax({
                url: 'index.php?route=checkout/payment_address/save',
                type: 'post',
                data: $('#collapse-payment-address input[type=\'text\'], #collapse-payment-address input[type=\'date\'], #collapse-payment-address input[type=\'datetime-local\'], #collapse-payment-address input[type=\'time\'], #collapse-payment-address input[type=\'password\'], #collapse-payment-address input[type=\'checkbox\']:checked, #collapse-payment-address input[type=\'radio\']:checked, #collapse-payment-address input[type=\'hidden\'], #collapse-payment-address textarea, #collapse-payment-address select'),
                dataType: 'json',
                beforeSend: function () {
                    $('#button-payment-address').button('loading');
                },
                complete: function () {
                    $('#button-payment-address').button('reset');
                },
                success: function (json) {
                    $('.alert, .text-danger').remove();

                    if (json['redirect']) {
                        location = json['redirect'];
                    } else if (json['error']) {
                        if (json['error']['warning']) {
                            $('#collapse-payment-address .panel-body').prepend('<div class="alert alert-warning">' + json['error']['warning'] + '<button type="button" class="close" data-dismiss="alert">&times;</button></div>');
                        }

                        for (i in json['error']) {
                            var element = $('#input-payment-' + i.replace('_', '-'));

                            if ($(element).parent().hasClass('input-group')) {
                                $(element).parent().after('<div class="text-danger">' + json['error'][i] + '</div>');
                            } else {
                                $(element).after('<div class="text-danger">' + json['error'][i] + '</div>');
                            }
                        }

                        // Highlight any found errors
                        $('.text-danger').parent().parent().addClass('has-error');
                    } else {
                        <?php if ($shipping_required) { ?>
                            $.ajax({
                                url: 'index.php?route=checkout/shipping_address',
                                dataType: 'html',
                                success: function (html) {
                                    $('#collapse-shipping-address .panel-body').html(html);

                                    $('#collapse-shipping-address').parent().find('.panel-heading .panel-title').html('<a href="#collapse-shipping-address" data-toggle="collapse" data-parent="#accordion" class="accordion-toggle"><?php echo $text_checkout_shipping_address; ?> <i class="fa fa-caret-down"></i></a>');

                                    $('a[href=\'#step3\']').parent().removeClass('disabled');
                                    $('a[href=\'#step3\']').trigger('click');
                                    $('a[href=\'#collapse-shipping-address\']').trigger('click');

                                    $('#collapse-shipping-method').parent().find('.panel-heading .panel-title').html('<?php echo $text_checkout_shipping_method; ?>');
                                    $('#collapse-payment-method').parent().find('.panel-heading .panel-title').html('<?php echo $text_checkout_payment_method; ?>');
                                    $('#collapse-checkout-datetimeslot').parent().find('.panel-heading .panel-title').html('<?php echo $text_checkout_datetimeslot; ?>');
                                    $('#collapse-checkout-confirm').parent().find('.panel-heading .panel-title').html('<?php echo $text_checkout_confirm; ?>');
                                },
                                error: function (xhr, ajaxOptions, thrownError) {
                                    alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
                                }
                            });
                        <?php } else { ?>
                            $.ajax({
                                url: 'index.php?route=checkout/payment_method',
                                dataType: 'html',
                                success: function (html) {
                                    $('#collapse-payment-method .panel-body').html(html);

                                    $('#collapse-payment-method').parent().find('.panel-heading .panel-title').html('<a href="#collapse-payment-method" data-toggle="collapse" data-parent="#accordion" class="accordion-toggle"><?php echo $text_checkout_payment_method; ?> <i class="fa fa-caret-down"></i></a>');

                                    $('a[href=\'#collapse-payment-method\']').trigger('click');
                                    $('#collapse-checkout-datetimeslot').parent().find('.panel-heading .panel-title').html('<?php echo $text_checkout_datetimeslot; ?>');
                                    $('#collapse-checkout-confirm').parent().find('.panel-heading .panel-title').html('<?php echo $text_checkout_confirm; ?>');
                                },
                                error: function (xhr, ajaxOptions, thrownError) {
                                    alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
                                }
                            });
                        <?php } ?>

                        $.ajax({
                            url: 'index.php?route=checkout/payment_address',
                            dataType: 'html',
                            success: function (html) {
                                $('#collapse-payment-address .panel-body').html(html);

                            },
                            error: function (xhr, ajaxOptions, thrownError) {
                                alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
                            }
                        });
                    }
                },
                error: function (xhr, ajaxOptions, thrownError) {
                    alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
                }
            });
        });

        // Shipping Address
        $(document).delegate('#button-shipping-address', 'click', function (e) {
            
            $.ajax({
                url: 'index.php?route=checkout/shipping_address/save',
                type: 'post',
                data: $('#collapse-shipping-address input[type=\'text\'], #collapse-shipping-address input[type=\'hidden\'], #collapse-shipping-address input[type=\'date\'], #collapse-shipping-address input[type=\'datetime-local\'], #collapse-shipping-address input[type=\'time\'], #collapse-shipping-address input[type=\'password\'], #collapse-shipping-address input[type=\'checkbox\']:checked, #collapse-shipping-address input[type=\'radio\']:checked, #collapse-shipping-address textarea, #collapse-shipping-address select'),
                dataType: 'json',
                beforeSend: function () {
                    $('#button-shipping-address').button('loading');
                },
                success: function (json) {
                    $('.alert, .text-danger').remove();
                    
                    if (json['redirect']) {
                        location = json['redirect'];
                    } else if (json['error']) {
                        $('#button-shipping-address').button('reset');
                        
                        if (json['error']['warning']) {
                            // $('#collapse-shipping-address .panel-body').prepend('<div class="alert alert-warning">' + json['error']['warning'] + '<button type="button" class="close" data-dismiss="alert">&times;</button></div>');
                            $('#collapse-shipping-address ').prepend('<div class="alert alert-warning">' + json['error']['warning'] + '<button type="button" class="close" data-dismiss="alert">&times;</button></div>');
                        }
                        if(json['error']['citc_warning']) {
                            $('#collapse-shipping-address ').prepend('<div class="alert alert-warning">' + json['error']['citc_warning'] + '<button type="button" class="close" data-dismiss="alert">&times;</button></div>');
                        }
                        // console.log(json['error']);
                        let address = $('#input-shipping-address-1');
                        if(json['error']['latitude_longitude'] && !json['error']['address_1']){
                            if ($(address).parent().hasClass('input-group')) {
                                $(address).parent().after('<div class="text-danger">' + json['error']['latitude_longitude'] + '</div>');
                            } else {
                                $(address).after('<div class="text-danger">' + json['error']['latitude_longitude'] + '</div>');
                            }
                        }
                        for (i in json['error']) {
                            console.log(i)
                            var element = $('#input-shipping-' + i.replace('_', '-'));

                            if ($(element).parent().hasClass('input-group')) {
                                $(element).parent().after('<div class="text-danger">' + json['error'][i] + '</div>');
                            } else {
                                $(element).after('<div class="text-danger">' + json['error'][i] + '</div>');
                            }
                        }

                        // Highlight any found errors
                        $('.text-danger').parent().parent().addClass('has-error');
                    } else {
                        $.ajax({
                            url: 'index.php?route=checkout/payment_method',
                            dataType: 'html',
                            complete: function () {
                                $('#button-shipping-address').button('reset');
                            },
                            success: function (html) {
                                $('#collapse-payment-method .panel-body').html(html);

                                $('#collapse-payment-method').parent().find('.panel-heading .panel-title').html('<a href="#collapse-shipping-method" data-toggle="collapse" data-parent="#accordion" class="accordion-toggle"><?php echo $text_checkout_shipping_method; ?> <i class="fa fa-caret-down"></i></a>');

                                $('a[href=\'#step5\']').parent().removeClass('disabled');
                                $('a[href=\'#step5\']').trigger('click');
                                $('#collapse-checkout-datetimeslot').parent().find('.panel-heading .panel-title').html('<?php echo $text_checkout_datetimeslot; ?>');
                                $('#collapse-checkout-confirm').parent().find('.panel-heading .panel-title').html('<?php echo $text_checkout_confirm; ?>');

                                $.ajax({
                                    url: 'index.php?route=checkout/shipping_address',
                                    dataType: 'html',
                                    success: function (html) {
                                        $('#collapse-shipping-address .panel-body').html(html);
                                    },
                                    error: function (xhr, ajaxOptions, thrownError) {
                                        alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
                                    }
                                });
                            },
                            error: function (xhr, ajaxOptions, thrownError) {
                                alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
                            }
                        });

                        $.ajax({
                            url: 'index.php?route=checkout/payment_address',
                            dataType: 'html',
                            success: function (html) {
                                $('#collapse-payment-address .panel-body').html(html);
                            },
                            error: function (xhr, ajaxOptions, thrownError) {
                                alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
                            }
                        });
                    }
                },
                error: function (xhr, ajaxOptions, thrownError) {
                    alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
                }
            });
        });

// Guest
        $(document).delegate('#button-guest', 'click', function () {
            $.ajax({
                url: 'index.php?route=checkout/guest/save',
                type: 'post',
                data: $('#collapse-payment-address input[type=\'text\'], #collapse-payment-address input[type=\'date\'], #collapse-payment-address input[type=\'datetime-local\'], #collapse-payment-address input[type=\'time\'], #collapse-payment-address input[type=\'checkbox\']:checked, #collapse-payment-address input[type=\'radio\']:checked, #collapse-payment-address input[type=\'hidden\'], #collapse-payment-address textarea, #collapse-payment-address select'),
                dataType: 'json',
                beforeSend: function () {
                    $('#button-guest').button('loading');
                },
                success: function (json) {
                    $('.alert, .text-danger').remove();

                    if (json['redirect']) {
                        location = json['redirect'];
                    } else if (json['error']) {
                        $('#button-guest').button('reset');

                        if (json['error']['warning']) {
                            $('#collapse-payment-address .panel-body').prepend('<div class="alert alert-warning">' + json['error']['warning'] + '<button type="button" class="close" data-dismiss="alert">&times;</button></div>');
                        }

                        for (i in json['error']) {
                            var element = $('#input-payment-' + i.replace('_', '-'));

                            if ($(element).parent().hasClass('input-group')) {
                                $(element).parent().after('<div class="text-danger">' + json['error'][i] + '</div>');
                            } else {
                                $(element).after('<div class="text-danger">' + json['error'][i] + '</div>');
                            }
                        }

                        // Highlight any found errors
                        $('.text-danger').parent().addClass('has-error');
                    } else {
                        <?php if ($shipping_required) { ?>
                            var shipping_address = $('#collapse-payment-address input[name=\'shipping_address\']:checked').prop('value');

                            if (shipping_address) {
                                $.ajax({
                                    url: 'index.php?route=checkout/shipping_method',
                                    dataType: 'html',
                                    complete: function () {
                                        $('#button-guest').button('reset');
                                    },
                                    success: function (html) {
                                        // Add the shipping address
                                        $.ajax({
                                            url: 'index.php?route=checkout/guest_shipping',
                                            dataType: 'html',
                                            success: function (html) {
                                                $('#collapse-shipping-address .panel-body').html(html);

                                                $('#collapse-shipping-address').parent().find('.panel-heading .panel-title').html('<a href="#collapse-shipping-address" data-toggle="collapse" data-parent="#accordion" class="accordion-toggle"><?php echo $text_checkout_shipping_address; ?> <i class="fa fa-caret-down"></i></a>');
                                            },
                                            error: function (xhr, ajaxOptions, thrownError) {
                                                alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
                                            }
                                        });

                                        $('#collapse-shipping-method .panel-body').html(html);

                                        $('#collapse-shipping-method').parent().find('.panel-heading .panel-title').html('<a href="#collapse-shipping-method" data-toggle="collapse" data-parent="#accordion" class="accordion-toggle"><?php echo $text_checkout_shipping_method; ?> <i class="fa fa-caret-down"></i></a>');

                                        $('a[href=\'#collapse-shipping-method\']').trigger('click');

                                        $('#collapse-payment-method').parent().find('.panel-heading .panel-title').html('<?php echo $text_checkout_payment_method; ?>');
                                        $('#collapse-checkout-datetimeslot').parent().find('.panel-heading .panel-title').html('<?php echo $text_checkout_datetimeslot; ?>');
                                        $('#collapse-checkout-confirm').parent().find('.panel-heading .panel-title').html('<?php echo $text_checkout_confirm; ?>');
                                    },
                                    error: function (xhr, ajaxOptions, thrownError) {
                                        alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
                                    }
                                });
                            } else {
                                $.ajax({
                                    url: 'index.php?route=checkout/guest_shipping',
                                    dataType: 'html',
                                    complete: function () {
                                        $('#button-guest').button('reset');
                                    },
                                    success: function (html) {
                                        $('#collapse-shipping-address .panel-body').html(html);

                                        $('#collapse-shipping-address').parent().find('.panel-heading .panel-title').html('<a href="#collapse-shipping-address" data-toggle="collapse" data-parent="#accordion" class="accordion-toggle"><?php echo $text_checkout_shipping_address; ?> <i class="fa fa-caret-down"></i></a>');

                                        $('a[href=\'#collapse-shipping-address\']').trigger('click');

                                        $('#collapse-shipping-method').parent().find('.panel-heading .panel-title').html('<?php echo $text_checkout_shipping_method; ?>');
                                        $('#collapse-payment-method').parent().find('.panel-heading .panel-title').html('<?php echo $text_checkout_payment_method; ?>');
                                        $('#collapse-checkout-datetimeslot').parent().find('.panel-heading .panel-title').html('<?php echo $text_checkout_datetimeslot; ?>');
                                        $('#collapse-checkout-confirm').parent().find('.panel-heading .panel-title').html('<?php echo $text_checkout_confirm; ?>');
                                    },
                                    error: function (xhr, ajaxOptions, thrownError) {
                                        alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
                                    }
                                });
                            }
                        <?php } else { ?>
                            $.ajax({
                                url: 'index.php?route=checkout/payment_method',
                                dataType: 'html',
                                complete: function () {
                                    $('#button-guest').button('reset');
                                },
                                success: function (html) {
                                    $('#collapse-payment-method .panel-body').html(html);

                                    $('#collapse-payment-method').parent().find('.panel-heading .panel-title').html('<a href="#collapse-payment-method" data-toggle="collapse" data-parent="#accordion" class="accordion-toggle"><?php echo $text_checkout_payment_method; ?> <i class="fa fa-caret-down"></i></a>');

                                    $('a[href=\'#collapse-payment-method\']').trigger('click');
                                    $('#collapse-checkout-datetimeslot').parent().find('.panel-heading .panel-title').html('<?php echo $text_checkout_datetimeslot; ?>');
                                    $('#collapse-checkout-confirm').parent().find('.panel-heading .panel-title').html('<?php echo $text_checkout_confirm; ?>');
                                },
                                error: function (xhr, ajaxOptions, thrownError) {
                                    alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
                                }
                            });
                        <?php } ?>
                    }
                },
                error: function (xhr, ajaxOptions, thrownError) {
                    alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
                }
            });
        });

        // Guest Shipping
        $(document).delegate('#button-guest-shipping', 'click', function () {
            $.ajax({
                url: 'index.php?route=checkout/guest_shipping/save',
                type: 'post',
                data: $('#collapse-shipping-address input[type=\'text\'], #collapse-shipping-address input[type=\'date\'], #collapse-shipping-address input[type=\'datetime-local\'], #collapse-shipping-address input[type=\'time\'], #collapse-shipping-address input[type=\'password\'], #collapse-shipping-address input[type=\'checkbox\']:checked, #collapse-shipping-address input[type=\'radio\']:checked, #collapse-shipping-address textarea, #collapse-shipping-address select'),
                dataType: 'json',
                beforeSend: function () {
                    $('#button-guest-shipping').button('loading');
                },
                success: function (json) {
                    $('.alert, .text-danger').remove();

                    if (json['redirect']) {
                        location = json['redirect'];
                    } else if (json['error']) {
                        $('#button-guest-shipping').button('reset');

                        if (json['error']['warning']) {
                            $('#collapse-shipping-address .panel-body').prepend('<div class="alert alert-warning">' + json['error']['warning'] + '<button type="button" class="close" data-dismiss="alert">&times;</button></div>');
                        }

                        for (i in json['error']) {
                            var element = $('#input-shipping-' + i.replace('_', '-'));

                            if ($(element).parent().hasClass('input-group')) {
                                $(element).parent().after('<div class="text-danger">' + json['error'][i] + '</div>');
                            } else {
                                $(element).after('<div class="text-danger">' + json['error'][i] + '</div>');
                            }
                        }

                        // Highlight any found errors
                        $('.text-danger').parent().addClass('has-error');
                    } else {
                        $.ajax({
                            url: 'index.php?route=checkout/shipping_method',
                            dataType: 'html',
                            complete: function () {
                                $('#button-guest-shipping').button('reset');
                            },
                            success: function (html) {
                                $('#collapse-shipping-method .panel-body').html(html);

                                $('#collapse-shipping-method').parent().find('.panel-heading .panel-title').html('<a href="#collapse-shipping-method" data-toggle="collapse" data-parent="#accordion" class="accordion-toggle"><?php echo $text_checkout_shipping_method; ?> <i class="fa fa-caret-down"></i>');

                                $('a[href=\'#collapse-shipping-method\']').trigger('click');

                                $('#collapse-payment-method').parent().find('.panel-heading .panel-title').html('<?php echo $text_checkout_payment_method; ?>');
                                $('#collapse-checkout-datetimeslot').parent().find('.panel-heading .panel-title').html('<?php echo $text_checkout_datetimeslot; ?>');
                                $('#collapse-checkout-confirm').parent().find('.panel-heading .panel-title').html('<?php echo $text_checkout_confirm; ?>');
                            },
                            error: function (xhr, ajaxOptions, thrownError) {
                                alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
                            }
                        });
                    }
                },
                error: function (xhr, ajaxOptions, thrownError) {
                    alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
                }
            });
        });

        $(document).delegate('#button-shipping-method', 'click', function () {
            $.ajax({
                url: 'index.php?route=checkout/shipping_method/save',
                type: 'post',
                data: $('#collapse-shipping-method input[type=\'radio\']:checked, #collapse-shipping-method textarea'),
                dataType: 'json',
                beforeSend: function () {
                    $('#button-shipping-method').button('loading');
                },
                success: function (json) {
                    $('.alert, .text-danger').remove();

                    if (json['redirect']) {
                        location = json['redirect'];
                    } else if (json['error']) {
                        $('#button-shipping-method').button('reset');

                        if (json['error']['warning']) {
                            $('#collapse-shipping-method .panel-body').prepend('<div class="alert alert-warning">' + json['error']['warning'] + '<button type="button" class="close" data-dismiss="alert">&times;</button></div>');
                        }
                    } else {
                        $.ajax({
                            url: 'index.php?route=checkout/payment_method',
                            dataType: 'html',
                            complete: function () {
                                $('#button-shipping-method').button('reset');
                            },
                            success: function (html) {
                                $('#collapse-payment-method .panel-body').html(html);

                                $('#collapse-payment-method').parent().find('.panel-heading .panel-title').html('<a href="#collapse-payment-method" data-toggle="collapse" data-parent="#accordion" class="accordion-toggle"><?php echo $text_checkout_payment_method; ?> <i class="fa fa-caret-down"></i></a>');

                                $('a[href=\'#step5\']').parent().removeClass('disabled');
                                $('a[href=\'#step5\']').trigger('click');
                                $('#collapse-checkout-datetimeslot').parent().find('.panel-heading .panel-title').html('<?php echo $text_checkout_datetimeslot; ?>');
                                $('#collapse-checkout-confirm').parent().find('.panel-heading .panel-title').html('<?php echo $text_checkout_confirm; ?>');
                            },
                            error: function (xhr, ajaxOptions, thrownError) {
                                alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
                            }
                        });
                    }
                },
                error: function (xhr, ajaxOptions, thrownError) {
                    alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
                }
            });
        });

        $(document).delegate('#button-payment-method', 'click', function () {
            $.ajax({
                url: 'index.php?route=checkout/payment_method/save',
                type: 'post',
                data: $('#collapse-payment-method input[type=\'radio\']:checked, #collapse-payment-method input[type=\'checkbox\']:checked, #collapse-payment-method textarea'),
                dataType: 'json',
                beforeSend: function () {
                    $('#button-payment-method').button('loading');
                },
                success: function (json) {
                    $('.alert, .text-danger').remove();
                    $('#button-payment-method').button('reset');
                    if (json['redirect']) {
                        location = json['redirect'];
                    } else if (json['error']) {
                        if (json['error']['warning']) {
                            $('#collapse-payment-method .panel-body').prepend('<div class="alert alert-warning">' + json['error']['warning'] + '<button type="button" class="close" data-dismiss="alert">&times;</button></div>');
                        }
                    } else {
                        $.ajax({
                            url: 'index.php?route=checkout/datetimeslot',
                            dataType: 'html',
                            complete: function () {
                                $('#button-payment-method').button('reset');
                            },
                            success: function (html) {
                                $('#collapse-checkout-datetimeslot .panel-body').html(html);

                                $('#collapse-checkout-datetimeslot').parent().find('.panel-heading .panel-title').html('<a href="#collapse-checkout-datetimeslot" data-toggle="collapse" data-parent="#accordion" class="accordion-toggle"><?php echo $text_checkout_datetimeslot; ?> <i class="fa fa-caret-down"></i></a>');

                                $('a[href=\'#step6\']').parent().removeClass('disabled');
                                $('a[href=\'#step6\']').trigger('click');
                            },
                            error: function (xhr, ajaxOptions, thrownError) {
                                alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
                            }
                        });
                    }
                },
                error: function (xhr, ajaxOptions, thrownError) {
                    alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
                }
            });
        });

        $(document).delegate('#button-datetime', 'click', function () {
            $.ajax({
                url: 'index.php?route=checkout/datetimeslot/save',
                type: 'post',
                data: $('#sellerdatetime'),
                dataType: 'json',
                beforeSend: function () {
                    $('#button-datetime').button('loading');
                },
                success: function (json) {
                    $('.alert, .text-danger').remove();
                    $('#button-datetime').button('reset');
                    if (json['error']) {
                        if (json['error']['warning']) {
                            $('#collapse-checkout-datetimeslot .panel-body').prepend('<div class="alert alert-warning">' + json['error']['warning'] + '<button type="button" class="close" data-dismiss="alert">&times;</button></div>');
                        }
                    } else {
                        $.ajax({
                            url: 'index.php?route=checkout/confirm',
                            dataType: 'html',
                            complete: function () {
                                $('#button-datetime').button('reset');
                            },
                            success: function (html) {
                                $('#collapse-checkout-confirm .panel-body').html(html);

                                $('#collapse-checkout-confirm').parent().find('.panel-heading .panel-title').html('<a href="#collapse-checkout-confirm" data-toggle="collapse" data-parent="#accordion" class="accordion-toggle"><?php echo $text_checkout_confirm; ?> <i class="fa fa-caret-down"></i></a>');

                                $('a[href=\'#step7\']').parent().removeClass('disabled');
                                $('a[href=\'#step7\']').trigger('click');
                            },
                            error: function (xhr, ajaxOptions, thrownError) {
                                alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
                            }
                        });
                    }
                },
                error: function (xhr, ajaxOptions, thrownError) {
                    alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
                }
            });
        });

        $(document).delegate('#date_slot', 'change', function () {
            $.ajax({
                url: 'index.php?route=checkout/datetimeslot/timeslot_by_date',
                type: 'post',
                data: $('#date_slot'),
                dataType: 'json',
                beforeSend: function () {
                    $('#button-datetime').val('Loading...');
                    $('#button-datetime').attr('disabled','disabled');
                    $('#button-datetime').addClass('disabled');

                },
                success: function (json) {
                    $('#time_slot').empty();
                    $('#button-datetime').val(json['button_continue']);
                    $.each(json['time_slots']['slots'], function (i, item) {
                        if(item != json['text_store_closed']){
                            $('#button-datetime').removeAttr('disabled');
                            $('#button-datetime').removeClass('disabled');
                        }
                        $('#time_slot').append($('<option>', { 
                            value: item,
                            text : item 
                        }));
                    });
                },
                error: function (xhr, ajaxOptions, thrownError) {
                    alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
                }
            });
        });
        //--></script>
    <?php echo $footer; ?>