<style>
	.custom-cart-body {
		color: white;
		background: #69489d;
		padding: 5px;
		height: 50px;
	}
	span.custom-cart-left {
		float: left;
	}
	span.custom-cart-right {
		float: right;
	}
	span.custom-cart-right.total {
		padding-right: 10px;
	}
	
</style>
<?php if (!isset($redirect)) { ?>

<div class="table-responsive">
  <table class="table table-bordered table-hover">
  <tbody>
  
	<?php 
	
		foreach($sellers as $key=>$seller){
			$seller_id= $key; ?>
	 <div class="custom-cart-header">
      <tr style="background-color:#69489d !important"  class="custom-cart-body">
		  <td colspan="5">

			
				<span class="custom-cart-left"><?php echo $seller['text_store']; ?></span> <span class="custom-cart-right">Items: <?php echo $seller['itemtotal']; ?></span> <span class="custom-cart-right total">	Total:  <?php echo $seller['producttotals']; ?></span>
				<br/>
				<span class="custom-cart-left">Min Limit: <?php echo $seller['text_minimum_order']; ?></span><span class="custom-cart-right">Free  <?php 
				if(($seller['product_total'] < $sellerinfo['free_delivery_limit']) or ($seller['free_delivery_limit'] == 0)){
							echo $seller['delivery_charges'];
						}else{
							echo 0.00;
						}
				?></span>
			 
			
		</td>
      </tr>
      <tr style="background-color:#69489d !important; color: white;" lass="custom-cart-body">
			
        <td class="text-left"><?php echo $column_name; ?></td>
        <td class="text-left"><?php echo $column_model; ?></td>
        <td class="text-right"><?php echo $column_quantity; ?></td>
        <td class="text-right"><?php echo $column_price; ?></td>
        <td class="text-right"><?php echo $column_total; ?></td>
	
		
      </tr>
	  
 </div>
    
      <?php foreach ($products as $product) { 
	
		if($product['seller_id'] == $seller_id){
	  ?>
      <tr>
        <td class="text-left"><a href="<?php echo $product['href']; ?>"><?php echo $product['name']; ?></a>
          <?php foreach ($product['option'] as $option) { ?>
          <br />
          &nbsp;<small> - <?php echo $option['name']; ?>: <?php echo $option['value']; ?></small>
          <?php } ?>
          <?php if($product['recurring']) { ?>
          <br />
          <span class="label label-info"><?php echo $text_recurring_item; ?></span> <small><?php echo $product['recurring']; ?></small>
          <?php } ?></td>
        <td class="text-left"><?php echo $product['model']; ?></td>
        <td class="text-right"><?php echo $product['quantity']; ?></td>
        <td class="text-right"><?php echo $product['price']; ?></td>
        <td class="text-right"><?php echo $product['total']; ?></td>
      </tr>
	  
	<?php } } } ?>
	  
	  
      <?php foreach ($vouchers as $voucher) { ?>
      <tr>
        <td class="text-left"><?php echo $voucher['description']; ?></td>
        <td class="text-left"></td>
        <td class="text-right">1</td>
        <td class="text-right"><?php echo $voucher['amount']; ?></td>
        <td class="text-right"><?php echo $voucher['amount']; ?></td>
      </tr>
      <?php } ?>
    </tbody>
    <tfoot>
      <?php foreach ($totals as $total) { ?>
      <tr>
        <td colspan="4" class="text-right" style="text-align: right;"><strong><?php echo $total['title']; ?>:</strong></td>
        <td class="text-right"><?php echo $total['text']; ?></td>
      </tr>
      <?php } ?>
    </tfoot>
  </table>
  
</div>
<?php echo $payment; ?>
<?php } else { ?>
<script type="text/javascript"><!--
location = '<?php echo $redirect; ?>';
//--></script>
<?php } ?>
