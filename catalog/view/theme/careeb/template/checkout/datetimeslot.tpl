<div style="display:none;">
<label><?php echo $text_date_select;?></label>
<select name="date_slot" id="date_slot">
	<?php foreach ($day_slot as $key => $value): ?>
		<option value="<?php echo $key ?>" <?php if($key == 'now') echo 'selected' ?>><?php echo $value ?></option>
	<?php endforeach ?>
</select>
</br>
<label><?php echo $text_time_select;?></label>
<select name="time_slot" id="time_slot">
	<?php foreach($time_slots['slots'] as $timeslot): ?>
		<option value="<?php echo $timeslot; ?>"><?php echo $timeslot; ?></option>
	<?php endforeach; ?>
</select>
<div class="clearfix"></div>
</div>
<div class="clearfix">
    <div class="row">
        <div class='col-sm-6'>
		<label><?php echo $text_date_select;?></label>
            <div class="form-group">
                <div class='input-group date' id='datetimepicker2'>
                    <input type='text' id="sellerdatetime" name="deliverytime" class="form-control" />
                    <span class="input-group-addon">
                        <span class="glyphicon glyphicon-calendar"></span>
                    </span>
                </div>
            </div>
        </div>
        <script type="text/javascript">
            $(function () {
                $('#datetimepicker2').datetimepicker({
                      minDate:new Date(),
					  defaultDate: new Date(),
                });
            });
        </script>
    </div>
</div>
<div class="buttons clearfix">
    <div class="pull-right">
      <input type="button" value="<?php echo $button_continue; ?>" id="button-datetime" data-loading-text="<?php echo $text_loading; ?>" class="btn btn-primary <?php if($time_slots['slots'][0] == $data['text_store_closed']) //echo 'disabled'?>" <?php if($time_slots["slots"][0] == $data['text_store_closed']) //echo 'disabled'?>/>
    </div>
</div>
