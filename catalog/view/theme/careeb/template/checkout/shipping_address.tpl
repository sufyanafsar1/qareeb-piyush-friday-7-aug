<style>
  #map {
    height: 400px;
    width: 100%;
  }
  .gm-fullscreen-control{ display:none;}
  .controls {
    margin-top: 16px;
    border: 1px solid transparent;
    border-radius: 2px 0 0 2px;
    box-sizing: border-box;
    -moz-box-sizing: border-box;
    height: 32px;
    outline: none;
    box-shadow: 0 2px 6px rgba(0, 0, 0, 0.3);
  }

  #pac-input {
    background-color: #fff;
    font-family: Roboto;
    font-size: 15px;
    font-weight: 300;
    margin-left: 12px;
    padding: 0 11px 0 13px;
    text-overflow: ellipsis;
    width: 400px;
  }

  #pac-input:focus {
    border-color: #4d90fe;
  }

  .pac-container {
    font-family: Roboto;
  }

  #type-selector {
    color: #fff;
    background-color: #4d90fe;
    padding: 5px 11px 0px 11px;
  }

  #type-selector label {
    font-family: Roboto;
    font-size: 13px;
    font-weight: 300;
  }

</style>
<form class="form-horizontal">
  <?php if ($addresses) { ?>
  <div class="radio">
    <label>
      <input type="radio" name="shipping_address" value="existing" checked="checked" />
      <?php echo $text_address_existing; ?></label>
  </div>
  <div id="shipping-existing">
    <select name="address_id" class="form-control">
      <?php foreach ($addresses as $address) { ?>
      <?php if ($address['address_id'] == $address_id) { ?>
      <option value="<?php echo $address['address_id']; ?>" selected="selected"><?php echo $address['firstname']; ?> <?php echo $address['lastname']; ?>, <?php echo $address['address_1']; ?>, <?php echo $address['city']; ?>, <?php echo $address['zone']; ?>, <?php echo $address['country']; ?></option>
      <?php } else { ?>
      <option value="<?php echo $address['address_id']; ?>"><?php echo $address['firstname']; ?> <?php echo $address['lastname']; ?>, <?php echo $address['address_1']; ?>, <?php echo $address['city']; ?>, <?php echo $address['zone']; ?>, <?php echo $address['country']; ?></option>
      <?php } ?>
      <?php } ?>
    </select>
  </div>
  <div class="radio">
    <label>
      <input type="radio" name="shipping_address" value="new" />
      <?php echo $text_address_new; ?></label>
  </div>
  <?php } ?>
  <br />
  <div id="shipping-new" style="display: <?php echo ($addresses ? 'none' : 'block'); ?>;">
    <div class="form-group required">
      <div class="col-sm-12">
        <input id="pac-input" class="controls" type="text" placeholder="Search Box"/>
        <div id="map"></div>
      </div>
    </div>

    <div class="form-group required">
      <label class="col-sm-2 control-label" for="input-shipping-firstname"><?php echo $entry_firstname; ?></label>
      <div class="col-sm-10">
        <input type="text" name="firstname" value="" placeholder="<?php echo $entry_firstname; ?>" id="input-shipping-firstname" class="form-control" />
      </div>
    </div>
    <div class="form-group required">
      <label class="col-sm-2 control-label" for="input-shipping-lastname"><?php echo $entry_lastname; ?></label>
      <div class="col-sm-10">
        <input type="text" name="lastname" value="" placeholder="<?php echo $entry_lastname; ?>" id="input-shipping-lastname" class="form-control" />
      </div>
    </div>
    <!--<div class="form-group">
      <label class="col-sm-2 control-label" for="input-shipping-company"><?php echo $entry_company; ?></label>
      <div class="col-sm-10">
        <input type="text" name="company" value="" placeholder="<?php echo $entry_company; ?>" id="input-shipping-company" class="form-control" />
      </div>
    </div>-->
    <div class="form-group required">
      <label class="col-sm-2 control-label" for="input-shipping-address-1"><?php echo $entry_address_1; ?></label>
      <div class="col-sm-10">
        <input type="text" name="address_1" value="" placeholder="<?php echo $entry_address_1; ?>" id="input-shipping-address-1" class="form-control" />
      </div>
    </div>
   <!-- <div class="form-group">
      <label class="col-sm-2 control-label" for="input-shipping-address-2"><?php echo $entry_address_2; ?></label>
      <div class="col-sm-10">
        <input type="text" name="address_2" value="" placeholder="<?php echo $entry_address_2; ?>" id="input-shipping-address-2" class="form-control" />
      </div>
    </div>-->
    <!-- <div class="form-group required"> -->
      <!-- <label class="col-sm-2 control-label" for="input-shipping-city"><?php echo $entry_city; ?></label> -->
      <!-- <div class="col-sm-10"> -->
        <!-- <input type="text" name="city" value="" placeholder="<?php echo $entry_city; ?>" id="input-shipping-city" class="form-control" /> -->
      <!-- </div> -->
    <!-- </div> -->
    <!--<div class="form-group required">
      <label class="col-sm-2 control-label" for="input-shipping-postcode"><?php echo $entry_postcode; ?></label>
      <div class="col-sm-10">
        <input type="text" name="postcode" value="<?php echo $postcode; ?>" placeholder="<?php echo $entry_postcode; ?>" id="input-shipping-postcode" class="form-control" />
      </div>
    </div>-->
    <!-- <div class="form-group required"> -->
      <!-- <label class="col-sm-2 control-label" for="input-shipping-country"><?php echo $entry_country; ?></label> -->
      <!-- <div class="col-sm-10"> -->
        <!-- <select name="country_id" id="input-shipping-country" class="form-control"> -->
          <!-- <option value=""><?php echo $text_select; ?></option> -->
          <!-- <?php foreach ($countries as $country) { ?> -->
          <!-- <?php if ($country['country_id'] == $country_id) { ?> -->
          <!-- <option value="<?php echo $country['country_id']; ?>" selected="selected"><?php echo $country['name']; ?></option> -->
          <!-- <?php } else { ?> -->
          <!-- <option value="<?php echo $country['country_id']; ?>"><?php echo $country['name']; ?></option> -->
          <!-- <?php } ?> -->
          <!-- <?php } ?> -->
        <!-- </select> -->
      <!-- </div> -->
    <!-- </div> -->
    <!-- <div class="form-group required"> -->
      <!-- <label class="col-sm-2 control-label" for="input-shipping-zone"><?php echo $entry_zone; ?></label> -->
      <!-- <div class="col-sm-10"> -->
        <!-- <select name="zone_id" id="input-shipping-zone" class="form-control"> -->
        <!-- </select> -->
      <!-- </div> -->
    <!-- </div> -->
	
	  <!-- Country Id -->
    <input type="hidden" name="country_id" value="184" />
    <!-- Citc Region -->
    <div class="form-group required">
      <label class="col-sm-2 control-label" for="input-shipping-country"><?php echo $entry_region ?></label>
      <div class="col-sm-10">
        <select name="citc_region_id" id="input-shipping-citc-region" class="form-control">
          <option value=""><?php echo $text_select; ?></option>
          <?php foreach($citc_region  as $region) { 
            if($citc_region_id == $region['region_id']) { ?>
              <option selected value="<?php echo $region['region_id'] ?>" class="select_option"><?= $region['regionName'] ?></option>
            <?php } else { ?>
                <option value="<?php echo $region['region_id'] ?>" class="select_option"><?= $region['regionName'] ?></option>
            <?php } 
          } ?>
        </select>
      </div>
    </div>

    <!-- Citc City -->
    <div class="form-group required">
      <label class="col-sm-2 control-label" for="input-shipping-country"><?php echo $entry_city ?></label>
      <div class="col-sm-10">
        <select name="citc_city_id" id="input-shipping-citc-city" class="form-control">
          <option value="" ><?php echo $text_select; ?></option>
        </select>
      </div>
    </div>

    <!-- lattitude hidden field -->
    <input type="hidden" name="latitude" id="shipping_lattitude"  />
    <!-- longitude hidden field -->
    <input type="hidden" name="longitude" id="shipping_longitude" />

    <?php foreach ($custom_fields as $custom_field) { ?>
    <?php if ($custom_field['location'] == 'address') { ?>
    <?php if ($custom_field['type'] == 'select') { ?>
    <div class="form-group<?php echo ($custom_field['required'] ? ' required' : ''); ?> custom-field" data-sort="<?php echo $custom_field['sort_order']; ?>">
      <label class="col-sm-2 control-label" for="input-shipping-custom-field<?php echo $custom_field['custom_field_id']; ?>"><?php echo $custom_field['name']; ?></label>
      <div class="col-sm-10">
        <select name="custom_field[<?php echo $custom_field['custom_field_id']; ?>]" id="input-shipping-custom-field<?php echo $custom_field['custom_field_id']; ?>" class="form-control">
          <option value=""><?php echo $text_select; ?></option>
          <?php foreach ($custom_field['custom_field_value'] as $custom_field_value) { ?>
          <option value="<?php echo $custom_field_value['custom_field_value_id']; ?>"><?php echo $custom_field_value['name']; ?></option>
          <?php } ?>
        </select>
      </div>
    </div>
    <?php } ?>
    <?php if ($custom_field['type'] == 'radio') { ?>
    <div class="form-group<?php echo ($custom_field['required'] ? ' required' : ''); ?> custom-field" data-sort="<?php echo $custom_field['sort_order']; ?>">
      <label class="col-sm-2 control-label"><?php echo $custom_field['name']; ?></label>
      <div class="col-sm-10">
        <div id="input-shipping-custom-field<?php echo $custom_field['custom_field_id']; ?>">
          <?php foreach ($custom_field['custom_field_value'] as $custom_field_value) { ?>
          <div class="radio">
            <label>
              <input type="radio" name="custom_field[<?php echo $custom_field['custom_field_id']; ?>]" value="<?php echo $custom_field_value['custom_field_value_id']; ?>" />
              <?php echo $custom_field_value['name']; ?></label>
          </div>
          <?php } ?>
        </div>
      </div>
    </div>
    <?php } ?>
    <?php if ($custom_field['type'] == 'checkbox') { ?>
    <div class="form-group<?php echo ($custom_field['required'] ? ' required' : ''); ?> custom-field" data-sort="<?php echo $custom_field['sort_order']; ?>">
      <label class="col-sm-2 control-label"><?php echo $custom_field['name']; ?></label>
      <div class="col-sm-10">
        <div id="input-shipping-custom-field<?php echo $custom_field['custom_field_id']; ?>">
          <?php foreach ($custom_field['custom_field_value'] as $custom_field_value) { ?>
          <div class="checkbox">
            <label>
              <input type="checkbox" name="custom_field[<?php echo $custom_field['custom_field_id']; ?>][]" value="<?php echo $custom_field_value['custom_field_value_id']; ?>" />
              <?php echo $custom_field_value['name']; ?></label>
          </div>
          <?php } ?>
        </div>
      </div>
    </div>
    <?php } ?>
    <?php if ($custom_field['type'] == 'text') { ?>
    <div class="form-group<?php echo ($custom_field['required'] ? ' required' : ''); ?> custom-field" data-sort="<?php echo $custom_field['sort_order']; ?>">
      <label class="col-sm-2 control-label" for="input-shipping-custom-field<?php echo $custom_field['custom_field_id']; ?>"><?php echo $custom_field['name']; ?></label>
      <div class="col-sm-10">
        <input type="text" name="custom_field[<?php echo $custom_field['custom_field_id']; ?>]" value="<?php echo $custom_field['value']; ?>" placeholder="<?php echo $custom_field['name']; ?>" id="input-shipping-custom-field<?php echo $custom_field['custom_field_id']; ?>" class="form-control" />
      </div>
    </div>
    <?php } ?>
    <?php if ($custom_field['type'] == 'textarea') { ?>
    <div class="form-group<?php echo ($custom_field['required'] ? ' required' : ''); ?> custom-field" data-sort="<?php echo $custom_field['sort_order']; ?>">
      <label class="col-sm-2 control-label" for="input-shipping-custom-field<?php echo $custom_field['custom_field_id']; ?>"><?php echo $custom_field['name']; ?></label>
      <div class="col-sm-10">
        <textarea name="custom_field[<?php echo $custom_field['custom_field_id']; ?>]" rows="5" placeholder="<?php echo $custom_field['name']; ?>" id="input-shipping-custom-field<?php echo $custom_field['custom_field_id']; ?>" class="form-control"><?php echo $custom_field['value']; ?></textarea>
      </div>
    </div>
    <?php } ?>
    <?php if ($custom_field['type'] == 'file') { ?>
    <div class="form-group<?php echo ($custom_field['required'] ? ' required' : ''); ?> custom-field" data-sort="<?php echo $custom_field['sort_order']; ?>">
      <label class="col-sm-2 control-label"><?php echo $custom_field['name']; ?></label>
      <div class="col-sm-10">
        <button type="button" id="button-shipping-custom-field<?php echo $custom_field['custom_field_id']; ?>" data-loading-text="<?php echo $text_loading; ?>" class="btn btn-default"><i class="fa fa-upload"></i> <?php echo $button_upload; ?></button>
        <input type="hidden" name="custom_field[<?php echo $custom_field['custom_field_id']; ?>]" value="" id="input-shipping-custom-field<?php echo $custom_field['custom_field_id']; ?>" />
      </div>
    </div>
    <?php } ?>
    <?php if ($custom_field['type'] == 'date') { ?>
    <div class="form-group<?php echo ($custom_field['required'] ? ' required' : ''); ?> custom-field" data-sort="<?php echo $custom_field['sort_order']; ?>">
      <label class="col-sm-2 control-label" for="input-shipping-custom-field<?php echo $custom_field['custom_field_id']; ?>"><?php echo $custom_field['name']; ?></label>
      <div class="col-sm-10">
        <div class="input-group date">
          <input type="text" name="custom_field[<?php echo $custom_field['custom_field_id']; ?>]" value="<?php echo $custom_field['value']; ?>" placeholder="<?php echo $custom_field['name']; ?>" data-date-format="YYYY-MM-DD" id="input-shipping-custom-field<?php echo $custom_field['custom_field_id']; ?>" class="form-control" />
          <span class="input-group-btn">
          <button type="button" class="btn btn-default"><i class="fa fa-calendar"></i></button>
          </span></div>
      </div>
    </div>
    <?php } ?>
    <?php if ($custom_field['type'] == 'time') { ?>
    <div class="form-group<?php echo ($custom_field['required'] ? ' required' : ''); ?> custom-field" data-sort="<?php echo $custom_field['sort_order']; ?>">
      <label class="col-sm-2 control-label" for="input-shipping-custom-field<?php echo $custom_field['custom_field_id']; ?>"><?php echo $custom_field['name']; ?></label>
      <div class="col-sm-10">
        <div class="input-group time">
          <input type="text" name="custom_field[<?php echo $custom_field['custom_field_id']; ?>]" value="<?php echo $custom_field['value']; ?>" placeholder="<?php echo $custom_field['name']; ?>" data-date-format="HH:mm" id="input-shipping-custom-field<?php echo $custom_field['custom_field_id']; ?>" class="form-control" />
          <span class="input-group-btn">
          <button type="button" class="btn btn-default"><i class="fa fa-calendar"></i></button>
          </span></div>
      </div>
    </div>
    <?php } ?>
    <?php if ($custom_field['type'] == 'datetime') { ?>
    <div class="form-group<?php echo ($custom_field['required'] ? ' required' : ''); ?> custom-field" data-sort="<?php echo $custom_field['sort_order']; ?>">
      <label class="col-sm-2 control-label" for="input-shipping-custom-field<?php echo $custom_field['custom_field_id']; ?>"><?php echo $custom_field['name']; ?></label>
      <div class="col-sm-10">
        <div class="input-group datetime">
          <input type="text" name="custom_field[<?php echo $custom_field['custom_field_id']; ?>]" value="<?php echo $custom_field['value']; ?>" placeholder="<?php echo $custom_field['name']; ?>" data-date-format="YYYY-MM-DD HH:mm" id="input-shipping-custom-field<?php echo $custom_field['custom_field_id']; ?>" class="form-control" />
          <span class="input-group-btn">
          <button type="button" class="btn btn-default"><i class="fa fa-calendar"></i></button>
          </span></div>
      </div>
    </div>
    <?php } ?>
    <?php } ?>
    <?php } ?>
  </div>
  <div class="buttons clearfix">
    <div class="pull-right">
      <input type="button" value="<?php echo $button_continue; ?>" id="button-shipping-address" data-loading-text="<?php echo $text_loading; ?>" class="btn btn-primary" />
    </div>
  </div>
</form>
<script type="text/javascript">
$(document).ready(() => {
	let citc_region_id = $('#input-shipping-citc-region').val();
	// console.log(citc_region_id);
	// Set city on page load if Region id value is selected
	if(citc_region_id) {
		set_citc_city(citc_region_id);
	}
	// Set city on Region change
	$('#input-shipping-citc-region').on('change', function(e) {
		let citc_region_id = $('#input-shipping-citc-region').val();
		// console.log(citc_region_id);
		set_citc_city(citc_region_id);
	});
});
function set_citc_city(citc_region_id) {
	$.ajax({
		url: 'index.php?route=account/register/get_citc_city',
		type: 'post',
		data: {citc_region_id: citc_region_id},
		dataType: 'json',
		success: (json) => {
			let output = [];
      if(!'<?php echo $citc_city_id ?>') {
        output.push(`<option selected value="" > <?php echo $text_select; ?></option>`);
      }
			json.data.forEach((city) => {
				if (city.city_id == '<?php echo $citc_city_id ?>') {
					output.push(`<option selected value="${city.city_id}|${city.city_name}"> ${city.city_name} </option>`);
				} else {
					output.push(`<option value="${city.city_id}|${city.city_name}"> ${city.city_name} </option>`);
				}
			});
			$('#input-shipping-citc-city').html(output.join(''));
		},
		error: (error) => {
			console.log(`error: ${error}`)
		}
	});
}
</script>

<script type="text/javascript"><!--
    $('input[name=\'shipping_address\']').on('change', function() {
        if (this.value == 'new') {
            $('#shipping-existing').hide();
            $('#shipping-new').show();
        } else {
            $('#shipping-existing').show();
            $('#shipping-new').hide();
        }
    });
    //--></script>
<script type="text/javascript"><!--
    $('#collapse-shipping-address .form-group[data-sort]').detach().each(function() {
        if ($(this).attr('data-sort') >= 0 && $(this).attr('data-sort') <= $('#collapse-shipping-address .form-group').length) {
            $('#collapse-shipping-address .form-group').eq($(this).attr('data-sort')).before(this);
        }

        if ($(this).attr('data-sort') > $('#collapse-shipping-address .form-group').length) {
            $('#collapse-shipping-address .form-group:last').after(this);
        }

        if ($(this).attr('data-sort') < -$('#collapse-shipping-address .form-group').length) {
            $('#collapse-shipping-address .form-group:first').before(this);
        }
    });
    //--></script>
<script type="text/javascript"><!--
    $('#collapse-shipping-address button[id^=\'button-shipping-custom-field\']').on('click', function() {
        var node = this;

        $('#form-upload').remove();

        $('body').prepend('<form enctype="multipart/form-data" id="form-upload" style="display: none;"><input type="file" name="file" /></form>');

        $('#form-upload input[name=\'file\']').trigger('click');

        if (typeof timer != 'undefined') {
            clearInterval(timer);
        }

        timer = setInterval(function() {
            if ($('#form-upload input[name=\'file\']').val() != '') {
                clearInterval(timer);

                $.ajax({
                    url: 'index.php?route=tool/upload',
                    type: 'post',
                    dataType: 'json',
                    data: new FormData($('#form-upload')[0]),
                    cache: false,
                    contentType: false,
                    processData: false,
                    beforeSend: function() {
                        $(node).button('loading');
                    },
                    complete: function() {
                        $(node).button('reset');
                    },
                    success: function(json) {
                        $(node).parent().find('.text-danger').remove();

                        if (json['error']) {
                            $(node).parent().find('input[name^=\'custom_field\']').after('<div class="text-danger">' + json['error'] + '</div>');
                        }

                        if (json['success']) {
                            alert(json['success']);

                            $(node).parent().find('input[name^=\'custom_field\']').attr('value', json['code']);
                        }
                    },
                    error: function(xhr, ajaxOptions, thrownError) {
                        alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
                    }
                });
            }
        }, 500);
    });
    //--></script>
<script type="text/javascript"><!--
    $('.date').datetimepicker({
        pickTime: false
    });

    $('.time').datetimepicker({
        pickDate: false
    });

    $('.datetime').datetimepicker({
        pickDate: true,
        pickTime: true
    });
    //--></script>
<script type="text/javascript"><!--
    $('#collapse-shipping-address select[name=\'country_id\']').on('change', function() {
        $.ajax({
            url: 'index.php?route=checkout/checkout/country&country_id=' + this.value,
            dataType: 'json',
            beforeSend: function() {
                $('#collapse-shipping-address select[name=\'country_id\']').after(' <i class="fa fa-circle-o-notch fa-spin"></i>');
            },
            complete: function() {
                $('.fa-spin').remove();
            },
            success: function(json) {
                if (json['postcode_required'] == '1') {
                    $('#collapse-shipping-address input[name=\'postcode\']').parent().parent().addClass('required');
                } else {
                    $('#collapse-shipping-address input[name=\'postcode\']').parent().parent().removeClass('required');
                }

                html = '<option value=""><?php echo $text_select; ?></option>';

                if (json['zone'] && json['zone'] != '') {
                    for (i = 0; i < json['zone'].length; i++) {
                        html += '<option value="' + json['zone'][i]['zone_id'] + '"';

                        if (json['zone'][i]['zone_id'] == '<?php echo $zone_id; ?>') {
                            html += ' selected="selected"';
                        }

                        html += '>' + json['zone'][i]['name'] + '</option>';
                    }
                } else {
                    html += '<option value="0" selected="selected"><?php echo $text_none; ?></option>';
                }

                $('#collapse-shipping-address select[name=\'zone_id\']').html(html);
            },
            error: function(xhr, ajaxOptions, thrownError) {
                alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
            }
        });
    });

    $('#collapse-shipping-address select[name=\'country_id\']').trigger('change');
    //--></script>

<script>
    var marker;
    //initMapStart
    function initMap() {
        var myCenter = {lat: 31.5546, lng: 74.3572};

        var map = new google.maps.Map(document.getElementById('map'), {
            zoom: 14,
            center: myCenter,
            mapTypeId: google.maps.MapTypeId.ROADMAP
        });
        var geocoder = new google.maps.Geocoder;
        google.maps.event.addListener(map, 'click', function(e) {

            placeMarker(e.latLng, map,geocoder);
        });

        // Create the search box and link it to the UI element.
        var input = /** @type {HTMLInputElement} */(
            document.getElementById('pac-input'));
        map.controls[google.maps.ControlPosition.TOP_LEFT].push(input);



        var searchBox = new google.maps.places.SearchBox(
            /** @type {HTMLInputElement} */(input));

        // [START region_getplaces]
        // Listen for the event fired when the user selects an item from the
        // pick list. Retrieve the matching places for that item.
        google.maps.event.addListener(searchBox, 'places_changed', function() {
            var places = searchBox.getPlaces();

            if (places.length == 0) {
                return;
            }


            // For each place, get the icon, place name, and location.

            var bounds = new google.maps.LatLngBounds();
            for (var i = 0, place; place = places[i]; i++) {
                var image = {
                    url: place.icon,
                    size: new google.maps.Size(71, 71),
                    origin: new google.maps.Point(0, 0),
                    anchor: new google.maps.Point(17, 34),
                    scaledSize: new google.maps.Size(25, 25)
                };

                // Create a marker for each place.
                if (!marker || !marker.setPosition) {
                    marker = new google.maps.Marker({
                        position: place.geometry.location,
                        map: map,
                    });
                } else {
                    marker.setPosition(place.geometry.location);
                }



                // $('#input-lat'+mapId).val(place.geometry.location.lat());
                //$('#input-long'+mapId).val(place.geometry.location.lng());
                geocodeLatLng(place.geometry.location,geocoder);
                bounds.extend(place.geometry.location);
				$('#shipping_lattitude').val(place.geometry.location.lat());
                $('#shipping_longitude').val(place.geometry.location.lng());
            }

            map.fitBounds(bounds);
            map.setZoom(14);
        });
        // [END region_getplaces]

    }//initMapEnd

    //placeMarkerStart
    function placeMarker(position, map,geocoder) {
        if (!marker || !marker.setPosition) {
            marker = new google.maps.Marker({
                position: position,
                map: map,
            });
        } else {
            marker.setPosition(position);
        }

        map.panTo(position);
        geocodeLatLng(position,geocoder);
		$('#shipping_lattitude').val(position.lat);
        $('#shipping_longitude').val(position.lng);
    } //placeMarkerEnd

    //geocodeLatLngStart
    function geocodeLatLng(location,geocoder) {

        geocoder.geocode({'location': location}, function(results, status) {
            if (status === 'OK') {
                if (results[0]) {
                    var city = "";
                    var area = "";
                    debugger;
                    for(var i=0, len=results[0].address_components.length; i<len; i++) {
                        var ac = results[0].address_components[i];
                        if(ac.types.indexOf("locality") >= 0) city = ac.long_name;
                        if(ac.types.indexOf("route") >= 0) area = ac.long_name;

                    }
                    $("#input-shipping-address-1").val(results[0].formatted_address);
                    // $("#input-area"+mapId).val(area);
                    $("#input-shipping-city").val(city);

                } else {
                    window.alert('No results found');
                }
            } else {
                window.alert('Geocoder failed due to: ' + status);
            }
        });
    }//geocodeLatLngEnd
</script>

<script async defer
        src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBZ4-CaBdvKPNqamRcb4yzRDn3vYVuJgE0&libraries=places&callback=initMap">
</script>