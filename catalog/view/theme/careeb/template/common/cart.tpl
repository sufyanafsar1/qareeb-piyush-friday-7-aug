<div class="b-cart dropdown_cart cart">
	<button id="cartBtn" type="button" data-toggle="collapse" data-target="#cartDropDown" data-loading-text="<?php echo $text_loading; ?>" class="btn-dropdown-cart dropdown-toggle">
      <span class="b-cart-total" id="b-cart-total">
       <?php echo $text_items; ?>
      </span>
	</button>
	<style>
		.custom-cart-body {
			color: white;
			background: #69489d;
			padding: 5px;
			height: 50px;
		}
		span.custom-cart-left {
			float: left;
		}
		span.custom-cart-right {
			float: right;
		}
		span.custom-cart-right.total {
			padding-right: 10px;
		}
	</style>
	
	<?php 
	$product_error = 0;
	if ($sellerallInfo || $vouchers) {
	?>	
	
		<ul class="dropdown-menu pull-right collapse" id="cartDropDown">
			<span class="retailer-chooser-arrow"></span>
			<div class="force-overflow"></div>	
			
			<?php
			
			foreach($sellerallInfo as $sellerinfo){ ?>
			
			<li>	
				<div class="custom-cart-header">
					<div class="custom-cart-body">
						<span class="custom-cart-left"><?php echo $sellerinfo['store_name']; ?></span> <span class="custom-cart-right">Items: <?php echo $sellerinfo['product_count_total']; ?></span> <span class="custom-cart-right total">	Total:  <?php echo $sellerinfo['product_total']; ?></span>
						<br/>
						<span class="custom-cart-left">Min Limit: <?php echo $sellerinfo['minimumOrder']; ?></span><span class="custom-cart-right">Delivery Fee:  <?php 
						if(($sellerinfo['product_total'] < $sellerinfo['free_delivery_limit']) or ($sellerinfo['free_delivery_limit'] == 0)){
							echo $sellerinfo['delivery_charges'];
						}else{
							echo 0.00;
						}
						
						
						 ?></span>
					</div> 
				</div>
			</li>
			<li>	
				<div class="storeName"><span><?php echo $sellerinfo['store_name']; ?></span></div> 
			</li>	
			<li>	
				<?php if($sellerinfo['product_total'] < $sellerinfo['minimumOrder']){ $product_error = 1;?>
	
					<div class="alert-danger alert-custom mini-order-limit"> <?php echo  $sellerinfo['text_minimum_order']; ?></div>
				<?php } ?>  
			</li>					
							
				<?php 
				$i=0; 
				foreach ($sellerinfo['products'] as $product) { ?>
				<li>
					<div class="mainDivCart">
						<div class="col-sm-2 thumbDiv">
							<?php if ($product['thumb']) { ?>
								<a href="<?php echo $product['href']; ?>"><img src="<?php echo $product['thumb']; ?>" alt="<?php echo $product['name']; ?>" title="<?php echo $product['name']; ?>" class="cart-product" /></a>
							<?php } ?>	
						</div>
						
						<div class="col-sm-8" style=" text-align: left;">
							<a class="productName" href="<?php echo $product['href']; ?>"><?php echo $product['name']; ?></a>	
							<?php if ($product['option']) { ?>
								<?php foreach ($product['option'] as $option) { ?>
								<br />
								- <small><?php echo $option['name']; ?> <?php echo $option['value']; ?></small>
								<?php } ?>
							<?php } ?>
							<?php if ($product['recurring']) { ?>
								<br />
								- <small><?php echo $text_recurring; ?> <?php echo $product['recurring']; ?></small>
							<?php } ?>	
							<span class="price price-popup"><?php echo $product['total']; ?></span>
							<br />
							<div class="qty-button">
								<div class="visible-xs title_column"><?php //echo $column_price; ?></div>
								
								<button onclick="changeCartQty('<?php echo 'item-number'.$product['cart_id'];?>',0); return false;" class="decrease"><img src="catalog/view/theme/careeb/img/cart-minus.svg"></button>
								
								<input onchange="updateCartQty('<?php echo $product['cart_id']; ?>', '<?php echo 'item-number'.$product['cart_id'];?>')" id="item-number<?php echo $product['cart_id']; ?>" type="text" name="quantity[<?php echo $product['cart_id']; ?>]" value="<?php echo $product['quantity']; ?>" size="1" class="form-control qty-input" />
							
								<button onclick="changeCartQty('<?php echo 'item-number'.$product['cart_id'];?>',1); return false;" class="increase"><img src="catalog/view/theme/careeb/img/cart-plus.svg"></button>
								
							</div>
						</div>
						<div class="col-sm-2 removeDiv">
							<div class="remove-custom">
								<?php /* <button type="button" onclick="confirm('you want to delete this product?'); cart.remove('<?php echo $product['cart_id']; ?>');" title="<?php echo $button_remove; ?>" class="button_remove"><img src="catalog/view/theme/careeb/img/cart-x.svg"></button> */?>
								<button type="button" onclick="confirm('you want to delete this product?'); cart.remove('<?php echo $product['cart_id']; ?>');" title="<?php echo $button_remove; ?>" class="button_remove"><i class="fa fa-times" aria-hidden="true"></i></button>
							</div>
						</div>
					</div>
				</li>						
				<?php 
				$i++; 
				}
				} ?>
				<li>
				<?php 
				foreach ($vouchers as $voucher) { ?>
					<div class="col-sm-12 voucher">
						<div class="col-sm-6 image">								
							<div class="remove"><button type="button" onclick="voucher.remove('<?php echo $voucher['cart_id']; ?>');" title="<?php echo $button_remove; ?>" class="btn-danger"><i class="fa fa-times"></i></button></div>							
						</div>
						
						<div class="col-sm-6" style=" text-align: left;">
							<div class="description"><?php echo $voucher['description']; ?></div>
							<div class="quantity"><span class="price"><?php echo $voucher['amount']; ?> x 1 </div>
						</div>
						
					</div>					
				<?php 
				} ?>				
			</li>	
			<li>	
				<div class="couponPopup mainDivCart"><?php echo $coupon_popup; ?></div> 
			</li>	
			<li class="sub-total">
				<div class="cart_bottom">
					<div class="col-sm-12 minicart_total">
						<?php foreach ($totals as $total) { ?>
							<div class="col-sm-6 text-left"><b><?php echo $total['title']; ?></b></div>			
							<div class="col-sm-6 text-right"><b><?php echo $total['text']; ?></b></div>			
						<?php } ?>
					</div>
								  
				</div>
			</li>
			<li class="btnLI">
				<div class="buttons">    
					<span class="checkout_bt">
					   <?php  				  
							if(isset($_SESSION['default']['customer_id']) && $_SESSION['default']['customer_id'] > 0)
							{
								if($product_error){
						?>
									<a class="btn btn-primary btn-primary-custom" disabled><?php echo $text_checkout; ?></a>
						
						<?php
								}else{ ?>
									<a class="btn btn-primary btn-primary-custom" href="<?php echo $checkout; ?>"><?php echo $text_checkout; ?></a>
							<?php
								}

							} else{ ?>
								<button class="btn btn-primary btn-primary-custom" id="chkoutLogin" onclick="showLoginPopup();"> <?php echo $text_checkout; ?></button>
						 
							<?php  
							}   ?>
						   
					</span>	
				</div>
				<div class="cart_bottom">
					<!--<table class="minicart_total">
					  <?php foreach ($totals as $total) { ?>
					  <tr>
						<td class="text-left"><span><?php echo $total['title']; ?></span></td>
						<td class="text-right <?php echo ($total==end($totals) ? ' last' : ''); ?>"><?php echo $total['text']; ?></td>
					  </tr>
					  <?php } ?>
					</table> -->
					<div class="alert-danger alert-custom free-delivery"> <?php echo  $data['delivery']; ?></div>
					<div class="alert-success-custom alert-custom free-delivery"> <?php echo  $data['next_delivery']; ?></div>
				</div>				
			</li>
			</ul>
			
		<?php
		 }else { ?>
			<ul class="dropdown-menu pull-right collapse CartBlank" id="cartDropDown">
				<span class="retailer-chooser-arrow"></span>
				<li>
					<p class="text-center"><?php echo $text_empty; ?></p>
				</li>
			</ul>
		<?php 
		} ?>
	
</div><!-- b-cart--->
