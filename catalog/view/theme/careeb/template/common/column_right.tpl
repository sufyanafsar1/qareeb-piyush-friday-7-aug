<?php if ($modules) {
	$i = 1; foreach ($modules as $module) {
		if (!is_null($module)):
			if ($i == 1): ?>
				<column id="column-right" class="col-sm-3 hidden-xs">	
			<?php endif ?>
			  	<div class='filter'>
			  		<?php echo $module; ?>
			  	</div>
			<?php $i++;
		endif;
	}
	if ($i > 1) {
		echo "</column>";
	}
} ?>