<?php


$user_agent = getenv("HTTP_USER_AGENT");

if(strpos($user_agent, "Win") !== FALSE){
  $os = "Windows";
}elseif(strpos($user_agent, "Mac") !== FALSE){
  $os = "Mac";
}else{
  $os = "other";
}
?>
<?php global $config; global $request; ?>
<?php
  $boss_manager = array(
    'option' => array(
      'bt_scroll_top' => true,
      'animation'   => true,      
    ),
    'layout' => array(
      'mode_css'   => 'wide',
      'box_width'   => 1200,
      'h_mode_css'   => 'inherit',
      'h_box_width'   => 1200,
      'f_mode_css'   => 'inherit',
      'f_box_width'   => 1200
    ),
    'status' => 1
  );
?>
<?php if($config->get('boss_manager')){
    $boss_manager = $config->get('boss_manager'); 
  }else{
    $boss_manager = $boss_manager;
  } 
  $header_link = isset($boss_manager['header_link'])?$boss_manager['header_link']:''; 
  $option = isset($boss_manager['option'])?$boss_manager['option']:''; 
  $loading = isset($boss_manager['option']['loading'])?$boss_manager['option']['loading']:''; 
?>
<!DOCTYPE html>
<!--[if IE]><![endif]-->
<!--[if IE 8 ]><html dir="<?php echo $direction; ?>" lang="<?php echo $lang; ?>" class="ie8"><![endif]-->
<!--[if IE 9 ]><html dir="<?php echo $direction; ?>" lang="<?php echo $lang; ?>" class="ie9"><![endif]-->
<!--[if (gt IE 9)|!(IE)]><!-->
<html dir="<?php echo $direction; ?>" lang="<?php echo $lang; ?>">
<!--<![endif]-->
<html>
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title><?php echo $title; ?></title>
    <base href="<?php echo $base; ?>" />
    <?php if ($description) { ?>
    <meta name="description" content="<?php echo $description; ?>" />
    <?php } ?>
    
    <?php if ($keywords) { ?>
    <meta name="keywords" content= "<?php echo $keywords; ?>" />
    <?php } ?>
    
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <link href="https://careeb.com/development/image/catalog/logo/Favicon-1.png" rel="icon">
    <link rel="stylesheet" href="catalog/view/theme/<?php echo $config->get('config_template'); ?>/css/bootstrap.css">
    <link rel="stylesheet" href="catalog/view/theme/<?php echo $config->get('config_template'); ?>/css/font-awesome.min.css">
    <link href="https://fonts.googleapis.com/css?family=Lato" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Lato:100,400" rel="stylesheet">
    <link rel="stylesheet" href="catalog/view/theme/<?php echo $config->get('config_template'); ?>/css/style.css">
    <?php if($direction=='rtl'){ ?>
    <link rel="stylesheet" type="text/css" href="catalog/view/theme/<?php echo $config->get('config_template'); ?>/css/stylertl.css" />
    <?php } ?>
    <script src="catalog/view/theme/<?php echo $config->get('config_template'); ?>/js/jQuery.js"></script>
    <script src="catalog/view/theme/<?php echo $config->get('config_template'); ?>/js/bootstrap.min.js"></script>
    <script src="catalog/view/theme/<?php echo $config->get('config_template'); ?>/js/tricks.js"></script>
    <script type="text/javascript" src="catalog/view/javascript/bossthemes/common.js"></script>
    <script type="text/javascript" src="catalog/view/javascript/bossthemes/google-map.js"></script>
    <script type="text/javascript" src="catalog/view/javascript/bossthemes/ipmapper.js"></script>
    
    <script src="catalog/view/javascript/bossthemes/cs.bossthemes.js" type="text/javascript" ></script>
    <script src="catalog/view/javascript/bossthemes/parallax/jquery.parallax-1.1.3.js" type="text/javascript" ></script>
    <script src="catalog/view/javascript/bossthemes/bootstrap-select.js"></script>

    <?php if (isset($analytics) && !empty($analytics)) { ?>
<?php foreach ($analytics as $analytic) { ?>
<?php echo $analytic; ?>
<?php } } ?>
<?php if(isset($this->request->get['route'])){$route1 = $this->request->get['route'];}else{$route1 ="";}
  if(isset($route1) && ($route1== "common/home" || $route1=="") && $loading){ ?>
    <script type="text/javascript"><!--
      $(document).ready(function() {
        $(".bt-loading").fadeOut("1500", function () {
          $('#bt_loading').css("display", "none");
        });
      });
    //--></script>
  <?php }else{ ?>
    <script type="text/javascript"><!--
    $(document).ready(function() {
    $('#bt_loading').css("display", "none");
    });
    //--></script>
  <?php } ?>
      <link rel="manifest" href="/manifest.json">
      <script src="https://cdn.onesignal.com/sdks/OneSignalSDK.js" async></script>
      <script>
          var OneSignal = window.OneSignal || [];
          OneSignal.push(["init", {
              appId: "21057e6c-1429-44b2-8e38-2c3f5eae3186",
              autoRegister: false,
              persistNotification: false,
              welcomeNotification: {
                  "title": "Careeb Online Grocery Store",
                  "message": "Thanks for subscribing !",
                  // "url": "" /* Leave commented for the notification to not open a window on Chrome and Firefox (on Safari, it opens to your webpage) */
              },

              notifyButton: {
                  enable: true, /* Required to use the notify button */
                  size: 'medium', /* One of 'small', 'medium', or 'large' */
                  showCredit: false, /* Hide the OneSignal logo */
                  position: 'bottom-left', /* Either 'bottom-left' or 'bottom-right' */
                  colors: { // Customize the colors of the main button and dialog popup button

                      'dialog.button.background': 'rgb(22,107,24)',
                      'circle.background': 'rgb(22,107,24)',
                      'circle.foreground': 'white'
                  }


              }

          }]);
      </script>
  </head>
  <?php 
  if(isset($request->get['route'])){
    $route = $request->get['route'];
  }else{
    $route ="";
  }
  if(isset($route) && ($route== "common/home" || $route=="")){
    $home_page='landingBody';
  }else{
    $home_page="landingBody";
  }
?>
  
  <body class="<?php echo $home_page; ?>">

    <div class="TopBar">
      
      <div class="logo pull-left">
         <?php if ($logo) { ?>
      <a href="<?php echo $home; ?>"><img src="<?php echo $logo; ?>" title="<?php echo $name; ?>" class="img-responsive" alt="<?php echo $name; ?>" /></a>
      <?php } else { ?>
      <h1><a href="<?php echo $home; ?>"><?php echo $name; ?></a></h1>
      <?php } ?>
     
        </div>
      <div class="links pull-right">
    <?php if(isset($header_link['language']) && $header_link['language']){ ?> <div class="pull-left language_div"><?php echo $language; ?></div> <?php } ?>
        <div class="pull-right">
        <!--<a href=""><?php echo $visit_careeb; ?></a>-->
          <br/>
          <a class="hitw" href=""><?php echo $how_it_work ?> </a> |
        <?php if ($logged) { ?>
        <a href="" class="dropdown-toggle" data-toggle="dropdown"><span>Hello</span>, <?php echo $customer_name ?></a>
         <ul class="dropdown-menu home_dropdown-menu-right">
      <li><a href="<?php echo $account; ?>"><?php echo $text_account_deatil; ?></a></li>
                <li><a href="<?php echo $wishlist; ?>"><?php echo $text_wishlist; ?></a></li>     
                <li><a href="<?php echo $transaction; ?>"><?php echo $text_store_credit; ?></a></li>
                <li><a href="<?php echo $order; ?>"><?php echo $text_order; ?></a></li>
          <hr class="custom-hr"/>
        <li><a href="<?php echo $logout; ?>"><?php echo $text_logout; ?></a></li>     
    </ul>
        <?php } else { ?>
        <a href="javascript:void(0);" id="signIn"><?php echo $text_register .'/'.$text_login; ?></a>
        <?php } ?>
      </div>

      </div>

    </div> <!-- Topbar End-->
    
     <div class='slider'>

      <div id="carousel-example-generic" class="carousel slide" data-ride="carousel">
      
          <!-- Wrapper for slides -->
          <div class="carousel-inner" role="listbox">
            <div class="item active">
              <img class="img-responsive" src="catalog/view/theme/<?php echo $config->get('config_template'); ?>/img/landingSliderimg1.jpg" alt="...">
              <div class="carousel-caption">

                <h3><?php echo html_entity_decode($text_shopping); ?></h3>
                <p><?php echo html_entity_decode($text_grocery); ?></p>
                 
                 
                 <div class="col-sm-9">
                     <form method="POST" action="<?php echo $action; ?>">
                      <div style=''>
                        <select  name="city_id" id="city_id">
                          <option value="0"><?php echo $text_city; ?></option>
                          <img style="position: absolute" src="catalog/view/theme/<?php echo $config->get('config_template'); ?>/img/landingbuttonselect.png"/>
                              <?php  foreach ($cities as $city) { 
                                   $selectedCity = ($city['city_id']==$city_saved) ? 'selected' : '';
                                ?>                            
                                <option value="<?php echo $city['city_id']; ?>" <?php echo $selectedCity; ?> ><?php echo $city['name']; ?></option>                           
                                <?php } ?>
                        </select>
                      </div>
                     <!-- <select>
                        <option><?php echo $text_region; ?></option>
                      </select> -->
                      <select  name="area_id" id="area_id">
                        <option value="0"><?php echo $text_area; ?></option>
                        <img src="catalog/view/theme/<?php echo $config->get('config_template'); ?>/img/arrowButtommm.png">
                      </select>
                      
                      <input type="submit" id="findMe" value="<?php echo $text_find; ?>"/>                      
                     
                        <div class="col-sm-12" id="error_alert"></div>
                    </form>                  
                 </div>
                 
                  <div class="col-sm-3">
                     <form id="frmFindMe"  method="POST" action="<?php echo $action; ?>">
                         <input type="text" name="user_lat" id="user_lat" value="" style="display:none" />
                         <input type="text" name="user_lng" id="user_lng" value="" style="display:none"/>
                         <input type="text" name="user_area" id="user_area" value="" style="display:none"/>
                         <button type="button" id="locateMe" class='locateMe'><img class="img-responsive" src="catalog/view/theme/<?php echo $config->get('config_template'); ?>/img/locateMe.png"/>&nbsp;&nbsp;&nbsp;<?php echo $text_locate_me; ?></button>
                     </form> 
                  </div>
            
                  

<br/><br/><br/>

                <div class='gotonextSection'><img style="transform: scale(0.8);" src="catalog/view/theme/<?php echo $config->get('config_template'); ?>/img/whitearrow.png"></div>

              </div>
            </div>
           
          </div>
         
        </div>
	<!-- Modal -->
    <div id="model-left">
   
	<div class="modal left fade" id="storeHomeModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" data-backdrop="static" data-keyboard="false">
     
		<div class="modal-dialog" role="document">
			<div class="modal-content">
<span class="retailer-chooser-arrow"></span>
				<div class="modal-header" id="modelHeader">
				
                                       
				</div>

				<div class="modal-body">
				    <div>
                        <div id="modalBody">
                        <?php if(isset($stores)) { ?>
                            <h3 class="retailer-group-label"><?php echo $text_store_recommended; ?></h3>
                            <ul class="retailer-options-wrapper unstyled">
                            <?php foreach($stores as $store) { ?>
                                <li class="retailer-option">
                                    <a class="retailer-option-inner-wrapper absolute-center" <?php if($product_count != 0){ ?>"<?php } ?> href="<?php echo $store['action'] ?>" data-bypass="true">
                                        <div class="retailer-option-logo-wrapper">
                                            <img class="absolute-center" src="<?php echo $store['image']; ?>" alt="<?php echo $store['firstname']  ?>">
                                        </div>
                                        <div class="retailer-option-body">
                                            <h3><?php echo $store['firstname']?></h3>
                                            <
                                        </div>
                                    </a>
                                </li>
                                <?php } ?>
                            </ul>
                        <?php }  ?>
                        </div>
                    </div>
				</div><!-- modal-body -->

			</div><!-- modal-content -->
		</div><!-- modal-dialog -->
	</div><!-- modal -->
</div>
    </div>
    <?php echo $signin; ?>
 <script type="text/javascript"><!--
$('select[name=\'city_id\']').on('change', function() {
   if(this.value == 0)
   return false;
	$.ajax({
		url: 'index.php?route=common/header_home/areas&city_id=' + this.value,
		dataType: 'json',
		beforeSend: function() {
			//$('select[name=\'city_id\']').after(' <i class="fa fa-circle-o-notch fa-spin"></i>');
		},
		complete: function() {
		//	$('.fa-spin').remove();
		},
		success: function(json) {
		//	$('.fa-spin').remove();
	
			
	
			html = '<option value="0"><?php echo $text_area; ?></option>';
	
			if (json) {
				for (i = 0; i < json['areas'].length; i++) {
					html += '<option value="' + json['areas'][i]['area_id'] + '"';
	
					if (json['areas'][i]['area_id'] == json['area_saved']) {
						html += ' selected="selected"';
			  		}
	
			  		html += '>' + json['areas'][i]['name'] + '</option>';
				}
			} else {
				html += '<option value="0" selected="selected"><?php echo $text_area; ?></option>';
			}
	
			$('select[name=\'area_id\']').html(html);
		},
		error: function(xhr, ajaxOptions, thrownError) {
			alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
		}
	});
});

$('select[name=\'city_id\']').trigger('change');
//--></script>  
<script type="text/javascript">
$("#locateMe").click(function(){
   getUserCurrentLocation();
});

    function getUserCurrentLocation() {
        navigator.geolocation.getCurrentPosition(success, error, options);
    }
    
    
var options = {
  enableHighAccuracy: true,
  timeout: 5000,
  maximumAge: 0
};

function success(pos) {
    var crd = pos.coords;
    var lat = crd.latitude;
    var lng = crd.longitude;
    
    
    $("#user_lat").val(lat);
    $("#user_lng").val(lng);
    var auto_area = "";
    var auto_city = "";
    var apiKey = "AIzaSyBlpCdiNIP0fJnV1TFhaNwyEXbDNCrqz9E";
    var baseUrl= "https://maps.googleapis.com/maps/api/geocode/json?latlng=";
    var url = encodeURI(baseUrl + lat+","+lng+"&key="+apiKey); //geocoding url
    $.getJSON(url, function(data) { //get Geocoded JSONP data
                    if(data !="")
                    {

                      var result = data.results[0];

                      
                      for(var i=0, len=result.address_components.length; i<len; i++) {
                        
                    	var ac = result.address_components[i];
                    	if(ac.types.indexOf("locality") >= 0) auto_city = ac.long_name;
                        if(ac.types.indexOf("route") >= 0) auto_area = ac.long_name;
                    	
                    }//for loop

                      $("#user_area").val(auto_area);

                    }// Data not null

        var apiLink =   'index.php?route=common/store/getStoresByAreaId&user_lat=' + lat + '&user_lng='+ lng+ '&user_area='+ auto_area;

        setTimeout(function(){
            //$("#frmFindMe" ).submit();
            openStoresModal(apiLink);
        }, 200);

    }); //json

   
};

function error(err) {
 
    $(".alert-danger").remove();
    $('#error_alert').append('<div class="alert alert-danger alert-custom"><i class="fa fa-exclamation-circle"></i> <?php echo $text_location_disable; ?> </div>');
   
    return false;
};    


$('#findMe').click(function () {
        var city = $("#city_id").val();
        var area = $("#area_id").val();

                if (city == 0) {
                    $(".alert-danger").remove();
                    $('#error_alert').append('<div class="alert alert-danger alert-custom"><i class="fa fa-exclamation-circle"></i> <?php echo $text_city; ?> </div>');
                    $('#city_id').addClass('has-error');                  
                    $('#city_id').focus();
                    return false;
                }
                else if (area==0) {
                    $(".alert-danger").remove();
                    $('#error_alert').append('<div class="alert alert-danger alert-custom" ><i class="fa fa-exclamation-circle"></i>  <?php echo $text_area; ?></div>');
                    $('#area_id').addClass('has-error');                  
                    $('#area_id').focus();
                    return false;
                }
                
              var apiLink =   'index.php?route=common/store/getStoresByAreaId&area_id=' + area+ '&city_id='+ city;
             openStoresModal(apiLink)
               
               return false;
    });
    
    
    function openStoresModal(apiLink){

        //Retrieve Stores        
       $.ajax({
		url: apiLink ,
		dataType: 'json',	
		success: function(json) {
		  var headerHtml = '<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>';
          
              if(json['stores'] !=""){
                headerHtml += "<h1 class='centered'>" + json['text_store']+  "  <strong>" + json['area'] + "</strong></h1>";
              }else{
                 headerHtml += "<h1 class='centered'>" + json['error_service']+  "  <strong>" + json['area'] + "</strong></h1>";
              }
              var bodyHtml = '';
               if(json['stores'] !=""){
                    bodyHtml += "<h3 class='retailer-group-label'> " + json['text_store_recommended'] + "</h3>";
                    bodyHtml += "<ul class='retailer-options-wrapper unstyled'>";
                    for (i = 0; i < json['stores'].length; i++) {
                        bodyHtml += "<li class='retailer-option'>";
                        if(json['product_count'] > 0){
                         bodyHtml += "<a class='retailer-option-inner-wrapper absolute-center' href='"+ json['stores'][i]['action']  +"' data-bypass='true'>";
                        }else{
                           bodyHtml += "<a class='retailer-option-inner-wrapper absolute-center'  href='"+ json['stores'][i]['action']  +"' data-bypass='true'>"; 
                        }
                        
                        bodyHtml += "<div class='retailer-option-logo-wrapper'>";
                        bodyHtml += "<img class='absolute-center' src='" + json['stores'][i]['image'] + "' alt='" + json['stores'][i]['name'] + "'>";
                        bodyHtml += "</div>";
                        
                        
                        bodyHtml += "<div class='retailer-option-body'>";
                        bodyHtml += "<h3>" + json['stores'][i]['name'] + "</h3>";
                        bodyHtml += "</div>";
                        bodyHtml += "</a>";
                        bodyHtml += "</li>";                        
                    }
                    bodyHtml += "</ul>";
               }else{
                        bodyHtml += "<h4 class='centered'> " + json['error_area'] + "</h4>";
               }
              
          
          	$('#modelHeader').html(headerHtml);
            $('#modalBody').html(bodyHtml);
	
		},
		error: function(xhr, ajaxOptions, thrownError) {
			alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
		}
	   }); //Ajax end
 
               $("#storeHomeModal").modal('show');
    }
    
    
</script>