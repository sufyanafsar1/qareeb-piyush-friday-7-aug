<?php echo $signin; ?> 
<?php echo $forgotepopup; ?> 

<div id="bt_container"></div>
<footer>
	<div class="container-fluid container">		
		<div class="col-sm-6 desktop-section">
			<div class="footerPart">
				<?php if ($informations) { ?>			
					<div class="footerMenu">
						<h3><?php echo $text_information; ?></h3>
						<?php foreach ($informations as $information) { ?>
							<div><a href="<?php echo $information['href']; ?>"><?php echo $information['title']; ?></a></li></div>
						<?php } ?>
					</div>
				
				<?php } ?>   
				
				<div class="footerMenu">
					<h3><?php echo $text_service; ?></h3>
					<div><a href="<?php echo $contact; ?>"><?php echo $text_contact; ?></a></div>
					<?php /* <div><a href="<?php echo $return; ?>"><?php echo $text_return; ?></a></div> */ ?>		
				</div>
			</div>

			<div class="footerPart">
			<?php /*
				<div class="footerMenu">
					<h3><?php echo $text_extra; ?></h3>				
					<div><a href="<?php echo $special; ?>"><?php echo $text_special; ?></a></div>	
				</div> */ ?>						
			
				<div class="footerMenu">
					<h3><?php echo $text_join_us; ?></h3>				
					<div><a href="javascript:void(0);"><?php echo $text_sell_with_us; ?></a></div>
					<div><a href="javascript:void(0);"><?php echo $text_careers; ?></a></div>
					<div><a href="javascript:void(0);"><?php echo $text_become_a_shopper; ?></a></div>	
				</div>			
			</div>
		
		</div>

		<div class="col-sm-6">
			<?php echo $btfooter; ?>
		</div>
		
		<div class="col-sm-6 mobile-section">
			<div class="footerPart">
				<?php if ($informations) { ?>			
					<div class="footerMenu">
						<h3><?php echo $text_information; ?></h3>
						<?php foreach ($informations as $information) { ?>
							<div><a href="<?php echo $information['href']; ?>"><?php echo $information['title']; ?></a></li></div>
						<?php } ?>
					</div>
				
				<?php } ?>   
				
				<div class="footerMenu">
					<h3><?php echo $text_service; ?></h3>
					<div><a href="<?php echo $contact; ?>"><?php echo $text_contact; ?></a></div>
					<?php /* <div><a href="<?php echo $return; ?>"><?php echo $text_return; ?></a></div> */ ?>		
				</div>
			</div>

			<div class="footerPart">
				<?php /*
				<div class="footerMenu">
					<h3><?php echo $text_extra; ?></h3>				
					<div><a href="<?php echo $special; ?>"><?php echo $text_special; ?></a></div>		
				</div>	*/ ?>
			
				<div class="footerMenu">
					<h3><?php echo $text_join_us; ?></h3>				
					<div><a href="javascript:void(0);"><?php echo $text_sell_with_us; ?></a></div>
					<div><a href="javascript:void(0);"><?php echo $text_careers; ?></a></div>
					<div><a href="javascript:void(0);"><?php echo $text_become_a_shopper; ?></a></div>	
				</div>			
			</div>
		
		</div>
	</div>
	
	<div class="Rectangle-Footer desktop-section">
		<div class="container">		
			<div class="col-sm-6">	
				<p class="copiright"><?php echo $text_technologies; ?></p>
			</div>
			<div class="col-sm-3">
				<p class="downloadApp">	
					<span><?php echo $text_download_app; ?></span>
					<a href="https://play.google.com/store/apps/details?id=com.qareeb.user">
						<img src="catalog/view/theme/careeb/img/android-ico.svg">
					</a>
					<a href="https://itunes.apple.com/sa/app//id1453159366">
						<img src="catalog/view/theme/careeb/img/i-os-ico.svg">	
					</a>
				</p>
			</div>
			<div class="col-sm-3">
				<p class="stayConnected">
					<span><?php echo $text_stay_connected; ?></span>	
					<a href="https://www.facebook.com/QareebKSA"><img src="catalog/view/theme/careeb/img/fb.svg"></a>
					<a href="https://twitter.com/QareebKSA"><img src="catalog/view/theme/careeb/img/twitter.svg"></a>
					<a href="https://www.instagram.com/QareebKSA/"><img src="catalog/view/theme/careeb/img/insta.svg"></a>
					<a href="https://www.snapchat.com/QareebKSA"><img src="catalog/view/theme/careeb/img/snapchat.svg"></a>	
				</p>
			</div>
		</div>
	</div>
		
	<div class="Rectangle-Footer mobile-section">
		<div class="container">			
			<p class="stayConnected"><span><?php echo $text_stay_connected; ?></span></p>
			<p class="stayConnected stayConnectedNew">
				<a href="https://www.facebook.com/QareebKSA">
					<img src="catalog/view/theme/careeb/img/fb.svg">
				</a>
				<a href="https://twitter.com/QareebKSA">
					<img src="catalog/view/theme/careeb/img/twitter.svg">
				</a>
				<a href="https://www.instagram.com/QareebKSA/">	
					<img src="catalog/view/theme/careeb/img/insta.svg">	
				</a>
				<a href="https://www.snapchat.com/QareebKSA">
					<img src="catalog/view/theme/careeb/img/snapchat.svg">
				</a>
			</p>
			<p class="downloadApp">	<span><?php echo $text_download_app; ?></span></p>
			<p class="downloadApp downloadAppNew">
				<a href="https://play.google.com/store/apps/details?id=com.qareeb.user">
					<img src="catalog/view/theme/careeb/img/android-ico.svg">
				</a>
				<a href="https://itunes.apple.com/sa/app//id1453159366">
					<img src="catalog/view/theme/careeb/img/i-os-ico.svg">	
				</a>
			</p>
			<p class="copiright"><span><?php echo $text_technologies; ?></span></p>
		</div>
	</div>
</footer>

	<!--<script type="text/javascript">
		var Tawk_API=Tawk_API||{}, Tawk_LoadStart=new Date();
		(function(){
		var s1=document.createElement("script"),s0=document.getElementsByTagName("script")[0];
		s1.async=true;
		s1.src='https://embed.tawk.to/59264fc8b3d02e11ecc66bf0/default';
		s1.charset='UTF-8';
		s1.setAttribute('crossorigin','*');
		s0.parentNode.insertBefore(s1,s0);
		})();
	</script>-->
	<!--End of Tawk.to Script-->
	
	<!--Start of Tawk.to Script-->
	<script type="text/javascript">
	var Tawk_API=Tawk_API||{}, Tawk_LoadStart=new Date();
	(function(){
	var s1=document.createElement("script"),s0=document.getElementsByTagName("script")[0];
	s1.async=true;
	s1.src='https://embed.tawk.to/5c423dc651410568a1075005/default';
	s1.charset='UTF-8';
	s1.setAttribute('crossorigin','*');
	s0.parentNode.insertBefore(s1,s0);
	})();
	</script>
	<!--End of Tawk.to Script-->

	<div id="modal-wishlist_category" class="modal col-sm-6 custom-margin-3" aria-hidden="true" style="display: none;">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-hidden="true">x</button>
				</div>
				<div class="modal-body modal-body-custom">
					<div class="row">
						<form action="#" method="get" onsubmit="return false" id="wishlist_cat_form"> 
							<div class="col-md-12">
								<div class="form-group">
									<input type="radio" name="wishlist_catergory" checked="checked" value="Main">Main<br>
									<?php
										foreach($wishlist_cat as $key => $value){
											if(!is_null($value['category_name']) && $value['category_name'] != 'Main'){
												echo '<input type="radio" name="wishlist_catergory" value="'.$value['category_name'].'">'. $value['category_name'].'<br>';
											}
										}
									?>
									<input type="hidden" id="wishlist_product_id" name="wishlist_product_id">
									<input type="hidden" id="wishlist_seller_id" name="wishlist_seller_id">
									<input type="radio" name="wishlist_catergory" value="custom" for="wishlist_new_category"><input type="text" id="wishlist_new_category" name="wishlist_new_category" class="form-control" placeholder="New Category Name">
								</div>
							</div>
							<div class="col-md-12">
								<button class="btn btn-primary" id="wishlist_cat_btn">Select</button>
							</div>
						</form>
					</div>
				</div>
			</div>
		</div>
	</div>

	<!-- Modal -->
    <div id="model">   
		<div class="modal fade" id="locationModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" data-backdrop="static" data-keyboard="false">
     
			<div class="modal-dialog" role="document">
				<div class="modal-content">
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
						<h3 class="centered"><?php echo $text_change_location; ?> </h3>
					</div>
					
					<div class="modal-body">						
						<div class="">
							<div class="col-md-6">
								<?php                     
								if(!empty($data['locationData']['cityName'])){
									echo $text_current_city.'<b>'.$data['locationData']['cityName'].'</b>';
								}
								?>
							</div>
							<div class="col-md-6">
								<?php
								if(!empty($data['locationData']['areaName'])){
									echo $text_current_area.'<b>'.$data['locationData']['areaName'].'</b>';
								}
								?>
							</div>
						</div>
						
						<br>
						<hr>
						
						<form method="POST" action="<?php echo $action; ?>" style="text-align: center;margin: 0 auto;">
							<div class='select_field'>
								<select class="hideShow" name="city_id" id="city_id" style="background-image: url(catalog/view/theme/<?php echo $config_template; ?>/img/landingbuttonselect.png);background-repeat: no-repeat;background-position: 94%;background-size: 15px;">
									<option value="0"><?php echo $text_city; ?></option>
									<?php
									foreach ($cities as $city) { 
										/* $selectedCity = ($city['city_id']==$city_saved) ? 'selected' : ''; */
										$selectedCity = ($city['city_id']==$SelectedCityID) ? 'selected' : '';
									?>                             
										<option <?php echo $selectedCity; ?>  value="<?php echo $city['city_id']; ?>"><?php echo $city['name']; ?></option>
									<?php 
									} ?>
								</select>
								<select  style="background-image: url(catalog/view/theme/<?php echo $config_template; ?>/img/landingbuttonselect.png);background-repeat: no-repeat;background-position: 94%;background-size: 15px;" name="area_id" id="area_id">
									<option value="0"><?php echo $text_area; ?></option>
								</select>
							</div>
							<div class="location_btn">
								<input type="submit" id="findMe" value="<?php echo $text_find ?>"/>
								<button type="button" id="locateMe_new" class="locateMe"><img class="img-responsive" src="catalog/view/theme/<?php echo $config_template; ?>/img/locateMe.png"><?php echo $text_locate_me ?></button>
							</div>
							<div class="col-sm-8" id="error_alert"></div>
						</form>
					
						<form id="frmFindMe_new"  method="POST" action="<?php echo $action; ?>">
							<input type="text" name="user_lat" id="user_lat_new" value="" style="display:none" />
							<input type="text" name="user_lng" id="user_lng_new" value="" style="display:none"/>
							<input type="text" name="user_area" id="user_area_new" value="" style="display:none"/>
									 
						</form> 
					</div><!-- modal-body -->
				</div><!-- modal-content -->
			</div><!-- modal-dialog -->
		</div><!-- modal -->
	</div>	
	<script>
		$(window).load(function() {
			$('select[name=\'city_id\']').trigger('change');
		});
	</script>
	
	<script type="text/javascript"><!--
	$('select[name=\'city_id\']').on('change', function() {
	  $.ajax({
		url: 'index.php?route=common/header_home/areas&city_id=' + this.value,
		dataType: 'json',
		beforeSend: function() {
		  //$('select[name=\'city_id\']').after(' <i class="fa fa-circle-o-notch fa-spin"></i>');
		},
		complete: function() {
		//  $('.fa-spin').remove();
		},
		success: function(json) {
		//  $('.fa-spin').remove();
	  
		  
	  
		  html = '<option value="0"><?php echo $text_area; ?></option>';
	  
		  if (json) {
			for (i = 0; i < json['areas'].length; i++) {
			  html += '<option value="' + json['areas'][i]['area_id'] + '"';
	  
			  if (json['areas'][i]['area_id'] == <?php echo $SelectedAreaID; ?>) {
				html += ' selected="selected"';
				}
	  
				html += '>' + json['areas'][i]['name'] + '</option>';
			}
		  } else {
			html += '<option value="0" selected="selected"><?php echo $text_area; ?></option>';
		  }
	  
		  $('select[name=\'area_id\']').html(html);
		},
		error: function(xhr, ajaxOptions, thrownError) {
		  alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
		}
	  });
	});

	$('select[name=\'city_id\']').trigger('change');
	//--></script>  
	<script type="text/javascript">
	$("#locateMe_new").click(function(){

	   getUserCurrentLocation(); 
	});

		function getUserCurrentLocation() {
			navigator.geolocation.getCurrentPosition(success, error, options);
		}
		
		
	var options = {
	  enableHighAccuracy: true,
	  timeout: 5000,
	  maximumAge: 0
	};

	function success(pos) {
		var crd = pos.coords;
		var lat = crd.latitude;
		var lng = crd.longitude;
		

		$("#user_lat_new").val(lat);
		$("#user_lng_new").val(lng);
	 
		var apiKey = "AIzaSyBlpCdiNIP0fJnV1TFhaNwyEXbDNCrqz9E";
		var baseUrl= "https://maps.googleapis.com/maps/api/geocode/json?latlng=";
		var url = encodeURI(baseUrl + lat+","+lng+"&key="+apiKey); //geocoding url
		$.getJSON(url, function(data) { //get Geocoded JSONP data
						if(data !="")
						{
						  
						  var result = data.results[0];
						  var area = "Lahore";
						  
						  for(var i=0, len=result.address_components.length; i<len; i++) {
							
							var ac = result.address_components[i];
							if(ac.types.indexOf("locality") >= 0) city = ac.long_name;
							if(ac.types.indexOf("route") >= 0) area = ac.long_name;
							
						}//for loop
							
						  $("#user_area_new").val(area);    
							
						}// Data not null
							 var apiLink =   'index.php?route=common/store/getStoresByAreaId&user_lat=' + lat + '&user_lng='+ lng+ '&user_area='+ area;  
						  setTimeout(function(){
							//$("#frmFindMe_new" ).submit();
							openStoresModal(apiLink);
							}, 200); 
				}); //json
			
	   
	};

	function error(err) {
	 
		$(".alert-danger").remove();
		$('#error_alert').append('<div class="alert alert-danger alert-custom"><i class="fa fa-exclamation-circle"></i> <?php echo $text_location_disable; ?></div>');
	   
		return false;
	};    



	$('#findMe').click(function () {
			var city = $("#city_id").val();
			var area = $("#area_id").val();

					if (city == 0) {
						$(".alert-danger").remove();
						$('#error_alert').append('<div class="alert alert-danger alert-custom"><i class="fa fa-exclamation-circle"></i> <?php echo $text_city; ?></div>');
						$('#city_id').addClass('has-error');                  
						$('#city_id').focus();
						return false;
					}
					else if (area==0) {
						$(".alert-danger").remove();
						$('#error_alert').append('<div class="alert alert-danger alert-custom" ><i class="fa fa-exclamation-circle"></i>  <?php echo $text_area; ?></div>');
						$('#area_id').addClass('has-error');                  
						$('#area_id').focus();
						return false;
					}
					
					$('#modelCommonHeader').html('');
					$('#modalCommonBody').html('');
				
				   var apiLink =   'index.php?route=common/store/getStoresByAreaId&area_id=' + area+ '&city_id='+ city;
				   openStoresModal(apiLink);
				   return false;
		});
		function closeModel(){	
			$('.modal-backdrop').fadeOut();
		}
		
			function openStoresModal(apiLink){
				
			$("#locationModal").modal('hide');
			
		   
			//Retrieve Stores        
		   $.ajax({
			url: apiLink ,
			dataType: 'json',	
			success: function(json) {
			  var headerHtml = '<button type="button" class="close" onclick="closeModel();"><span aria-hidden="true">&times;</span></button>';
			  
				  if(json['stores'] !=""){
					headerHtml += "<h1 class='centered'>" + json['text_store']+  "  <strong>" + json['area'] + "</strong></h1>";
				  }else{
					 headerHtml += "<h1 class='centered'>" + json['error_service']+  "  <strong>" + json['area'] + "</strong></h1>";
				  }
				  var bodyHtml = '';
				   if(json['stores'] !=""){
						bodyHtml += "<h3 class='retailer-group-label'> " + json['text_store_recommended'] + "</h3>";
						bodyHtml += "<ul class='retailer-options-wrapper unstyled'>";
						for (i = 0; i < json['stores'].length; i++) {
							bodyHtml += "<li class='retailer-option'><div class='col-sm-12 firstPopupDiv'><div class='col-sm-6'>";
							if(json['product_count'] > 0){
							 bodyHtml += "<a class='retailer-option-inner-wrapper absolute-center'  href='"+ json['stores'][i]['action']  +"' data-bypass='true'>";
							}else{
							   bodyHtml += "<a class='retailer-option-inner-wrapper absolute-center'  href='"+ json['stores'][i]['action']  +"' data-bypass='true'>"; 
							}
							
							bodyHtml += "<div class='retailer-option-logo-wrapper'>";
							bodyHtml += "<img class='absolute-center' src='" + json['stores'][i]['image'] + "' alt='" + json['stores'][i]['name'] + "'>";
							bodyHtml += "</div>";
							
							bodyHtml += "</a></div><div class='col-sm-6'>";
							if(json['product_count'] > 0){
							 bodyHtml += "<a class='retailer-option-inner-wrapper absolute-center'  href='"+ json['stores'][i]['action']  +"' data-bypass='true'>";
							}else{
							   bodyHtml += "<a class='retailer-option-inner-wrapper absolute-center'  href='"+ json['stores'][i]['action']  +"' data-bypass='true'>"; 
							}
															
							bodyHtml += "<div class='retailer-option-body'>";
							bodyHtml += "<h3>" + json['stores'][i]['name'] + "</h3>";
							bodyHtml += "</div>";
							bodyHtml += "</a></div></div>";
							
							bodyHtml += "<div class='col-sm-12 secondPopupDiv'><div class='col-sm-6'>";
							if(json['product_count'] > 0){
							 bodyHtml += "<a class='retailer-option-inner-wrapper absolute-center'  href='"+ json['stores'][i]['action']  +"' data-bypass='true'>";
							}else{
							   bodyHtml += "<a class='retailer-option-inner-wrapper absolute-center'  href='"+ json['stores'][i]['action']  +"' data-bypass='true'>"; 
							}
							
							bodyHtml += "<div class='retailer-option-body'>";
							bodyHtml += json['text_minimum'] +" " + json['stores'][i]['minimum_order'] + "";				
							bodyHtml += "</div>";
							
							bodyHtml += "</a></div><div class='col-sm-6'>";
							if(json['product_count'] > 0){
							 bodyHtml += "<a class='retailer-option-inner-wrapper absolute-center'  href='"+ json['stores'][i]['action']  +"' data-bypass='true'>";
							}else{
							   bodyHtml += "<a class='retailer-option-inner-wrapper absolute-center'  href='"+ json['stores'][i]['action']  +"' data-bypass='true'>"; 
							}
							
							bodyHtml += "<div class='retailer-option-body'>";
							bodyHtml += json['text_delivery'] +" " + json['stores'][i]['delivery_charges'] + "";
							bodyHtml += "</div>";
							bodyHtml += "</a></div></div>";
							
							bodyHtml += "</li>";                        
						}
						bodyHtml += "</ul>";
				   }else{
							bodyHtml += "<h4 class='centered'> " + json['error_area'] + "</h4>";
				   }
				  
			  
				$('#modelCommonHeader').html(headerHtml);
				$('#modalCommonBody').html(bodyHtml);
		
			},
			error: function(xhr, ajaxOptions, thrownError) {
				alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
			}
		   }); //Ajax end
	 
				   $("#storeModal").modal('show');
				   
		}
		

		
		
		
		$('#storeModal').click(function(){
			$('.navbar-collapse .navbar-nav a.dropdown-toggle').click();
		});
	</script>
</body>
</html>
