<?php global $config; global $request; ?>
<div id="bt_container"></div>
<footer>
	<div class="container-fluid container">		
		<div class="col-sm-6 desktop-section">
			<div class="footerPart">
				<?php if ($informations) { ?>			
					<div class="footerMenu">
						<h3><?php echo $text_information; ?></h3>
						<?php foreach ($informations as $information) { ?>
							<div><a href="<?php echo $information['href']; ?>"><?php echo $information['title']; ?></a></li></div>
						<?php } ?>
					</div>
				
				<?php } ?>   
				
				<div class="footerMenu">
					<h3><?php echo $text_service; ?></h3>
					<div><a href="<?php echo $contact; ?>"><?php echo $text_contact; ?></a></div>
					<?php /* <div><a href="<?php echo $return; ?>"><?php echo $text_return; ?></a></div>*/ ?>				
				</div>
			</div>

			<div class="footerPart">
				<?php /*
				<div class="footerMenu">
					<h3><?php echo $text_extra; ?></h3>				
					<div><a href="<?php echo $special; ?>"><?php echo $text_special; ?></a></div>		
				</div>	*/ ?>	
			
				<div class="footerMenu">
					<h3><?php echo $text_join_us; ?></h3>				
					<div><a href="javascript:void(0);"><?php echo $text_sell_with_us; ?></a></div>
					<div><a href="javascript:void(0);"><?php echo $text_careers; ?></a></div>
					<div><a href="javascript:void(0);"><?php echo $text_become_a_shopper; ?></a></div>	
				</div>			
			</div>
		
		</div>

		<div class="col-sm-6">
			<?php echo $btfooter; ?>
		</div>
		
		<div class="col-sm-6 mobile-section">
			<div class="footerPart">
				<?php if ($informations) { ?>			
					<div class="footerMenu">
						<h3><?php echo $text_information; ?></h3>
						<?php foreach ($informations as $information) { ?>
							<div><a href="<?php echo $information['href']; ?>"><?php echo $information['title']; ?></a></li></div>
						<?php } ?>
					</div>
				
				<?php } ?>   
				
				<div class="footerMenu">
					<h3><?php echo $text_service; ?></h3>
					<div><a href="<?php echo $contact; ?>"><?php echo $text_contact; ?></a></div>
					<?php /* <div><a href="<?php echo $return; ?>"><?php echo $text_return; ?></a></div>	*/ ?>			
				</div>
			</div>

			<div class="footerPart">
				<?php /*
				<div class="footerMenu">
					<h3><?php echo $text_extra; ?></h3>				
					<div><a href="<?php echo $special; ?>"><?php echo $text_special; ?></a></div>		
				</div>	*/ ?>	
			
				<div class="footerMenu">
					<h3><?php echo $text_join_us; ?></h3>				
					<div><a href="javascript:void(0);"><?php echo $text_sell_with_us; ?></a></div>
					<div><a href="javascript:void(0);"><?php echo $text_careers; ?></a></div>
					<div><a href="javascript:void(0);"><?php echo $text_become_a_shopper; ?></a></div>	
				</div>			
			</div>
		
		</div>
	</div>
	
	<div class="Rectangle-Footer desktop-section">
		<div class="container">		
			<div class="col-sm-6">	
				<p class="copiright"><?php echo $text_technologies; ?></p>
			</div>
			<div class="col-sm-3">
				<p class="downloadApp">	
					<span><?php echo $text_download_app; ?></span>
					<a href="https://play.google.com/store/apps/details?id=com.qareeb.user">
						<img src="catalog/view/theme/careeb/img/android-ico.svg">
					</a>
					<a href="https://itunes.apple.com/sa/app//id1453159366">
						<img src="catalog/view/theme/careeb/img/i-os-ico.svg">	
					</a>
				</p>
			</div>
			<div class="col-sm-3">
				<p class="stayConnected">
					<span><?php echo $text_stay_connected; ?></span>	
					<a href="https://www.facebook.com/QareebKSA"><img src="catalog/view/theme/careeb/img/fb.svg"></a>
					<a href="https://twitter.com/QareebKSA"><img src="catalog/view/theme/careeb/img/twitter.svg"></a>
					<a href="https://www.instagram.com/QareebKSA/"><img src="catalog/view/theme/careeb/img/insta.svg"></a>
					<a href="https://www.snapchat.com/QareebKSA"><img src="catalog/view/theme/careeb/img/snapchat.svg"></a>	
				</p>
			</div>
		</div>
	</div>
		
	<div class="Rectangle-Footer mobile-section">
		<div class="container">			
			<p class="stayConnected"><span><?php echo $text_stay_connected; ?></span></p>
			<p class="stayConnected stayConnectedNew">
				<a href="https://www.facebook.com/QareebKSA">
					<img src="catalog/view/theme/careeb/img/fb.svg">
				</a>
				<a href="https://twitter.com/QareebKSA">
					<img src="catalog/view/theme/careeb/img/twitter.svg">
				</a>
				<a href="https://www.instagram.com/QareebKSA/">	
					<img src="catalog/view/theme/careeb/img/insta.svg">	
				</a>
				<a href="https://www.snapchat.com/QareebKSA">
					<img src="catalog/view/theme/careeb/img/snapchat.svg">
				</a>
			</p>
			<p class="downloadApp">	<span><?php echo $text_download_app; ?></span></p>
			<p class="downloadApp downloadAppNew">
				<a href="https://play.google.com/store/apps/details?id=com.qareeb.user">
					<img src="catalog/view/theme/careeb/img/android-ico.svg">
				</a>
				<a href="https://itunes.apple.com/sa/app//id1453159366">
					<img src="catalog/view/theme/careeb/img/i-os-ico.svg">	
				</a>
			</p>
			<p class="copiright"><span><?php echo $text_technologies; ?></span></p>
		</div>
	</div>
</footer>
<img style="display:none;" style="width: 100%;margin-top: -20px;z-index: -26;position: relative;" src="catalog/view/theme/<?php echo $config->get('config_template'); ?>/img/landingfooter.jpg"/>
<script type="text/javascript">
var Tawk_API=Tawk_API||{}, Tawk_LoadStart=new Date();
(function(){
var s1=document.createElement("script"),s0=document.getElementsByTagName("script")[0];
s1.async=true;
s1.src='https://embed.tawk.to/5c423dc651410568a1075005/default';
s1.charset='UTF-8';
s1.setAttribute('crossorigin','*');
s0.parentNode.insertBefore(s1,s0);
})();
</script>
<!--End of Tawk.to Script-->
 </body> 
</html>