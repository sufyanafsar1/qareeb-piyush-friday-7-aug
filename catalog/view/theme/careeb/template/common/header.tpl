<?php
$user_agent = getenv("HTTP_USER_AGENT");
if(strpos($user_agent, "Win") !== FALSE){
	$os = "Windows";
}elseif(strpos($user_agent, "Mac") !== FALSE){
	$os = "Mac";
}else{
	$os = "other";
} 
?>
<?php global $config; global $request; ?>
<?php
	$boss_manager = array(
		'option' => array(
			'bt_scroll_top' => true,
			'animation' 	=> true,			
		),
		'layout' => array(
			'mode_css'   => 'wide',
			'box_width' 	=> 1200,
			'h_mode_css'   => 'inherit',
			'h_box_width' 	=> 1200,
			'f_mode_css'   => 'inherit',
			'f_box_width' 	=> 1200
		),
		'status' => 1
	);
?>
<?php 
	if($config->get('boss_manager')){
		$boss_manager = $config->get('boss_manager'); 
	}else{
		$boss_manager = $boss_manager;
	} 
	$header_link = isset($boss_manager['header_link'])?$boss_manager['header_link']:''; 
	$option = isset($boss_manager['option'])?$boss_manager['option']:''; 
	$loading = isset($boss_manager['option']['loading'])?$boss_manager['option']['loading']:''; 
?>
<!DOCTYPE html>
<!--[if IE]><![endif]-->
<!--[if IE 8 ]><html dir="<?php echo $direction; ?>" lang="<?php echo $lang; ?>" class="ie8"><![endif]-->
<!--[if IE 9 ]><html dir="<?php echo $direction; ?>" lang="<?php echo $lang; ?>" class="ie9"><![endif]-->
<!--[if (gt IE 9)|!(IE)]><!-->
<html dir="<?php echo $direction; ?>" lang="<?php echo $lang; ?>">
<!--<![endif]-->
<head>
	<meta charset="UTF-8" />
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title><?php echo $title; ?></title>
	<base href="<?php echo $base; ?>" />
	<?php if ($description) { ?>
		<meta name="description" content="<?php echo $description; ?>" />
	<?php } ?>
	<?php if ($keywords) { ?>
		<meta name="keywords" content= "<?php echo $keywords; ?>" />
	<?php } ?>
	
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<link href="image/catalog/logo/Favicon-1.png" rel="icon">
	<link rel="stylesheet" href="catalog/view/theme/<?php echo $config->get('config_template'); ?>/css/qareeb-inner.css">
		
	<link href='https://fonts.googleapis.com/css?family=Montserrat:200,300,400,600,700,800,900' rel='stylesheet' type='text/css'>
		
		<!-- Facebook Pixel Code -->
	<script>
		!function(f,b,e,v,n,t,s)
		{if(f.fbq)return;n=f.fbq=function(){n.callMethod?
		n.callMethod.apply(n,arguments):n.queue.push(arguments)};
		if(!f._fbq)f._fbq=n;n.push=n;n.loaded=!0;n.version='2.0';
		n.queue=[];t=b.createElement(e);t.async=!0;
		t.src=v;s=b.getElementsByTagName(e)[0];
		s.parentNode.insertBefore(t,s)}(window, document,'script',
		'https://connect.facebook.net/en_US/fbevents.js');
		fbq('init', '511229229464737');
		fbq('track', 'PageView');
	  </script>
	  <noscript><img height="1" width="1" style="display:none"
		src="https://www.facebook.com/tr?id=511229229464737&ev=PageView&noscript=1"
	  /></noscript>
	  <!-- End Facebook Pixel Code -->

	<script src="catalog/view/theme/<?php echo $config->get('config_template'); ?>/js/qareeb-inner.js"></script>
		
	<!-- dynamic -->
	<?php foreach ($styles as $style) { ?>
		<link href="<?php echo $style['href']; ?>" type="text/css" rel="<?php echo $style['rel']; ?>" media="<?php echo $style['media']; ?>" />
	<?php } ?>
	
	<?php foreach ($scripts as $script) { ?>
		<script src="<?php echo $script; ?>" type="text/javascript"></script>
	<?php } ?>
	
	<?php if($direction=='rtl'){ ?>
		<link rel="stylesheet" type="text/css" href="catalog/view/theme/<?php echo $config->get('config_template'); ?>/css/stylertl.css" />
	<?php } ?>
	<!-- dynamic -->

			
	<?php if (isset($analytics) && !empty($analytics)) { ?>
		<?php foreach ($analytics as $analytic) { ?>
			<?php echo $analytic; ?>
	<?php } } ?>
	
	<?php if(isset($this->request->get['route'])){$route1 = $this->request->get['route'];}else{$route1 ="";}
		
	if(isset($route1) && ($route1== "common/home" || $route1=="") && $loading){ ?>
		<script type="text/javascript"><!--
			$(document).ready(function() {
				$(".bt-loading").fadeOut("1500", function () {
					$('#bt_loading').css("display", "none");
				});
			});
		//--></script>
	<?php 
	}else{ ?>
		<script type="text/javascript"><!--
			$(document).ready(function() {
			$('#bt_loading').css("display", "none");
			});
		//--></script>
	<?php
	} ?>

	<script src="https://cdn.onesignal.com/sdks/OneSignalSDK.js" async></script>
	<script>
		var OneSignal = window.OneSignal || [];
		OneSignal.push(["init", {
			appId: "c7e4b4b0-091a-42bd-a879-02b103bc2178", //83a7f3c9-82d4-446c-a844-bdf1d1f1d86a
			autoRegister: false,
			persistNotification: false,
			welcomeNotification: {
				"title": "Qareeb Online Grocery Store",
				"message": "Thanks for subscribing !",
				// "url": "" /* Leave commented for the notification to not open a window on Chrome and Firefox (on Safari, it opens to your webpage) */
			},

			notifyButton: {
				enable: true, /* Required to use the notify button */
				size: 'medium', /* One of 'small', 'medium', or 'large' */
				showCredit: false, /* Hide the OneSignal logo */
				position: 'bottom-left', /* Either 'bottom-left' or 'bottom-right' */
				colors: { // Customize the colors of the main button and dialog popup button

					'dialog.button.background': 'rgb(22,107,24)',
					'circle.background': 'rgb(22,107,24)',
					'circle.foreground': 'white'
				}


			}

		}]);
	</script>
	<script src="/service_worker-inner.js" defer></script>
</head>

<?php 
	if(isset($request->get['route'])){
		$route = $request->get['route'];
	}else{
		$route ="";
	}
	if(isset($route) && ($route== "common/home" || $route=="")){
		$home_page='bt-home-page';
	}else{
		$home_page="bt-other-page";
	}
?>
<body class="<?php echo $home_page; ?> <?php if($direction=='rtl') echo 'rtl';?> landingBody<?php echo $lang; ?>">
<?php if($loading){ ?>
	<div id="bt_loading"><div class="bt-loading">
		<div id="fountainG">
			<div id="fountainG_1" class="fountainG"></div>
			<div id="fountainG_2" class="fountainG"></div>
			<div id="fountainG_3" class="fountainG"></div>
			<div id="fountainG_4" class="fountainG"></div>
			<div id="fountainG_5" class="fountainG"></div>
			<div id="fountainG_6" class="fountainG"></div>
			<div id="fountainG_7" class="fountainG"></div>
			<div id="fountainG_8" class="fountainG"></div>
		</div>
	</div></div>
<?php } ?>

<?php 
if($mobileCheck == 1){ ?>
	<!-- Start navbar -->
	<nav class="navbar navbar-default">
		<div class="container container-fluid">
			<!-- Brand and toggle get grouped for better mobile display -->
			<div class="navbar-header">					
				<button type="button" class="navbar-toggle menuIcon" aria-expanded="false" onclick="openNav()">
					<img src="catalog/view/theme/careeb/img/list.svg" />
				</button>			
				<?php echo $cart; ?>				
				<button type="button" class="navbar-toggle collapsed searchIcon" data-toggle="collapse" data-target="#bs-example-navbar-collapse-2" aria-expanded="false">
					<img src="catalog/view/theme/careeb/img/search.svg" />
				</button>
				
				<?php if ($logo) { ?>
					<a class="navbar-brand desktop-section" href="<?php echo $home; ?>"><img src="image/catalog/logo/QareebLogo.svg" title="<?php echo $name; ?>" class="img-responsive" alt="<?php echo $name; ?>" /></a>	
					<a class="navbar-brand mobile-section" href="<?php echo $home; ?>"><img src="image/catalog/logo/QareebLogoMobile.svg" title="<?php echo $name; ?>" class="img-responsive" alt="<?php echo $name; ?>" /></a>				
				<?php } else { ?>
					<h1><a href="<?php echo $home; ?>"><?php echo $name; ?></a></h1>
				<?php } ?>	
				
			</div>	
			<div class="collapse" id="bs-example-navbar-collapse-3">			
				<?php echo $cart; ?>			
			</div>
			
			<div class="collapse" id="bs-example-navbar-collapse-2">		
				<div class="search-area-logicsfort-wrapper">
					<div class="container container-fluid search-area-logicsfort">
						<?php
						if (isset($search)) {
							echo $search;
						} else {
							echo '<form style="width: 366px;height: 75px;"></form>';
						}
						?>
					</div>
				</div>		
			</div>

			<div id="mySidenav-Mobile" class="sidenav-Mobile">
				<?php echo $language; ?>
				<a href="javascript:void(0)" class="closebtn" onclick="closeNav()">&times;</a>
							
				<a href="#" class="dropdown-toggle storeLocation" data-toggle="modal" data-target="#storeModal" ><img src="catalog/view/theme/careeb/image/store-icon.svg"/>
					<?php 
					if(!empty($seller_info['name'])){
						echo $seller_info['name']; 
					} else { 
						echo $text_store; 
					}
					?> 
					<span class="caret"></span>
				</a>
				
				<a href="#" class="dropdown-toggle2" data-toggle="modal" data-target="#locationModal">
					<img src="catalog/view/theme/careeb/image/location-icon.svg"/>
					<?php 
					if($data['areaName'] != '' || $data['cityName'] != '') {
						if($data['areaName']){
							echo $data['areaName'];
						}
					} else {
						echo $text_change_location; 
					}
					?>
				</a>
				
				<?php 
				if ($logged) { ?>				
					<a href="<?php echo $account; ?>"><img src="catalog/view/theme/careeb/image/account-menu.svg"/> <?php echo $text_account_deatil; ?></a>
					
					<a href="<?php echo $logout; ?>"><img src="catalog/view/theme/careeb/image/logout-ico.svg"/> <?php echo $text_logout; ?></a>					
				<?php 
				} else { ?>						
					<a href="javascript:void(0);" id="signIn" title="<?php echo $text_register .'/'.$text_login; ?>"><img src="catalog/view/theme/<?php echo $config->get('config_template'); ?>/img/account-mobile.svg"> <?php echo $text_account; ?></a>
				<?php 
				} ?>			  
			</div>
			<script>
				function openNav() {
				  document.getElementById("mySidenav-Mobile").style.width = "250px";
				}

				function closeNav() {
				  document.getElementById("mySidenav-Mobile").style.width = "0";
				 // $('.btn-group .btn-group-vertical').css({"display":"none"});
				}
			</script>			
		</div><!-- /.container-fluid -->
	</nav>
	<!-- End navbar -->
<?php } else { ?>
	<!-- Start navbar -->
	<nav class="navbar navbar-default">
		<div class="container container-fluid">
			<!-- Brand and toggle get grouped for better mobile display -->
			<div class="navbar-header">	 
				<button type="button" class="navbar-toggle collapsed menuIcon" data-toggle="collapse" data-target=	"#bs-example-navbar-collapse-1" aria-expanded="false">
					<img src="catalog/view/theme/careeb/img/list.svg" />
				</button>
				<button type="button" class="navbar-toggle collapsed cartIcon" data-toggle="collapse" data-target="#bs-example-navbar-collapse-3" aria-expanded="false">
					<img src="catalog/view/theme/careeb/img/cart.svg" />
				</button>
				<button type="button" class="navbar-toggle collapsed searchIcon" data-toggle="collapse" data-target="#bs-example-navbar-collapse-2" aria-expanded="false">
					<img src="catalog/view/theme/careeb/img/search.svg" />
				</button>
				<?php if ($logo) { ?>
					<a class="navbar-brand desktop-section" href="<?php echo $home; ?>"><img src="image/catalog/logo/QareebLogo.svg" title="<?php echo $name; ?>" class="img-responsive" alt="<?php echo $name; ?>" /></a>	
					<a class="navbar-brand mobile-section" href="<?php echo $home; ?>"><img src="image/catalog/logo/QareebLogoMobile.svg" title="<?php echo $name; ?>" class="img-responsive" alt="<?php echo $name; ?>" /></a>				
				<?php } else { ?>
					<h1><a href="<?php echo $home; ?>"><?php echo $name; ?></a></h1>
				<?php } ?>	
			</div>	
			<div class="collapse" id="bs-example-navbar-collapse-3">			
				<?php //echo $cart; ?>			
			</div>
			
			<?php 
			/* <div class="collapse" id="bs-example-navbar-collapse-2">		
				<div class="search-area-logicsfort-wrapper">
					<div class="container container-fluid search-area-logicsfort">
						<?php
						if (isset($search)) {
							echo $search;
						} else {
							echo '<form style="width: 366px;height: 75px;"></form>';
						}
						?>
					</div>
				</div>		
			</div> */ ?>

			<!-- Collect the nav links, forms, and other content for toggling --> 
			<div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">   
				<ul class="nav navbar-nav nav-store">
					<li>  
						<a href="#" class="dropdown-toggle" data-toggle="modal" data-target="#storeModal" ><img src="catalog/view/theme/careeb/image/store.png"/>
							<?php 
							if(!empty($seller_info['name'])){
								echo $seller_info['name']; 
							} else { 
								echo $text_store; 
							}
							?> <span class="caret"></span>
						</a>
					</li>
					<li>  
						<a href="#" class="dropdown-toggle2" data-toggle="modal" data-target="#locationModal">
							<img src="catalog/view/theme/careeb/image/location.png"/>
							<?php 
							if($data['areaName'] != '' || $data['cityName'] != '') {
								if($data['areaName']){
									echo $data['areaName'];
								}
							} else {
								echo $text_change_location; 
							}
							?>
					   </a>
					</li>
				</ul>
				
				<ul class="nav navbar-nav pull-right">
					<?php if(isset($header_link['language']) && $header_link['language']){ ?>
						<li><?php echo $language; ?></li>
					<?php } ?>
		  
					<?php if(isset($header_link['cart_mini']) && $header_link['cart_mini']){ ?>
						<li class="cartList desktop-section"><?php echo $cart; ?></li>
					<?php } ?>
					
					<li id="customer-dropdown">
						<?php 
						if ($logged) { ?>
							<a href="" class="dropdown-toggle" data-toggle="dropdown"><img src="catalog/view/theme/<?php echo $config->get('config_template'); ?>/img/account.svg"></a>
							<ul class="customer-loggedin dropdown-menu dropdown-menu-right">
								<li><a href="javascript:void(0);"><?php echo $customer_name ?></a></li>
								<li><a href="<?php echo $account; ?>"><i class="fa fa-user"></i> <?php echo $text_account_deatil; ?></a></li>
										<li><a href="<?php echo $wishlist; ?>"><i class="fa fa-heart"></i> <?php echo $text_wishlist; ?></a></li>     
										<li><a href="<?php echo $transaction; ?>"><i class="fa fa-money"></i> <?php echo $text_store_credit; ?></a></li>
										<li><a href="<?php echo $order; ?>"> <i class="fa fa-shopping-cart"></i> <?php echo $text_order; ?></a></li>
								  <hr class="custom-hr"/>
								<li><a href="<?php echo $logout; ?>"><i class="fa fa-sign-out" ></i> <?php echo $text_logout; ?></a></li>     
							</ul>
						<?php 
						} else { ?>						
							<a href="javascript:void(0);" id="signIn" title="<?php echo $text_register .'/'.$text_login; ?>"><img src="catalog/view/theme/<?php echo $config->get('config_template'); ?>/img/account.svg"></a>
						<?php 
						} ?>
					</li>
				</ul>
			</div><!-- /.collapse -->
		</div><!-- /.container-fluid -->
	</nav>
	<!-- End navbar -->
<?php } ?>
<?php echo $store; ?>

<?php 
if($displayTop == 1) { ?>
	<div class='VigatableBg'> </div>

	<?php
	if ( $seller_info ) {
		date_default_timezone_get();
		$nowTime = date("g:i a"); 
		$now = strtotime("now");
		$interval = $seller_info["delivery_timegap"] * 60;
		$package = $seller_info["package_ready"] * 60;
		$nowdata = dayData("now");

		if ( $seller_info[strtolower(substr($nowdata["half"], 0, 3)) . "_start_time"] != "00:00:00" && $seller_info[strtolower(substr($nowdata["half"], 0, 3)) . "_end_time"] != "00:00:00" ) {
			$nowstarttime = strtotime($nowdata["date"]. " " . $seller_info[strtolower(substr($nowdata["half"], 0, 3)) . "_start_time"]) + $interval;
			$nowendtime = strtotime($nowdata["date"]. " " . $seller_info[strtolower(substr($nowdata["half"], 0, 3)) . "_end_time"]);
			$nowoutput = ""; $nownextdelivery = ""; $nowdelivery = ""; $j = 0;
			for( $i = $nowstarttime; $i < $nowendtime; $i += $interval) {
				$nntt = $i + $interval;
				if( $i < $now ) {
					$nowoutput.= "<li class=unavailable>unavailable</li>";
				} else {
					if ( $j == 0 ) {
						$nextTime = $now + $package;
						if ($i < $nextTime) {
							$nowoutput.= "<li class=unavailable> echo $text_unavailable</li>";
						} else {
							$nownextdelivery = arabicTime(strtotime(date("h:i a", $i)),$languageId);
							$nowdelivery = arabicTime(strtotime(date("h:i a", $i)),$languageId)." - ".arabicTime(strtotime(date("h:i a", $nntt)),$languageId);
							$nowoutput.= "<li>".arabicTime(strtotime(date("h:i a", $i)),$languageId)." to ".arabicTime(strtotime(date("h:i a", $nntt)),$languageId)."</li>";
							$j++;
						}
					} else {
						$nowoutput.= "<li>".arabicTime(strtotime(date("h:i a", $i)),$languageId)." to ".arabicTime(strtotime(date("h:i a", $nntt)),$languageId)."</li>"; $j++;
					}
				}
			}
		} else {
			$nownextdelivery = $nowoutput = $text_close;
		}

		$fdata = dayData(" +1 day");

		if ( $seller_info[strtolower(substr($fdata["half"], 0, 3)) . "_start_time"] != "00:00:00" && $seller_info[strtolower(substr($fdata["half"], 0, 3)) . "_end_time"] != "00:00:00" ) {
			$fstarttime = strtotime($fdata["date"]. " " . $seller_info[strtolower(substr($fdata["half"], 0, 3)) . "_start_time"]) + $interval;
			$fendtime = strtotime($fdata["date"]. " " . $seller_info[strtolower(substr($fdata["half"], 0, 3)) . "_end_time"]);
			$foutput = ""; $fnextdelivery = ""; $fdelivery = ""; $k = 0;
			for( $i = $fstarttime; $i < $fendtime; $i += $interval) {
				$nntt = $i + $interval;
				if( $i < $now ) {
					$foutput.= "<li class=unavailable>echo $text_unavailable</li>";
				} else {
					if ( $k == 0 ) {
						$nextTime = $now + $package;
						if ($i < $nextTime) {
							$foutput.= "<li class=unavailable>echo $text_unavailable</li>";
						} else {
							$fnextdelivery = arabicTime(strtotime(date("h:i a", $i)),$languageId);
							$fdelivery = arabicTime(strtotime(date("h:i a", $i)),$languageId)." - ".arabicTime(strtotime(date("h:i a", $nntt)),$languageId);
							$foutput.= "<li>".arabicTime(strtotime(date("h:i a", $i)),$languageId)." to ".arabicTime(strtotime(date("h:i a", $nntt)),$languageId)."</li>"; $k++;
						}
					} else {
						$foutput.= "<li>".arabicTime(strtotime(date("h:i a", $i)),$languageId)." to ".arabicTime(strtotime(date("h:i a", $nntt)),$languageId)."</li>"; $k++;
					}
					//if ( $k == 0 ) { $fnextdelivery = arabicTime(strtotime(date("h:i a", $i)),$languageId); $fdelivery = arabicTime(strtotime(date("h:i a", $i)),$languageId)." - ".arabicTime(strtotime(date("h:i a", $nntt)),$languageId); }
					//$foutput.= "<li>".arabicTime(strtotime(date("h:i a", $i)),$languageId)." to ".arabicTime(strtotime(date("h:i a", $nntt)),$languageId)."</li>"; $k++;
				}
			}
		} else {
			$fnextdelivery = $foutput = $text_close;
		}

		$sdata = dayData(" +2 day");
		if ( $seller_info[strtolower(substr($sdata["half"], 0, 3)) . "_start_time"] != "00:00:00" && $seller_info[strtolower(substr($sdata["half"], 0, 3)) . "_end_time"] != "00:00:00" ) {
			$sstarttime = strtotime($sdata["date"]. " " . $seller_info[strtolower(substr($sdata["half"], 0, 3)) . "_start_time"]) + $interval;
			$sendtime = strtotime($sdata["date"]. " " . $seller_info[strtolower(substr($sdata["half"], 0, 3)) . "_end_time"]);
			$soutput = ""; $snextdelivery = ""; $sdelivery = ""; $l = 0;
			for( $i = $sstarttime; $i < $sendtime; $i += $interval) {
				$nntt = $i + $interval;
				if( $i < $now ) {
					$soutput.= "<li class=unavailable>echo $text_unavailable</li>";
				} else {
					if ( $l == 0 ) {
						$nextTime = $now + $package;
						if ($i < $nextTime) {
							$soutput.= "<li class=unavailable>echo $text_unavailable</li>";
						} else {
							$snextdelivery = arabicTime(strtotime(date("h:i a", $i)),$languageId);
							$name = strtolower($sdata["full"]);
							$sdelivery = $days[$name] . " " . arabicTime(strtotime(date("h:i a", $i)),$languageId)." - ".arabicTime(strtotime(date("h:i a", $nntt)),$languageId);
							$soutput.= "<li>".arabicTime(strtotime(date("h:i a", $i)),$languageId)." to ".arabicTime(strtotime(date("h:i a", $nntt)),$languageId)."</li>"; $l++;
						}
					} else {
						$soutput.= "<li>".arabicTime(strtotime(date("h:i a", $i)),$languageId)." to ".arabicTime(strtotime(date("h:i a", $nntt)),$languageId)."</li>"; $l++;
					}
					//if ( $l == 0 ) { $snextdelivery = date("h:i a", $i); }
					//$soutput.= "<li>".date("h:i a", $i)." to ".date("h:i a", $nntt)."</li>"; $l++;
				}
			}
		} else {
			$soutput = $snextdelivery =  $text_close;
		}

		$tdata = dayData(" +3 day");
		if ( $seller_info[strtolower(substr($tdata["half"], 0, 3)) . "_start_time"] != "00:00:00" && $seller_info[strtolower(substr($tdata["half"], 0, 3)) . "_end_time"] != "00:00:00" ) {
			$tstarttime = strtotime($tdata["date"]. " " . $seller_info[strtolower(substr($tdata["half"], 0, 3)) . "_start_time"]) + $interval;
			$tendtime = strtotime($tdata["date"]. " " . $seller_info[strtolower(substr($tdata["half"], 0, 3)) . "_end_time"]);
			$toutput = ""; $tnextdelivery = ""; $tdelivery = ""; $m = 0;
			for( $i = $tstarttime; $i < $tendtime; $i += $interval) {
				$nntt = $i + $interval;
				if( $i < $now ) {
					$toutput.= "<li class=unavailable>echo $text_unavailable</li>";
				} else {
					if ( $m == 0 ) {
						$nextTime = $now + $package;
						if ($i < $nextTime) {
							$toutput.= "<li class=unavailable>echo $text_unavailable</li>";
						} else {
							$tnextdelivery = arabicTime(strtotime(date("h:i a", $i)),$languageId);
							$name = strtolower($tdata["full"]);
							$tdelivery = $days[$name] . " " . arabicTime(strtotime(date("h:i a", $i)),$languageId)." - ".arabicTime(strtotime(date("h:i a", $nntt)),$languageId);
							$toutput.= "<li>".arabicTime(strtotime(date("h:i a", $i)),$languageId)." to ".arabicTime(strtotime(date("h:i a", $nntt)),$languageId)."</li>"; $m++;
						}
					} else {
						$toutput.= "<li>".arabicTime(strtotime(date("h:i a", $i)),$languageId)." to ".arabicTime(strtotime(date("h:i a", $nntt)),$languageId)."</li>"; $m++;
					}
					//if ( $m == 0 ) { $tnextdelivery = arabicTime(strtotime(date("h:i a", $i)),$languageId); }
					//$toutput.= "<li>".arabicTime(strtotime(date("h:i a", $i)),$languageId)." to ".arabicTime(strtotime(date("h:i a", $nntt)),$languageId)."</li>"; $m++;
				}
			}
		} else {
			$toutput = $tnextdelivery = $text_close;
		}

		$ffdata = dayData(" +4 day");
		if ( $seller_info[strtolower(substr($ffdata["half"], 0, 3)) . "_start_time"] != "00:00:00" && $seller_info[strtolower(substr($ffdata["half"], 0, 3)) . "_end_time"] != "00:00:00" ) {
			$ffstarttime = strtotime($ffdata["date"]. " " . $seller_info[strtolower(substr($ffdata["half"], 0, 3)) . "_start_time"]) + $interval;
			$ffendtime = strtotime($ffdata["date"]. " " . $seller_info[strtolower(substr($ffdata["half"], 0, 3)) . "_end_time"]);
			$ffoutput = ""; $ffnextdelivery = ""; $ffdelivery = ""; $n = 0;
			for( $i = $ffstarttime; $i < $ffendtime; $i += $interval) {
				$nntt = $i + $interval;
				if( $i < $now ) {
					$foutput.= "<li class=unavailable>echo $text_unavailable</li>";
				} else {
					if ( $n == 0 ) {
						$nextTime = $now + $package;
						if ($i < $nextTime) {
							$ffoutput.= "<li class=unavailable>echo $text_unavailable</li>";
						} else {
							$ffnextdelivery = arabicTime(strtotime(date("h:i a", $i)),$languageId);
							$name = strtolower($ffdata["full"]);
							$ffdelivery = $days[$name] . " " . arabicTime(strtotime(date("h:i a", $i)),$languageId)." - ".arabicTime(strtotime(date("h:i a", $nntt)),$languageId);
							$ffoutput.= "<li>".arabicTime(strtotime(date("h:i a", $i)),$languageId)." to ".arabicTime(strtotime(date("h:i a", $nntt)),$languageId)."</li>"; $n++;
						}
					} else {
						$ffoutput.= "<li>".arabicTime(strtotime(date("h:i a", $i)),$languageId)." to ".arabicTime(strtotime(date("h:i a", $nntt)),$languageId)."</li>"; $n++;
					}
					//if ( $n == 0 ) { $ffnextdelivery = date("h:i a", $i); }
					//$ffoutput.= "<li>".date("h:i a", $i)." to ".date("h:i a", $nntt)."</li>"; $n++;
				}
			}
		} else {
			$ffoutput = $ffnextdelivery = $text_close;
		}

		$fffdata = dayData(" +5 day");
		if ( $seller_info[strtolower(substr($fffdata["half"], 0, 3)) . "_start_time"] != "00:00:00" && $seller_info[strtolower(substr($fffdata["half"], 0, 3)) . "_end_time"] != "00:00:00" ) {
			$fffstarttime = strtotime($fffdata["date"]. " " . $seller_info[strtolower(substr($fffdata["half"], 0, 3)) . "_start_time"]) + $interval;
			$fffendtime = strtotime($fffdata["date"]. " " . $seller_info[strtolower(substr($fffdata["half"], 0, 3)) . "_end_time"]);
			$fffoutput = ""; $fffnextdelivery = ""; $fffdelivery = ""; $o = 0;
			for( $i = $fffstarttime; $i < $fffendtime; $i += $interval) {
				$nntt = $i + $interval;
				if( $i < $now ) {
					$fffoutput.= "<li class=unavailable>echo $text_unavailable</li>";
				} else {
					if ( $o == 0 ) {
						$nextTime = $now + $package;
						if ($i < $nextTime) {
							$fffoutput.= "<li class=unavailable>echo $text_unavailable</li>";
						} else {
							$fffnextdelivery = arabicTime(strtotime(date("h:i a", $i)),$languageId);

							$name = strtolower($fffdata["full"]);

							$fffdelivery = $days[$name] . " " . arabicTime(strtotime(date("h:i a", $i)),$languageId)." - ".arabicTime(strtotime(date("h:i a", $nntt)),$languageId);
							$fffoutput.= "<li>".arabicTime(strtotime(date("h:i a", $i)),$languageId)." to ".arabicTime(strtotime(date("h:i a", $nntt)),$languageId)."</li>"; $o++;
						}
					} else {
						$fffoutput.= "<li>".arabicTime(strtotime(date("h:i a", $i)),$languageId)." to ".arabicTime(strtotime(date("h:i a", $nntt)),$languageId)."</li>"; $o++;
					}
					//if ( $o == 0 ) { $fffnextdelivery = arabicTime(strtotime(date("h:i a", $i)),$languageId); }
					//$fffoutput.= "<li>".arabicTime(strtotime(date("h:i a", $i)),$languageId)." to ".arabicTime(strtotime(date("h:i a", $nntt)),$languageId)."</li>"; $o++;
				}
			}
		} else {
			$fffoutput = $fffnextdelivery = $text_close;
		}

		$ssdata = dayData(" +6 day");
		if ( $seller_info[strtolower(substr($ssdata["half"], 0, 3)) . "_start_time"] != "00:00:00" && $seller_info[strtolower(substr($ssdata["half"], 0, 3)) . "_end_time"] != "00:00:00" ) {
			$ssstarttime = strtotime($ssdata["date"]. " " . $seller_info[strtolower(substr($ssdata["half"], 0, 3)) . "_start_time"]) + $interval;
			$ssendtime = strtotime($ssdata["date"]. " " . $seller_info[strtolower(substr($ssdata["half"], 0, 3)) . "_end_time"]);
			$ssoutput = ""; $ssnextdelivery = ""; $ssdelivery = ""; $p = 0;
			for( $i = $ssstarttime; $i < $ssendtime; $i += $interval) {
				$nntt = $i + $interval;
				if( $i < $now ) {
					$ssoutput.= "<li class=unavailable>echo $text_unavailable</li>";
				} else {
					if ( $p == 0 ) {
						$nextTime = $now + $package;
						if ($i < $nextTime) {
							$ssoutput.= "<li class=unavailable>echo $text_unavailable</li>";
						} else {
							$ssnextdelivery = arabicTime(strtotime(date("h:i a", $i)),$languageId);

							$name = strtolower($ssdata["full"]);
							$ssdelivery = $days[$name] . " " . arabicTime(strtotime(date("h:i a", $i)),$languageId)." - ".arabicTime(strtotime(date("h:i a", $nntt)),$languageId);
							$ssoutput.= "<li>".arabicTime(strtotime(date("h:i a", $i)),$languageId)." to ".arabicTime(strtotime(date("h:i a", $nntt)),$languageId)."</li>"; $p++;
						}
					} else {
					  $ssoutput.= "<li>".arabicTime(strtotime(date("h:i a", $i)),$languageId)." to ".arabicTime(strtotime(date("h:i a", $nntt)),$languageId)."</li>"; $p++;
					}
					//if ( $p == 0 ) { $ssnextdelivery = date("h:i a", $i); }
					//$ssoutput.= "<li>".date("h:i a", $i)." to ".date("h:i a", $nntt)."</li>"; $p++;
				}
			}
		} else {
			$ssoutput = $ssnextdelivery = $text_close;
		}
	?>
		<div class="store-header-wrapper">
		  
			<?php
			if (!empty($seller_info['banner'])) {
			?>
				<style> .VigatableBg{background-image: url('./image/<?php echo $seller_info['banner']; ?>'); } </style>
			<?php 
			} ?>

			<div class="search-area-logicsfort-wrapper">
				<div class="container container-fluid search-area-logicsfort">
					<?php
					if (isset($search)) {
						echo $search;
					} else {
						echo '<form style="width: 366px;height: 75px;"></form>';
					}
					?>
				</div>
			</div>
		</div>
		<div class="slot-time-wrapper">
			<div class="container container-fluid">
				<div class="col-md-4 text-left logo-wrapper ">
					<div class="store-logo-wrapper ">
						<a href="index.php?route=store/store&seller_id=<?= $seller_info['seller_id']; ?>">
							<img src="./image/<?php echo $seller_info['image']; ?>" class="img-circle img-thumbnail img-rounded" />
						</a>
					</div>
				</div>
				<div class="col-sm-12">            
					<?php
					$storeName = $seller_info['name'];
					if (!empty($storeName)) {
					?>
						
							<h2 class="storeTitle">                       
								<a href="index.php?route=store/store&seller_id=<?= $seller_info['seller_id']; ?>"><?= $storeName; ?></a>
							</h2>
						
					<?php 
					}?>
				</div>
				<div class="col-sm-4">  			
					<button type="button" class="btn-lg time-sch-btn" data-toggle="modal" data-target="#myModal2">
						<?php if($_SESSION['default']['language'] == 'ar'){ ?>
							<i class="fa fa-arrow-left"></i>
						<?php } ?>
						<i class="fa fa-clock-o"></i>
							<?php 
							
							if($nowdelivery != "") {
							  echo " $text_next_delivery <strong>$text_today, ".$nowdelivery;
							} elseif($fdelivery != "") {
							 echo " $text_next_delivery <strong>$text_tomorrow, ".$fdelivery;
							} elseif($sdelivery != "") {
							 echo " $text_next_delivery <strong>".$sdelivery;
							} elseif($tdelivery != "") {
							 echo " $text_next_delivery <strong>".$tdelivery;
							} elseif($ffdelivery != "") {
							 echo " $text_next_delivery <strong>".$ffdelivery;
							} elseif($fffdelivery != "") {
							 echo " $text_next_delivery <strong>".$fffdelivery;
							} elseif($ssdelivery != "") {
							 echo " $text_next_delivery <strong>".$ssdelivery;
							} else {
							 echo "<strong>$text_no_delivery";
							} 
												
							?></strong>
						<?php if($_SESSION['default']['language'] == 'en'){ ?>
							<i class="fa fa-arrow-right"></i>
						<?php } ?>
					</button>
				</div>
				<div class="charges-delivery-wrapper col-sm-8">
					<div class="col-md-4 text-right">
						<i class="fa fa-truck"></i> <?php echo $text_order_limit; ?> <strong><?= $seller_info['minimum_order']; ?></strong>
					</div>
					<div class="col-md-4 text-right">
						<i class="fa fa-dollar"></i> <?php echo $text_delivery_charges; ?> <strong><?= $seller_info['delivery_charges']; ?></strong>
					</div>
					<div class="col-md-4 text-right vat-val">
						<?php echo  $text_vat_included; ?>
					</div>
				</div>
			</div> 

			<!-- Modal -->
			<div id="myModal2" class="modal fade" role="dialog">
				<div class="modal-dialog">

					<!-- Modal content-->
					<div class="modal-content">
						<div class="modal-header time-model-header">
							<button type="button" class="close" data-dismiss="modal">&times;</button>
		<!--                    <h4 class="modal-title">Modal Header</h4>-->

							<div class="store-logo-wrapper text-center">
								<img src="./image/<?php echo $seller_info['image']; ?>" class="img-circle img-thumbnail img-rounded" />
							</div>


						</div>
						<div class="modal-body">
							<div class="row">
								<div class="title text-center">
									<h2><?php echo $text_delivery_schedule; ?></h2>
								</div>
								<div class="slide-btn-main">
								<button id="prev-slide" class="pull-left"><i class="fa fa-arrow-left" aria-hidden="true"></i></button>
								<span><?php echo $text_delivery_checkout; ?></span>
								<button id="next-slide" class="pull-right"><i class="fa fa-arrow-right" aria-hidden="true"></i></button>
								</div>
								
								<div class="slide-main">
									
									<div class="col-md-12 slide-active">
										<div class="pricing pricing-active hover-effect">
										<div class="pricing-head pricing-head-active">
											<h3><?php echo $text_today; ?>
		<!--                                                <span>Officia deserunt mollitia </span>-->
											</h3>
											<h4 class="<?= $nownextdelivery; ?>">
												
												<?php if($nownextdelivery != ""){ ?>
												<span><?php echo $text_next_delivery; ?></span>
												<?php } else { ?>
												<span><?php echo $text_store_closed; ?></span>
												<?php } ?>
												<i><?php echo $nownextdelivery; ?></i>
											</h4>
										</div>
										<ul class="pricing-content list-unstyled text-center">
											<?php echo $nowoutput; ?>
										</ul>
									</div>
										
									</div>
								
									
								
								
								<div class="col-md-12">
									<div class="col-md-4">
									<div class="pricing hover-effect">
										<div class="pricing-head">
											<h3><?php $name = strtolower($fdata["full"]); echo $days[$name]; ?>
		<!--                                                <span>Officia deserunt mollitia </span>-->
											</h3>
											<h4>
												<i><?php echo $fnextdelivery; ?></i>
												<span><?php echo $text_next_delivery; ?></span>
											</h4>
										</div>
										<ul class="pricing-content list-unstyled text-center">
											<?php echo $foutput; ?>
										</ul>
									</div>
									</div>
										<div class="col-md-4">
									<div class="pricing hover-effect">
										<div class="pricing-head">

												<h3><?php $name = strtolower($sdata["full"]); echo $days[$name]; ?>
											</h3>
											<h4>
												<i><?php echo $snextdelivery; ?></i>
												<span><?php echo $text_next_delivery; ?></span>
											</h4>
										</div>
										<ul class="pricing-content list-unstyled text-center">
											<?php echo $soutput; ?>
										</ul>
									</div>
								</div>
									
								<div class="col-md-4">
									<div class="pricing hover-effect">
										<div class="pricing-head">

												<h3><?php $name = strtolower($tdata["full"]); echo $days[$name]; ?>
											</h3>
											<h4>
												<i><?php echo $tnextdelivery; ?></i>
												<span><?php echo $text_next_delivery; ?></span>
											</h4>
										</div>
										<ul class="pricing-content list-unstyled text-center">
											<?php echo $toutput; ?>
										</ul>
									</div>
								</div>
							  
								
								
								</div>
									
									
								<div class="col-md-12">
									<div class="col-md-4">
									<div class="pricing hover-effect">
										<div class="pricing-head">

												<h3><?php $name = strtolower($ffdata["full"]); echo $days[$name]; ?>
		<!--                                                <span>Officia deserunt mollitia </span>-->
											</h3>
											<h4>
												<i><?php echo $ffnextdelivery; ?></i>
												<span><?php echo $text_next_delivery; ?></span>
											</h4>
										</div>
										<ul class="pricing-content list-unstyled text-center">
											<?php echo $ffoutput; ?>
										</ul>
									</div>
								</div>
								<div class="col-md-4">
									<div class="pricing hover-effect">
										<div class="pricing-head">

												<h3><?php $name = strtolower($fffdata["full"]); echo $days[$name]; ?>
		<!--                                                <span>Officia deserunt mollitia </span>-->
											</h3>
											<h4>
												<i><?php echo $fffnextdelivery; ?></i>
												<span><?php echo $text_next_delivery; ?></span>
											</h4>
										</div>
										<ul class="pricing-content list-unstyled text-center">
											<?php echo $fffoutput; ?>
										</ul>
									</div>
								</div>
								<div class="col-md-4">
									<div class="pricing hover-effect">
										<div class="pricing-head">

												<h3><?php $name = strtolower($ssdata["full"]); echo $days[$name]; ?>
		<!--                                                <span>Officia deserunt mollitia </span>-->
											</h3>
											<h4>
												<i><?php echo $ssnextdelivery; ?></i>
												<span><?php echo $text_next_delivery; ?></span>
											</h4>
										</div>
										<ul class="pricing-content list-unstyled text-center">
											<?php echo $ssoutput; ?>
										</ul>
									</div>
								</div>
							  
								</div>
								</div>
							</div>
						</div>
						<div class="modal-footer">
							<p class="btn pull-left"><?php echo $text_current_time; ?>: <?= $nowTime; ?></p>
							<button type="button" class="btn btn-default" data-dismiss="modal"><?php echo $text_close; ?></button>
						</div>
					</div>

				</div>
			</div>    

		</div> 
	  
	   <script>
			$(document).ready(function(){
				$(".slide-main > div").each(function(e) {
					if (e != 0)
						$(this).hide();
				});

				$("#next-slide").click(function(){
					if ($(".slide-main > div:visible").next().length != 0)
						$(".slide-main > div:visible").next().show().prev().hide();
					else {
						$(".slide-main > div:visible").hide();
						$(".slide-main > div:first").show();
					}
					return false;
				});

				$("#prev-slide").click(function(){
					if ($(".slide-main > div:visible").prev().length != 0)
						$(".slide-main > div:visible").prev().show().next().hide();
					else {
						$(".slide-main > div:visible").hide();
						$(".slide-main > div:last").show();
					}
					return false;
				});
				
				 $('.customer-loggedin').hover(function() {$("#customer-dropdown").addClass('open')});
					$('#customer-dropdown').mouseleave(function() {$(this).removeClass('open')});
			});
		</script>
	<?php 
	} 
}?>
