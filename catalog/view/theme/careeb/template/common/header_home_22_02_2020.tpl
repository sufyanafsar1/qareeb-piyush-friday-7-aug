<?php
$user_agent = getenv("HTTP_USER_AGENT");

if(strpos($user_agent, "Win") !== FALSE){
	$os = "Windows";
}elseif(strpos($user_agent, "Mac") !== FALSE){
	$os = "Mac";
}else{
	$os = "other";
}
?>
<?php global $config; global $request; ?>
<?php
$boss_manager = array(
	'option' => array(
		'bt_scroll_top' => true,
		'animation'   => true,      
	),
	'layout' => array(
		'mode_css'   => 'wide',
		'box_width'   => 1200,
		'h_mode_css'   => 'inherit',
		'h_box_width'   => 1200,
		'f_mode_css'   => 'inherit',
		'f_box_width'   => 1200
	),
	'status' => 1
);
?>
<?php 
if($config->get('boss_manager')){
	$boss_manager = $config->get('boss_manager'); 
}else{
	$boss_manager = $boss_manager;
} 
$header_link = isset($boss_manager['header_link'])?$boss_manager['header_link']:''; 
$option = isset($boss_manager['option'])?$boss_manager['option']:''; 
$loading = isset($boss_manager['option']['loading'])?$boss_manager['option']['loading']:''; 
?>
<!DOCTYPE html>
<![if IE]><![endif]
[if IE 8 ]><html dir="<?php echo $direction; ?>" lang="<?php echo $lang; ?>" class="ie8"><![endif]
[if IE 9 ]><html dir="<?php echo $direction; ?>" lang="<?php echo $lang; ?>" class="ie9"><![endif]
[if (gt IE 9)|!(IE)]>
<html dir="<?php echo $direction; ?>" lang="<?php echo $lang; ?>">
<![endif]>
<html>
	<head>
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<title><?php echo $title; ?></title>
		<base href="<?php echo $base; ?>" />
		<?php if ($description) { ?>
			<meta name="description" content="<?php echo $description; ?>" />
		<?php } ?>
    
		<?php if ($keywords) { ?>
			<meta name="keywords" content= "<?php echo $keywords; ?>" />
		<?php } ?>
    
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<link href="image/catalog/logo/Favicon-1.png" rel="icon">    
		<link rel="stylesheet" href="catalog/view/theme/<?php echo $config->get('config_template'); ?>/css/qareeb.css">
		 <link href='//fonts.googleapis.com/css?family=Montserrat:thin,extra-light,light,100,200,300,400,500,600,700,800' rel='stylesheet' type='text/css'>  
		
		<link href='https://fonts.googleapis.com/css?family=Montserrat:200,300,400,600,700,800,900' rel='stylesheet' type='text/css'>
		
		
		<?php if($direction=='rtl'){ ?>
			<link rel="stylesheet" type="text/css" href="catalog/view/theme/<?php echo $config->get('config_template'); ?>/css/stylertl.css" />
		<?php } ?>
		
		<script src="catalog/view/theme/<?php echo $config->get('config_template'); ?>/js/qareeb.js"></script>    
		
		<?php if (isset($analytics) && !empty($analytics)) { ?>
			<?php foreach ($analytics as $analytic) { ?>
				<?php echo $analytic; ?>
		<?php } } ?>
		
		<?php 
		 if(isset($this->request->get['route'])){
			$route1 = $this->request->get['route'];
		}else{		
			$route1 ="";
		}
			
		if(isset($route1) && ($route1== "common/home" || $route1=="") && $loading){ ?>
			<script type="text/javascript">
				$(document).ready(function() {
					$(".bt-loading").fadeOut("1500", function () {
						$('#bt_loading').css("display", "none");
					});
				});
			</script>
		<?php 
		}else{ ?>
			<script type="text/javascript">
				$(document).ready(function() {
					$('#bt_loading').css("display", "none");
				});
			</script>
		<?php 
		} ?>
		
		<link rel="manifest" href="/manifest.json">
		<script src="https://cdn.onesignal.com/sdks/OneSignalSDK.js" async defer></script>
		<script>
			var OneSignal = window.OneSignal || [];
			OneSignal.push(["init", {
				appId: "21057e6c-1429-44b2-8e38-2c3f5eae3186",
				autoRegister: false,
				persistNotification: false,
				welcomeNotification: {
					"title": "Careeb Online Grocery Store",
					"message": "Thanks for subscribing !",
					 "url": "" /* Leave commented for the notification to not open a window on Chrome and Firefox (on Safari, it opens to your webpage) */
				},

				notifyButton: {
					enable: true, /* Required to use the notify button */
					size: 'medium', /* One of 'small', 'medium', or 'large' */
					showCredit: false, /* Hide the OneSignal logo */
					position: 'bottom-left', /* Either 'bottom-left' or 'bottom-right' */
					colors: { // Customize the colors of the main button and dialog popup button
                      'dialog.button.background': 'rgb(22,107,24)',
                      'circle.background': 'rgb(22,107,24)',
                      'circle.foreground': 'white'
					}
				}

			}]);
		</script>
		<script src="/service_worker.js" defer></script>
	</head>
	<?php 
	if(isset($request->get['route'])){
		$route = $request->get['route'];
	}else{
		$route ="";
	}
	if(isset($route) && ($route== "common/home" || $route=="")){
		$home_page='landingBody landingBody'.$lang;
	}else{
		$home_page="landingBody landingBody".$lang;
	}
	?>	
  
	<body class="<?php echo $home_page; ?>">
	
		<div class="TopBar desktop-section">
		
			<div class="container">
		  
				<div class="logo pull-left">
					<?php 
					if ($logo) { ?>
						<a href="<?php echo $home; ?>"><img src="image/catalog/logo/QareebLogo.svg" title="<?php echo $name; ?>" class="img-responsive" alt="<?php echo $name; ?>" /></a>
					<?php 
					} else { ?>
						<h1><a href="<?php echo $home; ?>"><?php echo $name; ?></a></h1>
					<?php 
					} ?>     

				</div>
				
				<div class="links pull-right">					
					<?php  
					if ($logged) { ?>
						<a href="" class="dropdown-toggle arabic-section" data-toggle="dropdown"><span>Hello</span>, <?php echo $customer_name ?></a>
						
						<ul class="dropdown-menu home_dropdown-menu-right">
							<li><a href="<?php echo $account; ?>"><?php echo $text_account_deatil; ?></a></li>
							<li><a href="<?php echo $wishlist; ?>"><?php echo $text_wishlist; ?></a></li> 
							<li><a href="<?php echo $transaction; ?>"><?php echo $text_store_credit; ?></a></li>
							<li><a href="<?php echo $order; ?>"><?php echo $text_order; ?></a></li>
							<hr class="custom-hr"/>
							<li><a href="<?php echo $logout; ?>"><?php echo $text_logout; ?></a></li>     
						</ul>
					<?php 
					} else { ?>
						<a class="arabic-section" href="javascript:void(0);" id="signIn" title="<?php echo $text_register .'/'.$text_login; ?>"><img src="catalog/view/theme/<?php echo $config->get('config_template'); ?>/img/account.svg"></a>						
					<?php 
					}  ?>
					
					<a href="" class="arabic-section"><img src="catalog/view/theme/<?php echo $config->get('config_template'); ?>/img/cart-icon.svg" alt="..."></a>
					
					<a href="hitw" style="display:none"><?php echo $text_home; ?></a>
					<a href="<?php echo $home; ?>"><?php echo $text_home; ?></a>
					<a href="#how-it-works" class="how-it-works-Div"><?php echo $how_it_work ?> </a>	
					<a href="#partner-stores" class="partner-stores-Div"><?php echo $partner_stores; ?></a>
					<a href="#service-areas" class="service-areas-Div"><?php echo $serving_areas; ?></a>
					<a href="#supports" class="supports"><?php echo $supports; ?></a>
					<script>
						$(".how-it-works-Div").click(function() {
							$('html, body').animate({
								scrollTop: $("#how-it-works").offset().top
							}, 2000);
						});
						$(".partner-stores-Div").click(function() {
							$('html, body').animate({
								scrollTop: $("#partner-stores").offset().top
							}, 2000);
						});
						$(".service-areas-Div").click(function() {
							$('html, body').animate({
								scrollTop: $("#service-areas").offset().top
							}, 2000);
						});
						$(".supports").click(function() {
							$('html, body').animate({
								scrollTop: $("#supports").offset().top
							}, 2000);
						});
					</script>
					<?php 
					if(isset($header_link['language']) && $header_link['language']){ ?> 
						<div class="language_div"><?php echo $language; ?></div> 
					<?php 
					} ?>
					
					<a class="english-section" href=""><img src="catalog/view/theme/<?php echo $config->get('config_template'); ?>/img/cart-icon.svg" alt="..."></a>
					
					<?php  
					if ($logged) { ?>
						<a href="" class="dropdown-toggle english-section" data-toggle="dropdown"><span>Hello</span>, <?php echo $customer_name ?></a>
						
						<ul class="dropdown-menu home_dropdown-menu-right">
							<li><a href="<?php echo $account; ?>"><?php echo $text_account_deatil; ?></a></li>
							<li><a href="<?php echo $wishlist; ?>"><?php echo $text_wishlist; ?></a></li> 
							<li><a href="<?php echo $transaction; ?>"><?php echo $text_store_credit; ?></a></li>
							<li><a href="<?php echo $order; ?>"><?php echo $text_order; ?></a></li>
							<hr class="custom-hr"/>
							<li><a href="<?php echo $logout; ?>"><?php echo $text_logout; ?></a></li>     
						</ul>
					<?php 
					} else { ?>
						<a class="english-section" href="javascript:void(0);" id="signIn" title="<?php echo $text_register .'/'.$text_login; ?>"><img src="catalog/view/theme/<?php echo $config->get('config_template'); ?>/img/account.svg"></a>						
					<?php 
					}  ?> 
					
				</div>
			</div>

		</div> <!-- Topbar End-->
		
		
		<div class="TopBarNew mobile-section">      
			<div class="logo pull-left">
													
				<a href="<?php echo $home; ?>"><img class="qareeb_logo" src="image/catalog/logo/QareebLogoMobile.svg" title="<?php echo $name; ?>" class="img-responsive" alt="<?php echo $name; ?>" /></a>
				
			</div>
			
			<div class="links pull-right placeholder">
				<?php 
				if(isset($header_link['language']) && $header_link['language']){ ?> 
					<div class="pull-left language_divNew"><?php echo $language; ?></div> 
				<?php 
				} ?>									
			</div> 
			<div class="container container-fluid">
				<!-- Brand and toggle get grouped for better mobile display -->
				<div class="navbar-header">					
					<button type="button" class="navbar-toggle menuIcon" aria-expanded="false" onclick="openNav()">
						<img src="catalog/view/theme/careeb/img/list.svg" />
					</button>						
					
					<!-- <div class="logo pull-left">
						<a href="<?php echo $home; ?>"><img class="qareeb_logo" src	="image/catalog/logo/QareebLogoMobile.svg" title="<?php echo $name; ?>" class="img-responsive" alt="<?php echo $name; ?>" /></a>
					</div> -->
					
				</div>	
			
				<div id="mySidenav-Mobile" class="sidenav-Mobile">
					 <div class="sidenav-Mobile-Background"> 
						<?php echo $language; ?>
						<a href="javascript:void(0)" class="closebtn" onclick="closeNav()">&times;</a>
								
						<!-- <a href="#" class="dropdown-toggle storeLocation" data-toggle="modal" data-target="#storeModal" >test</a> -->
						
						<a href="<?php echo $home; ?>"><?php echo $text_home; ?></a>
						<a href="#how-it-works-app" class="how-it-works-Div-app"><?php echo $how_it_work ?> </a>	
						<a href="#partner-stores-app" class="partner-stores-Div-app"><?php echo $partner_stores; ?></a>
						<a href="#service-areas-app" class="service-areas-Div-app"><?php echo $serving_areas; ?></a>
						<a href="#supports-app" class="supports-app"><?php echo $supports; ?></a>
										
					</div> 
				</div>
				
				<script>
					$(".how-it-works-Div-app").click(function() {
						document.getElementById("mySidenav-Mobile").style.width = "0";
						$('html, body').animate({
							scrollTop: $("#how-it-works-app").offset().top
						}, 2000);
					});
					$(".partner-stores-Div-app").click(function() {
						document.getElementById("mySidenav-Mobile").style.width = "0";
						$('html, body').animate({
							scrollTop: $("#partner-stores-app").offset().top
						}, 2000);
					});
					$(".service-areas-Div-app").click(function() {
						document.getElementById("mySidenav-Mobile").style.width = "0";
						$('html, body').animate({
							scrollTop: $("#service-areas-app").offset().top
						}, 2000);
					});
					$(".supports-app").click(function() {
						document.getElementById("mySidenav-Mobile").style.width = "0";
						$('html, body').animate({
							scrollTop: $("#supports-app").offset().top
						}, 2000);
					});
				</script>
				<script>
					function openNav() {
					  document.getElementById("mySidenav-Mobile").style.width = "250px";
					}

					function closeNav() {
					  document.getElementById("mySidenav-Mobile").style.width = "0";
					}
				</script>			
			</div>
		</div> <!-- Topbar End-->
		
		<div class="searchMainDiv">
			<div class="carousel slide" data-ride="carousel"> 	
				<img class="mobHide" src="catalog/view/theme/<?php echo $config->get('config_template'); ?>/img/landing-banner-qareeb.jpg">
				<img class="deskHide" src="catalog/view/theme/<?php echo $config->get('config_template'); ?>/img/header-img-mobile.png">
				<div class="carousel-caption">				
					<div class="container">					
						<h3><?php echo html_entity_decode($text_shopping); ?></h3>
						<p class="desktop-section"><?php echo html_entity_decode($text_grocery); ?></p>
						<p class="mobile-section"><?php echo html_entity_decode($text_grocery_mobile); ?></p>
						 <div class="col-sm-8">
							<form method="POST" action="<?php echo $action; ?>">
								<div class="col-sm-2"></div>
								<div class="col-sm-10">
									<select  name="city_id" id="city_id">
										<option value="0" hidden><?php echo $text_city; ?></option>
										<img style="position: absolute" src="catalog/view/theme/<?php echo $config->get('config_template'); ?>/img/landingbuttonselect.png"/>
										<?php  
										foreach ($cities as $city) { 
											$selectedCity = ($city['city_id']==$city_saved) ? 'selected' : '';
										?>                            
											<option value="<?php echo $city['city_id']; ?>" <?php echo $selectedCity; ?> ><?php echo $city['name']; ?></option>                           
										<?php 
										} ?>
									</select>
								
									<!-- <select>
									<option><?php echo $text_region; ?></option>
									</select> -->
								
									<select  name="area_id" id="area_id">
										<option value="0" hidden><?php echo $text_area; ?></option>
										<img src="catalog/view/theme/<?php echo $config->get('config_template'); ?>/img/arrowButtommm.png">
									</select>
								</div>
								<div class="col-sm-12">		
									<div id="error_alert"></div>
									<input type="submit" id="findMe" value="<?php echo $text_find; ?>"/>
									<div class="mobile-section orLine"><span><?php echo $text_or; ?></span></div>				
								</div>										
							</form>                  
						</div>
						
						
						<div class="col-sm-4">
							<form id="frmFindMe"  method="POST" action="<?php echo $action; ?>">
								<input type="text" name="user_lat" id="user_lat" value="" style="display:none" />
								<input type="text" name="user_lng" id="user_lng" value="" style="display:none"/>
								<input type="text" name="user_area" id="user_area" value="" style="display:none"/>
								<button type="button" id="locateMe" class="locateMe"><span class="desktop-section"><img class="img-responsive" src="catalog/view/theme/<?php echo $config->get('config_template'); ?>/img/map-icon.svg"/>&nbsp;&nbsp;&nbsp;<?php echo $text_locate_me; ?></span><span class="mobile-section"><img class="img-responsive" src="catalog/view/theme/<?php echo $config->get('config_template'); ?>/img/map-icon.svg"/></span></button>								
							</form> 
						</div> 
						
						<div class="col-sm-12 desktop-section">				
							<div class="english-section">
								<a class="app-btn-store" href="https://itunes.apple.com/sa/app//id1453159366" target="_blank"><img src="catalog/view/theme/careeb/img/app-store-btnNew.svg"></a>
								
								<a href="https://play.google.com/store/apps/details?id=com.qareeb.user" target="_blank"><img src="catalog/view/theme/careeb/img/play-store-btn.svg"></a>	
							</div>
							<div class="arabic-section">
								<a class="app-btn-store" href="https://itunes.apple.com/sa/app//id1453159366" target="_blank"><img src="catalog/view/theme/careeb/img/app-store-btn-ar.svg"></a>
								
								<a href="https://play.google.com/store/apps/details?id=com.qareeb.user" target="_blank"><img src="catalog/view/theme/careeb/img/play-store-btn-ar.svg"></a>	
							</div>
						</div>
					</div>
				</div>				
			</div>
			
			<!-- Modal -->
			<div id="model-left">
				<div class="modal left fade" id="storeHomeModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" data-backdrop="static" data-keyboard="false">     
					<div class="modal-dialog" role="document">
						<div class="modal-content">
							<span class="retailer-chooser-arrow"></span>
							<div class="modal-header" id="modelHeader"></div>

							<div class="modal-body">
								<div>
									<div id="modalBody">
									<?php 
									if(isset($stores)) { ?>
										<h3 class="retailer-group-label"><?php echo $text_store_recommended; ?></h3>
										<ul class="retailer-options-wrapper unstyled">
										<?php 
										foreach($stores as $store) { ?>
											<li class="retailer-option">
												<a class="retailer-option-inner-wrapper absolute-center" <?php if($product_count != 0){ ?> onclick="return confirm('Your cart items will be removed while changing to other store. Are you sure to move to this store?')"<?php } ?> href="<?php echo $store['action'] ?>" data-bypass="true">
													<div class="retailer-option-logo-wrapper">
														<img class="absolute-center" src="<?php echo $store['image']; ?>" alt="<?php echo $store['firstname']  ?>">
													</div>
													<div class="retailer-option-body">
														<h3><?php echo $store['firstname']?></h3>
														<
													</div>
												</a>
											</li>
										<?php 
										} ?>
										</ul>
									<?php 
									}  ?>
									</div>
								</div>
							</div><!-- modal-body -->
						</div><!-- modal-content -->
					</div><!-- modal-dialog -->
				</div><!-- modal -->
			</div>
		</div>
		 <div class='slider sliderNew mobile-section'>
			<div id="carousel-example-generic" class="carousel slide" data-ride="carousel">      
				<!-- Wrapper for slides -->
				<div class="carousel-inner" role="listbox">
					<div class="item active">
						<img class="img-responsive mainBanner" src="catalog/view/theme/<?php echo $config->get('config_template'); ?>/img/header-img-mobile.jpg" alt="...">
						<div class="carousel-captionNew container">												
							<div class="TopContentNew">							
								<div class="col-sm-12 sencond6">
									<p class="Well-do-the-shoppin"><?php echo html_entity_decode($text_shopping); ?></p>
									<p class="Order-fresh-grocerie"><?php echo html_entity_decode($text_grocery); ?></p>
									
								</div>							 
							</div>	
						</div>
					</div>
				</div>		
			</div>
		</div>
		
		<div class="secondBanner mobile-section">
			<div class="container english-section">
				<p>
					<a  class="app-store-btn" href="https://itunes.apple.com/sa/app//id1453159366" target="_blank"><img src="catalog/view/theme/careeb/img/app-store-btnNew.svg"></a>
					
					<a class="play-store-btn" href="https://play.google.com/store/apps/details?id=com.qareeb.user" target="_blank"><img src="catalog/view/theme/careeb/img/play-store-btn.svg"></a> 
				</p>	
			</div>
			
			<div class="container arabic-section">
				<p>
					<a  class="app-store-btn" href="https://itunes.apple.com/sa/app//id1453159366" target="_blank"><img src="catalog/view/theme/careeb/img/app-store-btn-ar.svg"></a>
					
					<a class="play-store-btn" href="https://play.google.com/store/apps/details?id=com.qareeb.user" target="_blank"><img src="catalog/view/theme/careeb/img/play-store-btn-ar.svg"></a> 
				</p>	
			</div>			
		</div>
			
		<!-- Modal -->
		<div id="model-left">
			<div class="modal left fade" id="storeHomeModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" data-backdrop="static" data-keyboard="false">
 
				<div class="modal-dialog" role="document">
					<div class="modal-content">
						<span class="retailer-chooser-arrow"></span>
						<div class="modal-header" id="modelHeader"></div>

						<div class="modal-body">
							<div>
								<div id="modalBody">
								<?php 
								if(isset($stores)) { ?>
									<h3 class="retailer-group-label"><?php echo $text_store_recommended; ?></h3>
									<ul class="retailer-options-wrapper unstyled">
									<?php 
									foreach($stores as $store) { ?>
										<li class="retailer-option">
											<a class="retailer-option-inner-wrapper absolute-center" <?php if($product_count != 0){ ?> onclick="return confirm('Your cart items will be removed while changing to other store. Are you sure to move to this store?')"<?php } ?> href="<?php echo $store['action'] ?>" data-bypass="true">
												<div class="retailer-option-logo-wrapper">
													<img class="absolute-center" src="<?php echo $store['image']; ?>" alt="<?php echo $store['firstname']  ?>">
												</div>
												<div class="retailer-option-body">
													<h3><?php echo $store['firstname']?></h3>
													<
												</div>
											</a>
										</li>
									<?php 
									} ?>
									</ul>
								<?php 
								}  ?>
								</div>
							</div>
						</div><!-- modal-body -->
					</div><!-- modal-content -->
				</div><!-- modal-dialog -->
			</div><!-- modal -->
		</div>
	
		<?php echo $signin; ?>
		<?php echo $forgotepopup; ?>
		
		<script type="text/javascript"><!--
			$("#weAreWithYou2Link").click(function() {
				$('html,body').animate({
					scrollTop: $("#weAreWithYou2").offset().top},
					'slow');
			});

			$('select[name=\'city_id\']').on('change', function() {
				if(this.value == 0)
				return false;
				$.ajax({
					url: 'index.php?route=common/header_home/areas&city_id=' + this.value,
					dataType: 'json',
					beforeSend: function() {
						$('select[name=\'city_id\']').after(' <i class="fa fa-circle-o-notch fa-spin"></i>');
					},
					complete: function() {
					
						$('.fa-spin').remove();
					},
					success: function(json) {
						console.log(json);
						$('.fa-spin').remove();
				
						html = '<option value="0" hidden><?php echo $text_area; ?></option>';
				
						if (json) {
							for (i = 0; i < json['areas'].length; i++) {
								html += '<option value="' + json['areas'][i]['area_id'] + '"';
				
								if (json['areas'][i]['area_id'] == json['area_saved']) {
									html += ' selected="selected"';
								}
				
								html += '>' + json['areas'][i]['name'] + '</option>';
							}
						} else {
							html += '<option value="0" selected="selected" hidden><?php echo $text_area; ?></option>';
						}
						//console.log(htm);
						$('select[name=\'area_id\']').html(html);
					},
					error: function(xhr, ajaxOptions, thrownError) {
						alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
						//alert(thrownError);
					}
				});
			});

			$('select[name=\'city_id\']').trigger('change');
		
			$("#locateMe").click(function(){
				//alert('ok');
			   getUserCurrentLocation();
			});

			function getUserCurrentLocation() {
				navigator.geolocation.getCurrentPosition(success, error, options);
			}			
				
			var options = {
				enableHighAccuracy: true,
				timeout: 5000,
				maximumAge: 0
			};

			function success(pos) {
				var crd = pos.coords;
				var lat = crd.latitude;
				var lng = crd.longitude;
				$("#user_lat").val(lat);
				$("#user_lng").val(lng);
				var auto_area = "";
				var auto_city = "";
				var apiKey = "AIzaSyBlpCdiNIP0fJnV1TFhaNwyEXbDNCrqz9E";
				var baseUrl= "https://maps.googleapis.com/maps/api/geocode/json?latlng=";
				var url = encodeURI(baseUrl + lat+","+lng+"&key="+apiKey); //geocoding url
				$.getJSON(url, function(data) { //get Geocoded JSONP data
					if(data !="")
					{
						var result = data.results[0];
					  
						for(var i=0, len=result.address_components.length; i<len; i++) {
						
							var ac = result.address_components[i];
							if(ac.types.indexOf("locality") >= 0) auto_city = ac.long_name;
							if(ac.types.indexOf("route") >= 0) auto_area = ac.long_name;
						
						}//for loop

						$("#user_area").val(auto_area);

					}// Data not null

					var apiLink =   'index.php?route=common/store/getStoresByAreaId&user_lat=' + lat + '&user_lng='+ lng+ '&user_area='+ auto_area;

					setTimeout(function(){
						$("#frmFindMe" ).submit();
						
						openStoresModal(apiLink);
					}, 200);

				}); //json

			};

			function error(err) {
			 
				$(".alert-danger").remove();
				$('#error_alert').append('<div class="alert alert-danger alert-custom"><i class="fa fa-exclamation-circle"></i> <?php echo $text_location_disable; ?> </div>');
			   
				return false;
			};    


			$('#findMe').click(function () {
				var city = $("#city_id").val();
				var area = $("#area_id").val();

						if (city == 0) {
							$(".alert-danger").remove();
							$('#error_alert').append('<div class="alert alert-danger alert-custom"><i class="fa fa-exclamation-circle"></i> <?php echo $text_city; ?> </div>');
							$('#city_id').addClass('has-error');                  
							$('#city_id').focus();
							return false;
						}
						else if (area==0) {
							$(".alert-danger").remove();
							$('#error_alert').append('<div class="alert alert-danger alert-custom" ><i class="fa fa-exclamation-circle"></i>  <?php echo $text_area; ?></div>');
							$('#area_id').addClass('has-error');                  
							$('#area_id').focus();
							return false;
						}
						
					  var apiLink =   'index.php?route=common/store/getStoresByAreaId&area_id=' + area+ '&city_id='+ city;
					 openStoresModal(apiLink)
					   
					   return false;
			});
			
			
			function openStoresModal(apiLink){

				//Retrieve Stores        
				$.ajax({
					url: apiLink ,
					dataType: 'json',	
					success: function(json) {
						var headerHtml = '<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>';
				  
						if(json['stores'] !=""){
							headerHtml += "<h1 class='centered'>" + json['text_store']+  "  <strong>" + json['area'] + "</strong></h1>";
						}else{
							headerHtml += "<h1 class='centered'>" + json['error_service']+  "  <strong>" + json['area'] + "</strong></h1>";
						}
						
						var bodyHtml = '';
						if(json['stores'] !=""){
							bodyHtml += "<h3 class='retailer-group-label'> " + json['text_store_recommended'] + "</h3>";
							bodyHtml += "<ul class='retailer-options-wrapper unstyled'>";
							for (i = 0; i < json['stores'].length; i++) {
								bodyHtml += "<li class='retailer-option'><div class='col-sm-12 firstPopupDiv'><div class='col-sm-6'>";
								if(json['product_count'] > 0){
								 bodyHtml += "<a class='retailer-option-inner-wrapper absolute-center' onclick='return confirm('Your cart items will be removed while changing to other store. Are you sure to move to this store?')' href='"+ json['stores'][i]['action']  +"' data-bypass='true'>";
								}else{
								   bodyHtml += "<a class='retailer-option-inner-wrapper absolute-center'  href='"+ json['stores'][i]['action']  +"' data-bypass='true'>"; 
								}
								
								bodyHtml += "<div class='retailer-option-logo-wrapper'>";
								bodyHtml += "<img class='absolute-center' src='" + json['stores'][i]['image'] + "' alt='" + json['stores'][i]['name'] + "'>";
								bodyHtml += "</div>";
								
								bodyHtml += "</a></div><div class='col-sm-6'>";
								if(json['product_count'] > 0){
								 bodyHtml += "<a class='retailer-option-inner-wrapper absolute-center' onclick='return confirm('Your cart items will be removed while changing to other store. Are you sure to move to this store?')' href='"+ json['stores'][i]['action']  +"' data-bypass='true'>";
								}else{
								   bodyHtml += "<a class='retailer-option-inner-wrapper absolute-center'  href='"+ json['stores'][i]['action']  +"' data-bypass='true'>"; 
								}
																
								bodyHtml += "<div class='retailer-option-body'>";
								bodyHtml += "<h3>" + json['stores'][i]['name'] + "</h3>";
								bodyHtml += "</div>";
								bodyHtml += "</a></div></div>";
								
								bodyHtml += "<div class='col-sm-12 secondPopupDiv'><div class='col-sm-6'>";
								if(json['product_count'] > 0){
								 bodyHtml += "<a class='retailer-option-inner-wrapper absolute-center' onclick='return confirm('Your cart items will be removed while changing to other store. Are you sure to move to this store?')' href='"+ json['stores'][i]['action']  +"' data-bypass='true'>";
								}else{
								   bodyHtml += "<a class='retailer-option-inner-wrapper absolute-center'  href='"+ json['stores'][i]['action']  +"' data-bypass='true'>"; 
								}
								
								bodyHtml += "<div class='retailer-option-body'>";
								bodyHtml += json['text_minimum'] +" " + json['stores'][i]['minimum_order'] + "";				
								bodyHtml += "</div>";
								
								bodyHtml += "</a></div><div class='col-sm-6'>";
								if(json['product_count'] > 0){
								 bodyHtml += "<a class='retailer-option-inner-wrapper absolute-center' onclick='return confirm('Your cart items will be removed while changing to other store. Are you sure to move to this store?')' href='"+ json['stores'][i]['action']  +"' data-bypass='true'>";
								}else{
								   bodyHtml += "<a class='retailer-option-inner-wrapper absolute-center'  href='"+ json['stores'][i]['action']  +"' data-bypass='true'>"; 
								}
																
								bodyHtml += "<div class='retailer-option-body'>";
								bodyHtml += json['text_delivery'] +" " + json['stores'][i]['delivery_charges'] + "";
								bodyHtml += "</div>";
								bodyHtml += "</a></div></div>";
								
								bodyHtml += "</li>";                        
							}
							bodyHtml += "</ul>";
						}else{
							bodyHtml += "<h4 class='centered'> " + json['error_area'] + "</h4>";
						}
					  
						$('#modelHeader').html(headerHtml);
						$('#modalBody').html(bodyHtml);			
					},
					error: function(xhr, ajaxOptions, thrownError) {
						alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
					}
				}); //Ajax end
		 
			   $("#storeHomeModal").modal('show');
			}
		</script>