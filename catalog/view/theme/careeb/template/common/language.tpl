<?php 
if (count($languages) > 2) { ?>
	<form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data" class="">
		<div class="btn-group">
			<button class="btn-link dropdown-toggle" data-toggle="dropdown">
			<span><?php echo $text_language; ?></span>
			<?php foreach ($languages as $language) { ?>
				<?php if ($language['code'] != $code) { ?>
					<strong><img src="image/flags/<?php echo $language['image']; ?>" alt="<?php echo $language['name']; ?>" title="<?php echo $language['name']; ?>" /><b><?php echo $language['name']; ?></b></strong>
				<?php } ?>
			<?php } ?>
			<i class="fa fa-angle-down"></i></button>
			<ul class="dropdown-menu">
				<?php foreach ($languages as $language) { ?>
					<li>
						<a href="<?php echo $language['code']; ?>"><img src="image/flags/<?php echo $language['image']; ?>" alt="<?php echo $language['name']; ?>" title="<?php echo $language['name']; ?>" /><span><?php echo $language['name']; ?></span></a>	  
					</li>
				<?php } ?>
			</ul>
		</div>
		<input type="hidden" name="code" value="" />
		<input type="hidden" name="redirect" value="<?php echo $redirect; ?>" />
	</form>
<?php 
} else if (count($languages) > 1 && count($languages) ==2 ) { ?>
    
	<form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data" class="language">
		<div class="btn-group">
			<?php foreach ($languages as $language) { ?>
				<?php if ($language['code'] != $code) { ?>
					<a href="<?php echo $language['code']; ?>"><?php echo $language['name']; ?>
						<img class="desktopSec" src="catalog/view/theme/careeb/img/lang-switcher-arrow.svg" alt="<?php echo $language['name']; ?>" title="<?php echo $language['name']; ?>" />
						<img class="mobileSec" src="catalog/view/theme/careeb/img/lang-switcher-arrow-mobile.svg" alt="<?php echo $language['name']; ?>" title="<?php echo $language['name']; ?>" />
					</a>									
				<?php } ?>
			<?php } ?>		
		</div>
		<input type="hidden" name="code" value="" />
		<input type="hidden" name="redirect" value="<?php echo $redirect; ?>" />
	</form>
<?php 
} ?>