<div id="search">
  <form class="navbar-form navbar-left">
        <div class="form-group">
          <input type="text" class="form-control" placeholder="<?php echo $text_search; ?>" name="search" id="search-live" value="<?php echo $search; ?>"/>
          <input type="hidden"  name="seller_id" id="seller_id" value="<?php echo $seller_id; ?>"/>
          <button type="button" class="searchbtn" id="btnSearch"><span class="mobile-section"><?php echo $text_search; ?></span></button>
        </div>
      </form>
</div>
<div id= "live-search-dropdown" style= "display: none;">
  <ul style="list-style-type: none; margin-left: 0px; margin-bottom: 0px; padding-left:  0px; font-weight: bold;">
   
  </ul>
 </div>
<script>
$("#btnSearch").click(function(){   
    var filter = $("#search-live").val();
	if(filter == ''){
		$("#search-live").attr("placeholder", "Please enter some keywords.").placeholder();
		return false;
	}
    var seller_id = <?php echo $seller_id ?>;
   window.location.href = 'index.php?route=product/search&search='+filter+'&seller_id='+seller_id; 
});
</script>