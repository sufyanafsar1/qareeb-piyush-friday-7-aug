<!-- Modal -->
<div id="model-left">

	<div class="modal left fade" id="storeModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" data-backdrop="static" data-keyboard="false">
	
		<div class="modal-dialog" role="document">
			<div class="modal-content">
				<span class="retailer-chooser-arrow"></span>
				<div class="modal-header" id="modelCommonHeader">
					<?php /* <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button> */ ?>
					<?php if($stores) { ?>
						<h1 class="centered"><?php echo $text_store; ?> <strong><?php echo $area; ?></strong></h1>
					<?php } else { ?>
						<h1 class="centered"><?php echo $error_service; ?>  <strong><?php echo $area; ?></strong></h1>
					<?php } ?>
					<p class="header-location-link centered"><a href="#" class="dropdown-toggle" data-toggle="modal" data-target="#locationModal" ><?php echo $text_change_location; ?> <span class="caret"></span></a></p>
				</div>

				<div class="modal-body">
					<div>                      
						<div id="modalCommonBody">
							<?php 
							if($stores) { ?>
								<h3 class="retailer-group-label"><?php echo $text_store_recommended; ?></h3>
								<ul class="retailer-options-wrapper unstyled">
									<?php 
									foreach($stores as $store) { ?>
										<li class="retailer-option">
											<div class="col-sm-12 firstPopupDiv">
												<div class="col-sm-6">
													<a class="retailer-option-inner-wrapper absolute-center" href="<?php echo $store['action'] ?>" data-bypass="true">
														<div class="retailer-option-logo-wrapper">
															<img class="absolute-center" src="<?php echo $store['image']; ?>" alt="<?php echo $store['firstname']." ".$store['lastname']  ?>">
														</div>												
													</a>
												</div>
												<div class="col-sm-6">
													<a class="retailer-option-inner-wrapper absolute-center" href="<?php echo $store['action'] ?>" data-bypass="true">												
														<div class="retailer-option-body">
															<h3><?php echo $store['name'];?></h3>
														</div>
													</a>
												</div>
											</div>
											<div class="col-sm-12 secondPopupDiv">
												<div class="col-sm-6">
													<a class="retailer-option-inner-wrapper absolute-center"  href="<?php echo $store['action'] ?>" data-bypass="true">
														<div class="retailer-option-body">
															<?php echo $text_minimum.' '.$store['minimum_order'];?>
														</div>												
													</a>
												</div>
												<div class="col-sm-6">
													<a class="retailer-option-inner-wrapper absolute-center" href="<?php echo $store['action'] ?>" data-bypass="true">	
														<div class="retailer-option-body">
															<?php echo $text_delivery.' '.$store['delivery_charges'];?>
														</div>
													</a>
												</div>
											</div>
										</li>
									<?php 
									} ?>
								</ul>
							<?php 
							} else {?>
								<h4 class="centered"><?php echo $error_area; ?> </h4>
							<?php 
							} ?>
						</div>
					</div>
				</div><!-- modal-body -->	

			</div><!-- modal-content -->
		</div><!-- modal-dialog -->
	</div><!-- modal -->
</div>