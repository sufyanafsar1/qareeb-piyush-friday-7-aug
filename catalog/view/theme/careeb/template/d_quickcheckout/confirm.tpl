<!-- 
	Ajax Quick Checkout 
	v6.0.0
	Dreamvention.com 
	d_quickcheckout/confirm.tpl 
-->
<div id="confirm_view" class="qc-step" data-col="<?php echo $col; ?>" data-row="<?php echo $row; ?>"></div>
<script type="text/html" id="confirm_template">
<div id="confirm_wrap">
	<div class="">
		<div class="">
			<form id="confirm_form" class="form-horizontal">
			</form>
			
			
           <div class="col-sm-4"></div>
                <div class="h3Next">
                  
                <button  id="qc_confirm_order" class="next" <%= model.show_confirm ? '' : 'disabled="disabled"' %>><% if(Number(model.payment_popup)) { %><?php echo $button_confirm; ?><% }else{ %><?php echo $button_confirm; ?><% } %></span><i class="fa fa-arrow-right"></i></button>
                </div>
                 <div class="col-sm-4"></div>
		</div>
	</div>
</div>
</script>
<script>

$(function() {
	qc.confirm = $.extend(true, {}, new qc.Confirm(<?php echo $json; ?>));
	qc.confirmView = $.extend(true, {}, new qc.ConfirmView({
		el:$("#confirm_view"), 
		model: qc.confirm, 
		template: _.template($("#confirm_template").html())
	}));
});

</script>