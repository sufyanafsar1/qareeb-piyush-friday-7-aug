<script>
var config = <?php echo $json_config; ?>;
if(typeof(ga) == "undefined")
   config.general.analytics_event = 0;
</script>
<style>
<?php echo $config['design']['custom_style']; ?>
<?php if($config['design']['only_quickcheckout']){ ?>
body > *{
	display: none
}
body > #d_quickcheckout{
	display: block;
} 
#d_quickcheckout.container #d_logo{
	margin: 20px 0px;
}
<?php } ?>
</style>

       <ul id="order" class="nav nav-tabs nav-justified" style="display:none">
            <li class="disabled active"><a href="#tab-address" data-toggle="tab"><?php echo $title_delivery; ?></a></li>
            <li class="disabled"><a href="#tab-delivery" data-toggle="tab"><?php echo $title_delivery_date_time; ?></a></li>
            <li class="disabled"><a href="#tab-payment" data-toggle="tab"><?php echo $title_payment; ?></a></li>
            <li class="disabled"><a href="#tab-summary" data-toggle="tab"><?php echo $title_summary; ?></a></li>
          </ul>
          <div class="tab-content tab-content-custom" id="cartContent">
            <div class="tab-pane active" id="tab-address">
            <div class="container">
            <div class="checkOut"><div class="text-center">
            <img class="progressImg img-responsive" src="catalog/view/theme/bt_claudine/image/step-1.png" alt="">
            <div class="h3Next">
                <h3>Select Delivery Address</h3>
              <button type="button" id="button-interval" data-loading-text="Loading" name="next">Next <i class="fa fa-arrow-right"></i></button></div></div>
             <div class="address">
        <div class="panel panel-default">
          <div class="panel-heading">
            <div class='pull-left'>
              <h3><img src="catalog/view/theme/bt_claudine/image/addressIcon.png" alt=""><?php echo $c_name;?> </h3>
            </div>
            <div class='pull-right'>
              <img src="catalog/view/theme/bt_claudine/image/addicon.png">
              <img src="catalog/view/theme/bt_claudine/image/editIcon.png">
            </div>
          </div>
          <div class="panel-body">
            <div class="col-md-6">
              <p><?php echo $payment_address;?></p>
            </div>
            <div class="col-md-6">
              <img class="img-responsive" src="catalog/view/theme/bt_claudine/image/map.png" alt="">
            </div>
          </div>
        </div>
      </div>
      <?php echo $timeslot; ?>
              <div class="col-sm-4"></div>
		

           
	          
            
            </div></div></div>
            
             
            <div class="tab-pane" id="tab-payment">
            <div class="container">
            <div class="checkOut">
            <div class="text-center">
            <img class="progressImg img-responsive" src="catalog/view/theme/bt_claudine/image/step-2.png" alt="">
            <div class="h3Next"><button type="button" class="back" id="button-address1" data-loading-text="Loading" name="next"><i class="fa fa-arrow-left"></i> Back</button>
                <h3>Payment Method</h3>
              <button type="button" id="button-payment" data-loading-text="Loading" name="next">Next <i class="fa fa-arrow-right"></i></button></div></div>
 <div class="address">
             <?php echo $payment_method; ?>
               <div class="col-sm-12" id="bank_details">
    
                <h2>Bank Transfer Instructions</h2>
                 <p><b><?php echo $text_bank_transfer; ?></b></p>
                 <div class="well well-sm">
                  <p><?php echo $text_bank_details; ?>
                </p>
                  <p><?php echo $text_bank_warning; ?></p>
                </div> 
              </div>
                               
       
             </div>
             
            </div>
            </div>
             
            </div>
                        
            <div class="tab-pane" id="tab-summary">
            <div class="container">
            <div class="checkOut">
              <div class="text-center">
                <img class="progressImg img-responsive" src="catalog/view/theme/bt_claudine/image/step-3.png" alt="">
                <div class="h3Next"><button type="button" class="back" id="button-payment1" data-loading-text="Loading" name="next"><i class="fa fa-arrow-left"></i> Back</button>
                <h3>Summary</h3>
              </div>
            </div><div class="address">
              <?php echo $cart; ?>
		<?php echo $payment; ?></div>
		<?php echo $confirm; ?>
              
            </div>
            </div>
            </div>
          </div>

<div id="d_quickcheckout">
	<?php echo $field; ?>

</div>
<script type="text/javascript"><!--
$("#button-interval").css("display","none");
// Disable the tabs
$('#order a[data-toggle=\'tab\']').on('click', function(e) {
	return false;
});
$('#button-payment1').on('click', function() {
	$('a[href=\'#tab-payment\']').tab('show');
   
});
$('#button-address1').on('click', function() {
	$('a[href=\'#tab-address\']').tab('show');
   
});
/*
 $('#button-interval').on('click', function() {
 	    $('a[href=\'#tab-payment\']').tab('show');
      paymentMethod();
      $("#cartContent").attr('class', 'tab-content');
    
 });
 */

$(document).delegate('#button-interval', 'click', function() {

    
    $.ajax({
        url: 'index.php?route=module/timeslot/timeslot/save',
        type: 'post',
        data: {get_slot : $('#get_slots').val(), delivery_slot_time: $('#slots').val()},
        dataType: 'html',
        beforeSend: function() {
        	$('#button-interval').button('loading');
		},
        complete: function() {
			$('#button-interval').button('reset');
        },
        success: function(json) {
            
            if(json=='fulled'){
            
                $("#error_max_slot").css("display","block");
                $("#error_max_slot").css("color","red");
               
                return false;
        }else{
            
            $("#error_max_slot").css("display","none");
            
        }
            $('.alert, .text-danger').remove();

            if (json['redirect']) {
                location = json['redirect'];
            } else if (json['error']) {
                if (json['error']['warning']) {
                    $('#tab-delivery').prepend('<div class="alert alert-warning">' + json['error']['warning'] + '<button type="button" class="close" data-dismiss="alert">&times;</button></div>');
                }
            } else {
                $('a[href=\'#tab-payment\']').tab('show');
                  paymentMethod();
                  $("#cartContent").attr('class', 'tab-content');
            }
        },
        error: function(xhr, ajaxOptions, thrownError) {
            alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
        }
    });
});



/*
 $('#button-address').on('click', function() {
 	$('a[href=\'#tab-payment\']').tab('show');
     paymentMethod();
     $("#cartContent").attr('class', 'tab-content');
 });
 */
$('#button-payment').on('click', function() {
	$('a[href=\'#tab-summary\']').tab('show');
    $("#cartContent").attr('class', 'tab-content tab-content-custom');
});
 

</script>
