<?php global $config; ?>

<div class="clearfix"></div>
<div class="boss-testimonial" id="boss_testimonial_min_height_<?php echo $module; ?>">
    
    <div class='testimonials'>
      <div class='container text-center'>
       <?php if(isset($testimonial_title) && $testimonial_title!=''){?>
        <h3> <?php  echo $testimonial_title; ?></h3>
        <?php } ?>
        
        <?php 
         $i = 1;
        foreach ($testimonials as $testimonial) {
         $activeClass = ($i ==1)? "active" : "";
                   ?>                  
        <div class='opinion <?php echo  $activeClass; ?>'>
			<div class="title"><h4><u><?php echo $testimonial['title']; ?></u></h4></div>
			</br>
            <p>
				<?php if(isset($show_message) && $show_message){ ?>
					<?php echo $testimonial['description']; ?>
				<?php } ?>
                <br/>				
				<div class="average">
					<?php // echo $text_average; ?>
					<div class="rating">
					  <?php for ($i = 1; $i <= 5; $i++) { ?>
					  <?php if ($testimonial['rating'] < $i) { ?>
					  <span class="fa fa-stack"><i class="fa fa-star-o fa-stack-2x"></i></span>
					  <?php } else { ?>
					  <span class="fa fa-stack"><i class="fa fa-star fa-stack-2x"></i></span>
					  <?php } ?>
					  <?php } ?>
					</div>  
					-&nbsp;<i><b><?php echo $testimonial['name'].'</b> '.$testimonial['city']; ?></i>
				 </div>				
            </p>
        </div>
       <?php $i++; } ?>
      </div><!-- container text-center-->
    </div><!-- testimonials-->
</div><!-- boss-testimonial-->