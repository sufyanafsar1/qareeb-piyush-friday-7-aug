<div class="bt-box">
	<div id="submenu">
		<div class="box-heading"><span><?php echo $heading_title; ?></span></div>
		<div class="box-content">
			<div class="submenu-inside bxslider">
				<?php foreach ($categories as $category) { ?>
					<dl class="col-md-3 cate-list-item<?php echo($category['children'])? ' submenu':''; ?>">
						<dt class="cate-name">
							<a href="<?php echo $category['href']; ?>" <?php if ($category['category_id'] == $category_id) echo 'class="active"'; ?> style="background:linear-gradient(rgba(0, 0, 0, 0.5), rgba(20, 20, 20, 0.5)), url(<?php echo $category['image'] ?>)"><?php echo $category['name']; ?></a>
						</dt>
					</dl>
				<?php } ?>
			</div>
		</div>
	</div>
</div>
<script type="text/javascript">
	$(document).ready(function(){
		$('.bxslider').bxSlider({
			slideWidth: 310,
		  	captions: false,
		  	maxSlides: 4,
		  	moveSlides: 4,
		  	slideMargin: 5,
		  	responsive: true
		});
	});
</script>