<div class="Mask desktop-section">	
	<div class="container shopperContainer">		
		<div class="col-sm-12">		
			<div class="col-sm-12">	
				<h1 class="Mask-Copy-h1"><?php echo $heading_title; ?></h1>
			</div>
			<div class="col-sm-8">						
				<div class="col-sm-12">		
					<div class="col-sm-5">	
						<ul class="ulDiv">
							<li><?php echo $firstli; ?></li>
							<li><?php echo $secondi; ?></li>
							<li><?php echo $thirdli; ?></li>
						</ul>
					</div>
					<div class="col-sm-5">							
						<ul class="ulDiv">
							<li><?php echo $forthli; ?></li>
							<li><?php echo $fifthli; ?></li>
							<li><?php echo $sixthli; ?></li>
						</ul>
					</div>
				</div>
				<div class="contentDiv col-sm-11">		
					<span class="Mask-Copy-span"><?php echo $avld_text; ?></span>	
					<h1 class="Mask-Copy-h2"><?php echo $download_text; ?></h1>
					<a class="app-store-btn english-section" href="https://itunes.apple.com/sa/app//id1453159366" target="_blank"><img class="img-responsive" src="catalog/view/theme/careeb/img/app-store-btn.svg"></a>
					
					<a class="play-store-btn english-section" href="https://play.google.com/store/apps/details?id=com.qareeb.user" target="_blank"><img class="img-responsive" src="catalog/view/theme/careeb/img/google-play-store.svg"></a>
					
					<a class="app-store-btn arabic-section" href="https://itunes.apple.com/sa/app//id1453159366" target="_blank"><img class="img-responsive" src="catalog/view/theme/careeb/img/app-store-btn-white-ar.svg"></a>
					
					<a class="play-store-btn arabic-section" href="https://play.google.com/store/apps/details?id=com.qareeb.user" target="_blank"><img class="img-responsive" src="catalog/view/theme/careeb/img/google-play-store-white-ar.svg"></a>
				</div>
			</div>			
		</div>
	</div>
	<div class="container containerNew" id="supports">	
		<div class="col-sm-12">			
			<div class="col-sm-6">		
			
				<h1 class="Mask-Copy-h1"><?php echo $text_need_assistances; ?></h1>
				<span class="riyadh-Copy-6"><hr></span>	
				<p class="lookText"><?php echo $text_query; ?></p>
				</br>
				<p class="classAddress"><?php echo $text_UAE; ?></p>
				<p class="classAddress"><?php echo $text_SA; ?></p>
			</div>
			<div class="col-sm-6">		
				<form name="needAssistantFromDesktop" id="needAssistantFromDesktop">	

					<div class="col-sm-6">
						<fieldset>
							<legend><?php echo $text_name; ?></legend>
							<input type="text" name="name" id="nameDesk">
						</fieldset>						
					</div>
					<div class="col-sm-6">
						<fieldset>
							<legend><?php echo $text_email; ?></legend>
							<input type="text" name="email" id="emailDesk">
						</fieldset>						
					</div>
					<div class="col-sm-6">
						<fieldset>
							<legend><?php echo $text_phone; ?></legend>
							 <input type="text" name="phone" id="phoneDesk">
						</fieldset>						
					</div>
					<div class="col-sm-6">
						<fieldset>
							<legend><?php echo $text_subject; ?></legend>
							<input type="text" name="subject" id="subjectDesk">
						</fieldset>						
					</div>
					<div class="col-sm-12">
						<fieldset>
							  <legend><?php echo $text_mesage; ?></legend>							  
							  <textarea type="text" name="message" id="messageDesk" rows="4"></textarea>
						</fieldset>
						
					</div>
					<div class="col-sm-12">
						<button class="btnClass" type="button" name="submit" id="b_button_desktop" value="<?php echo $text_btn_subject; ?>" onclick="need_assistant_desktop()"><?php echo $text_btn_subject; ?></button>
					</div>
				</form>
			</div>
		</div>
	</div>
</div>
<div class="Rectangle-Copy-New desktop-section">	
	<div class="container containerNew">		
		<div class="col-sm-12">				
			<div class="col-sm-6">		
				<p class="classAddress"><?php echo $text_tel; ?></p>
			</div>
		</div>
		
	</div>			
	<div class="container containerImg">
		<img src="catalog/view/theme/careeb/img/footer-map.jpg">	
	</div>
</div>

<div class="Mask mobile-section">	
	<div class="container">		
		<h1 class="Mask-Copy-h1"><?php echo $heading_title; ?></h1>
		<div class="col-sm-12">			
			<div class="col-sm-5">			
				<ul class="ulDiv">
					<li><?php echo $firstli; ?></li>
					<li><?php echo $secondi; ?></li>
					<li><?php echo $thirdli; ?></li>
					<li><?php echo $forthli; ?></li>
					<li><?php echo $fifthli; ?></li>
					<li><?php echo $sixthli; ?></li>					
				</ul>
			</div>
		</div>
		<div class="colsm12">
			<p class="Mask-Copy-span"><?php echo $avld_text; ?></p>	
			
			<h1 class="Mask-Copy-h2"><?php echo $download_text; ?></h1>
			
			<p class="app-btn english-section"><a class="app-store-btn" href="https://itunes.apple.com/sa/app//id1453159366" target="_blank"><img src="catalog/view/theme/careeb/img/app-store-btn-mobile.svg"></a></p>
			
			<p class="app-btn english-section"><a class="play-store-btn" href="https://play.google.com/store/apps/details?id=com.qareeb.user" target="_blank"><img src="catalog/view/theme/careeb/img/google-play-store-mobile.svg"></a></p>
			
			<p class="app-btn arabic-section"><a class="app-store-btn" href="https://itunes.apple.com/sa/app//id1453159366" target="_blank"><img src="catalog/view/theme/careeb/img/app-store-btn-mobile-ar.svg"></a></p>
			
			<p class="app-btn arabic-section"><a class="play-store-btn" href="https://play.google.com/store/apps/details?id=com.qareeb.user" target="_blank"><img src="catalog/view/theme/careeb/img/google-play-store-mobile-ar.svg"></a></p>
		</div>
	</div>
	<p class="contentDivNew">
		<img src="catalog/view/theme/careeb/img/shopper-img-mobile.png">		
	</p>
	
	<div class="container containerNew" id="supports-app">				
		<h1 class="Mask-Copy-h1"><?php echo $text_need_assistances; ?></h1>
		<span class="riyadh-Copy-6"><hr></span>	
		<p class="lookText"><?php echo $text_query_mob; ?></p>
		
		<p class="classAddress"><?php echo $text_UAE; ?></p>
		<p class="classAddress"><?php echo $text_SA; ?></p>
				
		<p class="classAddress"><?php echo $text_tel; ?></p>		
	</div>	
	
	<div class="container containerNew MaskForm">				
		
		<form name="needAssistantFromMobile" id="needAssistantFromMobile">
		
			<div class="col-sm-12">
				<fieldset>
					<legend><?php echo $text_name; ?></legend>
					<input type="text" name="name" id="nameMob">					
				</fieldset>						
			</div>			
			<div class="col-sm-12">
				<fieldset>
					<legend><?php echo $text_email; ?></legend>
					<input type="text" name="email" id="emailMob">
				</fieldset>						
			</div>
			<div class="col-sm-12">
				<fieldset>
					<legend><?php echo $text_phone; ?></legend>
					 <input type="text" name="phone" id="phoneMob">
				</fieldset>						
			</div>
			<div class="col-sm-12">
				<fieldset>
					<legend><?php echo $text_subject; ?></legend>
					<input type="text" name="subject" id="subjectMob">
				</fieldset>						
			</div>
			<div class="col-sm-12">
				<fieldset>
					  <legend><?php echo $text_mesage; ?></legend>							  
					  <textarea type="text" name="message" id="messageMob" rows="4"></textarea>
				</fieldset>
				
			</div>
			<div class="col-sm-12" style="text-align: center;">
				<button class="btnClass" type="button" name="submit" id="b_button_mobile" value="<?php echo $text_btn_subject; ?>" onclick="need_assistant_mobile()"><?php echo $text_btn_subject; ?></button>
			</div>
			
		</form>
	</div>	
</div>


<script type="text/javascript">
function need_assistant_desktop(val){
	
	var nameDesk = $("#nameDesk").val();
	var emailDesk = $("#emailDesk").val();
	var phoneDesk = $("#phoneDesk").val();
	var subjectDesk = $("#subjectDesk").val();
	var messageDesk = $("#messageDesk").val();
			
	$.ajax({
		type: 'POST',
		dataType: 'json',
		data: { 'name': nameDesk, 'email': emailDesk, 'phone': phoneDesk, 'subject': subjectDesk, 'message': messageDesk },
		url: 'index.php?route=module/newslettersubscribe/needDesktop',
		beforeSend: function () {
			$('#b_button_desktop').button('loading');
		},
		complete: function () {
			$('#b_button_desktop').button('reset');
		},
		success: function (json) {							
			if (json['error_name']) {	
				$('#nameDesk').focus();
				$('#nameDesk').css('border', '1px solid #F44336');				
			} else {
				$('#nameDesk').css('border', '1px solid #69489d');
			}
			
			if (json['error_email']) {	
				$('#emailDesk').focus();
				$('#emailDesk').css('border', '1px solid #F44336');	
			} else {
				$('#emailDesk').css('border', '1px solid #69489d');
			} 
			
			if (json['error_phone']) {	
				$('#phoneDesk').focus();
				$('#phoneDesk').css('border', '1px solid #F44336');			
			} else {
				$('#phoneDesk').css('border', '1px solid #69489d');
			}
			
			if (json['error_subject']) {
				$('#subjectDesk').focus();
				$('#subjectDesk').css('border', '1px solid #F44336');	 			
			} else {
				$('#subjectDesk').css('border', '1px solid #69489d');
			}
			
			if (json['error_message']) {
				$('#messageDesk').focus();
				$('#messageDesk').css('border', '1px solid #F44336');	
			} else {
				$('#messageDesk').css('border', '1px solid #69489d');
			}
			
			if (json['success']) {	
				$('#needAssistantFromDesktop')[0].reset();
				
				$('#needAssistantFromDesktop').html('<div class="alert alert-success" style="margin:5px;"><i class="fa fa-check-circle"></i> ' + json['success'] + '</div>');					
			} 
		}
	});
}
function need_assistant_mobile(val){
	
	var nameMob = $("#nameMob").val();
	var emailMob = $("#emailMob").val();
	var phoneMob = $("#phoneMob").val();
	var subjectMob = $("#subjectMob").val();
	var messageMob = $("#messageMob").val();
			
	$.ajax({
		type: 'POST',
		dataType: 'json',
		data: { 'name': nameMob, 'email': emailMob, 'phone': phoneMob, 'subject': subjectMob, 'message': messageMob },
		url: 'index.php?route=module/newslettersubscribe/needDesktop',
		beforeSend: function () {
			$('#b_button_mobile').button('loading');
		},
		complete: function () {
			$('#b_button_mobile').button('reset');
		},
		success: function (json) {							
			if (json['error_name']) {	
				$('#nameMob').focus();
				$('#nameMob').css('border', '1px solid #F44336');				
			} else {
				$('#nameMob').css('border', '1px solid #69489d');
			}
			
			if (json['error_email']) {	
				$('#emailMob').focus();
				$('#emailMob').css('border', '1px solid #F44336');	
			} else {
				$('#emailMob').css('border', '1px solid #69489d');
			} 
			
			if (json['error_phone']) {	
				$('#phoneMob').focus();
				$('#phoneMob').css('border', '1px solid #F44336');			
			} else {
				$('#phoneMob').css('border', '1px solid #69489d');
			}
			
			if (json['error_subject']) {
				$('#subjectMob').focus();
				$('#subjectMob').css('border', '1px solid #F44336');	 			
			} else {
				$('#subjectMob').css('border', '1px solid #69489d');
			}
			
			if (json['error_message']) {
				$('#messageMob').focus();
				$('#messageMob').css('border', '1px solid #F44336');	
			} else {
				$('#messageMob').css('border', '1px solid #69489d');
			}
			
			if (json['success']) {	
				$('#needAssistantFromMobile')[0].reset();
				
				$('#needAssistantFromMobile').html('<div class="alert alert-success" style="margin:5px;"><i class="fa fa-check-circle"></i> ' + json['success'] + '</div>');					
			} 
		}
	});
}
</script>