<div class="bt-featured bt-box">
<div class="box-heading"><h3><?php echo $heading_title; ?></h3></div>
<div class="box-content">
  <div class="box-product">
  <?php foreach ($products as $product) { ?>
  <div class="box_item">
      <div class="image">
       <div class="btn-quickshop" style="margin-left: -30px; margin-top: -30px; top: 50%; left: 50%;">
                                        
                                        <button title="Quick View" onclick="getModalContent(<?php echo $product['product_id']?>,<?php echo $seller_id?>);" class="sft_quickshop_icon " data-toggle="modal" data-target="#myModal">
                                            <i class="fa fa-eye"></i>
                                        </button>
                                    </div>
      <img src="<?php echo $product['thumb']; ?>" alt="<?php echo $product['name']; ?>" title="<?php echo $product['name']; ?>" class="img-responsive" /></div>
      <div class="prod-detail">
        <div class="name"><?php echo $product['name']; ?></div>
        <?php if ($product['rating']) { ?>
        <div class="rating">
          <?php for ($i = 1; $i <= 5; $i++) { ?>
          <?php if ($product['rating'] < $i) { ?>
          <span class="fa fa-stack"><i class="fa fa-star-o fa-stack-2x"></i></span>
		  <?php } else { ?>
		  <span class="fa fa-stack"><i class="fa fa-star fa-stack-2x"></i></span>
          <?php } ?>
          <?php } ?>
        </div>
        <?php } ?>
        <?php if ($product['price']) { ?>
        <p class="price">
          <?php if (!$product['special']) { ?>
          <?php echo $product['price']; ?>
          <?php } else { ?>
          <span class="price-new"><?php echo $product['special']; ?></span> <span class="price-old"><?php echo $product['price']; ?></span>
          <?php } ?>
          <?php if ($product['tax']) { ?>
          <span class="price-tax"><?php echo $text_tax; ?> <?php echo $product['tax']; ?></span>
          <?php } ?>
        </p>
        <?php } ?>
      </div>
      <div class="button-group">
        <button type="button" onclick="cart.add('<?php echo $product['product_id']; ?>');"><i class="fa fa-shopping-cart"></i> <span class="hidden-xs hidden-sm hidden-md"><?php echo $button_cart; ?></span></button>
        <button type="button" data-toggle="tooltip" title="<?php echo $button_wishlist; ?>" onclick="wishlist.add('<?php echo $product['product_id']; ?>');"><i class="fa fa-heart"></i></button>
        <button type="button" data-toggle="tooltip" title="<?php echo $button_compare; ?>" onclick="compare.add('<?php echo $product['product_id']; ?>');"><i class="fa fa-exchange"></i></button>
      </div>
  </div>
  <?php } ?>
</div>
</div>
</div>
