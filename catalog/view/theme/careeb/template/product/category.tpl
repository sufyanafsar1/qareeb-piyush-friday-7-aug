<?php echo $header; ?>

<?php global $config; ?>
<script>
$(document).ready(function() {
    
	// Product List
	$('#list-view').click(function() {
		$('#content .product-grid > .clearfix').remove();

		//$('#content .product-layout').attr('class', 'product-layout product-list col-xs-12');
		$('#content .row > .product-grid').attr('class', 'productNew product-layout product-list col-xs-12');

		localStorage.setItem('display', 'list');
	});

	// Product Grid
	$('#grid-view').click(function() {
		// What a shame bootstrap does not take into account dynamically loaded columns
		cols = $('#column-right, #column-left').length;

		if (cols == 2) {
			$('#content .product-list').attr('class', 'productNew product-layout product-grid col-lg-6 col-md-6 col-sm-12 col-xs-12');
		} else if (cols == 1) {
			$('#content .product-list').attr('class', 'productNew product-layout product-grid col-lg-4 col-md-4 col-sm-6 col-xs-12');
		} else {
			$('#content .product-list').attr('class', 'productNew product-layout product-grid col-lg-2 col-md-2 col-sm-6 col-xs-12');
		}

		 localStorage.setItem('display', 'grid');
	});

	if (localStorage.getItem('display') == 'list') {
		$('#list-view').trigger('click');
	} else {
		$('#grid-view').trigger('click');
	}

});
</script>
<div class="">
    <div id="content">
        <div id="bulk-load" style="display:none"></div>
        <?php 
		if ($categories) { ?>
            <div class="icon-section text-center">
                <div class="container">
                    <?php foreach ($categories as $category) { ?>
                        <div class="box">
                            <a href="<?php echo $category['href']; ?>"><img src="<?php echo $category['thumb']; ?>" title="<?php echo $category['name']; ?>" alt="<?php echo $category['name']; ?>" />        
                            <h3><?php echo $category['name']; ?></h3></a>
                        </div>
                    <?php } ?>
                </div>
            </div>
        <?php 
		} ?>
		
		<?php /* if ($thumb) { ?>
        <?php //if ($thumb && $category_image) { ?>
            <div data-delay="400" data-animate="fadeInUp" class="block-cate-page fadeInUp animated text-center"><a title="banner category" href="#"><img src="<?php echo $thumb ?>" alt="banner-category"></a></div>
        <?php } */ ?>
		
        <?php 
		if ($column_left != null): ?>
		<div class="col-sm-3"><h3 class='title'><?php echo $text_filter; ?></h3></div>
		<div class='col-sm-9'>
        <?php 
		else: ?>
		<div class="container text-center">
        <?php 
		endif ?> 
			<h3 class="title">
				<span>
					<?php
					if(!empty($_SESSION['storeID'])){
						$cateURL = 'index.php?route=store/store&seller_id='.$_SESSION['storeID'];
					}else{
						$cateURL = 'javascript:void();';
					}
					?>
					<a href="<?= $cateURL; ?>"><?php echo $text_categories ?></a>
					
					<?php 
					for($i=1; $i < count($breadcrumbs)-1; $i++): ?>
						/ <a href="<?php echo $breadcrumbs[$i]['href']; ?>"><?php echo $breadcrumbs[$i]['text']; ?></a>
					<?php 
					endfor ?>
				</span>
				<?php echo $heading_title; ?>
			</h3>
			
			<div class="col-md-4">
				<div class='pull-left'>
					<div class="btn-group hidden-xs">
						<button type="button" id="list-view" class="btn btn-default" data-toggle="tooltip" title="<?php echo $button_list; ?>"><i class="fa fa-th-list"></i></button>
						<button type="button" id="grid-view" class="btn btn-default" data-toggle="tooltip" title="<?php echo $button_grid; ?>"><i class="fa fa-th"></i></button>
					</div>
				</div>
			</div>
			<div class="col-md-8">
				<div class='pull-right'>
					<form class='sortedBy'>
						<select onchange="change_sort();">
							<?php 
							foreach ($sorts as $sorts) { ?>
								<?php 
								if ($sorts['value'] == $sort . '-' . $order) { ?>
									<option value="<?php echo $sorts['href']; ?>" selected="selected"><?php echo $sorts['text']; ?></option>
								<?php 
								} else { ?>
									<option value="<?php echo $sorts['href']; ?>"><?php echo $sorts['text']; ?></option>
								<?php 
								} ?>
							<?php 
							} ?>
						</select>
						<img class='arrow' src="catalog/view/theme/<?php echo $config->get('config_template'); ?>/img/arrowbittom.png">
					</form>
				</div>
			</div>
		</div>
			
		<script type="text/javascript" src="catalog/view/javascript/bossthemes/cloud-zoom/cloud-zoom.1.0.3.js"></script>
		
		<script type="text/javascript" src="catalog/view/javascript/bossthemes/carouFredSel-6.2.1.js"></script>
		
		<script type="text/javascript">
			$(window).load(function() {	   
				boss_quick_shop();
				$('.display').bind('click', function() {
					$('.sft_quickshop_icon').remove();
					boss_quick_shop();
				});	   
				
			});
			
			function boss_quick_shop(){					
				$('#column-left .bt-item-large .product-thumb').each(function(index, value){		
					var id_product='';
					var reloadurl=false;
					
					if($(".image>a",this).attr('id')){
						
						var href_pro = $(".image>a",this).attr('id'); 
					}
					else if($(".image>a",this).attr('href')){
						
						var href_pro = $(".image>a",this).attr('href'); 
					}else{ 
						var href_pro = '';
					}
				   
					if	(href_pro){
						var check=href_pro.match("index.php"); 
					}
					var last_index = '';
					var product_id = 0;
					if(check=="index.php"){	 //direct link
						var str = href_pro.split("&");
						for (var i=0;i<str.length;i++){
							if(str[i].match("product_id=") =="product_id="){					

								last_index = str[i];
								var id = last_index.split("=");
								product_id = id[1];
								break;
							}
						}
						reloadurl=true;
					}else{	//mode SEO
						var check_seo = href_pro.match("product_id=");
						if(check_seo=="product_id="){				

							var str = href_pro.split("&");
							for (var i=0; i<str.length; i++){
								if(str[i].match("product_id=") == "product_id="){
									var temp = str[i].split("?");
									last_index = temp[temp.length-1]; // lay phan tu cuoi cung
									var id = last_index.split("=");
									product_id = id[1];
									break;
								}
							}					
							reloadurl=true;
						}else{				
							var str_1 = href_pro.split("/");
							var str_2 = str_1[str_1.length-1]; 					
							var temp = str_2.split("?");
							last_index = temp[0];
							var id_index = '';							
							if(id_index!=''){
								var id = id_index.split('=');
								product_id = id[1];	
								reloadurl=true;
							}
						}
					}
					
					if(reloadurl){		
						var seller_id = <?php echo $seller_id; ?>;
						
						var _qsHref = '<div class=\"btn-quickshop\" ><button  title =\"Quick View\" onclick=\"getModalContent('+product_id+','+seller_id+');\" class=\"sft_quickshop_icon \" data-toggle=\"modal\" data-target=\"#myModal\"><i class="fa fa-eye"></i></button></div>';			
						$('.image',this).prepend(_qsHref);					
						
						var quick_button = $('.btn-quickshop');								
						var width_button = (quick_button.width())/2 ;
						var height_button = (quick_button.height())/2;				
						var w_image = $('.image').width();
						var w_qs = quick_button.width();
						
						$('.btn-quickshop').css({
							'margin-left': - width_button + 'px',
							'margin-top': - height_button +'px',
							'top': '50%',
							'left': '50%'
						});		
							
					}
				});			
				var content_modal = '<div id="myModal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;"></div><div class="bt-qs-loading" style="position:fixed;top:50%;left:50%"></div>';
				$('#bt_container').append(content_modal);					
			}

			function getModalContent(product_id,seller_id, optiontype){		

				if(optiontype != ''){
					optiontype = optiontype;
				} else {
					optiontype = 1;
				}
				
				$.ajax({
					url: 'index.php?route=module/boss_quick_shop_product/&product_id=' + product_id+'&seller_id=' + seller_id+'&optiontype='+optiontype,
					dataType: 'json',
					beforeSend: function() {			
						$('.loading').html('<span class="wait">&nbsp;<img src="catalog/view/theme/default/image/loading.gif" alt="" /></span>');
						$('#myModal').html('');
					},		
					complete: function() {
						$('.wait').remove();
					},
					success: function(json) {
								
						$('#myModal').html(json['html']);
						$('#myModal > .modal-dialog').css({
							'width': '95%',
							'max-width': '900px',
						});
						$('#myModal').show();
					   populateRelatedProducts();
					}
				});
			}

			function populateRelatedProducts()
			{
					$('#product_related_qView').carouFredSel({
					auto: false,
					responsive: true,
					width: '100%',
					prev: '#prev_related_qView',
					next: '#next_related_qView',
					swipe: {
					onTouch : true
					},
					items: {
						width: 280,
						height: 'auto',
						visible: {
						min: 6,
						max: 6
						}
					},
					scroll: {
						direction : 'left',    //  The direction of the transition.
						duration  : 1000   //  The duration of the transition.
					}
				});

			}
		</script> 
		
        <?php //echo $content_top; ?>
		
        <?php if ($column_left != null): ?>
            <?php echo $column_left; ?>
            <div class="col-sm-9 height">
        <?php else: ?>
			<?php if($_REQUEST['type'] == 'list'){ ?>
			<div class="container">
			<?php } else { ?>
            <div class="container text-center">
			<?php } ?>
        <?php endif ?>
				
            <div class="text-center">
                <div class="products row StoreDiv" id="newPro">

                    <?php if ($products) {  ?>
                        <?php foreach ($products as $product) {?>
                       
                            <div class="productNew product-layout product-list col-xs-12" style="<?php if($language_id == 3){echo "float: right";}?>" id="<?php echo $product['product_id']; ?>">
                                <div class="image">
                             <?php  $existInList = false;
								
									if(isset($_SESSION['default']['wishlist'])){
										
										foreach ($_SESSION['default']['wishlist'] as $key => $value) {
											if ($value[0] == $product['product_id'] && $value[2] == $seller_id) {
										  
												$existInList = true;
											}
										}
									}
									
									if ($existInList) {
										unset($this->session->data['wishlist'][$index]);
									}
									if($existInList) { ?>
                                                <div class="heart-box-pink" id="heart<?php echo $product['product_id']; ?>">
                                                  <div class="heart-label">
                                                    <a class="wishlist1" onclick="wishlist_remove('<?php echo $product['product_id']; ?>',<?php echo $seller_id?>); changeHeart('<?php echo $product['product_id']; ?>',<?php echo $seller_id?>)">
                                                      <i class="fa fa-heart" style="color:#E8114A"></i>
                                                    </a>
                                                  </div>
                                                </div>
                                            <?php } else{ ?>
                                                <div class="heart-box" id="heart<?php echo $product['product_id']; ?>">
                                                  <div class="heart-label">
                                                     <a class="wishlist1" onclick="wishlist_cat_add('<?php echo $product['product_id']; ?>',<?php echo $seller_id?>)"><i class="fa fa-heart"></i></a>
                                                  </div>
                                                </div>
                                 	<?php } ?>
									
									<?php
									if($product['discount'] != 0 ){
										//is_int($product['discount']) or is_float($product['discount'])
										if($product['discount'] != 0){
											echo '<div class="discount-tag"> <p class="discount-p">'.$product['discount'].'%</p> </div>';
										}
										
									}
									?>
									
                                    <div class="btn-quickshop" style="margin-left: -30px; margin-top: -30px; top: 50%; left: 50%;">
                                        
                                        <button title="Quick View" onclick="getModalContent(<?php echo $product['product_id']?>,<?php echo $seller_id?>, '1');" class="sft_quickshop_icon " data-toggle="modal" data-target="#myModal">
                                            <i class="fa fa-eye"></i>
                                        </button>
										
                                    </div>
                                    <a id="<?php echo $product['href']; ?>" href="javascript:void(0)">
                                        <img class="img-responsive" src="<?php echo $product['thumb']; ?>" alt="<?php echo properName($product['name']); ?>" title="<?php echo properName($product['name']); ?>">
                                    </a>
                                </div>
                                <h3><?php echo getItemName($product['name'],30); ?></h3>
								<div class="priceBox">
                                <?php if ($product['price']):                                   
									if (!$product['special']) { ?>
                                      <p><?php echo $product['price']; ?></p>
                                      <?php } else { ?>
                                        <p><?php echo $product['special']; ?> </p>
										<p class='oldPrice'><?php echo $product['price']; ?></p>
                                      <?php } 
                                      if ($product['tax']) { ?>
                                        <p class='oldPrice'><?php echo $text_tax; ?>   <p><?php echo $product['tax']; ?></p></p>
                                    <?php } 
								endif ?>
								</div>
                                <div id="pro_div_id_<?php echo $product['product_id']?>">
                                    <?php  if (count($data['product_data']['cart_data'])):
                                        $count = 0;
                                          foreach ($data['product_data']['cart_data'] as $key => $value): 
                                            if (($value['product_id'] == $product['product_id']) && ($value['seller_id'] == $seller_id)):
                                              $count = 1; 
                                              $quantity = $value['quantity'];
                                              $cart_id = $value['cart_id'];
                                              break;
                                            endif;
                                          endforeach;
                                          if ($count == 1): ?>
                                            <div style='position:relative'>
                                              <a href="javascript:void(0)"><input  onchange="update_cart_data_home('<?php echo $cart_id; ?>', '<?php echo 'select-number'.$cart_id;?>');" id="select-number<?php echo $cart_id; ?>" disabled value="<?php echo $quantity; ?>"/></a>
                                              <a style='position:absolute;left: 40px;top: 24px;color: white' onclick="changeQty('<?php echo 'select-number'.$cart_id;?>',0); return false;" class="decrease" href="javascript:void(0)">_</a>
                                              <a style='position:absolute;right: 40px;top: 31px;color: white' onclick="changeQty('<?php echo 'select-number'.$cart_id;?>',1); return false;" class="increase" href="javascript:void(0)">+</a>
                                            </div>
                                          <?php else: ?>
										  <?php 
                                            if($product['options']){ ?>
										
											<a href="javascript:void(0)" id="custom" onclick="getModalContent(<?php echo $product['product_id']?>,<?php echo $seller_id?>, '1');" data-toggle="modal" data-target="#myModal"><button><?php echo $button_cart; ?></button></a>
											
											<?php } else { ?>
												
											<a href="javascript:void(0)" id="custom" onclick="btadd.cart('<?php echo $product['product_id']; ?>', '<?php echo $product['minimum']; ?>','<?php echo $seller_id; ?>');"><button><?php echo $button_cart; ?></button></a> 
											
											<?php } ?> 
                                        <?php endif ?>
                                    <?php else: ?>
										<?php if($product['options']){ ?>
										
										<a href="javascript:void(0)" id="custom" onclick="getModalContent(<?php echo $product['product_id']?>,<?php echo $seller_id?>, '1');" data-toggle="modal" data-target="#myModal"><button><?php echo $button_cart; ?></button></a>
										
										<?php } else { ?>
											
                                        <a href="javascript:void(0)" id="custom" onclick="btadd.cart('<?php echo $product['product_id']; ?>', '<?php echo $product['minimum']; ?>','<?php echo $seller_id; ?>');"><button><?php echo $button_cart; ?></button></a> 
										
										<?php } ?>
                                       
                                    <?php endif ?>
                                </div>
                            </div>
                        <?php } ?>

                        <div id="loadingDiv" style="display:none">
							<img src="image/loader.gif">
						</div>

                   <?php } ?>
                </div>
            </div>
            <?php if (!$categories && !$products) { ?>
                <div class="content_bg" style="background: #fff">
                    <p><?php echo $text_empty; ?></p>
                </div>
            <?php }
            //echo $content_bottom; ?>
        </div>

        <script type="text/javascript">

        function getUrlParameter(sParam) {
            var sPageURL = window.location.search.substring(1),
                sURLVariables = sPageURL.split('&'),
                sParameterName,
                i;
        
            for (i = 0; i < sURLVariables.length; i++) {
                sParameterName = sURLVariables[i].split('=');
            
                if (sParameterName[0] === sParam) {
                    return sParameterName[1] === undefined ? true : decodeURIComponent(sParameterName[1]);
                }
            }
        };

            window.addEventListener("load", function(){
            	checkScroll = false;
            	page = 2;
            	limit = 12;
				productload =0;
                path = getUrlParameter('path');
                seller_id = path = getUrlParameter('seller_id');
            	$(window).on('beforeunload',function() {
					if(productload == 0){
						if(!checkScroll){
							checkScroll = true;
							$.ajax({
								url: 'index.php?route=product/categoryproduct',
								type: 'GET',
								data: {"page":page,"limit":limit,"path":'<?php echo $path; ?>',"seller_id":<?php echo $seller_id; ?>},
								dataType: 'html',
								beforeSend: function(){
									$("#loadingDiv").show();
								},
								success: function (html) {
									if(html == ''){
										productload =1;
									}
									if (!$.trim(html)){   
										productload =1;
										$('#loadingDiv').html('');
									}
																
									if(html != 'fail'){
										page = page + 1;
										$('#loadingDiv').before(html);
										$("#loadingDiv").hide();
									}else{
										$(window).unbind('beforeunload');
										$("#loadingDiv").hide();
									}
								},
								error: function (xhr, ajaxOptions, thrownError) {
									alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
								},
								complete: function(){
									checkScroll = false;
								}
							});
						}
					}
                
            	});
				
				$(window).on('wheel', function(event){

					// deltaY obviously records vertical scroll, deltaX and deltaZ exist too
					if(event.originalEvent.deltaY < 0){
						// wheeled up
					}
					else {
						if(productload == 0){
							if(!checkScroll){
								checkScroll = true;
								$.ajax({
									url: 'index.php?route=product/categoryproduct',
									type: 'GET',
									data: {"page":page,"limit":limit,"path":'<?php echo $path; ?>',"seller_id":<?php echo $seller_id; ?>},
									dataType: 'html',
									beforeSend: function(){
										$("#loadingDiv").show();
									},
									success: function (html) {
										if(html == ''){
											productload =1;
										}
										if (!$.trim(html)){  
											productload =1;									
											$('#loadingDiv').html('');
										}
																	
										if(html != 'fail'){
											page = page + 1;
											$('#loadingDiv').before(html);
											$("#loadingDiv").hide();
										}else{
											$(window).unbind('scroll');
											$("#loadingDiv").hide();
										}
									},
									error: function (xhr, ajaxOptions, thrownError) {
										alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
									},
									complete: function(){
										checkScroll = false;
									}
								});
							}
						}
					}
				});
				
				$('body').on({
					'touchmove': function(e) { 
						if(!checkScroll){
							checkScroll = true;
							$.ajax({
								url: 'index.php?route=product/categoryproduct',
								type: 'GET',
								data: {"page":page,"limit":limit,"path":'<?php echo $path; ?>',"seller_id":<?php echo $seller_id; ?>},
								dataType: 'html',
								beforeSend: function(){
									$("#loadingDiv").show();
								},
								success: function (html) {
									if (!$.trim(html)){   
										$('#loadingDiv').html('');
									}
																
									if(html != 'fail'){
										page = page + 1;
										$('#loadingDiv').before(html);
										$("#loadingDiv").hide();
									}else{
										$(window).unbind('scroll');
										$("#loadingDiv").hide();
									}
								},
								error: function (xhr, ajaxOptions, thrownError) {
									alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
								},
								complete: function(){
									checkScroll = false;
								}
							});
						}	
					}
				});	
            }); 

            function change_sort() {
                var url = '<?php echo $current_url; ?>&' + $('.sortedBy select').val();
                $.ajax({
                    type: 'GET',
                    url: url,
                    data: {},
                    beforeSend: function( xhr ) {
                        $("#bulk-load").show();
                    },
                    complete: function (data) {
                        $('#content').html($("#content", data.responseText).html());
                        $("#bulk-load").hide();
                    }
                });   
            }
        </script>
    </div>
</div>

<?php echo $footer; ?>