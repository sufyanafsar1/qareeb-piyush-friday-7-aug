<?php echo $header; ?>

<?php global $config; ?>

<div class="">
    <div id="content">
        <div id="bulk-load" style="display:none"></div>
        <?php if ($categories) { ?>
            <div class="icon-section text-center">
                <div class="container">
                    <?php foreach ($categories as $category) { ?>
                        <div class="box">
                            <img src="<?php echo $category['thumb']; ?>" title="<?php echo $category['name']; ?>" alt="<?php echo $category['name']; ?>" />        
                            <h3><a href="<?php echo $category['href']; ?>"><?php echo $category['name']; ?></a></h3>
                        </div>
                    <?php } ?>
                </div>
            </div>
        <?php } ?>
		<?php /* if ($thumb) { ?>
        <?php //if ($thumb && $category_image) { ?>
            <div data-delay="400" data-animate="fadeInUp" class="block-cate-page fadeInUp animated text-center"><a title="banner category" href="#"><img src="<?php echo $thumb ?>" alt="banner-category"></a></div>
        <?php } */ ?>
        <?php if ($column_left != null): ?>

            <div class="col-sm-3"><h3 class='title'><?php echo $text_filter; ?></h3></div>

            <div class='col-sm-9'>
        <?php else: ?>
            <div class="container text-center">
        <?php endif ?>             

                <h3 class="title"><span>
                    <?php
                        if(!empty($_SESSION['storeID'])){
                            $cateURL = 'index.php?route=store/store&seller_id='.$_SESSION['storeID'];
                        }else{
                            $cateURL = 'javascript:void();';
                        }
                        ?>
                        <a href="<?= $cateURL; ?>">
                    <?php echo $text_categories ?>
                        </a>
                <?php for($i=1; $i < count($breadcrumbs)-1; $i++): ?>
                    / <a href="<?php echo $breadcrumbs[$i]['href']; ?>"><?php echo $breadcrumbs[$i]['text']; ?></a>
                <?php endfor ?>
                </span><?php echo $heading_title; ?></h3>
                <div class='pull-right'>
                  <form class='sortedBy'>
                    <select onchange="change_sort();">

                      <?php foreach ($sorts as $sorts) { ?>
                    <?php if ($sorts['value'] == $sort . '-' . $order) { ?>
                    <option value="<?php echo $sorts['href']; ?>" selected="selected"><?php echo $sorts['text']; ?></option>
                    <?php } else { ?>
                    <option value="<?php echo $sorts['href']; ?>"><?php echo $sorts['text']; ?></option>
                    <?php } ?>
                    <?php } ?>
                    </select>
                    <img class='arrow' src="catalog/view/theme/<?php echo $config->get('config_template'); ?>/img/arrowbittom.png">
                  </form>
                </div>
            </div>
        <?php echo $content_top; ?>
        <?php if ($column_left != null): ?>
            <?php echo $column_left; ?>
            <div class="col-sm-9 height">
        <?php else: ?>
            <div class="container text-center">
        <?php endif ?>
            <div class="text-center">
                <div class="products">
                    <?php if ($products) {  ?>
                        <?php foreach ($products as $product) {?>
                       
                            <div class="product" id="<?php echo $product['product_id']; ?>">
                                <div class="image">
                             <?php  $existInList = false;
								
									if(isset($_SESSION['default']['wishlist'])){
										
										foreach ($_SESSION['default']['wishlist'] as $key => $value) {
											if ($value[0] == $product['product_id'] && $value[2] == $seller_id) {
										  
												$existInList = true;
											}
										}
									}
									
									if ($existInList) {
										unset($this->session->data['wishlist'][$index]);
									}
									if($existInList) { ?>
                                                <div class="heart-box-pink" id="heart<?php echo $product['product_id']; ?>">
                                                  <div class="heart-label">
                                                    <a class="wishlist1" onclick="wishlist_remove('<?php echo $product['product_id']; ?>',<?php echo $seller_id?>); changeHeart('<?php echo $product['product_id']; ?>',<?php echo $seller_id?>)">
                                                      <i class="fa fa-heart" style="color:#E8114A"></i>
                                                    </a>
                                                  </div>
                                                </div>
                                            <?php } else{ ?>
                                                <div class="heart-box" id="heart<?php echo $product['product_id']; ?>">
                                                  <div class="heart-label">
                                                     <a class="wishlist1" onclick="wishlist_cat_add('<?php echo $product['product_id']; ?>',<?php echo $seller_id?>)"><i class="fa fa-heart"></i></a>
                                                  </div>
                                                </div>
                                 	<?php } ?>
                                    <div class="btn-quickshop" style="margin-left: -30px; margin-top: -30px; top: 50%; left: 50%;">
                                        
                                        <button title="Quick View" onclick="getModalContent(<?php echo $product['product_id']?>,<?php echo $seller_id?>, '1');" class="sft_quickshop_icon " data-toggle="modal" data-target="#myModal">
                                            <i class="fa fa-eye"></i>
                                        </button>
										
                                    </div>
                                    <a id="<?php echo $product['href']; ?>" href="javascript:void(0)">
                                        <img class="img-responsive" src="<?php echo $product['thumb']; ?>" alt="<?php echo properName($product['name']); ?>" title="<?php echo properName($product['name']); ?>">
                                    </a>
                                </div>
                                <h3><?php echo getItemName($product['name'],30); ?></h3>
								<div class="priceBox">
                                <?php if ($product['price']):                                   
									if (!$product['special']) { ?>
                                      <p><?php echo $product['price']; ?></p>
                                      <?php } else { ?>
                                        <p><?php echo $product['special']; ?> </p>
										<p class='oldPrice'><?php echo $product['price']; ?></p>
                                      <?php } 
                                      if ($product['tax']) { ?>
                                        <p class='oldPrice'><?php echo $text_tax; ?>   <p><?php echo $product['tax']; ?></p></p>
                                    <?php } 
								endif ?>
								</div>
                                <div id="pro_div_id_<?php echo $product['product_id']?>">
                                    <?php  if (count($data['product_data']['cart_data'])):
                                        $count = 0;
                                          foreach ($data['product_data']['cart_data'] as $key => $value): 
                                            if (($value['product_id'] == $product['product_id']) && ($value['seller_id'] == $seller_id)):
                                              $count = 1; 
                                              $quantity = $value['quantity'];
                                              $cart_id = $value['cart_id'];
                                              break;
                                            endif;
                                          endforeach;
                                          if ($count == 1): ?>
                                            <div style='position:relative'>
                                              <a href="javascript:void(0)"><input  onchange="update_cart_data_home('<?php echo $cart_id; ?>', '<?php echo 'select-number'.$cart_id;?>');" id="select-number<?php echo $cart_id; ?>" disabled value="<?php echo $quantity; ?>"/></a>
                                              <a style='position:absolute;left: 40px;top: 24px;color: white' onclick="changeQty('<?php echo 'select-number'.$cart_id;?>',0); return false;" class="decrease" href="javascript:void(0)">_</a>
                                              <a style='position:absolute;right: 40px;top: 31px;color: white' onclick="changeQty('<?php echo 'select-number'.$cart_id;?>',1); return false;" class="increase" href="javascript:void(0)">+</a>
                                            </div>
                                          <?php else: ?>
										  <?php 
                                            if($product['options']){ ?>
										
											<a href="javascript:void(0)" id="custom" onclick="getModalContent(<?php echo $product['product_id']?>,<?php echo $seller_id?>, '0');" data-toggle="modal" data-target="#myModal"><button><?php echo $button_cart; ?></button></a>
											
											<?php } else { ?>
												
											<a href="javascript:void(0)" id="custom" onclick="btadd.cart('<?php echo $product['product_id']; ?>', '<?php echo $product['minimum']; ?>','<?php echo $seller_id; ?>');"><button><?php echo $button_cart; ?></button></a> 
											
											<?php } ?> 
                                        <?php endif ?>
                                    <?php else: ?>
										<?php if($product['options']){ ?>
										
										<a href="javascript:void(0)" id="custom" onclick="getModalContent(<?php echo $product['product_id']?>,<?php echo $seller_id?>, '0');" data-toggle="modal" data-target="#myModal"><button><?php echo $button_cart; ?></button></a>
										
										<?php } else { ?>
											
                                        <a href="javascript:void(0)" id="custom" onclick="btadd.cart('<?php echo $product['product_id']; ?>', '<?php echo $product['minimum']; ?>','<?php echo $seller_id; ?>');"><button><?php echo $button_cart; ?></button></a> 
										
										<?php } ?>
                                       
                                    <?php endif ?>
                                </div>
                            </div>
                        <?php } ?>
                   <?php } ?>
                </div>
            </div>
            <?php if (!$categories && !$products) { ?>
                <div class="content_bg" style="background: #fff">
                    <p><?php echo $text_empty; ?></p>
                </div>
            <?php }
            echo $content_bottom; ?>
        </div>

        <script type="text/javascript">
            function change_sort() {
                var url = '<?php echo $current_url; ?>&' + $('.sortedBy select').val();
                $.ajax({
                    type: 'GET',
                    url: url,
                    data: {},
                    beforeSend: function( xhr ) {
                        $("#bulk-load").show();
                    },
                    complete: function (data) {
                        $('#content').html($("#content", data.responseText).html());
                        $("#bulk-load").hide();
                    }
                });   
            }
        </script>
    </div>
</div>

<?php echo $footer; ?>