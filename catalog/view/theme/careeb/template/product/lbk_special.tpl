<?php echo $header; ?>

<?php global $config; ?>

<div class="row">
    <div id="content" class="col-sm-12 StoreDiv">
        <div id="bulk-load" style="display:none"></div>

		<div class="container text-center">

			<h3 class="title pull-left"><?php echo $heading_title; ?></h3>
			
			<?php if ($products) {  ?>
			
				<div class='pull-right'>
					<form class='sortedBy'>
						<select onchange="change_sort();">

						  <?php foreach ($sorts as $sorts) { ?>
						<?php if ($sorts['value'] == $sort . '-' . $order) { ?>
						<option value="<?php echo $sorts['href']; ?>" selected="selected"><?php echo $sorts['text']; ?></option>
						<?php } else { ?>
						<option value="<?php echo $sorts['href']; ?>"><?php echo $sorts['text']; ?></option>
						<?php } ?>
						<?php } ?>
						</select>
						<img class='arrow' src="catalog/view/theme/<?php echo $config->get('config_template'); ?>/img/arrowbittom.png">
					  </form>
				</div>
			<?php }  ?>
		</div>
		
		<script type="text/javascript" src="catalog/view/javascript/bossthemes/cloud-zoom/cloud-zoom.1.0.3.js"></script>
		
		<script type="text/javascript" src="catalog/view/javascript/bossthemes/carouFredSel-6.2.1.js"></script>
		
		<script type="text/javascript">
			$(window).load(function() {	   
				boss_quick_shop();
				$('.display').bind('click', function() {
					$('.sft_quickshop_icon').remove();
					boss_quick_shop();
				});	   
				
			});
			
			function boss_quick_shop(){					
				$('#column-left .bt-item-large .product-thumb').each(function(index, value){		
					var id_product='';
					var reloadurl=false;
					
					if($(".image>a",this).attr('id')){
						
						var href_pro = $(".image>a",this).attr('id'); 
					}
					else if($(".image>a",this).attr('href')){
						
						var href_pro = $(".image>a",this).attr('href'); 
					}else{ 
						var href_pro = '';
					}
				   
					if	(href_pro){
						var check=href_pro.match("index.php"); 
					}
					var last_index = '';
					var product_id = 0;
					if(check=="index.php"){	 //direct link
						var str = href_pro.split("&");
						for (var i=0;i<str.length;i++){
							if(str[i].match("product_id=") =="product_id="){					

								last_index = str[i];
								var id = last_index.split("=");
								product_id = id[1];
								break;
							}
						}
						reloadurl=true;
					}else{	//mode SEO
						var check_seo = href_pro.match("product_id=");
						if(check_seo=="product_id="){				

							var str = href_pro.split("&");
							for (var i=0; i<str.length; i++){
								if(str[i].match("product_id=") == "product_id="){
									var temp = str[i].split("?");
									last_index = temp[temp.length-1]; // lay phan tu cuoi cung
									var id = last_index.split("=");
									product_id = id[1];
									break;
								}
							}					
							reloadurl=true;
						}else{				
							var str_1 = href_pro.split("/");
							var str_2 = str_1[str_1.length-1]; 					
							var temp = str_2.split("?");
							last_index = temp[0];
							var id_index = '';							
							if(id_index!=''){
								var id = id_index.split('=');
								product_id = id[1];	
								reloadurl=true;
							}
						}
					}
					
					if(reloadurl){		
						var seller_id = <?php echo $seller_id; ?>;
						
						var _qsHref = '<div class=\"btn-quickshop\" ><button  title =\"Quick View\" onclick=\"getModalContent('+product_id+','+seller_id+');\" class=\"sft_quickshop_icon \" data-toggle=\"modal\" data-target=\"#myModal\"><i class="fa fa-eye"></i></button></div>';			
						$('.image',this).prepend(_qsHref);					
						
						var quick_button = $('.btn-quickshop');								
						var width_button = (quick_button.width())/2 ;
						var height_button = (quick_button.height())/2;				
						var w_image = $('.image').width();
						var w_qs = quick_button.width();
						
						$('.btn-quickshop').css({
							'margin-left': - width_button + 'px',
							'margin-top': - height_button +'px',
							'top': '50%',
							'left': '50%'
						});		
							
					}
				});			
				var content_modal = '<div id="myModal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;"></div><div class="bt-qs-loading" style="position:fixed;top:50%;left:50%"></div>';
				$('#bt_container').append(content_modal);					
			}

			function getModalContent(product_id,seller_id, optiontype){		

				if(optiontype != ''){
					optiontype = optiontype;
				} else {
					optiontype = 1;
				}
				
				$.ajax({
					url: 'index.php?route=module/boss_quick_shop_product/&product_id=' + product_id+'&seller_id=' + seller_id+'&optiontype='+optiontype,
					dataType: 'json',
					beforeSend: function() {			
						$('.loading').html('<span class="wait">&nbsp;<img src="catalog/view/theme/default/image/loading.gif" alt="" /></span>');
						$('#myModal').html('');
					},		
					complete: function() {
						$('.wait').remove();
					},
					success: function(json) {
								
						$('#myModal').html(json['html']);
						$('#myModal > .modal-dialog').css({
							'width': '95%',
							'max-width': '900px',
						});
						$('#myModal').show();
					   populateRelatedProducts();
					}
				});
			}

			function populateRelatedProducts()
			{
					$('#product_related_qView').carouFredSel({
					auto: false,
					responsive: true,
					width: '100%',
					prev: '#prev_related_qView',
					next: '#next_related_qView',
					swipe: {
					onTouch : true
					},
					items: {
						width: 280,
						height: 'auto',
						visible: {
						min: 6,
						max: 6
						}
					},
					scroll: {
						direction : 'left',    //  The direction of the transition.
						duration  : 1000   //  The duration of the transition.
					}
				});

			}
		</script>
		
        <?php //echo $content_top; ?>
        <?php if ($column_left != null): ?>
            <?php echo $column_left; ?>
            <div class="col-sm-9 height">
        <?php else: ?>
            <div class="container text-center">
        <?php endif ?>
            <div class="text-center">
                <div class="products">					
					<div class="row">
                    <?php 
					if ($products) {  ?>
                        <?php 
						foreach ($products as $product) {?>                       
                            <div class="product" id="<?php echo $product['product_id']; ?>">
                                <div class="image">
                             <?php  $existInList = false;
								if(isset($_SESSION['default']['wishlist'])){
                                  foreach ($_SESSION['default']['wishlist'] as $key => $value) {
                                    if ($value[0] == $product['product_id'] && $value[2] == $seller_id) {
                                      
                                      $existInList = true;
                                    }
                                  }
								}
                                  if ($existInList) {
                                    unset($this->session->data['wishlist'][$index]);
                                  }
                                  if($existInList) { ?>
                                                <div class="heart-box-pink" id="heart<?php echo $product['product_id']; ?>">
                                                  <div class="heart-label">
                                                    <a class="wishlist1" onclick="wishlist_remove('<?php echo $product['product_id']; ?>',<?php echo $seller_id?>); changeHeart('<?php echo $product['product_id']; ?>',<?php echo $seller_id?>)">
                                                      <i class="fa fa-heart" style="color:#E8114A"></i>
                                                    </a>
                                                  </div>
                                                </div>
                                            <?php } else{ ?>
                                                <div class="heart-box" id="heart<?php echo $product['product_id']; ?>">
                                                  <div class="heart-label">
                                                     <a class="wishlist1" onclick="wishlist_cat_add('<?php echo $product['product_id']; ?>',<?php echo $seller_id?>)"><i class="fa fa-heart"></i></a>
                                                  </div>
                                                </div>
                                 	<?php } ?>
                                    <div class="btn-quickshop" style="margin-left: -30px; margin-top: -30px; top: 50%; left: 50%;">
                                        
                                        <button title="Quick View" onclick="getModalContent(<?php echo $product['product_id']?>,<?php echo $seller_id?>);" class="sft_quickshop_icon " data-toggle="modal" data-target="#myModal">
                                            <i class="fa fa-eye"></i>
                                        </button>
                                    </div>
                                    <a id="<?php echo $product['href']; ?>" href="javascript:void(0)">
                                        <img class="img-responsive" src="<?php echo $product['thumb']; ?>" alt="<?php echo properName($product['name']); ?>" title="<?php echo properName($product['name']); ?>">
                                    </a>
                                </div>
                                <h3><?php echo getItemName($product['name'],30); ?></h3>
                                <?php if ($product['price']):
                                    if (!$product['special']) { ?>
                                      <p><?php echo $product['price']; ?></p>
                                      <?php } else { ?>
                                        <p><?php echo $product['special']; ?>   <p class='oldPrice'><?php echo $product['price']; ?></p></p>
                                      <?php } 
                                      if ($product['tax']) { ?>
                                        <p class='oldPrice'><?php echo $text_tax; ?>   <p><?php echo $product['tax']; ?></p></p>
                                    <?php } 
                                endif ?>
                                <div id="pro_div_id_<?php echo $product['product_id']?>">
                                    <?php  if (count($data['product_data']['cart_data'])):
                                        $count = 0;
                                          foreach ($data['product_data']['cart_data'] as $key => $value): 
                                            if (($value['product_id'] == $product['product_id']) && ($value['seller_id'] == $seller_id)):
                                              $count = 1; 
                                              $quantity = $value['quantity'];
                                              $cart_id = $value['cart_id'];
                                              break;
                                            endif;
                                          endforeach;
                                          if ($count == 1): ?>
                                            <div style='position:relative'>
                                              <a href="javascript:void(0)"><input  onchange="update_cart_data_home('<?php echo $cart_id; ?>', '<?php echo 'select-number'.$cart_id;?>');" id="select-number<?php echo $cart_id; ?>" disabled value="<?php echo $quantity; ?>"/></a>
                                              <a style='position:absolute;left: 40px;top: 24px;color: white' onclick="changeQty('<?php echo 'select-number'.$cart_id;?>',0); return false;" class="decrease" href="javascript:void(0)">_</a>
                                              <a style='position:absolute;right: 40px;top: 31px;color: white' onclick="changeQty('<?php echo 'select-number'.$cart_id;?>',1); return false;" class="increase" href="javascript:void(0)">+</a>
                                            </div>
                                          <?php else: ?>
                                            <a href="javascript:void(0)" id="custom" onclick="btadd.cart('<?php echo $product['product_id']; ?>', '<?php echo $product['minimum']; ?>','<?php echo $seller_id; ?>');"><button><?php echo $button_cart; ?></button></a>  
                                        <?php endif ?>
                                    <?php else: ?>
                                        <a href="javascript:void(0)" id="custom" onclick="btadd.cart('<?php echo $product['product_id']; ?>', '<?php echo $product['minimum']; ?>','<?php echo $seller_id; ?>');"><button><?php echo $button_cart; ?></button></a> 
                                    <?php endif ?>
                                </div>
                            </div>
                        <?php 
						} ?>
						</div>
						<div class="row">
							<div class="col-sm-6 text-left"><?php echo $pagination; ?></div>
							<div class="col-sm-6 text-right"><?php echo $results; ?></div>
						</div>
					<?php 
					} else { ?>
						<div class="content_bg" style="background: #fff">
							<h4><?php echo $text_empty; ?></h4>
						 </div>
					<?php 
					} ?>               
                </div>
            </div>
                          
            <?php  echo $content_bottom; ?>
        </div>

        <script type="text/javascript">
            function change_sort() {
                var url = $('.sortedBy select').val();
                $.ajax({
                    type: 'GET',
                    url: url,
                    data: {},
                    beforeSend: function( xhr ) {
                        $("#bulk-load").show();
                    },
                    complete: function (data) {
                        $('#content').html($("#content", data.responseText).html());
                        $("#bulk-load").hide();
                    }
                });   
            }
        </script>
    </div>
</div>

<?php echo $footer; ?>