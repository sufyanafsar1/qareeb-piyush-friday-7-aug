<?php echo $header; ?>
<?php global $config; ?>

<div class="bt-breadcrumb">
	<div class="container">
		<ul class="breadcrumb breadcrumb-arrow">
			<?php foreach ($breadcrumbs as $breadcrumb) { ?>
				<li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
			<?php } ?>
		</ul>
	</div>
</div>

<div class="container container-search">
	<div class="row">
		<?php echo $column_left; ?>
		<?php echo $column_right; ?>
		<?php if ($column_left && $column_right) { ?>
			<?php $class = 'col-sm-6'; ?>
		<?php } elseif ($column_left || $column_right) { ?>
			<?php $class = 'col-sm-9'; ?>
		<?php } else { ?>
			<?php $class = 'col-sm-12'; ?>
		<?php } ?>
	
		<div id="content" class="<?php echo $class; ?>">
		
			<script type="text/javascript" src="catalog/view/javascript/bossthemes/cloud-zoom/cloud-zoom.1.0.3.js"></script>
		
			<script type="text/javascript" src="catalog/view/javascript/bossthemes/carouFredSel-6.2.1.js"></script>
		
			<script type="text/javascript">
				$(window).load(function() {	   
					boss_quick_shop();
					$('.display').bind('click', function() {
						$('.sft_quickshop_icon').remove();
						boss_quick_shop();
					});	   
					
				});
				
				function boss_quick_shop(){					
					$('#column-left .bt-item-large .product-thumb').each(function(index, value){		
						var id_product='';
						var reloadurl=false;
						
						if($(".image>a",this).attr('id')){
							
							var href_pro = $(".image>a",this).attr('id'); 
						}
						else if($(".image>a",this).attr('href')){
							
							var href_pro = $(".image>a",this).attr('href'); 
						}else{ 
							var href_pro = '';
						}
					   
						if	(href_pro){
							var check=href_pro.match("index.php"); 
						}
						var last_index = '';
						var product_id = 0;
						if(check=="index.php"){	 //direct link
							var str = href_pro.split("&");
							for (var i=0;i<str.length;i++){
								if(str[i].match("product_id=") =="product_id="){					

									last_index = str[i];
									var id = last_index.split("=");
									product_id = id[1];
									break;
								}
							}
							reloadurl=true;
						}else{	//mode SEO
							var check_seo = href_pro.match("product_id=");
							if(check_seo=="product_id="){				

								var str = href_pro.split("&");
								for (var i=0; i<str.length; i++){
									if(str[i].match("product_id=") == "product_id="){
										var temp = str[i].split("?");
										last_index = temp[temp.length-1]; // lay phan tu cuoi cung
										var id = last_index.split("=");
										product_id = id[1];
										break;
									}
								}					
								reloadurl=true;
							}else{				
								var str_1 = href_pro.split("/");
								var str_2 = str_1[str_1.length-1]; 					
								var temp = str_2.split("?");
								last_index = temp[0];
								var id_index = '';							
								if(id_index!=''){
									var id = id_index.split('=');
									product_id = id[1];	
									reloadurl=true;
								}
							}
						}
						
						if(reloadurl){		
							var seller_id = <?php echo $seller_id; ?>;
							
							var _qsHref = '<div class=\"btn-quickshop\" ><button  title =\"Quick View\" onclick=\"getModalContent('+product_id+','+seller_id+');\" class=\"sft_quickshop_icon \" data-toggle=\"modal\" data-target=\"#myModal\"><i class="fa fa-eye"></i></button></div>';			
							$('.image',this).prepend(_qsHref);					
							
							var quick_button = $('.btn-quickshop');								
							var width_button = (quick_button.width())/2 ;
							var height_button = (quick_button.height())/2;				
							var w_image = $('.image').width();
							var w_qs = quick_button.width();
							
							$('.btn-quickshop').css({
								'margin-left': - width_button + 'px',
								'margin-top': - height_button +'px',
								'top': '50%',
								'left': '50%'
							});		
								
						}
					});			
					var content_modal = '<div id="myModal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;"></div><div class="bt-qs-loading" style="position:fixed;top:50%;left:50%"></div>';
					$('#bt_container').append(content_modal);					
				}

				function getModalContent(product_id,seller_id, optiontype){		

					if(optiontype != ''){
						optiontype = optiontype;
					} else {
						optiontype = 1;
					}
					
					$.ajax({
						url: 'index.php?route=module/boss_quick_shop_product/&product_id=' + product_id+'&seller_id=' + seller_id+'&optiontype='+optiontype,
						dataType: 'json',
						beforeSend: function() {			
							$('.loading').html('<span class="wait">&nbsp;<img src="catalog/view/theme/default/image/loading.gif" alt="" /></span>');
							$('#myModal').html('');
						},		
						complete: function() {
							$('.wait').remove();
						},
						success: function(json) {
									
							$('#myModal').html(json['html']);
							$('#myModal > .modal-dialog').css({
								'width': '95%',
								'max-width': '900px',
							});
							$('#myModal').show();
						   populateRelatedProducts();
						}
					});
				}

				function populateRelatedProducts()
				{
						$('#product_related_qView').carouFredSel({
						auto: false,
						responsive: true,
						width: '100%',
						prev: '#prev_related_qView',
						next: '#next_related_qView',
						swipe: {
						onTouch : true
						},
						items: {
							width: 280,
							height: 'auto',
							visible: {
							min: 6,
							max: 6
							}
						},
						scroll: {
							direction : 'left',    //  The direction of the transition.
							duration  : 1000   //  The duration of the transition.
						}
					});

				}
			</script>
		
			<?php //echo $content_top; ?>
			<div class="content_bg">
			
				<div class="col-sm-12">
					<h3 class="control-label" for="input-search"><?php echo $entry_search; ?></h3>
				</div>
		  
				<div class="col-sm-3">
					<input type="text" name="search" value="<?php echo $search; ?>" placeholder="<?php echo $text_keyword; ?>" id="input-search" class="form-control" />
					
					<input type="hidden" name="seller_id" value="<?php echo $seller_id; ?>" id="seller_id" class="form-control" />     
				</div>
				
				<div class="col-sm-3">
					<select name="category_id" class="form-control ddlCustom"  >
						<option value="0"><?php echo $text_category; ?></option>
						<?php foreach ($categories as $category_1) { ?>
						<?php if ($category_1['category_id'] == $category_id) { ?>
						<option value="<?php echo $category_1['category_id']; ?>" selected="selected"><?php echo $category_1['name']; ?></option>
						<?php } else { ?>
						<option value="<?php echo $category_1['category_id']; ?>"><?php echo $category_1['name']; ?></option>
						<?php } ?>
						<?php foreach ($category_1['children'] as $category_2) { ?>
						<?php if ($category_2['category_id'] == $category_id) { ?>
						<option value="<?php echo $category_2['category_id']; ?>" selected="selected">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<?php echo $category_2['name']; ?></option>
						<?php } else { ?>
						<option value="<?php echo $category_2['category_id']; ?>">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<?php echo $category_2['name']; ?></option>
						<?php } ?>
						<?php foreach ($category_2['children'] as $category_3) { ?>
						<?php if ($category_3['category_id'] == $category_id) { ?>
						<option value="<?php echo $category_3['category_id']; ?>" selected="selected">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<?php echo $category_3['name']; ?></option>
						<?php } else { ?>
						<option value="<?php echo $category_3['category_id']; ?>">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<?php echo $category_3['name']; ?></option>
						<?php } ?>
						<?php } ?>
						<?php } ?>
						<?php } ?>
					</select>
				</div>
			   
				<div class="col-sm-3">
					<label class="checkbox-inline">
					<?php if ($sub_category) { ?>
					<input type="checkbox" name="sub_category" value="1" checked="checked" />
					<?php } else { ?>
					<input type="checkbox" name="sub_category" value="1" />
					<?php } ?>
					<?php echo $text_sub_category; ?></label>
				</div>
				
				<div class="col-sm-3">
					<label class="checkbox-inline">
					<?php if ($description) { ?>
						<input type="checkbox" name="description" value="1" id="description" checked="checked" />
					<?php } else { ?>
						<input type="checkbox" name="description" value="1" id="description" />
					<?php } ?>
					<?php echo $entry_description; ?></label>
				</div>
				
				<div class="col-sm-12"><input type="button" value="<?php echo $button_search; ?>" id="button-search" class="btn" /></div>
			</div>		  
		</div>
    </div>
	
	<div class="col-sm-12" style="display:none;"><h2><?php echo $text_search; ?></h2></div>
	
	<div class="text-center">
        <?php 
		if ($products) { ?>
			<div class="product-filter col-md-12">
				<div class="compare-total" style="display:none;"><a href="<?php echo $compare; ?>" id="compare-total"><?php echo $text_compare; ?></a></div>
				
				<div class="sortedBy pull-right">
					<label class="control-label" for="input-sort"><?php echo $text_sort; ?></label>
					<select id="input-sort" class="ddlCustom" onchange="location = this.value;" >
						<?php foreach ($sorts as $sorts) { ?>
							<?php if ($sorts['value'] == $sort . '-' . $order) { ?>
									<option value="<?php echo $sorts['href']; ?>" selected="selected"><?php echo $sorts['text']; ?></option>
							<?php } else { ?>
								<option value="<?php echo $sorts['href']; ?>"><?php echo $sorts['text']; ?></option>
							<?php } ?>
						<?php } ?>
					</select>
				</div>
				
				<div class="sortedBy limit pull-right" style="display:none;">  
					<label class="control-label" for="input-limit"><?php echo $text_limit; ?></label>
					<select id="input-limit" class="ddlCustom" onchange="location = this.value;">
						<?php foreach ($limits as $limits) { ?>
							<?php if ($limits['value'] == $limit) { ?>
								<option value="<?php echo $limits['href']; ?>" selected="selected"><?php echo $limits['text']; ?></option>
							<?php } else { ?>
								<option value="<?php echo $limits['href']; ?>"><?php echo $limits['text']; ?></option>
							<?php } ?>
						<?php } ?>
					</select>
				</div>
			</div>
		  
			<div class="row layout-thumb products">
				<?php 
				foreach ($products as $product) {?>
				
					<div class="product" id="<?php echo $product['product_id']; ?>">
						<div class="image">
							<?php
							$existInList = false;
							foreach ($_SESSION['default']['wishlist'] as $key => $value)
							 {
								if ($value[0] == $product['product_id'] && $value[2] ==$seller_id)
								 {
									$existInList = true;
								  }
							 }
							 if ($existInList) {
								unset($this->session->data['wishlist'][$index]);
							}
							
							if($existInList) { ?>
								<div class="heart-box-pink" id="heart<?php echo $product['product_id']; ?>">
									<div class="heart-label">
										<a class="wishlist1"  onclick="wishlist_remove('<?php echo $product['product_id']; ?>','<?php echo $seller_id ?>'); changeHeart('<?php echo $product['product_id']; ?>','<?php echo $seller_id ?>')">
											<i class="fa fa-heart" style="color:#E8114A"></i>
										</a>
									</div>
								</div>
							<?php 
							} else{ ?>
								<div class="heart-box" id="heart<?php echo $product['product_id']; ?>">
									<div class="heart-label">
									   <a class="wishlist1" onclick="wishlist_cat_add('<?php echo $product['product_id']; ?>','<?php echo $seller_id ?>')"><i class="fa fa-heart"></i></a>
									</div>
								</div>
							<?php 
							} ?>	
							  
							<?php 
							if(in_array($product['product_id'], $_SESSION['default']['wishlist'])) { ?>
								<div class="heart-box-pink" id="heart<?php echo $product['product_id']; ?>">
									<div class="heart-label">
										<a class="wishlist1"  onclick="wishlist_remove('<?php echo $product['product_id']; ?>'); changeHeart('<?php echo $product['product_id']; ?>')"><i class="fa fa-heart" style="color:#E8114A"></i></a>
									</div>
								</div>
							<?php 
							} else{ ?>
								<div class="heart-box" id="heart<?php echo $product['product_id']; ?>" style='display: none;'>
									<div class="heart-label">
										<a class="wishlist1"  onclick="btadd.wishlist('<?php echo $product['product_id']; ?>'); changeHeart('<?php echo $product['product_id']; ?>')"><i class="fa fa-heart"></i></a>
									</div>
								</div>
							<?php 
							} ?>
							
							<?php
							if($product['discount'] != 0 ){
								//is_int($product['discount']) or is_float($product['discount'])
								if($product['discount'] != 0){
									echo '<div class="discount-tag"> <p class="discount-p">'.$product['discount'].'%</p> </div>';
								}
								
							}
							?>
							<div class="btn-quickshop" style="margin-left: -30px; margin-top: -30px; top: 50%; left: 50%;">								  
								  <button title="Quick View" onclick="getModalContent(<?php echo $product['product_id']?>,<?php echo $seller_id ?>, <?php echo $optiontype; ?>);" class="sft_quickshop_icon " data-toggle="modal" data-target="#myModal">
									  <i class="fa fa-eye"></i>
								  </button>
							</div>
							<a id="<?php echo $product['href']; ?>" href="javascript:void(0)">
								<img class="img-responsive" src="<?php echo $product['thumb']; ?>" alt="<?php echo properName($product['name']); ?>" title="<?php echo properName($product['name']); ?>">
							</a>
						</div>
						  
						<h3><?php echo getItemName($product['name'],30); ?></h3>
						<div class="priceBox">
							<?php 
							if ($product['price']):
								if (!$product['special']) { ?>
									<p><?php echo $product['price']; ?></p>
								<?php
								} else { ?>
									<p><?php echo $product['special']; ?>   <p class='oldPrice'><?php echo $product['price']; ?></p></p>
								<?php } 
								
								if ($product['tax']) { ?>
									<p class='oldPrice'><?php echo $text_tax; ?>   <p><?php echo $product['tax']; ?></p></p>
								<?php 
								} 
							endif ?>
						</div>
						<div id="pro_div_id_<?php echo $product['product_id']?>">
							<?php  
							if (count($data['product_data']['cart_data'])):
								$count = 0;
								foreach ($data['product_data']['cart_data'] as $key => $value): 
									if (($value['product_id'] == $product['product_id']) && ($value['seller_id'] == $seller_id)):
										$count = 1; 
										$quantity = $value['quantity'];
										$cart_id = $value['cart_id'];
										break;
									endif;
								endforeach;
									
								if ($count == 1): ?>
									<div style='position:relative'>
										<a href="javascript:void(0)"><input  onchange="update_cart_data_home('<?php echo $cart_id; ?>', '<?php echo 'select-number'.$cart_id;?>');" id="select-number<?php echo $cart_id; ?>" disabled value="<?php echo $quantity; ?>"/></a>
										<a style='position:absolute;left: 40px;top: 24px;color: white' onclick="changeQty('<?php echo 'select-number'.$cart_id;?>',0); return false;" class="decrease" href="javascript:void(0)">_</a>
										<a style='position:absolute;right: 40px;top: 31px;color: white' onclick="changeQty('<?php echo 'select-number'.$cart_id;?>',1); return false;" class="increase" href="javascript:void(0)">+</a>
									</div>
								<?php else: ?>
									<?php if($product['options']){ ?>
										<a href="javascript:void(0)" id="custom" onclick="getModalContent(<?php echo $product['product_id']?>,<?php echo $seller_id?>, '1');" data-toggle="modal" data-target="#myModal"><button><?php echo $button_cart; ?></button></a>
									<?php } else { ?>
										<a href="javascript:void(0)" id="custom" onclick="btadd.cart('<?php echo $product['product_id']; ?>', '<?php echo $product['minimum']; ?>','<?php echo $seller_id; ?>');"><button><?php echo $button_cart; ?></button></a>
									<?php } ?>
								<?php endif ?>
							<?php else: ?>
								<?php if($product['options']){ ?>
									<a href="javascript:void(0)" id="custom" onclick="getModalContent(<?php echo $product['product_id']?>,<?php echo $seller_id?>, '1');" data-toggle="modal" data-target="#myModal"><button><?php echo $button_cart; ?></button></a>
								<?php } else { ?>
									<a href="javascript:void(0)" id="custom" onclick="btadd.cart('<?php echo $product['product_id']; ?>', '<?php echo $product['minimum']; ?>','<?php echo $seller_id; ?>');"><button><?php echo $button_cart; ?></button></a>
								<?php } ?>								
							<?php endif ?>
						</div>
					</div>
				<?php 
				} ?>
				
				<div id="loadingDiv" style="display:none">
					<img src="image/loader.gif">
				</div>
			</div>
        <?php 
		} else { ?>
			<div class="row">
				<div class="col-sm-12">
					<h3 class="noNroductMatches" style="margin-bottom: 20px;margin-top: 0px;"><?php echo $text_empty; ?></h3>
				</div>
			</div>
        <?php 
		} ?>
	</div>
	<div class="row">
		<?php /* <div class="bt_pagination">
			<div class="col-sm-6 text-left">
				<?php 
				if(!empty($pagination)){?>
					<div class="links"><?php echo $pagination; ?></div>
				<?php 
				} ?>
			</div>
			<div class="col-sm-6 text-right"><div class="results"><?php echo $results; ?></div></div>
		</div> */ ?>
		<?php echo $content_bottom; ?>
	</div>
</div>
<?php echo $footer; ?> 
<script type="text/javascript"><!--
window.addEventListener("load", function(){
	checkScroll = false;
	seller_id = <?php echo $seller_id; ?>;
	search = '<?php echo $search; ?>';
	start = 10;
	end = 20;
	limit = 10;
	page = 2;
	$(window).on('scroll',function() {
		if(!checkScroll){
			checkScroll = true;
			$.ajax({
				url: 'index.php?route=product/searchproduct',
				data: {"seller_id":seller_id,"search":search,"page":page,"start":start,"limit":limit},
				dataType: 'html',
				beforeSend: function(){
					$("#loadingDiv").show();
				},
				success: function (html) {	
					if (!$.trim(html)){   
						$('#loadingDiv').html('');
					}
					
					if(html != 'fail'){
						page = page + 1;
						start = end;
						end = end + 10;
						$('#loadingDiv').before(html);
						$("#loadingDiv").hide();
					}else{
						$(window).unbind('scroll');
						$("#loadingDiv").hide();
					}
				},
				error: function (xhr, ajaxOptions, thrownError) {
					alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
				},
				complete: function(){
					checkScroll = false;
				}
			});
		}

	});
	
	$(window).on('wheel', function(event){

		// deltaY obviously records vertical scroll, deltaX and deltaZ exist too
		if(event.originalEvent.deltaY < 0){
			// wheeled up
		}
		else {
			// wheeled down
			if(!checkScroll){
				checkScroll = true;
				$.ajax({
					url: 'index.php?route=product/searchproduct',
					data: {"seller_id":seller_id,"search":search,"page":page,"start":start,"limit":limit},
					dataType: 'html',
					beforeSend: function(){
						$("#loadingDiv").show();
					},
					success: function (html) {	
						if (!$.trim(html)){   
							$('#loadingDiv').html('');
						}
						
						if(html != 'fail'){
							page = page + 1;
							start = end;
							end = end + 10;
							$('#loadingDiv').before(html);
							$("#loadingDiv").hide();
						}else{
							$(window).unbind('scroll');
							$("#loadingDiv").hide();
						}
					},
					error: function (xhr, ajaxOptions, thrownError) {
						alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
					},
					complete: function(){
						checkScroll = false;
					}
				});
			}
		}
	});
	
	$('body').on({
		'touchmove': function(e) { 
			if(!checkScroll){
				checkScroll = true;
				$.ajax({
					url: 'index.php?route=product/searchproduct',
					data: {"search":search,"page":page,"start":start,"limit":limit},
					dataType: 'html',
					beforeSend: function(){
						$("#loadingDiv").show();
					},
					success: function (html) {	
						if (!$.trim(html)){   
							$('#loadingDiv').html('');
						}
						
						if(html != 'fail'){
							page = page + 1;
							start = end;
							end = end + 10;
							$('#loadingDiv').before(html);
							$("#loadingDiv").hide();
						}else{
							$(window).unbind('scroll');
							$("#loadingDiv").hide();
						}
					},
					error: function (xhr, ajaxOptions, thrownError) {
						alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
					},
					complete: function(){
						checkScroll = false;
					}
				});
			}	
		}
	});	
});
$('#button-search').bind('click', function() {
    
	url = 'index.php?route=product/search';
	
	var search = $('#content input[name=\'search\']').prop('value');
	
	if (search) {
		url += '&search=' + encodeURIComponent(search);
	}

	var category_id = $('#content select[name=\'category_id\']').prop('value');
	
	if (category_id > 0) {
		url += '&category_id=' + encodeURIComponent(category_id);
	}
	
	var sub_category = $('#content input[name=\'sub_category\']:checked').prop('value');
	
	if (sub_category) {
		url += '&sub_category=true';
	}
		
	var filter_description = $('#content input[name=\'description\']:checked').prop('value');
	
	if (filter_description) {
		url += '&description=true';
	}
    var seller = $('#content input[name=\'seller_id\']').prop('value');

	if (seller) {
		url += '&seller_id=' + encodeURIComponent(seller);
	}

	location = url;
});

$('#content input[name=\'search\']').bind('keydown', function(e) {
	if (e.keyCode == 13) {
		$('#button-search').trigger('click');
	}
});

$('select[name=\'category_id\']').on('change', function() {
	if (this.value == '0') {
		$('input[name=\'sub_category\']').prop('disabled', true);
	} else {
		$('input[name=\'sub_category\']').prop('disabled', false);
	}
});

$('select[name=\'category_id\']').trigger('change');
--></script>