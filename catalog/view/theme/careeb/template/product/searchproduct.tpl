<?php 
if ($products) {        
	foreach ($products as $product) {?>

		<div class="product" id="<?php echo $product['product_id']; ?>">
			<div class="image">
				<?php					
				$existInList = false;
				foreach ($_SESSION['default']['wishlist'] as $key => $value)
				 {
					if ($value[0] == $product['product_id'] && $value[2] ==$seller_id)
					 {
						$existInList = true;
					  }
				 }
				 if ($existInList) {
					unset($this->session->data['wishlist'][$index]);
				}
				
				if($existInList) { ?>
					<div class="heart-box-pink" id="heart<?php echo $product['product_id']; ?>">
						<div class="heart-label">
							<a class="wishlist1"  onclick="wishlist_remove('<?php echo $product['product_id']; ?>','<?php echo $seller_id ?>'); changeHeart('<?php echo $product['product_id']; ?>','<?php echo $seller_id ?>')">
								<i class="fa fa-heart" style="color:#E8114A"></i>
							</a>
					   </div>
					</div>
				<?php 
				} else { ?>
					<div class="heart-box" id="heart<?php echo $product['product_id']; ?>">
						<div class="heart-label">
							<a class="wishlist1" onclick="wishlist_cat_add('<?php echo $product['product_id']; ?>','<?php echo $seller_id ?>')"><i class="fa fa-heart"></i></a>
						</div>
					</div>
				<?php 
				} ?>	
				  
				<?php 
				if(in_array($product['product_id'], $_SESSION['default']['wishlist'])) { ?>
					<div class="heart-box-pink" id="heart<?php echo $product['product_id']; ?>">
						<div class="heart-label">
							<a class="wishlist1"  onclick="wishlist_remove('<?php echo $product['product_id']; ?>'); changeHeart('<?php echo $product['product_id']; ?>')"><i class="fa fa-heart" style="color:#E8114A"></i></a>
						</div>
					</div>
				<?php 
				} else{ ?>
					<div class="heart-box" id="heart<?php echo $product['product_id']; ?>" style='display: none;'>
						<div class="heart-label">
							<a class="wishlist1"  onclick="btadd.wishlist('<?php echo $product['product_id']; ?>'); changeHeart('<?php echo $product['product_id']; ?>')"><i class="fa fa-heart"></i></a>
						</div>
					</div>
				<?php 
				} ?>
				<?php
				if($product['discount'] != 0 ){
					//is_int($product['discount']) or is_float($product['discount'])
					if($product['discount'] != 0){
						echo '<div class="discount-tag"> <p class="discount-p">'.$product['discount'].'%</p> </div>';
					}
					
				}
				?>
				<div class="btn-quickshop" style="margin-left: -30px; margin-top: -30px; top: 50%; left: 50%;">
					<button title="Quick View" onclick="getModalContent(<?php echo $product['product_id']?>,<?php echo $seller_id ?>, <?php echo $optiontype; ?>);" class="sft_quickshop_icon " data-toggle="modal" data-target="#myModal">
						<i class="fa fa-eye"></i>
					</button>
				</div>
				<a id="<?php echo $product['href']; ?>" href="javascript:void(0)">
					<img class="img-responsive" src="<?php echo $product['thumb']; ?>" alt="<?php echo properName($product['name']); ?>" title="<?php echo properName($product['name']); ?>">
				</a>
			</div>
			<h3><?php echo getItemName($product['name'],30); ?></h3>
			<div class="priceBox">
				<?php 
				if ($product['price']):
					if (!$product['special']) { ?>
						<p><?php echo $product['price']; ?></p>
					<?php 
					} else { ?>
						<p><?php echo $product['special']; ?>   <p class='oldPrice'><?php echo $product['price']; ?></p></p>
					<?php 
					} 
					if ($product['tax']) { ?>
						<p class='oldPrice'><?php echo $text_tax; ?>   <p><?php echo $product['tax']; ?></p></p>
					<?php 
					} 
				endif ?>
			</div>
			<div id="pro_div_id_<?php echo $product['product_id']?>">
				<?php  
				if (count($data['product_data']['cart_data'])):
					$count = 0;
					foreach ($data['product_data']['cart_data'] as $key => $value): 
						if (($value['product_id'] == $product['product_id']) && ($value['seller_id'] == $seller_id)):
							$count = 1; 
							$quantity = $value['quantity'];
							$cart_id = $value['cart_id'];
							break;
						endif;
					endforeach;
					
					if ($count == 1): ?>
						<div style='position:relative'>
							<a href="javascript:void(0)"><input  onchange="update_cart_data_home('<?php echo $cart_id; ?>', '<?php echo 'select-number'.$cart_id;?>');" id="select-number<?php echo $cart_id; ?>" disabled value="<?php echo $quantity; ?>"/></a>
							<a style='position:absolute;left: 40px;top: 24px;color: white' onclick="changeQty('<?php echo 'select-number'.$cart_id;?>',0); return false;" class="decrease" href="javascript:void(0)">_</a>
							<a style='position:absolute;right: 40px;top: 31px;color: white' onclick="changeQty('<?php echo 'select-number'.$cart_id;?>',1); return false;" class="increase" href="javascript:void(0)">+</a>
						</div>
					<?php else: ?>							
						<?php if($product['options']){ ?>
							<a href="javascript:void(0)" id="custom" onclick="getModalContent(<?php echo $product['product_id']?>,<?php echo $seller_id?>, '1');" data-toggle="modal" data-target="#myModal"><button><?php echo $button_cart; ?></button></a>
						<?php } else { ?>
							<a href="javascript:void(0)" id="custom" onclick="btadd.cart('<?php echo $product['product_id']; ?>', '<?php echo $product['minimum']; ?>','<?php echo $seller_id; ?>');"><button><?php echo $button_cart; ?></button></a>
						<?php } ?>	
					<?php endif ?>
				<?php else: ?>						
					<?php if($product['options']){ ?>
						<a href="javascript:void(0)" id="custom" onclick="getModalContent(<?php echo $product['product_id']?>,<?php echo $seller_id?>, '1');" data-toggle="modal" data-target="#myModal"><button><?php echo $button_cart; ?></button></a>
					<?php } else { ?>
						<a href="javascript:void(0)" id="custom" onclick="btadd.cart('<?php echo $product['product_id']; ?>', '<?php echo $product['minimum']; ?>','<?php echo $seller_id; ?>');"><button><?php echo $button_cart; ?></button></a>
					<?php } ?>
				<?php endif ?>
			</div>
		</div>
	<?php 
	} 
} ?>