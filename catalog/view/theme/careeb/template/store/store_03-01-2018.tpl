<?php echo $header; ?>

<div class="row search-area-logicsfort-wrapper">
    <div class="container search-area-logicsfort">
        <?php
        if (isset($search)) {
            echo $search;
        } else {
            echo '<form style="width: 366px;height: 75px;"></form>';
        }
        ?>
    </div>
</div>

<?php 
if($getTime['storeTimeStatus'] == 1){
    $alertClass = 'alert-info';
} else{
    $alertClass = 'alert-danger';
}
?>
<div class="container">
<div class="col-md-12">
<div class="alert <?= $alertClass; ?>">
    <?= $next_delivery; ?>
</div>
</div>
</div>

<?php
$storeName = $sellerData['firstname'] . ' ' . $sellerData['lastname'];
if (!empty($storeName)) {
    echo '<h2 class=text-center>' . $storeName . '</h2>';
}

if (!empty($sellerData['banner'])) {
    ?>
    <style>
        .VigatableBg{background-image: url('./image/<?php echo $sellerData['banner']; ?>'); background-size: 100%;}
    </style>
<?php } ?>

<?php
$column_left = trim($column_left);
$column_right = trim($column_right);
if ($column_left || $column_right) {
    ?>
    <div class="container left-menucategory-fixed">
        <div class="row">
            <?php
        }
        echo $column_left;
        echo $column_right;
        if ($column_left && $column_right) {
            ?>
            <div id="content" class="col-sm-6">
            <?php } else if ($column_left || $column_right) {
                ?>
                <div id="content" class="col-sm-9">
                <?php } else { ?>
                    <div class="row">
                        <div  class="col-sm-1 col-sm-rtl"></div>
                        <div id="content" class="col-sm-10 col-sm-rtl">
                        <?php } ?>
                        <?php if ($categories) {   ?>
                            <div class="bt-box bt-box-store">
                                <div id="submenu">
                                    <div class="box-content">
                                        <div class="submenu-inside bxcslider">
                                            <?php foreach ($categories as $category) { ;?>
                                                <dl class="col-md-3 cate-list-item<?php echo($category['children']) ? ' submenu' : ''; ?>">
                                                    <dt class="cate-name">
                                                        <a href="<?php echo $category['href']; ?>" <?php if ($category['category_id'] == $category_id) echo 'class="active"'; ?> style="background:linear-gradient(rgba(0, 0, 0, 0.5), rgba(20, 20, 20, 0.5)), url(<?php echo $category['image'] ?>)"><?php echo $category['name']; ?></a>
                                                    </dt>
                                                </dl>
                                            <?php } ?>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        <?php } ?>

                        <?php //var_dump($sellerData); ?>
                        <div class="store_time">
                            <h3 class="text-center">Store Timing</h3>
                            
                            <table class="table table-responsive table-bordered timing-table">
                                <thead
                                    <tr>
                                        <th>Days</th>
                                        <th>Opening Time</th>
                                        <th>Closing Time</th>
                                        <th>Status</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr class="<?= ((date("l") == 'Monday') ? 'active' : '') . ' ' . (((date("l") == 'Monday') && $sellerData['mon_end_time'] > date("h:i:sa")) ? 'sOpen' : 'sClosed'); ?>">
                                        <td><strong>Monday</strong></td>
                                        <td>
                                            <?= (($sellerData['mon_start_time'] != '00:00:00' || $sellerData['mon_end_time'] != '00:00:00') ? date("g:i a", strtotime($sellerData['mon_start_time'])) : 'Closed'); ?>
                                        </td>
                                        <td>
                                            <?= (($sellerData['mon_start_time'] != '00:00:00' || $sellerData['mon_end_time'] != '00:00:00') ? date("g:i a", strtotime($sellerData['mon_end_time'])) : 'Closed'); ?>
                                        </td>
                                        <td>
                                            <?php
                                            if (date("l") == 'Monday') {
                                                echo (($sellerData['mon_end_time'] > date("h:i:sa")) ? 'Open' : 'Closed');
                                            }
                                            ?>
                                        </td>
                                    </tr>
                                    <tr class="<?= ((date("l") == 'Tuesday') ? 'active' : '') . ' ' . (((date("l") == 'Tuesday') && $sellerData['tue_end_time'] > date("h:i:sa")) ? 'sOpen' : 'sClosed'); ?>">
                                        <td><strong>Tuesday</strong></td>
                                        <td>
                                            <?= (($sellerData['tue_start_time'] != '00:00:00' || $sellerData['tue_end_time'] != '00:00:00') ? date("g:i a", strtotime($sellerData['tue_start_time'])) : 'Closed'); ?>
                                        </td>
                                        <td>
                                            <?= (($sellerData['tue_start_time'] != '00:00:00' || $sellerData['tue_end_time'] != '00:00:00') ? date("g:i a", strtotime($sellerData['tue_end_time'])) : 'Closed'); ?>
                                        </td>
                                        <td>
                                            <?php
                                            if (date("l") == 'Tuesday') {
                                                echo (($sellerData['tue_end_time'] > date("h:i:sa")) ? 'Open' : 'Closed');
                                            }
                                            ?>
                                        </td>
                                    </tr>
                                    <tr class="<?= ((date("l") == 'Wednesday') ? 'active' : '') . ' ' . (((date("l") == 'Wednesday') && $sellerData['wed_end_time'] > date("h:i:sa")) ? 'sOpen' : 'sClosed'); ?>">
                                        <td><strong>Wednesday</strong></td>
                                        <td>
                                            <?= (($sellerData['wed_start_time'] != '00:00:00' || $sellerData['wed_end_time'] != '00:00:00') ? date("g:i a", strtotime($sellerData['wed_start_time'])) : 'Closed'); ?>
                                        </td>
                                        <td>
                                            <?= (($sellerData['wed_start_time'] != '00:00:00' || $sellerData['wed_end_time'] != '00:00:00') ? date("g:i a", strtotime($sellerData['wed_end_time'])) : 'Closed'); ?>
                                        </td>
                                        <td>
                                            <?php
                                            if (date("l") == 'Wednesday') {
                                                echo (($sellerData['wed_end_time'] > date("h:i:sa")) ? 'Open' : 'Closed');
                                            }
                                            ?>
                                        </td>
                                    </tr>
                                    <tr class="<?= ((date("l") == 'Thursday') ? 'active' : '') . ' ' . (((date("l") == 'Thursday') && $sellerData['thu_end_time'] > date("h:i:sa")) ? 'sOpen' : 'sClosed'); ?>">
                                        <td><strong>Thursday</strong></td>
                                        <td>
                                            <?= (($sellerData['thu_start_time'] != '00:00:00' || $sellerData['thu_end_time'] != '00:00:00') ? date("g:i a", strtotime($sellerData['thu_start_time'])) : 'Closed'); ?>
                                        </td>
                                        <td>
                                            <?= (($sellerData['thu_start_time'] != '00:00:00' || $sellerData['thu_end_time'] != '00:00:00') ? date("g:i a", strtotime($sellerData['thu_end_time'])) : 'Closed'); ?>
                                        </td>
                                        <td>
                                            <?php
                                            if (date("l") == 'Thursday') {
                                                echo (($sellerData['thu_end_time'] > date("h:i:sa")) ? 'Open' : 'Closed');
                                            }
                                            ?>
                                        </td>
                                    </tr>
                                    <tr class="<?= ((date("l") == 'Friday') ? 'active' : '') . ' ' . (((date("l") == 'Friday') && $sellerData['fri_end_time'] > date("h:i:sa")) ? 'sOpen' : 'sClosed'); ?>">
                                        <td><strong>Friday</strong></td>
                                        <td>
                                            <?= (($sellerData['fri_start_time'] != '00:00:00' || $sellerData['fri_end_time'] != '00:00:00') ? date("g:i a", strtotime($sellerData['fri_start_time'])) : 'Closed'); ?>
                                        </td>
                                        <td>
                                            <?= (($sellerData['fri_start_time'] != '00:00:00' || $sellerData['fri_end_time'] != '00:00:00') ? date("g:i a", strtotime($sellerData['fri_end_time'])) : 'Closed'); ?>
                                        </td>
                                        <td>
                                            <?php
                                            if (date("l") == 'Friday') {
                                                echo (($sellerData['fri_end_time'] > date("h:i:sa")) ? 'Open' : 'Closed');
                                            }
                                            ?>
                                        </td>
                                    </tr>
                                    <tr class="<?= ((date("l") == 'Saturday') ? 'active' : '') . ' ' . (((date("l") == 'Saturday') && $sellerData['sat_end_time'] > date("h:i:sa")) ? 'sOpen' : 'sClosed'); ?>">
                                        <td><strong>Saturday</strong></td>
                                        <td>
                                            <?= (($sellerData['sat_start_time'] != '00:00:00' || $sellerData['sat_end_time'] != '00:00:00') ? date("g:i a", strtotime($sellerData['sat_start_time'])) : 'Closed'); ?>
                                        </td>
                                        <td>
                                            <?= (($sellerData['sat_start_time'] != '00:00:00' || $sellerData['sat_end_time'] != '00:00:00') ? date("g:i a", strtotime($sellerData['sat_end_time'])) : 'Closed'); ?>
                                        </td>
                                        <td>
                                            <?php
                                            if (date("l") == 'Saturday') {
                                                echo (($sellerData['sat_end_time'] > date("h:i:sa")) ? 'Open' : 'Closed');
                                            }
                                            ?>
                                        </td>
                                    </tr>
                                    <tr class="<?= ((date("l") == 'Sunday') ? 'active' : '') . ' ' . (((date("l") == 'Sunday') && $sellerData['sun_end_time'] > date("h:i:sa")) ? 'sOpen' : 'sClosed'); ?>">
                                        <td><strong>Sunday</strong></td>
                                        <td>
                                            <?= (($sellerData['sun_start_time'] != '00:00:00' || $sellerData['sun_end_time'] != '00:00:00') ? date("g:i a", strtotime($sellerData['sun_start_time'])) : 'Closed'); ?>
                                        </td>
                                        <td>
                                            <?= (($sellerData['sun_start_time'] != '00:00:00' || $sellerData['sun_end_time'] != '00:00:00') ? date("g:i a", strtotime($sellerData['sun_end_time'])) : 'Closed'); ?>
                                        </td>
                                        <td>
                                            <?php
                                            if (date("l") == 'Sunday') {
                                                echo (($sellerData['sun_end_time'] > date("h:i:sa")) ? 'Open' : 'Closed');
                                            }
                                            ?>
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>

                        <?php /* ?>    
                          <div class="text-center">
                          <div class="products">
                          <?php if ($products) { ?>
                          <?php foreach ($products as $product) { ?>

                          <div class="product" id="<?php echo $product['product_id']; ?>">
                          <div class="image">
                          <?php
                          $existInList = false;
                          foreach ($_SESSION['default']['wishlist'] as $key => $value) {
                          if ($value[0] == $product['product_id'] && $value[2] == $seller_id) {
                          $existInList = true;
                          }
                          }
                          if ($existInList) {
                          unset($this->session->data['wishlist'][$index]);
                          }
                          if ($existInList) {
                          ?>
                          <div class="heart-box-pink" id="heart<?php echo $product['product_id']; ?>">
                          <div class="heart-label">
                          <a class="wishlist1" onclick="wishlist_remove('<?php echo $product['product_id']; ?>', '<?php echo $seller_id; ?>'); changeHeart('<?php echo $product['product_id']; ?>', '<?php echo $seller_id; ?>')">
                          <i class="fa fa-heart" style="color:#E8114A"></i>
                          </a>
                          </div>
                          </div>
                          <?php } else { ?>
                          <div class="heart-box" id="heart<?php echo $product['product_id']; ?>">
                          <div class="heart-label">
                          <a class="wishlist1" onclick="wishlist_cat_add('<?php echo $product['product_id']; ?>', '<?php echo $seller_id; ?>')"><i class="fa fa-heart"></i></a>
                          </div>
                          </div>
                          <?php } ?>
                          <div class="btn-quickshop" style="margin-left: -30px; margin-top: -30px; top: 50%; left: 50%;">

                          <button title="Quick View" onclick="getModalContent(<?php echo $product['product_id'] ?>,<?php echo $seller_id ?>);" class="sft_quickshop_icon " data-toggle="modal" data-target="#myModal">
                          <i class="fa fa-eye"></i>
                          </button>
                          </div>
                          <a id="<?php echo $product['href']; ?>" href="javascript:void(0)">
                          <img class="img-responsive" src="<?php echo $product['image']; ?>" alt="<?php echo properName($product['name']); ?>" title="<?php echo properName($product['name']); ?>">
                          </a>
                          </div>
                          <h3><?php echo getItemName($product['name'], 30); ?></h3>
                          <?php
                          if ($product['price']):
                          if (empty($product['special'])) {
                          ?>
                          <p><?php echo $product['price']; ?></p>
                          <?php } else { ?>
                          <p><?php echo $product['special']; ?>   <p class='oldPrice price-old'><?php echo $product['price']; ?></p></p>
                          <?php
                          }
                          if ($product['tax']) {
                          ?>
                          <p class='oldPrice price-old'><?php echo $text_tax; ?>   <p><?php echo $product['tax']; ?></p></p>
                          <?php
                          }
                          endif
                          ?>
                          <div id="pro_div_id_<?php echo $product['product_id'] ?>">
                          <?php
                          if (count($data['product_data']['cart_data'])):
                          $count = 0;
                          foreach ($data['product_data']['cart_data'] as $key => $value):
                          if (($value['product_id'] == $product['product_id']) && ($value['seller_id'] == $seller_id)):
                          $count = 1;
                          $quantity = $value['quantity'];
                          $cart_id = $value['cart_id'];
                          break;
                          endif;
                          endforeach;
                          if ($count == 1):
                          ?>
                          <div style='position:relative'>
                          <a href="javascript:void(0)"><input  onchange="update_cart_data_home('<?php echo $cart_id; ?>', '<?php echo 'select-number' . $cart_id; ?>');" id="select-number<?php echo $cart_id; ?>" disabled value="<?php echo $quantity; ?>"/></a>
                          <a style='position:absolute;left: 40px;top: 24px;color: white' onclick="changeQty('<?php echo 'select-number' . $cart_id; ?>', 0); return false;" class="decrease" href="javascript:void(0)">_</a>
                          <a style='position:absolute;right: 40px;top: 31px;color: white' onclick="changeQty('<?php echo 'select-number' . $cart_id; ?>', 1); return false;" class="increase" href="javascript:void(0)">+</a>
                          </div>
                          <?php else: ?>
                          <a href="javascript:void(0)" id="custom" onclick="btadd.cart('<?php echo $product['product_id']; ?>', '<?php echo $product['minimum']; ?>', '<?php echo $seller_id; ?>');"><button><?php echo $button_cart; ?></button></a>
                          <?php endif ?>
                          <?php else: ?>
                          <a href="javascript:void(0)" id="custom" onclick="btadd.cart('<?php echo $product['product_id']; ?>', '<?php echo $product['minimum']; ?>', '<?php echo $seller_id; ?>');"><button><?php echo $button_cart; ?></button></a>
                          <?php endif ?>
                          </div>
                          </div>
                          <?php } ?>
                          <?php } else { ?>
                          <h2> Sorry no product found!</h2>

                          <?php } ?>
                          </div>
                          </div>
                          <?php */ ?>



                        <?php
                        echo $content_top;
                        if ($column_left && $column_right) {
                            ?>
                        </div>
                    <?php } else if ($column_left || $column_right) { ?>
                    </div>
                <?php } else { ?>
                </div> </div> 
            <?php
        }
        if ($column_left || $column_right) {
            ?>
        </div></div>
<?php } ?>


<?php echo $content_bottom; ?>


<script type="text/javascript"><!--
$(document).ready(function ($) {
        $('.bxcslider').bxSlider({
            onSliderLoad: function () {
                $(".bt-box-store").css("visibility", "visible");
            },
            slideWidth: 310,
            captions: false,
            maxSlides: 4,
            moveSlides: 4,
            slideMargin: 5,
            responsive: true,
            infiniteLoop: false
        });
    });
//--></script>

    <?php echo $footer; ?>