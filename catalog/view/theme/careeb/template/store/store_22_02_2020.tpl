<?php echo $header; ?>

<div class="container container-fluid left-menucategory-fixed">
   
	<div class="">
	
		<div  class="col-sm-1 col-sm-rtl"></div>
		
		<div id="content" class="col-sm-rtl StoreDiv">
		
			<?php /*
			if ($categories) { ?>
				<div class="bt-box bt-box-store">
					<div id="submenu">
					<h3 class="content-categories-title"><?php echo $text_categories; ?></h3>
					<?php 	<div class="box-content">
							<div class="submenu-inside bxcslider">
								<?php
								foreach ($categories as $category) {
								   
									if(isset($category['children'])) { ?>
									
									<dl class="cate-list-item<?php echo($category['children']) ? ' submenu' : ''; ?>">
										
									<?php } else { ?>
									
									<dl class="cate-list-item submenu">												
									<?php }	?>											
									
										<dt class="cate-name">
											
											<a href="<?php echo $category['href']; ?>" <?php if((isset($category_id)) && ($category['category_id'] == $category_id)) { echo 'class="active"'; } ?>><img src="<?php echo $category['image'] ?>" ></br><?php echo $category['name']; ?></a>
										</dt>
									</dl>
								<?php } ?>
							</div>
						</div> 
					</div>
				</div>
			<?php 
			} */ ?>

			<script type="text/javascript" src="catalog/view/javascript/bossthemes/cloud-zoom/cloud-zoom.1.0.3.js" type="text/javascript"></script>
		
			 <!-- <script type="text/javascript" src="catalog/view/javascript/bossthemes/carouFredSel-6.2.1.js"></script> -->
			
			<script type="text/javascript">
				$(window).load(function() {	   
					boss_quick_shop();
					$('.display').bind('click', function() {
						$('.sft_quickshop_icon').remove();
						boss_quick_shop();
					});	   
					
				});
				
				function boss_quick_shop(){					
					$('#column-left .bt-item-large .product-thumb').each(function(index, value){		
						var id_product='';
						var reloadurl=false;
						
						if($(".image>a",this).attr('id')){
							
							var href_pro = $(".image>a",this).attr('id'); 
						}
						else if($(".image>a",this).attr('href')){
							
							var href_pro = $(".image>a",this).attr('href'); 
						}else{ 
							var href_pro = '';
						}
					   
						if	(href_pro){
							var check=href_pro.match("index.php"); 
						}
						var last_index = '';
						var product_id = 0;
						if(check=="index.php"){	 //direct link
							var str = href_pro.split("&");
							for (var i=0;i<str.length;i++){
								if(str[i].match("product_id=") =="product_id="){					

									last_index = str[i];
									var id = last_index.split("=");
									product_id = id[1];
									break;
								}
							}
							reloadurl=true;
						}else{	//mode SEO
							var check_seo = href_pro.match("product_id=");
							if(check_seo=="product_id="){				

								var str = href_pro.split("&");
								for (var i=0; i<str.length; i++){
									if(str[i].match("product_id=") == "product_id="){
										var temp = str[i].split("?");
										last_index = temp[temp.length-1]; // lay phan tu cuoi cung
										var id = last_index.split("=");
										product_id = id[1];
										break;
									}
								}					
								reloadurl=true;
							}else{				
								var str_1 = href_pro.split("/");
								var str_2 = str_1[str_1.length-1]; 					
								var temp = str_2.split("?");
								last_index = temp[0];
								var id_index = '';							
								if(id_index!=''){
									var id = id_index.split('=');
									product_id = id[1];	
									reloadurl=true;
								}
							}
						}
						
						if(reloadurl){		
							var seller_id = <?php echo $seller_id; ?>;
							
							var _qsHref = '<div class=\"btn-quickshop\" ><button  title =\"Quick View\" onclick=\"getModalContent('+product_id+','+seller_id+');\" class=\"sft_quickshop_icon \" data-toggle=\"modal\" data-target=\"#myModal\"><i class="fa fa-eye"></i></button></div>';			
							$('.image',this).prepend(_qsHref);					
							
							var quick_button = $('.btn-quickshop');								
							var width_button = (quick_button.width())/2 ;
							var height_button = (quick_button.height())/2;				
							var w_image = $('.image').width();
							var w_qs = quick_button.width();
							
							$('.btn-quickshop').css({
								'margin-left': - width_button + 'px',
								'margin-top': - height_button +'px',
								'top': '50%',
								'left': '50%'
							});		
								
						}
					});			
					var content_modal = '<div id="myModal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;"></div><div class="bt-qs-loading" style="position:fixed;top:50%;left:50%"></div>';
					$('#bt_container').append(content_modal);					
				} 

				function getModalContent(product_id,seller_id, optiontype){		

					if(optiontype != ''){
						optiontype = optiontype;
					} else {
						optiontype = 1;
					}
					
					$.ajax({
						url: 'index.php?route=module/boss_quick_shop_product/&product_id=' + product_id+'&seller_id=' + seller_id+'&optiontype='+optiontype,
						dataType: 'json',
						beforeSend: function() {			
							$('.loading').html('<span class="wait">&nbsp;<img src="catalog/view/theme/default/image/loading.gif" alt="" /></span>');
							$('#myModal').html('');
						},		
						complete: function() {
							$('.wait').remove();
						},
						success: function(json) {
									
							$('#myModal').html(json['html']);
							$('#myModal > .modal-dialog').css({
								'width': '95%',
								'max-width': '900px',
							});
							$('#myModal').show();
						   populateRelatedProducts();
						}
					});
				}

				function populateRelatedProducts()
				{
						$('#product_related_qView').carouFredSel({
						auto: false,
						responsive: true,
						width: '100%',
						prev: '#prev_related_qView',
						next: '#next_related_qView',
						swipe: {
						onTouch : true
						},
						items: {
							width: 280,
							height: 'auto',
							visible: {
							min: 6,
							max: 6
							}
						},
						scroll: {
							direction : 'left',    //  The direction of the transition.
							duration  : 1000   //  The duration of the transition.
						}
					});

				}
			</script> 
	
	
  <!-- 	 <link rel="stylesheet"  href="catalog/view/theme/careeb/stylesheet/lightslider/lightslider.css"/>
    <style>
    	ul{
			list-style: none outside none;
		    padding-left: 0;
            margin: 0;
		}
        .demo .item{
            margin-bottom: 60px;
        }
		.content-slider li{
		    background-color: #ed3020;
		    text-align: center;
		    color: #FFF;
		}
		.content-slider h3 {
		    margin: 0;
		    padding: 70px 0;
		}
		.demo{
			width: auto;
		}
		.bx-prev{
			    background: url(../img/add_prev_hover.png) no-repeat!important;
		}
		
    </style>
 <script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.10.3/jquery-ui.min.js" type="text/javascript"></script>
    <script src="catalog/view/theme/careeb/js/lightslider/lightslider.js" type="text/javascript"></script> 
    <script>
	   $(document).ready(function() {
		if(/Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent) ) {
			 $('#responsive').lightSlider({
				item:2,
				loop:false,
				slideMove:2,
				easing: 'cubic-bezier(0.25, 0, 0.25, 1)',
				auto:true,
				speed:2000
			});
		}else{
		   $('#responsive').lightSlider({
				item:6,
				loop:false,
				slideMove:6,
				easing: 'cubic-bezier(0.25, 0, 0.25, 1)',
				speed:2000,
				auto:true,
				responsive : [
					{
						breakpoint:768,
						settings: {
							item:3,
							slideMove:1,
							slideMargin:6,
						  }
					},
					{
						breakpoint:480,
						settings: {
							item:2,
							slideMove:1
						  }
					}
				]
			});
		}	
	  });
    </script>-->
			<h3 class="content-categories-title"><?php echo $text_categories; ?></h3>
			<br/>
			<!--<div class="demo">
				<div class="item">
					<ul id="responsive" class="content-slider">
						<?php
						foreach ($categories as $category) {
						   
							if(isset($category['children'])) { ?>
							
							<dl class="cate-list-item<?php //echo($category['children']) ? ' submenu' : ''; ?>">
								
							<?php } else { ?>
							
							<dl class="cate-list-item submenu">												
							<?php }	?>											
							
								<dt class="cate-name">
									<a href="<?php //echo $category['href']; ?>" <?php if((isset($category_id)) && ($category['category_id'] == $category_id)) { //echo 'class="active"';?><?php } ?>><img src="<?php //echo $category['image'] ?>" ></br></br><?php //echo $category['name']; ?></a>
								</dt>
							</dl>
						<?php } ?>
					</ul>
				</div>
			</div>--> 
			
			
			<?php 
			if ($categories) { ?>
				<div class="icon-section text-center">
					<div class="container">
						<?php foreach ($categories as $category) { ?>
							<div class="box">
								<a href="<?php echo $category['href']; ?>"><img src="<?php echo $category['image']; ?>" title="<?php echo $category['name']; ?>" alt="<?php echo $category['name']; ?>" />        
								<h3><?php echo $category['name']; ?></h3></a>
							</div>
						<?php } ?>
					</div>
				</div>
			<?php 
			} ?>
		
			<br/><br/>	
				
				
				
				
				
			<?php echo $content_top;?> 
			
			<?php 
			if ($products) {  ?>
				<div class="text-center">
				<h3 class="content-products-title"><?php echo $text_products; ?></h3>
					<div class="products">
						<?php foreach ($products as $product) {?>
							<div class="product" id="<?php echo $product['product_id']; ?>">
								<div class="image">
									<?php  
									$existInList = false;
									if(isset($_SESSION['default']['wishlist'])){
									foreach ($_SESSION['default']['wishlist'] as $key => $value) {
										if ($value[0] == $product['product_id'] && $value[2] == $seller_id) {
											$existInList = true;
										}
									}
									}
									if ($existInList) {
										unset($this->session->data['wishlist'][$index]);
									}
									if($existInList) {
									?>
										<div class="heart-box-pink" id="heart<?php echo $product['product_id']; ?>">
											<div class="heart-label">
												<a class="wishlist1" onclick="wishlist_remove('<?php echo $product['product_id']; ?>',<?php echo $seller_id?>); changeHeart('<?php echo $product['product_id']; ?>',<?php echo $seller_id?>)">
												<i class="fa fa-heart" style="color:#E8114A"></i>
												</a>
											</div>
										</div>
									<?php } else{ ?>
										<div class="heart-box" id="heart<?php echo $product['product_id']; ?>">
											<div class="heart-label">
												<a class="wishlist1" onclick="wishlist_cat_add('<?php echo $product['product_id']; ?>',<?php echo $seller_id?>)"><i class="fa fa-heart"></i></a>
											</div>
										</div>
									<?php } ?>
									<div class="btn-quickshop">
										<button title="Quick View" onclick="getModalContent(<?php echo $product['product_id']?>,<?php echo $seller_id?> , '1');" class="sft_quickshop_icon " data-toggle="modal" data-target="#myModal"><i class="fa fa-eye"></i></button>
									</div>
									<a id="<?php echo $product['href']; ?>" href="javascript:void(0)"><img class="img-responsive" src="<?php echo $product['thumb']; ?>" alt="<?php echo properName($product['name']); ?>" title="<?php echo properName($product['name']); ?>"></a>
								</div>

								<h3><?php echo getItemName($product['name'],30); ?></h3>
								<div class="priceListing">
								<?php if ($product['price']):
									if (!$product['special']) { ?>
										<p><?php echo $product['price']; ?></p>
									<?php } else { ?>
										<p>
											<?php echo $product['special']; ?>
											<?php if ($product['price']) { ?>
												<p class="oldPrice"><?php echo $product['price']; ?></p>
											<?php } ?>
										</p>
									<?php }
									if ($product['tax']) { ?>
										<p class="oldPrice">
											<?php echo $text_tax; ?>
											<?php if ($product['tax']) { ?>
												<p><?php echo $product['tax']; ?></p>
											<?php } ?>
										</p>
									<?php } ?>
								<?php endif ?>
								</div>		

								<div id="pro_div_id_<?php echo $product['product_id']?>">
									<?php  
									if (count($data['product_data']['cart_data'])):
										$count = 0;
										foreach ($data['product_data']['cart_data'] as $key => $value):
											if (($value['product_id'] == $product['product_id']) && ($value['seller_id'] == $seller_id)):
												$count = 1;
												$quantity = $value['quantity'];
												$cart_id = $value['cart_id'];
												break;
											endif;
										endforeach;
										if ($count == 1): ?>
											<div style='position:relative'>
												<a href="javascript:void(0)"><input  onchange="update_cart_data_home('<?php echo $cart_id; ?>', '<?php echo 'select-number'.$cart_id;?>');" id="select-number<?php echo $cart_id; ?>" disabled value="<?php echo $quantity; ?>"/></a>
												<a style='position:absolute;left: 40px;top: 24px;color: white' onclick="changeQty('<?php echo 'select-number'.$cart_id;?>',0); return false;" class="decrease" href="javascript:void(0)">_</a>
												<a style='position:absolute;right: 40px;top: 31px;color: white' onclick="changeQty('<?php echo 'select-number'.$cart_id;?>',1); return false;" class="increase" href="javascript:void(0)">+</a>
											</div>
										<?php else: ?>
											<?php if($product['options']){ ?>
												<a href="javascript:void(0)" id="custom" onclick="getModalContent(<?php echo $product['product_id']?>,<?php echo $seller_id?>, '1');" data-toggle="modal" data-target="#myModal"><button><?php echo $button_cart; ?></button></a>
											<?php } else { ?>
												<a href="javascript:void(0)" id="custom" onclick="btadd.cart('<?php echo $product['product_id']; ?>', '<?php echo $product['minimum']; ?>','<?php echo $seller_id; ?>');"><button><?php echo $button_cart; ?></button></a>
											<?php } ?>
										<?php endif ?>
									<?php else: ?>
										<?php if($product['options']){ ?>
											<a href="javascript:void(0)" id="custom" onclick="getModalContent(<?php echo $product['product_id']?>,<?php echo $seller_id?>, '1');" data-toggle="modal" data-target="#myModal"><button><?php echo $button_cart; ?></button></a>
										<?php } else { ?>
											<a href="javascript:void(0)" id="custom" onclick="btadd.cart('<?php echo $product['product_id']; ?>', '<?php echo $product['minimum']; ?>','<?php echo $seller_id; ?>');"><button><?php echo $button_cart; ?></button></a>
										<?php } ?>
									<?php endif ?>
								</div>
							</div>
						<?php } ?>
						<div id="loadingDiv" style="display:none;">
							<img src="image/loader.gif">
						</div>
					</div>
				</div>
			<?php 
			} ?>
		</div>
	</div>
</div>
		
		
<div class="weAreWithYou">   
	<?php if($language_id == 3){?>
		<img class="img-responsive banner-image-responsive" src="catalog/view/theme/careeb/img/AR_Slider.png" usemap="#bannermap"> 
		<map name="bannermap">
			<area shape="rect" coords="800,330,985,385" href="https://itunes.apple.com/st/app/salatty/id1200691958?mt=8">
			<area shape="rect" coords="1003,330,1190,385" href="https://play.google.com/store/apps/details?id=com.rs.qareebstore&hl=en">
		</map>
	<?php }else{ ?>
		<img class="img-responsive banner-image-responsive" src="catalog/view/theme/careeb/img/EN_Slider.png" usemap="#bannermap"> 
	<map name="bannermap">
		<area shape="rect" coords="153,320,340,375" href="https://itunes.apple.com/st/app/salatty/id1200691958?mt=8">
		<area shape="rect" coords="358,320,550,375" href="https://play.google.com/store/apps/details?id=com.rs.qareebstore&hl=en">
	</map>
	<?php }?>
	
	<!--<div class="container">				
		<a href="https://itunes.apple.com/st/app/salatty/id1200691958?mt=8" target="_blank"><img class="img-responsive" src="catalog/view/theme/careeb/img/apple.png"></a>        		<a href="https://play.google.com/store/apps/details?id=com.rs.careebstore&amp;hl=en" target="_blank"><img class="img-responsive" src="catalog/view/theme/careeb/img/googlePlay.png"></a>      	
	</div>-->
</div>

<?php echo $content_bottom; ?>


<script type="text/javascript"><!--

window.addEventListener("load", function(){
	checkScroll = false;
	start = 10;
	end = 20;
	limit = 10;
	productload = 0;
	 $(window).on('beforeunload',function() {
		if(!checkScroll){
			checkScroll = true;
			$.ajax({
				url: 'index.php?route=store/storeproduct',
				data: {"start":start,"limit":limit,"seller_id":<?php echo $seller_id; ?>},
				dataType: 'html',
				beforeSend: function(){
					$("#loadingDiv").show();
				},
				success: function (html) {	
					if(isEmpty(html)){
						alert('dd');
					}
					if(html == ''){	
						$('#loadingDiv').html('');
					}
					if(html != 'fail'){
						start = end;
						end = end + 10;
						$('#loadingDiv').before(html);
						$("#loadingDiv").hide();
					}else{
						$(window).unbind('beforeunload');
						$("#loadingDiv").hide();
					}
				},
				error: function (xhr, ajaxOptions, thrownError) {
					alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
				},
				complete: function(){
					checkScroll = false;
				}
			});
		}

	});
	
	$(window).on('wheel', function(event){

		// deltaY obviously records vertical scroll, deltaX and deltaZ exist too
		if(event.originalEvent.deltaY < 0){
			// wheeled up
		}
		else {
			if(productload == 0){
				if(!checkScroll){
					checkScroll = true;
					$.ajax({
						url: 'index.php?route=store/storeproduct',
						data: {"start":start,"limit":limit,"seller_id":<?php echo $seller_id; ?>},
						dataType: 'html',
						beforeSend: function(){
							$("#loadingDiv").show();
						},
						success: function (html) {	
							if(html == ''){
								productload =1;
								$('#loadingDiv').html('');
							}
							if(html != 'fail'){
								start = end;
								end = end + 10;
								$('#loadingDiv').before(html);
								$("#loadingDiv").hide();
							}else{
								$(window).unbind('scroll');
								$("#loadingDiv").hide();
							}
						},
						error: function (xhr, ajaxOptions, thrownError) {
							alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
						},
						complete: function(){
							checkScroll = false;
						}
					});
				}
			}
		}
	});
	
	$('body').on({
		'touchmove': function(e) { 
			if(!checkScroll){
				checkScroll = true;
				$.ajax({
					url: 'index.php?route=store/storeproduct',
					data: {"start":start,"limit":limit,"seller_id":<?php echo $seller_id; ?>},
					dataType: 'html',
					beforeSend: function(){
						$("#loadingDiv").show();
					},
					success: function (html) {	
						if(html == ''){
							$('#loadingDiv').html('');
						}
						if(html != 'fail'){
							start = end;
							end = end + 10;
							$('#loadingDiv').before(html);
							$("#loadingDiv").hide();
						}else{
							$(window).unbind('scroll');
							$("#loadingDiv").hide();
						}
					},
					error: function (xhr, ajaxOptions, thrownError) {
						alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
					},
					complete: function(){
						checkScroll = false;
					}
				});
			}	
		}
	});	 
});
/*$(document).ready(function ($) {

	var isMobile = window.matchMedia("only screen and (max-width: 767px)");

	if (isMobile.matches) {
		$('.bxcslider').bxSlider({
            onSliderLoad: function () {
                $(".bt-box-store").css("visibility", "visible");
            },
            slideWidth: 175,
            captions: false,
            maxSlides: 2,
            moveSlides: 4,
            slideMargin: 5,
            responsive: true,
            infiniteLoop: false,
			auto: true,
			autoControls: true,
			stopAutoOnClick: true,
			speed: 1000					
        });
	} else { 
		$('.bxcslider').bxSlider({
            onSliderLoad: function () {
                $(".bt-box-store").css("visibility", "visible");
            },
            slideWidth: 175,
            captions: false,
            maxSlides: 6,
            moveSlides: 6,
            slideMargin: 5,
            responsive: true,
            infiniteLoop: false,
			auto: true,
			autoControls: true,
			stopAutoOnClick: true,
			speed: 1000			
        });
	} 
        
});*/
</script>

<?php echo $footer; ?>