<div id="modal-forgot" class="modal col-sm-6 custom-margin-3 ">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header" style="display:none;">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
			</div>
			<div class="modal-body modal-body-custom">
				<div class="row">
					<div class="col-sm-12" id="quick-forget">
					   
						<div class="tabbable"> <!-- Only required for left/right tabs -->
							<ul class="nav nav-login">
								<button type="button" class="close" data-dismiss="modal" aria-hidden="true" style="padding: 25px 15px;">&times;</button>
							</ul>
							
							<div class="tab-content">
							<div id="forgotformerror"></div>
								<form method="post" enctype="multipart/form-data" class="form-horizontal" id="forgotform">
                                    <div class="forgot-padding-custom">
									<h2 class="accountTitle ForgotPassword"><?php echo $text_forgotten; ?></h2>
									<p class="Qareeb-associated-email"><?php echo $text_associated_email_address; ?></p>
									<fieldset>
										<legend><?php echo $entry_email; ?></legend>
										<input type="text" name="email" value="" placeholder="<?php echo $entry_email_placeholder; ?>" id="input-email" />
									</fieldset>
									
                                    </div>
									
									<div class="custom-margin-1">
										<input type="submit" id="btnResetpassword" value="<?php echo $entry_reset_password; ?>" class="btn button_login btn-primary-custom" />
								  </div>
								</form>
							
						</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<script>
$("#forgotform").submit(function(e) {
    var form = $(this);
    var url = "<?php print($action); ?>";
	
	$.ajax({
           type: "POST",
           url: url,
		   dataType: 'json',
           data: form.serialize(), // serializes the form's elements.
           success: function(json)
           {
				if (json['error']) {
					$(".alert-danger").remove();
					$(".alert-success").remove();
					$('#forgotformerror').after('<div class="alert alert-danger" style="margin:5px;"><i class="fa fa-exclamation-circle"></i> ' + json['error'] + '</div>');
				}
				if (json['success']) {
					$(".alert-danger").remove();
					$(".alert-success").remove();
					$('#forgotformerror').after('<div class="alert alert-success" style="margin:5px;"><i class="fa fa-exclamation-circle"></i> ' + json['success'] + '</div>');
				} 
           }
         });

    e.preventDefault(); // avoid to execute the actual submit of the form.*/
});
</script>