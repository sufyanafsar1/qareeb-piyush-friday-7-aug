<?php
// header('Content-Type: text/html; charset=UTF-8');
define('_MPDF_TTFONTPATH', '/var/www/html/mpdf60/ttfonts/');
define('_MPDF_TTFONTDATAPATH', '/var/www/html/mpdf60/ttfontdata/');
include("/var/www/html/mpdf60/mpdf.php");

    $content = "";
    $content .= '<!DOCTYPE html>';
    // $content .= '<html dir="'.$direction.'" lang="'.$lang.'">';
    $content .= '<html lang="arabic-indic">';
    $content .= '<head>';
    $content .= '<meta charset="UTF-8" />';
    $content .= '<title>'.$title.'</title>';
    $content .= '<base href="'.$base.'" />';
    $content .= '<link rel="icon" type="image/png" href="https://www.qareeb.com/image/catalog/logo/favicon.png">
                    <link href="_cpanel/view/javascript/bootstrap/css/bootstrap.css" rel="stylesheet" media="all" />
                    <script type="text/javascript" src="_cpanel/view/javascript/jquery/jquery-2.1.1.min.js"></script>
                    <script type="text/javascript" src="_cpanel/view/javascript/bootstrap/js/bootstrap.min.js"></script>
                    <link href="_cpanel/view/javascript/font-awesome/css/font-awesome.min.css" type="text/css" rel="stylesheet" />
                    <link type="text/css" href="_cpanel/view/stylesheet/stylesheet.css" rel="stylesheet" media="all" />
                </head>';
    $content .= '<style>
                    .table>thead>tr>th,
                    .table>tbody>tr>th,
                    .table>tfoot>tr>th,
                    .table>thead>tr>td,
                    .table>tbody>tr>td,
                    .table>tfoot>tr>td {
                        padding: 5px;
                    }
                    </style>';
$content .= '<body style="font-size: 10px;">
        <div class="container invoice-page">';


// echo $content; upload/image/catalog/logo/Qareebnewlogo.png / QareebLogo.svg


    foreach ($orders as $order) {
        // $content .= '<div style="page-break-after: always; min-height:80%">';
        $content .= '<div style=" min-height:80%">';
        $content .= '<div class="row">
                    <table class="table">
                    <tbody>
                    <tr>
                    <td class="custom-td custom-td-new" style="width: 70%;">
                    <img src="../image/catalog/logo/Qareebnewlogo.png" style="max-width: 19%;" title="Qareeb.com " class="img-responsive" alt="Qareeb.com "></td>
                    <td class="custom-td custom-td-new invoice-no" style="width: 30%;">';
        $content .= '<h3>'.$text_invoice.' #'.$order['order_id'].'</h3>';
        $content .= '</td>
                    </tr>
                    </tbody>
                    </table>
                    </div>';
        $content .= '<table class="table table-bordered">
                    <thead>
                    <tr>';
        $content .= '<td colspan="2">'. $text_order_detail.' 
                    <span style="float: right;">
                    <b>Store Name:</b>'.$store.'
                    <span>
                    </td>';
        $content .= '</tr>
                    </thead>
                    <tbody>
                    <tr>
                    <td style="width: 50%;">';
        $content .= '<b>'.$text_shipping_address.'</b>';
        $content .= '<address style="margin-bottom:2px;">
                    '.$order['shipping_address'].'
                    </address>';
        $content .= '<b>'.$column_telephone.': </b>
                    '.$order['telephone'].'
                    </td>';
        $content .= '<td style="width: 50%;"><b>'.$text_date_added.'</b>
                    '.$order['date_added'].'<br />';

        if ($order['invoice_no']) {
            $content .= '<b>'.$text_invoice_no.'</b>'.$order['invoice_no'].'<br />';
        } 
        $content .= '<b>'.$text_payment_method.'</b>'.$order['payment_method'].'<br />'; 
        
        if ($order['delivery_time']) {
            $content .= '<b>Delivery Time</b>'.$order['delivery_time'].'<br />';
        }
        
        $content .= '</td>
                    </tr>
                    </tbody>
                    </table>';

        $content .= '<table class="table table-bordered">
                    <thead>
                    <tr>
                    <td><b>'.$column_model.'</b></td>
                    <td><b>'.$column_product. '</b></td>
                    <td class="text-right" lang="ar"><b> '.$column_quantity.'</b></td>
                    <td class="text-right" lang="ar"><b>'.$column_price. '</b></td>
                    <td class="text-right" lang="ar"><b>'.$column_total.'</b></td>
                    </tr>
                    </thead>
                    <tbody>';

    

        foreach ($order['product'] as $product) {
            $content .= '<tr ';
            if($product['product_status'] == 2){echo "class='product_not_found_info'";}
            $content .= '>';
            $content .= '<td>'.$product["model"].'</td>';
            $content .= '<td>'.$product["name"];
            
            foreach ($product['option'] as $option) { 
                $content .= "<br />
                            &nbsp;<small> - ".$option['name'].": ".$option['value']."</small>";
            } 
            $content .= '</td>';
            $content .= '<td class="text-right">'. $product['quantity'].'</td>
                        <td class="text-right">'.$product['price'].'</td>
                        <td class="text-right">'.$product['total'].'</td>
                        </tr>';
        } 
        
        foreach ($order['voucher'] as $voucher) {
            $content .= '<tr>
                        <td>'.$voucher['description'].'</td>
                        <td></td>
                        <td class="text-right">1</td>
                        <td class="text-right">'.$voucher['amount'].'</td>
                        <td class="text-right">'.$voucher['amount'].'</td>
                        </tr>';
        }
        
        foreach ($order['total'] as $total) {
            $content .= '<tr>
                        <td class="text-right" colspan="4"><b>'.$total['title'].'</b></td>
                        <td class="text-right">'.$total['text'].'</td>
                        </tr>';
        }
        
        $content .= '</tbody>
                    </table>';
            
        if ($order['comment']) {
            $content .= '<table class="table table-bordered">
                        <thead>
                        <tr>
                        <td><b>'.$text_comment.'</b></td>
                        </tr>
                        </thead>
                        <tbody>
                        <tr>
                        <td>'.$order['comment'].'</td>
                        </tr>
                        </tbody>
                        </table>';
        }
        
        $content .= '<div class="row">
                <table class="table">
                <tbody>
                <tr>
                <td class="custom-td custom-td-new" style="width: 50%;"> <b>Qareeb Saudi Trading company</b>
                </td>
                <td class="custom-td" style="width: 25%;"> <img src="_cpanel/view/image/phone.png" /> &nbsp;
                '.$order['store_telephone'].'
                </td>
                <td class="custom-td" style="width: 25%;"> <img src="_cpanel/view/image/email.png" />&nbsp;
                '.$order['store_email'].'
                </td>
                </tr>
                </tbody>
                </table>
                </div>
                </div>';
    }
    $content .= '</div>
                </body>
                </html>';



$mpdf = new \mPDF('ar', 'A4', '', '', 32, 25, 27, 25, 16, 13);
// $mpdf->debug = true;
// $mpdf = new \mPDF();
// $mpdf = new mPDF('c', 'A4', '', '', 10, 10, 10, 10, 6, 3);


$mpdf->WriteHTML($content);
// $mpdf->Output('', 'I');


// echo $download_pdf =  '/var/www/html/'.date('ymd_is').'test.pdf';

// $download_pdf =  date('ymd_is').'test.pdf';

$customer_id = $_GET['customer_id'] ? $_GET['customer_id'] : '';
$order_id    = $_GET['order_id'] ? $_GET['order_id'] : '';

$dirPath = '/var/www/html/order-invoice/';
$jsonDirPath  = $_SERVER['HTTP_HOST'].'/'.'order-invoice/';
$fileName = $order_id.'_'.$customer_id;
$fileName = md5($fileName).'.pdf';

$jsonData['fileName'] = $jsonDirPath . $fileName;
$jsonData['error']    = "";
$invoice_path = $dirPath.$fileName;
print_r(json_encode($jsonData));
$mpdf->Output($invoice_path, 'F');

// $mpdf->Output($invoice_path, 'F');
// $mpdf->Output();

// echo $content;
exit;

    ?>
    