
	<?php echo $header; ?>
	<style>
		// option {
		// 	color: #000 !important;
		// 	padding: 8px 16px !important;
		// 	border: 1px solid transparent !important;
		// 	border-color: red red red !important;
		// 	cursor: pointer !important;
		// 	user-select: none !important;
		// }
		
		.citc_select_box{
			-webkit-appearance:none; 
			-moz-appearance:none; 
			appearance:none;
			padding: 0 0 0 10px;
			background: none;
			background-color: #FAFBFD ;
			font-size: 14px;
			// color: white;
			border: none !important;
			outline: none !important;
			box-shadow: none;
		}
		.citc_select_box option
		{
			-webkit-appearance:none; 
			-moz-appearance:none; 
			appearance:none;
			margin: 30px 30px 30px 30px;
			padding: 30px 30px 30px 30px;
			font-size: 15px;
		}
		// select {
		// 	width: 100%;
		// 	font-size: 100%;
		// 	font-weight: bold;
		// 	cursor: pointer;
		// 	border-radius: 0;
		// 	background-color: transparent;
		// 	border: none;
		
		// 	padding: 10px;
		// 	padding-right: 38px;
		// 	appearance: none;
		// 	-webkit-appearance: none;
		// 	-moz-appearance: none;
		// 	}
		// 	/* For IE <= 11 */
		// select::-ms-expand {
		// 	display: none; 
		// }
		// select:hover,
		// select:focus {
		// 	background-color: transparent;
		// 	border: none;
		// }

	</style>

<div class="accountDiMain">
	<div class="container">
		<ul class="breadcrumb" style="display:none;">
			<?php foreach ($breadcrumbs as $breadcrumb) { ?>
				<li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
			<?php } ?>
		</ul>
		
		<div class="row">
			<?php //echo $column_left; ?>
			<?php if ($column_left && $column_right) { ?>
				<?php $class = 'col-sm-6'; ?>
			<?php } elseif ($column_left || $column_right) { ?>
				<?php $class = 'col-sm-9'; ?>
			<?php } else { ?>
				<?php $class = 'col-sm-12'; ?>
			<?php } ?>
			
			
<div id="content" class="col-sm-12<?php //echo $class; ?>"><?php echo $content_top; ?>
	<?php /* <h1><?php echo $heading_title; ?></h1>
	<p><?php echo $text_account_already; ?></p> */ ?>
	
	<?php if ($error_warning) { ?>
		<div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> <?php echo $error_warning; ?></div>
	<?php } ?>
	<form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data" class="form-horizontal">
		<fieldset id="registerMainTitle">
			<legend class="mainLegend"><?php echo $heading_title; ?></legend>

			<div class="col-sm-4 socialDivRegister">
				<div class="Or-Login-with">
					<span><?php echo $text_sign_u_with; ?></span>
				</div>
				<div class="SocialIconImageSignup">
					<img src="catalog/view/theme/careeb/img/facebook-login.svg" class="facebook-login">
						<img src="catalog/view/theme/careeb/img/twitter-login.svg" class="twitter-login">
				</div>
			</div>
			<div class="col-sm-2 socialDivRegister2 desktop-section">
				<div class="linesignup"></div>
				<div class="text-left"><?php echo $text_or; ?></div>
				<div class="linesignup"></div>
			</div>	

			<div class="mobile-section orLine"><span><?php echo $text_or; ?></span></div>	
			
			<div id="accountDivRegister" class="col-sm-6">				
				<?php /* <legend><?php echo $text_your_details; ?></legend> */ ?>
				<div class="form-group required" style="display: none <?php //echo (count($customer_groups) > 1 ? 'block' : 'none'); ?>;">
					<label class="col-sm-2 control-label"><?php echo $entry_customer_group; ?></label>
					<div class="col-sm-10">
						<?php 
						foreach ($customer_groups as $customer_group) { 								
							if ($customer_group['customer_group_id'] == $customer_group_id) { ?>
								<div class="radio">
									<label>
									<input type="radio" name="customer_group_id" value="<?php echo $customer_group['customer_group_id']; ?>" checked="checked" />
									<?php echo $customer_group['name']; ?></label>
								</div>
							<?php 
							} else { ?>
								<div class="radio">
									<label>
									<input type="radio" name="customer_group_id" value="<?php echo $customer_group['customer_group_id']; ?>" />
									<?php echo $customer_group['name']; ?></label>
								</div>
							<?php 
							} 
						} ?>
					</div>
				</div>
				
				<div class="col-sm-6 div6siX">
					<fieldset class="accountInput">
						<legend><?php echo $entry_firstname; ?>
						
						</legend>
						<input type="text" name="firstname" value="<?php echo $firstname; ?>" placeholder="<?php echo $entry_firstname; ?>" id="input-firstname" />					
					</fieldset>
					<?php if ($error_firstname) { ?>
						<div class="text-danger"><?php echo $error_firstname; ?></div>
					<?php } ?>
				</div>
				
				<div class="col-sm-6 div6siX">
					<fieldset class="accountInput">
						<legend><?php echo $entry_lastname; ?></legend>
						<input type="text" name="lastname" value="<?php echo $lastname; ?>" placeholder="<?php echo $entry_lastname; ?>" id="input-lastname" />					
					</fieldset>
					<?php if ($error_lastname) { ?>
						<div class="text-danger"><?php echo $error_lastname; ?></div>
					<?php } ?>
				</div>
				
				<div class="col-sm-12 div6siX">
					<fieldset class="accountInput">
						<legend><?php echo $entry_email; ?></legend>
						<input type="email" name="email" value="<?php echo $email; ?>" placeholder="<?php echo $entry_email; ?>" id="input-email" />						
					</fieldset>
					<?php if ($error_email) { ?>
						<div class="text-danger"><?php echo $error_email; ?></div>
					<?php } ?>
				</div>
				
				<div class="col-sm-12 div6siX">
					<fieldset class="accountInput">
						<legend><?php echo $entry_telephone; ?></legend>
						<input type="tel" name="telephone" value="<?php echo $telephone; ?>" placeholder="<?php echo $entry_telephone; ?>" id="input-telephone" />						
					</fieldset>
					<?php if ($error_telephone) { ?>
						<div class="text-danger"><?php echo $error_telephone; ?></div>
					<?php } ?>
				</div>
				
				  <?php foreach ($custom_fields as $custom_field) { ?>
				  <?php if ($custom_field['location'] == 'account') { ?>
				  <?php if ($custom_field['type'] == 'select') { ?>
				  <div id="custom-field<?php echo $custom_field['custom_field_id']; ?>" class="form-group custom-field" data-sort="<?php echo $custom_field['sort_order']; ?>">
					<label class="col-sm-2 control-label" for="input-custom-field<?php echo $custom_field['custom_field_id']; ?>"><?php echo $custom_field['name']; ?></label>
					<div class="col-sm-10">
					  <select name="custom_field[<?php echo $custom_field['location']; ?>][<?php echo $custom_field['custom_field_id']; ?>]" id="input-custom-field<?php echo $custom_field['custom_field_id']; ?>" class="form-control">
						<option value=""><?php echo $text_select; ?></option>
						<?php foreach ($custom_field['custom_field_value'] as $custom_field_value) { ?>
						<?php if (isset($register_custom_field[$custom_field['custom_field_id']]) && $custom_field_value['custom_field_value_id'] == $register_custom_field[$custom_field['custom_field_id']]) { ?>
						<option value="<?php echo $custom_field_value['custom_field_value_id']; ?>" selected="selected"><?php echo $custom_field_value['name']; ?></option>
						<?php } else { ?>
						<option value="<?php echo $custom_field_value['custom_field_value_id']; ?>"><?php echo $custom_field_value['name']; ?></option>
						<?php } ?>
						<?php } ?>
					  </select>
					  <?php if (isset($error_custom_field[$custom_field['custom_field_id']])) { ?>
					  <div class="text-danger"><?php echo $error_custom_field[$custom_field['custom_field_id']]; ?></div>
					  <?php } ?>
					</div>
				  </div>
				  <?php } ?>
				  <?php if ($custom_field['type'] == 'radio') { ?>
				  <div id="custom-field<?php echo $custom_field['custom_field_id']; ?>" class="form-group custom-field" data-sort="<?php echo $custom_field['sort_order']; ?>">
					<label class="col-sm-2 control-label"><?php echo $custom_field['name']; ?></label>
					<div class="col-sm-10">
					  <div>
						<?php foreach ($custom_field['custom_field_value'] as $custom_field_value) { ?>
						<div class="radio">
						  <?php if (isset($register_custom_field[$custom_field['custom_field_id']]) && $custom_field_value['custom_field_value_id'] == $register_custom_field[$custom_field['custom_field_id']]) { ?>
						  <label>
							<input type="radio" name="custom_field[<?php echo $custom_field['location']; ?>][<?php echo $custom_field['custom_field_id']; ?>]" value="<?php echo $custom_field_value['custom_field_value_id']; ?>" checked="checked" />
							<?php echo $custom_field_value['name']; ?></label>
						  <?php } else { ?>
						  <label>
							<input type="radio" name="custom_field[<?php echo $custom_field['location']; ?>][<?php echo $custom_field['custom_field_id']; ?>]" value="<?php echo $custom_field_value['custom_field_value_id']; ?>" />
							<?php echo $custom_field_value['name']; ?></label>
						  <?php } ?>
						</div>
						<?php } ?>
					  </div>
					  <?php if (isset($error_custom_field[$custom_field['custom_field_id']])) { ?>
					  <div class="text-danger"><?php echo $error_custom_field[$custom_field['custom_field_id']]; ?></div>
					  <?php } ?>
					</div>
				  </div>
				  <?php } ?>
				  <?php if ($custom_field['type'] == 'checkbox') { ?>
				  <div id="custom-field<?php echo $custom_field['custom_field_id']; ?>" class="form-group custom-field" data-sort="<?php echo $custom_field['sort_order']; ?>">
					<label class="col-sm-2 control-label"><?php echo $custom_field['name']; ?></label>
					<div class="col-sm-10">
					  <div>
						<?php foreach ($custom_field['custom_field_value'] as $custom_field_value) { ?>
						<div class="checkbox">
						  <?php if (isset($register_custom_field[$custom_field['custom_field_id']]) && in_array($custom_field_value['custom_field_value_id'], $register_custom_field[$custom_field['custom_field_id']])) { ?>
						  <label>
							<input type="checkbox" name="custom_field[<?php echo $custom_field['location']; ?>][<?php echo $custom_field['custom_field_id']; ?>][]" value="<?php echo $custom_field_value['custom_field_value_id']; ?>" checked="checked" />
							<?php echo $custom_field_value['name']; ?></label>
						  <?php } else { ?>
						  <label>
							<input type="checkbox" name="custom_field[<?php echo $custom_field['location']; ?>][<?php echo $custom_field['custom_field_id']; ?>][]" value="<?php echo $custom_field_value['custom_field_value_id']; ?>" />
							<?php echo $custom_field_value['name']; ?></label>
						  <?php } ?>
						</div>
						<?php } ?>
					  </div>
					  <?php if (isset($error_custom_field[$custom_field['custom_field_id']])) { ?>
					  <div class="text-danger"><?php echo $error_custom_field[$custom_field['custom_field_id']]; ?></div>
					  <?php } ?>
					</div>
				  </div>
				  <?php } ?>
				  <?php if ($custom_field['type'] == 'text') { ?>
				  <div id="custom-field<?php echo $custom_field['custom_field_id']; ?>" class="form-group custom-field" data-sort="<?php echo $custom_field['sort_order']; ?>">
					<label class="col-sm-2 control-label" for="input-custom-field<?php echo $custom_field['custom_field_id']; ?>"><?php echo $custom_field['name']; ?></label>
					<div class="col-sm-10">
					  <input type="text" name="custom_field[<?php echo $custom_field['location']; ?>][<?php echo $custom_field['custom_field_id']; ?>]" value="<?php echo (isset($register_custom_field[$custom_field['custom_field_id']]) ? $register_custom_field[$custom_field['custom_field_id']] : $custom_field['value']); ?>" placeholder="<?php echo $custom_field['name']; ?>" id="input-custom-field<?php echo $custom_field['custom_field_id']; ?>" class="form-control" />
					  <?php if (isset($error_custom_field[$custom_field['custom_field_id']])) { ?>
					  <div class="text-danger"><?php echo $error_custom_field[$custom_field['custom_field_id']]; ?></div>
					  <?php } ?>
					</div>
				  </div>
				  <?php } ?>
				  <?php if ($custom_field['type'] == 'textarea') { ?>
				  <div id="custom-field<?php echo $custom_field['custom_field_id']; ?>" class="form-group custom-field" data-sort="<?php echo $custom_field['sort_order']; ?>">
					<label class="col-sm-2 control-label" for="input-custom-field<?php echo $custom_field['custom_field_id']; ?>"><?php echo $custom_field['name']; ?></label>
					<div class="col-sm-10">
					  <textarea name="custom_field[<?php echo $custom_field['location']; ?>][<?php echo $custom_field['custom_field_id']; ?>]" rows="5" placeholder="<?php echo $custom_field['name']; ?>" id="input-custom-field<?php echo $custom_field['custom_field_id']; ?>" class="form-control"><?php echo (isset($register_custom_field[$custom_field['custom_field_id']]) ? $register_custom_field[$custom_field['custom_field_id']] : $custom_field['value']); ?></textarea>
					  <?php if (isset($error_custom_field[$custom_field['custom_field_id']])) { ?>
					  <div class="text-danger"><?php echo $error_custom_field[$custom_field['custom_field_id']]; ?></div>
					  <?php } ?>
					</div>
				  </div>
				  <?php } ?>
				  <?php if ($custom_field['type'] == 'file') { ?>
				  <div id="custom-field<?php echo $custom_field['custom_field_id']; ?>" class="form-group custom-field" data-sort="<?php echo $custom_field['sort_order']; ?>">
					<label class="col-sm-2 control-label"><?php echo $custom_field['name']; ?></label>
					<div class="col-sm-10">
					  <button type="button" id="button-custom-field<?php echo $custom_field['custom_field_id']; ?>" data-loading-text="<?php echo $text_loading; ?>" class="btn btn-default"><i class="fa fa-upload"></i> <?php echo $button_upload; ?></button>
					  <input type="hidden" name="custom_field[<?php echo $custom_field['location']; ?>][<?php echo $custom_field['custom_field_id']; ?>]" value="<?php echo (isset($register_custom_field[$custom_field['custom_field_id']]) ? $register_custom_field[$custom_field['custom_field_id']] : ''); ?>" />
					  <?php if (isset($error_custom_field[$custom_field['custom_field_id']])) { ?>
					  <div class="text-danger"><?php echo $error_custom_field[$custom_field['custom_field_id']]; ?></div>
					  <?php } ?>
					</div>
				  </div>
				  <?php } ?>
				  <?php if ($custom_field['type'] == 'date') { ?>
				  <div id="custom-field<?php echo $custom_field['custom_field_id']; ?>" class="form-group custom-field" data-sort="<?php echo $custom_field['sort_order']; ?>">
					<label class="col-sm-2 control-label" for="input-custom-field<?php echo $custom_field['custom_field_id']; ?>"><?php echo $custom_field['name']; ?></label>
					<div class="col-sm-10">
					  <div class="input-group date">
						<input type="text" name="custom_field[<?php echo $custom_field['location']; ?>][<?php echo $custom_field['custom_field_id']; ?>]" value="<?php echo (isset($register_custom_field[$custom_field['custom_field_id']]) ? $register_custom_field[$custom_field['custom_field_id']] : $custom_field['value']); ?>" placeholder="<?php echo $custom_field['name']; ?>" data-date-format="YYYY-MM-DD" id="input-custom-field<?php echo $custom_field['custom_field_id']; ?>" class="form-control" />
						<span class="input-group-btn">
						<button type="button" class="btn btn-default"><i class="fa fa-calendar"></i></button>
						</span></div>
					  <?php if (isset($error_custom_field[$custom_field['custom_field_id']])) { ?>
					  <div class="text-danger"><?php echo $error_custom_field[$custom_field['custom_field_id']]; ?></div>
					  <?php } ?>
					</div>
				  </div>
				  <?php } ?>
				  <?php if ($custom_field['type'] == 'time') { ?>
				  <div id="custom-field<?php echo $custom_field['custom_field_id']; ?>" class="form-group custom-field" data-sort="<?php echo $custom_field['sort_order']; ?>">
					<label class="col-sm-2 control-label" for="input-custom-field<?php echo $custom_field['custom_field_id']; ?>"><?php echo $custom_field['name']; ?></label>
					<div class="col-sm-10">
					  <div class="input-group time">
						<input type="text" name="custom_field[<?php echo $custom_field['location']; ?>][<?php echo $custom_field['custom_field_id']; ?>]" value="<?php echo (isset($register_custom_field[$custom_field['custom_field_id']]) ? $register_custom_field[$custom_field['custom_field_id']] : $custom_field['value']); ?>" placeholder="<?php echo $custom_field['name']; ?>" data-date-format="HH:mm" id="input-custom-field<?php echo $custom_field['custom_field_id']; ?>" class="form-control" />
						<span class="input-group-btn">
						<button type="button" class="btn btn-default"><i class="fa fa-calendar"></i></button>
						</span></div>
					  <?php if (isset($error_custom_field[$custom_field['custom_field_id']])) { ?>
					  <div class="text-danger"><?php echo $error_custom_field[$custom_field['custom_field_id']]; ?></div>
					  <?php } ?>
					</div>
				  </div>
				  <?php } ?>
				  <?php if ($custom_field['type'] == 'datetime') { ?>
				  <div id="custom-field<?php echo $custom_field['custom_field_id']; ?>" class="form-group custom-field" data-sort="<?php echo $custom_field['sort_order']; ?>">
					<label class="col-sm-2 control-label" for="input-custom-field<?php echo $custom_field['custom_field_id']; ?>"><?php echo $custom_field['name']; ?></label>
					<div class="col-sm-10">
					  <div class="input-group datetime">
						<input type="text" name="custom_field[<?php echo $custom_field['location']; ?>][<?php echo $custom_field['custom_field_id']; ?>]" value="<?php echo (isset($register_custom_field[$custom_field['custom_field_id']]) ? $register_custom_field[$custom_field['custom_field_id']] : $custom_field['value']); ?>" placeholder="<?php echo $custom_field['name']; ?>" data-date-format="YYYY-MM-DD HH:mm" id="input-custom-field<?php echo $custom_field['custom_field_id']; ?>" class="form-control" />
						<span class="input-group-btn">
						<button type="button" class="btn btn-default"><i class="fa fa-calendar"></i></button>
						</span></div>
					  <?php if (isset($error_custom_field[$custom_field['custom_field_id']])) { ?>
					  <div class="text-danger"><?php echo $error_custom_field[$custom_field['custom_field_id']]; ?></div>
					  <?php } ?>
					</div>
				  </div>
				  <?php } ?>
				  <?php } ?>
				<?php } ?>
			
				<div class="col-sm-12 div6siX">
					<fieldset class="accountInput">
						<legend><?php echo $entry_address_1; ?></legend>
						<input type="text" name="address_1" value="<?php echo $address_1; ?>" placeholder="<?php echo $entry_address_1; ?>" id="input-address-1" />						
					</fieldset>
					<?php if ($error_address_1) { ?>
						<div class="text-danger"><?php echo $error_address_1; ?></div>
					<?php } ?>
				</div>
								
				<!-- <div class="col-sm-12 div6siX">
					<fieldset class="accountInput">
						<legend><?php echo $entry_city; ?></legend>
						<input type="text" name="city" value="<?php echo $city; ?>" placeholder="<?php echo $entry_city; ?>" id="input-city" />						
					</fieldset>
					<?php if ($error_city) { ?>
						<div class="text-danger"><?php echo $error_city; ?></div>
					<?php } ?>
				</div> -->
				<div class="col-sm-12 div6siX">
				
					<fieldset class="accountInput">
						<legend><?php echo $entry_region ?></legend>
						<select name="citc_region_id" class="citc_select_box" id="select_citc_region">
							<option class="select_option" selected disabled >Select Rigion</option>
							<?php foreach($citc_region  as $region) { 
								if($citc_region_id == $region['region_id']) { ?>
									<option selected value="<?php echo $region['region_id'] ?>" class="select_option"><?php echo $region['regionName'] ?></option>
								<?php } else { ?>
									<option value="<?php echo $region['region_id'] ?>" class="select_option"><?php echo $region['regionName'] ?></option>
								<?php } 
							} ?>
						</select>
					</fieldset>					
					<?php if ($error_citc_region) { ?>
						<div class="text-danger"><?php echo $error_citc_region; ?></div>
					<?php } ?>
				</div>
				
				<div class="col-sm-12 div6siX">
					<fieldset class="accountInput">
						<legend><?php echo $entry_city ?></legend>
						<select name="citc_city_id" id="select_citc_city" style="padding: 0 0 0 10px; background: transparent; box-shadow: none">
							<option selected disabled >Select City</option>
						</select>
					</fieldset>
					<?php if ($error_citc_city) { ?>
						<div class="text-danger"><?php echo $error_citc_city; ?></div>
					<?php } ?>
				</div>
				<div class="col-sm-12 div6siX">
					<fieldset class="accountInput">
						<legend><?php echo $entry_password; ?></legend>
						<input type="password" name="password" value="<?php echo $password; ?>" placeholder="<?php echo $entry_password; ?>" id="input-password" style="border: 0px !important; padding-left: 10px; background: #fafbfd;" />						
					</fieldset>
					<?php if ($error_password) { ?>
						<div class="text-danger"><?php echo $error_password; ?></div>
					<?php } ?>
				</div>
				<div class="col-sm-12 div6siX">
					<fieldset class="accountInput">
						<legend><?php echo $entry_confirm; ?></legend>
						<input type="password" name="confirm" value="<?php echo $confirm; ?>" placeholder="<?php echo $entry_confirm; ?>" id="input-confirmpassword" style="border: 0px !important; padding-left: 10px; background: #fafbfd;" />						
					</fieldset>
					
				</div>
				<div class="col-sm-12 div6siX">
				  <?php //echo $captcha; ?>
				  <div class="g-recaptcha" data-sitekey="6Ld1PsUZAAAAAPZjo5T4cCSHdyioNkpeVbLGCIPq"></div>
				  <?php if ($error_recaptcha) { ?>
						<div class="text-danger"><?php echo $error_recaptcha; ?></div>
					<?php } ?>
				 </div>
				  <?php foreach ($custom_fields as $custom_field) { ?>
				  <?php if ($custom_field['location'] == 'address') { ?>
				  <?php if ($custom_field['type'] == 'select') { ?>
				  <div id="custom-field<?php echo $custom_field['custom_field_id']; ?>" class="form-group custom-field" data-sort="<?php echo $custom_field['sort_order']; ?>">
					<label class="col-sm-2 control-label" for="input-custom-field<?php echo $custom_field['custom_field_id']; ?>"><?php echo $custom_field['name']; ?></label>
					<div class="col-sm-10">
					  <select name="custom_field[<?php echo $custom_field['location']; ?>][<?php echo $custom_field['custom_field_id']; ?>]" id="input-custom-field<?php echo $custom_field['custom_field_id']; ?>" class="form-control">
						<option value=""><?php echo $text_select; ?></option>
						<?php foreach ($custom_field['custom_field_value'] as $custom_field_value) { ?>
						<?php if (isset($register_custom_field[$custom_field['custom_field_id']]) && $custom_field_value['custom_field_value_id'] == $register_custom_field[$custom_field['custom_field_id']]) { ?>
						<option value="<?php echo $custom_field_value['custom_field_value_id']; ?>" selected="selected"><?php echo $custom_field_value['name']; ?></option>
						<?php } else { ?>
						<option value="<?php echo $custom_field_value['custom_field_value_id']; ?>"><?php echo $custom_field_value['name']; ?></option>
						<?php } ?>
						<?php } ?>
					  </select>
					  <?php if (isset($error_custom_field[$custom_field['custom_field_id']])) { ?>
					  <div class="text-danger"><?php echo $error_custom_field[$custom_field['custom_field_id']]; ?></div>
					  <?php } ?>
					</div>
				  </div>
				  <?php } ?>
				  <?php if ($custom_field['type'] == 'radio') { ?>
				  <div id="custom-field<?php echo $custom_field['custom_field_id']; ?>" class="form-group custom-field" data-sort="<?php echo $custom_field['sort_order']; ?>">
					<label class="col-sm-2 control-label"><?php echo $custom_field['name']; ?></label>
					<div class="col-sm-10">
					  <div>
						<?php foreach ($custom_field['custom_field_value'] as $custom_field_value) { ?>
						<div class="radio">
						  <?php if (isset($register_custom_field[$custom_field['custom_field_id']]) && $custom_field_value['custom_field_value_id'] == $register_custom_field[$custom_field['custom_field_id']]) { ?>
						  <label>
							<input type="radio" name="custom_field[<?php echo $custom_field['location']; ?>][<?php echo $custom_field['custom_field_id']; ?>]" value="<?php echo $custom_field_value['custom_field_value_id']; ?>" checked="checked" />
							<?php echo $custom_field_value['name']; ?></label>
						  <?php } else { ?>
						  <label>
							<input type="radio" name="custom_field[<?php echo $custom_field['location']; ?>][<?php echo $custom_field['custom_field_id']; ?>]" value="<?php echo $custom_field_value['custom_field_value_id']; ?>" />
							<?php echo $custom_field_value['name']; ?></label>
						  <?php } ?>
						</div>
						<?php } ?>
					  </div>
					  <?php if (isset($error_custom_field[$custom_field['custom_field_id']])) { ?>
					  <div class="text-danger"><?php echo $error_custom_field[$custom_field['custom_field_id']]; ?></div>
					  <?php } ?>
					</div>
				  </div>
				  <?php } ?>
				  <?php if ($custom_field['type'] == 'checkbox') { ?>
				  <div id="custom-field<?php echo $custom_field['custom_field_id']; ?>" class="form-group custom-field" data-sort="<?php echo $custom_field['sort_order']; ?>">
					<label class="col-sm-2 control-label"><?php echo $custom_field['name']; ?></label>
					<div class="col-sm-10">
					  <div>
						<?php foreach ($custom_field['custom_field_value'] as $custom_field_value) { ?>
						<div class="checkbox">
						  <?php if (isset($register_custom_field[$custom_field['custom_field_id']]) && in_array($custom_field_value['custom_field_value_id'], $register_custom_field[$custom_field['custom_field_id']])) { ?>
						  <label>
							<input type="checkbox" name="custom_field[<?php echo $custom_field['location']; ?>][<?php echo $custom_field['custom_field_id']; ?>][]" value="<?php echo $custom_field_value['custom_field_value_id']; ?>" checked="checked" />
							<?php echo $custom_field_value['name']; ?></label>
						  <?php } else { ?>
						  <label>
							<input type="checkbox" name="custom_field[<?php echo $custom_field['location']; ?>][<?php echo $custom_field['custom_field_id']; ?>][]" value="<?php echo $custom_field_value['custom_field_value_id']; ?>" />
							<?php echo $custom_field_value['name']; ?></label>
						  <?php } ?>
						</div>
						<?php } ?>
					  </div>
					  <?php if (isset($error_custom_field[$custom_field['custom_field_id']])) { ?>
					  <div class="text-danger"><?php echo $error_custom_field[$custom_field['custom_field_id']]; ?></div>
					  <?php } ?>
					</div>
				  </div>
				  <?php } ?>
				  <?php if ($custom_field['type'] == 'text') { ?>
				  <div id="custom-field<?php echo $custom_field['custom_field_id']; ?>" class="form-group custom-field" data-sort="<?php echo $custom_field['sort_order']; ?>">
					<label class="col-sm-2 control-label" for="input-custom-field<?php echo $custom_field['custom_field_id']; ?>"><?php echo $custom_field['name']; ?></label>
					<div class="col-sm-10">
					  <input type="text" name="custom_field[<?php echo $custom_field['location']; ?>][<?php echo $custom_field['custom_field_id']; ?>]" value="<?php echo (isset($register_custom_field[$custom_field['custom_field_id']]) ? $register_custom_field[$custom_field['custom_field_id']] : $custom_field['value']); ?>" placeholder="<?php echo $custom_field['name']; ?>" id="input-custom-field<?php echo $custom_field['custom_field_id']; ?>" class="form-control" />
					  <?php if (isset($error_custom_field[$custom_field['custom_field_id']])) { ?>
					  <div class="text-danger"><?php echo $error_custom_field[$custom_field['custom_field_id']]; ?></div>
					  <?php } ?>
					</div>
				  </div>
				  <?php } ?>
				  <?php if ($custom_field['type'] == 'textarea') { ?>
				  <div id="custom-field<?php echo $custom_field['custom_field_id']; ?>" class="form-group custom-field" data-sort="<?php echo $custom_field['sort_order']; ?>">
					<label class="col-sm-2 control-label" for="input-custom-field<?php echo $custom_field['custom_field_id']; ?>"><?php echo $custom_field['name']; ?></label>
					<div class="col-sm-10">
					  <textarea name="custom_field[<?php echo $custom_field['location']; ?>][<?php echo $custom_field['custom_field_id']; ?>]" rows="5" placeholder="<?php echo $custom_field['name']; ?>" id="input-custom-field<?php echo $custom_field['custom_field_id']; ?>" class="form-control"><?php echo (isset($register_custom_field[$custom_field['custom_field_id']]) ? $register_custom_field[$custom_field['custom_field_id']] : $custom_field['value']); ?></textarea>
					  <?php if (isset($error_custom_field[$custom_field['custom_field_id']])) { ?>
					  <div class="text-danger"><?php echo $error_custom_field[$custom_field['custom_field_id']]; ?></div>
					  <?php } ?>
					</div>
				  </div>
				  <?php } ?>
				  <?php if ($custom_field['type'] == 'file') { ?>
				  <div id="custom-field<?php echo $custom_field['custom_field_id']; ?>" class="form-group custom-field" data-sort="<?php echo $custom_field['sort_order']; ?>">
					<label class="col-sm-2 control-label"><?php echo $custom_field['name']; ?></label>
					<div class="col-sm-10">
					  <button type="button" id="button-custom-field<?php echo $custom_field['custom_field_id']; ?>" data-loading-text="<?php echo $text_loading; ?>" class="btn btn-default"><i class="fa fa-upload"></i> <?php echo $button_upload; ?></button>
					  <input type="hidden" name="custom_field[<?php echo $custom_field['location']; ?>][<?php echo $custom_field['custom_field_id']; ?>]" value="<?php echo (isset($register_custom_field[$custom_field['custom_field_id']]) ? $register_custom_field[$custom_field['custom_field_id']] : ''); ?>" />
					  <?php if (isset($error_custom_field[$custom_field['custom_field_id']])) { ?>
					  <div class="text-danger"><?php echo $error_custom_field[$custom_field['custom_field_id']]; ?></div>
					  <?php } ?>
					</div>
				  </div>
				  <?php } ?>
				  <?php if ($custom_field['type'] == 'date') { ?>
				  <div id="custom-field<?php echo $custom_field['custom_field_id']; ?>" class="form-group custom-field" data-sort="<?php echo $custom_field['sort_order']; ?>">
					<label class="col-sm-2 control-label" for="input-custom-field<?php echo $custom_field['custom_field_id']; ?>"><?php echo $custom_field['name']; ?></label>
					<div class="col-sm-10">
					  <div class="input-group date">
						<input type="text" name="custom_field[<?php echo $custom_field['location']; ?>][<?php echo $custom_field['custom_field_id']; ?>]" value="<?php echo (isset($register_custom_field[$custom_field['custom_field_id']]) ? $register_custom_field[$custom_field['custom_field_id']] : $custom_field['value']); ?>" placeholder="<?php echo $custom_field['name']; ?>" data-date-format="YYYY-MM-DD" id="input-custom-field<?php echo $custom_field['custom_field_id']; ?>" class="form-control" />
						<span class="input-group-btn">
						<button type="button" class="btn btn-default"><i class="fa fa-calendar"></i></button>
						</span></div>
					  <?php if (isset($error_custom_field[$custom_field['custom_field_id']])) { ?>
					  <div class="text-danger"><?php echo $error_custom_field[$custom_field['custom_field_id']]; ?></div>
					  <?php } ?>
					</div>
				  </div>
				  <?php } ?>
				  <?php if ($custom_field['type'] == 'time') { ?>
				  <div id="custom-field<?php echo $custom_field['custom_field_id']; ?>" class="form-group custom-field" data-sort="<?php echo $custom_field['sort_order']; ?>">
					<label class="col-sm-2 control-label" for="input-custom-field<?php echo $custom_field['custom_field_id']; ?>"><?php echo $custom_field['name']; ?></label>
					<div class="col-sm-10">
					  <div class="input-group time">
						<input type="text" name="custom_field[<?php echo $custom_field['location']; ?>][<?php echo $custom_field['custom_field_id']; ?>]" value="<?php echo (isset($register_custom_field[$custom_field['custom_field_id']]) ? $register_custom_field[$custom_field['custom_field_id']] : $custom_field['value']); ?>" placeholder="<?php echo $custom_field['name']; ?>" data-date-format="HH:mm" id="input-custom-field<?php echo $custom_field['custom_field_id']; ?>" class="form-control" />
						<span class="input-group-btn">
						<button type="button" class="btn btn-default"><i class="fa fa-calendar"></i></button>
						</span></div>
					  <?php if (isset($error_custom_field[$custom_field['custom_field_id']])) { ?>
					  <div class="text-danger"><?php echo $error_custom_field[$custom_field['custom_field_id']]; ?></div>
					  <?php } ?>
					</div>
				  </div>
				  <?php } ?>
				  <?php if ($custom_field['type'] == 'datetime') { ?>
				  <div id="custom-field<?php echo $custom_field['custom_field_id']; ?>" class="form-group custom-field" data-sort="<?php echo $custom_field['sort_order']; ?>">
					<label class="col-sm-2 control-label" for="input-custom-field<?php echo $custom_field['custom_field_id']; ?>"><?php echo $custom_field['name']; ?></label>
					<div class="col-sm-10">
					  <div class="input-group datetime">
						<input type="text" name="custom_field[<?php echo $custom_field['location']; ?>][<?php echo $custom_field['custom_field_id']; ?>]" value="<?php echo (isset($register_custom_field[$custom_field['custom_field_id']]) ? $register_custom_field[$custom_field['custom_field_id']] : $custom_field['value']); ?>" placeholder="<?php echo $custom_field['name']; ?>" data-date-format="YYYY-MM-DD HH:mm" id="input-custom-field<?php echo $custom_field['custom_field_id']; ?>" class="form-control" />
						<span class="input-group-btn">
						<button type="button" class="btn btn-default"><i class="fa fa-calendar"></i></button>
						</span></div>
					  <?php if (isset($error_custom_field[$custom_field['custom_field_id']])) { ?>
					  <div class="text-danger"><?php echo $error_custom_field[$custom_field['custom_field_id']]; ?></div>
					  <?php } ?>
					</div>
				  </div>
				  <?php } ?>
				  <?php } ?>
				<?php } ?>
				<div class="col-sm-12">
					<p class="Subscribe-to-our-new">
					<?php 
					if ($newsletter) { ?>
						<input type="checkbox" name="newsletter" value="1" checked="checked" />
					<?php 
					} else { ?>
						<input type="checkbox" name="newsletter" value="1" />
					<?php 
					} ?><?php echo $entry_newsletter; ?></p>
				</div>
				
				<div class="col-sm-12">
					<?php 
					if ($text_agree) { ?>
						<div class="buttons">
						  <div class="text-center">
							<input type="submit" value="<?php echo $button_continue; ?>" class="btn btn-primary registerBtn" />						
						  </div>
						</div>
						
						<div class="buttons">
						  <div class="text-center">						
							<p class="terms-condition"><?php echo $text_agree; ?></p>						
							<input type="hidden" name="agree" value="1" checked="checked" />						
						  </div>
						</div>
					<?php 
					} else { ?>
						<div class="buttons">
						  <div class="pull-right">
							<input type="submit" value="<?php echo $button_continue; ?>" class="btn btn-primary" />
						  </div>
						</div>
					<?php 
					} ?>
				</div>
			</div>
		</fieldset>
	</form>
	<?php echo $content_bottom; ?>
</div>
<?php echo $column_right; ?>
			
			
			
		</div>
	</div>
</div>
<script src='https://www.google.com/recaptcha/api.js' async defer >
<script type="text/javascript">
{literal}

{/literal}
</script>
<script type="text/javascript">
$(document).ready(() => {
	let citc_region_id = $('#select_citc_region').val();
	// console.log(citc_region_id);
	// Set city on page load if Region id value is selected
	if(citc_region_id) {
		set_citc_city(citc_region_id);
	}
	// Set city on Region change
	$('#select_citc_region').on('change', function(e) {
		let citc_region_id = $('#select_citc_region').val();
		// console.log(citc_region_id);
		set_citc_city(citc_region_id);
	});
});
function set_citc_city(citc_region_id) {
	$.ajax({
		url: 'index.php?route=account/register/get_citc_city',
		type: 'post',
		data: {citc_region_id: citc_region_id},
		dataType: 'json',
		success: (json) => {
			let output = [];
			if(!'<?php echo $citc_city_id ?>') {
				output.push(`<option selected disabled > Select City</option>`);
			}
			json.data.forEach((city) => {
				if (city.city_id == '<?php echo $citc_city_id ?>') {
					output.push(`<option selected value="${city.city_id}|${city.city_name}"> ${city.city_name} </option>`);
				} else {
					output.push(`<option value="${city.city_id}|${city.city_name}"> ${city.city_name} </option>`);
				}
			});
			$('#select_citc_city').html(output.join(''));
		},
		error: (error) => {
			console.log(`error: ${error}`)
		}
	});
}
</script>

<script type="text/javascript"><!--



// Sort the custom fields
$('#account .form-group[data-sort]').detach().each(function() {
	if ($(this).attr('data-sort') >= 0 && $(this).attr('data-sort') <= $('#account .form-group').length) {
		$('#account .form-group').eq($(this).attr('data-sort')).before(this);
	}

	if ($(this).attr('data-sort') > $('#account .form-group').length) {
		$('#account .form-group:last').after(this);
	}

	if ($(this).attr('data-sort') < -$('#account .form-group').length) {
		$('#account .form-group:first').before(this);
	}
});

$('#address .form-group[data-sort]').detach().each(function() {
	if ($(this).attr('data-sort') >= 0 && $(this).attr('data-sort') <= $('#address .form-group').length) {
		$('#address .form-group').eq($(this).attr('data-sort')).before(this);
	}

	if ($(this).attr('data-sort') > $('#address .form-group').length) {
		$('#address .form-group:last').after(this);
	}

	if ($(this).attr('data-sort') < -$('#address .form-group').length) {
		$('#address .form-group:first').before(this);
	}
});

$('input[name=\'customer_group_id\']').on('change', function() {
	$.ajax({
		url: 'index.php?route=account/register/customfield&customer_group_id=' + this.value,
		dataType: 'json',
		success: function(json) {
			$('.custom-field').hide();
			$('.custom-field').removeClass('required');

			for (i = 0; i < json.length; i++) {
				custom_field = json[i];

				$('#custom-field' + custom_field['custom_field_id']).show();

				if (custom_field['required']) {
					$('#custom-field' + custom_field['custom_field_id']).addClass('required');
				}
			}


		},
		error: function(xhr, ajaxOptions, thrownError) {
			alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
		}
	});
});

$('input[name=\'customer_group_id\']:checked').trigger('change');
//--></script>
<script type="text/javascript"><!--
$('button[id^=\'button-custom-field\']').on('click', function() {
	var node = this;

	$('#form-upload').remove();

	$('body').prepend('<form enctype="multipart/form-data" id="form-upload" style="display: none;"><input type="file" name="file" /></form>');

	$('#form-upload input[name=\'file\']').trigger('click');

	if (typeof timer != 'undefined') {
    	clearInterval(timer);
	}

	timer = setInterval(function() {
		if ($('#form-upload input[name=\'file\']').val() != '') {
			clearInterval(timer);

			$.ajax({
				url: 'index.php?route=tool/upload',
				type: 'post',
				dataType: 'json',
				data: new FormData($('#form-upload')[0]),
				cache: false,
				contentType: false,
				processData: false,
				beforeSend: function() {
					$(node).button('loading');
				},
				complete: function() {
					$(node).button('reset');
				},
				success: function(json) {
					$(node).parent().find('.text-danger').remove();

					if (json['error']) {
						$(node).parent().find('input').after('<div class="text-danger">' + json['error'] + '</div>');
					}

					if (json['success']) {
						alert(json['success']);

						$(node).parent().find('input').attr('value', json['code']);
					}
				},
				error: function(xhr, ajaxOptions, thrownError) {
					alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
				}
			});
		}
	}, 500);
});
//--></script>
<script type="text/javascript"><!--
$('.date').datetimepicker({
	pickTime: false
});

$('.time').datetimepicker({
	pickDate: false
});

$('.datetime').datetimepicker({
	pickDate: true,
	pickTime: true
});
//--></script>
<script type="text/javascript"><!--
$('select[name=\'country_id\']').on('change', function() {
	$.ajax({
		url: 'index.php?route=account/account/country&country_id=' + this.value,
		dataType: 'json',
		beforeSend: function() {
			$('select[name=\'country_id\']').after(' <i class="fa fa-circle-o-notch fa-spin"></i>');
		},
		complete: function() {
			$('.fa-spin').remove();
		},
		success: function(json) {
			if (json['postcode_required'] == '1') {
				$('input[name=\'postcode\']').parent().parent().addClass('required');
			} else {
				$('input[name=\'postcode\']').parent().parent().removeClass('required');
			}

			html = '<option value=""><?php echo $text_select; ?></option>';

			if (json['zone'] && json['zone'] != '') {
				for (i = 0; i < json['zone'].length; i++) {
					html += '<option value="' + json['zone'][i]['zone_id'] + '"';

					if (json['zone'][i]['zone_id'] == "<?php echo $zone_id; ?>") {
						html += ' selected="selected"';
					}

					html += '>' + json['zone'][i]['name'] + '</option>';
				}
			} else {
				html += '<option value="0" selected="selected"><?php echo $text_none; ?></option>';
			}

			$('select[name=\'zone_id\']').html(html);
		},
		error: function(xhr, ajaxOptions, thrownError) {
			alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
		}
	});
});

$('select[name=\'country_id\']').trigger('change');
//--></script>
<?php echo $footer; ?>
