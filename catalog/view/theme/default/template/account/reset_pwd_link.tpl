<?php echo $header; ?>
<div class="bt-breadcrumb">
	<div class="container-fluid container">
		<ul class="breadcrumb">
			<?php foreach ($breadcrumbs as $breadcrumb) { ?>
			<li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
			<?php } ?>
		</ul>
	</div>
</div>

<div class="container-fluid container">
	<div class="">
		
		<?php echo $column_left; ?>
		
		<?php if ($column_left && $column_right) { ?>
			<?php $class = ' '; ?>
		<?php } elseif ($column_left || $column_right) { ?>
			<?php $class = 'col-sm-9'; ?>
		<?php } else { ?>
			<?php $class = 'col-sm-12'; ?>
		<?php } ?>
		
		<div id="content" class="background-white form-horizontal <?php echo $class; ?>">
		
			<?php echo $content_top; ?>
			
			<div class="content_bg">
			
				<?php if ($error_warning) { ?>
					<div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> <?php echo $error_warning; ?></div>
				<?php } ?>
				
				<form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data" class="form-horizontal">
				
					<h2 class="accountTitle"><?php echo $heading_title; ?></h2>
					<p><?php echo $text_password; ?></p>

					<fieldset class="accountDiv">
						<div class="required col-sm-4">
							<label>Your Password / Confirm Password</label>
						</div>
						<div class="required col-sm-8">
							<div class="required col-sm-6">								
								<div>
									<input type="password" name="password" value="<?php echo $password; ?>" placeholder="Enter your password" id="input-password" class="form-control" />			
								</div>
							</div>
							
							<div class="required col-sm-6">								
								<div>
									<input type="password" name="confirm" value="<?php echo $confirm; ?>" id="input-confirm" class="form-control" placeholder="Enter your confirm password"/>		
								</div>
							</div>
							<input type="hidden" id="" name="email" value="<?php if(isset($email)){ echo $email; } ?>">
							<div class="buttons clearfix col-sm-12">								
								<div class="pull-right">
									<input type="submit" value="<?php echo $button_continue; ?>" class="btn btn-primary" />
									<a href="<?php echo $cancel; ?>" class="btn"><?php echo $button_cancel; ?></a>
								</div>
							</div>
						</div>
					</fieldset>
				</form>
				
				<?php echo $content_bottom; ?>
			</div>
			<?php echo $column_right; ?>
		</div>
	</div>
</div>
<?php echo $footer; ?>