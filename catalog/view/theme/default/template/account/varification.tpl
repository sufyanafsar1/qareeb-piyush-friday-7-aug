<?php echo $header; ?>
<div class="accountDiMain">
	<div class="container">
		<ul class="breadcrumb" style="display:none;">
			<?php foreach ($breadcrumbs as $breadcrumb) { ?>
				<li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
			<?php } ?>
		</ul>
		
		<?php if ($error_warning) { ?>
			<div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> <?php echo $error_warning; ?></div>
		<?php } ?>
		
		<div class="row">
			<?php //echo $column_left; ?>
			<?php if ($column_left && $column_right) { ?>
				<?php $class = 'col-sm-6'; ?>
			<?php } elseif ($column_left || $column_right) { ?>
				<?php $class = 'col-sm-9'; ?>
			<?php } else { ?>
				<?php $class = 'col-sm-12'; ?>
			<?php } ?>
			
			
<div id="content" class="col-sm-12<?php //echo $class; ?>"><?php echo $content_top; ?>	
	
	<form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data" class="form-horizontal">
		<fieldset id="registerMainTitle">
			<p><?php echo $text_varification; ?></p>
			<legend class="mainLegend"><?php echo $heading_title; ?></legend>

			<div id="accountDivRegister">	
				<input type="hidden" name="telephone" value="<?php echo $telephone; ?>"  id="input-telephone" class="form-control" />
				<input type="hidden" name="passwordSent" value="<?php echo $passwordSent; ?>"  id="input-passwordSent" class="form-control" />
                 
				<input type="text" name="password" value="<?php echo $password; ?>" placeholder="<?php echo $entry_password; ?>"  id="input-password" class="form-control" />                    
								
				<input type="submit" value="<?php echo $button_continue; ?>" class="btn btn-primary" />		
				<a href="<?php echo $resend; ?>" class="btn btn-primary"><?php echo $button_resend; ?></a>
			</div>
	</form>
	<?php echo $content_bottom; ?>
</div>
<?php echo $column_right; ?>
			
			
			
		</div>
	</div>
</div>
<?php echo $footer; ?>
