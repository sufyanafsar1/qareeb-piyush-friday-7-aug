<label><?php echo $text_date_select;?></label>
<select name="date_slot" id="date_slot" style="width: 80%;">
	<?php foreach ($day_slot as $key => $value): ?>
		<option value="<?php echo $key ?>" <?php if($key == 'now') echo 'selected' ?>><?php echo $value ?></option>
	<?php endforeach ?>
</select>

<label><?php echo $text_time_select;?></label>
<select name="time_slot" id="time_slot" style="width: 80%;">
	<?php foreach($time_slots['slots'] as $timeslot): ?>
		<option value="<?php echo $timeslot; ?>"><?php echo $timeslot; ?></option>
	<?php endforeach; ?>
</select>
<div class="clearfix"></div>
<div class="buttons clearfix">
    <div class="pull-right">
      <input type="button" value="<?php echo $button_continue; ?>" id="button-datetime" data-loading-text="<?php echo $text_loading; ?>" class="btn btn-primary <?php if($time_slots['slots'][0] == 'Store Closed') echo 'disabled'?>" <?php if($time_slots["slots"][0] == 'Store Closed') echo 'disabled'?>/>
    </div>
</div>