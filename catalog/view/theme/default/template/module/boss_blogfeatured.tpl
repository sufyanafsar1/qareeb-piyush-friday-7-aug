<div class="boss-blog-featured">
	<div class="container">
	<div class="row">
		<div class="block-heading"><span><?php echo $heading_title; ?><b class="line not-animated" data-animate="slide-in-right" data-delay="100">&nbsp;</b></span></div>
		<div class="block-content">
			<div class="box-article">
				<?php  if($use_slider){ ?>
				<ul id="slider-article<?php echo $module; ?>">
					<?php foreach ($articles as $article) { ?>
					<li><div class="article_content"><div class="article-item"><div class="std">
						<?php if ($article['thumb']) { ?>
							<div class="image">
								<a href="<?php echo $article['href']; ?>" title="<?php echo $article['name']; ?>"><img src="<?php echo $article['thumb']; ?>" alt="<?php echo $article['name']; ?>" /></a>
								<?php $date = new DateTime($article['date_added']);?>
								<div class="time-stamp"> 
									<small class="time-date">							
									<?php echo $date->format('d');?></small>
									<small class="time-month">							
									<?php echo $date->format('M');?></small>									
								</div>	
							</div>
						<?php } ?>						
							<div class="article-name"><a href="<?php echo $article['href']; ?>" title="<?php echo $article['name']; ?>"><?php echo $article['name']; ?></a></div>
							<div class="time_comment">
								<?php $date = new DateTime($article['date_added']);?>
								<small class="time-month">							
								<?php echo $date->format('M');?></small> 
								<small class="time-month">							
								<?php echo $date->format('d');?></small>,
								<small class="time-year">							
								<?php echo $date->format('Y');?></small> 
								&nbsp;&nbsp; | &nbsp;&nbsp;
								<span class="post-by"><span><?php echo $text_by.' '. $article['author']; ?></span></span>	
								&nbsp;&nbsp; | &nbsp;&nbsp;		
								<span class="comment-count"><a href="<?php echo $article['href']; ?>"><?php echo $article['comment']. ' '.$text_comment; ?></a></span>
								
							</div>	
							<div class="description"><p><?php echo $article['content']; ?></p></div>
							
						</div>								
					</div></div></li>
					<?php } ?>
				</ul>
				<a id="article_prev<?php echo $module; ?>" class="btn-nav-center prev nav_thumb" href="javascript:void(0)" title="prev"><i class="fa fa-angle-left"></i></a>
				<a id="article_next<?php echo $module; ?>" class="btn-nav-center next nav_thumb" href="javascript:void(0)" title="next"><i class="fa fa-angle-right"></i></a>
				<?php }else{  ?>
				<div class="row">
					<?php $i=100; foreach ($articles as $article) { ?>
					<div class="col-sm-3 col-xs-12">
					<div class="article_content not-animated" data-animate="slide-in-up" data-delay="<?php echo $i; ?>"><div class="article-item"><div class="std">
						<?php if ($article['thumb']) { ?>
							<div class="image">
								<a href="<?php echo $article['href']; ?>" title="<?php echo $article['name']; ?>"><img src="<?php echo $article['thumb']; ?>" alt="<?php echo $article['name']; ?>" /></a>
								<?php $date = new DateTime($article['date_added']);?>
								<div class="time-stamp"> 
									<small class="time-date">							
									<?php echo $date->format('d');?></small>
									<small class="time-month">							
									<?php echo $date->format('M');?></small>									
								</div>	
							</div>
						<?php } ?>						
							<div class="article-name"><a href="<?php echo $article['href']; ?>" title="<?php echo $article['name']; ?>"><?php echo $article['name']; ?></a></div>
							<div class="time_comment">
								<?php $date = new DateTime($article['date_added']);?>
								<small class="time-month">							
								<?php echo $date->format('M');?></small> 
								<small class="time-month">							
								<?php echo $date->format('d');?></small>,
								<small class="time-year">							
								<?php echo $date->format('Y');?></small> 
								&nbsp;&nbsp; | &nbsp;&nbsp;
								<span class="post-by"><span><?php echo $text_by.' '. $article['author']; ?></span></span>	
								&nbsp;&nbsp; | &nbsp;&nbsp;		
								<span class="comment-count"><a href="<?php echo $article['href']; ?>"><?php echo $article['comment']. ' '.$text_comment; ?></a></span>
								
							</div>	
							<div class="description"><p><?php echo $article['content']; ?></p></div>
							
						</div>								
					</div></div>
					</div>
					<?php $i=$i+100; } ?>
				</div>
				<?php } ?>
			</div>
		</div>
	</div>
<?php if($use_slider){ ?>
<script type="text/javascript"><!--
$(window).load(function(){
    $('#slider-article<?php echo $module; ?>').carouFredSel({
        auto: false,
        responsive: true,
        width: '100%',
        next: '#article_next<?php echo $module; ?>',
        prev: '#article_prev<?php echo $module; ?>',
        swipe: {
			onTouch : false
        },
        items: {
            width: 500,
            height: 'auto',
            visible: {
            min: 1,
            max: 3
            }
        },
        scroll: {
            direction : 'left',    //  The direction of the transition.
            duration  : 1000   //  The duration of the transition.
        }
    });
});
//--></script>
<?php } ?>
</div>
</div>