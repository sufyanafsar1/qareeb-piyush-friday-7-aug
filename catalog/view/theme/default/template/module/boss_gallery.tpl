<div class="boss_flickr">
	<?php if(isset($heading_title) && $heading_title!=''){ ?>
    <div class="box-heading block-title">
        <span><?php echo $heading_title; ?></span>
    </div>
	<?php } ?>
	
	<?php if(isset($gallery_images) && !empty($gallery_images)) { ?>
    <div class="boss_flickr_photo">
        <?php foreach ($gallery_images as $gallery_image) { ?>
		<a target="_blank" href="<?php echo $gallery_image['link'] ?>" title="<?php echo $gallery_image['gallery_image_description'];?>" class="<?php echo $gallery_image['class']; ?>"><img  src="<?php echo $gallery_image['image'] ?>" title="<?php echo $gallery_image['gallery_image_description'];?>" alt="<?php echo $gallery_image['gallery_image_description'];?>" /></a>
   
		<?php } ?>
    </div>
	<?php } ?>

</div>
