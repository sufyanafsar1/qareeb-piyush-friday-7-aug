<script src="https://beautiful.start.payfort.com/checkout.js"></script>
<script>
function configureStart() {
    StartCheckout.config({
        key: "<?php echo $open_api_key; ?>",
        formLabel: 'Pay',
        complete: function (params) {
            submitFormWithToken(params); // params.token.id, params.email
        },
        cancel: function() {
            stopProcessing();
        }
    });
}

function openStartPopup() {
    console.log('open popup');

    StartCheckout.open({
        amount: <?php echo $amount; ?>,
        currency: "<?php echo $currency; ?>",
        email: "<?php echo $email; ?>"
    });
}

function submitFormWithToken(params) {
    document.getElementById('payfortToken').value = params.token.id;
    document.getElementById('payfortEmail').value = params.email;
    document.getElementById('startForm').submit();
}

function PayfortStartSubmit(submitButton) {
    startProcessing(submitButton);

    if(typeof StartCheckout == 'undefined'){
        console.log('StartCheckout is not defined yet...');
        setTimeout(PayfortStartSubmit, 200);
    } else {
        configureStart();
        openStartPopup();
    }
}

var isProcessing, submitButton;

function startProcessing(submitBtn) {
    if (!isProcessing) {
        isProcessing = true;
        submitButton = submitBtn;
        submitButton.disabled = true;
        submitButton.value = "<?php echo $text_wait; ?>";
    }
}

function stopProcessing() {
    isProcessing = false;
    submitButton.disabled = false;
    submitButton.value = "<?php echo $button_confirm; ?>";
}
</script>

<form name="startForm" id="startForm" action="<?php echo $pay_url; ?>" method="POST">
    <input id="payfortToken" type="hidden" name="payfortToken" value=""/>
    <input id="payfortEmail" type="hidden" name="payfortEmail" value=""/>
</form>

<div class="buttons">
    <div class="pull-right">
        <input type="submit" onclick="PayfortStartSubmit(this);" value="<?php echo $button_confirm; ?>" class="btn btn-primary" />
    </div>
</div>
