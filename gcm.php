<?php

// API access key from Google API's Console
define( 'API_ACCESS_KEY', 'AAAAeDIkjlA:APA91bHZDZm47TjbOuUBP6WmfxDh66LlrI1HPmYX3G0nFQKG1FyqX7pGrEUl7QTGe9KPQ6GtnidsFbK39AMZFDkL-K1DHz58oPB1sGtTBxyQ9pc18lkoa3q3fLoHt8907EQ9c42Wrs1l' );


$registrationIds = array('dZBFgMs_n_A:APA91bHOWpqmxSkraOmj4o3-kCt4HRjCrMAZoZ4sg-I53MJOkRtVEkZgaDUWL4-6bP_d2Sg4l5g1_GwVrXPVZs79lUCulT4jXsPh7DLec0V8EN5368d7PU6EsY3PoQHeOo2UmJ4F5595');

// prep the bundle
$msg = array
(
	'message' 	=> 'here is a message. message',
	'title'		=> 'This is a title. title',
	'subtitle'	=> 'This is a subtitle. subtitle',
	'tickerText'	=> 'Ticker text here...Ticker text here...Ticker text here',
	'vibrate'	=> 1,
	'sound'		=> 1,
	'largeIcon'	=> 'large_icon',
	'smallIcon'	=> 'small_icon'
);

$fields = array
(
	'registration_ids' 	=> $registrationIds,
	'data'			=> $msg
);
 
$headers = array
(
	'Authorization: key=' . API_ACCESS_KEY,
	'Content-Type: application/json'
);
 
$ch = curl_init();
curl_setopt( $ch,CURLOPT_URL, 'https://android.googleapis.com/gcm/send' );
curl_setopt( $ch,CURLOPT_POST, true );
curl_setopt( $ch,CURLOPT_HTTPHEADER, $headers );
curl_setopt( $ch,CURLOPT_RETURNTRANSFER, true );
curl_setopt( $ch,CURLOPT_SSL_VERIFYPEER, false );
curl_setopt( $ch,CURLOPT_POSTFIELDS, json_encode( $fields ) );
$result = curl_exec($ch );
curl_close( $ch );

echo $result;