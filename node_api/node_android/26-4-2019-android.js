const express = require('express');
const bodyParser = require('body-parser');
const mysql = require('mysql');
const path = require('path');
const app = express();
const port = 3100;


DB_PREFIX = "oc_"
device_id = "";
result = {};
proArray = [];
languageid = 1;
end = 9;

const db = mysql.createConnection ({
    host: 'careeb-stage.cf9bruqved5k.us-east-2.rds.amazonaws.com',
    user: 'careeb',
    password: 'careeb1209',
    database: 'Qareeb_App'
});

app.get('/', (req,res)=> {
    res.send("Hey there come back leter");
});

app.get('/getProductBYcategoryAndLoc', function(req,res){
	var languageid = req.query.language_id;
    device_id = req.query.device_id;
	var end = req.query.end
    
    DataQuery("SELECT p.product_id, (SELECT AVG(rating) AS total FROM "+DB_PREFIX+"review r1 WHERE r1.product_id = p.product_id AND r1.status = '1' GROUP BY r1.product_id) AS rating, (SELECT price FROM "+DB_PREFIX+"product_discount pd2 WHERE pd2.product_id = p.product_id AND pd2.customer_group_id = '1' AND pd2.quantity = '1' AND ((pd2.date_start = '0000-00-00' OR pd2.date_start < NOW()) AND (pd2.date_end = '0000-00-00' OR pd2.date_end > NOW())) ORDER BY pd2.priority ASC, pd2.price ASC LIMIT 1) AS discount, (SELECT price FROM "+DB_PREFIX+"product_special ps WHERE ps.product_id = p.product_id AND ps.customer_group_id = '1' AND ((ps.date_start = '0000-00-00' OR ps.date_start < NOW()) AND (ps.date_end = '0000-00-00' OR ps.date_end > NOW())) ORDER BY ps.priority ASC, ps.price ASC LIMIT 1) AS special FROM "+DB_PREFIX+"sellercategories p2c LEFT JOIN "+DB_PREFIX+"product p ON (p2c.product_id = p.product_id) LEFT JOIN "+DB_PREFIX+"product_description pd ON (p.product_id = pd.product_id)  WHERE p.product_id IN (SELECT a.product_id FROM( SELECT sp.product_id, (6371 * acos( cos( radians("+req.query.latitude+") ) * cos( radians( sa.latitude ) ) * cos( radians( sa.longitude ) - radians("+req.query.longitude+") ) + sin( radians("+req.query.latitude+") ) * sin( radians(sa.latitude ) ) ) ) AS distance FROM "+DB_PREFIX+"sellers s JOIN "+DB_PREFIX+"saddress sa ON s.seller_id = sa.seller_id LEFT JOIN "+DB_PREFIX+"sellers_products sp ON (s.seller_id = sp.seller_id) WHERE s.seller_id="+ req.query.seller_id+" HAVING distance < 10) a) AND pd.language_id = '1' AND p.status = '1' AND p.date_available <= NOW() AND p2c.category_id = '"+req.query.cat_id +"' GROUP BY p.product_id ORDER BY p.sort_order ASC, LCASE(pd.name) ASC LIMIT "+req.query.start +","+end,function(err,newRes){
       
        newRes = JSON.parse(newRes);
        
        var pathNumber = 0;
        for(i=0;i<=newRes.length-1;i++){

            getProduct = DataQuery("SELECT DISTINCT p.status, p.product_id, p.quantity, p.price, pd.meta_description, pd.name AS name, p.image, (SELECT price FROM " + DB_PREFIX + "product_discount pd2 WHERE pd2.product_id = p.product_id  AND pd2.quantity = '1' AND ((pd2.date_start = '0000-00-00' OR pd2.date_start < NOW()) AND (pd2.date_end = '0000-00-00' OR pd2.date_end > NOW())) ORDER BY pd2.priority ASC, pd2.price ASC LIMIT 1) AS discount, (SELECT price FROM " + DB_PREFIX + "product_special ps WHERE ps.product_id = p.product_id AND ps.seller_id='"+ req.query.seller_id +"' AND ((ps.date_start = '0000-00-00' OR ps.date_start < NOW()) AND (ps.date_end = '0000-00-00' OR ps.date_end > NOW())) ORDER BY ps.priority ASC, ps.price ASC LIMIT 1) AS special,( SELECT quantity FROM "+DB_PREFIX+"cart WHERE device_id ='"+device_id+"' and product_id='"+newRes[i]['product_id']+"' ) as product_in_cart,( SELECT product_id FROM "+DB_PREFIX+"customer_wishlist WHERE device_id ='"+device_id+"' AND seller_id='"+ req.query.seller_id +"' AND product_id='"+newRes[i]['product_id']+"') as in_wishlist, (SELECT ss.name FROM " + DB_PREFIX + "stock_status ss WHERE ss.stock_status_id = p.stock_status_id AND ss.language_id = '" + languageid  + "') AS stock_status FROM " + DB_PREFIX + "product p LEFT JOIN " + DB_PREFIX + "product_description pd ON (p.product_id = pd.product_id) LEFT JOIN " + DB_PREFIX + "product_to_store p2s ON (p.product_id = p2s.product_id) LEFT JOIN " + DB_PREFIX + "sellers_products sp ON (p.product_id = sp.product_id) WHERE p.product_id = '" + newRes[i]['product_id'] + "' AND sp.seller_id = '" + req.query.seller_id + "' AND pd.language_id = '" + languageid + "' AND p.status = '1' AND p.date_available <= NOW() AND p2s.store_id = 0 ",function(err,proData){

                proArray[pathNumber] = JSON.parse(proData);
                pathNumber++;
            });
        }
        setTimeout(function () {
            res.send(proArray);
            proArray = [];
        },500);
    });
});


//Get Product by area id
app.get('/getProductByAreaID', function(req,res){

    device_id = req.query.device_id;
    var languageid = req.query.language_id;
	var end = req.query.end
    
    DataQuery("SELECT p.product_id, (SELECT AVG(rating) AS total FROM "+DB_PREFIX+"review r1 WHERE r1.product_id = p.product_id AND r1.status = '1' GROUP BY r1.product_id) AS rating, (SELECT price FROM "+DB_PREFIX+"product_discount pd2 WHERE pd2.product_id = p.product_id AND pd2.customer_group_id = '1' AND pd2.quantity = '1' AND ((pd2.date_start = '0000-00-00' OR pd2.date_start < NOW()) AND (pd2.date_end = '0000-00-00' OR pd2.date_end > NOW())) ORDER BY pd2.priority ASC, pd2.price ASC LIMIT 1) AS discount, (SELECT price FROM "+DB_PREFIX+"product_special ps WHERE ps.product_id = p.product_id AND ps.customer_group_id = '1' AND ((ps.date_start = '0000-00-00' OR ps.date_start < NOW()) AND (ps.date_end = '0000-00-00' OR ps.date_end > NOW())) ORDER BY ps.priority ASC, ps.price ASC LIMIT 1) AS special FROM "+DB_PREFIX+"sellercategories p2c LEFT JOIN "+DB_PREFIX+"product p ON (p2c.product_id = p.product_id) LEFT JOIN "+DB_PREFIX+"product_description pd ON (p.product_id = pd.product_id) LEFT JOIN "+DB_PREFIX+"sellers_products sp ON (p.product_id = sp.product_id) LEFT JOIN "+DB_PREFIX+"saddress sa ON (sa.seller_id = sp.seller_id) WHERE p2c.seller_id ='"+req.query.seller_id+"' AND sp.seller_id ='"+req.query.seller_id+"' AND  pd.language_id = '"+languageid+"' AND p.status = '1' AND p.date_available <= NOW() AND sa.area_id = '"+req.query.areaID+"' AND p2c.category_id = '"+req.query.cat_id+"' GROUP BY p.product_id ORDER BY p.sort_order ASC, LCASE(pd.name) ASC LIMIT "+req.query.start+","+end ,function(err,newRes){
       
        newRes = JSON.parse(newRes);
        
        var pathNumber = 0;
        for(i=0;i<=newRes.length-1;i++){

            getProduct = DataQuery("SELECT DISTINCT p.status, p.product_id, p.quantity, sp.price, pd.meta_description, pd.name AS name, p.image, (SELECT price FROM " + DB_PREFIX + "product_discount pd2 WHERE pd2.product_id = p.product_id  AND pd2.quantity = '1' AND ((pd2.date_start = '0000-00-00' OR pd2.date_start < NOW()) AND (pd2.date_end = '0000-00-00' OR pd2.date_end > NOW())) ORDER BY pd2.priority ASC, pd2.price ASC LIMIT 1) AS discount, (SELECT price FROM " + DB_PREFIX + "product_special ps WHERE ps.product_id = p.product_id AND ps.seller_id='"+ req.query.seller_id +"' AND ((ps.date_start = '0000-00-00' OR ps.date_start < NOW()) AND (ps.date_end = '0000-00-00' OR ps.date_end > NOW())) ORDER BY ps.priority ASC, ps.price ASC LIMIT 1) AS special,( SELECT quantity FROM "+DB_PREFIX+"cart WHERE device_id ='"+device_id+"' and product_id='"+newRes[i]['product_id']+"' ) as product_in_cart,( SELECT product_id FROM "+DB_PREFIX+"customer_wishlist WHERE device_id ='"+device_id+"' AND seller_id='"+ req.query.seller_id +"' AND product_id='"+newRes[i]['product_id']+"') as in_wishlist, (SELECT ss.name FROM " + DB_PREFIX + "stock_status ss WHERE ss.stock_status_id = p.stock_status_id AND ss.language_id = '" + languageid  + "') AS stock_status FROM " + DB_PREFIX + "product p LEFT JOIN " + DB_PREFIX + "product_description pd ON (p.product_id = pd.product_id) LEFT JOIN " + DB_PREFIX + "product_to_store p2s ON (p.product_id = p2s.product_id) LEFT JOIN " + DB_PREFIX + "sellers_products sp ON (p.product_id = sp.product_id) WHERE p.product_id = '" + newRes[i]['product_id'] + "' AND sp.seller_id = '" + req.query.seller_id + "' AND pd.language_id = '" + languageid + "' AND p.status = '1' AND p.date_available <= NOW() AND p2s.store_id = 0 ",function(err,proData){

                proArray[pathNumber] = JSON.parse(proData);
                pathNumber++;
            });
        }
        setTimeout(function () {
            res.send(proArray);
            proArray = [];
        },500);
    });
});


app.get("/search",function(req,res){
	var languageid = req.query.language_id;
	var end = req.query.end
    
	DataQuery("SELECT p.product_id, (SELECT AVG(rating) AS total FROM "+DB_PREFIX+"review r1 WHERE r1.product_id = p.product_id AND r1.status = '1' GROUP BY r1.product_id) AS rating, (SELECT price FROM "+DB_PREFIX+"product_discount pd2 WHERE pd2.product_id = p.product_id AND pd2.customer_group_id = '1' AND pd2.quantity = '1' AND ((pd2.date_start = '0000-00-00' OR pd2.date_start < NOW()) AND (pd2.date_end = '0000-00-00' OR pd2.date_end > NOW())) ORDER BY pd2.priority ASC, pd2.price ASC LIMIT 1) AS discount, (SELECT price FROM "+DB_PREFIX+"product_special ps WHERE ps.product_id = p.product_id AND ps.customer_group_id = '1' AND ((ps.date_start = '0000-00-00' OR ps.date_start < NOW()) AND (ps.date_end = '0000-00-00' OR ps.date_end > NOW())) ORDER BY ps.priority ASC, ps.price ASC LIMIT 1) AS special FROM "+DB_PREFIX+"product p LEFT JOIN "+DB_PREFIX+"product_description pd ON (p.product_id = pd.product_id) LEFT JOIN "+DB_PREFIX+"product_to_store p2s ON (p.product_id = p2s.product_id) LEFT JOIN "+DB_PREFIX+"sellers_products sp ON (p.product_id = sp.product_id) WHERE sp.seller_id= '"+req.query.seller_id+"' AND p.status = '1' AND p.date_available <= NOW() AND p2s.store_id = '0' AND ( pd.name LIKE '%"+req.query.search+"%' OR pd.tag LIKE '%"+req.query.search+"%' OR LCASE(p.model) = '"+req.query.search+"' OR LCASE(p.sku) = '"+req.query.search+"' OR LCASE(p.upc) = '"+req.query.search+"' OR LCASE(p.ean) = '"+req.query.search+"' OR LCASE(p.jan) = '"+req.query.search+"' OR LCASE(p.isbn) = '"+req.query.search+"' OR LCASE(p.mpn) = '"+req.query.search+"') GROUP BY p.product_id ORDER BY p.sort_order ASC, LCASE(pd.name) ASC LIMIT "+req.query.start +","+end ,function(err,newRes){
      
        newRes = JSON.parse(newRes);
        
        var pathNumber = 0;
        for(i=0;i<=newRes.length-1;i++){

            getProduct = DataQuery("SELECT DISTINCT p.status, p.product_id, p.quantity, sp.price, pd.meta_description, pd.name AS name, p.image, (SELECT price FROM " + DB_PREFIX + "product_discount pd2 WHERE pd2.product_id = p.product_id  AND pd2.quantity = '1' AND ((pd2.date_start = '0000-00-00' OR pd2.date_start < NOW()) AND (pd2.date_end = '0000-00-00' OR pd2.date_end > NOW())) ORDER BY pd2.priority ASC, pd2.price ASC LIMIT 1) AS discount, (SELECT price FROM " + DB_PREFIX + "product_special ps WHERE ps.product_id = p.product_id AND ps.seller_id='"+ req.query.seller_id +"' AND ((ps.date_start = '0000-00-00' OR ps.date_start < NOW()) AND (ps.date_end = '0000-00-00' OR ps.date_end > NOW())) ORDER BY ps.priority ASC, ps.price ASC LIMIT 1) AS special,( SELECT quantity FROM "+DB_PREFIX+"cart WHERE device_id ='"+device_id+"' and product_id='"+newRes[i]['product_id']+"' ) as product_in_cart,( SELECT product_id FROM "+DB_PREFIX+"customer_wishlist WHERE device_id ='"+device_id+"' AND seller_id='"+ req.query.seller_id +"' AND product_id='"+newRes[i]['product_id']+"') as in_wishlist, (SELECT ss.name FROM " + DB_PREFIX + "stock_status ss WHERE ss.stock_status_id = p.stock_status_id AND ss.language_id = '" + languageid  + "') AS stock_status FROM " + DB_PREFIX + "product p LEFT JOIN " + DB_PREFIX + "product_description pd ON (p.product_id = pd.product_id) LEFT JOIN " + DB_PREFIX + "product_to_store p2s ON (p.product_id = p2s.product_id) LEFT JOIN " + DB_PREFIX + "sellers_products sp ON (p.product_id = sp.product_id) WHERE p.product_id = '" + newRes[i]['product_id'] + "' AND sp.seller_id = '" + req.query.seller_id + "' AND pd.language_id = '" + languageid + "' AND p.status = '1' AND p.date_available <= NOW() AND p2s.store_id = 0 ",function(err,proData){

                proArray[pathNumber] = JSON.parse(proData);
                pathNumber++;
            });
        }
        setTimeout(function () {
            res.send(proArray);
            proArray = [];
        },500);
    });
});

//Get SubCategory
app.get("/getSubCategory",function(req,res){
    var languageid = req.query.language_id;
    if(req.query.is_area == 1){

        DataQuery("SELECT c.category_id,c.image,cd.name,cd.description FROM "+DB_PREFIX+"category c LEFT JOIN "+DB_PREFIX+"category_description cd ON (c.category_id = cd.category_id) WHERE c.category_id IN(SELECT DISTINCT(sp.category_id) FROM "+DB_PREFIX+"saddress sa LEFT JOIN "+DB_PREFIX+"sellercategories sp ON (sa.seller_id=sp.seller_id) WHERE sa.seller_id="+req.query.seller_id+" AND sa.area_id="+req.query.area_id+" ) AND c.parent_id = '"+req.query.cat_id+"' AND cd.language_id = '"+languageid+"' AND c.status = '1' ORDER BY c.sort_order, LCASE(cd.name)",function(err,newRes){
            
            res.send(JSON.parse(newRes));
        });
    }else{
        DataQuery("SELECT c.category_id,c.image,cd.name,cd.description FROM "+DB_PREFIX+"category c LEFT JOIN "+DB_PREFIX+"category_description cd ON (c.category_id = cd.category_id)  WHERE c.category_id IN( SELECT DISTINCT(a.category_id) FROM( SELECT sp.category_id, (6371 * acos( cos( radians("+req.query.latitude+") ) * cos( radians( sa.latitude ) ) * cos( radians( sa.longitude ) - radians("+req.query.longitude+") ) + sin( radians("+req.query.latitude+") ) * sin( radians(sa.latitude ) ) ) ) AS distance FROM  "+DB_PREFIX+"saddress sa LEFT JOIN "+DB_PREFIX+"sellercategories sp ON (sa.seller_id = sp.seller_id)WHERE sa.seller_id="+req.query.seller_id+" HAVING distance < "+req.query.radius+") a) AND c.parent_id = '"+req.query.cat_id+"' AND cd.language_id = '"+languageid+"'   AND c.status = '1' ORDER BY c.sort_order, LCASE(cd.name)",function(err,newRes){
            
            res.send(JSON.parse(newRes));
        });
    }
    
});

function DataQuery(querySQL,callback){

    result = [];
    db.query(querySQL, (err, res) => {
        if(err) throw err
        if(res.toString().length != 0){
            result = res
        }else{
            result = '[{"msg":"fail"}]';
        }
        callback(null, JSON.stringify(result));
    });
}

db.connect((err) => {
    if (err) {
        throw err;
    }
    console.log('Connected to database');
});

global.db = db;

app.set('port', process.env.port || port);
app.set('view engine', 'ejs');
app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());

app.listen(port, () => {
    console.log(`Server running on port: ${port}`);
});