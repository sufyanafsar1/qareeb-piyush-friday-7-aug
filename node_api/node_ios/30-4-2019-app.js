
const express = require('express');
const bodyParser = require('body-parser');
const mysql = require('mysql');
const path = require('path');
const app = express();
const port = 3000;

DB_PREFIX = "oc_"
device_id = "";
result = {};
proArray = [];
languageid = 1;
resultArr = [];

const db = mysql.createConnection ({
    host: 'careeb-stage.cf9bruqved5k.us-east-2.rds.amazonaws.com',
    user: 'careeb',
    password: 'careeb1209',
    database: 'Qareeb_App'
});


app.get('/', (req,res)=> {
    res.send("Hey there come back leter");
});

app.get('/getStoreBankDetail', function(req,res){
	var languageid = req.query.language;
	
	if(languageid==1){
		query  = "SELECT value FROM `oc_setting` WHERE `key` LIKE ('bank_transfer_bank1')";
	}
	if(languageid==3){
		query  = "SELECT value FROM `oc_setting` WHERE `key` LIKE ('bank_transfer_bank3')";
	}
	
	DataQuery(query,function(err,newRes){
			newRes = JSON.parse(newRes);
				setTimeout(function () {
					res.send(newRes);		
				},100);
	});	
	
});

app.get('/getCartWithDeliveryCharges', function(req,res){
	var device_id = req.query.device_id;
	var customer_id = req.query.customer_id;
	var languageid = req.query.language;
	var filter;
	if (typeof customer_id !== "undefined") {
		filter = "c.customer_id = '"+customer_id+"'";
	}
	if (typeof device_id !== "undefined") {
		filter = "c.device_id = '"+device_id+"'";
	}
	
	getCartProducts = "SELECT c.cart_id, p.product_id, p.model,sp.quantity as stock,p.image,c.quantity, sp.price, pd.name, (sp.price*c.quantity) as total FROM `oc_cart` c LEFT JOIN oc_product p ON p.product_id = c.product_id LEFT JOIN oc_sellers_products sp ON sp.seller_id = c.seller_id LEFT JOIN oc_product_description pd ON pd.product_Id = p.product_id WHERE pd.language_id = "+languageid+" AND "+filter+" GROUP BY c.product_id ORDER BY pd.name";

		DataQuery(getCartProducts,function(err,newRes){

			resultArr[0] = 'Success';
			newRes = JSON.parse(newRes);
			var total = 0;
			for(i=0;i<=newRes.length-1;i++){
				total = total + newRes[i]['total'];
			}
			
			resultArr[1] = total;
			resultArr[2] = newRes;
			setTimeout(function () { 
				res.send(resultArr);		
			},100);
	});	
});		


app.get('/getCartTotal', function(req,res){
	var device_id = req.query.device_id;
	var customer_id = req.query.customer_id;
	var cartQuery;
	if (typeof device_id !== "undefined") {

		cartQuery = "SELECT count(*) as total FROM `oc_cart` WHERE device_id = '"+device_id+"'";
		DataQuery(cartQuery,function(err,newRes){
			newRes = JSON.parse(newRes);
				setTimeout(function () {
					res.send(newRes);		
				},100);
		});	
	} else if (typeof customer_id !== "undefined") {
		cartQuery = "SELECT count(*) as total FROM `oc_cart` WHERE customer_id = '"+customer_id+"'";
		DataQuery(cartQuery,function(err,newRes){
			newRes = JSON.parse(newRes);
				setTimeout(function () {
					res.send(newRes);		
				},100);
		});	
	} else {
		res.send('0');
	}
});

app.get('/getStoreHomeCategory', function(req,res){
	
	var seller_id = req.query.seller_id;
	var languageid = req.query.language_id;
	var storeCategory, storeCatName;
	
	// get store category 
	storeCategory = "SELECT special_category FROM oc_sellers WHERE seller_id = '"+seller_id+"'";

	DataQuery(storeCategory,function(err,newRes){       
		resultArr = [];
		
			newRes = JSON.parse(newRes);
			if (typeof(newRes[0]['special_category']) != "undefined" && newRes[0]['special_category']!=''){
				// get category name
					storeCatName = "SELECT DISTINCT category_id,name FROM  oc_category_description cd WHERE cd.category_id IN ("+newRes[0]['special_category']+") AND cd.language_id = "+languageid+" ";
				
					DataQuery(storeCatName,function(err,newRes){       
					resultArr = JSON.parse(newRes);
				});	
			}
		
		
		setTimeout(function () {
			res.send(resultArr);		
		},100);
	
	});	
});

app.get('/getStoreSpecial', function(req,res){
	var seller_id = req.query.seller_id;
	var languageid = req.query.language_id;
	var device_id = req.query.device_id;
	var specialProductQuery;
	
	// get special products
	specialProductQuery = "SELECT DISTINCT ps.product_id, pd.name, pd.description, p.image, sp.price as regularPrice, ps.price as price, (((sp.price-ps.price)/sp.price)*100) as discountRate FROM oc_product_special ps LEFT JOIN oc_product p ON (ps.product_id = p.product_id) LEFT JOIN oc_product_description pd ON (p.product_id = pd.product_id) LEFT JOIN oc_sellers_products sp ON (p.product_id = sp.product_id) WHERE  pd.language_id = '"+languageid+"' AND (sp.seller_id = '"+seller_id+"' AND ps.seller_id = '"+seller_id+"') AND p.status = '1' AND p.date_available <= NOW() AND ((ps.date_start = '0000-00-00' OR ps.date_start < NOW()) AND (ps.date_end = '0000-00-00' OR ps.date_end > NOW())) GROUP BY ps.product_id ORDER BY p.sort_order ASC, LCASE(pd.name) ASC LIMIT "+req.query.start +",15";

	
	DataQuery(specialProductQuery,function(err,newRes){ 
		newRes = JSON.parse(newRes);
		tempArr = [];
		var pathNumber = 0;
		for(i=0;i<=newRes.length-1;i++){
			
			getProduct = DataQuery("SELECT DISTINCT p.option_available, p.status, p.product_id, p.quantity, sp.price, pd.meta_description, pd.name AS name, p.image, (SELECT price FROM " + DB_PREFIX + "product_discount pd2 WHERE pd2.product_id = p.product_id  AND pd2.quantity = '1' AND ((pd2.date_start = '0000-00-00' OR pd2.date_start < NOW()) AND (pd2.date_end = '0000-00-00' OR pd2.date_end > NOW())) ORDER BY pd2.priority ASC, pd2.price ASC LIMIT 1) AS discount, (SELECT price FROM " + DB_PREFIX + "product_special ps WHERE ps.product_id = p.product_id AND ps.seller_id='"+ req.query.seller_id +"' AND ((ps.date_start = '0000-00-00' OR ps.date_start < NOW()) AND (ps.date_end = '0000-00-00' OR ps.date_end > NOW())) ORDER BY ps.priority ASC, ps.price ASC LIMIT 1) AS special,( SELECT quantity FROM "+DB_PREFIX+"cart WHERE device_id ='"+device_id+"' and product_id='"+newRes[i]['product_id']+"' ) as product_in_cart,( SELECT product_id FROM "+DB_PREFIX+"customer_wishlist WHERE device_id ='"+device_id+"' AND seller_id='"+ req.query.seller_id +"' AND product_id='"+newRes[i]['product_id']+"') as in_wishlist, (SELECT ss.name FROM " + DB_PREFIX + "stock_status ss WHERE ss.stock_status_id = p.stock_status_id AND ss.language_id = '" + languageid  + "') AS stock_status FROM " + DB_PREFIX + "product p LEFT JOIN " + DB_PREFIX + "product_description pd ON (p.product_id = pd.product_id) LEFT JOIN " + DB_PREFIX + "product_to_store p2s ON (p.product_id = p2s.product_id) LEFT JOIN " + DB_PREFIX + "sellers_products sp ON (p.product_id = sp.product_id) WHERE p.product_id = '" + newRes[i]['product_id'] + "' AND sp.seller_id = '" + req.query.seller_id + "' AND pd.language_id = '" + languageid + "' AND p.status = '1' AND p.date_available <= NOW() AND p2s.store_id = 0 ",function(err,proData){
				proArray[pathNumber] = JSON.parse(proData);
                pathNumber++;
            });
			
		}
		resultArr = proArray;
		
		
		setTimeout(function () {
			res.send(resultArr);		
		},100);
	});	
});


app.get('/getStoreFeatured', function(req,res){
	
	var seller_id = req.query.seller_id;
	var languageid = req.query.language_id;
	var device_id = req.query.device_id;
	var featuredProductQuery;
	
	// get featured products
		featuredProductQuery = "SELECT DISTINCT ps.product_id, pd.name, p.image, sp.price, '' as regularPrice  FROM oc_product_feature ps LEFT JOIN oc_product p ON (ps.product_id = p.product_id) LEFT JOIN oc_product_description pd ON (p.product_id = pd.product_id) LEFT JOIN oc_sellers_products sp ON (p.product_id = sp.product_id) WHERE pd.language_id = '"+languageid+"' AND ps.seller_id = '"+seller_id+"' AND p.date_available <= NOW() AND ((ps.date_start = '0000-00-00' OR ps.date_start < NOW()) AND (ps.date_end = '0000-00-00' OR ps.date_end > NOW())) GROUP BY ps.product_id ORDER BY p.sort_order ASC, LCASE(pd.name) ASC LIMIT "+req.query.start +",9";
		
		DataQuery(featuredProductQuery,function(err,newRes){       
			newRes = JSON.parse(newRes);
			tempArr = [];
			var pathNumber = 0;
			
			for(i=0;i<=newRes.length-1;i++){
				getProduct = DataQuery("SELECT DISTINCT p.option_available, p.status, p.product_id, p.quantity, sp.price, pd.meta_description, pd.name AS name, p.image, (SELECT price FROM " + DB_PREFIX + "product_discount pd2 WHERE pd2.product_id = p.product_id  AND pd2.quantity = '1' AND ((pd2.date_start = '0000-00-00' OR pd2.date_start < NOW()) AND (pd2.date_end = '0000-00-00' OR pd2.date_end > NOW())) ORDER BY pd2.priority ASC, pd2.price ASC LIMIT 1) AS discount, (SELECT price FROM " + DB_PREFIX + "product_special ps WHERE ps.product_id = p.product_id AND ps.seller_id='"+ req.query.seller_id +"' AND ((ps.date_start = '0000-00-00' OR ps.date_start < NOW()) AND (ps.date_end = '0000-00-00' OR ps.date_end > NOW())) ORDER BY ps.priority ASC, ps.price ASC LIMIT 1) AS special,( SELECT quantity FROM "+DB_PREFIX+"cart WHERE device_id ='"+device_id+"' and product_id='"+newRes[i]['product_id']+"' ) as product_in_cart,( SELECT product_id FROM "+DB_PREFIX+"customer_wishlist WHERE device_id ='"+device_id+"' AND seller_id='"+ req.query.seller_id +"' AND product_id='"+newRes[i]['product_id']+"') as in_wishlist, (SELECT ss.name FROM " + DB_PREFIX + "stock_status ss WHERE ss.stock_status_id = p.stock_status_id AND ss.language_id = '" + languageid  + "') AS stock_status FROM " + DB_PREFIX + "product p LEFT JOIN " + DB_PREFIX + "product_description pd ON (p.product_id = pd.product_id) LEFT JOIN " + DB_PREFIX + "product_to_store p2s ON (p.product_id = p2s.product_id) LEFT JOIN " + DB_PREFIX + "sellers_products sp ON (p.product_id = sp.product_id) WHERE p.product_id = '" + newRes[i]['product_id'] + "' AND sp.seller_id = '" + req.query.seller_id + "' AND pd.language_id = '" + languageid + "' AND p.status = '1' AND p.date_available <= NOW() AND p2s.store_id = 0 ",function(err,proData){
					proArray[pathNumber] = JSON.parse(proData);
					pathNumber++;
				});
				
			}
			resultArr = proArray;
			
		});	
		
		setTimeout(function () {
			res.send(resultArr);		
		},100);
});


app.get('/getStoreSpecialFeatured', function(req,res){
	var seller_id = req.query.seller_id;
	var languageid = req.query.language_id;
	var specialProductQuery;
	
	// get special products
	specialProductQuery = "SELECT DISTINCT ps.product_id, pd.name, pd.description, p.image, sp.price as regularPrice, ps.price as price, (((sp.price-ps.price)/sp.price)*100) as discountRate FROM oc_product_special ps LEFT JOIN oc_product p ON (ps.product_id = p.product_id) LEFT JOIN oc_product_description pd ON (p.product_id = pd.product_id) LEFT JOIN oc_sellers_products sp ON (p.product_id = sp.product_id) WHERE  pd.language_id = '"+languageid+"' AND (sp.seller_id = '"+seller_id+"' AND ps.seller_id = '"+seller_id+"') AND p.status = '1' AND p.date_available <= NOW() AND ((ps.date_start = '0000-00-00' OR ps.date_start < NOW()) AND (ps.date_end = '0000-00-00' OR ps.date_end > NOW())) GROUP BY ps.product_id ORDER BY p.sort_order ASC, LCASE(pd.name) ASC LIMIT "+req.query.start +",9";

	
	DataQuery(specialProductQuery,function(err,newRes){ 
		newRes = JSON.parse(newRes);
		tempArr = [];
		var pathNumber = 0;
		for(i=0;i<=newRes.length-1;i++){
			getProduct = DataQuery("SELECT DISTINCT p.option_available, p.status, p.product_id, p.quantity, sp.price, pd.meta_description, pd.name AS name, p.image, (SELECT price FROM " + DB_PREFIX + "product_discount pd2 WHERE pd2.product_id = p.product_id  AND pd2.quantity = '1' AND ((pd2.date_start = '0000-00-00' OR pd2.date_start < NOW()) AND (pd2.date_end = '0000-00-00' OR pd2.date_end > NOW())) ORDER BY pd2.priority ASC, pd2.price ASC LIMIT 1) AS discount, (SELECT price FROM " + DB_PREFIX + "product_special ps WHERE ps.product_id = p.product_id AND ps.seller_id='"+ req.query.seller_id +"' AND ((ps.date_start = '0000-00-00' OR ps.date_start < NOW()) AND (ps.date_end = '0000-00-00' OR ps.date_end > NOW())) ORDER BY ps.priority ASC, ps.price ASC LIMIT 1) AS special,( SELECT quantity FROM "+DB_PREFIX+"cart WHERE device_id ='"+device_id+"' and product_id='"+newRes[i]['product_id']+"' ) as product_in_cart,( SELECT product_id FROM "+DB_PREFIX+"customer_wishlist WHERE device_id ='"+device_id+"' AND seller_id='"+ req.query.seller_id +"' AND product_id='"+newRes[i]['product_id']+"') as in_wishlist, (SELECT ss.name FROM " + DB_PREFIX + "stock_status ss WHERE ss.stock_status_id = p.stock_status_id AND ss.language_id = '" + languageid  + "') AS stock_status FROM " + DB_PREFIX + "product p LEFT JOIN " + DB_PREFIX + "product_description pd ON (p.product_id = pd.product_id) LEFT JOIN " + DB_PREFIX + "product_to_store p2s ON (p.product_id = p2s.product_id) LEFT JOIN " + DB_PREFIX + "sellers_products sp ON (p.product_id = sp.product_id) WHERE p.product_id = '" + newRes[i]['product_id'] + "' AND sp.seller_id = '" + req.query.seller_id + "' AND pd.language_id = '" + languageid + "' AND p.status = '1' AND p.date_available <= NOW() AND p2s.store_id = 0 ",function(err,proData){
				proArray[pathNumber] = JSON.parse(proData);
                pathNumber++;
            });
			
		}
		resultArr[1] = proArray;
		
		
		// get featured products
		featuredProductQuery = "SELECT DISTINCT p.option_available, ps.product_id, pd.name, p.image, sp.price, '' as regularPrice  FROM oc_product_feature ps LEFT JOIN oc_product p ON (ps.product_id = p.product_id) LEFT JOIN oc_product_description pd ON (p.product_id = pd.product_id) LEFT JOIN oc_sellers_products sp ON (p.product_id = sp.product_id) WHERE pd.language_id = '"+languageid+"' AND ps.seller_id = '"+seller_id+"' AND p.date_available <= NOW() AND ((ps.date_start = '0000-00-00' OR ps.date_start < NOW()) AND (ps.date_end = '0000-00-00' OR ps.date_end > NOW())) GROUP BY ps.product_id ORDER BY p.sort_order ASC, LCASE(pd.name) ASC LIMIT "+req.query.start +",9";
		
		DataQuery(featuredProductQuery,function(err,newRes2){       
			resultArr[0] = JSON.parse(newRes2);
		});	
		
		setTimeout(function () {
			res.send(resultArr);		
		},100);
	});	
});


/* app.get('/getStoreSpecialFeatured', function(req,res){
	var seller_id = req.query.seller_id;
	var languageid = req.query.language_id;
	var specialProductQuery;
	
	// get special products
	specialProductQuery = "SELECT DISTINCT ps.product_id, pd.name, p.image, sp.price as regularPrice, ps.price as price, (((sp.price-ps.price)/sp.price)*100) as discountRate FROM oc_product_special ps LEFT JOIN oc_product p ON (ps.product_id = p.product_id) LEFT JOIN oc_product_description pd ON (p.product_id = pd.product_id) LEFT JOIN oc_sellers_products sp ON (p.product_id = sp.product_id) WHERE  pd.language_id = '"+languageid+"' AND (sp.seller_id = '"+seller_id+"' AND ps.seller_id = '"+seller_id+"') AND p.status = '1' AND p.date_available <= NOW() AND ((ps.date_start = '0000-00-00' OR ps.date_start < NOW()) AND (ps.date_end = '0000-00-00' OR ps.date_end > NOW())) GROUP BY ps.product_id ORDER BY p.sort_order ASC, LCASE(pd.name) ASC LIMIT "+req.query.start +",9";

	DataQuery(specialProductQuery,function(err,newRes){       
		resultArr[1] = JSON.parse(newRes);
		
		// get featured products
		featuredProductQuery = "SELECT DISTINCT ps.product_id, pd.name, p.image, sp.price, '' as regularPrice  FROM oc_product_feature ps LEFT JOIN oc_product p ON (ps.product_id = p.product_id) LEFT JOIN oc_product_description pd ON (p.product_id = pd.product_id) LEFT JOIN oc_sellers_products sp ON (p.product_id = sp.product_id) WHERE pd.language_id = '"+languageid+"' AND ps.seller_id = '"+seller_id+"' AND p.date_available <= NOW() AND ((ps.date_start = '0000-00-00' OR ps.date_start < NOW()) AND (ps.date_end = '0000-00-00' OR ps.date_end > NOW())) GROUP BY ps.product_id ORDER BY p.sort_order ASC, LCASE(pd.name) ASC LIMIT "+req.query.start +",9";
		
		DataQuery(featuredProductQuery,function(err,newRes2){       
			resultArr[0] = JSON.parse(newRes2);
		});	
		
		setTimeout(function () {
			res.send(resultArr);		
		},100);
	});	
});*/

app.get('/getProductBYcategoryAndLoc', function(req,res){

    device_id = req.query.device_id;
	var languageid = req.query.language_id;
    
    DataQuery("SELECT p.product_id, (SELECT AVG(rating) AS total FROM "+DB_PREFIX+"review r1 WHERE r1.product_id = p.product_id AND r1.status = '1' GROUP BY r1.product_id) AS rating, (SELECT price FROM "+DB_PREFIX+"product_discount pd2 WHERE pd2.product_id = p.product_id AND pd2.customer_group_id = '1' AND pd2.quantity = '1' AND ((pd2.date_start = '0000-00-00' OR pd2.date_start < NOW()) AND (pd2.date_end = '0000-00-00' OR pd2.date_end > NOW())) ORDER BY pd2.priority ASC, pd2.price ASC LIMIT 1) AS discount, (SELECT price FROM "+DB_PREFIX+"product_special ps WHERE ps.product_id = p.product_id AND ps.customer_group_id = '1' AND ((ps.date_start = '0000-00-00' OR ps.date_start < NOW()) AND (ps.date_end = '0000-00-00' OR ps.date_end > NOW())) ORDER BY ps.priority ASC, ps.price ASC LIMIT 1) AS special FROM "+DB_PREFIX+"sellercategories p2c LEFT JOIN "+DB_PREFIX+"product p ON (p2c.product_id = p.product_id) LEFT JOIN "+DB_PREFIX+"product_description pd ON (p.product_id = pd.product_id)  WHERE p.product_id IN (SELECT a.product_id FROM( SELECT sp.product_id, (6371 * acos( cos( radians("+req.query.latitude+") ) * cos( radians( sa.latitude ) ) * cos( radians( sa.longitude ) - radians("+req.query.longitude+") ) + sin( radians("+req.query.latitude+") ) * sin( radians(sa.latitude ) ) ) ) AS distance FROM "+DB_PREFIX+"sellers s JOIN "+DB_PREFIX+"saddress sa ON s.seller_id = sa.seller_id LEFT JOIN "+DB_PREFIX+"sellers_products sp ON (s.seller_id = sp.seller_id) WHERE s.seller_id="+ req.query.seller_id+" HAVING distance < 10) a) AND pd.language_id = '1' AND p.status = '1' AND p.date_available <= NOW() AND p2c.category_id = '"+req.query.cat_id +"' GROUP BY p.product_id ORDER BY p.sort_order ASC, LCASE(pd.name) ASC LIMIT "+req.query.start +",9",function(err,newRes){
       
        newRes = JSON.parse(newRes);
        
        var pathNumber = 0;
        for(i=0;i<=newRes.length-1;i++){

            getProduct = DataQuery("SELECT DISTINCT p.option_available, p.status, p.product_id, p.quantity, sp.price, pd.meta_description, pd.name AS name, p.image, (SELECT price FROM " + DB_PREFIX + "product_discount pd2 WHERE pd2.product_id = p.product_id  AND pd2.quantity = '1' AND ((pd2.date_start = '0000-00-00' OR pd2.date_start < NOW()) AND (pd2.date_end = '0000-00-00' OR pd2.date_end > NOW())) ORDER BY pd2.priority ASC, pd2.price ASC LIMIT 1) AS discount, (SELECT price FROM " + DB_PREFIX + "product_special ps WHERE ps.product_id = p.product_id AND ps.seller_id='"+ req.query.seller_id +"' AND ((ps.date_start = '0000-00-00' OR ps.date_start < NOW()) AND (ps.date_end = '0000-00-00' OR ps.date_end > NOW())) ORDER BY ps.priority ASC, ps.price ASC LIMIT 1) AS special,( SELECT quantity FROM "+DB_PREFIX+"cart WHERE device_id ='"+device_id+"' and product_id='"+newRes[i]['product_id']+"' ) as product_in_cart,( SELECT product_id FROM "+DB_PREFIX+"customer_wishlist WHERE device_id ='"+device_id+"' AND seller_id='"+ req.query.seller_id +"' AND product_id='"+newRes[i]['product_id']+"') as in_wishlist, (SELECT ss.name FROM " + DB_PREFIX + "stock_status ss WHERE ss.stock_status_id = p.stock_status_id AND ss.language_id = '" + languageid  + "') AS stock_status FROM " + DB_PREFIX + "product p LEFT JOIN " + DB_PREFIX + "product_description pd ON (p.product_id = pd.product_id) LEFT JOIN " + DB_PREFIX + "product_to_store p2s ON (p.product_id = p2s.product_id) LEFT JOIN " + DB_PREFIX + "sellers_products sp ON (p.product_id = sp.product_id) WHERE p.product_id = '" + newRes[i]['product_id'] + "' AND sp.seller_id = '" + req.query.seller_id + "' AND pd.language_id = '" + languageid + "' AND p.status = '1' AND p.date_available <= NOW() AND p2s.store_id = 0 ",function(err,proData){

                proArray[pathNumber] = JSON.parse(proData);
                pathNumber++;
            });
        }
        setTimeout(function () {
            res.send(proArray);
            proArray = [];
        },100);
    });
});

app.get('/getProductBYcategoryAndLoc2', function(req,res){

    device_id = req.query.device_id;
	var languageid = req.query.language_id;
	var city_id = req.query.city_id;
	
	var specialQuery  = "";
	if (city_id > 0) {	
		if (city_id == 25) {
			var customer_group_id = 1;
		} else if (city_id == 40) {
			var customer_group_id = 2;
		} else if (city_id == 24) {
			var customer_group_id = 3;
		} else if (city_id == 37) {
			var customer_group_id = 7;
		} else if (city_id == 31) {
			var customer_group_id = 8;
		}else if (city_id == 33) {
			var customer_group_id = 9;
		}
		
		if(customer_group_id!='' && customer_group_id>0){		
			specialQuery = 'AND ps.customer_group_id = '+customer_group_id;
		}		
	}
    
    DataQuery2("SELECT p.product_id, (SELECT AVG(rating) AS total FROM "+DB_PREFIX+"review r1 WHERE r1.product_id = p.product_id AND r1.status = '1' GROUP BY r1.product_id) AS rating, (SELECT price FROM "+DB_PREFIX+"product_discount pd2 WHERE pd2.product_id = p.product_id AND pd2.customer_group_id = '1' AND pd2.quantity = '1' AND ((pd2.date_start = '0000-00-00' OR pd2.date_start < NOW()) AND (pd2.date_end = '0000-00-00' OR pd2.date_end > NOW())) ORDER BY pd2.priority ASC, pd2.price ASC LIMIT 1) AS discount, (SELECT price FROM "+DB_PREFIX+"product_special ps WHERE ps.product_id = p.product_id "+specialQuery+" AND ((ps.date_start = '0000-00-00' OR ps.date_start < NOW()) AND (ps.date_end = '0000-00-00' OR ps.date_end > NOW())) ORDER BY ps.priority ASC, ps.price ASC LIMIT 1) AS special FROM "+DB_PREFIX+"sellercategories p2c LEFT JOIN "+DB_PREFIX+"product p ON (p2c.product_id = p.product_id) LEFT JOIN "+DB_PREFIX+"product_description pd ON (p.product_id = pd.product_id)  WHERE p.product_id IN (SELECT a.product_id FROM( SELECT sp.product_id, (6371 * acos( cos( radians("+req.query.latitude+") ) * cos( radians( sa.latitude ) ) * cos( radians( sa.longitude ) - radians("+req.query.longitude+") ) + sin( radians("+req.query.latitude+") ) * sin( radians(sa.latitude ) ) ) ) AS distance FROM "+DB_PREFIX+"sellers s JOIN "+DB_PREFIX+"saddress sa ON s.seller_id = sa.seller_id LEFT JOIN "+DB_PREFIX+"sellers_products sp ON (s.seller_id = sp.seller_id) WHERE s.seller_id="+ req.query.seller_id+" HAVING distance < 10) a) AND pd.language_id = '1' AND p.status = '1' AND p.date_available <= NOW() AND p2c.category_id = '"+req.query.cat_id +"' GROUP BY p.product_id ORDER BY p.sort_order ASC, LCASE(pd.name) ASC LIMIT "+req.query.start +",9",function(err,newRes){
		var result2 = new Object();
		if(newRes=="fail"){
			result2.message = "fail";
		} else {
			newRes = JSON.parse(newRes);
			result2.message = "success";
			
			var pathNumber = 0;
			for(i=0;i<=newRes.length-1;i++){

				getProduct = DataQuery("SELECT DISTINCT p.option_available, p.status, p.product_id, p.quantity, sp.price, pd.meta_description, pd.name AS name, p.image, (SELECT price FROM " + DB_PREFIX + "product_discount pd2 WHERE pd2.product_id = p.product_id  AND pd2.quantity = '1' AND ((pd2.date_start = '0000-00-00' OR pd2.date_start < NOW()) AND (pd2.date_end = '0000-00-00' OR pd2.date_end > NOW())) ORDER BY pd2.priority ASC, pd2.price ASC LIMIT 1) AS discount, (SELECT price FROM " + DB_PREFIX + "product_special ps WHERE ps.product_id = p.product_id "+specialQuery+" AND ps.seller_id='"+ req.query.seller_id +"' AND ((ps.date_start = '0000-00-00' OR ps.date_start < NOW()) AND (ps.date_end = '0000-00-00' OR ps.date_end > NOW())) ORDER BY ps.priority ASC, ps.price ASC LIMIT 1) AS special,( SELECT quantity FROM "+DB_PREFIX+"cart WHERE device_id ='"+device_id+"' and product_id='"+newRes[i]['product_id']+"' ) as product_in_cart,( SELECT product_id FROM "+DB_PREFIX+"customer_wishlist WHERE device_id ='"+device_id+"' AND seller_id='"+ req.query.seller_id +"' AND product_id='"+newRes[i]['product_id']+"') as in_wishlist, (SELECT ss.name FROM " + DB_PREFIX + "stock_status ss WHERE ss.stock_status_id = p.stock_status_id AND ss.language_id = '" + languageid  + "') AS stock_status FROM " + DB_PREFIX + "product p LEFT JOIN " + DB_PREFIX + "product_description pd ON (p.product_id = pd.product_id) LEFT JOIN " + DB_PREFIX + "product_to_store p2s ON (p.product_id = p2s.product_id) LEFT JOIN " + DB_PREFIX + "sellers_products sp ON (p.product_id = sp.product_id) WHERE p.product_id = '" + newRes[i]['product_id'] + "' AND sp.seller_id = '" + req.query.seller_id + "' AND pd.language_id = '" + languageid + "' AND p.status = '1' AND p.date_available <= NOW() AND p2s.store_id = 0 ",function(err,proData){

					proArray[pathNumber] = JSON.parse(proData);
					pathNumber++;
				});
			}
			result2.products = proArray;
		}
        setTimeout(function () {
            res.send(result2);
            proArray = [];
        },100);
    });
});

//Get Product by area id
app.get('/getProductByAreaID', function(req,res){

    device_id = req.query.device_id;
    var languageid = req.query.language_id;
	var end = req.query.end;
	var filter = '';
	var priceFilter = '';
	
	var filter_id = req.query.filter_id;
	var pstart = req.query.pstart;
	var pend = req.query.pend;
	
	if(filter_id!=''){
		if(filter_id == 'Name (A-Z)'){
			filter = 'ORDER BY LCASE(pd.name) ASC';
		}
		if(filter_id == 'Name (Z-A)'){
			filter = 'ORDER BY LCASE(pd.name) DESC';
		}
		if(filter_id == 'Price (Low-High)'){
			filter = 'ORDER BY sp.price ASC';
		}
		if(filter_id == 'Price (High-Low)'){
			filter = 'ORDER BY sp.price DESC';
		}
		if(filter_id == 'Price Range'){
			if((pstart!='' && pend!='')&&(pstart>0 && pend>0)){
				priceFilter = ' AND sp.price BETWEEN '+pstart+' AND '+pend ;
			}
			filter = 'ORDER BY sp.price ASC';
		}
		
	} else {
		filter = 'ORDER BY p.sort_order ASC, LCASE(pd.name) ASC';
	}
	
	DataQuery("SELECT p.product_id, (SELECT AVG(rating) AS total FROM "+DB_PREFIX+"review r1 WHERE r1.product_id = p.product_id AND r1.status = '1' GROUP BY r1.product_id) AS rating, (SELECT price FROM "+DB_PREFIX+"product_discount pd2 WHERE pd2.product_id = p.product_id AND pd2.customer_group_id = '1' AND pd2.quantity = '1' AND ((pd2.date_start = '0000-00-00' OR pd2.date_start < NOW()) AND (pd2.date_end = '0000-00-00' OR pd2.date_end > NOW())) ORDER BY pd2.priority ASC, pd2.price ASC LIMIT 1) AS discount, (SELECT price FROM "+DB_PREFIX+"product_special ps WHERE ps.product_id = p.product_id AND ps.customer_group_id = '1' AND ((ps.date_start = '0000-00-00' OR ps.date_start < NOW()) AND (ps.date_end = '0000-00-00' OR ps.date_end > NOW())) ORDER BY ps.priority ASC, ps.price ASC LIMIT 1) AS special FROM "+DB_PREFIX+"sellercategories p2c LEFT JOIN "+DB_PREFIX+"product p ON (p2c.product_id = p.product_id) LEFT JOIN "+DB_PREFIX+"product_description pd ON (p.product_id = pd.product_id) LEFT JOIN "+DB_PREFIX+"sellers_products sp ON (p.product_id = sp.product_id) LEFT JOIN "+DB_PREFIX+"saddress sa ON (sa.seller_id = sp.seller_id) WHERE p2c.seller_id ='"+req.query.seller_id+"' AND sp.seller_id ='"+req.query.seller_id+"' AND  pd.language_id = '"+languageid+"' AND p.status = '1' AND p.date_available <= NOW() AND sa.area_id = '"+req.query.areaID+"' AND p2c.category_id = '"+req.query.cat_id+"' "+priceFilter+" GROUP BY p.product_id "+filter+" LIMIT "+req.query.start+","+end ,function(err,newRes){   
        newRes = JSON.parse(newRes);
        
        var pathNumber = 0;
        for(i=0;i<=newRes.length-1;i++){

            getProduct = DataQuery("SELECT DISTINCT p.option_available, p.status, p.product_id, p.quantity, sp.price, pd.meta_description, pd.name AS name, p.image, (SELECT price FROM " + DB_PREFIX + "product_discount pd2 WHERE pd2.product_id = p.product_id  AND pd2.quantity = '1' AND ((pd2.date_start = '0000-00-00' OR pd2.date_start < NOW()) AND (pd2.date_end = '0000-00-00' OR pd2.date_end > NOW())) ORDER BY pd2.priority ASC, pd2.price ASC LIMIT 1) AS discount, (SELECT price FROM " + DB_PREFIX + "product_special ps WHERE ps.product_id = p.product_id AND ps.seller_id='"+ req.query.seller_id +"' AND ((ps.date_start = '0000-00-00' OR ps.date_start < NOW()) AND (ps.date_end = '0000-00-00' OR ps.date_end > NOW())) ORDER BY ps.priority ASC, ps.price ASC LIMIT 1) AS special,( SELECT quantity FROM "+DB_PREFIX+"cart WHERE device_id ='"+device_id+"' and product_id='"+newRes[i]['product_id']+"' ) as product_in_cart,( SELECT product_id FROM "+DB_PREFIX+"customer_wishlist WHERE device_id ='"+device_id+"' AND seller_id='"+ req.query.seller_id +"' AND product_id='"+newRes[i]['product_id']+"') as in_wishlist, (SELECT ss.name FROM " + DB_PREFIX + "stock_status ss WHERE ss.stock_status_id = p.stock_status_id AND ss.language_id = '" + languageid  + "') AS stock_status FROM " + DB_PREFIX + "product p LEFT JOIN " + DB_PREFIX + "product_description pd ON (p.product_id = pd.product_id) LEFT JOIN " + DB_PREFIX + "product_to_store p2s ON (p.product_id = p2s.product_id) LEFT JOIN " + DB_PREFIX + "sellers_products sp ON (p.product_id = sp.product_id) WHERE p.product_id = '" + newRes[i]['product_id'] + "' AND sp.seller_id = '" + req.query.seller_id + "' AND pd.language_id = '" + languageid + "' AND p.status = '1' AND p.date_available <= NOW() AND p2s.store_id = 0 ",function(err,proData){

                proArray[pathNumber] = JSON.parse(proData);
                pathNumber++;
            });
        }
        setTimeout(function () {
            res.send(proArray);
            proArray = [];
        },500);
    });
});


app.get('/getProductByAreaID2', function(req,res){

    device_id = req.query.device_id;
    var languageid = req.query.language_id;
	var end = req.query.end;
	var city_id = req.query.city_id;
	
	var specialQuery  = "";
	if (city_id > 0) {	
		if (city_id == 25) {
			var customer_group_id = 1;
		} else if (city_id == 40) {
			var customer_group_id = 2;
		} else if (city_id == 24) {
			var customer_group_id = 3;
		} else if (city_id == 37) {
			var customer_group_id = 7;
		} else if (city_id == 31) {
			var customer_group_id = 8;
		}else if (city_id == 33) {
			var customer_group_id = 9;
		}
		
		if(customer_group_id!='' && customer_group_id>0){		
			specialQuery = 'AND ps.customer_group_id = '+customer_group_id;
		}		
	}
	
	DataQuery2("SELECT p.product_id, (SELECT AVG(rating) AS total FROM "+DB_PREFIX+"review r1 WHERE r1.product_id = p.product_id AND r1.status = '1' GROUP BY r1.product_id) AS rating, (SELECT price FROM "+DB_PREFIX+"product_discount pd2 WHERE pd2.product_id = p.product_id AND pd2.customer_group_id = '1' AND pd2.quantity = '1' AND ((pd2.date_start = '0000-00-00' OR pd2.date_start < NOW()) AND (pd2.date_end = '0000-00-00' OR pd2.date_end > NOW())) ORDER BY pd2.priority ASC, pd2.price ASC LIMIT 1) AS discount, (SELECT price FROM "+DB_PREFIX+"product_special ps WHERE ps.product_id = p.product_id "+specialQuery+" AND ((ps.date_start = '0000-00-00' OR ps.date_start < NOW()) AND (ps.date_end = '0000-00-00' OR ps.date_end > NOW())) ORDER BY ps.priority ASC, ps.price ASC LIMIT 1) AS special FROM "+DB_PREFIX+"sellercategories p2c LEFT JOIN "+DB_PREFIX+"product p ON (p2c.product_id = p.product_id) LEFT JOIN "+DB_PREFIX+"product_description pd ON (p.product_id = pd.product_id) LEFT JOIN "+DB_PREFIX+"sellers_products sp ON (p.product_id = sp.product_id) LEFT JOIN "+DB_PREFIX+"saddress sa ON (sa.seller_id = sp.seller_id) WHERE p2c.seller_id ='"+req.query.seller_id+"' AND sp.seller_id ='"+req.query.seller_id+"' AND  pd.language_id = '"+languageid+"' AND p.status = '1' AND p.date_available <= NOW() AND sa.area_id = '"+req.query.areaID+"' AND p2c.category_id = '"+req.query.cat_id+"' GROUP BY p.product_id ORDER BY p.sort_order ASC, LCASE(pd.name) ASC LIMIT "+req.query.start+","+end ,function(err,newRes){
		
		var result2 = new Object();
		if(newRes=="fail"){
			result2.message = "fail";
		} else {
		   
			newRes = JSON.parse(newRes);
			result2.message = "success";
			
			var pathNumber = 0;
			for(i=0;i<=newRes.length-1;i++){

				getProduct = DataQuery("SELECT DISTINCT p.option_available, p.status, p.product_id, p.quantity, sp.price, pd.meta_description, pd.name AS name, p.image, (SELECT price FROM " + DB_PREFIX + "product_discount pd2 WHERE pd2.product_id = p.product_id  AND pd2.quantity = '1' AND ((pd2.date_start = '0000-00-00' OR pd2.date_start < NOW()) AND (pd2.date_end = '0000-00-00' OR pd2.date_end > NOW())) ORDER BY pd2.priority ASC, pd2.price ASC LIMIT 1) AS discount, (SELECT price FROM " + DB_PREFIX + "product_special ps WHERE ps.product_id = p.product_id "+specialQuery+" AND ps.seller_id='"+ req.query.seller_id +"' AND ((ps.date_start = '0000-00-00' OR ps.date_start < NOW()) AND (ps.date_end = '0000-00-00' OR ps.date_end > NOW())) ORDER BY ps.priority ASC, ps.price ASC LIMIT 1) AS special,( SELECT quantity FROM "+DB_PREFIX+"cart WHERE device_id ='"+device_id+"' and product_id='"+newRes[i]['product_id']+"' ) as product_in_cart,( SELECT product_id FROM "+DB_PREFIX+"customer_wishlist WHERE device_id ='"+device_id+"' AND seller_id='"+ req.query.seller_id +"' AND product_id='"+newRes[i]['product_id']+"') as in_wishlist, (SELECT ss.name FROM " + DB_PREFIX + "stock_status ss WHERE ss.stock_status_id = p.stock_status_id AND ss.language_id = '" + languageid  + "') AS stock_status FROM " + DB_PREFIX + "product p LEFT JOIN " + DB_PREFIX + "product_description pd ON (p.product_id = pd.product_id) LEFT JOIN " + DB_PREFIX + "product_to_store p2s ON (p.product_id = p2s.product_id) LEFT JOIN " + DB_PREFIX + "sellers_products sp ON (p.product_id = sp.product_id) WHERE p.product_id = '" + newRes[i]['product_id'] + "' AND sp.seller_id = '" + req.query.seller_id + "' AND pd.language_id = '" + languageid + "' AND p.status = '1' AND p.date_available <= NOW() AND p2s.store_id = 0 ",function(err,proData){

					proArray[pathNumber] = JSON.parse(proData);
					pathNumber++;
				});
			}
			result2.products = proArray;
		}
        setTimeout(function () {
            res.send(result2);
            proArray = [];
        },500);
    });
});


app.get("/search",function(req,res){

	var languageid = req.query.language_id;
	var city_id = req.query.city_id;
	
	var specialQuery  = "";
	if (city_id > 0) {	
		if (city_id == 25) {
			var customer_group_id = 1;
		} else if (city_id == 40) {
			var customer_group_id = 2;
		} else if (city_id == 24) {
			var customer_group_id = 3;
		} else if (city_id == 37) {
			var customer_group_id = 7;
		} else if (city_id == 31) {
			var customer_group_id = 8;
		}else if (city_id == 33) {
			var customer_group_id = 9;
		}
		
		if(customer_group_id!='' && customer_group_id>0){		
			specialQuery = 'AND ps.customer_group_id = '+customer_group_id;
		}		
	}
	
	//DataQuery("SELECT p.product_id, (SELECT AVG(rating) AS total FROM "+DB_PREFIX+"review r1 WHERE r1.product_id = p.product_id AND r1.status = '1' GROUP BY r1.product_id) AS rating, (SELECT price FROM "+DB_PREFIX+"product_discount pd2 WHERE pd2.product_id = p.product_id AND pd2.customer_group_id = '1' AND pd2.quantity = '1' AND ((pd2.date_start = '0000-00-00' OR pd2.date_start < NOW()) AND (pd2.date_end = '0000-00-00' OR pd2.date_end > NOW())) ORDER BY pd2.priority ASC, pd2.price ASC LIMIT 1) AS discount, (SELECT price FROM "+DB_PREFIX+"product_special ps WHERE ps.product_id = p.product_id AND ps.customer_group_id = '1' AND ((ps.date_start = '0000-00-00' OR ps.date_start < NOW()) AND (ps.date_end = '0000-00-00' OR ps.date_end > NOW())) ORDER BY ps.priority ASC, ps.price ASC LIMIT 1) AS special FROM "+DB_PREFIX+"product p LEFT JOIN "+DB_PREFIX+"product_description pd ON (p.product_id = pd.product_id) LEFT JOIN "+DB_PREFIX+"product_to_store p2s ON (p.product_id = p2s.product_id) LEFT JOIN "+DB_PREFIX+"sellers_products sp ON (p.product_id = sp.product_id) WHERE sp.seller_id= '"+req.query.seller_id+"' AND pd.language_id = '"+languageid+"' AND p.status = '1' AND p.date_available <= NOW() AND p2s.store_id = '0' AND ( pd.name LIKE '%"+req.query.search+"%' OR pd.tag LIKE '%"+req.query.search+"%' OR LCASE(p.model) = '"+req.query.search+"' OR LCASE(p.sku) = '"+req.query.search+"' OR LCASE(p.upc) = '"+req.query.search+"' OR LCASE(p.ean) = '"+req.query.search+"' OR LCASE(p.jan) = '"+req.query.search+"' OR LCASE(p.isbn) = '"+req.query.search+"' OR LCASE(p.mpn) = '"+req.query.search+"') GROUP BY p.product_id ORDER BY p.sort_order ASC, LCASE(pd.name) ASC LIMIT "+req.query.start +",9" ,function(err,newRes){
	
    DataQuery("SELECT p.product_id, (SELECT AVG(rating) AS total FROM "+DB_PREFIX+"review r1 WHERE r1.product_id = p.product_id AND r1.status = '1' GROUP BY r1.product_id) AS rating, (SELECT price FROM "+DB_PREFIX+"product_discount pd2 WHERE pd2.product_id = p.product_id AND pd2.customer_group_id = '1' AND pd2.quantity = '1' AND ((pd2.date_start = '0000-00-00' OR pd2.date_start < NOW()) AND (pd2.date_end = '0000-00-00' OR pd2.date_end > NOW())) ORDER BY pd2.priority ASC, pd2.price ASC LIMIT 1) AS discount, (SELECT price FROM "+DB_PREFIX+"product_special ps WHERE ps.product_id = p.product_id "+specialQuery+" AND ((ps.date_start = '0000-00-00' OR ps.date_start < NOW()) AND (ps.date_end = '0000-00-00' OR ps.date_end > NOW())) ORDER BY ps.priority ASC, ps.price ASC LIMIT 1) AS special FROM "+DB_PREFIX+"product p LEFT JOIN "+DB_PREFIX+"product_description pd ON (p.product_id = pd.product_id) LEFT JOIN "+DB_PREFIX+"product_to_store p2s ON (p.product_id = p2s.product_id) LEFT JOIN "+DB_PREFIX+"sellers_products sp ON (p.product_id = sp.product_id) WHERE sp.seller_id= '"+req.query.seller_id+"' AND p.status = '1' AND p.date_available <= NOW() AND p2s.store_id = '0' AND ( pd.name LIKE '%"+req.query.search+"%' OR pd.tag LIKE '%"+req.query.search+"%' OR LCASE(p.model) = '"+req.query.search+"' OR LCASE(p.sku) = '"+req.query.search+"' OR LCASE(p.upc) = '"+req.query.search+"' OR LCASE(p.ean) = '"+req.query.search+"' OR LCASE(p.jan) = '"+req.query.search+"' OR LCASE(p.isbn) = '"+req.query.search+"' OR LCASE(p.mpn) = '"+req.query.search+"') GROUP BY p.product_id ORDER BY p.sort_order ASC, LCASE(pd.name) ASC LIMIT "+req.query.start +",9" ,function(err,newRes){
      
        newRes = JSON.parse(newRes);
        
        var pathNumber = 0;
        for(i=0;i<=newRes.length-1;i++){

            getProduct = DataQuery("SELECT DISTINCT p.option_available, p.status, p.product_id, p.quantity, sp.price, pd.meta_description, pd.name AS name, p.image, (SELECT price FROM " + DB_PREFIX + "product_discount pd2 WHERE pd2.product_id = p.product_id  AND pd2.quantity = '1' AND ((pd2.date_start = '0000-00-00' OR pd2.date_start < NOW()) AND (pd2.date_end = '0000-00-00' OR pd2.date_end > NOW())) ORDER BY pd2.priority ASC, pd2.price ASC LIMIT 1) AS discount, (SELECT price FROM " + DB_PREFIX + "product_special ps WHERE ps.product_id = p.product_id "+specialQuery+" AND ps.seller_id='"+ req.query.seller_id +"' AND ((ps.date_start = '0000-00-00' OR ps.date_start < NOW()) AND (ps.date_end = '0000-00-00' OR ps.date_end > NOW())) ORDER BY ps.priority ASC, ps.price ASC LIMIT 1) AS special,( SELECT quantity FROM "+DB_PREFIX+"cart WHERE device_id ='"+device_id+"' and product_id='"+newRes[i]['product_id']+"' ) as product_in_cart,( SELECT product_id FROM "+DB_PREFIX+"customer_wishlist WHERE device_id ='"+device_id+"' AND seller_id='"+ req.query.seller_id +"' AND product_id='"+newRes[i]['product_id']+"') as in_wishlist, (SELECT ss.name FROM " + DB_PREFIX + "stock_status ss WHERE ss.stock_status_id = p.stock_status_id AND ss.language_id = '" + languageid  + "') AS stock_status FROM " + DB_PREFIX + "product p LEFT JOIN " + DB_PREFIX + "product_description pd ON (p.product_id = pd.product_id) LEFT JOIN " + DB_PREFIX + "product_to_store p2s ON (p.product_id = p2s.product_id) LEFT JOIN " + DB_PREFIX + "sellers_products sp ON (p.product_id = sp.product_id) WHERE p.product_id = '" + newRes[i]['product_id'] + "' AND sp.seller_id = '" + req.query.seller_id + "' AND pd.language_id = '" + languageid + "' AND p.status = '1' AND p.date_available <= NOW() AND p2s.store_id = 0 ",function(err,proData){

                proArray[pathNumber] = JSON.parse(proData);
                pathNumber++;
            });
        }
        setTimeout(function () {
            res.send(proArray);
            proArray = [];
        },100);
    });
});


app.get("/search2",function(req,res){

	var languageid = req.query.language_id;
	
	
    DataQuery("SELECT p.product_id, (SELECT AVG(rating) AS total FROM "+DB_PREFIX+"review r1 WHERE r1.product_id = p.product_id AND r1.status = '1' GROUP BY r1.product_id) AS rating, (SELECT price FROM "+DB_PREFIX+"product_discount pd2 WHERE pd2.product_id = p.product_id AND pd2.customer_group_id = '1' AND pd2.quantity = '1' AND ((pd2.date_start = '0000-00-00' OR pd2.date_start < NOW()) AND (pd2.date_end = '0000-00-00' OR pd2.date_end > NOW())) ORDER BY pd2.priority ASC, pd2.price ASC LIMIT 1) AS discount, (SELECT price FROM "+DB_PREFIX+"product_special ps WHERE ps.product_id = p.product_id AND ps.customer_group_id = '1' AND ((ps.date_start = '0000-00-00' OR ps.date_start < NOW()) AND (ps.date_end = '0000-00-00' OR ps.date_end > NOW())) ORDER BY ps.priority ASC, ps.price ASC LIMIT 1) AS special FROM "+DB_PREFIX+"product p LEFT JOIN "+DB_PREFIX+"product_description pd ON (p.product_id = pd.product_id) LEFT JOIN "+DB_PREFIX+"product_to_store p2s ON (p.product_id = p2s.product_id) LEFT JOIN "+DB_PREFIX+"sellers_products sp ON (p.product_id = sp.product_id) WHERE sp.seller_id= '"+req.query.seller_id+"' AND p.status = '1' AND p.date_available <= NOW() AND p2s.store_id = '0' AND ( pd.name LIKE '%"+req.query.search+"%' OR pd.tag LIKE '%"+req.query.search+"%' OR LCASE(p.model) = '"+req.query.search+"' OR LCASE(p.sku) = '"+req.query.search+"' OR LCASE(p.upc) = '"+req.query.search+"' OR LCASE(p.ean) = '"+req.query.search+"' OR LCASE(p.jan) = '"+req.query.search+"' OR LCASE(p.isbn) = '"+req.query.search+"' OR LCASE(p.mpn) = '"+req.query.search+"') GROUP BY p.product_id ORDER BY p.sort_order ASC, LCASE(pd.name) ASC LIMIT "+req.query.start +",9" ,function(err,newRes){
      
        newRes = JSON.parse(newRes);
		
		var result2 = new Object();
		result2.message = "success";
        
        var pathNumber = 0;
        for(i=0;i<=newRes.length-1;i++){

            getProduct = DataQuery("SELECT DISTINCT p.option_available, p.status, p.product_id, p.quantity, sp.price, pd.meta_description, pd.name AS name, p.image, (SELECT price FROM " + DB_PREFIX + "product_discount pd2 WHERE pd2.product_id = p.product_id  AND pd2.quantity = '1' AND ((pd2.date_start = '0000-00-00' OR pd2.date_start < NOW()) AND (pd2.date_end = '0000-00-00' OR pd2.date_end > NOW())) ORDER BY pd2.priority ASC, pd2.price ASC LIMIT 1) AS discount, (SELECT price FROM " + DB_PREFIX + "product_special ps WHERE ps.product_id = p.product_id AND ps.seller_id='"+ req.query.seller_id +"' AND ((ps.date_start = '0000-00-00' OR ps.date_start < NOW()) AND (ps.date_end = '0000-00-00' OR ps.date_end > NOW())) ORDER BY ps.priority ASC, ps.price ASC LIMIT 1) AS special,( SELECT quantity FROM "+DB_PREFIX+"cart WHERE device_id ='"+device_id+"' and product_id='"+newRes[i]['product_id']+"' ) as product_in_cart,( SELECT product_id FROM "+DB_PREFIX+"customer_wishlist WHERE device_id ='"+device_id+"' AND seller_id='"+ req.query.seller_id +"' AND product_id='"+newRes[i]['product_id']+"') as in_wishlist, (SELECT ss.name FROM " + DB_PREFIX + "stock_status ss WHERE ss.stock_status_id = p.stock_status_id AND ss.language_id = '" + languageid  + "') AS stock_status FROM " + DB_PREFIX + "product p LEFT JOIN " + DB_PREFIX + "product_description pd ON (p.product_id = pd.product_id) LEFT JOIN " + DB_PREFIX + "product_to_store p2s ON (p.product_id = p2s.product_id) LEFT JOIN " + DB_PREFIX + "sellers_products sp ON (p.product_id = sp.product_id) WHERE p.product_id = '" + newRes[i]['product_id'] + "' AND sp.seller_id = '" + req.query.seller_id + "' AND pd.language_id = '" + languageid + "' AND p.status = '1' AND p.date_available <= NOW() AND p2s.store_id = 0 ",function(err,proData){

                proArray[pathNumber] = JSON.parse(proData);
                pathNumber++;
            });
        }
		result2.products = proArray;
        setTimeout(function () {
            res.send(result2);
            proArray = [];
        },100);
    });
});

//Get SubCategory
app.get("/getSubCategory",function(req,res){
    var languageid = req.query.language_id;
    if(req.query.is_area == 1){

        DataQuery("SELECT c.category_id,c.image,cd.name,cd.description FROM "+DB_PREFIX+"category c LEFT JOIN "+DB_PREFIX+"category_description cd ON (c.category_id = cd.category_id) WHERE c.category_id IN(SELECT DISTINCT(sp.category_id) FROM "+DB_PREFIX+"saddress sa LEFT JOIN "+DB_PREFIX+"sellercategories sp ON (sa.seller_id=sp.seller_id) WHERE sa.seller_id="+req.query.seller_id+" AND sa.area_id="+req.query.area_id+" ) AND c.parent_id = '"+req.query.cat_id+"' AND cd.language_id = '"+languageid+"' AND c.status = '1' ORDER BY c.sort_order, LCASE(cd.name)",function(err,newRes){
            
            res.send(JSON.parse(newRes));
        });
    }else{
        DataQuery("SELECT c.category_id,c.image,cd.name,cd.description FROM "+DB_PREFIX+"category c LEFT JOIN "+DB_PREFIX+"category_description cd ON (c.category_id = cd.category_id)  WHERE c.category_id IN( SELECT DISTINCT(a.category_id) FROM( SELECT sp.category_id, (6371 * acos( cos( radians("+req.query.latitude+") ) * cos( radians( sa.latitude ) ) * cos( radians( sa.longitude ) - radians("+req.query.longitude+") ) + sin( radians("+req.query.latitude+") ) * sin( radians(sa.latitude ) ) ) ) AS distance FROM  "+DB_PREFIX+"saddress sa LEFT JOIN "+DB_PREFIX+"sellercategories sp ON (sa.seller_id = sp.seller_id)WHERE sa.seller_id="+req.query.seller_id+" HAVING distance < "+req.query.radius+") a) AND c.parent_id = '"+req.query.cat_id+"' AND cd.language_id = '"+languageid+"'   AND c.status = '1' ORDER BY c.sort_order, LCASE(cd.name)",function(err,newRes){
            
            res.send(JSON.parse(newRes));
        });
    }
    
});

function DataQuery(querySQL,callback){

    result = [];
    db.query(querySQL, (err, res) => {
        if(err) throw err
        if(res.toString().length != 0){
            result = res
        }else{
            result = '[{"msg":"fail"}]';
        }
        callback(null, JSON.stringify(result));
    });
}

function DataQuery2(querySQL,callback){

    result = [];
    db.query(querySQL, (err, res) => {
        if(err) throw err
        if(res.toString().length != 0){
            result = res;
			callback(null, JSON.stringify(result));
        }else{
            result = 'fail';
			callback(null, result);
        }
        
    });
}

db.connect((err) => {
    if (err) {
        throw err;
    }
    console.log('Connected to database');
});

global.db = db;

app.set('port', process.env.port || port);
app.set('view engine', 'ejs');
app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());

app.listen(port, () => {
    console.log(`Server running on port: ${port}`);
});