//Load HTTP module
const http = require("http");
const hostname = 'qareeb.com';
const port = 3002;

//Create HTTP server and listen on port 3000 for requests
const server = http.createServer((req, res) => {

  //Set the response HTTP header with HTTP status and Content type
  res.statusCode = 200;
  res.setHeader('Content-Type', 'text/plain');
  res.end('Hello World\n');
});

//listen for request on port 3000, and as a callback function have the port listened on logged
server.listen(port, hostname, () => {
  console.log(`Server running at http://${hostname}:${port}/`);
});

/*

var http = require('http');

//create a server object:
http.createServer(function (req, res) {
  res.write('Hello World!'); 
  res.end(); 
}).listen(3000);*/