<?php

include("../../config.php");

define('HOST',DB_HOSTNAME);
define('USER',DB_USERNAME);
define('PASS',DB_PASSWORD);
define('DB',DB_DATABASE);

$con = mysqli_connect(HOST,USER,PASS,DB); 

$apicall = $_REQUEST['apicall'];
$languageid = 1;
$customer_group_id = 1;


if(isset($apicall) && $apicall=='filterLabel'){
	
		
	$output = array();
	
	$tmp[] = 'Default';
	$tmp[] = 'Name (A-Z)';
	$tmp[] = 'Name (Z-A)';
	$tmp[] = 'Price (Low-High)';
	$tmp[] = 'Price (High-Low)';
	$tmp[] = 'Price Range';
	
	$output['filter'] = $tmp;
	
	$tmp2[] = '1';
	$tmp2[] = '20';
	$output['priceRange'] = $tmp2;

	echo json_encode($output,JSON_UNESCAPED_SLASHES);
	
	exit;
	
	
}

if(isset($apicall) && $apicall=='getCity'){
	$output = array();
	$sqlCity = "SELECT * FROM " . DB_PREFIX . "city ";
	$sqlCityRes = mysqli_query($con,$sqlCity);
	while($sqlCityRow=mysqli_fetch_assoc($sqlCityRes)){
		$output[] = $sqlCityRow;
	}
	echo json_encode($output,JSON_UNESCAPED_SLASHES);
	exit;
}

if(isset($apicall) && $apicall=='getSellerSpecialProduct'){
	
	
	$seller_id = $_REQUEST['seller_id'];
	
	$sql = "SELECT DISTINCT p.product_id	
	FROM oc_product p 
	LEFT JOIN oc_product_special ps ON (p.product_id = ps.product_id) 
	LEFT JOIN oc_product_description pd ON (p.product_id = pd.product_id) 
	LEFT JOIN oc_sellers_products sp ON (p.product_id = sp.product_id) 
	WHERE ps.seller_id ='" . $seller_id . "' 
	AND sp.seller_id ='" . $seller_id . "'
	AND p.status = '1' 
	AND p.date_available <= NOW() 
	AND ps.customer_group_id = '" . $customer_group_id . "' 
	AND ((ps.date_start = '0000-00-00' OR ps.date_start < NOW()) 
	AND (ps.date_end = '0000-00-00' OR ps.date_end > NOW())) 
	GROUP BY ps.product_id ORDER BY p.sort_order ASC";
	
	$checkProduct = mysqli_query($con,$sql);
	
	echo mysqli_num_rows($checkProduct);
	
	exit;
}

if(isset($apicall) && $apicall=='getSubCategory'){
	$output = array();
	$cat_id = $_REQUEST['cat_id'];
	$language_id = $_REQUEST['language_id'];
	$seller_id = $_REQUEST['seller_id'];
	
	mysqli_set_charset($con,"utf8");
	$sqlCategory = "SELECT c.category_id,c.image,REPLACE(cd.name,'&amp;','&') AS name,cd.description FROM " . DB_PREFIX . "category c LEFT JOIN " . DB_PREFIX . "category_description cd ON (c.category_id = cd.category_id) WHERE c.category_id IN(SELECT DISTINCT(sp.category_id) FROM " . DB_PREFIX . "saddress sa 
		LEFT JOIN " . DB_PREFIX . "sellercategories sp ON (sa.seller_id=sp.seller_id)" . " 
		WHERE sa.seller_id=" . $seller_id . " ) AND c.parent_id = '" . $cat_id . "' AND cd.language_id = '" . (int) $language_id . "'   AND c.status = '1' ORDER BY c.sort_order, LCASE(cd.name)";
	$selectCatRes = mysqli_query($con,$sqlCategory);
		while($selectCatRow=mysqli_fetch_assoc($selectCatRes)){
			
			$sqlSubCatCheck = "SELECT category_id FROM ".DB_PREFIX."category WHERE parent_id = ".$selectCatRow['category_id'];
			$sqlSubCatCheckRes = mysqli_query($con,$sqlSubCatCheck);
			if(mysqli_num_rows($sqlSubCatCheckRes)>0){
				$selectCatRow['sub_category'] = 1;
			} else {
				$selectCatRow['sub_category'] = 0;
			}
				
			$output[] = $selectCatRow;
		}
	echo json_encode($output,JSON_UNESCAPED_SLASHES);
	exit;
}

if(isset($apicall) && $apicall=='updateCartQuantity'){
	$output = array();
	$product_quantity = $_REQUEST['product_quantity'];
	$device_id = $_REQUEST['device_id'];
	$product_id = $_REQUEST['product_id'];
	$customer_id = $_REQUEST['customer_id'];
	$action = $_REQUEST['action'];
	
	
	if(isset($customer_id) && $customer_id>0){
		$where = 'customer_id = '.$customer_id;
	} else {
		$where = "device_id = '".$device_id."'";
	}
	
		if($action == 'add'){
			$addQuery = "UPDATE ".DB_PREFIX ."cart SET quantity = quantity+1 WHERE product_id = ".$product_id." AND ".$where;
			mysqli_query($con,$addQuery);
			
		} else if($action == 'minus'){
			$query = "SELECT quantity FROM " . DB_PREFIX . "cart WHERE product_id = '" . $product_id."' AND ".$where;
			$resCart = mysqli_query($con,$query);
			
			if(mysqli_num_rows($resCart)>0){
				$rowCart=mysqli_fetch_assoc($resCart);
				if($rowCart['quantity']==1){
					$delQuery = "DELETE FROM " . DB_PREFIX . "cart WHERE product_id = '" . $product_id."' AND ".$where;
					mysqli_query($con,$delQuery);
				} else {
					$minusQuery = "UPDATE " . DB_PREFIX . "cart SET quantity = quantity-1 WHERE product_id = '" . $product_id."' AND ".$where;
					mysqli_query($con,$minusQuery);
				}
			}
		}
	
	//echo json_encode($output,JSON_UNESCAPED_SLASHES);
	exit;
}

if(isset($apicall) && $apicall=='getStoreBankDetail'){
	$output = array();
	$language_id = $_REQUEST['language'];
	if($language_id==1){
		$query  = "SELECT * FROM `oc_setting` WHERE `key` LIKE ('bank_transfer_bank1')";
	} else if($language_id==3){
		$query  = "SELECT * FROM `oc_setting` WHERE `key` LIKE ('bank_transfer_bank3')";
	}
	$res = mysqli_query($con,$query);
	$row=mysqli_fetch_assoc($res);
	
	
	echo json_encode($row['value'],JSON_UNESCAPED_SLASHES);
	exit;

	
}

if(isset($apicall) && $apicall=='getStoreCategories'){
	$output = array();
	
	$seller_id = $_REQUEST['seller'];
	$language_id = $_REQUEST['language'];
	
	$parent_id=0;
	
	if(isset($_REQUEST['parent_id']) && $_REQUEST['parent_id']>0){
		$parent_id = $_REQUEST['parent_id'];
	}
	
	mysqli_set_charset($con,"utf8");
	$sqlCategory = "SELECT c.category_id,c.image,REPLACE(cd.name,'&amp;','&') AS name,cd.description FROM " . DB_PREFIX . "category c LEFT JOIN " . DB_PREFIX . "category_description cd ON (c.category_id = cd.category_id) WHERE c.category_id IN(SELECT DISTINCT(sp.category_id) FROM " . DB_PREFIX . "saddress sa 
		LEFT JOIN " . DB_PREFIX . "sellercategories sp ON (sa.seller_id=sp.seller_id)" . " 
		WHERE sa.seller_id=" . $seller_id . " ) AND c.parent_id = '" . $parent_id . "' AND cd.language_id = '" . (int) $language_id . "'   AND c.status = '1' ORDER BY c.sort_order, LCASE(cd.name)";
	$selectCatRes = mysqli_query($con,$sqlCategory);
		while($selectCatRow=mysqli_fetch_assoc($selectCatRes)){
			
			$selectCatRow['name'] = html_entity_decode($selectCatRow['name']);
			$output[] = $selectCatRow;
		}
		
		echo json_encode($output,JSON_UNESCAPED_SLASHES);
		exit;
}


if(isset($apicall) && $apicall=='getCartWithDeliveryCharges'){
	$output = array();
	
	$device_id = $_REQUEST['device_id'];
	$customer_id = $_REQUEST['customer_id'];
	$languageid = $_REQUEST['language'];
	
	if(isset($customer_id) && $customer_id >0) {
		$filter = 'c.customer_id = "'.$customer_id.'"';
	} else if(isset($device_id) && $device_id !='') {
		$filter = 'c.device_id = "'.$device_id.'"';
	}
	
	mysqli_query("SET NAMES 'utf8'");
 
	$getCartProducts = 'SELECT c.cart_id, p.product_id, p.model,sp.quantity as stock,p.image,c.quantity, sp.price, REPLACE(pd.name,"&amp;","&") AS name, (sp.price*c.quantity) as total 
			FROM `oc_cart` c LEFT JOIN oc_product p ON p.product_id = c.product_id LEFT JOIN oc_sellers_products sp ON sp.seller_id = c.seller_id 
			LEFT JOIN oc_product_description pd ON pd.product_Id = p.product_id 
			WHERE pd.language_id = '.$languageid.' AND '.$filter.' GROUP BY c.product_id ORDER BY pd.name';
			
	$cartProdRes = mysqli_query($con,$getCartProducts);
	
	$output['msg'] = "Success";
	$tmpArr = array();
	$total = 0;
	while($cartProdRow=mysqli_fetch_array($cartProdRes)){
		$tmpArr[] = $cartProdRow;
		$total = $total + $cartProdRow['total'];
	}
	$output['order_total'] = $total;
	$output['result'] = $tmpArr;
	echo json_encode($output,JSON_UNESCAPED_UNICODE);
	//echo json_encode($output,JSON_UNESCAPED_SLASHES);
	
	exit;
}


if(isset($apicall) && $apicall=='getStoreHomeCategory'){
	$output = array();
	
	$device_id = (isset($_REQUEST['device_id'])) ? $_REQUEST['device_id'] : '12';
	$seller_id = $_REQUEST['seller_id'];
	$languageid = $_REQUEST['language_id'];
	
	$specialQuery = '';
	
	if(isset($_REQUEST['city_id'])){		
		$getCustomerGroudId = "SELECT customer_group_id FROM " . DB_PREFIX . "customer_group WHERE cityId = '" . $_REQUEST['city_id'] . "'";
			
		mysqli_set_charset($con,"utf8");
		$getCGRes = mysqli_query($con,$getCustomerGroudId);
		$getCGRow = mysqli_fetch_assoc($getCGRes);
		$customer_group_id =  $getCGRow['customer_group_id'];

		$specialQuery  = "AND ps.customer_group_id = '" . $customer_group_id . "'";
	}
	
	// Featured Products
	$tmpArr = array();

	$setFullGroup = 'SET SESSION sql_mode =
                  REPLACE(REPLACE(REPLACE(
                  @@sql_mode,
                  "ONLY_FULL_GROUP_BY,", ""),
                  ",ONLY_FULL_GROUP_BY", ""),
                  "ONLY_FULL_GROUP_BY", "")';
	
	$featuredProductQuery = "SELECT DISTINCT ps.product_id, REPLACE(pd.name,'&amp;','&') 
	AS name, p.image, sp.price, '' as regularPrice  
	FROM oc_product_feature ps 
	LEFT JOIN oc_product p ON (ps.product_id = p.product_id) 
	LEFT JOIN oc_product_description pd ON (p.product_id = pd.product_id) 
	LEFT JOIN oc_product_to_store p2s ON (p.product_id = p2s.product_id) 
	LEFT JOIN oc_sellers_products sp ON (p.product_id = sp.product_id) 
	WHERE pd.language_id = '".$languageid."' 
	AND ps.seller_id = '".$seller_id."' 
	AND sp.seller_id  = '".$seller_id."' 
	AND p.date_available <= NOW() 
	AND ((ps.date_start LIKE '0000-00-00' 
	OR ps.date_start < NOW()) 
	AND (ps.date_end LIKE '0000-00-00' 
	OR ps.date_end > NOW())) 
	AND p.status = 1 
	AND p2s.store_id = 0 
	GROUP BY ps.product_id ORDER BY p.sort_order ASC, LCASE(pd.name) ASC LIMIT 0,20
	";

	$setFullGroupRes = mysqli_query($con, $setFullGroup);
	$featuredProductRes = mysqli_query($con,$featuredProductQuery);
	// mysqli_num_rows($featuredProductRes);
	
	if(mysqli_num_rows($featuredProductRes)>0){
		
		$tmpArr['category_id'] = '0';
		if($languageid==1){
			$tmpArr['name'] = 'Featured Products';
		} else if($languageid==3){
			$tmpArr['name'] = 'منتجات مميزة';
		}
		
		$tmPrdArr = array();
		while($featuredProductRow=mysqli_fetch_assoc($featuredProductRes)){
			// echo '<pre>';
			// print_r($featuredProductRow);
			// echo '</pre>';
			// exit;
			$product_id = $featuredProductRow['product_id'];
			
			$getProduct = "SELECT DISTINCT p.status, p.product_id, p.quantity, sp.price, pd.meta_description, REPLACE(pd.name,'&amp;','&') AS product_name, p.image, COALESCE((SELECT price FROM " . DB_PREFIX . "product_discount pd2 WHERE pd2.product_id = p.product_id  AND pd2.quantity = '1' AND ((pd2.date_start = '0000-00-00' OR pd2.date_start < NOW()) AND (pd2.date_end = '0000-00-00' OR pd2.date_end > NOW())) ORDER BY pd2.priority ASC, pd2.price ASC LIMIT 1),0) AS discount, COALESCE((SELECT price FROM " . DB_PREFIX . "product_special ps WHERE ps.product_id = p.product_id ".$specialQuery." AND ps.seller_id='". $seller_id."' AND ((ps.date_start = '0000-00-00' OR ps.date_start < NOW()) AND (ps.date_end = '0000-00-00' OR ps.date_end > NOW())) ORDER BY ps.priority ASC, ps.price ASC LIMIT 1),0) AS special,COALESCE((SELECT quantity FROM ".DB_PREFIX."cart WHERE device_id ='".$device_id."' and product_id='".$product_id."'),0) as product_in_cart,( SELECT product_id FROM ".DB_PREFIX."customer_wishlist WHERE device_id ='".$device_id."' AND seller_id='". $seller_id ."' AND product_id='".$product_id."') as in_wishlist, (SELECT ss.name FROM " . DB_PREFIX . "stock_status ss WHERE ss.stock_status_id = p.stock_status_id AND ss.language_id = '" . $languageid  . "') AS stock_status FROM " . DB_PREFIX . "product p LEFT JOIN " . DB_PREFIX . "product_description pd ON (p.product_id = pd.product_id) LEFT JOIN " . DB_PREFIX . "product_to_store p2s ON (p.product_id = p2s.product_id) LEFT JOIN " . DB_PREFIX . "sellers_products sp ON (p.product_id = sp.product_id) WHERE p.product_id = '" . $product_id . "' AND sp.seller_id = '" . $seller_id . "' AND pd.language_id = '" . $languageid . "' AND p.status = '1' AND p.date_available <= NOW() AND p2s.store_id = 0";
			
			mysqli_set_charset($con,"utf8");
			$getProductRes = mysqli_query($con,$getProduct);
			$getProductRow=mysqli_fetch_assoc($getProductRes);
			$getProductRow['price'] = $getProductRow['price'].' SR';
			//$getProductRow['price'] = number_format($getProductRow['price'],2).' SR';
			$getProductRow['name'] = html_entity_decode($getProductRow['product_name']);
			
			//option available or not
			$product_option_queryF = "SELECT * FROM " . DB_PREFIX . "product_option po  WHERE po.product_id = '" . (int)$product_id . "' AND po.seller_id = '" . (int)$seller_id . "'";
			
			$getProductOptionF = mysqli_query($con,$product_option_queryF);

			if(mysqli_num_rows($getProductOptionF)>0){			
				$option_availableF = 1;
			} else {
				$option_availableF = 0;
			}	
			
			$getProductRow['option_available'] = $option_availableF;
			
			//$getProductRow['option_available'] = (int)$getProductRow['option_available'];
			
			if($getProductRow['special']=='0.0000'){
				$getProductRow['special'] = '0';
			}
			if($getProductRow['name'] != '' && $getProductRow['price'] != 0){	
				$tmPrdArr[] = $getProductRow;
			}
		}
		// echo'<pre>';
		// print_r($tmPrdArr);
		// exit;
		$tmpArr['products'] = $tmPrdArr;
		$output[] = $tmpArr;
	}
	// print_r($output);
	// Special Products
	$tmpArr = array();
			
	$specialProductQuery = "SELECT DISTINCT ps.product_id, pd.name, pd.description, p.image, sp.price as regularPrice, ps.price as price, (((sp.price-ps.price)/sp.price)*100) as discountRate FROM oc_product_special ps LEFT JOIN oc_product p ON (ps.product_id = p.product_id) LEFT JOIN oc_product_description pd ON (p.product_id = pd.product_id) LEFT JOIN oc_sellers_products sp ON (p.product_id = sp.product_id) WHERE  pd.language_id = '".$languageid."' AND (sp.seller_id = '".$seller_id."' AND ps.seller_id = '".$seller_id."') AND p.status = '1' AND p.date_available <= NOW() AND ((ps.date_start = '0000-00-00' OR ps.date_start < NOW()) AND (ps.date_end = '0000-00-00' OR ps.date_end > NOW())) GROUP BY ps.product_id ORDER BY p.sort_order ASC, LCASE(pd.name) ASC LIMIT 0,20";
	
	$specialProductRes = mysqli_query($con,$specialProductQuery);
	if(mysqli_num_rows($specialProductRes)>0){
		
		$tmpArr['category_id'] = '1';
		if($languageid==1){
			$tmpArr['name'] = 'Special Products';
		} else if($languageid==3){
			$tmpArr['name'] = 'عروض خاصة';
		}
		
		$tmPrdArr = array();
		while($specialProductRow=mysqli_fetch_assoc($specialProductRes)){
			$product_id = $specialProductRow['product_id'];
			
			$getProduct = "SELECT DISTINCT p.status, p.product_id, p.quantity, sp.price, pd.meta_description, REPLACE(pd.name,'&amp;','&') AS product_name, p.image, COALESCE((SELECT price FROM " . DB_PREFIX . "product_discount pd2 WHERE pd2.product_id = p.product_id  AND pd2.quantity = '1' AND ((pd2.date_start = '0000-00-00' OR pd2.date_start < NOW()) AND (pd2.date_end = '0000-00-00' OR pd2.date_end > NOW())) ORDER BY pd2.priority ASC, pd2.price ASC LIMIT 1),0) AS discount, ps.price AS special,COALESCE((SELECT quantity FROM ".DB_PREFIX."cart WHERE device_id ='".$device_id."' and product_id='".$product_id."'),0) as product_in_cart,( SELECT product_id FROM ".DB_PREFIX."customer_wishlist WHERE device_id ='".$device_id."' AND seller_id='". $seller_id ."' AND product_id='".$product_id."') as in_wishlist, (SELECT ss.name FROM " . DB_PREFIX . "stock_status ss WHERE ss.stock_status_id = p.stock_status_id AND ss.language_id = '" . $languageid  . "') AS stock_status 
			FROM " . DB_PREFIX . "product p 
			LEFT JOIN " . DB_PREFIX . "product_special ps ON (p.product_id = ps.product_id) 
			LEFT JOIN " . DB_PREFIX . "product_description pd ON (p.product_id = pd.product_id) 
			LEFT JOIN " . DB_PREFIX . "product_to_store p2s ON (p.product_id = p2s.product_id) 
			LEFT JOIN " . DB_PREFIX . "sellers_products sp ON (p.product_id = sp.product_id) 
			WHERE p.product_id = '" . $product_id . "' 
			AND ps.seller_id ='" . $seller_id . "'
			AND sp.seller_id = '" . $seller_id . "' 
			AND pd.language_id = '" . $languageid . "' 
			AND p.status = '1' 
			AND p.date_available <= NOW() ".$specialQuery." 
			AND ((ps.date_start = '0000-00-00' OR ps.date_start < NOW()) 
			AND (ps.date_end = '0000-00-00' OR ps.date_end > NOW())) 
			GROUP BY ps.product_id 
			AND p2s.store_id = 0";
			
			mysqli_set_charset($con,"utf8");
			$getProductRes = mysqli_query($con,$getProduct);
			$getProductRow=mysqli_fetch_assoc($getProductRes);
			$getProductRow['price'] = $getProductRow['price'].' SR';
			//$getProductRow['price'] = number_format($getProductRow['price'],2).' SR';
			$getProductRow['name'] = html_entity_decode($getProductRow['product_name']);
			
			//option available or not
			$product_option_queryS = "SELECT * FROM " . DB_PREFIX . "product_option po  WHERE po.product_id = '" . (int)$product_id . "' AND po.seller_id = '" . (int)$seller_id . "'";
			
			$getProductOptionS = mysqli_query($con,$product_option_queryS);

			if(mysqli_num_rows($getProductOptionS)>0){			
				$option_availableS = 1;
			} else {
				$option_availableS = 0;
			}	
			
			$getProductRow['option_available'] = $option_availableS;
			
			//$getProductRow['option_available'] = (int)$getProductRow['option_available'];
			
			if($getProductRow['special']=='0.0000'){
				$getProductRow['special'] = '0';
			}
				
			if($getProductRow['product_id']>0){
				if($getProductRow['name'] != '' && $getProductRow['price'] != 0){
					$tmPrdArr[] = $getProductRow;
				}
			}
		}
		$tmpArr['products'] = $tmPrdArr;
		$output[] = $tmpArr;
	}
	
	
	// get store category 
	$storeCategory = "SELECT special_category FROM oc_sellers WHERE seller_id = ".$seller_id;
	$selectCatRes = mysqli_query($con,$storeCategory);
	$selectCatRow=mysqli_fetch_array($selectCatRes);
	
		// get category name
		//$storeCatName = "SELECT DISTINCT category_id,REPLACE(name,'&amp;','&') AS name FROM  oc_category_description cd WHERE cd.category_id IN (".$selectCatRow['special_category'].") AND cd.language_id = ".$languageid." ";
		
		$storeCatName = "SELECT DISTINCT cd.category_id ,REPLACE(cd.name,'&amp;','&') AS name ,c.sort_order FROM  oc_category_description cd LEFT JOIN oc_category c ON (cd.category_id = c.category_id) WHERE cd.category_id IN (".$selectCatRow['special_category'].") AND cd.language_id = ".$languageid." ORDER BY c.sort_order ASC, cd.name ASC";
		
		mysqli_set_charset($con,"utf8");
		$selectCatRes2 = mysqli_query($con,$storeCatName);
		if($selectCatRes2):
		while($selectCatRow2=mysqli_fetch_assoc($selectCatRes2)){
			$catArr[] = $selectCatRow2;
		}
		endif;

	
	if(isset($catArr)  && count($catArr)>0){
		foreach($catArr as $row){
			$tmpArr = array();
			
			
			$storeCatProdList = "SELECT DISTINCT os.product_id 
			FROM oc_sellercategories os 
			LEFT JOIN oc_product_description pd ON (os.product_id = pd.product_id) 
			LEFT JOIN oc_product p ON (os.product_id = p.product_id) 
			LEFT JOIN oc_sellers_products sp ON (os.product_id = sp.product_id)
			WHERE pd.language_id = '".$languageid."' 
			AND os.seller_id = '".$seller_id."' 
			AND (os.category_id = ".$row['category_id']." 
			AND sp.seller_id = '".$seller_id."' 
			AND  p.status = '1' 
			AND p.date_available <= NOW()) 
			GROUP BY p.product_id 
			ORDER BY p.sort_order ASC LIMIT 0,20";
			$storeCatProListRes = mysqli_query($con,$storeCatProdList);	
			
			$tmPrdArr = array();
			while($storeCatProListRow=mysqli_fetch_assoc($storeCatProListRes)){
				$product_id = $storeCatProListRow['product_id'];
				
				$getProduct = "SELECT DISTINCT p.status, p.product_id, p.quantity, sp.price, pd.meta_description, REPLACE(pd.name,'&amp;','&') AS product_name, p.image, COALESCE((SELECT price FROM " . DB_PREFIX . "product_discount pd2 WHERE pd2.product_id = p.product_id  AND pd2.quantity = '1' AND ((pd2.date_start = '0000-00-00' OR pd2.date_start < NOW()) AND (pd2.date_end = '0000-00-00' OR pd2.date_end > NOW())) ORDER BY pd2.priority ASC, pd2.price ASC LIMIT 1),0) AS discount, COALESCE((SELECT price FROM " . DB_PREFIX . "product_special ps WHERE ps.product_id = p.product_id ".$specialQuery." AND ps.seller_id='". $seller_id."' AND ((ps.date_start = '0000-00-00' OR ps.date_start < NOW()) AND (ps.date_end = '0000-00-00' OR ps.date_end > NOW())) ORDER BY ps.priority ASC, ps.price ASC LIMIT 1),0) AS special,COALESCE((SELECT quantity FROM ".DB_PREFIX."cart WHERE device_id ='".$device_id."' and product_id='".$product_id."'),0) as product_in_cart,( SELECT product_id FROM ".DB_PREFIX."customer_wishlist WHERE device_id ='".$device_id."' AND seller_id='". $seller_id ."' AND product_id='".$product_id."') as in_wishlist, (SELECT ss.name FROM " . DB_PREFIX . "stock_status ss WHERE ss.stock_status_id = p.stock_status_id AND ss.language_id = '" . $languageid  . "') AS stock_status FROM " . DB_PREFIX . "product p LEFT JOIN " . DB_PREFIX . "product_description pd ON (p.product_id = pd.product_id) LEFT JOIN " . DB_PREFIX . "product_to_store p2s ON (p.product_id = p2s.product_id) LEFT JOIN " . DB_PREFIX . "sellers_products sp ON (p.product_id = sp.product_id) WHERE p.product_id = '" . $product_id . "' AND sp.seller_id = '" . $seller_id . "' AND pd.language_id = '" . $languageid . "' AND p.status = '1' AND p.date_available <= NOW() AND p2s.store_id = 0";
				
				mysqli_set_charset($con,"utf8");
				$getProductRes = mysqli_query($con,$getProduct);
				$getProductRow=mysqli_fetch_assoc($getProductRes);
				$getProductRow['price'] = $getProductRow['price'].' SR';
				//$getProductRow['price'] = number_format($getProductRow['price'],2).' SR';
				$getProductRow['name'] = html_entity_decode($getProductRow['product_name']);
				
				//option available or not
				$product_option_queryC = "SELECT * FROM " . DB_PREFIX . "product_option po  WHERE po.product_id = '" . (int)$product_id . "' AND po.seller_id = '" . (int)$seller_id . "'";
				
				$getProductOptionC = mysqli_query($con,$product_option_queryC);

				if(mysqli_num_rows($getProductOptionC)>0){			
					$option_availableC = 1;
				} else {
					$option_availableC = 0;
				}	
				
				$getProductRow['option_available'] = $option_availableC;
				
				//$getProductRow['option_available'] = (int)$getProductRow['option_available'];
				
				if($getProductRow['special']=='0.0000'){
					$getProductRow['special'] = '0';
				}
				if($getProductRow['name'] != '' && $getProductRow['price'] != 0){
					$tmPrdArr[] = $getProductRow;
				}
			}
			if(count($tmPrdArr)>0){
				$tmpArr['category_id'] = $row['category_id'];
				$tmpArr['name'] = strtoupper(html_entity_decode($row['name']));
				$tmpArr['products'] = $tmPrdArr;
				$output[] = $tmpArr;
			}
			
			
		}
	}
	echo json_encode($output,JSON_UNESCAPED_SLASHES);
	exit;
}
