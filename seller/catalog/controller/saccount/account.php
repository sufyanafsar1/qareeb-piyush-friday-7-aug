<?php 
class ControllerSaccountAccount extends Controller { 
	public function index() {
		if (!$this->seller->isLogged()) {
	  		$this->session->data['redirect'] = $this->url->link('saccount/account', '', 'SSL');
	  		$this->response->redirect($this->url->link('saccount/login', '', 'SSL'));
    	} 

		$this->language->load('saccount/account');
		$this->document->setTitle($this->language->get('heading_title'));

      	$data['breadcrumbs'] = array();
      	$data['breadcrumbs'][] = array(
        	'text'      => $this->language->get('text_home'),
			'href'      => $this->url->link('common/home')
      	);

      	$data['breadcrumbs'][] = array(       	
        	'text'      => $this->language->get('text_account'),
			'href'      => $this->url->link('saccount/account', '', 'SSL')
      	);

		if (isset($this->session->data['success'])) {
    		$data['success'] = $this->session->data['success'];
			
			unset($this->session->data['success']);
		} else {
			$data['success'] = '';
		}

    	$data['heading_title'] = $this->language->get('heading_title');

    	$data['text_my_account'] = $this->language->get('text_my_account');
		$data['text_my_orders'] = $this->language->get('text_my_orders');
		$data['text_my_newsletter'] = $this->language->get('text_my_newsletter');
    	$data['text_edit'] = $this->language->get('text_edit');
    	$data['text_password'] = $this->language->get('text_password');
    	$data['text_address'] = $this->language->get('text_address');
		$data['text_wishlist'] = $this->language->get('text_wishlist');
    	$data['text_order'] = $this->language->get('text_order');
    	$data['text_download'] = $this->language->get('text_download');
		$data['text_reward'] = $this->language->get('text_reward');
		$data['text_return'] = $this->language->get('text_return');
		$data['text_transaction'] = $this->language->get('text_transaction');
		$data['text_newsletter'] = $this->language->get('text_newsletter');
		$data['text_getpaid'] = $this->language->get('text_getpaid');
		$data['text_messagebox'] = $this->language->get('text_messagebox');
		$data['text_checkmessage'] = $this->language->get('text_checkmessage');
    	$data['edit'] = $this->url->link('saccount/edit', '', 'SSL');
    	$data['password'] = $this->url->link('saccount/password', '', 'SSL');

		$address_id  = $this->seller->getAddressId();		
		$seller_id  = $this->seller->getId();		

		$data['smembership_module_status'] = $this->config->get('membership_status');
		if ($this->config->get('membership_status')=='1'){
			$data['text_plan'] = $this->language->get('text_plan');
			$data['plan'] = $this->url->link('saccount/plan', '', 'SSL');
		}

		$data['text_images'] = $this->language->get('text_images');

		$data['images'] = $this->url->link('saccount/uploadimages', 'seller_id=' . $seller_id, 'SSL');
		$data['address'] = $this->url->link('saccount/address/update', 'address_id=' . $address_id, 'SSL');

		$data['text_export'] = $this->language->get('text_export');
		$data['export'] = $this->url->link('saccount/smartexportimport','', 'SSL');

		$data['wishlist'] = $this->url->link('saccount/wishlist');
    	$data['order'] = $this->url->link('saccount/order', '', 'SSL');
    	$data['download'] = $this->url->link('saccount/download', '', 'SSL');
		$data['return'] = $this->url->link('saccount/return', '', 'SSL');
		$data['transaction'] = $this->url->link('saccount/transaction', '', 'SSL');
		$data['newsletter'] = $this->url->link('saccount/newsletter', '', 'SSL');
		$data['text_extensions'] = $this->language->get('text_extensions');
		$data['text_manageextensions'] = $this->language->get('text_manageextensions');
		$data['text_addextensions'] = $this->language->get('text_addextensions');
		$data['manageextensions'] = $this->url->link('saccount/extension', '', 'SSL');
		$data['addextensions'] = $this->url->link('saccount/extension/insert', '', 'SSL');	
		$data['address2'] = $this->url->link('saccount/address2', '', 'SSL');
		$data['messagebox'] = $this->url->link('saccount/messages', '', 'SSL');

		$data['text_plan'] = $this->language->get('text_plan');
		$data['text_my_plan'] = $this->language->get('text_my_plan');
		$data['plan'] = $this->url->link('saccount/plan', '', 'SSL');

		$data['offer'] = $this->url->link('saccount/offer', '', 'SSL');	
		$data['attributes'] = $this->url->link('saccount/attribute', '', 'SSL');	
		$data['text_attributes'] = $this->language->get('text_attributes');	
		$data['text_offer'] = $this->language->get('text_offer');	

		/**code added here**/
		$data['option'] = $this->url->link('saccount/option', '', 'SSL');
		$data['category'] = $this->url->link('saccount/category', '', 'SSL');
		$data['categoryid'] = $this->url->link('saccount/categoryid', '', 'SSL');
		$data['text_option'] = $this->language->get('text_option');
		$data['text_category'] = $this->language->get('text_category');	
		$data['text_download'] = $this->language->get('text_download');			
		/**/

		if ($this->config->get('reward_status')) {
			$data['reward'] = $this->url->link('saccount/reward', '', 'SSL');
		} else {
			$data['reward'] = '';
		}

		$data['column_left'] = $this->load->controller('common/column_left');
		$data['column_right'] = $this->load->controller('common/column_right');
		$data['content_top'] = $this->load->controller('common/content_top');
		$data['content_bottom'] = $this->load->controller('common/content_bottom');
		$data['footer'] = $this->load->controller('common/footer');
		$data['header'] = $this->load->controller('common/header');

		if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/template/saccount/account.tpl')) {
			$this->response->setOutput($this->load->view($this->config->get('config_template') . '/template/saccount/account.tpl', $data));
		} else {
			$this->response->setOutput($this->load->view('default/template/saccount/account.tpl', $data));
		}
  	}
}
?>