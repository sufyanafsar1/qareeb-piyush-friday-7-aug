<?php
class ControllerSAccountForgotten extends Controller {
	private $error = array();

	public function index() {
		if ($this->seller->isLogged()) {
			$this->response->redirect($this->url->link('saccount/saccount', '', 'SSL'));
		}

		$this->load->language('saccount/forgotten');

		$this->document->setTitle($this->language->get('heading_title'));

		$this->load->model('saccount/seller');

		if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validate()) {
			$this->load->language('mail/forgotten');

			$password = substr(sha1(uniqid(mt_rand(), true)), 0, 10);

			$this->model_saccount_seller->editPassword($this->request->post['email'], $password);

			$subject = sprintf($this->language->get('text_subject'), $this->config->get('config_name'));

			$message  = sprintf($this->language->get('text_greeting'), $this->config->get('config_name')) . "\n\n";
			$message .= $this->language->get('text_password') . "\n\n";
			$message .= $password;
			
			
			$seller_info = $this->model_saccount_seller->getSellerByEmail($this->request->post['email']);
			
			$template = $this->model_saccount_seller->getEmailTemplate('seller-forgot-password-email');	

			$c_name = $seller_info['firstname'] . ' ' . $seller_info['lastname'];
			
			$tags = array("[SELLER_NAME]", "[PASSWORD]", "[SUBJECT]");
			$tagsValues = array($c_name, $password, $template['subject']);
			
			//get subject
			$subject = html_entity_decode($template['subject'], ENT_QUOTES, 'UTF-8');
			$subject = str_replace($tags, $tagsValues, $subject);
			$subject = html_entity_decode($subject, ENT_QUOTES);
									
			//get message body
			$msg = html_entity_decode($template['description'], ENT_QUOTES, 'UTF-8');
			$msg = str_replace($tags, $tagsValues, $msg);
			$msg = html_entity_decode($msg, ENT_QUOTES);
					
			$header = html_entity_decode($this->config->get('config_mail_header'), ENT_QUOTES, 'UTF-8');
			$header = str_replace($tags, $tagsValues, $header);
			$message = html_entity_decode($header, ENT_QUOTES);
			$message .= $msg;
			$message .= html_entity_decode($this->config->get('config_mail_footer'), ENT_QUOTES, 'UTF-8'); 

			$mail = new Mail();		$mail->protocol = $this->config->get('config_mail_protocol');		$mail->parameter = $this->config->get('config_mail_parameter');		$mail->smtp_hostname = $this->config->get('config_mail_smtp_hostname');		$mail->smtp_username = $this->config->get('config_mail_smtp_username');		$mail->smtp_password = html_entity_decode($this->config->get('config_mail_smtp_password'), ENT_QUOTES, 'UTF-8');		$mail->smtp_port = $this->config->get('config_mail_smtp_port');		$mail->smtp_timeout = $this->config->get('config_mail_smtp_timeout');
			$mail->setTo($this->request->post['email']);
			$mail->setFrom($this->config->get('config_email'));
			$mail->setSender($this->config->get('config_name'));
			$mail->setSubject($subject);
			//$mail->setText(html_entity_decode($message, ENT_QUOTES, 'UTF-8'));
			$mail->setHtml($message);
			$mail->send();

			$this->session->data['success'] = $this->language->get('text_success');

			// Add to activity log
			if ($seller_info) {
				$this->load->model('saccount/activity');

				$activity_data = array(
					'seller_id' => $seller_info['seller_id'],
					'name'        => $seller_info['firstname'] . ' ' . $seller_info['lastname']
				);

				$this->model_saccount_activity->addActivity('forgotten', $activity_data);
			}

			$this->response->redirect($this->url->link('saccount/login', '', 'SSL'));
		}

		$data['breadcrumbs'] = array();

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_home'),
			'href' => $this->url->link('common/home')
		);

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_account'),
			'href' => $this->url->link('saccount/account', '', 'SSL')
		);

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_forgotten'),
			'href' => $this->url->link('saccount/forgotten', '', 'SSL')
		);

		$data['heading_title'] = $this->language->get('heading_title');

		$data['text_your_email'] = $this->language->get('text_your_email');
		$data['text_email'] = $this->language->get('text_email');

		$data['entry_email'] = $this->language->get('entry_email');

		$data['button_continue'] = $this->language->get('button_continue');
		$data['button_back'] = $this->language->get('button_back');

		if (isset($this->error['warning'])) {
			$data['error_warning'] = $this->error['warning'];
		} else {
			$data['error_warning'] = '';
		}

		$data['action'] = $this->url->link('saccount/forgotten', '', 'SSL');

		$data['back'] = $this->url->link('saccount/login', '', 'SSL');

		$data['column_left'] = $this->load->controller('common/column_left');
		$data['column_right'] = $this->load->controller('common/column_right');
		$data['content_top'] = $this->load->controller('common/content_top');
		$data['content_bottom'] = $this->load->controller('common/content_bottom');
		$data['footer'] = $this->load->controller('common/footer');
		$data['header'] = $this->load->controller('common/header');

		if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/template/saccount/forgotten.tpl')) {
			$this->response->setOutput($this->load->view($this->config->get('config_template') . '/template/saccount/forgotten.tpl', $data));
		} else {
			$this->response->setOutput($this->load->view('default/template/saccount/forgotten.tpl', $data));
		}
	}

	protected function validate() {
		if (!isset($this->request->post['email'])) {
			$this->error['warning'] = $this->language->get('error_email');
		} elseif (!$this->model_saccount_seller->getTotalSellersByEmail($this->request->post['email'])) {
			$this->error['warning'] = $this->language->get('error_email');
		}

		return !$this->error;
	}
}