<?php
// Text
$_['text_home']          = 'Home';
$_['text_wishlist']      = 'Wish List (%s)';
$_['text_shopping_cart'] = 'Shopping Cart';
$_['text_category']      = 'Categories';
$_['text_account']       = 'My Account';
$_['text_register']      = 'Register';
$_['text_login']         = 'Login';
$_['text_order']         = 'View Your Order History';
$_['text_transaction']   = 'Your Transactions';
$_['text_download']      = 'Manage Downloads';
$_['text_logout']        = 'Logout';
$_['text_checkout']      = 'Checkout';
$_['text_search']        = 'Search';
$_['text_all']           = 'Show All';

$_['text_welcome']           = 'Welcome you are logged in as';
$_['text_extensions'] = 'My Store'; 
$_['text_my_account'] = 'My Account'; 
$_['text_manageextensions'] = 'Manage Products';
$_['text_addextensions']    = 'Add new Product';
$_['text_getpaid']    = 'How you wish to be paid ?';
$_['text_plan']      = 'Upgarde Your Plan';
$_['text_export'] = 'Upload Products in Bulk';
$_['text_images'] = 'Upload Images';
$_['text_plan']      = 'Upgrade your plan';
$_['text_my_plan']     = 'My Plan';
$_['text_my_order']     = 'My Orders';
$_['text_edit']     = 'Edit your account information';
$_['text_password']     = 'Change Password';
$_['text_address']     = 'Modify your address book entries';

$_['text_option']       = 'Manage option';
$_['text_attributes'] = 'Manage Attributes';

$_['text_offer']       = 'Add already listed product';
$_['text_category']    = 'Manage Category';
