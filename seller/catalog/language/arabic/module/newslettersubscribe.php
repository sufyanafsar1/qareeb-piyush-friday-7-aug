<?php
// Heading 
$_['heading_title'] 	 = 'ابقى على اتصال';

//Fields
$_['entry_email'] 		 = 'البريد الإلكتروني';
$_['entry_name'] 		 = 'اسم';

//Buttons
$_['entry_button'] 		 = 'الاشتراك';
$_['entry_unbutton'] 	 = 'إلغاء الاشتراك';

//text
$_['text_subscribe'] 	 = 'اشترك هنا';
$_['text_subscribe_now'] 	 = 'إشترك الآن';
$_['text_email'] 	 = 'وضع عنوان البريد الإلكتروني الخاص بك هنا';

$_['mail_subject']   	 = 'النشرة الإخبارية الاشتراك';

//Error
$_['error_invalid'] 	 = 'بريد إلكتروني خاطئ';

$_['subscribe']	    	 = 'الاشتراك بنجاح';
$_['unsubscribe'] 	     = 'إلغاء الاشتراك بنجاح';
$_['alreadyexist'] 	     = 'موجود مسبقا';
$_['notexist'] 	    	 = 'معرف البريد الإلكتروني غير موجود';

?>