<?php
class ModelLocalisationArea extends Model {
	public function getArea($area_id) {
		$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "area WHERE area_id = '" . (int)$area_id . "' AND status = '1'");

		return $query->row;
	}

	public function getAreasByCityId($city_id) {
		$area_data = $this->cache->get('area.' . (int)$city_id);

		if (!$area_data) {
			$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "area WHERE city_id = '" . (int)$city_id . "' AND status = '1' ORDER BY name");

			$area_data = $query->rows;

			$this->cache->set('area.' . (int)$city_id, $area_data);
		}

		return $area_data;
	}
    
     public function getAreasByArrayId($data = array()) {
		$comma_separated = implode(",", $data);
     
        $sql = "SELECT *, a.name, a.code, c.name AS city FROM " . DB_PREFIX . "area a LEFT JOIN " . DB_PREFIX . "city c ON (a.city_id = c.city_id)";
		if($comma_separated !="")
        {
           $sql .= " WHERE area_id IN(".$comma_separated.")"; 
        }
      
		$query = $this->db->query($sql);

		return $query->rows;
	}
}