<?php
class ModelLocalisationCity extends Model {
	public function getCity($city_id) {
		$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "city WHERE city_id = '" . (int)$city_id . "' AND status = '1'");

		return $query->row;
	}

	public function getCities() {
		$city_data = $this->cache->get('city.status');

		if (!$city_data) {
			$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "city WHERE status = '1' ORDER BY name ASC");

			$city_data = $query->rows;

			$this->cache->set('city.status', $city_data);
		}

		return $city_data;
	}
    
    public function getStoreCities($storeId) {	

			$query = $this->db->query("SELECT value FROM " . DB_PREFIX . "setting s WHERE s.store_id=".(int)$storeId." AND s.key ='config_area'");
          	$city_result = $query->row;
              if(!empty($city_result) && !empty($city_result['value']))
              {
                 $city_data = json_decode($city_result['value']);
                           
              }			

		return $city_data;
	}
}