<!DOCTYPE html>
<!--[if IE]><![endif]-->
<!--[if IE 8 ]><html dir="<?php echo $direction; ?>" lang="<?php echo $lang; ?>" class="ie8"><![endif]-->
<!--[if IE 9 ]><html dir="<?php echo $direction; ?>" lang="<?php echo $lang; ?>" class="ie9"><![endif]-->
<!--[if (gt IE 9)|!(IE)]><!-->
<html dir="<?php echo $direction; ?>" lang="<?php echo $lang; ?>">
<!--<![endif]-->
<head>
<meta charset="UTF-8" />
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<title><?php echo $title; ?></title>
<base href="<?php echo $base; ?>" />
<?php if ($description) { ?>
<meta name="description" content="<?php echo $description; ?>" />
<?php } ?>
<?php if ($keywords) { ?>
<meta name="keywords" content= "<?php echo $keywords; ?>" />
<?php } ?>
<script src="catalog/view/javascript/jquery/jquery-2.1.1.min.js" type="text/javascript"></script>
<link href="catalog/view/javascript/bootstrap/css/bootstrap.min.css" rel="stylesheet" media="screen" />
<script src="catalog/view/javascript/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
<link href="catalog/view/javascript/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
<link href="//fonts.googleapis.com/css?family=Open+Sans:400,400i,300,700" rel="stylesheet" type="text/css" />
<link href="catalog/view/theme/default/stylesheet/stylesheet.css" rel="stylesheet">
<?php foreach ($styles as $style) { ?>
<link href="<?php echo $style['href']; ?>" type="text/css" rel="<?php echo $style['rel']; ?>" media="<?php echo $style['media']; ?>" />
<?php } ?>
<script src="catalog/view/javascript/common.js" type="text/javascript"></script>
<?php foreach ($links as $link) { ?>
<link href="<?php echo $link['href']; ?>" rel="<?php echo $link['rel']; ?>" />
<?php } ?>
<?php foreach ($scripts as $script) { ?>
<script src="<?php echo $script; ?>" type="text/javascript"></script>
<?php } ?>
<?php foreach ($analytics as $analytic) { ?>
<?php echo $analytic; ?>
<?php } ?>

<script>

$(document).ready(function() {
// Override summernotes image manager
	$('button[data-event=\'showImageDialog\']').attr('data-toggle', 'image').removeAttr('data-event');
	
	$(document).delegate('button[data-toggle=\'image\']', 'click', function() {
		$('#modal-image').remove();
		
		$(this).parents('.note-editor').find('.note-editable').focus();
				
		$.ajax({
			url: 'index.php?route=common/filemanager',
			dataType: 'html',
			beforeSend: function() {
				$('#button-image i').replaceWith('<i class="fa fa-circle-o-notch fa-spin"></i>');
				$('#button-image').prop('disabled', true);
			},
			complete: function() {
				$('#button-image i').replaceWith('<i class="fa fa-upload"></i>');
				$('#button-image').prop('disabled', false);
			},
			success: function(html) {
				$('body').append('<div id="modal-image" class="modal">' + html + '</div>');
	
				$('#modal-image').modal('show');
			}
		});	
	});
	
	// Image Manager
	$(document).delegate('a[data-toggle=\'image\']', 'click', function(e) {
		e.preventDefault();
		
		$('.popover').popover('hide', function() {
			$('.popover').remove();
		});
					
		var element = this;
		
		$(element).popover({
			html: true,
			placement: 'right',
			trigger: 'manual',
			content: function() {
				return '<button type="button" id="button-image" class="btn btn-primary"><i class="fa fa-pencil"></i></button> <button type="button" id="button-clear" style="display:none" class="btn btn-danger"><i class="fa fa-trash-o"></i></button>';
			}
		});
		
		$(element).popover('show');

		$('#button-image').on('click', function() {
			$('#modal-image').remove();
		
			$.ajax({
				url: 'index.php?route=common/filemanager&token=&target=' + $(element).parent().find('input').attr('id') + '&thumb=' + $(element).attr('id'),
				dataType: 'html',
				beforeSend: function() {
					$('#button-image i').replaceWith('<i class="fa fa-circle-o-notch fa-spin"></i>');
					$('#button-image').prop('disabled', true);
				},
				complete: function() {
					$('#button-image i').replaceWith('<i class="fa fa-pencil"></i>');
					$('#button-image').prop('disabled', false);
				},
				success: function(html) {
					$('body').append('<div id="modal-image" class="modal">' + html + '</div>');
		
					$('#modal-image').modal('show');
				}
			});
			
			$(element).popover('hide', function() {
				$('.popover').remove();
			});
		});		
		
		$('#button-clear').on('click', function() {
			$(element).find('img').attr('src', $(element).find('img').attr('data-placeholder'));
			
			$(element).parent().find('input').attr('value', '');
			
			$(element).popover('hide', function() {
				$('.popover').remove();
			});
		});
	});
	
	// tooltips on hover
	$('[data-toggle=\'tooltip\']').tooltip({container: 'body', html: true});

	// Makes tooltips work on ajax generated content
	$(document).ajaxStop(function() {
		$('[data-toggle=\'tooltip\']').tooltip({container: 'body'});
	});
	
	// https://github.com/opencart/opencart/issues/2595
	$.event.special.remove = {
		remove: function(o) {
			if (o.handler) { 
				o.handler.apply(this, arguments);
			}
		}
	}
	
	$('[data-toggle=\'tooltip\']').on('remove', function() {
		$(this).tooltip('destroy');
	});	
});

</script>

</head>
<body class="<?php echo $class; ?>">
<nav id="top">
  <div class="container">
   <!-- <?php echo $currency; ?>-->
    <?php echo $language; ?>
    <div id="top-links" class="nav pull-right">
     
    </div>
  </div>
</nav>
<header>
  <div class="container">
    <div class="row">
      <div class="col-sm-6">
        <div id="logo">
          <?php if ($logo) { ?>
          <a href="<?php echo $home; ?>"><img src="<?php echo $logo; ?>" title="<?php echo $name; ?>" alt="<?php echo $name; ?>" class="img-responsive" /></a>
          <?php } else { ?>
          <h1><a href="<?php echo $home; ?>"><?php echo $name; ?></a></h1>
          <?php } ?>
        </div>
      </div>
     <div class="col-sm-6">
	 
	 <?php if ($logged) { ?>
	<div class="col-sm-12" style="margin-top:40px;text-align:right;">
		<span style="display:block; font-weight:bold;font-size:15px;color:red;ont-style:italic;">
		<?php echo $text_welcome; ?><a href="index.php?route=saccount/account">
		<?php echo $username; ?></a> <b>(</b> <a href="index.php?route=saccount/logout"><?php echo $text_logout; ?></a> <b>)</b></span>
	</div>
	<?php } ?>
	
	
      </div>
     <!-- <div class="col-sm-3"><?php echo $cart; ?></div>-->
    </div>
  </div>
</header>
<?php if ($logged) { ?>
<div class="container">
  <nav id="menu" class="navbar">
    <div class="navbar-header"><span id="category" class="visible-xs"><?php echo $text_category; ?></span>
      <button type="button" class="btn btn-navbar navbar-toggle" data-toggle="collapse" data-target=".navbar-ex1-collapse"><i class="fa fa-bars"></i></button>
    </div>
    <div class="collapse navbar-collapse navbar-ex1-collapse">
               <ul class="nav navbar-nav">
	  <li><a href="<?php echo $account; ?>"><?php echo $text_home; ?></a></li>
            <li class="dropdown"><a href="<?php echo $account; ?>" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">My Store<span class="caret"></span></a>
				<ul class="dropdown-menu">

					<li><a href="index.php?route=saccount/extension"><?php echo $text_manageextensions; ?></a></li>
					<li><a href="index.php?route=saccount/extension/insert"><?php echo $text_addextensions; ?></a></li>
					<li><a href="index.php?route=saccount/offer"><?php echo $text_offer;?></a></li>
					<li><a href="index.php?route=saccount/attribute"><?php echo $text_attributes;?></a></li>
					<li><a href="index.php?route=saccount/option"><?php echo $text_option;?></a></li>
					
					<li><a href="index.php?route=saccount/download"><?php echo $text_download;?></a></li>
					
					<li><a href="index.php?route=saccount/uploadimages"><?php echo $text_images;?></a></li>
					<li><a href="index.php?route=saccount/smartexportimport"><?php echo $text_export;?></a></li>
					
     				</ul>
		</li>
		<li class="dropdown"><a href="<?php echo $account; ?>" class="dropdown-toggle" 
		data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"><?php echo $text_my_account;?><span class="caret"></span></a>
			<ul class="dropdown-menu">
				<li><a href="index.php?route=saccount/edit"><?php echo $text_edit;?></a></li>
				<li><a href="index.php?route=saccount/password"><?php echo $text_password;?></a></li>
				<li><a href="index.php?route=saccount/address"><?php echo $text_address;?></a></li>
				<li><a href="index.php?route=saccount/address2"><?php echo $text_getpaid;?></a></li>
			</ul>
		</li>
		<li class="dropdown"><a href="<?php echo $account; ?>" class="dropdown-toggle" data-toggle="dropdown"
		role="button" aria-haspopup="true" aria-expanded="false"><?php echo $text_my_order;?><span class="caret"></span></a>
			<ul class="dropdown-menu">
				<li><a href="index.php?route=saccount/order"><?php echo $text_order;?></a></li>
				<li><a href="index.php?route=saccount/transaction"><?php echo $text_transaction;?></a></li>
			</ul>
		</li>
           
          </ul>
         
        
    </div>
  </nav>
</div>
<?php } ?>
