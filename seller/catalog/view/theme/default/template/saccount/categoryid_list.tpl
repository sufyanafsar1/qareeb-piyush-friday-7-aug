<?php echo $header; ?>
<?php echo $column_left; ?>
<script type="text/javascript" src="catalog/view/javascript/jquery/ajaxupload.js"></script>
<style>
.page-header {
    border-bottom: 0px solid #eee;
    margin: 40px 0 20px;
    padding-bottom: 9px;
}
</style>
<div class="container">
	<div class="row">
		<div id="content">
			<div class="page-header">
				<div class="container-fluid">
					<h1><?php echo $heading_title; ?></h1>
				</div>
			</div>
			<div class="container-fluid">
				<?php if ($error_warning) { ?>
					<div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> <?php echo $error_warning; ?>
						<button type="button" class="close" data-dismiss="alert">&times;</button>
					</div>
				<?php } ?>
				<?php if ($success) { ?>
					<div class="alert alert-success"><i class="fa fa-check-circle"></i> <?php echo $success; ?>
						<button type="button" class="close" data-dismiss="alert">&times;</button>
					</div>
				<?php } ?>
				<div class="panel panel-default">
					<div class="panel-heading">
						<h3 class="panel-title"><i class="fa fa-list"></i> <?php echo $heading_title; ?></h3>
					</div>
					<div class="panel-body">
						<form action="<?php echo $delete; ?>" method="post" enctype="multipart/form-data" id="form-category">
							<div class="table-responsive">
								<table class="table table-bordered table-hover">
									<thead>
										<tr>
											<td class="text-center">
												<?php if ($sort == 'name') { ?>
													<a href="<?php echo $sort_name; ?>" class="<?php echo strtolower($order); ?>"><?php echo $column_name; ?></a>
												<?php } else { ?>
													<a href="<?php echo $sort_name; ?>"><?php echo $column_name; ?></a>
												<?php } ?>
											</td>
											<td class="text-center">
												<?php if ($sort == 'id') { ?>
													<a href="<?php echo $sort_id; ?>" class="<?php echo strtolower($order); ?>"><?php echo $column_id; ?></a>
												<?php } else { ?>
													<a href="<?php echo $sort_id; ?>"><?php echo $column_id; ?></a>
												<?php } ?>
											</td>
										</tr>
									</thead>
									<tbody>
										<?php if ($categories) { ?>
											<?php foreach ($categories as $category) { ?>
												<tr>
													<td class="text-center"><?php echo $category['name']; ?></td>
													<td class="text-center"><?php echo $category['category_id']; ?></td>
												</tr>
											<?php } ?>
										<?php } else { ?>
											<tr>
												<td class="text-center" colspan="2"><?php echo $text_no_results; ?></td>
											</tr>
										<?php } ?>
									</tbody>
								</table>
							</div>
						</form>
						<div class="row">
							<div class="col-sm-6 text-left"><?php echo $pagination; ?></div>
							<div class="col-sm-6 text-right"><?php echo $results; ?></div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<?php echo $footer; ?>