<?php echo $header; ?>

<div class="container">

  <ul class="breadcrumb">

    <?php foreach ($breadcrumbs as $breadcrumb) { ?>

    <li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>

    <?php } ?>

  </ul>

  <div class="row"><?php echo $column_left; ?>

    <?php if ($column_left && $column_right) { ?>

    <?php $class = 'col-sm-6'; ?>

    <?php } elseif ($column_left || $column_right) { ?>

    <?php $class = 'col-sm-9'; ?>

    <?php } else { ?>

    <?php $class = 'col-sm-12'; ?>

    <?php } ?>

    <div id="content" class="<?php echo $class; ?>"><?php echo $content_top; ?>

      <h1><?php echo $heading_title; ?></h1>

     
      <div class="table-responsive">
	  
	  
	  <table class="form" style="width:100%">

        <tr>
		
		<td style="width:200px">Order ID:

            <input type="text" name="filter_order_id" value="<?php echo $filter_order_id; ?>" id="filter_order_id" class="form-control"/></td>

          <td >Date Start:

	  <div class="input-group date">
                          <input type="text" name="filter_date_start" value="<?php echo $filter_date_start; ?>" placeholder="Date Start" data-date-format="YYYY-MM-DD" class="form-control" />
                          <span class="input-group-btn">
                          <button class="btn btn-default" type="button"><i class="fa fa-calendar"></i></button>
                          </span></div>

	</td>


          
          <td >Date End:

              <div class="input-group date">
                          <input type="text" name="filter_date_end" value="<?php echo $filter_date_end; ?>" 
			  placeholder="Date End" data-date-format="YYYY-MM-DD" class="form-control" />
                          <span class="input-group-btn">
                          <button class="btn btn-default" type="button"><i class="fa fa-calendar"></i></button>
                          </span></div>
			  </td>

         
         

          <td style="text-align: right;"><a onclick="filter();" class="btn btn-primary">Filter</a></td>

        </tr>

      </table>

	  
	  <br/>

	  <?php if ($orders) { ?>


        <table class="table table-bordered table-hover">

          <thead>

            <tr>

              <td class="text-right"><?php echo $column_order_id; ?></td>

              <td class="text-left"><?php echo $column_status; ?></td>

              <td class="text-left"><?php echo $column_date_added; ?></td>

              <td class="text-right"><?php echo $column_product; ?></td>

              <td class="text-left"><?php echo $column_customer; ?></td>

              <td class="text-right"><?php echo $column_total; ?></td>

              <td></td>

            </tr>

          </thead>

          <tbody>

            <?php foreach ($orders as $order) { ?>

            <tr>

              <td class="text-right">#<?php echo $order['order_id']; ?></td>

              <td class="text-left"><?php echo $order['status']; ?></td>

              <td class="text-left"><?php echo $order['date_added']; ?></td>

              <td class="text-right"><?php echo $order['products']; ?></td>

              <td class="text-left"><?php echo $order['name']; ?></td>

              <td class="text-right"><?php echo $order['total']; ?></td>

              <td class="text-right"><a href="<?php echo $order['href']; ?>" data-toggle="tooltip" title="<?php echo $button_view; ?>" 
			  class="btn btn-info"><i class="fa fa-eye"></i></a>
			  
			  &nbsp;&nbsp;<a class="btn btn-info" target="_blank" href="<?php echo $order['invoice']; ?>" title="Print Invoice"
			  ><i class="fa fa-print"></i></a>
			  
			  
		
			  </td>

            </tr>

            <?php } ?>

          </tbody>

        </table>

      </div>

      <div class="text-right"><?php echo $pagination; ?></div>

	 
 <script type="text/javascript"><!--
$('input[name^=\'selected\']').on('change', function() {
	$('#button-shipping, #button-invoice').prop('disabled', true);
	
	var selected = $('input[name^=\'selected\']:checked');
	
	if (selected.length) {
		$('#button-invoice').prop('disabled', false);
	}
	
	for (i = 0; i < selected.length; i++) {
		if ($(selected[i]).parent().find('input[name^=\'shipping_code\']').val()) {
			$('#button-shipping').prop('disabled', false);
			
			break;
		}
	}
});

$('input[name^=\'selected\']:first').trigger('change');

$('a[id^=\'button-delete\']').on('click', function(e) {
	e.preventDefault();
	
	if (confirm('<?php echo $text_confirm; ?>')) {
		location = $(this).attr('href');
	}
});
//--></script> 



      <?php } else { ?>

      <p><?php echo $text_empty; ?></p>

      <?php } ?>

      <div class="buttons clearfix">

        <div class="pull-right"><a href="<?php echo $continue; ?>" class="btn btn-primary"><?php echo $button_continue; ?></a></div>

      </div>

      <?php echo $content_bottom; ?></div>

    <?php echo $column_right; ?></div>

</div>

<script src="catalog/view/javascript/jquery/datetimepicker/moment.js" type="text/javascript"></script>
<script src="catalog/view/javascript/jquery/datetimepicker/bootstrap-datetimepicker.min.js" type="text/javascript"></script>
<link href="catalog/view/javascript/jquery/datetimepicker/bootstrap-datetimepicker.min.css" type="text/css" rel="stylesheet" media="screen" />

<script type="text/javascript"><!--
$('.date').datetimepicker({
	pickTime: false
});

$('.time').datetimepicker({
	pickDate: false
});

$('.datetime').datetimepicker({
	pickDate: true,
	pickTime: true
});
//--></script> 

 
	  <script type="text/javascript"><!--

function filter() {

	url = 'index.php?route=saccount/order';

	
	

	var filter_date_start = $('input[name=\'filter_date_start\']').val();

	

	if (filter_date_start) {

		url += '&filter_date_start=' + encodeURIComponent(filter_date_start);

	}



	var filter_date_end = $('input[name=\'filter_date_end\']').val();

	

	if (filter_date_end) {

		url += '&filter_date_end=' + encodeURIComponent(filter_date_end);

	}

		

	

	var filter_order_id = $('input[name=\'filter_order_id\']').val();

	

	if (filter_order_id) {

		url += '&filter_order_id=' + encodeURIComponent(filter_order_id);

	}	



	location = url;

}

//--></script> 


<?php echo $footer; ?>