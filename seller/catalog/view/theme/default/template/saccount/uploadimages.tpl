<?php echo $header; ?><?php echo $column_left; ?>
<link href="catalog/view/javascript/uploadimage/uploadfile.css" rel="stylesheet">
<script src="//ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>
<script src="catalog/view/javascript/uploadimage/jquery.uploadfile.min.js"></script>

<script type="text/javascript"><!--
$(document).ready(function()
{
var settings = {
    url: "index.php?route=saccount/uploadimages/upload&parent=<?php echo $parent; ?>",
    dragDrop:true,
    fileName: "myfile",
    allowedTypes:"jpg,png,gif,doc,pdf,zip",	
    returnType:"json",
	 onSuccess:function(files,data,xhr)
    {
        //alert(data[0]);
    },
    showDelete:true,
    deleteCallback: function(data,pd)
	{
    for(var i=0;i<data.length;i++)
    {
        $.post("delete.php",{op:"delete",name:data[i]},
        function(resp, textStatus, jqXHR)
        {
            //Show Message  
            $("#status").append("<div>File Deleted</div>");      
        });
     }      
    pd.statusbar.hide(); //You choice to hide/not.

}
}
var uploadObj = $("#mulitplefileuploader").uploadFile(settings);

});
//--></script> 

<div class="container">
<div class="row">
<div id="content">
  <div class="page-header">
    <div class="container-fluid">
     
      <h1><?php echo $heading_title; ?></h1>
      <ul class="breadcrumb">
        <?php foreach ($breadcrumbs as $breadcrumb) { ?>
        <li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
        <?php } ?>
      </ul>
    </div>
  </div>
  <div class="container-fluid">
    <?php if ($error_warning) { ?>
    <div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> <?php echo $error_warning; ?>
      <button type="button" class="close" data-dismiss="alert">&times;</button>
    </div>
    <?php } ?>
    
    <div class="panel panel-default">
      <div class="panel-heading">
        <h3 class="panel-title"><i class="fa fa-list"></i> <?php echo $heading_title; ?></h3>
      </div>
      <div class="panel-body">
      
        <div id="tab-general">
          <div id="mulitplefileuploader"><?php echo $text_upload; ?></div>
				<div id="status"></div>
          <a href="<?php echo $imageshref;?>" target="_blank" class="btn btn-primary" style="float:right"><?php echo $text_view; ?>
	  </a>
        </div>
		
		<div class="buttons">
		<div class="pull-left"><a href="<?php echo $back; ?>" class="btn btn-primary"><?php echo $button_back; ?></a></div>
		
		</div>
		
      </div>
    </div>
  </div>
</div>
</div>
</div>
<?php echo $footer; ?>