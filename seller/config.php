<?php
// HTTP
define('HTTP_SERVER', 'http://dev.algokraze.com/seller/');

// HTTPS
define('HTTPS_SERVER', 'http://dev.algokraze.com/seller/');

define('HTTP_SERVER1', 'http://dev.algokraze.com/');

define('HTTPS_SERVER1', 'http://dev.algokraze.com/');

// DIR
define('DIR_APPLICATION', '/home/centos/app/upload/seller/catalog/');
define('DIR_SYSTEM', '/home/centos/app/upload/seller/system/');
define('DIR_IMAGE', '/home/centos/app/upload/image/');
define('DIR_LANGUAGE', '/home/centos/app/upload/seller/catalog/language/');
define('DIR_TEMPLATE', '/home/centos/app/upload/seller/catalog/view/theme/');
define('DIR_CONFIG', '/home/centos/app/upload/seller/system/config/');
define('DIR_CACHE', '/home/centos/app/upload/seller/system/storage/cache/');
define('DIR_DOWNLOAD', '/home/centos/app/upload/seller/system/storage/download/');
define('DIR_LOGS', '/home/centos/app/upload/seller/system/storage/logs/');
define('DIR_MODIFICATION', '/home/centos/app/upload/seller/system/storage/modification/');
define('DIR_UPLOAD', '/home/centos/app/upload/seller/system/storage/upload/');

// DB
define('DB_DRIVER', 'mysqli');
define('DB_HOSTNAME', 'localhost');
define('DB_USERNAME', 'root');
define('DB_PASSWORD', 'ZAQ!2wsx');
define('DB_DATABASE', 'qareeb_app');
define('DB_PORT', '3306');
define('DB_PREFIX', 'oc_');
