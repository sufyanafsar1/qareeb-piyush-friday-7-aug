if ('serviceWorker' in navigator) {
    window.addEventListener('load', () => {
        navigator.serviceWorker.register('cache-inner.js').then(reg => console.log('Service Worker: Registered (Pages)')).catch(err => console.log(`Service Worker:Error:${err}`));
    });
}