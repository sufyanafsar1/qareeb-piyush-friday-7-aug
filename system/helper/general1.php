<?php
function token($length = 32) {
	// Create token to login with
	$string = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
	
	$token = '';
	
	for ($i = 0; $i < $length; $i++) {
		$token .= $string[mt_rand(0, strlen($string) - 1)];
	}	
	
	return $token;
}

// Create Random Password
function createPassword($length =6) {
	
//	$string = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
	$string = '0123456789';
	$token = '';
	
	for ($i = 0; $i < $length; $i++) {
		$token .= $string[mt_rand(0, strlen($string) - 1)];
	}	
	
	return $token;
}

// Capatilize first letter of every word in string
function properName($name)
{
    $properName = ucwords(strtolower($name));
    return $properName;
}

function getItemName($itemName,$itemLength)
{
    $language = $_SESSION['default']['language'];
    $name ="";                                    
    if($language =='ar')
    {
        $name =  properName($itemName);
    } else{
        $shortname = substr(properName($itemName),0,$itemLength);
       $name =   wordwrap($shortname,20,"<br>\n",TRUE); 
    } 
 
    return $name;
}

function sms__unicode($message) {
  if (function_exists('iconv')) {
    $latin = @iconv('UTF-8', 'ISO-8859-1', $message);
    if (strcmp($latin, $message)) {
      $arr = unpack('H*hex', @iconv('UTF-8', 'UCS-2BE', $message));
      return strtoupper($arr['hex']) .'&unicode=u';
    }
  }
  return FALSE;
}

function sendSMS($telephone,$msg){
    $language = $_SESSION['default']['language'];
    $sender         =   'Qareeb';
    $telephone = str_replace(" ", "", $telephone);
    $telephone = str_replace("+", "", $telephone);
    // $apiUserName    =   'fltsysinc';
    // $apiPassword    =   'fssi2609';
    $apiUserName    =   'qareeb';
    $apiPassword    =   'M@dina786';
    http://www.jawalbsms.ws/api.php/sendsms?user=XXXX&pass=XXXX&to=966XXXXXXXXX&message=SENDING-TEST&sender=TEST 
    $ch = curl_init('http://www.jawalbsms.ws/api.php/sendsms?user='.$apiUserName.'&pass='.$apiPassword);
//   $ch = curl_init('http://www.smszing.com/HttpBox/sendsms?');
  curl_setopt($ch, CURLOPT_POST, 1);

  error_log("sending sms: http://www.jawalbsms.ws/api.php/sendsms?user=".$apiUserName."&pass=".$apiPassword."&sender=".$sender."&to=".$telephone."&message=".sms__unicode($msg), 3, '/home/centos/app/upload/system/storage/logs/sms.log'); 
  if($language =='ar')
    {
		curl_setopt($ch, CURLOPT_POSTFIELDS,"&sender=".$sender."&to=".$telephone."&message=".sms__unicode($msg));
	} else{
		curl_setopt($ch, CURLOPT_POSTFIELDS,"&sender=".$sender."&to=".$telephone."&message=".$msg);
	}
  curl_setopt($ch, CURLOPT_RETURNTRANSFER,1);
  error_log($msg, 3, '/home/centos/app/upload/system/storage/logs/sms.log'); 
  $data = curl_exec($ch);
  $explode = explode(":",$data);  //e.g OK:966553044161:c5fed567-35e2-4709-a9ca-8ecba6be20d0
  error_log($data, 3, '/home/centos/app/upload/system/storage/logs/sms.log'); 
  return $explode[0];
}

function tetSMS($mobile,$message)
{
    //$username = "923454546967";
    //$password = "7264"; 
    $username = "923334145044";
    $password = "9851";   
    $sender = "Mannan";   
    $url = "http://sendpk.com/api/sms.php?username=".$username."&password=".$password."&mobile=".$mobile."&sender=".urlencode($sender)."&message=".urlencode($message)."";
    
    $ch = curl_init();
    $timeout = 30;
    curl_setopt($ch,CURLOPT_URL,$url);
    curl_setopt($ch,CURLOPT_RETURNTRANSFER,1);
    curl_setopt($ch,CURLOPT_CONNECTTIMEOUT,$timeout);
    $responce = curl_exec($ch);
    curl_close($ch);
    
  
  $explode1 = explode(":",$responce);  //e.g OK ID:6502124 get part before : and result will be OK ID    
  $explode2 = explode(" ",$explode1[0]); //e.g OK ID get part before space it will return OK  
  $status = $explode2[0];  
  return $status;
}
function get_browser_language( $available = [], $default = 'en' ) {
	if ( isset( $_SERVER[ 'HTTP_ACCEPT_LANGUAGE' ] ) ) {
		$langs = explode( ',', $_SERVER['HTTP_ACCEPT_LANGUAGE'] );
    if ( empty( $available ) ) {
      return $langs[ 0 ];
    }
		foreach ( $langs as $lang ){
			$lang = substr( $lang, 0, 2 );
			if( in_array( $lang, $available ) ) {
				return $lang;
			}
		}
	}
	return $default;
}
function strToDateTime($strDate){
    if(!empty($strDate)){
        return date('Y-m-d H:i:s', strtotime($strDate));
    }
   else{
    return NULL;
   }
    
}

function getZone($zoneId)
{
    $zoneArray =  array();
    if($zoneId == 1) //customerGroup   Riyadh
    {
      $zoneArray =  array("customer"=>1, "zoneId"=>2879, "zoneArea"=>"Riyadh"); 
    }
    else if($zoneId == 2) //customerGroup  Dammam 
    {
        $zoneArray =  array("customer"=>1, "zoneId"=>4226, "zoneArea"=>"Dammam"); 
    }
    else if($zoneId == 3) //customerGroup  Jeddah 
    {
        
        $zoneArray =  array("customer"=>1, "zoneId"=>4224, "zoneArea"=>"Jeddah");
    }
    else if($zoneId == 5) //customerGroup  Other-Cities  
    {        
        $zoneArray =  array("customer"=>1, "zoneId"=>4230, "zoneArea"=>"Other-Cities");
    }
    else{       
        $zoneArray =  array("customer"=>0, "zoneId"=>$zoneId);
    }
    
    return $zoneArray;
}

function getZoneId($zoneId)
{
    
    if($zoneId == 1) //customerGroup   Riyadh
    {
      return 2879; 
    }
    else if($zoneId == 2) //customerGroup  Dammam 
    {
      
        return 4226; 
    }
    else if($zoneId == 3) //customerGroup  Jeddah 
    {
        return 4224;
       
    }
    else if($zoneId == 5) //customerGroup  Other-Cities  
    {        
        
         return 4230;
    }
    else{       
        
         return $zoneId;
    }
    
    
}

function setCareebCookie($cookieName,$cookieValue,$isArray){
    
    if($isArray){
        setcookie($cookieName, json_encode($cookieValue), time() + (86400 * 30), "/"); // 86400 = 1 day
    }else{
        setcookie($cookieName, $cookieValue, time() + (86400 * 30), "/"); // 86400 = 1 day    
        }
    
    
}

function unsetCareebCookie($cookieName){
  setcookie($cookieName, '', time() - 86400,"/");
  unset($_COOKIE[$cookieName]);
}

function debugging($array)
{
   
    echo"<pre>";
    print_r($array);
    die("<br/> Debugging END");
}
