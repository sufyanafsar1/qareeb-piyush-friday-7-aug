<?php
function seller($seller_id) {
    global $loader, $registry;
    $loader->model('catalog/seller');
    $model = $registry->get('model_catalog_seller');
    $result = $model->getSeller($seller_id);
    if ( count( $result ) > 0 ) {
        return $result;
    } else {
        return false;
    }
}

function dayData($day) {
    $date = array(
        "full" => date("l", strtotime($day)),
        "half" => date("D", strtotime($day)),
        "time" => date("h:i a", strtotime($day)),
        "short_time" => date("G:i:s", strtotime($day)),
        "strtime" => strtotime($day),
        "date" => date("Y-m-d", strtotime($day))
    );
    return $date;
}