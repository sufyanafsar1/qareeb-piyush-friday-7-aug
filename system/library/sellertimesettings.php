<?php

class sellertimesettings {

    function something() {
        $string = 'Hello World';
        return $string;
    }

    public function __construct($registry) {
        $this->config = $registry->get('config');
        $this->db = $registry->get('db');
        $this->request = $registry->get('request');
        $this->session = $registry->get('session');
    }

    public function getTimebhutta($seller_id) {
        $query = $this->db->query("SELECT * FROM " . DB_PREFIX . "sellers WHERE seller_id = '" . (int) $seller_id . "'");

        $sellerData = $query->row;

        if ((date("l") == 'Monday') && $sellerData['mon_end_time'] > date("h:i:sa")) {
            $storeTimeStatusText = 'Same day delivery.';
            $storeTimeStatus = 1;
        } else if ((date("l") == 'Monday') && $sellerData['mon_end_time'] < date("h:i:sa")) {
            $storeTimeStatusText = 'Opps Store Closed! Delivery will be on next working day.';
            $storeTimeStatus = 0;
        }

        if ((date("l") == 'Tuesday') && $sellerData['tue_end_time'] > date("h:i:sa")) {
            $storeTimeStatusText = 'Same day delivery.';
            $storeTimeStatus = 1;
        } else if ((date("l") == 'Tuesday') && $sellerData['tue_end_time'] < date("h:i:sa")) {
            $storeTimeStatusText = 'Opps Store Closed! Delivery will be on next working day.';
            $storeTimeStatus = 0;
        }

        if ((date("l") == 'Wednesday') && $sellerData['wed_end_time'] > date("h:i:sa")) {
            $storeTimeStatusText = 'Same day delivery.';
            $storeTimeStatus = 1;
        } else if ((date("l") == 'Wednesday') && $sellerData['wed_end_time'] < date("h:i:sa")) {
            $storeTimeStatusText = 'Opps Store Closed! Delivery will be on next working day.';
            $storeTimeStatus = 0;
        }

        if ((date("l") == 'Thursday') && $sellerData['thu_end_time'] > date("h:i:sa")) {
            $storeTimeStatusText = 'Same day delivery.';
            $storeTimeStatus = 1;
        } else if ((date("l") == 'Thursday') && $sellerData['thu_end_time'] < date("h:i:sa")) {
            $storeTimeStatusText = 'Opps Store Closed! Delivery will be on next working day.';
            $storeTimeStatus = 0;
        }

        if ((date("l") == 'Friday') && $sellerData['fri_end_time'] > date("h:i:sa")) {
            $storeTimeStatusText = 'Same day delivery.';
            $storeTimeStatus = 1;
        } else if ((date("l") == 'Friday') && $sellerData['fri_end_time'] < date("h:i:sa")) {
            $storeTimeStatusText = 'Opps Store Closed! Delivery will be on next working day.';
            $storeTimeStatus = 0;
        }

        if ((date("l") == 'Saturday') && $sellerData['sat_end_time'] > date("h:i:sa")) {
            $storeTimeStatusText = 'Same day delivery.';
            $storeTimeStatus = 1;
        } else if ((date("l") == 'Saturday') && $sellerData['sat_end_time'] < date("h:i:sa")) {
            $storeTimeStatusText = 'Opps Store Closed! Delivery will be on next working day.';
            $storeTimeStatus = 0;
        }

        if ((date("l") == 'Sunday') && $sellerData['sun_end_time'] > date("h:i:sa")) {
            $storeTimeStatusText = 'Same day delivery.';
            $storeTimeStatus = 1;
        } else if ((date("l") == 'Sunday') && $sellerData['sun_end_time'] < date("h:i:sa")) {
            $storeTimeStatusText = 'Opps Store Closed! Delivery will be on next working day.';
            $storeTimeStatus = 0;
        }

        $storeTimeResult['storeTimeStatusText'] = $storeTimeStatusText;
        $storeTimeResult['storeTimeStatus'] = $storeTimeStatus;
        return $storeTimeResult;
    }

}

?>