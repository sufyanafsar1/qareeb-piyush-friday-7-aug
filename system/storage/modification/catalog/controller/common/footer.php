<?php

class ControllerCommonFooter extends Controller {

    public function index() {

        $this->load->model('localisation/area');
		
		if(isset($this->session->data['area'])){
			$aread = $this->model_localisation_area->getAreasByIds($this->session->data['area']);
			$data['locationData'] = $aread;
		} else {
			$data['locationData'] = '';
		}

		if(isset($this->session->data['area']['area_id']) && ($this->session->data['area']['area_id']>0)){
			$data['SelectedAreaID'] = $this->session->data['area']['area_id'];
		}else{
			$data['SelectedAreaID'] = 0;
		}

		if(isset($this->session->data['area']['city_id']) && ($this->session->data['area']['city_id']>0)){
			$data['SelectedCityID'] = $this->session->data['area']['city_id'];
		}else{
			$data['SelectedCityID'] = 0;
		}

        $this->load->language('common/footer');

        $data['scripts'] = $this->document->getScripts('footer');

        $data['text_information'] = $this->language->get('text_information');
        $data['text_service'] = $this->language->get('text_service');
        $data['text_extra'] = $this->language->get('text_extra');

        $data['text_sitemap'] = $this->language->get('text_sitemap');
        $data['text_manufacturer'] = $this->language->get('text_manufacturer');
        $data['text_voucher'] = $this->language->get('text_voucher');
        $data['text_affiliate'] = $this->language->get('text_affiliate');
        $data['text_special'] = $this->language->get('text_special');
        $data['text_account'] = $this->language->get('text_account');
        $data['text_order'] = $this->language->get('text_order');
        $data['text_wishlist'] = $this->language->get('text_wishlist');
        $data['text_newsletter'] = $this->language->get('text_newsletter');
        $data['text_sell-with-us'] = $this->language->get('sell-with-us');
		$data['text_location_disable'] = $this->language->get('text_location_disable');
		
		$data['text_join_us'] = $this->language->get('text_join_us');
		$data['text_become_a_shopper'] = $this->language->get('text_become_a_shopper');
		$data['text_sell_with_us'] = $this->language->get('text_sell_with_us');
		$data['text_careers'] = $this->language->get('text_careers');
		
		$data['text_technologies'] = $this->language->get('text_technologies');
		$data['text_download_app'] = $this->language->get('text_download_app');
		$data['text_stay_connected'] = $this->language->get('text_stay_connected');

        $data['text_current_city'] = $this->language->get('text_current_city');
        $data['text_current_area'] = $this->language->get('text_current_area');




        $data['text_shop'] = $this->language->get('text_shop');
        $data['text_about'] = $this->language->get('text_about');
        $data['text_careers'] = $this->language->get('text_careers');
        $data['text_contact'] = $this->language->get('text_contact');

        $data['text_press'] = $this->language->get('text_press');
        $data['text_faq'] = $this->language->get('text_faq');
        $data['text_terms'] = $this->language->get('text_terms');
        $data['text_return'] = $this->language->get('text_return');
        $data['txt_contact_us'] = $this->language->get('txt_contact_us');

        $data['text_register_store'] = $this->language->get('text_register_store');
        $data['text_register_why'] = $this->language->get('text_register_why');
        $data['text_register_merchant'] = $this->language->get('text_register_merchant');

        $this->load->model('catalog/information');

        $data['informations'] = array();

        foreach ($this->model_catalog_information->getInformations() as $result) {
            if ($result['bottom']) {
                $data['informations'][] = array(
                    'title' => $result['title'],
                    'href' => $this->url->link('information/information', 'information_id=' . $result['information_id'])
                );
            }
        }



        $data['sitemap'] = $this->url->link('information/sitemap');
        $data['manufacturer'] = $this->url->link('product/manufacturer');
        $data['voucher'] = $this->url->link('account/voucher', '', 'SSL');
        $data['affiliate'] = $this->url->link('affiliate/account', '', 'SSL');
        $data['special'] = $this->url->link('product/special');
        $data['account'] = $this->url->link('account/account', '', 'SSL');
        $data['order'] = $this->url->link('account/order', '', 'SSL');
        $data['wishlist'] = $this->url->link('account/wishlist', '', 'SSL');
        $data['newsletter'] = $this->url->link('account/newsletter', '', 'SSL');

        $data['register_store'] = 'seller/index.php?route=saccount/register';  //$this->url->link('information/information', 'information_id=12');
        $data['register_why'] = $this->url->link('information/information', 'information_id=13', 'SSL');
        $data['register_merchant'] = 'seller';

        $data['shop'] = "#";
        $data['about'] = $data['informations'][0]['href'];
        $data['careers'] = "#";
        $data['contact'] = $this->url->link('information/contact');

        $data['press'] = $data['informations'][4]['href'];
        $data['faq'] = "#";
        $data['terms'] = $data['informations'][3]['href'];
        $data['return'] = $this->url->link('account/return/add', '', 'SSL');
        $data['signin'] = $this->load->controller('account/login');
        $this->load->model('account/wishlist');
        $data['wishlist_cat'] = $this->model_account_wishlist->getWishlistCategory();
        $data['likebox'] = html_entity_decode($this->config->get('likebox'));


        $data['btfooter'] = $this->load->controller('bossthemes/btfooter');

        $data['bulk_order'] = $this->url->link('account/newsletter', '', 'SSL');

		$data['powered'] = sprintf($this->language->get('text_powered'), date('Y', time()), $this->language->get('text_careeb'));
		
		if (!$this->customer->isLogged()) {	
			$data['forgotepopup'] = $this->load->controller('account/forgotpopup');
		}
		
        //$data['powered'] = sprintf($this->language->get('text_powered'), $this->config->get('config_name'), date('Y', time()));

        // Whos Online
        if ($this->config->get('config_customer_online')) {
            $this->load->model('tool/online');

            if (isset($this->request->server['REMOTE_ADDR'])) {
                $ip = $this->request->server['REMOTE_ADDR'];
            } else {
                $ip = '';
            }

            if (isset($this->request->server['HTTP_HOST']) && isset($this->request->server['REQUEST_URI'])) {
                $url = 'http://' . $this->request->server['HTTP_HOST'] . $this->request->server['REQUEST_URI'];
            } else {
                $url = '';
            }

            if (isset($this->request->server['HTTP_REFERER'])) {
                $referer = $this->request->server['HTTP_REFERER'];
            } else {
                $referer = '';
            }

            $this->model_tool_online->addOnline($ip, $this->customer->getId(), $url, $referer);
        }

        $this->load->language('common/store');
        $data['text_change_location'] = $this->language->get('text_change_location');

        $this->load->language('common/header_home');
        $data['text_city'] = $this->language->get('text_city');
        $data['text_region'] = $this->language->get('text_region');
        $data['text_area'] = $this->language->get('text_area');
        $data['text_find'] = $this->language->get('text_find');
        $data['text_locate_me'] = $this->language->get('text_locate_me');

        $data['action'] = $this->url->link('common/detail', '', 'SSL');
        $this->load->model('localisation/city');
        $languageId = (int) $this->config->get('config_language_id');
        $data['cities'] = $this->model_localisation_city->getCitiesByLanguage($languageId);
        $data['city_saved'] = 0;

		if(isset($_COOKIE['area'])){
            $areaData = json_decode($_COOKIE['area'], true);            
			$data['city_saved'] = $areaData['city_id'];    
		}

        $data['config_template'] = $this->config->get('config_template');

        if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/template/common/footer.tpl')) {

            return $this->load->view($this->config->get('config_template') . '/template/common/footer.tpl', $data);
        } else {
            return $this->load->view('default/template/common/footer.tpl', $data);
        }
    }

}
