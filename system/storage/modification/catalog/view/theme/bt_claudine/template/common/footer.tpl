<?php global $config;?>

<?php

	$boss_manager = array(

		'option' => array(

			'bt_scroll_top' => true,

			'animation' 	=> true,

		),

		'layout' => array(

			'mode_css'   => 'wide',

			'box_width' 	=> 1200,

			'h_mode_css'   => 'inherit',

			'h_box_width' 	=> 1200,

			'f_mode_css'   => 'inherit',

			'f_box_width' 	=> 1200

		),

		'status' => 1

	);

?>

<?php $footer_about = $config->get('boss_manager_footer_about'); ?>

<?php $footer_social = $config->get('boss_manager_footer_social'); ?>



<?php $footer_powered = $config->get('boss_manager_footer_powered'); ?>

<?php if($config->get('boss_manager')){

		$boss_manager = $config->get('boss_manager');

	}else{

		$boss_manager = $boss_manager;

	} ?>

<?php

	if(!empty($boss_manager)){

		$layout = isset($boss_manager['layout'])?$boss_manager['layout']:'';

		$footer_link = isset($boss_manager['footer_link'])?$boss_manager['footer_link']:'';

	}

	$f_style = '';

	if($layout['f_mode_css']=='fboxed'){

		$f_mode_css = 'bt-fboxed';

		$f_style = (!empty($layout['f_box_width']))?'style="max-width:'.$layout['f_box_width'].'px"':'';

	}else{

		$f_mode_css = '';

	}



?>

<div id="bt_footer_container" class="<?php echo $f_mode_css;?>" <?php echo $f_style;?>>



<footer id="bt_footer">

  <div class="container">

		      <?php echo $likebox; ?>


  <div class="row">

	<?php if(isset($footer_about['status']) && $footer_about['status']){ ?>

	<div class="footer-about">

		<?php if($footer_about['about_title'][$config->get('config_language_id')]) { ?>

			<h3><?php echo html_entity_decode($footer_about['about_title'][$config->get('config_language_id')],ENT_QUOTES, 'UTF-8'); ?></h3>

		<?php } ?>



		<?php echo html_entity_decode($footer_about['about_content'][$config->get('config_language_id')],ENT_QUOTES, 'UTF-8'); ?>

	</div>

	<?php } ?>

    <div class="row footer-social-subscribe footer_column">
        <div class="col-md-8">
             <?php if(isset($footer_social['status']) && $footer_social['status']){ ?>

			<div class="footer-social">

			<ul>

				<?php if(isset($footer_social['face_status']) && $footer_social['face_status']){;?><li class="facebook"><a target="_blank" href="<?php echo isset($footer_social['face_url'])?$footer_social['face_url']:'#'; ?>" data-original-title="Facebook" data-placement="top" data-toggle="tooltip"><i class="fa fa-facebook"></i></a></li><?php } ?>

				<?php if(isset($footer_social['twitter_status']) && $footer_social['twitter_status']){;?><li class="twitter"><a target="_blank" href="<?php echo isset($footer_social['twitter_url'])?$footer_social['twitter_url']:'#'; ?>" data-original-title="Twitter" data-placement="top" data-toggle="tooltip"><i class="fa fa-twitter"></i></a></li><?php } ?>

				<?php if(isset($footer_social['googleplus_status']) && $footer_social['googleplus_status']){;?><li class="google"><a target="_blank" href="<?php echo isset($footer_social['googleplus_url'])?$footer_social['googleplus_url']:'#'; ?>" data-original-title="Googleplus" data-placement="top" data-toggle="tooltip"><i class="fa fa-google-plus"></i></a></li><?php } ?>

				<?php if(isset($footer_social['pinterest_status']) && $footer_social['pinterest_status']){;?><li class="pinterest"><a target="_blank" href="<?php echo isset($footer_social['pinterest_url'])?$footer_social['pinterest_url']:'#'; ?>" data-original-title="Pinterest" data-placement="top" data-toggle="tooltip"><i class="fa fa-pinterest"></i></a></li><?php } ?>
                <?php if(isset($footer_social['youtube_status']) && $footer_social['youtube_status']){;?><li class="youtube"><a  target="_blank" href="<?php echo isset($footer_social['youtube_url'])?$footer_social['youtube_url']:'#'; ?>" data-original-title="Youtube" data-placement="top" data-toggle="tooltip"><i class="fa fa-youtube-play"></i></a></li><?php } ?>

				<?php if(isset($footer_social['instagram_status']) && $footer_social['instagram_status']){;?><li class="instagram"><a target="_blank" href="<?php echo isset($footer_social['instagram_url'])?$footer_social['instagram_url']:'#'; ?>" data-original-title="Instagram" data-placement="top" data-toggle="tooltip"><i class="fa fa-instagram"></i></a></li><?php } ?>

				<?php if(isset($footer_social['in_status']) && $footer_social['in_status']){;?><li class="linkedin"><a target="_blank" href="<?php echo isset($footer_social['in_url'])?$footer_social['in_url']:'#'; ?>" data-original-title="In" data-placement="top" data-toggle="tooltip"><i class="fa fa-linkedin"></i></a></li><?php } ?>

				<?php if(isset($footer_social['rss_status']) && $footer_social['rss_status']){;?><li class="rss"><a target="_blank" href="<?php echo isset($footer_social['rss_url'])?$footer_social['rss_url']:'#'; ?>" data-original-title="RSS" data-placement="top" data-toggle="tooltip"><i class="fa fa-rss"></i></a></li><?php } ?>

			</ul>

			</div><!-- footer-social -->

		<?php } ?>
        <div class="footer-menu">


  <div class="col-md-3 custom-footer-menu">



    <ul>

      <!--<li><a href="<?php echo $shop ?>"><?php echo $text_shop; ?></a></li>-->
      <li><a href="<?php echo $about ?>"><?php echo $text_about; ?></a></li>
      <li><a href="<?php echo $careers ?>"><?php echo $text_careers; ?></a></li>
      <li><a href="<?php echo $contact ?>"><?php echo $text_contact; ?></a></li>
        <li><a href="<?php echo $press ?>"><?php echo $text_press; ?></a></li>

    </ul>

  </div>


  <div class="col-md-3 custom-footer-menu">
     <ul>


      <li><a href="<?php echo $faq ?>"><?php echo $text_faq; ?></a></li>
      <li><a href="<?php echo $terms ?>"><?php echo $text_terms; ?></a></li>
      <li><a href="<?php echo $return ?>"><?php echo $text_return; ?></a></li>


    </ul>

  </div>


        </div><!-- footer-menu-->
        </div> <!-- Social Icons -->
         <div class="col-md-4">
            	<?php echo isset($btfooter)?$btfooter:''; ?>

                <div class="maroof-comodo">
               <!-- <a href="https://maroof.sa/11676" target="_blank" ><img src="image/icons/maroof-logo.png" height="64px" width="auto" alt="Mountain View"></a> -->

               <a href="https://sslanalyzer.comodoca.com/?url=careeb.com" target="_blank" ><img src="image/icons/comodo-secure-icon.png" height="64px" width="auto" alt="Mountain View"></a>

                </div>

         </div><!-- Subscribe -->
    </div> <!-- footer-social-subscribe -->


	</div>

  </div>


<div class="powered-payment">

    <div class="container">

		      <?php echo $likebox; ?>

	<div class="row">

		<div class="powered">

			<?php echo html_entity_decode(isset($footer_powered[$config->get('config_language_id')])?$footer_powered[$config->get('config_language_id')]:'',ENT_QUOTES, 'UTF-8'); ?>

		</div>

	</div>

    </div>

  </div>

</footer>

</div>
<?php echo $signin; ?>
<?php if(isset($boss_manager['option']['bt_scroll_top'])&&($boss_manager['option']['bt_scroll_top'])){ ?>

<div id="back_top" class="back_top"><span><i class="fa fa-caret-up"></i><b>Top</b></span></div>

 <script type="text/javascript">

        $(function(){

			$(window).scroll(function(){

				if($(this).scrollTop()>600){

				  $('#back_top').fadeIn();

				}

				else{

				  $('#back_top').fadeOut();

				}

			});

			$("#back_top").click(function (e) {

				e.preventDefault();

				$('body,html').animate({scrollTop:0},800,'swing');

			});

        });

		$(function () {

		  $('[data-toggle="tooltip"]').tooltip()

		});

		$(function () {

			$("#bt_footer h3").click(function(){

				$(this).toggleClass('opentab');

			});

		});

</script>

<?php } ?>

<!--

OpenCart is open source software and you are free to remove the powered by OpenCart if you want, but its generally accepted practise to make a small donation.

Please donate via PayPal to donate@opencart.com

//-->



<!-- Theme created by Welford Media for OpenCart 2.0 www.welfordmedia.co.uk -->

</div> <!-- End .blur -->

</div> <!-- End #bt_container -->

</body></html>