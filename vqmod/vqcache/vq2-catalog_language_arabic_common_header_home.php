<?php
// Text

	$_['text_buyeraccount']   = 'Buyer';
$_['text_selleraccount']  = 'Sell with us';
$_['text_viewselleraccount']  = 'View All Seller';

			
$_['text_home']          = 'الرئيسية';
$_['text_wishlist']      = 'قائمة رغباتي (%s)';
$_['text_shopping_cart'] = 'سلة الشراء';
$_['text_category']      = 'أقسام الموقع';
$_['text_account']       = 'حسابي';
$_['text_register']      = 'تسجيل جديد';
$_['text_login']         = 'تسجيل الدخول';
$_['text_order']         = 'طلباتي';
$_['text_transaction']   = 'رصيدي';
$_['text_download']      = 'ملفات التنزيل';
$_['text_logout']        = 'خروج';
$_['text_checkout']      = 'إنهاء الطلب';
$_['text_search']        = 'بحث';
$_['text_all']           = 'اذهب الى قسم';
$_['text_store_credit']  = 'مخزن اعتمادات';
$_['text_account_deatil']= 'تفاصيل الحساب';
$_['text_billing']		 = 'الفواتير';
$_['text_store_credit']  = 'الأتمان متجر';
$_['text_how_it_work']   = 'آلية الاستخدام';
$_['text_visit_careeb']  = 'زور قريب.كوم';
$_['text_shopping']      = 'دعنا نساعدك بالتسوق';
$_['text_grocery'] 	     = 'يمكنك طلب مواد غذائية طازجة وأكثر مع خدمة التسليم السريع!';
$_['text_city'] 		 = 'أختر مدينة';
$_['text_region'] 		 = 'أختر حي';
$_['text_area'] 		 = 'اختر حى';
$_['text_find'] 		 = 'ابحث عن متجرك المفضل';
$_['text_locate_me'] 	 = 'حدد مكاني';
$_['text_selleraccount']  ='بيع معنا';
$_['Dammam']			 = 'دمام';
$_['Jeddah']			 = 'جدة';
$_['Riyadh']			 = 'الرياض';

$_['text_location_disable'] 	 = 'آسف! موقعك على الجوال معطّل';
$_['partner_stores'] 	 = 'متاجر الشركاء';
$_['serving_areas'] 	 = 'مناطق الخدمة';
$_['supports'] 	 = 'الدعم';
$_['text_grocery_mobile'] 		 = 'يمكنك طلب مواد غذائية طازجة وأكثر مع خدمة التسليم السريع!';

$_['Al-Ada'] = 'الاضا';
$_['Al-Arifi'] = 'العريفي';
$_['Al-Badia'] = 'الباديا';
$_['Al-Murjan'] = 'المرجان';
$_['An-Nahda'] = 'النهدى';
$_['As-Safa'] = 'الصفا';
$_['text_or'] = 'أو';
