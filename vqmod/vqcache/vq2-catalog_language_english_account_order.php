<?php
// Heading
$_['heading_title']         = 'Order History';
				
				$_['text_seller']            = 'Product By:';
				$_['text_status']            = 'Status:';
				$_['text_products']            = 'Products:';
				$_['text_review']            = 'Reviews';
				$_['text_rate']            = 'Rate this seller';
			

// Text
$_['text_account']          = 'Account';
$_['text_order']            = 'Order Information';
$_['text_order_detail']     = 'Order Details';
$_['text_invoice_no']       = 'Invoice No.:';
$_['text_order_id']         = 'Order ID:';
$_['text_date_added']       = 'Date Added:';
$_['text_shipping_address'] = 'Shipping Address';
$_['text_shipping_method']  = 'Shipping Method:';
$_['text_payment_address']  = 'Payment Address';
$_['text_payment_method']   = 'Payment Method:';
$_['text_comment']          = 'Order Comments';
$_['text_history']          = 'Order History';
$_['text_success']          = 'Success: You have added <a href="%s">%s</a> to your <a href="%s">shopping cart</a>!';
$_['text_empty']            = 'You have not made any previous orders!';
$_['text_error']            = 'The order you requested could not be found!';
$_['text_delivery_time']    = 'Delivery Time:';
$_['text_shipping_fee']    = 'Shipping Fee';



// Column
$_['column_order_id']       = 'Order ID';
$_['column_product']        = 'No. of Products';
$_['column_customer']       = 'Customer';
$_['column_name']           = 'Product Name';
$_['column_model']          = 'Model';
$_['column_quantity']       = 'Quantity';
$_['column_price']          = 'Price';
$_['column_total']          = 'Total';
$_['column_action']         = 'Action';
$_['column_date_added']     = 'Date Added';
$_['column_status']         = 'Order Status';
$_['column_comment']        = 'Comment';
$_['column_review']        = 'Reviews';
$_['text_review']        = 'Reviews';

// Error
$_['error_reorder']         = '%s is not currently available to be reordered.';
$_['text_store']         = "Store doesn't exist in selected area!";
$_['text_product']         = 'Product not exist in store!';
$_['text_cart']         = 'Your cart items will be removed while changing to other store. Are you sure to move to this store?';