<?php echo $header; ?>
<div class="container">
  <ul class="breadcrumb">
    <?php foreach ($breadcrumbs as $breadcrumb) { ?>
    <li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
    <?php } ?>
  </ul>
  <div class="row"><?php echo $column_left; ?>
    <?php if ($column_left && $column_right) { ?>
    <?php $class = 'col-sm-6'; ?>
    <?php } elseif ($column_left || $column_right) { ?>
    <?php $class = 'col-sm-9'; ?>
    <?php } else { ?>
    <?php $class = 'col-sm-12'; ?>
    <?php } ?>
    <div id="content" class="<?php echo $class; ?>"><?php echo $content_top; ?>
      <h2><?php echo $heading_title; ?></h2>
      <?php if ($thumb || $description) { ?>
      <div class="row">
        <?php if ($thumb) { ?>
        <div class="col-sm-2"><img src="<?php echo $thumb; ?>" alt="<?php echo $heading_title; ?>" title="<?php echo $heading_title; ?>" class="img-thumbnail" /></div>
        <?php } ?>
        <?php if ($description) { ?>
        <div class="col-sm-10"><?php echo $description; ?></div>
        <?php } ?>
      </div>
      <hr>
      <?php } ?>
      <?php if ($categories) { ?>
      <h3><?php echo $text_refine; ?></h3>
      <?php if (count($categories) <= 5) { ?>
      <div class="row">
        <div class="col-sm-3">
          <ul>
            <?php foreach ($categories as $category) { ?>
            <li><a href="<?php echo $category['href']; ?>"><?php echo $category['name']; ?></a></li>
            <?php } ?>
          </ul>
        </div>
      </div>
      <?php } else { ?>
      <div class="row">
        <?php foreach (array_chunk($categories, ceil(count($categories) / 4)) as $categories) { ?>
        <div class="col-sm-3">
          <ul>
            <?php foreach ($categories as $category) { ?>
            <li><a href="<?php echo $category['href']; ?>"><?php echo $category['name']; ?></a></li>
            <?php } ?>
          </ul>
        </div>
        <?php } ?>
      </div>
      <?php } ?>
      <?php } ?>
      <?php if ($products) { ?>
      <p><a href="<?php echo $compare; ?>" id="compare-total"><?php echo $text_compare; ?></a></p>
      <div class="row">
        <div class="col-md-4">
          <div class="btn-group hidden-xs">
            <button type="button" id="list-view" class="btn btn-default" data-toggle="tooltip" title="<?php echo $button_list; ?>"><i class="fa fa-th-list"></i></button>
            <button type="button" id="grid-view" class="btn btn-default" data-toggle="tooltip" title="<?php echo $button_grid; ?>"><i class="fa fa-th"></i></button>
          </div>
        </div>
        <div class="col-md-2 text-right">
          <label class="control-label" for="input-sort"><?php echo $text_sort; ?></label>
        </div>
        <div class="col-md-3 text-right">
          <select id="input-sort" class="form-control" onchange="location = this.value;">
            <?php foreach ($sorts as $sorts) { ?>
            <?php if ($sorts['value'] == $sort . '-' . $order) { ?>
            <option value="<?php echo $sorts['href']; ?>" selected="selected"><?php echo $sorts['text']; ?></option>
            <?php } else { ?>
            <option value="<?php echo $sorts['href']; ?>"><?php echo $sorts['text']; ?></option>
            <?php } ?>
            <?php } ?>
          </select>
        </div>
        <div class="col-md-1 text-right">
          <label class="control-label" for="input-limit"><?php echo $text_limit; ?></label>
        </div>
        <div class="col-md-2 text-right">
          <select id="input-limit" class="form-control" onchange="location = this.value;">
            <?php foreach ($limits as $limits) { ?>
            <?php if ($limits['value'] == $limit) { ?>
            <option value="<?php echo $limits['href']; ?>" selected="selected"><?php echo $limits['text']; ?></option>
            <?php } else { ?>
            <option value="<?php echo $limits['href']; ?>"><?php echo $limits['text']; ?></option>
            <?php } ?>
            <?php } ?>
          </select>
        </div>
      </div>
      <br />

        
	  <div class="row" id="es-c">
		<?php foreach ($products as $product) { ?>
			
        <div class="product-layout product-list col-xs-12" id="p<?php echo $product['product_id']; ?>">
          <div class="product-thumb">
            <div class="image"><a href="<?php echo $product['href']; ?>"><?php if ($es_status) { ?><noscript><img src="<?php echo $product['thumb']; ?>" alt="<?php echo $product['name']; ?>" title="<?php echo $product['name']; ?>" class="img-responsive" /></noscript><img src="<?php echo $placeholder_img; ?>" data-src="<?php echo $product['thumb']; ?>"<?php } else { ?><img src="<?php echo $product['thumb']; ?>"<?php } ?> alt="<?php echo $product['name']; ?>" title="<?php echo $product['name']; ?>" class="img-responsive img-lazy" /></a></div>
            <div>
              <div class="caption">
                <h4><a href="<?php echo $product['href']; ?>"><?php echo $product['name']; ?></a></h4>
                <p><?php echo $product['description']; ?></p>
                <?php if ($product['rating']) { ?>
                <div class="rating">
                  <?php for ($i = 1; $i <= 5; $i++) { ?>
                  <?php if ($product['rating'] < $i) { ?>
                  <span class="fa fa-stack"><i class="fa fa-star-o fa-stack-2x"></i></span>
                  <?php } else { ?>
                  <span class="fa fa-stack"><i class="fa fa-star fa-stack-2x"></i><i class="fa fa-star-o fa-stack-2x"></i></span>
                  <?php } ?>
                  <?php } ?>
                </div>
                <?php } ?>
                <?php if ($product['price']) { ?>
                <p class="price">
                  <?php if (!$product['special']) { ?>
                  <?php echo $product['price']; ?>
                  <?php } else { ?>
                  <span class="price-new"><?php echo $product['special']; ?></span> <span class="price-old"><?php echo $product['price']; ?></span>
                  <?php } ?>
                  <?php if ($product['tax']) { ?>
                  <span class="price-tax"><?php echo $text_tax; ?> <?php echo $product['tax']; ?></span>
                  <?php } ?>
                </p>
                <?php } ?>
              </div>
              <div class="button-group">
                <button type="button" onclick="cart.add('<?php echo $product['product_id']; ?>', '<?php echo $product['minimum']; ?>');"><i class="fa fa-shopping-cart"></i> <span class="hidden-xs hidden-sm hidden-md"><?php echo $button_cart; ?></span></button>
                <button type="button" data-toggle="tooltip" title="<?php echo $button_wishlist; ?>" onclick="wishlist.add('<?php echo $product['product_id']; ?>');"><i class="fa fa-heart"></i></button>
                <button type="button" data-toggle="tooltip" title="<?php echo $button_compare; ?>" onclick="compare.add('<?php echo $product['product_id']; ?>');"><i class="fa fa-exchange"></i></button>
              </div>
            </div>
          </div>
        </div>
        <?php } ?>
      </div>
      <div class="row">
        <div class="col-sm-6 text-left">
  <?php if (empty($es_status)) { ?>
  <?php echo $pagination; ?>
  <?php } else { ?>
  <div class="es-p" id="es-p">
	<div class="es-l"><i class="fa fa-refresh fa-spin"></i><span class="es-lt"></span></div>
	<?php if ($use_more) { ?><div class="es-md"><button type="button" class="btn btn-primary es-m"><?php echo $text_more_results; ?></button></div><?php } ?>
	<div class="es-op"><?php echo $pagination; ?></div>
  </div>
  <?php } ?>
			</div>
        <div class="col-sm-6 text-right">
  <?php if (empty($es_status)) { ?>
  <?php echo $results; ?>
  <?php } else { ?>
  <div class="es-r"><?php echo $results; ?></div>
  <?php } ?>
			</div>
      </div>
      <?php } ?>
      <?php if (!$categories && !$products) { ?>
      <p><?php echo $text_empty; ?></p>
      <div class="buttons">
        <div class="pull-right"><a href="<?php echo $continue; ?>" class="btn btn-primary"><?php echo $button_continue; ?></a></div>
      </div>
      <?php } ?>
      <?php echo $content_bottom; ?>
<?php if (!empty($es_status) && $use_sticky_footer) { ?>
<div class="es-sf" id="es-sf">
	<div class="container">
		<div class="row">
			<div class="es-sf-c <?php echo ($column_left && $column_right) ? 'col-sm-offset-3 col-sm-6' : (($column_left && !$column_right) ? 'col-sm-offset-3 col-sm-9' : ((!$column_left && $column_right) ? 'col-sm-9' : 'col-sm-12')); ?>">
				<div class="row">
					<div class="col-sm-6 text-left">
						<div class="es-p" id="es-p">
							<div class="es-l"><i class="fa fa-refresh fa-spin"></i><span class="es-lt"></span></div>
							<?php if ($use_more) { ?><div class="es-md"><button type="button" class="btn btn-primary es-m"><?php echo $text_more_results; ?></button></div><?php } ?>
						</div>
					</div>
					<div class="col-sm-6 text-right"><div class="es-r"><?php echo $results; ?></div></div>
				</div>
			</div>
		</div>
	</div>
</div>
<?php } ?>
			</div>
    <?php echo $column_right; ?></div>
</div>

<?php if (!empty($es_status)) { ?><script type="text/javascript"><!--
!function(e,o){var t=parseInt("<?php echo $total_products; ?>",10),n=parseInt("<?php echo $start_page; ?>",10),a=parseInt("<?php echo $current_page; ?>",10),l=parseInt("<?php echo $page_limit; ?>",10),i=parseInt("<?php echo $bottom_pixels; ?>",10),c=parseInt("<?php echo $use_more_after; ?>",10),r=parseInt("<?php echo $auto_load_counter; ?>",10),s="<?php echo $text_showing; ?>",d="<?php echo $text_loading; ?>",p="<?php echo $next_page_link; ?>",f=null;e.use_fade_in=parseInt("<?php echo (int)$use_fade_in; ?>"),e.update_path=function(){if(window.history&&history.pushState&&f){var e=window.location,t=e.protocol+"//"+e.host+e.pathname+o.query.remove("page").set("pages",n+"-"+a).hash(f).toString(["route"]);window.history.replaceState(null,null,t)}},"function"!=typeof e.onBeforeAdd&&(e.onBeforeAdd=function(){o("#es-c").children(":not(.clearfix)").attr("data-state","loaded").removeAttr("style")}),"function"!=typeof e.onAdd&&(e.onAdd=function(t,n){o("#es-c").append(t.data),e.load_lazy_images(),e.use_fade_in?o("#es-c").children("[data-state!=loaded]:not(.clearfix)").hide().fadeInWithDelay(function(){n.resolve()}):n.resolve()}),"function"!=typeof e.onAfterAdd&&(e.onAfterAdd=function(){view=localStorage.getItem("display");var e=o("#column-right, #column-left").length,t="";if("list"==view)o("#es-c").children("[data-state!=loaded]:not(.clearfix)").attr("class","product-layout product-list col-xs-12");else{var n;2==e?(n="product-layout product-grid col-lg-6 col-md-6 col-sm-12 col-xs-12"):1==e?(n="product-layout product-grid col-lg-4 col-md-4 col-sm-6 col-xs-12"):(n="product-layout product-grid col-lg-3 col-md-3 col-sm-6 col-xs-12"),o("#es-c").children("[data-state!=loaded]:not(.clearfix)").attr("class",n)}}),e.load_lazy_images=function(){var e=o.Deferred();return o("img.img-lazy").show().lazy({bind:"event",enableThrottle:!0,effect:"fadeIn",effectTime:150,afterLoad:function(e){e.removeClass("img-lazy")},onFinishedAll:function(){e.resolve()}}),e.promise()},load_next_page=function(){var i=o.Deferred();return o.ajax({type:"POST",dataType:"json",url:p+(a+1),beforeSend:function(){var e=++a*l>t?t:a*l;o(".es-lt").html(d.replace("{start}",(a-1)*l+1).replace("{end}",e).replace("{total}",t)),o(".es-l").fadeIn(200)}}).done(function(t,n,a){t&&t.success&&t.data?(e.update_path(),o.when(e.onBeforeAdd(t)).then(e.onAdd(t,i)).then(e.onAfterAdd(t))):t&&t.error&&t.error_type&&"maintenance"==t.error_type?window.location.reload():window&&window.console&&window.console.log&&window.console.log("ES.error",t,n,a)}).fail(function(e,o){"parsererror"==o&&window&&window.console&&window.console.log&&window.console.log("ES.error",o,e)}).always(function(){var e=a*l>t?t:a*l;o(".es-l").fadeOut(200),o(".es-r").html(s.replace("{start}",(n-1)*l+1).replace("{end}",e).replace("{total}",t))}),i.promise()}<?php if ($use_fade_in) { ?>,o.fn.fadeInWithDelay=function(e){var t=this.length>25?10:25,n=o.Deferred(),a=n;return this.each(function(){var e=this;a=a.pipe(function(){return o.Deferred(function(n){o(e).delay(t).fadeIn(75,function(){o(this).removeAttr("style"),n.resolve()})}).promise()})}),n.resolve(),o.when(a).done(function(){"function"==typeof e&&e.call()}),this}<?php } ?>,o(function(){var d,p=!1,u=a*l>t?t:a*l;o(".es-op").hide(),o(".es-r").html(s.replace("{start}",(n-1)*l+1).replace("{end}",u).replace("{total}",t)),e.load_lazy_images(),o(window).scroll(function(){d&&clearTimeout(d),d=setTimeout(function(){o.each(o("#es-c").children(),function(){return o(this).offset().top>=o(window).scrollTop()&&this.id?(f=this.id,e.update_path(),!1):!0})},500)})<?php if ($use_back_top) { ?>,o(window).scroll(function(){o(this).scrollTop()>300?o(".back-to-top").fadeIn(200):o(".back-to-top").fadeOut(200)}),o("body").on("click",".back-to-top",function(){o("html, body").animate({scrollTop:0},700)})<?php } ?><?php if ($use_sticky_footer) { ?>,o(window).scroll(function(){o("#es-p").length&&o("#es-c").length&&(o(window).scrollTop()>o("#es-c").offset().top&&o(window).scrollTop()+o(window).height()<o("#es-p").offset().top?p||(o("#es-sf").fadeIn(),o("#es-sf .es-l").hide(),p=!0):(p=!1,o("#es-sf").hide()))})<?php } ?><?php if (!$use_more) { ?>,o(window).endlessScroll({contentTarget:"#es-c",bottomPixels:i,callback:function(e,t){load_next_page().done(function(){o.isFunction(t)&&t.call()})},ceaseFire:function(){return a>=Math.ceil(t/l)?!0:!1}})<?php } else if ($use_more && $use_more_after == 0){ ?>,o("body").on("click",".es-m",function(){o(".es-m").fadeOut(200).promise().done(function(){load_next_page().done(function(){t>=a*l+1&&o(".es-m").fadeIn(200)})})}),t>a*l&&o(".es-md").show()<?php } else { ?>,o(window).endlessScroll({contentTarget:"#es-c",bottomPixels:i,callback:function(e,n){r++,load_next_page().done(function(){o.isFunction(n)&&n.call(),r>=c&&t>=a*l+1&&o(".es-m").fadeIn(200)})},ceaseFire:function(){return a>=Math.ceil(t/l)?!0:r>=c?!0:!1}}),o("body").on("click",".es-m",function(){o(".es-m").fadeOut(200).promise().done(function(){load_next_page().done(function(){r=0})})}),t>a*l&&(o(".es-m").hide(),o(".es-md").show()),r>=c&&t>=a*l+1&&o(".es-m").fadeIn(200)<?php } ?>,o(window).trigger("scroll")})}(window.bull5i=window.bull5i||{},jQuery);
//--></script><?php } ?>
			
<?php echo $footer; ?>
