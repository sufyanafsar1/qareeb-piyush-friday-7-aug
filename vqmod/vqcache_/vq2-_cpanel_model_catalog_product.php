<?php
class ModelCatalogProduct extends Model
{
	// add model
	public function getSellers()
	{
		$sellers_data = array();

		//$query = $this->db->query("SELECT distinct(v.seller_id), CONCAT(v.firstname, ' ',v.lastname) AS name  FROM " . DB_PREFIX . "sellers as v," . DB_PREFIX . "seller as p where p.seller_id = v.seller_id ORDER BY v.firstname");

		$query = $this->db->query("SELECT distinct(v.seller_id), CONCAT(v.firstname, ' ',v.lastname) AS name  FROM " . DB_PREFIX . "sellers as v ORDER BY v.firstname");

		$sellers = $query->rows;
		if (count($sellers) > 0) {
			foreach ($sellers as $seller) {
				$sellers_data[$seller['seller_id']] = $seller['name'];
			}
		}
		return $sellers_data;
	}


	public function getProductseller($product_id)
	{
		$query = $this->db->query("SELECT seller_id  FROM " . DB_PREFIX . "product where product_id = '" . (int) $product_id . "'");

		return $query->row['seller_id'];
	}
	public function getProductsellername($seller_id)
	{
		$query = $this->db->query("SELECT CONCAT(v.firstname, ' ',v.lastname) AS name  FROM " . DB_PREFIX . "sellers v where 
		v.seller_id = '" . (int) $seller_id . "'");

		return $query->row['name'];
	}

	public function getProductSellers($product_id)
	{
		$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "sellers_products as sp," . DB_PREFIX . "sellers as s WHERE sp.product_id = '" . (int) $product_id . "' AND s.seller_id=sp.seller_id");
		$data = array();

		if (count($query->rows) > 0) {
			foreach ($query->rows as $rows) {
				$sql = "SELECT cp.category_id AS category_id, GROUP_CONCAT(cd1.name ORDER BY cp.level SEPARATOR '&nbsp;&nbsp;&gt;&nbsp;&nbsp;') AS name, c1.parent_id, c1.sort_order,c1.seller_id,c1.approve FROM " . DB_PREFIX . "category_path cp LEFT JOIN " . DB_PREFIX . "category c1 ON (cp.category_id = c1.category_id) LEFT JOIN " . DB_PREFIX . "category c2 ON (cp.path_id = c2.category_id) LEFT JOIN " . DB_PREFIX . "category_description cd1 ON (cp.path_id = cd1.category_id) LEFT JOIN " . DB_PREFIX . "category_description cd2 ON (cp.category_id = cd2.category_id) WHERE cd1.language_id = '" . (int) $this->config->get('config_language_id') . "' AND cd2.language_id = '" . (int) $this->config->get('config_language_id') . "' GROUP BY cp.category_id";
				$categories = $this->db->query($sql);
				$query2 = $this->db->query("SELECT * FROM " . DB_PREFIX . "sellercategories as osc," . DB_PREFIX . "category_description as ocd WHERE osc.seller_id = '" . (int) $rows['seller_id'] . "' AND osc.product_id = '" . (int) $product_id . "' AND ocd.language_id = '1' AND ocd.category_id = osc.category_id");
				if (count($query2->rows) > 0) {
					foreach ($query2->rows as $sc) {
						foreach ($categories->rows as $category) {
							if ($category['category_id'] == $sc['category_id']) {
								$sc['name'] = $category['name'];
								$rows['categories'][] = $sc;
							}
						}
					}
				}
				$data[] = $rows;
			}
		}
		return $data;
	}
	// add model
	public function getProductNamenew($product_id)
	{

		$product_name = "";
		$query = $this->db->query("SELECT name, language_id FROM oc_product_description  WHERE product_id = '" . (int) $product_id . "' AND language_id IN(3,1)");
		/* echo"<pre>";
		print_r($query);
		exit; */
		if (count($query->rows) > 0) {
			foreach ($query->rows as $result) {
				if ($result['language_id'] == 1) {
					$product_name_en = $result['name'];
				} else {
					$product_name_ar = $result['name'];
				}
			}
		}
		$product_name = $product_name_en . " - " . $product_name_ar;
		//$product_name = $product_name_ar;

		return $product_name;
	}
	public function getProductenglishName($product_id)
	{

		$product_name = "";
		$query = $this->db->query("SELECT name, language_id FROM oc_product_description  WHERE product_id = '" . (int) $product_id . "' AND language_id IN(3,1)");
		/* echo"<pre>";
		print_r($query);
		exit; */
		if (count($query->rows) > 0) {
			foreach ($query->rows as $result) {
				if ($result['language_id'] == 1) {
					$product_name_en = $result['name'];
				} else {
					$product_name_ar = $result['name'];
				}
			}
		}
		//$product_name = $product_name_en." - ".$product_name_ar;
		$product_name = $product_name_ar;

		return $product_name;
	}
	public function addProduct($data)
	{
		$this->event->trigger('pre.admin.product.add', $data);

		$this->db->query("INSERT 
			INTO " . DB_PREFIX . "product 
			SET 
				model = '" . $this->db->escape($data['model']) . "', 
				sku = '" . $this->db->escape($data['sku']) . "', 
				upc = '" . $this->db->escape($data['upc']) . "', 
				ean = '" . $this->db->escape($data['ean']) . "', 
				jan = '" . $this->db->escape($data['jan']) . "', 
				isbn = '" . $this->db->escape($data['isbn']) . "', 
				mpn = '" . $this->db->escape($data['mpn']) . "', 
				location = '" . $this->db->escape($data['location']) . "', 
				quantity = '" . (int) $data['quantity'] . "', 
				minimum = '" . (int) $data['minimum'] . "', 
				subtract = '" . (int) $data['subtract'] . "', 
				stock_status_id = '" . (int) $data['stock_status_id'] . "', 
				date_available = '" . $this->db->escape($data['date_available']) . "', 
				manufacturer_id = '" . (int) $data['manufacturer_id'] . "', 
				shipping = '" . (int) $data['shipping'] . "', 
				buy_price = '" . (float) $data['buy_price'] . "', 
				price = '" . (float) $data['price'] . "', 
				points = '" . (int) $data['points'] . "', 
				weight = '" . (float) $data['weight'] . "', 
				weight_class_id = '" . (int) $data['weight_class_id'] . "', 
				length = '" . (float) $data['length'] . "', 
				width = '" . (float) $data['width'] . "', 
				height = '" . (float) $data['height'] . "', 
				length_class_id = '" . (int) $data['length_class_id'] . "', 
				status = '" . (int) $data['status'] . "', 
				tax_class_id = '" . (int) $data['tax_class_id'] . "', 
				sort_order = '" . (int) $data['sort_order'] . "', 
				date_added = NOW()");

		$product_id = $this->db->getLastId();
				
		
		$this->db->query("UPDATE " . DB_PREFIX . "product SET approve = '" . (int)$data['approve'] . "' WHERE product_id = '" . (int)$product_id . "'");			   

		
		if($data['seller_id'] >0){	
		
		}else{
		if (isset($data['product_seller'])) {
				$i = 1;
			foreach ($data['product_seller'] as $product_seller) {
			
				 if ($i==1){
				  
					$this->db->query("UPDATE " . DB_PREFIX . "product SET seller_id = '" . (int)$product_seller['seller_id'] . "' WHERE product_id = '" . (int)$product_id . "'");

					$this->db->query("INSERT INTO " . DB_PREFIX . "seller SET vproduct_id = '" . (int)$product_id . "', seller_id = '" . (int)$product_seller['seller_id'] . "'");
		
			    }
				  
				  
				
				
				}
				
				$i++;
			}else{
			
			           if($this->config->get('config_defaultseller_id')){
					   
							$this->db->query("UPDATE " . DB_PREFIX . "product SET seller_id = '" . (int)$this->config->get('config_defaultseller_id') . "' WHERE product_id = '" . (int)$product_id . "'");

							$this->db->query("INSERT INTO " . DB_PREFIX . "seller SET vproduct_id = '" . (int)$product_id . "', seller_id = '" . (int)$this->config->get('config_defaultseller_id') . "'");
							
							$this->db->query("INSERT INTO " . DB_PREFIX . "sellers_products SET
							product_id = '" . (int)$product_id . "', 
							seller_id = '" . (int)$this->config->get('config_defaultseller_id') . "',
							quantity = '" . (int)$data['quantity'] . "',
							price = '" . (float)$data['price'] . "',
							date_added = NOW()");
					   
					   
					    }
			
			 }
			
			
		}

										
		
		
		if (isset($data['product_seller'])) {			
		
		foreach ($data['product_seller'] as $product_seller) {

		$this->db->query("INSERT INTO " . DB_PREFIX . "sellers_products SET product_id = '" . (int)$product_id . "', 
		seller_id = '" . (int)$product_seller['seller_id'] . "', quantity = '" . (int)$product_seller['quantity'] . "',
		price = '" . (float)$product_seller['price'] . "', date_added = NOW()");			
		
		}		
		
		}	

		
		if (isset($data['product_seller']) && !empty($data['product_seller'])) {
			if (count($data['product_seller']) > 0) {
				foreach ($data['product_seller'] as $seller_data) {
					$this->db->query("DELETE FROM " . DB_PREFIX . "sellercategories WHERE product_id = '" . (int) $product_id . "' AND seller_id = '" . (int) $seller_data["seller_id"] . "'");
					if (isset($seller_data['category_id'])) {
						if (count($seller_data['category_id']) > 0) {
							foreach ($seller_data['category_id'] as $category) {
								$this->db->query("INSERT 
									INTO " . DB_PREFIX . "sellercategories 
									SET 
										product_id = '" . (int) $product_id . "', 
										seller_id = '" . (int) $seller_data["seller_id"] . "', 
										category_id = '" . (int) $category . "'");
							}
						}
					}
				}
			} //exit;
		}


				$this->db->query("UPDATE " . DB_PREFIX . "product SET next_day_delivery_active = '" . $this->db->escape($data['next_day_delivery_active']) . "' WHERE product_id = '" . (int)$product_id . "'");
			



			if (isset($data['product_seller'])) {			
		
		$q=0;
				foreach ($data['product_seller'] as $product_seller) {

				    $q +=$product_seller['quantity'];		
				
				}
				
				$this->db->query("UPDATE " . DB_PREFIX . "product SET quantity = '" . (int)$q . "' WHERE product_id = '" . (int)$product_id . "'");
		
		}	



		
		if (isset($data['image'])) {
			$this->db->query("UPDATE " . DB_PREFIX . "product SET image = '" . $this->db->escape($data['image']) . "' WHERE product_id = '" . (int) $product_id . "'");
		}

		foreach ($data['product_description'] as $language_id => $value) {

			$this->db->query("INSERT INTO " . DB_PREFIX . "product_description SET product_id = '" . (int) $product_id . "', language_id = '" . (int) $language_id . "', name = '" . $this->db->escape($value['name']) . "', description = '" . $this->db->escape($value['description']) . "', tag = '" . $this->db->escape($value['tag']) . "', meta_title = '" . $this->db->escape($value['meta_title']) . "', meta_description = '" . $this->db->escape($value['meta_description']) . "', meta_keyword = '" . $this->db->escape($value['meta_keyword']) . "'");
		}

		if (isset($data['product_store'])) {
			foreach ($data['product_store'] as $store_id) {
				$this->db->query("INSERT INTO " . DB_PREFIX . "product_to_store SET product_id = '" . (int) $product_id . "', store_id = '" . (int) $store_id . "'");
			}
		}
		if (isset($data['product_area'])) {
			foreach ($data['product_area'] as $area_id) {
				$this->db->query("INSERT INTO " . DB_PREFIX . "product_to_area SET product_id = '" . (int) $product_id . "', area_id = '" . (int) $area_id . "'");
			}
		}

		if (isset($data['product_attribute'])) {
			foreach ($data['product_attribute'] as $product_attribute) {
				if ($product_attribute['attribute_id']) {
					foreach ($product_attribute['product_attribute_description'] as $language_id => $product_attribute_description) {
						$this->db->query("INSERT INTO " . DB_PREFIX . "product_attribute SET product_id = '" . (int) $product_id . "', attribute_id = '" . (int) $product_attribute['attribute_id'] . "', language_id = '" . (int) $language_id . "', text = '" .  $this->db->escape($product_attribute_description['text']) . "'");
					}
				}
			}
		}

		if (isset($data['product_option'])) {

			$this->db->query("UPDATE " . DB_PREFIX . "product SET option_available = '1' WHERE product_id = '" . (int) $product_id . "'");

			foreach ($data['product_option'] as $product_option) {
				if ($product_option['type'] == 'select' || $product_option['type'] == 'radio' || $product_option['type'] == 'checkbox' || $product_option['type'] == 'image') {
					if (isset($product_option['product_option_value'])) {
						$this->db->query("INSERT INTO " . DB_PREFIX . "product_option SET product_id = '" . (int) $product_id . "', option_id = '" . (int) $product_option['option_id'] . "', required = '" . (int) $product_option['required'] . "'");

						$product_option_id = $this->db->getLastId();

						foreach ($product_option['product_option_value'] as $product_option_value) {
							$this->db->query("INSERT INTO " . DB_PREFIX . "product_option_value SET product_option_id = '" . (int) $product_option_id . "', product_id = '" . (int) $product_id . "', option_id = '" . (int) $product_option['option_id'] . "', option_value_id = '" . (int) $product_option_value['option_value_id'] . "', quantity = '" . (int) $product_option_value['quantity'] . "', subtract = '" . (int) $product_option_value['subtract'] . "',buy_price = '" . (float) $product_option_value['buy_price'] . "', price = '" . (float) $product_option_value['price'] . "', price_prefix = '" . $this->db->escape($product_option_value['price_prefix']) . "', points = '" . (int) $product_option_value['points'] . "', points_prefix = '" . $this->db->escape($product_option_value['points_prefix']) . "', weight = '" . (float) $product_option_value['weight'] . "', weight_prefix = '" . $this->db->escape($product_option_value['weight_prefix']) . "'");
						}
					}
				} else {
					$this->db->query("INSERT INTO " . DB_PREFIX . "product_option SET product_id = '" . (int) $product_id . "', option_id = '" . (int) $product_option['option_id'] . "', value = '" . $this->db->escape($product_option['value']) . "', required = '" . (int) $product_option['required'] . "'");
				}
			}
		} else {
			$this->db->query("UPDATE " . DB_PREFIX . "product SET option_available = '0' WHERE product_id = '" . (int) $product_id . "'");
		}

		if (isset($data['product_discount'])) {
			foreach ($data['product_discount'] as $product_discount) {
				$this->db->query("INSERT INTO " . DB_PREFIX . "product_discount SET product_id = '" . (int) $product_id . "', customer_group_id = '" . (int) $product_discount['customer_group_id'] . "', quantity = '" . (int) $product_discount['quantity'] . "', priority = '" . (int) $product_discount['priority'] . "', price = '" . (float) $product_discount['price'] . "', date_start = '" . $this->db->escape($product_discount['date_start']) . "', date_end = '" . $this->db->escape($product_discount['date_end']) . "'");
			}
		}

		if (isset($data['product_special'])) {
			foreach ($data['product_special'] as $product_special) {
				$this->db->query("INSERT INTO " . DB_PREFIX . "product_special SET product_id = '" . (int) $product_id . "', customer_group_id = '" . (int) $product_special['customer_group_id'] . "', priority = '" . (int) $product_special['priority'] . "', price = '" . (float) $product_special['price'] . "', date_start = '" . $this->db->escape($product_special['date_start']) . "', date_end = '" . $this->db->escape($product_special['date_end']) . "'");
			}
		}

		if (isset($data['product_feature'])) {
			foreach ($data['product_feature'] as $product_feature) {
				$this->db->query("INSERT INTO " . DB_PREFIX . "product_feature SET product_id = '" . (int) $product_id . "', customer_group_id = '" . (int) $product_feature['customer_group_id'] . "', priority = '" . (int) $product_feature['priority'] . "', price = '" . (float) $product_feature['price'] . "', date_start = '" . $this->db->escape($product_feature['date_start']) . "', date_end = '" . $this->db->escape($product_feature['date_end']) . "', seller_id = '" . (int) $product_feature['seller_id'] . "'");
			}
		}


		if (isset($data['product_image'])) {
			foreach ($data['product_image'] as $product_image) {
				$this->db->query("INSERT INTO " . DB_PREFIX . "product_image SET product_id = '" . (int) $product_id . "', image = '" . $this->db->escape($product_image['image']) . "', sort_order = '" . (int) $product_image['sort_order'] . "'");
			}
		}

		if (isset($data['product_download'])) {
			foreach ($data['product_download'] as $download_id) {
				$this->db->query("INSERT INTO " . DB_PREFIX . "product_to_download SET product_id = '" . (int) $product_id . "', download_id = '" . (int) $download_id . "'");
			}
		}

		if (isset($data['product_category'])) {
			foreach ($data['product_category'] as $category_id) {
				$this->db->query("INSERT INTO " . DB_PREFIX . "product_to_category SET product_id = '" . (int) $product_id . "', category_id = '" . (int) $category_id . "'");
			}
		}

		if (isset($data['product_filter'])) {
			foreach ($data['product_filter'] as $filter_id) {
				$this->db->query("INSERT INTO " . DB_PREFIX . "product_filter SET product_id = '" . (int) $product_id . "', filter_id = '" . (int) $filter_id . "'");
			}
		}

		if (isset($data['product_related'])) {
			foreach ($data['product_related'] as $related_id) {
				$this->db->query("DELETE FROM " . DB_PREFIX . "product_related WHERE product_id = '" . (int) $product_id . "' AND related_id = '" . (int) $related_id . "'");
				$this->db->query("INSERT INTO " . DB_PREFIX . "product_related SET product_id = '" . (int) $product_id . "', related_id = '" . (int) $related_id . "'");
				$this->db->query("DELETE FROM " . DB_PREFIX . "product_related WHERE product_id = '" . (int) $related_id . "' AND related_id = '" . (int) $product_id . "'");
				$this->db->query("INSERT INTO " . DB_PREFIX . "product_related SET product_id = '" . (int) $related_id . "', related_id = '" . (int) $product_id . "'");
			}
		}

		if (isset($data['product_reward'])) {
			foreach ($data['product_reward'] as $customer_group_id => $product_reward) {
				if ((int) $product_reward['points'] > 0) {
					$this->db->query("INSERT INTO " . DB_PREFIX . "product_reward SET product_id = '" . (int) $product_id . "', customer_group_id = '" . (int) $customer_group_id . "', points = '" . (int) $product_reward['points'] . "'");
				}
			}
		}

		if (isset($data['product_layout'])) {
			foreach ($data['product_layout'] as $store_id => $layout_id) {
				$this->db->query("INSERT INTO " . DB_PREFIX . "product_to_layout SET product_id = '" . (int) $product_id . "', store_id = '" . (int) $store_id . "', layout_id = '" . (int) $layout_id . "'");
			}
		}

		if (isset($data['keyword'])) {
			$this->db->query("INSERT INTO " . DB_PREFIX . "url_alias SET query = 'product_id=" . (int) $product_id . "', keyword = '" . $this->db->escape($data['keyword']) . "'");
		}

		if (isset($data['product_recurrings'])) {
			foreach ($data['product_recurrings'] as $recurring) {
				$this->db->query("INSERT INTO `" . DB_PREFIX . "product_recurring` SET `product_id` = " . (int) $product_id . ", customer_group_id = " . (int) $recurring['customer_group_id'] . ", `recurring_id` = " . (int) $recurring['recurring_id']);
			}
		}

		$files = glob('../jsonData/*.json'); //get all file names		
		foreach ($files as $file) {
			if (is_file($file))
				unlink($file); //delete file		
		}

		$this->cache->delete('product');

		$this->event->trigger('post.admin.product.add', $product_id);

		return $product_id;
	}
	public function sellers($product_id, $data)
	{
	}
	public function editProduct($product_id, $data)
	{
		// echo '<pre>';
		// print_r($data);
		// echo '</pre>';
		// exit;
		
		$this->event->trigger('pre.admin.product.edit', $data);

		if (isset($data['product_seller']) && !empty($data['product_seller'])) {
			
			if (count($data['product_seller']) > 0) {
				foreach ($data['product_seller'] as $seller_data) {
					// echo '<pre>';
					// print_r($seller_data);
					// echo '</pre>';
					
					$this->db->query("DELETE FROM " . DB_PREFIX . "sellercategories WHERE product_id = '" . (int) $product_id . "' AND seller_id = '" . (int) $seller_data["seller_id"] . "'");
					if (count($seller_data['category_id']) > 0) {
						foreach ($seller_data['category_id'] as $category) {
							$this->db->query("INSERT INTO " . DB_PREFIX . "sellercategories SET product_id = '" . (int) $product_id . "', seller_id = '" . (int) $seller_data["seller_id"] . "', category_id = '" . (int) $category . "'");
						}
					}
				}
				// exit;
			}
		}

		$this->db->query("UPDATE " . 
			DB_PREFIX . "product 
			SET 
				model = '" . $this->db->escape($data['model']) . "', 
				sku = '" . $this->db->escape($data['sku']) . "', 
				upc = '" . $this->db->escape($data['upc']) . "', 
				ean = '" . $this->db->escape($data['ean']) . "', 
				jan = '" . $this->db->escape($data['jan']) . "', 
				isbn = '" . $this->db->escape($data['isbn']) . "', 
				mpn = '" . $this->db->escape($data['mpn']) . "', 
				location = '" . $this->db->escape($data['location']) . "', 
				quantity = '" . (int) $data['quantity'] . "', 
				minimum = '" . (int) $data['minimum'] . "', 
				subtract = '" . (int) $data['subtract'] . "', 
				stock_status_id = '" . (int) $data['stock_status_id'] . "', 
				date_available = '" . $this->db->escape($data['date_available']) . "', 
				manufacturer_id = '" . (int) $data['manufacturer_id'] . "', 
				shipping = '" . (int) $data['shipping'] . "', 
				buy_price = '" . (float) $data['buy_price'] . "', 
				price = '" . (float) $data['price'] . "', 
				points = '" . (int) $data['points'] . "', 
				weight = '" . (float) $data['weight'] . "', 
				weight_class_id = '" . (int) $data['weight_class_id'] . "', 
				length = '" . (float) $data['length'] . "', 
				width = '" . (float) $data['width'] . "', 
				height = '" . (float) $data['height'] . "', 
				length_class_id = '" . (int) $data['length_class_id'] . "', 
				status = '" . (int) $data['status'] . "', 
				tax_class_id = '" . (int) $data['tax_class_id'] . "', 
				sort_order = '" . (int) $data['sort_order'] . "', 
				date_modified = NOW() 
				WHERE product_id = '" . (int) $product_id . "'");


				$this->db->query("UPDATE " . DB_PREFIX . "product SET next_day_delivery_active = '" . $this->db->escape($data['next_day_delivery_active']) . "' WHERE product_id = '" . (int)$product_id . "'");
			



			if (isset($data['product_seller'])) {			
		
		$q=0;
				foreach ($data['product_seller'] as $product_seller) {

				    $q +=$product_seller['quantity'];		
				
				}
				
				$this->db->query("UPDATE " . DB_PREFIX . "product SET quantity = '" . (int)$q . "' WHERE product_id = '" . (int)$product_id . "'");
		
		}	



		
		if (isset($data['image'])) {
			$this->db->query("UPDATE " . DB_PREFIX . "product SET image = '" . $this->db->escape($data['image']) . "' WHERE product_id = '" . (int) $product_id . "'");
		}

		$this->db->query("DELETE FROM " . DB_PREFIX . "product_description WHERE product_id = '" . (int) $product_id . "'");

		foreach ($data['product_description'] as $language_id => $value) {
			$this->db->query("INSERT 
				INTO " . DB_PREFIX . "product_description 
				SET 
					product_id = '" . (int) $product_id . "', 
					language_id = '" . (int) $language_id . "', 
					name = '" . $this->db->escape($value['name']) . "', 
					description = '" . $this->db->escape($value['description']) . "', 
					tag = '" . $this->db->escape($value['tag']) . "', 
					meta_title = '" . $this->db->escape($value['meta_title']) . "',
					meta_description = '" . $this->db->escape($value['meta_description']) . "', 
					meta_keyword = '" . $this->db->escape($value['meta_keyword']) . "'");
		}

		$this->db->query("DELETE FROM " . DB_PREFIX . "product_to_store WHERE product_id = '" . (int) $product_id . "'");

		if (isset($data['product_store'])) {
			foreach ($data['product_store'] as $store_id) {
				$this->db->query("INSERT 
					INTO " . DB_PREFIX . "product_to_store 
					SET 
						product_id = '" . (int) $product_id . "', 
						store_id = '" . (int) $store_id . "'");
			}
		}

		$this->db->query("DELETE FROM " . DB_PREFIX . "product_to_area WHERE product_id = '" . (int) $product_id . "'");

		if (isset($data['product_area'])) {
			foreach ($data['product_area'] as $area_id) {
				$this->db->query("INSERT INTO " . DB_PREFIX . "product_to_area SET product_id = '" . (int) $product_id . "', area_id = '" . (int) $area_id . "'");
			}
		}

		$this->db->query("DELETE FROM " . DB_PREFIX . "product_attribute WHERE product_id = '" . (int) $product_id . "'");

		if (!empty($data['product_attribute'])) {
			foreach ($data['product_attribute'] as $product_attribute) {
				if ($product_attribute['attribute_id']) {
					foreach ($product_attribute['product_attribute_description'] as $language_id => $product_attribute_description) {
						$this->db->query("INSERT INTO " . DB_PREFIX . "product_attribute SET product_id = '" . (int) $product_id . "', attribute_id = '" . (int) $product_attribute['attribute_id'] . "', language_id = '" . (int) $language_id . "', text = '" .  $this->db->escape($product_attribute_description['text']) . "'");
					}
				}
			}
		}

		$this->db->query("DELETE FROM " . DB_PREFIX . "product_option WHERE product_id = '" . (int) $product_id . "'");
		$this->db->query("DELETE FROM " . DB_PREFIX . "product_option_value WHERE product_id = '" . (int) $product_id . "'");
		if (isset($data['product_option'])) {

			$this->db->query("UPDATE " . DB_PREFIX . "product SET option_available = '1' WHERE product_id = '" . (int) $product_id . "'");

			foreach ($data['product_option'] as $product_option) {
				if ($product_option['type'] == 'select' || $product_option['type'] == 'radio' || $product_option['type'] == 'checkbox' || $product_option['type'] == 'image') {
					if (isset($product_option['product_option_value'])) {
						$this->db->query("INSERT INTO " . DB_PREFIX . "product_option SET product_option_id = '" . (int) $product_option['product_option_id'] . "', product_id = '" . (int) $product_id . "', option_id = '" . (int) $product_option['option_id'] . "', required = '" . (int) $product_option['required'] . "'");

						$product_option_id = $this->db->getLastId();

						foreach ($product_option['product_option_value'] as $product_option_value) {
							$this->db->query("INSERT INTO " . DB_PREFIX . "product_option_value SET product_option_value_id = '" . (int) $product_option_value['product_option_value_id'] . "', product_option_id = '" . (int) $product_option_id . "', product_id = '" . (int) $product_id . "', option_id = '" . (int) $product_option['option_id'] . "', option_value_id = '" . (int) $product_option_value['option_value_id'] . "', quantity = '" . (int) $product_option_value['quantity'] . "', subtract = '" . (int) $product_option_value['subtract'] . "',buy_price = '" . (float) $product_option_value['buy_price'] . "', price = '" . (float) $product_option_value['price'] . "', price_prefix = '" . $this->db->escape($product_option_value['price_prefix']) . "', points = '" . (int) $product_option_value['points'] . "', points_prefix = '" . $this->db->escape($product_option_value['points_prefix']) . "', weight = '" . (float) $product_option_value['weight'] . "', weight_prefix = '" . $this->db->escape($product_option_value['weight_prefix']) . "'");
						}
					}
				} else {
					$this->db->query("INSERT INTO " . DB_PREFIX . "product_option SET product_option_id = '" . (int) $product_option['product_option_id'] . "', product_id = '" . (int) $product_id . "', option_id = '" . (int) $product_option['option_id'] . "', value = '" . $this->db->escape($product_option['value']) . "', required = '" . (int) $product_option['required'] . "'");
				}
			}
		} else {
			$this->db->query("UPDATE " . DB_PREFIX . "product SET option_available = '0' WHERE product_id = '" . (int) $product_id . "'");
		}

		$this->db->query("DELETE FROM " . DB_PREFIX . "product_discount WHERE product_id = '" . (int) $product_id . "'");
		if (isset($data['product_discount'])) {
			foreach ($data['product_discount'] as $product_discount) {
				$this->db->query("INSERT INTO " . DB_PREFIX . "product_discount SET product_id = '" . (int) $product_id . "', customer_group_id = '" . (int) $product_discount['customer_group_id'] . "', quantity = '" . (int) $product_discount['quantity'] . "', priority = '" . (int) $product_discount['priority'] . "', price = '" . (float) $product_discount['price'] . "', date_start = '" . $this->db->escape($product_discount['date_start']) . "', date_end = '" . $this->db->escape($product_discount['date_end']) . "'");
			}
		}

		$this->db->query("DELETE FROM " . DB_PREFIX . "product_special WHERE product_id = '" . (int) $product_id . "'");
		if (isset($data['product_special'])) {
			foreach ($data['product_special'] as $product_special) {
				$this->db->query("INSERT INTO " . DB_PREFIX . "product_special SET product_id = '" . (int) $product_id . "', customer_group_id = '" . (int) $product_special['customer_group_id'] . "', priority = '" . (int) $product_special['priority'] . "', price = '" . (float) $product_special['price'] . "', date_start = '" . $this->db->escape($product_special['date_start']) . "', date_end = '" . $this->db->escape($product_special['date_end']) . "'");
			}
		}

		$this->db->query("DELETE FROM " . DB_PREFIX . "product_feature WHERE product_id = '" . (int) $product_id . "'");
		if (isset($data['product_feature'])) {
			foreach ($data['product_feature'] as $product_feature) {
				$this->db->query("INSERT INTO " . DB_PREFIX . "product_feature SET product_id = '" . (int) $product_id . "', customer_group_id = '" . (int) $product_feature['customer_group_id'] . "', priority = '" . (int) $product_feature['priority'] . "', price = '" . (float) $product_feature['price'] . "', date_start = '" . $this->db->escape($product_feature['date_start']) . "', date_end = '" . $this->db->escape($product_feature['date_end']) . "', seller_id = '" . (int) $product_feature['seller_id'] . "'");
			}
		}

		$this->db->query("DELETE FROM " . DB_PREFIX . "product_image WHERE product_id = '" . (int) $product_id . "'");
		if (isset($data['product_image'])) {
			foreach ($data['product_image'] as $product_image) {
				$this->db->query("INSERT INTO " . DB_PREFIX . "product_image SET product_id = '" . (int) $product_id . "', image = '" . $this->db->escape($product_image['image']) . "', sort_order = '" . (int) $product_image['sort_order'] . "'");
			}
		}

		$this->db->query("DELETE FROM " . DB_PREFIX . "product_to_download WHERE product_id = '" . (int) $product_id . "'");
		if (isset($data['product_download'])) {
			foreach ($data['product_download'] as $download_id) {
				$this->db->query("INSERT INTO " . DB_PREFIX . "product_to_download SET product_id = '" . (int) $product_id . "', download_id = '" . (int) $download_id . "'");
			}
		}

		$this->db->query("DELETE FROM " . DB_PREFIX . "product_to_category WHERE product_id = '" . (int) $product_id . "'");
		if (isset($data['product_category'])) {
			foreach ($data['product_category'] as $category_id) {
				$this->db->query("INSERT INTO " . DB_PREFIX . "product_to_category SET product_id = '" . (int) $product_id . "', category_id = '" . (int) $category_id . "'");
			}
		}

		$this->db->query("DELETE FROM " . DB_PREFIX . "product_filter WHERE product_id = '" . (int) $product_id . "'");
		if (isset($data['product_filter'])) {
			foreach ($data['product_filter'] as $filter_id) {
				$this->db->query("INSERT INTO " . DB_PREFIX . "product_filter SET product_id = '" . (int) $product_id . "', filter_id = '" . (int) $filter_id . "'");
			}
		}

		$this->db->query("DELETE FROM " . DB_PREFIX . "product_related WHERE product_id = '" . (int) $product_id . "'");
		$this->db->query("DELETE FROM " . DB_PREFIX . "product_related WHERE related_id = '" . (int) $product_id . "'");
		if (isset($data['product_related'])) {
			foreach ($data['product_related'] as $related_id) {
				$this->db->query("DELETE FROM " . DB_PREFIX . "product_related WHERE product_id = '" . (int) $product_id . "' AND related_id = '" . (int) $related_id . "'");
				$this->db->query("INSERT INTO " . DB_PREFIX . "product_related SET product_id = '" . (int) $product_id . "', related_id = '" . (int) $related_id . "'");
				$this->db->query("DELETE FROM " . DB_PREFIX . "product_related WHERE product_id = '" . (int) $related_id . "' AND related_id = '" . (int) $product_id . "'");
				$this->db->query("INSERT INTO " . DB_PREFIX . "product_related SET product_id = '" . (int) $related_id . "', related_id = '" . (int) $product_id . "'");
			}
		}

		$this->db->query("DELETE FROM " . DB_PREFIX . "product_reward WHERE product_id = '" . (int) $product_id . "'");
		if (isset($data['product_reward'])) {
			foreach ($data['product_reward'] as $customer_group_id => $value) {
				if ((int) $value['points'] > 0) {
					$this->db->query("INSERT INTO " . DB_PREFIX . "product_reward SET product_id = '" . (int) $product_id . "', customer_group_id = '" . (int) $customer_group_id . "', points = '" . (int) $value['points'] . "'");
				}
			}
		}

		$this->db->query("DELETE FROM " . DB_PREFIX . "product_to_layout WHERE product_id = '" . (int) $product_id . "'");
		if (isset($data['product_layout'])) {
			foreach ($data['product_layout'] as $store_id => $layout_id) {
				$this->db->query("INSERT INTO " . DB_PREFIX . "product_to_layout SET product_id = '" . (int) $product_id . "', store_id = '" . (int) $store_id . "', layout_id = '" . (int) $layout_id . "'");
			}
		}

		$this->db->query("DELETE FROM " . DB_PREFIX . "url_alias WHERE query = 'product_id=" . (int) $product_id . "'");
		if ($data['keyword']) {
			$this->db->query("INSERT INTO " . DB_PREFIX . "url_alias SET query = 'product_id=" . (int) $product_id . "', keyword = '" . $this->db->escape($data['keyword']) . "'");
		}

		$this->db->query("DELETE FROM `" . DB_PREFIX . "product_recurring` WHERE product_id = " . (int) $product_id);
		if (isset($data['product_recurring'])) {
			foreach ($data['product_recurring'] as $product_recurring) {
				$this->db->query("INSERT INTO `" . DB_PREFIX . "product_recurring` SET `product_id` = " . (int) $product_id . ", customer_group_id = " . (int) $product_recurring['customer_group_id'] . ", `recurring_id` = " . (int) $product_recurring['recurring_id']);
			}
		}

		$files = glob('../jsonData/*.json'); //get all file names		
		foreach ($files as $file) {
			if (is_file($file))
				unlink($file); //delete file		
		}

		$this->cache->delete('product');
		$this->event->trigger('post.admin.product.edit', $product_id);
	}

	public function copyProduct($product_id)
	{
		$query = $this->db->query("SELECT DISTINCT * FROM " . DB_PREFIX . "product p LEFT JOIN " . DB_PREFIX . "product_description pd ON (p.product_id = pd.product_id) WHERE p.product_id = '" . (int) $product_id . "' AND pd.language_id = '" . (int) $this->config->get('config_language_id') . "'");

		if ($query->num_rows) {
			$data = $query->row;

			$data['sku'] = '';
			$data['upc'] = '';
			$data['viewed'] = '0';
			$data['keyword'] = '';
			$data['status'] = '0';

				$data['approve'] = '0';
			

			$data['product_attribute'] = $this->getProductAttributes($product_id);
			$data['product_description'] = $this->getProductDescriptions($product_id);
			$data['product_discount'] = $this->getProductDiscounts($product_id);
			$data['product_filter'] = $this->getProductFilters($product_id);
			$data['product_image'] = $this->getProductImages($product_id);
			$data['product_option'] = $this->getProductOptions($product_id);
			$data['product_related'] = $this->getProductRelated($product_id);
			$data['product_reward'] = $this->getProductRewards($product_id);
			$data['product_special'] = $this->getProductSpecials($product_id);
			$data['product_feature'] = $this->getProductFeatures($product_id);
			$data['product_category'] = $this->getProductCategories($product_id);
			$data['product_download'] = $this->getProductDownloads($product_id);
			$data['product_layout'] = $this->getProductLayouts($product_id);
			$data['product_store'] = $this->getProductStores($product_id);
			$data['product_recurrings'] = $this->getRecurrings($product_id);

			$this->addProduct($data);
		}
	}

	public function deleteProduct($product_id)
	{
		$this->event->trigger('pre.admin.product.delete', $product_id);

		$this->db->query("DELETE FROM " . DB_PREFIX . "product WHERE product_id = '" . (int) $product_id . "'");
		$this->db->query("DELETE FROM " . DB_PREFIX . "product_attribute WHERE product_id = '" . (int) $product_id . "'");
		$this->db->query("DELETE FROM " . DB_PREFIX . "product_description WHERE product_id = '" . (int) $product_id . "'");
		$this->db->query("DELETE FROM " . DB_PREFIX . "product_discount WHERE product_id = '" . (int) $product_id . "'");
		$this->db->query("DELETE FROM " . DB_PREFIX . "product_filter WHERE product_id = '" . (int) $product_id . "'");
		$this->db->query("DELETE FROM " . DB_PREFIX . "product_image WHERE product_id = '" . (int) $product_id . "'");
		$this->db->query("DELETE FROM " . DB_PREFIX . "product_option WHERE product_id = '" . (int) $product_id . "'");
		$this->db->query("DELETE FROM " . DB_PREFIX . "product_option_value WHERE product_id = '" . (int) $product_id . "'");
		$this->db->query("DELETE FROM " . DB_PREFIX . "product_related WHERE product_id = '" . (int) $product_id . "'");
		$this->db->query("DELETE FROM " . DB_PREFIX . "product_related WHERE related_id = '" . (int) $product_id . "'");
		$this->db->query("DELETE FROM " . DB_PREFIX . "product_reward WHERE product_id = '" . (int) $product_id . "'");
		$this->db->query("DELETE FROM " . DB_PREFIX . "product_special WHERE product_id = '" . (int) $product_id . "'");
		$this->db->query("DELETE FROM " . DB_PREFIX . "product_feature WHERE product_id = '" . (int) $product_id . "'");
		$this->db->query("DELETE FROM " . DB_PREFIX . "product_to_category WHERE product_id = '" . (int) $product_id . "'");
		$this->db->query("DELETE FROM " . DB_PREFIX . "product_to_download WHERE product_id = '" . (int) $product_id . "'");
		$this->db->query("DELETE FROM " . DB_PREFIX . "product_to_layout WHERE product_id = '" . (int) $product_id . "'");
		$this->db->query("DELETE FROM " . DB_PREFIX . "product_to_store WHERE product_id = '" . (int) $product_id . "'");
		$this->db->query("DELETE FROM " . DB_PREFIX . "product_to_area WHERE product_id = '" . (int) $product_id . "'");
		$this->db->query("DELETE FROM " . DB_PREFIX . "product_recurring WHERE product_id = " . (int) $product_id);
		$this->db->query("DELETE FROM " . DB_PREFIX . "review WHERE product_id = '" . (int) $product_id . "'");
		$this->db->query("DELETE FROM " . DB_PREFIX . "url_alias WHERE query = 'product_id=" . (int) $product_id . "'");

		$this->db->query("DELETE FROM " . DB_PREFIX . "seller WHERE vproduct_id = '" . (int) $product_id . "'");
		$this->db->query("DELETE FROM " . DB_PREFIX . "sellers_products WHERE product_id = '" . (int) $product_id . "'");
		$this->db->query("DELETE FROM " . DB_PREFIX . "sellercategories WHERE product_id = '" . (int) $product_id . "'");

		$this->cache->delete('product');

		$this->event->trigger('post.admin.product.delete', $product_id);
	}

	public function getProduct($product_id)
	{
		$query = $this->db->query("SELECT DISTINCT *, (SELECT keyword FROM " . DB_PREFIX . "url_alias WHERE query = 'product_id=" . (int) $product_id . "') AS keyword FROM " . DB_PREFIX . "product p LEFT JOIN " . DB_PREFIX . "product_description pd ON (p.product_id = pd.product_id) WHERE p.product_id = '" . (int) $product_id . "' AND pd.language_id = '" . (int) $this->config->get('config_language_id') . "'");

		return $query->row;
	}


	public function getProducts($data = array())
	{
		$sql = "SELECT * FROM " . DB_PREFIX . "product p LEFT JOIN " . DB_PREFIX . "product_description pd ON (p.product_id = pd.product_id) WHERE pd.language_id = '" . (int) $this->config->get('config_language_id') . "'";

		if (!empty($data['filter_name'])) {
			$sql .= " AND pd.name LIKE '%" . $this->db->escape($data['filter_name']) . "%'";
		}

		if (!empty($data['filter_model'])) {
			$sql .= " AND p.model LIKE '" . $this->db->escape($data['filter_model']) . "%'";
		}


				if (isset($data['filter_seller_name']) && !is_null($data['filter_seller_name'])) {
					$sql .= " AND vps.seller_id = '" . (int)$data['filter_seller_name'] . "'";
				}

				if (isset($data['filter_approve']) && !is_null($data['filter_approve'])) {

					$sql .= " AND p.approve = '" . (int)$data['filter_approve'] . "'";

				}
			
		if (isset($data['filter_price']) && !is_null($data['filter_price'])) {
			$sql .= " AND p.price LIKE '" . $this->db->escape($data['filter_price']) . "%'";
		}

		if (isset($data['filter_quantity']) && !is_null($data['filter_quantity'])) {
			$sql .= " AND p.quantity = '" . (int) $data['filter_quantity'] . "'";
		}

		if (isset($data['filter_status']) && !is_null($data['filter_status'])) {
			$sql .= " AND p.status = '" . (int) $data['filter_status'] . "'";
		}

		if (!empty($data['filter_upc'])) {
			$sql .= " AND p.upc LIKE '" . $this->db->escape($data['filter_upc']) . "%'";
		}

		$sql .= " GROUP BY p.product_id";

		
				$sort_data = array(
				'pd.name',
				'p.model',
				'p.price',
				'p.quantity',
				'p.approve',
				'p.status',
				'p.sort_order'
			);
			








		// if (isset($data['sort']) && in_array($data['sort'], $sort_data)) {
		// $sql .= " ORDER BY " . $data['sort'];
		// } else {
		// $sql .= " ORDER BY pd.name";
		// }

		// if (isset($data['order']) && ($data['order'] == 'DESC')) {
		// $sql .= " DESC";
		// } else {
		// $sql .= " ASC";
		// }

		if (isset($data['start']) || isset($data['limit'])) {
			if ($data['start'] < 0) {
				$data['start'] = 0;
			}

			if ($data['limit'] < 1) {
				$data['limit'] = 20;
			}

			$sql .= " LIMIT " . (int) $data['start'] . "," . (int) $data['limit'];
		}

		$query = $this->db->query($sql);
		// echo'<pre>';
		// print_r($sql);
		// exit;
		return $query->rows;
	}

	public function getProductsBySeller($data = array())
	{
		$sql = "SELECT * FROM " . DB_PREFIX . "product p LEFT JOIN " . DB_PREFIX . "product_description pd ON (p.product_id = pd.product_id) JOIN " . DB_PREFIX . "sellers_products sp ON (p.product_id = sp.product_id) WHERE pd.language_id = '" . (int) $this->config->get('config_language_id') . "'";

		if (!empty($data['filter_name'])) {
			$sql .= " AND pd.name LIKE '%" . $this->db->escape($data['filter_name']) . "%'";
		}

		if (!empty($data['filter_model'])) {
			$sql .= " AND p.model LIKE '" . $this->db->escape($data['filter_model']) . "%'";
		}


				if (isset($data['filter_seller_name']) && !is_null($data['filter_seller_name'])) {
					$sql .= " AND vps.seller_id = '" . (int)$data['filter_seller_name'] . "'";
				}

				if (isset($data['filter_approve']) && !is_null($data['filter_approve'])) {

					$sql .= " AND p.approve = '" . (int)$data['filter_approve'] . "'";

				}
			
		if (isset($data['filter_price']) && !is_null($data['filter_price'])) {
			$sql .= " AND p.price LIKE '" . $this->db->escape($data['filter_price']) . "%'";
		}

		if (isset($data['filter_quantity']) && !is_null($data['filter_quantity'])) {
			$sql .= " AND p.quantity = '" . (int) $data['filter_quantity'] . "'";
		}

		if (isset($data['seller_id']) && !is_null($data['seller_id'])) {
			$sql .= " AND sp.seller_id = '" . (int) $data['seller_id'] . "'";
		}


		if (isset($data['filter_status']) && !is_null($data['filter_status'])) {
			$sql .= " AND p.status = '" . (int) $data['filter_status'] . "'";
		}

		if (!empty($data['filter_upc'])) {
			$sql .= " AND p.upc LIKE '" . $this->db->escape($data['filter_upc']) . "%'";
		}

		$sql .= " GROUP BY p.product_id";

		
				$sort_data = array(
				'pd.name',
				'p.model',
				'p.price',
				'p.quantity',
				'p.approve',
				'p.status',
				'p.sort_order'
			);
			








		if (isset($data['sort']) && in_array($data['sort'], $sort_data)) {
			$sql .= " ORDER BY " . $data['sort'];
		} else {
			$sql .= " ORDER BY pd.name";
		}

		if (isset($data['order']) && ($data['order'] == 'DESC')) {
			$sql .= " DESC";
		} else {
			$sql .= " ASC";
		}

		if (isset($data['start']) || isset($data['limit'])) {
			if ($data['start'] < 0) {
				$data['start'] = 0;
			}

			if ($data['limit'] < 1) {
				$data['limit'] = 20;
			}

			$sql .= " LIMIT " . (int) $data['start'] . "," . (int) $data['limit'];
		}

		$query = $this->db->query($sql);

		return $query->rows;
	}


	public function getProductsByCategoryId($category_id)
	{
		$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "product p LEFT JOIN " . DB_PREFIX . "product_description pd ON (p.product_id = pd.product_id) LEFT JOIN " . DB_PREFIX . "product_to_category p2c ON (p.product_id = p2c.product_id) WHERE pd.language_id = '" . (int) $this->config->get('config_language_id') . "' AND p2c.category_id = '" . (int) $category_id . "' ORDER BY pd.name ASC");

		return $query->rows;
	}

	public function getProductDescriptions($product_id)
	{
		$product_description_data = array();

		$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "product_description WHERE product_id = '" . (int) $product_id . "'");

		foreach ($query->rows as $result) {
			$product_description_data[$result['language_id']] = array(
				'name'             => $result['name'],
				'description'      => $result['description'],
				'meta_title'       => $result['meta_title'],
				'meta_description' => $result['meta_description'],
				'meta_keyword'     => $result['meta_keyword'],
				'tag'              => $result['tag']
			);
		}

		return $product_description_data;
	}
	public function getProductName($product_id)
	{

		$product_name = "";
		$query = $this->db->query("SELECT GROUP_CONCAT(name SEPARATOR ' - ') name FROM " . DB_PREFIX . "product_description  WHERE product_id = '" . (int) $product_id . "' AND language_id IN(1,3) GROUP BY product_id");

		if (count($query->rows) > 0) {
			foreach ($query->rows as $result) {
				$product_name = $result['name'];
			}
		}
		return $product_name;
	}

	public function getProductCategories($product_id)
	{
		$product_category_data = array();

		$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "product_to_category WHERE product_id = '" . (int) $product_id . "'");

		foreach ($query->rows as $result) {
			$product_category_data[] = $result['category_id'];
		}

		return $product_category_data;
	}

	public function getProductCategoriesInfo($product_id)
	{
		$product_category_data = array();
		//$query = $this->db->query("SELECT CONCAT(cd.name,' - ',ocd.name) name FROM " . DB_PREFIX . "product_to_category ptc INNER JOIN " . DB_PREFIX . "category_description cd ON ptc.category_id=cd.category_id LEFT OUTER JOIN " . DB_PREFIX . "category_description ocd ON ptc.category_id=ocd.category_id  WHERE product_id = '" . (int)$product_id . "' AND cd.language_id = 1 AND ocd.language_id = 3");
		$query = $this->db->query("SELECT GROUP_CONCAT(name SEPARATOR ' - ') name FROM " . DB_PREFIX . "product_to_category ptc INNER JOIN " . DB_PREFIX . "category_description cd ON ptc.category_id=cd.category_id WHERE product_id = '" . (int) $product_id . "' AND language_id IN(1,3) GROUP BY cd.category_id");
		foreach ($query->rows as $result) {
			$product_category_data[] = $result;
		}

		return $product_category_data;
	}

	public function getProductFilters($product_id)
	{
		$product_filter_data = array();

		$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "product_filter WHERE product_id = '" . (int) $product_id . "'");

		foreach ($query->rows as $result) {
			$product_filter_data[] = $result['filter_id'];
		}

		return $product_filter_data;
	}

	public function getProductAttributes($product_id)
	{
		$product_attribute_data = array();

		$product_attribute_query = $this->db->query("SELECT attribute_id FROM " . DB_PREFIX . "product_attribute WHERE product_id = '" . (int) $product_id . "' GROUP BY attribute_id");

		foreach ($product_attribute_query->rows as $product_attribute) {
			$product_attribute_description_data = array();

			$product_attribute_description_query = $this->db->query("SELECT * FROM " . DB_PREFIX . "product_attribute WHERE product_id = '" . (int) $product_id . "' AND attribute_id = '" . (int) $product_attribute['attribute_id'] . "'");

			foreach ($product_attribute_description_query->rows as $product_attribute_description) {
				$product_attribute_description_data[$product_attribute_description['language_id']] = array('text' => $product_attribute_description['text']);
			}

			$product_attribute_data[] = array(
				'attribute_id'                  => $product_attribute['attribute_id'],
				'product_attribute_description' => $product_attribute_description_data
			);
		}

		return $product_attribute_data;
	}

	public function getProductOptions($product_id)
	{
		$product_option_data = array();

		$product_option_query = $this->db->query("SELECT * FROM `" . DB_PREFIX . "product_option` po LEFT JOIN `" . DB_PREFIX . "option` o ON (po.option_id = o.option_id) LEFT JOIN `" . DB_PREFIX . "option_description` od ON (o.option_id = od.option_id) WHERE po.product_id = '" . (int) $product_id . "' AND od.language_id = '" . (int) $this->config->get('config_language_id') . "'");

		foreach ($product_option_query->rows as $product_option) {
			$product_option_value_data = array();

			$product_option_value_query = $this->db->query("SELECT * FROM " . DB_PREFIX . "product_option_value WHERE product_option_id = '" . (int) $product_option['product_option_id'] . "'");

			foreach ($product_option_value_query->rows as $product_option_value) {
				$product_option_value_data[] = array(
					'product_option_value_id' => $product_option_value['product_option_value_id'],
					'option_value_id'         => $product_option_value['option_value_id'],
					'quantity'                => $product_option_value['quantity'],
					'subtract'                => $product_option_value['subtract'],
					'price'                   => $product_option_value['price'],
					'price_prefix'            => $product_option_value['price_prefix'],
					'points'                  => $product_option_value['points'],
					'points_prefix'           => $product_option_value['points_prefix'],
					'weight'                  => $product_option_value['weight'],
					'weight_prefix'           => $product_option_value['weight_prefix']
				);
			}

			$product_option_data[] = array(
				'product_option_id'    => $product_option['product_option_id'],
				'product_option_value' => $product_option_value_data,
				'option_id'            => $product_option['option_id'],

				'seller_id'            => $product_option['pseller_id'],
			
				'name'                 => $product_option['name'],
				'type'                 => $product_option['type'],
				'value'                => $product_option['value'],
				'required'             => $product_option['required']
			);
		}

		return $product_option_data;
	}

	public function getProductOptionValue($product_id, $product_option_value_id)
	{
		$query = $this->db->query("SELECT pov.option_value_id, ovd.name, pov.quantity, pov.subtract, pov.price, pov.price_prefix, pov.points, pov.points_prefix, pov.weight, pov.weight_prefix FROM " . DB_PREFIX . "product_option_value pov LEFT JOIN " . DB_PREFIX . "option_value ov ON (pov.option_value_id = ov.option_value_id) LEFT JOIN " . DB_PREFIX . "option_value_description ovd ON (ov.option_value_id = ovd.option_value_id) WHERE pov.product_id = '" . (int) $product_id . "' AND pov.product_option_value_id = '" . (int) $product_option_value_id . "' AND ovd.language_id = '" . (int) $this->config->get('config_language_id') . "'");

		return $query->row;
	}

	public function getProductImages($product_id)
	{
		$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "product_image WHERE product_id = '" . (int) $product_id . "' ORDER BY sort_order ASC");

		return $query->rows;
	}

	public function getProductDiscounts($product_id)
	{
		$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "product_discount WHERE product_id = '" . (int) $product_id . "' ORDER BY quantity, priority, price");

		return $query->rows;
	}

	public function getProductSpecials($product_id)
	{
		$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "product_special WHERE product_id = '" . (int) $product_id . "' ORDER BY priority, price");

		return $query->rows;
	}

	public function getProductFeatures($product_id)
	{
		$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "product_feature WHERE product_id = '" . (int) $product_id . "' ORDER BY priority, price");

		return $query->rows;
	}

	public function getProductRewards($product_id)
	{
		$product_reward_data = array();

		$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "product_reward WHERE product_id = '" . (int) $product_id . "'");

		foreach ($query->rows as $result) {
			$product_reward_data[$result['customer_group_id']] = array('points' => $result['points']);
		}

		return $product_reward_data;
	}

	public function getProductDownloads($product_id)
	{
		$product_download_data = array();

		$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "product_to_download WHERE product_id = '" . (int) $product_id . "'");

		foreach ($query->rows as $result) {
			$product_download_data[] = $result['download_id'];
		}

		return $product_download_data;
	}

	public function getProductStores($product_id)
	{
		$product_store_data = array();

		$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "product_to_store WHERE product_id = '" . (int) $product_id . "'");

		foreach ($query->rows as $result) {
			$product_store_data[] = $result['store_id'];
		}

		return $product_store_data;
	}

	public function getProductAreas($product_id)
	{
		$product_area_data = array();

		$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "product_to_area WHERE product_id = '" . (int) $product_id . "'");

		foreach ($query->rows as $result) {
			$product_area_data[] = $result['area_id'];
		}

		return $product_area_data;
	}

	public function getProductLayouts($product_id)
	{
		$product_layout_data = array();

		$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "product_to_layout WHERE product_id = '" . (int) $product_id . "'");

		foreach ($query->rows as $result) {
			$product_layout_data[$result['store_id']] = $result['layout_id'];
		}

		return $product_layout_data;
	}

	public function getProductRelated($product_id)
	{
		$product_related_data = array();

		$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "product_related WHERE product_id = '" . (int) $product_id . "'");

		foreach ($query->rows as $result) {
			$product_related_data[] = $result['related_id'];
		}

		return $product_related_data;
	}

	public function getRecurrings($product_id)
	{
		$query = $this->db->query("SELECT * FROM `" . DB_PREFIX . "product_recurring` WHERE product_id = '" . (int) $product_id . "'");

		return $query->rows;
	}

	public function getTotalProducts($data = array())
	{
								     				
		$sql = "SELECT  COUNT(DISTINCT p.product_id) AS total				
		FROM " . DB_PREFIX . "product p LEFT JOIN " . DB_PREFIX . "product_description pd ON (p.product_id = pd.product_id) 	
					
		LEFT JOIN " . DB_PREFIX . "sellers vds ON (p.seller_id = vds.seller_id) 
		LEFT JOIN " . DB_PREFIX . "sellers_products vps ON (p.product_id = vps.product_id)  ";		
		

		$sql .= " WHERE pd.language_id = '" . (int) $this->config->get('config_language_id') . "'";

		if (!empty($data['filter_name'])) {
			$sql .= " AND pd.name LIKE '%" . $this->db->escape($data['filter_name']) . "%'";
		}

		if (!empty($data['filter_model'])) {
			$sql .= " AND p.model LIKE '" . $this->db->escape($data['filter_model']) . "%'";
		}


				if (isset($data['filter_seller_name']) && !is_null($data['filter_seller_name'])) {
					$sql .= " AND vps.seller_id = '" . (int)$data['filter_seller_name'] . "'";
				}

				if (isset($data['filter_approve']) && !is_null($data['filter_approve'])) {

					$sql .= " AND p.approve = '" . (int)$data['filter_approve'] . "'";

				}
			
		if (isset($data['filter_price']) && !is_null($data['filter_price'])) {
			$sql .= " AND p.price LIKE '" . $this->db->escape($data['filter_price']) . "%'";
		}

		if (isset($data['filter_quantity']) && !is_null($data['filter_quantity'])) {
			$sql .= " AND p.quantity = '" . (int) $data['filter_quantity'] . "'";
		}

		if (isset($data['filter_status']) && !is_null($data['filter_status'])) {
			$sql .= " AND p.status = '" . (int) $data['filter_status'] . "'";
		}

		if (!empty($data['filter_upc'])) {
			$sql .= " AND p.upc LIKE '" . $this->db->escape($data['filter_upc']) . "%'";
		}

		$query = $this->db->query($sql);

		return $query->row['total'];
	}

	public function getTotalProductsByTaxClassId($tax_class_id)
	{
		$query = $this->db->query("SELECT COUNT(*) AS total FROM " . DB_PREFIX . "product WHERE tax_class_id = '" . (int) $tax_class_id . "'");

		return $query->row['total'];
	}

	public function getTotalProductsByStockStatusId($stock_status_id)
	{
		$query = $this->db->query("SELECT COUNT(*) AS total FROM " . DB_PREFIX . "product WHERE stock_status_id = '" . (int) $stock_status_id . "'");

		return $query->row['total'];
	}

	public function getTotalProductsByWeightClassId($weight_class_id)
	{
		$query = $this->db->query("SELECT COUNT(*) AS total FROM " . DB_PREFIX . "product WHERE weight_class_id = '" . (int) $weight_class_id . "'");

		return $query->row['total'];
	}

	public function getTotalProductsByLengthClassId($length_class_id)
	{
		$query = $this->db->query("SELECT COUNT(*) AS total FROM " . DB_PREFIX . "product WHERE length_class_id = '" . (int) $length_class_id . "'");

		return $query->row['total'];
	}

	public function getTotalProductsByDownloadId($download_id)
	{
		$query = $this->db->query("SELECT COUNT(*) AS total FROM " . DB_PREFIX . "product_to_download WHERE download_id = '" . (int) $download_id . "'");

		return $query->row['total'];
	}

	public function getTotalProductsByManufacturerId($manufacturer_id)
	{
		$query = $this->db->query("SELECT COUNT(*) AS total FROM " . DB_PREFIX . "product WHERE manufacturer_id = '" . (int) $manufacturer_id . "'");

		return $query->row['total'];
	}

	public function getTotalProductsByAttributeId($attribute_id)
	{
		$query = $this->db->query("SELECT COUNT(*) AS total FROM " . DB_PREFIX . "product_attribute WHERE attribute_id = '" . (int) $attribute_id . "'");

		return $query->row['total'];
	}

	public function getTotalProductsByOptionId($option_id)
	{
		$query = $this->db->query("SELECT COUNT(*) AS total FROM " . DB_PREFIX . "product_option WHERE option_id = '" . (int) $option_id . "'");

		return $query->row['total'];
	}

	public function getTotalProductsByProfileId($recurring_id)
	{
		$query = $this->db->query("SELECT COUNT(*) AS total FROM " . DB_PREFIX . "product_recurring WHERE recurring_id = '" . (int) $recurring_id . "'");

		return $query->row['total'];
	}

	public function getTotalProductsByLayoutId($layout_id)
	{
		$query = $this->db->query("SELECT COUNT(*) AS total FROM " . DB_PREFIX . "product_to_layout WHERE layout_id = '" . (int) $layout_id . "'");

		return $query->row['total'];
	}

	public function getPendingBarCodes()
	{

		$query = $this->db->query("SELECT pu.*,pd.name,p.upc as upc_current FROM " . DB_PREFIX . "product_upc pu LEFT JOIN " . DB_PREFIX . "product p ON(pu.product_id=p.product_id) LEFT JOIN " . DB_PREFIX . "product_description pd ON(p.product_id=pd.product_id)  WHERE pu.status=0 AND pd.language_id=1  ORDER BY pu.date_added DESC");

		return $query->rows;
	}

	public function acceptBarCode($data)
	{

		$upcQuery = $this->db->query("SELECT * FROM " . DB_PREFIX . "product_upc WHERE id='" . $data['id'] . "'");

		$upc = $upcQuery->row;
		$loggedInUserId = $this->user->isLogged();
		if (isset($upc)) {
			$query = $this->db->query("UPDATE " . DB_PREFIX . "product SET upc='" . $upc['upc'] . "'  WHERE product_id='" . $upc['product_id'] . "'");
			$query = $this->db->query("UPDATE " . DB_PREFIX . "product_upc SET status='1',approved_by='" . $loggedInUserId . "'  WHERE id='" . $data['id'] . "'");
			return 'success';
		}
		return 'failed';
	}

	public function rejectBarCode($data)
	{
		$loggedInUserId = $this->user->isLogged();

		$query = $this->db->query("UPDATE " . DB_PREFIX . "product_upc SET status='2',approved_by='" . $loggedInUserId . "'  WHERE id='" . $data['id'] . "'");
		return 'success';
	}

	public function getProductSellers1($product_id)
	{
		$query = $this->db->query("SELECT s.firstname, s.lastname FROM " . DB_PREFIX . "sellers_products as sp," . DB_PREFIX . "sellers as s WHERE sp.product_id = '" . (int) $product_id . "' AND s.seller_id=sp.seller_id");
		$data = array();
		if (count($query->rows) > 0) {
			foreach ($query->rows as $rows) {
				$data[] = $rows;
			}
		}
		return $data;
	}

	public function getTotalProductsByUpc($upc)
	{
		$query = $this->db->query("SELECT COUNT(*) AS total FROM " . DB_PREFIX . "product WHERE upc = '" . $this->db->escape($upc) . "'");

		return $query->row['total'];
	}

	public function getTotalProductsByUpcInProduct($upc, $product_id)
	{
		$query = $this->db->query("SELECT COUNT(*) AS total FROM " . DB_PREFIX . "product WHERE upc = '" . $this->db->escape($upc) . "' AND product_id != '" . $product_id . "'");

		return $query->row['total'];
	}
}
