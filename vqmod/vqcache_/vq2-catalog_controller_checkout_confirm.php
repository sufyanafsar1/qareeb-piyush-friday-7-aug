<?php
class ControllerCheckoutConfirm extends Controller {
	public function index() {
		
		$redirect = '';
$this->load->language('checkout/confirm');
		if ($this->cart->hasShipping()) {
			// Validate if shipping address has been set.
			if (!isset($this->session->data['shipping_address'])) {
				$redirect = $this->url->link('checkout/checkout', '', 'SSL');
			}

			// Validate if shipping method has been set.
			if (!isset($this->session->data['shipping_method'])) {
				$redirect = $this->url->link('checkout/checkout', '', 'SSL');
			}
		} else {
			unset($this->session->data['shipping_address']);
			unset($this->session->data['shipping_method']);
			unset($this->session->data['shipping_methods']);
		}

		// Validate if payment address has been set.
		if (!isset($this->session->data['payment_address'])) {
			$redirect = $this->url->link('checkout/checkout', '', 'SSL');
		}

		// Validate if payment method has been set.
		if (!isset($this->session->data['payment_method'])) {
			$redirect = $this->url->link('checkout/checkout', '', 'SSL');
		}

		// Validate cart has products and has stock.
		if ((!$this->cart->hasProducts() && empty($this->session->data['vouchers'])) || (!$this->cart->hasStock() && !$this->config->get('config_stock_checkout'))) {
			$redirect = $this->url->link('checkout/cart');
		}

		// Validate minimum quantity requirements.
		$products = $this->cart->getProducts();

		foreach ($products as $product) {
			$product_total = 0;

			foreach ($products as $product_2) {
				if ($product_2['product_id'] == $product['product_id']) {
					$product_total += $product_2['quantity'];
				}
			}

			if ($product['minimum'] > $product_total) {
				$redirect = $this->url->link('checkout/cart');

				break;
			}
		}

		if (!$redirect) {
			$order_data = array();

			$order_data['totals'] = array();
			$total = 0;
			$taxes = $this->cart->getTaxes();
			
			$this->load->model('extension/extension');

			$sort_order = array();

			$results = $this->model_extension_extension->getExtensions('total');
			
			foreach ($results as $key => $value) {
				$sort_order[$key] = $this->config->get($value['code'] . '_sort_order');
			}

			array_multisort($sort_order, SORT_ASC, $results);

			foreach ($results as $result) {
				if ($this->config->get($result['code'] . '_status')) {
					$this->load->model('total/' . $result['code']);

					$this->{'model_total_' . $result['code']}->getTotal($order_data['totals'], $total, $taxes);
				}
			}
			$vat =  (float)$this->config->get('config_vat');
			if(isset($vat) && $vat > 0){
               $this->load->model('total/vat');
                $this->{'model_total_vat'}->getTotal($order_data['totals'], $vat);
            }
			$sort_order = array();

			foreach ($order_data['totals'] as $key => $value) {
				if($value['code'] =='shipping')
					unset($order_data['totals'][$key]);
				$sort_order[$key] = $value['sort_order'];
			}
			
			array_multisort($sort_order, SORT_ASC, $order_data['totals']);

			$this->load->language('checkout/checkout');

			$order_data['invoice_prefix'] = $this->config->get('config_invoice_prefix');
			$order_data['store_id'] = $this->config->get('config_store_id');
			$order_data['store_name'] = $this->config->get('config_name');

			if ($order_data['store_id']) {
				$order_data['store_url'] = $this->config->get('config_url');
			} else {
				$order_data['store_url'] = HTTP_SERVER;
			}

			if ($this->customer->isLogged()) {
				$this->load->model('account/customer');

				$customer_info = $this->model_account_customer->getCustomer($this->customer->getId());

				$order_data['customer_id'] = $this->customer->getId();
				$order_data['customer_group_id'] = $customer_info['customer_group_id'];
				$order_data['firstname'] = $customer_info['firstname'];
				$order_data['lastname'] = $customer_info['lastname'];
				$order_data['email'] = $customer_info['email'];
				$order_data['telephone'] = $customer_info['telephone'];
				$order_data['fax'] = $customer_info['fax'];
				$order_data['custom_field'] = json_decode($customer_info['custom_field'], true);
			} elseif (isset($this->session->data['guest'])) {
				$order_data['customer_id'] = 0;
				$order_data['customer_group_id'] = $this->session->data['guest']['customer_group_id'];
				$order_data['firstname'] = $this->session->data['guest']['firstname'];
				$order_data['lastname'] = $this->session->data['guest']['lastname'];
				$order_data['email'] = $this->session->data['guest']['email'];
				$order_data['telephone'] = $this->session->data['guest']['telephone'];
				$order_data['fax'] = $this->session->data['guest']['fax'];
				$order_data['custom_field'] = $this->session->data['guest']['custom_field'];
			}

			$order_data['payment_firstname'] = $this->session->data['payment_address']['firstname'];
			$order_data['payment_lastname'] = $this->session->data['payment_address']['lastname'];
			$order_data['payment_company'] = $this->session->data['payment_address']['company'];
			$order_data['payment_address_1'] = $this->session->data['payment_address']['address_1'];
			$order_data['payment_address_2'] = $this->session->data['payment_address']['address_2'];
			$order_data['payment_city'] = $this->session->data['payment_address']['city'];
			$order_data['payment_postcode'] = $this->session->data['payment_address']['postcode'];
			$order_data['payment_zone'] = $this->session->data['payment_address']['zone'];
			$order_data['payment_zone_id'] = $this->session->data['payment_address']['zone_id'];
			$order_data['payment_country'] = $this->session->data['payment_address']['country'];
			$order_data['payment_country_id'] = $this->session->data['payment_address']['country_id'];
			$order_data['payment_address_format'] = $this->session->data['payment_address']['address_format'];
			$order_data['payment_custom_field'] = (isset($this->session->data['payment_address']['custom_field']) ? $this->session->data['payment_address']['custom_field'] : array());

			if (isset($this->session->data['payment_method']['title'])) {
				$order_data['payment_method'] = $this->session->data['payment_method']['title'];
			} else {
				$order_data['payment_method'] = '';
			}

			if (isset($this->session->data['payment_method']['code'])) {
				$order_data['payment_code'] = $this->session->data['payment_method']['code'];
			} else {
				$order_data['payment_code'] = '';
			}

			if ($this->cart->hasShipping()) {
				$order_data['shipping_firstname'] = $this->session->data['shipping_address']['firstname'];
				$order_data['shipping_lastname'] = $this->session->data['shipping_address']['lastname'];
				$order_data['shipping_company'] = $this->session->data['shipping_address']['company'];
				$order_data['shipping_address_1'] = $this->session->data['shipping_address']['address_1'];
				$order_data['shipping_address_2'] = $this->session->data['shipping_address']['address_2'];
				$order_data['shipping_city'] = $this->session->data['shipping_address']['city'];
				$order_data['shipping_postcode'] = $this->session->data['shipping_address']['postcode'];
				$order_data['shipping_zone'] = $this->session->data['shipping_address']['zone'];
				$order_data['shipping_zone_id'] = $this->session->data['shipping_address']['zone_id'];
				$order_data['shipping_country'] = $this->session->data['shipping_address']['country'];
				$order_data['shipping_country_id'] = $this->session->data['shipping_address']['country_id'];
				$order_data['shipping_address_format'] = $this->session->data['shipping_address']['address_format'];
				$order_data['shipping_custom_field'] = (isset($this->session->data['shipping_address']['custom_field']) ? $this->session->data['shipping_address']['custom_field'] : array());

				if (isset($this->session->data['shipping_method']['title'])) {
					$order_data['shipping_method'] = $this->session->data['shipping_method']['title'];
				} else {
					$order_data['shipping_method'] = '';
				}

				if (isset($this->session->data['shipping_method']['code'])) {
					$order_data['shipping_code'] = $this->session->data['shipping_method']['code'];
				} else {
					$order_data['shipping_code'] = '';
				}
			} else {
				$order_data['shipping_firstname'] = '';
				$order_data['shipping_lastname'] = '';
				$order_data['shipping_company'] = '';
				$order_data['shipping_address_1'] = '';
				$order_data['shipping_address_2'] = '';
				$order_data['shipping_city'] = '';
				$order_data['shipping_postcode'] = '';
				$order_data['shipping_zone'] = '';
				$order_data['shipping_zone_id'] = '';
				$order_data['shipping_country'] = '';
				$order_data['shipping_country_id'] = '';
				$order_data['shipping_address_format'] = '';
				$order_data['shipping_custom_field'] = array();
				$order_data['shipping_method'] = '';
				$order_data['shipping_code'] = '';
			}

			$order_data['products'] = array();

			foreach ($this->cart->getProductsV2() as $product) {
				$option_data = array();
				foreach ($product['option'] as $option) {
					$option_data[] = array(
						'product_option_id'       => $option['product_option_id'],
						'product_option_value_id' => $option['product_option_value_id'],
						'option_id'               => $option['option_id'],
						'option_value_id'         => $option['option_value_id'],
						'name'                    => $option['name'],
						'value'                   => $option['value'],
						'type'                    => $option['type']
					);
				}

				$order_data['products'][] = array(

				'seller_id' => $product['seller_id'],
			
					'product_id' => $product['product_id'],
					'name'       => $product['name'],
					'model'      => $product['model'],
					'option'     => $option_data,
					'download'   => $product['download'],
					'quantity'   => $product['quantity'],
					'subtract'   => $product['subtract'],
					'seller_id'  => $product['seller_id'],
					'price'      => $product['price'],
					'total'      => $product['total'],
					'tax'        => $this->tax->getTax($product['price'], $product['tax_class_id']),
					'reward'     => $product['reward']
				);
			}

			// Gift Voucher
			$order_data['vouchers'] = array();

			if (!empty($this->session->data['vouchers'])) {
				foreach ($this->session->data['vouchers'] as $voucher) {
					$order_data['vouchers'][] = array(
						'description'      => $voucher['description'],
						'code'             => token(10),
						'to_name'          => $voucher['to_name'],
						'to_email'         => $voucher['to_email'],
						'from_name'        => $voucher['from_name'],
						'from_email'       => $voucher['from_email'],
						'voucher_theme_id' => $voucher['voucher_theme_id'],
						'message'          => $voucher['message'],
						'amount'           => $voucher['amount']
					);
				}
			}

			$order_data['comment'] = $this->session->data['comment'];
			$order_data['total'] = $total;

			if (isset($this->request->cookie['tracking'])) {
				$order_data['tracking'] = $this->request->cookie['tracking'];

				$subtotal = $this->cart->getSubTotal();

				// Affiliate
				$this->load->model('affiliate/affiliate');

				$affiliate_info = $this->model_affiliate_affiliate->getAffiliateByCode($this->request->cookie['tracking']);

				if ($affiliate_info) {
					$order_data['affiliate_id'] = $affiliate_info['affiliate_id'];
					$order_data['commission'] = ($subtotal / 100) * $affiliate_info['commission'];
				} else {
					$order_data['affiliate_id'] = 0;
					$order_data['commission'] = 0;
				}

				// Marketing
				$this->load->model('checkout/marketing');

				$marketing_info = $this->model_checkout_marketing->getMarketingByCode($this->request->cookie['tracking']);

				if ($marketing_info) {
					$order_data['marketing_id'] = $marketing_info['marketing_id'];
				} else {
					$order_data['marketing_id'] = 0;
				}
			} else {
				$order_data['affiliate_id'] = 0;
				$order_data['commission'] = 0;
				$order_data['marketing_id'] = 0;
				$order_data['tracking'] = '';
			}

			$order_data['language_id'] = $this->config->get('config_language_id');
			$order_data['currency_id'] = $this->currency->getId();
			$order_data['currency_code'] = $this->currency->getCode();
			$order_data['currency_value'] = $this->currency->getValue($this->currency->getCode());
			$order_data['ip'] = $this->request->server['REMOTE_ADDR'];

			if (!empty($this->request->server['HTTP_X_FORWARDED_FOR'])) {
				$order_data['forwarded_ip'] = $this->request->server['HTTP_X_FORWARDED_FOR'];
			} elseif (!empty($this->request->server['HTTP_CLIENT_IP'])) {
				$order_data['forwarded_ip'] = $this->request->server['HTTP_CLIENT_IP'];
			} else {
				$order_data['forwarded_ip'] = '';
			}

			if (isset($this->request->server['HTTP_USER_AGENT'])) {
				$order_data['user_agent'] = $this->request->server['HTTP_USER_AGENT'];
			} else {
				$order_data['user_agent'] = '';
			}

			if (isset($this->request->server['HTTP_ACCEPT_LANGUAGE'])) {
				$order_data['accept_language'] = $this->request->server['HTTP_ACCEPT_LANGUAGE'];
			} else {
				$order_data['accept_language'] = '';
			}

			$this->load->model('checkout/order');
					
///add			
			$sellers = $this->cart->getSellers($order_data['customer_id']);
			$data['sellers'] = array();
			 $this->load->model('store/store');
			
			$allproducts = $this->cart->getProducts();
			 $orders_id =[];
			foreach ($sellers as $seller) {
			
				$sellerInfo = $this->model_store_store->getSellerByID($seller['seller_id']);
				
				if(isset($sellerInfo['minimum_order'])){
					$minimumOrderLimit = $sellerInfo['minimum_order'];
				}
				$minimumOrder = $this->currency->format($minimumOrderLimit);

				if(isset($sellerInfo)){
					if(isset($sellerInfo['firstname'])){
						$data['sellers'][$seller['seller_id']]['text_store'] = $sellerInfo['firstname'];
					} else {
						$data['sellers'][$seller['seller_id']]['text_store'] = $this->language->get('text_store');
					}
				} else {
					$data['sellers'][$seller['seller_id']]['text_store'] = $this->language->get('text_store');
				}
				$data['sellers'][$seller['seller_id']]['text_minimum_order'] = $this->language->get('text_minimum_order').' '.$minimumOrder;
				$data['sellers'][$seller['seller_id']]['delivery_charges'] = $sellerInfo['delivery_charges'];
				$data['sellers'][$seller['seller_id']]['free_delivery_limit'] = $sellerInfo['free_delivery_limit'];
				
			//adda oreder
				$producttotals = 0;
				$itemtotal = 0;
				foreach($allproducts as $product){
					if($seller['seller_id'] == $product['seller_id']){
						$producttotals += $product['total'];	
						$itemtotal ++;
					}
				}
				$data['sellers'][$seller['seller_id']]['producttotals'] = $producttotals;
				$data['sellers'][$seller['seller_id']]['itemtotal'] = $itemtotal;
				
				$order_data['seller_id'] = $seller['seller_id'];
				$order_data['total'] = $producttotals;
				
				 $order_id= $this->model_checkout_order->addOrder($order_data);
				 $this->session->data['order_id']= $order_id;
				 $orders_id[] = $order_id;
			}
			 $this->session->data['orders_id'] =$orders_id;
			
			
///add			
			/* foreach ($this->cart->getProducts() as $product) {
			
				$order_data['seller_id'] = $product['seller_id'];
				$order_data['total'] = $product['total'];
				
			// $this->session->data['order_id'] = $this->model_checkout_order->addOrder($order_data);
			} */
			   
									
			$this->load->model('checkout/order');

			$order_info = $this->model_checkout_order->getOrder($this->session->data['order_id']);
			
			/* $order_total_query = $this->db->query("SELECT * FROM `" . DB_PREFIX . "order_total` WHERE order_id = '" . (int)$this->session->data['order_id'] . "' ORDER BY sort_order ASC");

			foreach ($order_total_query->rows as $order_total) {
				$this->load->model('total/' . $order_total['code']);

				if (method_exists($this->{'model_total_' . $order_total['code']}, 'confirm')) {
					$this->{'model_total_' . $order_total['code']}->confirm($order_info, $order_total);
				}
			} */
			
			/*
			 * Added on Oct 15, 2016
			 */
		  
			// $this->load->model('module/deliveryTime/order');
			// $this->model_module_deliveryTime_order->addDeliveryTime($this->session->data['order_id'],$this->session->data['delivery_time']);
			//$this->model_module_deliveryTime_order->addOrderHistory($this->session->data['order_id'], $this->config->get('cod_order_status_id'));
                        

			$data['text_recurring_item'] = $this->language->get('text_recurring_item');
			$data['text_payment_recurring'] = $this->language->get('text_payment_recurring');

			$data['column_name'] = $this->language->get('column_name');
			$data['column_model'] = $this->language->get('column_model');
			$data['column_quantity'] = $this->language->get('column_quantity');
			$data['column_price'] = $this->language->get('column_price');
			$data['column_total'] = $this->language->get('column_total');
            
            $data['text_store'] = $this->language->get('text_store');
            $data['text_delivery_charges'] = $this->language->get('text_delivery_charges');
            $data['text_delivery_time'] = $this->language->get('text_delivery_time');

			$this->load->model('tool/upload');

			$data['products'] = array();
//echo '<pre>'; print_r($this->cart->getProducts());exit;
            $productIdArr = [];
			foreach ($this->cart->getProducts() as $product) {
				
				$sellerIdArr[] = $product['seller_id'];
				
				$option_data = array();

				foreach ($product['option'] as $option) {
					if ($option['type'] != 'file') {
						$value = $option['value'];
					} else {
						$upload_info = $this->model_tool_upload->getUploadByCode($option['value']);

						if ($upload_info) {
							$value = $upload_info['name'];
						} else {
							$value = '';
						}
					}

					$option_data[] = array(
						'name'  => $option['name'],
						'value' => (utf8_strlen($value) > 20 ? utf8_substr($value, 0, 20) . '..' : $value)
					);
				}

				$recurring = '';

				if ($product['recurring']) {
					$frequencies = array(
						'day'        => $this->language->get('text_day'),
						'week'       => $this->language->get('text_week'),
						'semi_month' => $this->language->get('text_semi_month'),
						'month'      => $this->language->get('text_month'),
						'year'       => $this->language->get('text_year'),
					);

					if ($product['recurring']['trial']) {
						$recurring = sprintf($this->language->get('text_trial_description'), $this->currency->format($this->tax->calculate($product['recurring']['trial_price'] * $product['quantity'], $product['tax_class_id'], $this->config->get('config_tax'))), $product['recurring']['trial_cycle'], $frequencies[$product['recurring']['trial_frequency']], $product['recurring']['trial_duration']) . ' ';
					}

					if ($product['recurring']['duration']) {
						$recurring .= sprintf($this->language->get('text_payment_description'), $this->currency->format($this->tax->calculate($product['recurring']['price'] * $product['quantity'], $product['tax_class_id'], $this->config->get('config_tax'))), $product['recurring']['cycle'], $frequencies[$product['recurring']['frequency']], $product['recurring']['duration']);
					} else {
						$recurring .= sprintf($this->language->get('text_payment_cancel'), $this->currency->format($this->tax->calculate($product['recurring']['price'] * $product['quantity'], $product['tax_class_id'], $this->config->get('config_tax'))), $product['recurring']['cycle'], $frequencies[$product['recurring']['frequency']], $product['recurring']['duration']);
					}
				}
		//echo'<pre>'; print_r($product); exit;

			$data['text_seller'] = $this->language->get('text_seller');
				
			
				$data['products'][] = array(

			
				'seller_id' => $product['seller_id'],
				'sellername'=>$product['seller'],
				'sellerhref'=> $this->url->link('product/seller', 'seller_id=' . $product['seller_id']),
			
					'cart_id'    => $product['cart_id'],
					'product_id' => $product['product_id'],
					'name'       => $product['name'].' <br/>'.$this->language->get('text_seller'). ' '.$product['seller'],
					'model'      => $product['model'],
					'option'     => $option_data,
					'recurring'  => $recurring,
					'quantity'   => $product['quantity'],
					'subtract'   => $product['subtract'],
					'seller_id'   => $product['seller_id'],
					'price'      => $this->currency->format($this->tax->calculate($product['price'], $product['tax_class_id'], $this->config->get('config_tax'))),
					'total'      => $this->currency->format($this->tax->calculate($product['price'], $product['tax_class_id'], $this->config->get('config_tax')) * $product['quantity']),
					
				'href'     => $this->url->link('product/product', 'seller_id=' . $product['seller_id'].'&product_id='.$product['product_id']),
			
				);
			}


            /*$orderId = $this->session->data['order_id'];
			$productIdArr = $this->db->query("select oc_order_product.product_id 
				                  from oc_order 
				                  join oc_order_product on oc_order_product.order_id = oc_order.order_id 
				                  where oc_order.order_id='$orderId'")->rows;
             $productIdArr =  array_column($productIdArr, 'product_id');*/
             //var_dump($productIdArr);exit;
             $sellerIdArr = array_unique( $sellerIdArr );
             $sellerIdStr =  implode(',', $sellerIdArr);
             
            /* $deliveryChargesArr = $this->db->query("
					                  select oc_sellers.delivery_charges, oc_sellers.seller_id, oc_sellers.firstname, oc_sellers.lastname 
					                  from oc_product 
					                  join oc_sellers on oc_sellers.seller_id = oc_product.product_id 
					                  in ( $productIdStr ) group by oc_sellers.seller_id")->rows; */
            $deliveryChargesArr = $this->db->query("
					                  select oc_sellers.delivery_charges, oc_sellers.seller_id, oc_sellers.free_delivery_limit, oc_sellers.firstname, oc_sellers.lastname 
					                  from oc_sellers 
					                where oc_sellers.seller_id in ( $sellerIdStr )")->rows;
			 //$deliveryChargesArr =  array_column($deliveryChargesArr, 'delivery_charges');
			 //$deliveryCharges = array_sum($deliveryChargesArr);
//var_dump($deliveryChargesArr);exit;
			// Gift Voucher
			$data['vouchers'] = array();

			if (!empty($this->session->data['vouchers'])) {
				foreach ($this->session->data['vouchers'] as $voucher) {
					$data['vouchers'][] = array(
						'description' => $voucher['description'],
						'amount'      => $this->currency->format($voucher['amount'])
					);
				}
			}

			
			foreach ($order_data['totals'] as $total) {
				
				if( $total['code'] == 'vat' ){
					$deliveryCharges = 0;
					foreach( $deliveryChargesArr as $deliveryChargesRow ){
						if($data['sellers'][$deliveryChargesRow['seller_id']]['producttotals'] <  $deliveryChargesRow['free_delivery_limit']){
							$deliveryCharges += $deliveryChargesRow['delivery_charges'];
						}
					}
					$data['totals'][] = array(
							'title' => $this->language->get('text_delivery_charges'),
							'text'  => $this->currency->format( $deliveryCharges ),
				        );
					$this->session->data['deliveryChargesArr'] = $deliveryChargesArr;
					//$total['value'] = $total['value'] + $deliveryCharges;
				}
				
				$data['totals'][] = array(
					'title' => $total['title'],
					'text'  => $this->currency->format($total['value']),
				);
			}

			$data['payment'] = $this->load->controller('payment/' . $this->session->data['payment_method']['code']);
		} else {
			$data['redirect'] = $redirect;
		}
		
		
//var_dump($this->session->data['deliveryChargesArr']);exit;
		$products = $this->cart->getProducts();
		
		
		
		$this->load->model('catalog/seller');
		$seller_time = $this->model_catalog_seller->getSeller($products[0]['seller_id']);
		$data['seller_name'] = $seller_time['name'];
		
                            
                         $this->load->model('slot/timing');

			$extension = $this->model_slot_timing->getInstalled("module");     
                        
                           
                            
                        if (!in_array("timeslot", $extension)) {    
                            
                                $template = "confirm";     
                        $this->template = 'default/template/checkout/'.$template.'.tpl';
			$this->response->setOutput($this->load->view($this->template, $data));   
                            
                             
                            
                            
                        }else{
                        
                    
                         $data['timing'] = $this->session->data['delivery_time'];  
                        $template = "confirm";     
                        $this->template = 'default/template/module/timeslot/'.$template.'.tpl';
			$this->response->setOutput($this->load->view($this->template, $data));
                           
                        
                        }        
                        




		
	}
}
