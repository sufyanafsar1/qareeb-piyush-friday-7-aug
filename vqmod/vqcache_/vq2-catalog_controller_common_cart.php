<?php
class ControllerCommonCart extends Controller {
	public function index() {
		
		require_once 'system/Mobile_Detect.php';
		$this->load->language('common/cart');

		// Totals
		$this->load->model('extension/extension');
        $this->load->model('store/store');

		$total_data = array();
		$total = 0;
		$taxes = $this->cart->getTaxes();
        
        if (isset($this->request->get['seller_id'])) {
			$sId = $this->request->get['seller_id'];
		} else if(isset($this->session->data['sellerId'])){
			$sId = $this->session->data['sellerId'];
		} else {
			//$sId = $_SESSION['storeID'];
					
			if(isset($this->session->data['storeID'])){
				$sId = $this->session->data['storeID'];
			} else {
				$sId = 0;
			}
		}
	
		$minimumOrderLimit = 0;
		$minimumOrder = 0;
		
		//get Seller Information
        $sellerInfo = $this->model_store_store->getSellerByID($sId);
		
		/* echo"<pre>";
		print_r($sellerInfo);
		exit; */
		$data['free_delivery_limit'] = $sellerInfo['free_delivery_limit']; 
		if(isset($sellerInfo['minimum_order'])){
			$minimumOrderLimit = $sellerInfo['minimum_order'];
		}
		$minimumOrder = $this->currency->format($minimumOrderLimit);

		if(isset($sellerInfo)){
			if(isset($sellerInfo['firstname'])){
				$data['text_store'] = $sellerInfo['firstname'];
			} else {
				$data['text_store'] = $this->language->get('text_store');
			}
		} else {
			$data['text_store'] = $this->language->get('text_store');
		}
		
		
        // Display prices
		if (($this->config->get('config_customer_price') && $this->customer->isLogged()) || !$this->config->get('config_customer_price')) {
			$sort_order = array();

			$results = $this->model_extension_extension->getExtensions('total');
			
			foreach ($results as $key => $value) {
				$sort_order[$key] = $this->config->get($value['code'] . '_sort_order');
			}

			array_multisort($sort_order, SORT_ASC, $results);

			foreach ($results as $result) {
			   if($result['code']=="sub_total" || $result['code']=="coupon")
               {
                
              
				if ($this->config->get($result['code'] . '_status')) {
					$this->load->model('total/' . $result['code']);

				//	$this->{'model_total_' . $result['code']}->getTotal($total_data, $total, $taxes);
                    $this->{'model_total_' . $result['code']}->getTotalV2($total_data, $total, $taxes,$sId);
				}
                }
			}

			$sort_order = array();

			foreach ($total_data as $key => $value) {
				$sort_order[$key] = $value['sort_order'];
			}

			array_multisort($sort_order, SORT_ASC, $total_data);
		}
		
         $files = glob(DIR_APPLICATION . '/controller/total/coupon_popup.php');

			if ($files) {
				foreach ($files as $file) {
					$extension = basename($file, '.php');

					$data[$extension] = $this->load->controller('total/' . $extension);
				}
			}
		$data['text_minimum_order'] = $this->language->get('text_minimum_order').$minimumOrder;
	
		$data['text_empty'] = $this->language->get('text_empty');
		$data['text_cart'] = $this->language->get('text_cart');
		$data['text_checkout'] = $this->language->get('text_checkout');
		$data['text_recurring'] = $this->language->get('text_recurring');
		//$data['text_items'] = '<img src="catalog/view/theme/careeb/image/cart-icn.png"/> '. sprintf($this->language->get('text_items'), $this->cart->countProducts() + (isset($this->session->data['vouchers']) ? count($this->session->data['vouchers']) : 0), $this->currency->format($total));
		
		$detect = new Mobile_Detect;		
		// Any mobile device (phones or tablets).
		/*if ( $detect->isMobile() ) {
			$data['text_items'] = '<img src="catalog/view/theme/careeb/img/cart.svg"/> <span id="cartTotal">'.$this->cart->countProducts()."</span>";
		} else {
			$data['text_items'] = '<img src="catalog/view/theme/careeb/img/cart-icon.svg"/> <span id="cartTotal">'.$this->cart->countProducts()."</span>";
		}*/
		
		if( $detect->isTablet() ) {
			$data['text_items'] = '<img src="catalog/view/theme/careeb/img/cart-icon.svg"/> <span id="cartTotal">'.$this->cart->countProducts()."</span>";
		} elseif ( $detect->isMobile() ) {
			$data['text_items'] = '<img src="catalog/view/theme/careeb/img/cart.svg"/> <span id="cartTotal">'.$this->cart->countProducts()."</span>";
		}  else {
			$data['text_items'] = '<img src="catalog/view/theme/careeb/img/cart-icon.svg"/> <span id="cartTotal">'.$this->cart->countProducts()."</span>";
		}
			
    	$data['text_loading'] = $this->language->get('text_loading');

		$data['button_remove'] = $this->language->get('button_remove');

        $this->load->model('slot/timing');
		$this->load->model('tool/image');
		$this->load->model('tool/upload');

		$data['products']       = array();
        $data['cartProducts']   = array();
       
		foreach ($this->cart->getSellerProducts($sId) as $product) {
		  
			if ($product['image']) {
                //$image = $this->model_tool_image->resize($product['image'], $this->config->get('config_image_cart_width'), $this->config->get('config_image_cart_height'));
				$image = $this->model_tool_image->resize($product['image'], 65, 65);
			} else {
				$image = '';
			}

			$option_data = array();

			foreach ($product['option'] as $option) {
				if ($option['type'] != 'file') {
					$value = $option['value'];
				} else {
					$upload_info = $this->model_tool_upload->getUploadByCode($option['value']);

					if ($upload_info) {
						$value = $upload_info['name'];
					} else {
						$value = '';
					}
				}

				$option_data[] = array(
					'name'  => $option['name'],
					'value' => (utf8_strlen($value) > 20 ? utf8_substr($value, 0, 20) . '..' : $value),
					'type'  => $option['type']
				);
			}

			// Display prices
			if (($this->config->get('config_customer_price') && $this->customer->isLogged()) || !$this->config->get('config_customer_price')) {
				$price = $this->currency->format($this->tax->calculate($product['price'], $product['tax_class_id'], $this->config->get('config_tax')));
			} else {
				$price = false;
			}

			// Display prices
			if (($this->config->get('config_customer_price') && $this->customer->isLogged()) || !$this->config->get('config_customer_price')) {
				$total = $this->currency->format($this->tax->calculate($product['price'], $product['tax_class_id'], $this->config->get('config_tax')) * $product['quantity']);
			} else {
				$total = false;
			}


			$data['text_seller'] = $this->language->get('text_seller');
				
			
			$data['products'][] = array(

			
				'seller_id' => $product['seller_id'],
				'sellername'=>$product['seller'],
				'sellerhref'=> $this->url->link('product/seller', 'seller_id=' . $product['seller_id']),
			
				'cart_id'   => $product['cart_id'],
				'thumb'     => $image,
				'name'      => $product['name'],
				'model'     => $product['model'],
				'option'    => $option_data,
				'recurring' => ($product['recurring'] ? $product['recurring']['name'] : ''),
				'quantity'  => $product['quantity'],
				'price'     => $price,
				'total'     => $total,
				
				'href'     => $this->url->link('product/product', 'seller_id=' . $product['seller_id'].'&product_id='.$product['product_id'])
			
			);
			
             if(array_key_exists($product['seller_id'],$sellerallInfo)){
					
					$sellerallInfo[$product['seller_id']]['product_total'] += $product['total'];	
					$sellerallInfo[$product['seller_id']]['product_count_total'] += 1;	
					//$sellerallInfo[$product['seller_id']]['free_delivery_limit'] = $sellerInfo['free_delivery_limit'];
					
					
					$sellerallInfo[$product['seller_id']]['products'][] = array(
					'cart_id'   => $product['cart_id'],
					'thumb'     => $image,
					'name'      => $product['name'],
					'model'     => $product['model'],
					'option'    => $option_data,
					'recurring' => ($product['recurring'] ? $product['recurring']['name'] : ''),
					'quantity'  => $product['quantity'],
					'price'     => $price,
					'total'     => $total,
					
				'href'     => $this->url->link('product/product', 'seller_id=' . $product['seller_id'].'&product_id='.$product['product_id'])
			
				);
			}else{
				  
				  
				$sellerInfo = $this->model_store_store->getSellerByID($product['seller_id']);
					if(isset($sellerInfo['minimum_order'])){
						$minimumOrderLimit = $sellerInfo['minimum_order'];
					}
					$minimumOrder = $this->currency->format($minimumOrderLimit);
					
					$sellerallInfo[$product['seller_id']]['text_minimum_order'] = $this->language->get('text_minimum_order').$minimumOrder;
					$sellerallInfo[$product['seller_id']]['delivery_charges'] = $sellerInfo['delivery_charges'];
					$sellerallInfo[$product['seller_id']]['minimumOrder'] = $minimumOrderLimit;
					
					$sellerallInfo[$product['seller_id']]['product_total'] = $product['total'];
					$sellerallInfo[$product['seller_id']]['product_count_total'] = 1;
					$sellerallInfo[$product['seller_id']]['free_delivery_limit'] = $sellerInfo['free_delivery_limit'];
					
					$sellerallInfo[$product['seller_id']]['store_name'] = $product['seller'];
					$sellerallInfo[$product['seller_id']]['products'][] = array(
					'cart_id'   => $product['cart_id'],
					'thumb'     => $image,
					'name'      => $product['name'],
					'model'     => $product['model'],
					'option'    => $option_data,
					'recurring' => ($product['recurring'] ? $product['recurring']['name'] : ''),
					'quantity'  => $product['quantity'],
					'price'     => $price,
					'total'     => $total,
					
				'href'     => $this->url->link('product/product', 'seller_id=' . $product['seller_id'].'&product_id='.$product['product_id'])
			
				);
				$data['cartProducts'][] = $product['product_id'];
			}
		}
		
		$data['sellerallInfo'] =$sellerallInfo;
		 
        unset($_SESSION['cartProducts']) ;
        $_SESSION['cartProducts'] = $data['cartProducts'];
		// Gift Voucher
		$data['vouchers'] = array();

		if (!empty($this->session->data['vouchers'])) {
			foreach ($this->session->data['vouchers'] as $key => $voucher) {
				$data['vouchers'][] = array(
					'key'         => $key,
					'description' => $voucher['description'],
					'amount'      => $this->currency->format($voucher['amount'])
				);
			}
		}

		$data['totals'] = array();
        $data['delivery'] ="";
		
		foreach ($total_data as $result) {
	
		   if($result['code'] =="sub_total")
           {
		      if($result['value'] < 100)
                {
                    $remaining = 100 - $result['value'];
                    $data['delivery'] = sprintf($this->language->get('text_no_free_delivery'), $remaining);
                }
                else if($result['value'] >= 100)
                {
                    $data['delivery'] = $this->language->get("text_free_delivery");
                }


                $data['orderLimit'] = ($result['value'] < $minimumOrderLimit) ? true : false;
		   }
		  
			$data['totals'][] = array(
				'title' => $result['title'],
				'text'  => $this->currency->format($result['value']),
                'amount'  => $result['value'],
			);
		}
        
  
		$data['cart'] = $this->url->link('checkout/cart');
		$data['checkout'] = $this->url->link('checkout/checkout', '', 'SSL');
        

      	$time = $this->model_slot_timing->getNextDeliveryTime($this->config->get('config_store_id'));
	    if (is_null($time)) {
	    	$data['next_delivery'] = $this->language->get("text_no_delivery_slot");	
	    }else{
	    	$data['next_delivery'] = $this->language->get("text_delivery_slot").$time;
	    }
		

		/* echo"<pre>";
		print_r($data);
		exit; */
        
		if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/template/common/cart.tpl')) {
			return $this->load->view($this->config->get('config_template') . '/template/common/cart.tpl', $data);
		} else {
			return $this->load->view('default/template/common/cart.tpl', $data);
		}
		
		

	}

	public function info() {
		$this->response->setOutput($this->index());
	}
}
