<?php echo $header; ?>
<div class="bt-breadcrumb">
	<div class="container">
		<ul class="breadcrumb">
			<?php foreach ($breadcrumbs as $breadcrumb) { ?>
				<li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
			<?php } ?>
		</ul>
	</div>
</div>
<style>
	#content.background-white {
		padding-right: 15px !important;
	}
</style>

<div class="container-fluid container">
	<div class="">
		<?php 
			echo $column_left; 
			echo $column_right;
			if ($column_left && $column_right) {
				$class = 'col-sm-6';
			} elseif ($column_left || $column_right) {
				$class = 'col-sm-9';
			} else {
				$class = 'col-sm-12';
			} 
		?>
		
		<div id="content" class="background-white form-horizontal <?php echo $class; ?>">
		
			<?php echo $content_top; ?>
			
			<h2 class="accountTitle">
				<?php echo $heading_title; ?>						
			</h2>

			<div class="content_bg">
			
				<fieldset class="accountDiv">

				<?php 
				if ($orders) { ?>

					<div class="table-responsive">

						<table class="table table-bordered table-hover" style="width: 100%;">

							<thead>

								<tr>

									<td class="text-left"><?php echo $column_order_id; ?></td>

									
	
			

									<td class="text-left"><?php echo $column_date_added; ?></td>

									<td class="text-left"><?php echo $column_product; ?></td>

									<td class="text-left"><?php echo $column_customer; ?></td>

									<td class="text-left"><?php echo $column_total; ?></td>

			
	 <td class="text-right"><?php echo $text_review; ?></td>
	
			

									<td></td>

								</tr>
	
							</thead>

							<tbody>

								<?php foreach ($orders as $order) { ?>

									<tr>

										<td class="text-left">#<?php echo $order['order_id']; ?></td>

										
	
			

										<td class="text-left"><?php echo $order['date_added']; ?></td>
		
										<td class="text-left"><?php echo $order['products']; ?></td>

										<td class="text-left"><?php echo $order['name']; ?></td>

										<td class="text-left"><?php echo $order['total']; ?></td>

			
	<td class="text-right">
	
	<div style="height:10px;clear:both;">&nbsp;</div>
	<?php 
	foreach($order['sellers'] as $sellerproduct){?>
	  <div><b><?php echo $text_seller; ?></b> <?php echo $sellerproduct['username']; ?></div>
	   <div><b><?php echo $text_products; ?></b> <?php echo $sellerproduct['numprod']; ?></div>
	   <div><b><?php echo $text_status; ?></b> <?php echo $sellerproduct['name']; ?></div>
	   <?php if($order['order_status_id'] == 19){ ?>
	   <div>
	   <?php if(!$sellerproduct['reviews']){?>
	   <a style="display:none;" href="index.php?route=account/review&order_id=<?php echo $order['order_id'].'&seller_id='.$sellerproduct['seller_id'];?>" 
	   class="btn btn-primary"><?php echo $text_rate; ?></a>
	   <a href="javascript:void(0);" class="btn btn-primary" onclick="reviewFormFunction(<?php echo $sellerproduct['seller_id']; ?>, <?php echo $order['order_id']; ?>)" id="reviewApplyId">Rate This Seller</a><?php } ?></div>
	   <?php } ?>
	<?php } ?>
	
	</td>
	
			

										<td class="text-center"><a href="<?php echo $order['href']; ?>" data-toggle="tooltip" title="<?php echo $button_view; ?>" class="btn-info"><i class="fa fa-eye"></i></a>&nbsp;&nbsp;<a href="<?php echo $order['reorder']; ?>"data-toggle="tooltip" title="<?php echo "Reorder"; ?>" class="btn-info"><i class="fa fa-shopping-cart"></i></a></td>

									</tr>
									<input type="hidden" id="orderId<?php echo $order['order_id']; ?>" value="0">
								<?php 
								} ?>

							</tbody>

						</table>

					</div>

					<div class="text-right"><?php echo $pagination; ?></div>

				<?php 
				} else { ?>

					<p><?php echo $text_empty; ?></p>

				<?php 
				} ?>
				
				</fieldset>
				</br>

				<div class="buttons clearfix">

					<div class="pull-right"><a href="<?php echo $continue; ?>" class="btn btn-primary"><?php echo $button_continue; ?></a></div>

				</div>

			</div>

			<?php echo $content_bottom; ?>
		</div>

	</div>

</div>
<div class="container">

		
		<?php if ($success) { ?>
  <div class="alert alert-success"><i class="fa fa-check-circle"></i> <?php echo $success; ?></div>
  <?php } ?>
			
		<?php if ($warning) { ?>
		<div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> <?php echo $warning; ?></div>
		<?php } ?>
  
  
			
    <div class="row">
        <div class="content_bg">

		
		<?php if ($success) { ?>
  <div class="alert alert-success"><i class="fa fa-check-circle"></i> <?php echo $success; ?></div>
  <?php } ?>
			
		<?php if ($warning) { ?>
		<div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> <?php echo $warning; ?></div>
		<?php } ?>
  
  
			
            <div class="row">
                <div class="col-sm-6">
                    <div class="review-content">                       
                        <div id="modal-ReviewForm" class="modal col-sm-6 custom-margin-3 ">
                            <div class="modal-dialog">
                                <div class="modal-content">
                                    <div class="modal-header">
										<h2 class="accountTitle" style="display: contents;">Reviews</h2>
                                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>                    
                                    </div>
                                    <div class="modal-body modal-body-custom">

		
		<?php if ($success) { ?>
  <div class="alert alert-success"><i class="fa fa-check-circle"></i> <?php echo $success; ?></div>
  <?php } ?>
			
		<?php if ($warning) { ?>
		<div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> <?php echo $warning; ?></div>
		<?php } ?>
  
  
			
                                        <div class="row">
                                            <div class="col-sm-12" id="quick-review">
												<div id="loadingDiv" style="display:none;    text-align: center; padding: 20px;">
													<img src="image/loader.gif">
												</div>                                          
                                            </div>
                                        </div>
                                    </div>
                                    
                                </div>
                            </div>
                        </div>                        
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<script>
function reviewFormFunction(seller_id, order_id){
	
	$('#quick-review').html('');
	$('#quick-review').html('<div id="loadingDiv" style="display:none;text-align: center; padding: 20px;"><img src="image/loader.gif"></div>');
	
	var orderId = $("#orderId"+order_id).val();
	if(orderId == 0){
		$("#loadingDiv").show();
	
		$('#quick-review').load('index.php?route=account/review&seller_id='+seller_id+'&order_id='+order_id);
	} else {
		$(".alert-danger").remove();
		$("#loadingDiv").show();
		$('#quick-review').html('Success: You have sucessfully submitted review!');
	}
	
	$('#modal-ReviewForm').modal('show');
}
</script>
<?php echo $footer; ?>