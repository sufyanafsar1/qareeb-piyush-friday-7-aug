<?php 
//$arr_length = count($riderOrders);
?>
<?php echo $header; ?><?php echo $column_left; ?>
<div id="content">
  <div class="page-header">
    <div class="container-fluid">
      <div class="pull-right">
        <button type="submit" id="button-shipping" form="form-order" formaction="<?php echo $shipping; ?>" data-toggle="tooltip" title="<?php echo $button_shipping_print; ?>" class="btn btn-info"><i class="fa fa-truck"></i></button>
        <button type="submit" id="button-invoice" form="form-order" formaction="<?php echo $invoice; ?>" data-toggle="tooltip" title="<?php echo $button_invoice_print; ?>" class="btn btn-info"><i class="fa fa-print"></i></button>
        <a href="<?php echo $add; ?>" data-toggle="tooltip" title="<?php echo $button_add; ?>" class="btn btn-primary"><i class="fa fa-plus"></i></a>
		 <a href="<?php echo $export; ?>" data-toggle="tooltip" title="<?php echo $button_export; ?>" class="btn btn-success">Export Excel</a>&nbsp;
		</div>
      <h1><?php echo $heading_title; ?></h1>
      <ul class="breadcrumb">
        <?php foreach ($breadcrumbs as $breadcrumb) { ?>
        <li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
        <?php } ?>
      </ul>
	  
    </div>
  </div>
  <div class="container-fluid">
    <div class="panel panel-default">
      <div class="panel-heading">
        <h3 class="panel-title"><i class="fa fa-list"></i> <?php echo $text_list; ?></h3>
      </div>
      <div class="panel-body">
        <div class="well">
          <div class="row">
            <div class="col-sm-4">
              <div class="form-group">
                <label class="control-label" for="input-order-id"><?php echo $entry_order_id; ?></label>
                <input type="text" name="filter_order_id" value="<?php echo $filter_order_id; ?>" placeholder="<?php echo $entry_order_id; ?>" id="input-order-id" class="form-control" />
              </div>
              <div class="form-group">
                <label class="control-label" for="input-customer"><?php echo $entry_customer; ?></label>
                <input type="text" name="filter_customer" value="<?php echo $filter_customer; ?>" placeholder="<?php echo $entry_customer; ?>" id="input-customer" class="form-control" />
              </div>
			  <div class="form-group">
                <label class="control-label" for="input-status">Seller Name</label>
                <select name="filter_seller_name" id="input-status" class="form-control">
                  <option value=""></option>
				<?php if (!is_null($filter_seller_name) && !$filter_seller_name) { ?>
                <?php } else { ?>
				<?php } ?>
                <?php foreach ($sellers as $key=>$seller) { ?>
					<?php if ($filter_seller_name == $key) { ?>
					  <option selected value="<?php echo $key; ?>"><?php echo $seller; ?></option>
						<?php } else { ?>
					  <option value="<?php echo $key; ?>"><?php echo $seller; ?></option>
					<?php } ?>
				<?php } ?>
                </select>
              </div>
			  <div class="form-group">
                <label class="control-label" for="input-status">Customer Type</label>
                <select name="filter_customer_type" id="input-status" class="form-control">
				<option value="*">All</option>
				<?php if ($filter_customer_type) { ?>
                  <option value="1" selected="selected">B2B</option>
                  <?php } else { ?>
                  <option value="1">B2B</option>
                  <?php } ?>
                  <?php if (!$filter_customer_type && !is_null($filter_customer_type)) { ?>
                  <option value="0" selected="selected">B2C</option>
                  <?php } else { ?>
                  <option value="0">B2C</option>
                  <?php } ?>
                  
                </select>
              </div>
            </div>
			
			
            <div class="col-sm-4">
              <div class="form-group">
                <label class="control-label" for="input-order-status"><?php echo $entry_order_status; ?></label>
                <select name="filter_order_status" id="input-order-status" class="form-control">
                  <option value="*"></option>
                  <?php if ($filter_order_status == '0') { ?>
                  <option value="0" selected="selected"><?php echo $text_missing; ?></option>
                  <?php } else { ?>
                  <option value="0"><?php echo $text_missing; ?></option>
                  <?php } ?>
                  <?php foreach ($order_statuses as $order_status) { ?>
                  <?php if ($order_status['order_status_id'] == $filter_order_status) { ?>
                  <option value="<?php echo $order_status['order_status_id']; ?>" selected="selected"><?php echo $order_status['name']; ?></option>
                  <?php } else { ?>
                  <option value="<?php echo $order_status['order_status_id']; ?>"><?php echo $order_status['name']; ?></option>
                  <?php } ?>
                  <?php } ?>
                </select>
              </div>
			  
			  
              <div class="form-group">
                <label class="control-label" for="input-total"><?php echo $entry_total; ?></label>
                <input type="text" name="filter_total" value="<?php echo $filter_total; ?>" placeholder="<?php echo $entry_total; ?>" id="input-total" class="form-control" />
              </div>
			  <!-- add city-->
			  
			 <div class="form-group">
                <label class="control-label" for="input-city">City Name</label>
                <input type="text" name="filter_city" value="<?php echo $filter_city; ?>" placeholder="<?php  echo $entry_city; ?>" id="input-city" class="form-control" />
             </div>
            </div>
			
            <div class="col-sm-4" style="display:none">
              <div class="form-group">
                <label class="control-label" for="input-date-added"><?php echo $entry_date_added; ?></label>
                <div class="input-group date">
                  <input type="text" name="filter_date_added" value="<?php echo $filter_date_added; ?>" placeholder="<?php echo $entry_date_added; ?>" data-date-format="YYYY-MM-DD" id="input-date-added" class="form-control" />
                  <span class="input-group-btn">
                  <button type="button" class="btn btn-default"><i class="fa fa-calendar"></i></button>
                  </span></div>
              </div>
              <div class="form-group">
                <label class="control-label" for="input-date-modified"><?php echo $entry_date_modified; ?></label>
                <div class="input-group date">
                  <input type="text" name="filter_date_modified" value="<?php echo $filter_date_modified; ?>" placeholder="<?php echo $entry_date_modified; ?>" data-date-format="YYYY-MM-DD" id="input-date-modified" class="form-control" />
                  <span class="input-group-btn">
                  <button type="button" class="btn btn-default"><i class="fa fa-calendar"></i></button>
                  </span></div>
              </div>
			
            </div>
			<div class="col-sm-4">
			<div class="form-group">
                <label class="control-label" for="input-rider"><?php echo $entry_rider; ?></label>
                <input type="text" name="filter_rider" value="<?php echo $filter_rider; ?>" placeholder="<?php echo $entry_rider; ?>" id="input-rider" class="form-control" />
              </div>
			 <div class="form-group">
                <label class="control-label" for="input-group"><?php echo $entry_group; ?></label>
                <select name="filter_group" id="input-group" class="form-control">
                  <?php foreach ($groups as $group) { ?>
                  <?php if ($group['value'] == $filter_group) { ?>
                  <option value="<?php echo $group['value']; ?>" selected="selected"><?php echo $group['text']; ?></option>
                  <?php } else { ?>
                  <option value="<?php echo $group['value']; ?>"><?php echo $group['text']; ?></option>
                  <?php } ?>
                  <?php } ?>
                </select>
              </div>
				<div>
				  <div id="daydisplaydate">
					  <div class="form-group test" id="test">
						<label class="control-label" for="input-date-start"><?php echo $entry_date_start; ?></label>
						<div class="input-group date">
						  <input type="text" name="filter_date_start" value="<?php echo $filter_date_start; ?>" placeholder="<?php echo $entry_date_start; ?>" data-date-format="YYYY-MM-DD" id="input-date-start" class="form-control" />
						  <span class="input-group-btn">
						  <button type="button" class="btn btn-default"><i class="fa fa-calendar"></i></button>
						  </span></div>
					  </div>
					  <div class="form-group">
						<label class="control-label" for="input-date-end"><?php echo $entry_date_end; ?></label>
						<div class="input-group date">
						  <input type="text" name="filter_date_end" value="<?php echo $filter_date_end; ?>" placeholder="<?php echo $entry_date_end; ?>" data-date-format="YYYY-MM-DD" id="input-date-end" class="form-control" />
						  <span class="input-group-btn">
						  <button type="button" class="btn btn-default"><i class="fa fa-calendar"></i></button>
						  </span></div>
					  </div>
				  </div>
					<div class="form-group" id="monthdisplaydate">
						<label class="control-label" for="input-group"><?php echo $entry_month; ?></label>
						<select name="filter_month" id="input-group" class="form-control">
						  <?php foreach ($month as $key => $value) { ?>
						  <?php if ($key == $filter_month) { ?>
						  <option value="<?php echo $key; ?>" selected="selected"><?php echo $value; ?></option>
						  <?php } else { ?>
						  <option value="<?php echo $key; ?>"><?php echo $value; ?></option>
						  <?php } ?>
						  <?php } ?>
						</select>
					</div>
					<div class="form-group" id="yeardisplaydate">
						<label class="control-label" for="input-group"><?php echo $entry_year; ?></label>
						<select name="filter_year" id="input-group" class="form-control">
						  <?php foreach ($years as $key => $value) { ?>
						  <?php if ($key == $filter_year) { ?>
						  <option value="<?php echo $key; ?>" selected="selected"><?php echo $value; ?></option>
						  <?php } else { ?>
						  <option value="<?php echo $key; ?>"><?php echo $value; ?></option>
						  <?php } ?>
						  <?php } ?>
						</select>
					</div>
				</div>
			</div>
              <button type="button" id="button-filter" class="btn btn-primary pull-right"><i class="fa fa-search"></i> <?php echo $button_filter; ?></button>
          </div>
        </div>
        <form method="post" enctype="multipart/form-data" target="_blank" id="form-order">
          <div class="table-responsive">
            <table class="table table-bordered table-hover">
              <thead>
                <tr>
                  <td style="width: 1px;" class="text-center"><input type="checkbox" onclick="$('input[name*=\'selected\']').prop('checked', this.checked);" /></td>
                  <td class="text-right"><?php if ($sort == 'o.order_id') { ?>
                    <a href="<?php echo $sort_order; ?>" class="<?php echo strtolower($order); ?>"><?php echo $column_order_id; ?></a>
                    <?php } else { ?>
                    <a href="<?php echo $sort_order; ?>"><?php echo $column_order_id; ?></a>
                    <?php } ?></td>
                  <td class="text-left"><?php if ($sort == 'customer') { ?>
                    <a href="<?php echo $sort_customer; ?>" class="<?php echo strtolower($order); ?>"><?php echo $column_customer; ?></a>
                    <?php } else { ?>
                    <a href="<?php echo $sort_customer; ?>"><?php echo $column_customer; ?></a>
                    <?php } ?></td>
					
                  <td class="text-left">Seller Name</td>
					
                  <td class="text-right"><?php if ($sort == 'o.total') { ?>
                    <a href="<?php echo $sort_total; ?>" class="<?php echo strtolower($order); ?>"><?php echo $column_total; ?></a>
                    <?php } else { ?>
                    <a href="<?php echo $sort_total; ?>"><?php echo $column_total; ?></a>
                    <?php } ?></td>
                  <td class="text-left"><?php if ($sort == 'o.date_added') { ?>
                    <a href="<?php echo $sort_date_added; ?>" class="<?php echo strtolower($order); ?>"><?php echo $column_date_added; ?></a>
                    <?php } else { ?>
                    <a href="<?php echo $sort_date_added; ?>"><?php echo $column_date_added; ?></a>
                    <?php } ?></td>
                  
				 <!-- <td class="text-left"><?php// if ($sort == 'o.date_modified') { ?>
                    <a href="<?php //echo $sort_date_modified; ?>" class="<?php //echo strtolower($order); ?>"><?php //echo $column_date_modified; ?></a>
                    <?php //} else { ?>
                    <a href="<?php //echo $sort_date_modified; ?>"><?php //echo $column_date_modified; ?></a>
                    <?php// } ?></td> -->
					
                    <td class="text-left">Rider Name</td>
                  
               
                   
                    <td class="text-left">Payment</td>	
					 <!--<td class="text-left">Seller</td>-->
					 <td class="text-left">City</td>
                    <td class="text-left">Status</td>	
					<!--<td class="text-left"><?php// if ($sort == 'status') { ?>
                    <a href="<?php //echo $sort_status; ?>" class="<?php //echo strtolower($order); ?>"><?php //echo $column_status; ?></a>
                    <?php //} else { ?>
                    <a href="<?php// echo $sort_status; ?>"><?php //echo $column_status; ?></a> 
                    <?php //} ?></td>-->
					

				<td class="text-left"><?php echo $column_seller; ?></td>
				<td class="text-left"><?php echo $column_seller_status; ?></td>
			
                  <td class="text-right"><?php echo $column_action; ?></td>
                </tr>
              </thead>
              <tbody>
                <?php if ($orders) { ?>
                <?php 
					$sequence = 0;
					foreach ($orders as $order) { 	
						$class1 = '';
						if($sequence == $order['sequence']){							
							$class = 'style="border-top: none; "';
							$class = 'style="border-top: 0px"';
						} else {
							$class = 'style="border-top: 3px solid #000;"';
							$class1 = 'style="border-bottom: 0px;"';
						}
						$sequence = $order['sequence'];
						/* echo"<pre>";
						print_r($order);
						exit; */
				?>
                <tr <?php echo $class; ?> >
                  <td <?php echo $class1; ?> class="text-center"><?php if (in_array($order['order_id'], $selected)) { ?>
                    <input type="checkbox" name="selected[]" value="<?php echo $order['order_id']; ?>" checked="checked" />
                    <?php } else { ?>
                    <input type="checkbox" name="selected[]" value="<?php echo $order['order_id']; ?>" />
                    <?php } ?>
                    <input type="hidden" name="shipping_code[]" value="<?php echo $order['shipping_code']; ?>" /></td>
                  <td <?php echo $class; ?> class="text-right"><?php echo $order['order_id']; ?></td>
                  <td class="text-left"><?php echo $order['customer']; ?></td>
                  <td class="text-left"> <?php echo $order['sellername']; ?> </td>
                  <td class="text-right"><?php echo $order['total']; ?></td>
                  <td class="text-left"><?php echo $order['date_added']; ?></td>
                 <!--  <td class="text-left"><?php //echo $order['date_modified']; ?></td> -->
                  <td class="text-left"> <?php echo $order['rider_name']; ?> </td>

				<td class="text-left"><?php echo $order['seller']; ?></td>
				<td class="text-left"><?php echo $order['sellerstatus']; ?></td>
			
                  <td class="text-left"> <?php echo $order['payment_method']; ?> </td>
                  <td class="text-left"> <?php echo $order['shipping_city']; ?> </td>
				  <td class="text-left"><?php echo $order['status']; ?></td>
                  <td class="text-right"><a href="<?php echo $order['view']; ?>" data-toggle="tooltip" title="<?php echo $button_view; ?>" class="btn btn-info"><i class="fa fa-eye"></i></a> <a href="<?php echo $order['edit']; ?>" data-toggle="tooltip" title="<?php echo $button_edit; ?>" class="btn btn-primary"><i class="fa fa-pencil"></i></a>
                    <button type="button" value="<?php echo $order['order_id']; ?>" id="button-delete<?php echo $order['order_id']; ?>" data-loading-text="<?php echo $text_loading; ?>" data-toggle="tooltip" title="<?php echo $button_delete; ?>" class="btn btn-danger"><i class="fa fa-trash-o"></i></button></td>
                </tr>
                <?php } ?>
				
                <?php } else { ?>
                <tr>
                  <td class="text-center" colspan="8"><?php echo $text_no_results; ?></td>
                </tr>
                <?php } ?>
              </tbody>
            </table>
          </div>
        </form>
        <div class="row">
          <div class="col-sm-6 text-left"><?php echo $pagination; ?></div>
          <div class="col-sm-6 text-right"><?php echo $results; ?></div>
        </div>
      </div>
    </div>
  </div>
  <script type="text/javascript"><!--
  
  var groupID = '';
$(document).ready(function(){
	var groupID  = $('#input-group').val();
	if(groupID == 'day'){
		$("#daydisplaydate").css('display','block');
	}else{
		$("#daydisplaydate").css('display','none');	
	}
	if(groupID == 'month'){
		$("#monthdisplaydate").css('display','block');
	}else{
		$("#monthdisplaydate").css('display','none');	
	}
	if(groupID == 'year'){
		$("#yeardisplaydate").css('display','block');
	}else{
		$("#yeardisplaydate").css('display','none');	
	}	
	
    $('#input-group').on('change', function(){
		groupID = $(this).val();
		if(groupID == 'day'){
			$("#daydisplaydate").css('display','block');
		}else{
			$("#daydisplaydate").css('display','none');	
		}
		if(groupID == 'month'){
			$("#monthdisplaydate").css('display','block');
		}else{
			$("#monthdisplaydate").css('display','none');	
		}
		if(groupID == 'year'){
			$("#yeardisplaydate").css('display','block');
		}else{
			$("#yeardisplaydate").css('display','none');	
		}	
	});
});

$('#button-filter').on('click', function() {
	url = 'index.php?route=sale/order&token=<?php echo $token; ?>';

	var filter_order_id = $('input[name=\'filter_order_id\']').val();

	if (filter_order_id) {
		url += '&filter_order_id=' + encodeURIComponent(filter_order_id);
	}


				var filter_seller = $('input[name=\'filter_seller\']').val();	
				if (filter_seller) {
					url += '&filter_seller=' + encodeURIComponent(filter_seller);
				}
				
			
	var filter_customer = $('input[name=\'filter_customer\']').val();

	if (filter_customer) {
		url += '&filter_customer=' + encodeURIComponent(filter_customer);
	}
	
	////////
	var filter_city = $('input[name=\'filter_city\']').val();

	if (filter_city) {
		url += '&filter_city=' + encodeURIComponent(filter_city);
	}
	///////
	var filter_rider = $('input[name=\'filter_rider\']').val();
	if (filter_rider) {
		url += '&filter_rider=' + encodeURIComponent(filter_rider);
	}
	var groupvalue  = $('#input-group').val();
	url += '&filter_group=' + groupvalue;
	if(groupvalue == 'day'){
		var startdate = $('input[name=\'filter_date_start\']').val();
		var enddate = $('#input-date-end').val();
		url += '&filter_date_start=' + startdate;
		url += '&filter_date_end=' + enddate;
	}else if(groupvalue == 'year'){
		var year = $('select[name=\'filter_year\']').val();
		url += '&filter_year=' + year;
	}else if(groupvalue == 'month'){
		var month = $('select[name=\'filter_month\']').val();
		url += '&filter_month=' + month;
	}
	
	
	var filter_order_status = $('select[name=\'filter_order_status\']').val();

	if (filter_order_status != '*') {
		url += '&filter_order_status=' + encodeURIComponent(filter_order_status);
	}
	
	var filter_customer_type = $('select[name=\'filter_customer_type\']').val();
	
	if (filter_customer_type != '*') {
		url += '&filter_customer_type=' + encodeURIComponent(filter_customer_type); 
	}

	var filter_total = $('input[name=\'filter_total\']').val();

	if (filter_total) {
		url += '&filter_total=' + encodeURIComponent(filter_total);
	}

	var filter_date_added = $('input[name=\'filter_date_added\']').val();

	if (filter_date_added) {
		url += '&filter_date_added=' + encodeURIComponent(filter_date_added);
	}

	var filter_date_modified = $('input[name=\'filter_date_modified\']').val();

	if (filter_date_modified) {
		url += '&filter_date_modified=' + encodeURIComponent(filter_date_modified);
	}
	
	var filter_seller_name = $('select[name=\'filter_seller_name\']').val();
	
	if (filter_seller_name) {
		url += '&filter_seller_name=' + encodeURIComponent(filter_seller_name);
	}
	
	filter_seller_name

	location = url;
});
//--></script>
  <script type="text/javascript"><!--
  

$('input[name=\'filter_rider\']').autocomplete({
	'source': function(request, response) {
		$.ajax({
			url: 'index.php?route=rider/rider/autocomplete&token=<?php echo $token; ?>&filter_name=' +  encodeURIComponent(request),
			dataType: 'json',
			success: function(json) {
				response($.map(json, function(item) {
					return {
						label: item['name'],
						value: item['rider_id']
					}
				}));
			}
		});
	},
	'select': function(item) {
		$('input[name=\'filter_rider\']').val(item['label']);
	}
});


				$('input[name=\'filter_seller\']').autocomplete({
	'source': function(request, response) {
		$.ajax({
			url: 'index.php?route=sale/seller/autocomplete&token=<?php echo $token; ?>&filter_name=' +  encodeURIComponent(request),
			dataType: 'json',			
			success: function(json) {
				response($.map(json, function(item) {
					return {
						label: item['name'],
						value: item['seller_id']
					}
				}));
			}
		});
	},
	'select': function(item) {
		$('input[name=\'filter_seller\']').val(item['label']);
	}	
});
			
$('input[name=\'filter_customer\']').autocomplete({
	
	'source': function(request, response) {
		$.ajax({
			url: 'index.php?route=customer/customer/autocomplete&token=<?php echo $token; ?>&filter_name=' +  encodeURIComponent(request),
			dataType: 'json',
			success: function(json) {
				response($.map(json, function(item) {
					return {
						label: item['name'],
						value: item['customer_id']
					}
				}));
			}
		});
	},
	'select': function(item) {
		$('input[name=\'filter_customer\']').val(item['label']);
	}
});
// city name
$('input[name=\'filter_city\']').autocomplete({
	'source': function(request, response) {
		$.ajax({
			url: 'index.php?route=sale/order/getcity&token=<?php echo $token; ?>&filter_city=' + encodeURIComponent(request),
			dataType: 'json',
			success: function(json) {
				response($.map(json, function(item) {
					return {
						label: item['name'],
						value: item['name']
					}
				}));
			}
		});
	},
	'select': function(item) {
		$('input[name=\'filter_city\']').val(item['label']);
	}
});
//--></script>
  <script type="text/javascript"><!--
$('input[name^=\'selected\']').on('change', function() {
	$('#button-shipping, #button-invoice').prop('disabled', true);

	var selected = $('input[name^=\'selected\']:checked');

	if (selected.length) {
		$('#button-invoice').prop('disabled', false);
	}

	for (i = 0; i < selected.length; i++) {
		if ($(selected[i]).parent().find('input[name^=\'shipping_code\']').val()) {
			$('#button-shipping').prop('disabled', false);

			break;
		}
	}
});

$('input[name^=\'selected\']:first').trigger('change');

// Login to the API
var token = '';

$.ajax({
	url: '<?php echo $store; ?>index.php?route=api/login',
	type: 'post',
	data: 'key=<?php echo $api_key; ?>',
	dataType: 'json',
	crossDomain: true,
	success: function(json) {
        $('.alert').remove();

        if (json['error']) {
    		if (json['error']['key']) {
    			$('#content > .container-fluid').prepend('<div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> ' + json['error']['key'] + ' <button type="button" class="close" data-dismiss="alert">&times;</button></div>');
    		}

            if (json['error']['ip']) {
    			$('#content > .container-fluid').prepend('<div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> ' + json['error']['ip'] + ' <button type="button" id="button-ip-add" data-loading-text="<?php echo $text_loading; ?>" class="btn btn-danger btn-xs pull-right"><i class="fa fa-plus"></i> <?php echo $button_ip_add; ?></button></div>');
    		}
        }

		if (json['token']) {
			token = json['token'];
		}
	},
	error: function(xhr, ajaxOptions, thrownError) {
		alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
	}
});

$(document).delegate('#button-ip-add', 'click', function() {
	$.ajax({
		url: 'index.php?route=user/api/addip&token=<?php echo $token; ?>&api_id=<?php echo $api_id; ?>',
		type: 'post',
		data: 'ip=<?php echo $api_ip; ?>',
		dataType: 'json',
		beforeSend: function() {
			$('#button-ip-add').button('loading');
		},
		complete: function() {
			$('#button-ip-add').button('reset');
		},
		success: function(json) {
			$('.alert').remove();

			if (json['error']) {
				$('#content > .container-fluid').prepend('<div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> ' + json['error'] + ' <button type="button" class="close" data-dismiss="alert">&times;</button></div>');
			}

			if (json['success']) {
				$('#content > .container-fluid').prepend('<div class="alert alert-success"><i class="fa fa-check-circle"></i> ' + json['success'] + ' <button type="button" class="close" data-dismiss="alert">&times;</button></div>');
			}
		},
		error: function(xhr, ajaxOptions, thrownError) {
			alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
		}
	});
});

$('button[id^=\'button-delete\']').on('click', function(e) {
	if (confirm('<?php echo $text_confirm; ?>')) {
		var node = this;

		$.ajax({
			url: '<?php echo $store; ?>index.php?route=api/order/delete&token=' + token + '&order_id=' + $(node).val(),
			dataType: 'json',
			crossDomain: true,
			beforeSend: function() {
				$(node).button('loading');
			},
			complete: function() {
				$(node).button('reset');
			},
			success: function(json) {
				$('.alert').remove();

				if (json['error']) {
					$('#content > .container-fluid').prepend('<div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> ' + json['error'] + ' <button type="button" class="close" data-dismiss="alert">&times;</button></div>');
				}

				if (json['success']) {
					$('#content > .container-fluid').prepend('<div class="alert alert-success"><i class="fa fa-check-circle"></i> ' + json['success'] + ' <button type="button" class="close" data-dismiss="alert">&times;</button></div>');
        location.reload(); // $('#form-order').load(document.URL +  ' #form-order');
				}
			},
			error: function(xhr, ajaxOptions, thrownError) {
				alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
			}
		});
	}
});
//--></script>
  <script src="view/javascript/jquery/datetimepicker/bootstrap-datetimepicker.min.js" type="text/javascript"></script>
  <link href="view/javascript/jquery/datetimepicker/bootstrap-datetimepicker.min.css" type="text/css" rel="stylesheet" media="screen" />
  <script type="text/javascript"><!--
$('.date').datetimepicker({
	pickTime: false
});
//--></script></div>
<?php echo $footer; ?>
