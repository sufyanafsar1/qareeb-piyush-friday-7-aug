<?php
class ControllerApiCart extends Controller {
	
	public function add() {
		$this->load->language('api/cart');

		$json = array();
		
		if (!isset($this->session->data['api_id'])) {
			$json['error']['warning'] = $this->language->get('error_permission');
		} else {
		
			if (isset($this->request->post['product'])) {
				$this->cart->clear();

				foreach ($this->request->post['product'] as $product) {
				
				
					
					if (isset($product['option'])) {
						$option = $product['option'];
					} else {
						$option = array();
					}
					
						$r ="";
					$this->load->model('catalog/product');
					if(isset($this->request->get['seller_id'])){
						if($this->request->get['seller_id'] != 0){
							$seller_id = (int)$this->request->get['seller_id'];
						}else{
							if (isset($product['seller_id'])) {
								$seller_id = (int)$product['seller_id'];
								if($seller_id == 0){
									$seller_id = $this->model_catalog_product->getDefaultSeller($product['product_id']);
								}
							} else {
								$seller_id = $this->model_catalog_product->getDefaultSeller($product['product_id']);
							}
							
						}
					}else{
						if (isset($product['seller_id'])) {
							$seller_id = (int)$product['seller_id'];
							if($seller_id == 0){
								$seller_id = $this->model_catalog_product->getDefaultSeller($product['product_id']);
							}
						} else {
							$seller_id = $this->model_catalog_product->getDefaultSeller($product['product_id']);
						}
					}
					
					if(isset($this->request->get['order_id'])){
						if($this->request->get['order_id'] != 0){
							$seller_id_order = $this->db->query("SELECT seller_id FROM `oc_order` WHERE `order_id` = ".$this->request->get['order_id']."");
							$seller_id = $seller_id_order->row['seller_id'];
						}
					}
					
					
					$this->cart->addtest($product['product_id'], $product['quantity'], $option, $seller_id, $recurring_id);					
				}

				$json['success'] = $this->language->get('text_success');

				unset($this->session->data['shipping_method']);
				unset($this->session->data['shipping_methods']);
				unset($this->session->data['payment_method']);
				unset($this->session->data['payment_methods']);
			
			} elseif (isset($this->request->post['product_id'])) {
				$this->load->model('catalog/product');

				$r ="";
				$this->load->model('catalog/product');
				
				
					if (isset($product['seller_id'])) {
						$seller_id = (int)$product['seller_id'];
						if($seller_id == 0){
							$seller_id = $this->model_catalog_product->getDefaultSeller($product['product_id']);
						}
					} else {
						$seller_id = $this->model_catalog_product->getDefaultSeller($product['product_id']);
					}
							
					if(isset($this->request->get['store_id'])){
						if($this->request->get['store_id'] != 0){
							 $seller_id = $this->request->get['store_id'];
						
						}
					}
					if(isset($this->request->get['order_id'])){
						
						if($this->request->get['order_id'] != 0){
							$product_seller_query_custome = $this->db->query("SELECT seller_id FROM `oc_order` WHERE `order_id` = ".$this->request->get['order_id']."");
							$seller_id = $product_seller_query_custome->row['seller_id'];
						
						}
					}
			
				$product_info = $this->model_catalog_product->getSProduct($this->request->post['product_id'],$seller_id);
		
				if ($product_info) {
					if (isset($this->request->post['quantity'])) {
						$quantity = $this->request->post['quantity'];
					} else {
						$quantity = 1;
					}

					if (isset($this->request->post['option'])) {
						$option = array_filter($this->request->post['option']);
					} else {
						$option = array();
					}

					
					$product_options = $this->model_catalog_product->getSProductOptions($this->request->post['product_id'],$seller_id);
				
					
			

					foreach ($product_options as $product_option) {
						if ($product_option['required'] && empty($option[$product_option['product_option_id']])) {
							$json['error']['option'][$product_option['product_option_id']] = sprintf($this->language->get('error_required'), $product_option['name']);
						}
					}
				
					if (!isset($json['error']['option'])) {
					
					$this->cart->addtest($this->request->post['product_id'], $quantity, $option, $seller_id, $recurring_id);	
					
						$json['success'] = $this->language->get('text_success');

						unset($this->session->data['shipping_method']);
						unset($this->session->data['shipping_methods']);
						unset($this->session->data['payment_method']);
						unset($this->session->data['payment_methods']);
					}
				} else {
					$json['error']['store'] = $this->language->get('error_store');
				}
			}
		}

		if (isset($this->request->server['HTTP_ORIGIN'])) {
			$this->response->addHeader('Access-Control-Allow-Origin: ' . $this->request->server['HTTP_ORIGIN']);
			$this->response->addHeader('Access-Control-Allow-Methods: GET, PUT, POST, DELETE, OPTIONS');
			$this->response->addHeader('Access-Control-Max-Age: 1000');
			$this->response->addHeader('Access-Control-Allow-Headers: Content-Type, Authorization, X-Requested-With');
		}

		$this->response->addHeader('Content-Type: application/json');
		$this->response->setOutput(json_encode($json));
	}
	

	public function edit() {
		$this->load->language('api/cart');

		$json = array();

		if (!isset($this->session->data['api_id'])) {
			$json['error'] = $this->language->get('error_permission');
		} else {
			$this->cart->update($this->request->post['key'], $this->request->post['quantity']);

			$json['success'] = $this->language->get('text_success');

			unset($this->session->data['shipping_method']);
			unset($this->session->data['shipping_methods']);
			unset($this->session->data['payment_method']);
			unset($this->session->data['payment_methods']);
			unset($this->session->data['reward']);
		}

		if (isset($this->request->server['HTTP_ORIGIN'])) {
			$this->response->addHeader('Access-Control-Allow-Origin: ' . $this->request->server['HTTP_ORIGIN']);
			$this->response->addHeader('Access-Control-Allow-Methods: GET, PUT, POST, DELETE, OPTIONS');
			$this->response->addHeader('Access-Control-Max-Age: 1000');
			$this->response->addHeader('Access-Control-Allow-Headers: Content-Type, Authorization, X-Requested-With');
		}

		$this->response->addHeader('Content-Type: application/json');
		$this->response->setOutput(json_encode($json));
	}

	public function remove() {
		$this->load->language('api/cart');

		$json = array();

		if (!isset($this->session->data['api_id'])) {
			$json['error'] = $this->language->get('error_permission');
		} else {
			
			if (isset($this->request->post['key'])) {
				$this->cart->remove($this->request->post['key']);

				unset($this->session->data['vouchers'][$this->request->post['key']]);

				$json['success'] = $this->language->get('text_success');

				unset($this->session->data['shipping_method']);
				unset($this->session->data['shipping_methods']);
				unset($this->session->data['payment_method']);
				unset($this->session->data['payment_methods']);
				unset($this->session->data['reward']);
			}
		}

		if (isset($this->request->server['HTTP_ORIGIN'])) {
			$this->response->addHeader('Access-Control-Allow-Origin: ' . $this->request->server['HTTP_ORIGIN']);
			$this->response->addHeader('Access-Control-Allow-Methods: GET, PUT, POST, DELETE, OPTIONS');
			$this->response->addHeader('Access-Control-Max-Age: 1000');
			$this->response->addHeader('Access-Control-Allow-Headers: Content-Type, Authorization, X-Requested-With');
		}

		$this->response->addHeader('Content-Type: application/json');
		$this->response->setOutput(json_encode($json));
	}

	public function products() {
		
		
		$this->load->language('api/cart');

		$json = array();
		
		if (!isset($this->session->data['api_id'])) {
			$json['error']['warning'] = $this->language->get('error_permission');
		} else {
			
			
			
			if (!$this->cart->hasStock() && (!$this->config->get('config_stock_checkout') || $this->config->get('config_stock_warning'))) {
				$json['error']['stock'] = $this->language->get('error_stock');
			}
	
			$json['products'] = array();
			$seller_first_id= 0;
			$products = $this->cart->getProducts();
			
			if(isset($this->request->get['seller_first_id'])){
				if($this->request->get['seller_first_id'] != 0){
					$seller_first_id = $this->request->get['seller_first_id'];
				}	
			}
			
			
			
			if($seller_first_id != 0){
				foreach ($products as $product) {
					if($seller_first_id == $product['seller_id']){
						$product_total = 0;
						
						$sellerIdArr[] = $product['seller_id'];
						
						
						
						foreach ($products as $product_2) {
							if ($product_2['product_id'] == $product['product_id']) {
								$product_total += $product_2['quantity'];
							}
						}
					
						if ($product['minimum'] > $product_total) {
							$json['error']['minimum'][] = sprintf($this->language->get('error_minimum'), $product['name'], $product['minimum']);
						}

						$option_data = array();

						foreach ($product['option'] as $option) {
							$option_data[] = array(
								'product_option_id'       => $option['product_option_id'],
								'product_option_value_id' => $option['product_option_value_id'],
								'name'                    => $option['name'],
								'value'                   => $option['value'],
								'type'                    => $option['type']
							);
						}
						if ($product['special'] != 0) {
							$product['price'] = $product['special'];
						}
						
						if(isset($_GET['order_id'])){
							$product['seller_id'] = 0;
							$product_seller_query_custome = $this->db->query("SELECT seller_id FROM `oc_order` WHERE `order_id` = ".$_GET['order_id']."");
							$product['seller_id'] = $product_seller_query_custome->row['seller_id'];
						}
						
						$seller_name = '';
						
						if(isset($product['seller_id'])){
							$product_seller_name_query_custome = $this->db->query("SELECT username  FROM `oc_sellers` WHERE `seller_id` = ".$product['seller_id']."");
							$seller_name = $product_seller_name_query_custome->row['username'];
						}

					$this->load->model('catalog/product');
				 
					if (isset($product['seller_id'])) {
						$seller_id = (int)$product['seller_id'];
						
						if($seller_id == 0){
							$seller_id = $this->model_catalog_product->getDefaultSeller($product['product_id']);
						}
						
					} else {
	
						$seller_id = $this->model_catalog_product->getDefaultSeller($product['product_id']);
					}

					$seller = "";
					
					$seller_query = $this->db->query("SELECT * FROM " . DB_PREFIX . "sellers WHERE seller_id = '" . (int)$seller_id . "'");
					$seller_name = "";
					if ($seller_query->num_rows) {
						$seller_name = $seller_query->row['firstname'];
					}
					

			$this->load->model('catalog/product');
				 
				if (isset($product['seller_id'])) {
					$seller_id = (int)$product['seller_id'];
					if($seller_id == 0){
					$seller_id = $this->model_catalog_product->getDefaultSeller($product['product_id']);
					}
					} else {
					$seller_id = $this->model_catalog_product->getDefaultSeller($product['product_id']);
					}

					$seller = "";
					
					$seller_query = $this->db->query("SELECT * FROM " . DB_PREFIX . "sellers WHERE seller_id = '" . (int)$seller_id . "'");
					$seller = "";
					if ($seller_query->num_rows) {
						$seller = $seller_query->row['firstname'];
					}
					
			
					$json['products'][] = array(

				'seller_id' => $seller_id,
					'seller' => $seller,
					
			

						'seller_id'  => $product['seller_id'],
						'seller_name' => $seller_name,
						'cart_id'    => $product['cart_id'],
						'product_id' => $product['product_id'],
						'name'       => $product['name'],
						'model'      => $product['model'],
						'option'     => $option_data,
						'quantity'   => $product['quantity'],
						'stock'      => $product['stock'] ? true : !(!$this->config->get('config_stock_checkout') || $this->config->get('config_stock_warning')),
						'shipping'   => $product['shipping'],
						'price'      => $this->currency->format($this->tax->calculate($product['price'], $product['tax_class_id'], $this->config->get('config_tax'))),
						'total'      => $this->currency->format($this->tax->calculate($product['price'], $product['tax_class_id'], $this->config->get('config_tax')) * $product['quantity']),
						'reward'     => $product['reward']
					);
					
					}
				}
				
				$json['vouchers'] = array();

				if (!empty($this->session->data['vouchers'])) {
					foreach ($this->session->data['vouchers'] as $key => $voucher) {
						$json['vouchers'][] = array(
							'code'             => $voucher['code'],
							'description'      => $voucher['description'],
							'from_name'        => $voucher['from_name'],
							'from_email'       => $voucher['from_email'],
							'to_name'          => $voucher['to_name'],
							'to_email'         => $voucher['to_email'],
							'voucher_theme_id' => $voucher['voucher_theme_id'],
							'message'          => $voucher['message'],
							'amount'           => $this->currency->format($voucher['amount'])
						);
					}
				}

				// Totals
				$this->load->model('extension/extension');

				$total_data = array();
				$total = 0;
				$taxes = $this->cart->getTaxes();

				$sort_order = array();

				$results = $this->model_extension_extension->getExtensions('total');
			
				foreach ($results as $key => $value) {
					$sort_order[$key] = $this->config->get($value['code'] . '_sort_order');
				}

				array_multisort($sort_order, SORT_ASC, $results);

				foreach ($results as $result) {
					
					if ($this->config->get($result['code'] . '_status')) {
						$this->load->model('total/' . $result['code']);
						
						if($result['code'] == 'credit'){
							if(isset($this->request->get['order_id'])){
								$this->{'model_total_' . $result['code']}->getTotalNew($total_data, $total, $taxes, $this->request->get['order_id']);
							} else {
								$this->{'model_total_' . $result['code']}->getTotal($total_data, $total, $taxes);
							}
						} else {
							$this->{'model_total_' . $result['code']}->getTotal($total_data, $total, $taxes);
						}
					}
				}
						
				$vat =  (float)$this->config->get('config_vat');
				if(isset($vat) && $vat > 0){
				   $this->load->model('total/vat');
					$this->{'model_total_vat'}->getTotal($total_data, $vat);
				}

				$sort_order = array();

				foreach ($total_data as $key => $value) {
					if($value['code'] =='shipping')
						unset($total_data['totals'][$key]);
					$sort_order[$key] = $value['sort_order'];
				}
				
				array_multisort($sort_order, SORT_ASC, $total_data);

				$json['totals'] = array();

				$order_id = $this->request->get['order_id'];
				
				if(isset($order_id)){
				
					$deliveryChargesArr = $this->db->query("
						select oc_sellers.delivery_charges, oc_sellers.seller_id, oc_sellers.firstname, oc_sellers.lastname 
						from oc_order_seller_shipping_fee 
						join oc_sellers on oc_sellers.seller_id = oc_order_seller_shipping_fee.seller_id 
						where oc_order_seller_shipping_fee.order_id = '$order_id'")->rows;
				} else {
					
					$sellerIdArr = array_unique( $sellerIdArr );
					$sellerIdStr =  implode(',', $sellerIdArr);				
											
					$deliveryChargesArr = $this->db->query("
											  select oc_sellers.delivery_charges, oc_sellers.seller_id, oc_sellers.firstname, oc_sellers.lastname 
											  from oc_sellers 
											where oc_sellers.seller_id in ( $sellerIdStr )")->rows;
					
					
					//$this->session->data['deliveryChargesArr'] = $deliveryChargesArr;
				
				}
						
						
				session_start();
				$deliveryCharges1 = $this->db->query("SELECT delivery_charges FROM `oc_sellers` WHERE `seller_id` = ".$_SESSION['selleridsore']);

				$deliveryCharges = 0;
			
				foreach ($total_data as $total) {
					
					if( $total['code'] == 'vat' ){
						foreach( $deliveryChargesArr as $deliveryChargesRow ){
							
							$deliveryCharges = $deliveryChargesRow['delivery_charges'];
							 $json['totals'][] = array(
									'title' => 'Delivery Charges',
									'text'  => $this->currency->format( $deliveryCharges1->row['delivery_charges'] ),
							); 
						}
					}
				
					$json['totals'][] = array(
						'title' => $total['title'],
						'text'  => $this->currency->format($total['value'])
					);
				}

				if (isset($this->request->server['HTTP_ORIGIN'])) {
					$this->response->addHeader('Access-Control-Allow-Origin: ' . $this->request->server['HTTP_ORIGIN']);
					$this->response->addHeader('Access-Control-Allow-Methods: GET, PUT, POST, DELETE, OPTIONS');
					$this->response->addHeader('Access-Control-Max-Age: 1000');
					$this->response->addHeader('Access-Control-Allow-Headers: Content-Type, Authorization, X-Requested-With');
				}
			
				
				
				$this->response->addHeader('Content-Type: application/json');
				$this->response->setOutput(json_encode($json));
				
				
				
			}else{
				$product_total = 0;
				foreach ($products as $product) {
				
				
					$sellerIdArr[] = $product['seller_id'];
				
					foreach ($products as $product_2) {
						if ($product_2['product_id'] == $product['product_id']) {
							$product_total += $product_2['quantity'];
						}
					}
				
					if ($product['minimum'] > $product_total) {
						$json['error']['minimum'][] = sprintf($this->language->get('error_minimum'), $product['name'], $product['minimum']);
					}

					$option_data = array();

					foreach ($product['option'] as $option) {
						$option_data[] = array(
							'product_option_id'       => $option['product_option_id'],
							'product_option_value_id' => $option['product_option_value_id'],
							'name'                    => $option['name'],
							'value'                   => $option['value'],
							'type'                    => $option['type']
						);
					}
					if ($product['special'] != 0) {
						//$price = $product_special_query->row['price'];
						$product['price'] = $product['special'];
					}
				
					if(isset($_GET['order_id'])){
						$product['seller_id'] = 0;
						$product_seller_query_custome = $this->db->query("SELECT seller_id FROM `oc_order` WHERE `order_id` = ".$_GET['order_id']."");
						$product['seller_id'] = $product_seller_query_custome->row['seller_id'];
					}
					
					$seller_name = '';
				
					if(isset($product['seller_id'])){
						$product_seller_name_query_custome = $this->db->query("SELECT username  FROM `oc_sellers` WHERE `seller_id` = ".$product['seller_id']."");
						$seller_name = $product_seller_name_query_custome->row['username'];
					}
					
					$this->load->model('catalog/product');
					 
					if (isset($product['seller_id'])) {
						$seller_id = (int)$product['seller_id'];

						if($seller_id == 0){
							$seller_id = $this->model_catalog_product->getDefaultSeller($product['product_id']);
						}
					} else {
						$seller_id = $this->model_catalog_product->getDefaultSeller($product['product_id']);
					}

					$seller = "";
					
					$seller_query = $this->db->query("SELECT * FROM " . DB_PREFIX . "sellers WHERE seller_id = '" . (int)$seller_id . "'");
					$seller = "";
					if ($seller_query->num_rows) {
						$seller = $seller_query->row['firstname'];
					}
						
					

			$this->load->model('catalog/product');
				 
				if (isset($product['seller_id'])) {
					$seller_id = (int)$product['seller_id'];
					if($seller_id == 0){
					$seller_id = $this->model_catalog_product->getDefaultSeller($product['product_id']);
					}
					} else {
					$seller_id = $this->model_catalog_product->getDefaultSeller($product['product_id']);
					}

					$seller = "";
					
					$seller_query = $this->db->query("SELECT * FROM " . DB_PREFIX . "sellers WHERE seller_id = '" . (int)$seller_id . "'");
					$seller = "";
					if ($seller_query->num_rows) {
						$seller = $seller_query->row['firstname'];
					}
					
			
					$json['products'][] = array(

				'seller_id' => $seller_id,
					'seller' => $seller,
					
			
						'seller_id'  => $product['seller_id'],
						'seller_name' => $seller_name,
						'cart_id'    => $product['cart_id'],
						'product_id' => $product['product_id'],
						'name'       => $product['name'],
						'model'      => $product['model'],
						'option'     => $option_data,
						'quantity'   => $product['quantity'],
						'stock'      => $product['stock'] ? true : !(!$this->config->get('config_stock_checkout') || $this->config->get('config_stock_warning')),
						'shipping'   => $product['shipping'],
						'price'      => $this->currency->format($this->tax->calculate($product['price'], $product['tax_class_id'], $this->config->get('config_tax'))),
						'total'      => $this->currency->format($this->tax->calculate($product['price'], $product['tax_class_id'], $this->config->get('config_tax')) * $product['quantity']),
						'reward'     => $product['reward']
					);
					
				}
				/* echo"<pre>";
				print_r($json['products']);
				exit; */

					// Voucher
				$json['vouchers'] = array();
		
				if (!empty($this->session->data['vouchers'])) {
					foreach ($this->session->data['vouchers'] as $key => $voucher) {
						$json['vouchers'][] = array(
							'code'             => $voucher['code'],
							'description'      => $voucher['description'],
							'from_name'        => $voucher['from_name'],
							'from_email'       => $voucher['from_email'],
							'to_name'          => $voucher['to_name'],
							'to_email'         => $voucher['to_email'],
							'voucher_theme_id' => $voucher['voucher_theme_id'],
							'message'          => $voucher['message'],
							'amount'           => $this->currency->format($voucher['amount'])
						);
					}
				}

				// Totals
				$this->load->model('extension/extension');

				$total_data = array();
				$total = 0;
				$taxes = $this->cart->getTaxes();

				$sort_order = array();

				$results = $this->model_extension_extension->getExtensions('total');

				foreach ($results as $key => $value) {
					$sort_order[$key] = $this->config->get($value['code'] . '_sort_order');
				}

				array_multisort($sort_order, SORT_ASC, $results);

				foreach ($results as $result) {
					if ($this->config->get($result['code'] . '_status')) {
						$this->load->model('total/' . $result['code']);
						
						if($result['code'] == 'credit'){
							if(isset($this->request->get['order_id'])){
								$this->{'model_total_' . $result['code']}->getTotalNew($total_data, $total, $taxes, $this->request->get['order_id']);
							} else {
								$this->{'model_total_' . $result['code']}->getTotal($total_data, $total, $taxes);
							}
						} else {
							$this->{'model_total_' . $result['code']}->getTotal($total_data, $total, $taxes);
						}
					}
				}
			
				$vat =  (float)$this->config->get('config_vat');
				if(isset($vat) && $vat > 0){
				   $this->load->model('total/vat');
					$this->{'model_total_vat'}->getTotal($total_data, $vat);
				}

				$sort_order = array();

				foreach ($total_data as $key => $value) {
					if($value['code'] =='shipping')
						unset($total_data['totals'][$key]);
					$sort_order[$key] = $value['sort_order'];
				}

				array_multisort($sort_order, SORT_ASC, $total_data);

				$json['totals'] = array();

				$order_id = $this->request->get['order_id'];
				
				if(isset($order_id)){
				
					$deliveryChargesArr = $this->db->query("
						select oc_sellers.delivery_charges, oc_sellers.seller_id, oc_sellers.firstname, oc_sellers.lastname 
						from oc_order_seller_shipping_fee 
						join oc_sellers on oc_sellers.seller_id = oc_order_seller_shipping_fee.seller_id 
						where oc_order_seller_shipping_fee.order_id = '$order_id'")->rows;
				} else {
					
					$sellerIdArr = array_unique( $sellerIdArr );
					$sellerIdStr =  implode(',', $sellerIdArr);				
											
					$deliveryChargesArr = $this->db->query("
											  select oc_sellers.delivery_charges, oc_sellers.seller_id, oc_sellers.firstname, oc_sellers.lastname 
											  from oc_sellers 
											where oc_sellers.seller_id in ( $sellerIdStr )")->rows;
					
					
					//$this->session->data['deliveryChargesArr'] = $deliveryChargesArr;
				
				}
			
				/* foreach ($total_data as $total) {
					
					if( $total['code'] == 'vat' ){
						foreach( $deliveryChargesArr as $deliveryChargesRow ){
							
							$deliveryCharges = $deliveryChargesRow['delivery_charges'];
							$json['totals'][] = array(
									'title' => 'Delivery Charges',
									'text'  => $this->currency->format( $deliveryCharges ),
							);
							//$total['value'] += $deliveryCharges;
						}
					}
					// if($total['code'] == 'total'){
						//$total['value'] += $deliveryCharges;
					//} 
				
					$json['totals'][] = array(
						'title' => $total['title'],
						'text'  => $this->currency->format($total['value'])
					);
				} */
			
				if($order_id != 0 and $order_id != ''){
					$seller_id_order = $this->db->query("SELECT seller_id FROM `oc_order` WHERE `order_id` = ".$order_id."");
					$_SESSION['selleridsore'] = $seller_id_order->row['seller_id'];
				}
				
				session_start();
				$deliveryCharges1 = $this->db->query("SELECT delivery_charges FROM `oc_sellers` WHERE `seller_id` = ".$_SESSION['selleridsore']);

				$deliveryCharges = 0;
				foreach ($total_data as $total) {
					
					if( $total['code'] == 'vat' ){
						foreach( $deliveryChargesArr as $deliveryChargesRow ){
							
							$deliveryCharges = $deliveryChargesRow['delivery_charges'];
							 $json['totals'][] = array(
									'title' => 'Delivery Charges',
									'text'  => $this->currency->format( $deliveryCharges1->row['delivery_charges'] ),
							); 
							//$total['value'] += $deliveryCharges;
						}
					}
					/* if($total['code'] == 'total'){
						$total['value'] += $deliveryCharges;
					} */
					if($total['code'] == 'sub_total'){
					$sub_total = $total['value'];
					
					}
					if($total['code'] == 'total'){
						$total_new = $sub_total+  $deliveryCharges1->row['delivery_charges'];
						$total['value'] = $total_new;
							
						
					}
					$json['totals'][] = array(
						'title' => $total['title'],
						'text'  => $this->currency->format($total['value'])
					);
				}

				if (isset($this->request->server['HTTP_ORIGIN'])) {
					$this->response->addHeader('Access-Control-Allow-Origin: ' . $this->request->server['HTTP_ORIGIN']);
					$this->response->addHeader('Access-Control-Allow-Methods: GET, PUT, POST, DELETE, OPTIONS');
					$this->response->addHeader('Access-Control-Max-Age: 1000');
					$this->response->addHeader('Access-Control-Allow-Headers: Content-Type, Authorization, X-Requested-With');
				}
			
				$this->response->addHeader('Content-Type: application/json');
				$this->response->setOutput(json_encode($json));
			}
		}
	}
}