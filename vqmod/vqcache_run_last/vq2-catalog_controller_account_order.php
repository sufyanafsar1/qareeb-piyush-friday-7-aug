<?php
class ControllerAccountOrder extends Controller {
	private $error = array();

	public function index() {
		if (!$this->customer->isLogged()) {
			$this->session->data['redirect'] = $this->url->link('account/order', '', 'SSL');

			$this->response->redirect($this->url->link('account/login', '', 'SSL'));
		}


			if (isset($this->session->data['success'])) {
				$data['success'] = $this->session->data['success'];		
				unset($this->session->data['success']);
			} else {
				$data['success'] = '';
			}

			if (isset($this->session->data['warning'])) {
				$data['warning'] = $this->session->data['warning'];		
				unset($this->session->data['warning']);
			} else {
				$data['warning'] = '';
			}
			
		$this->load->language('account/order');

		$this->document->setTitle($this->language->get('heading_title'));

		$data['breadcrumbs'] = array();

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_home'),
			'href' => $this->url->link('common/home')
		);

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_account'),
			'href' => $this->url->link('account/account', '', 'SSL')
		);

		$url = '';

		if (isset($this->request->get['page'])) {
			$url .= '&page=' . $this->request->get['page'];
		}

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('heading_title'),
			'href' => $this->url->link('account/order', $url, 'SSL')
		);

		$data['heading_title'] = $this->language->get('heading_title');

		$data['text_empty'] = $this->language->get('text_empty');

		$data['column_order_id'] = $this->language->get('column_order_id');
		$data['column_status'] = $this->language->get('column_status');
		$data['column_date_added'] = $this->language->get('column_date_added');
		$data['column_customer'] = $this->language->get('column_customer');
		$data['column_product'] = $this->language->get('column_product');
		$data['column_total'] = $this->language->get('column_total');

		$data['button_view'] = $this->language->get('button_view');
		$data['button_continue'] = $this->language->get('button_continue');

		if (isset($this->request->get['page'])) {
			$page = $this->request->get['page'];
		} else {
			$page = 1;
		}

		$data['orders'] = array();

		$this->load->model('account/order');

		$order_total = $this->model_account_order->getTotalOrders();

		$results = $this->model_account_order->getOrders(($page - 1) * 10, 10);
		//var_dump($results);exit;
		foreach ($results as $result) {
			$product_total = $this->model_account_order->getTotalOrderProductsByOrderId($result['order_id']);
			$voucher_total = $this->model_account_order->getTotalOrderVouchersByOrderId($result['order_id']);
			$Total1 = $this->model_account_order->getOrderTotals1($result['order_id']);
			$Total2 = $this->model_account_order->getOrderTotals2($result['order_id']);
			
			$date_added = date("Y-m-d", strtotime($result['date_added']));				
			$date = '2018-11-02';
	
			$beforDate = 0;
			$afterDate = 0;
			if($result['date_added'] != '0000-00-00 00:00:00'){
				if($date_added <= $date){
					$Final_Total = ($Total1 + $Total2);				
				} else {
					$Final_Total = $Total1;		
				}				
			} else {
				$Final_Total = ($Total1 + $Total2);
			}
			

			
			$data['text_seller'] = $this->language->get('text_seller');
			$data['text_rate'] = $this->language->get('text_rate');
			$data['text_status'] = $this->language->get('text_status');
			$data['text_products'] = $this->language->get('text_products');
			$data['text_review'] = $this->language->get('text_review');
				$data['text_review'] = $this->language->get('text_review');
			
			$languageId = (int)$this->config->get('config_language_id');
			
			$this->load->model('account/review');
			
			$sellers12 = $this->model_account_order->getSellerOrderProducts($result['order_id']);
			
			$sellers = array();
			
			
			
			foreach($sellers12 as $sellerproduct){
			
			 $reviews = $this->model_account_review->getReview($sellerproduct['seller_id'],$result['order_id']);
			
			
             $sellerName = ($languageId==3) ? $sellerproduct['lastname'] : $sellerproduct['firstname'] ;
			  $sellers[] = array(
				'username'   => $sellerName,
				'seller_id'   => $sellerproduct['seller_id'],
				'numprod'     => $sellerproduct['numprod'],
				'name'     => $sellerproduct['name'],
				'reviews'     => $reviews
				
			);
			   
			  
			}
			
			
			
			
			
			$data['orders'][] = array(

			'sellers' => $sellers,
			
			
				'order_id'   => $result['order_id'],
				'name'       => $result['firstname'] . ' ' . $result['lastname'],
				'status'     => $result['status'],
				'order_status_id'     => $result['order_status_id'],
				'date_added' => date($this->language->get('date_format_short'), strtotime($result['date_added'])),
				'products'   => ($product_total + $voucher_total),
				'total'      => $this->currency->format($Final_Total, $result['currency_code'], $result['currency_value']),
				'href'       => $this->url->link('account/order/info', 'order_id=' . $result['order_id'], 'SSL'),
				'reorder'       => $this->url->link('account/order/reorder', 'order_id=' . $result['order_id'], 'SSL')
			);
		}

		$pagination = new Pagination();
		$pagination->total = $order_total;
		$pagination->page = $page;
		$pagination->limit = 10;
		$pagination->url = $this->url->link('account/order', 'page={page}', 'SSL');

		$data['pagination'] = $pagination->render();

		$data['results'] = sprintf($this->language->get('text_pagination'), ($order_total) ? (($page - 1) * 10) + 1 : 0, ((($page - 1) * 10) > ($order_total - 10)) ? $order_total : ((($page - 1) * 10) + 10), $order_total, ceil($order_total / 10));

		$data['continue'] = $this->url->link('account/account', '', 'SSL');

		$data['column_left'] = $this->load->controller('common/column_left');
		$data['column_right'] = $this->load->controller('common/column_right');
		$data['content_top'] = $this->load->controller('common/content_top');
		$data['content_bottom'] = $this->load->controller('common/content_bottom');
		$data['footer'] = $this->load->controller('common/footer');
		$data['header'] = $this->load->controller('common/header');

		if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/template/account/order_list.tpl')) {
			$this->response->setOutput($this->load->view($this->config->get('config_template') . '/template/account/order_list.tpl', $data));
		} else {
			$this->response->setOutput($this->load->view('default/template/account/order_list.tpl', $data));
		}
	}

	public function info() {
		$this->load->language('account/order');

		if (isset($this->request->get['order_id'])) {
			$order_id = $this->request->get['order_id'];
		} else {
			$order_id = 0;
		}

		if (!$this->customer->isLogged()) {
			$this->session->data['redirect'] = $this->url->link('account/order/info', 'order_id=' . $order_id, 'SSL');

			$this->response->redirect($this->url->link('account/login', '', 'SSL'));
		}

		$this->load->model('account/order');

		$order_info = $this->model_account_order->getOrder($order_id);

		if ($order_info) {
			$this->document->setTitle($this->language->get('text_order'));

			$data['breadcrumbs'] = array();

			$data['breadcrumbs'][] = array(
				'text' => $this->language->get('text_home'),
				'href' => $this->url->link('common/home')
			);

			$data['breadcrumbs'][] = array(
				'text' => $this->language->get('text_account'),
				'href' => $this->url->link('account/account', '', 'SSL')
			);

			$url = '';

			if (isset($this->request->get['page'])) {
				$url .= '&page=' . $this->request->get['page'];
			}

			$data['breadcrumbs'][] = array(
				'text' => $this->language->get('heading_title'),
				'href' => $this->url->link('account/order', $url, 'SSL')
			);

			$data['breadcrumbs'][] = array(
				'text' => $this->language->get('text_order'),
				'href' => $this->url->link('account/order/info', 'order_id=' . $this->request->get['order_id'] . $url, 'SSL')
			);

			$data['heading_title'] = $this->language->get('text_order');

			$data['text_order_detail'] = $this->language->get('text_order_detail');
			$data['text_invoice_no'] = $this->language->get('text_invoice_no');
			$data['text_order_id'] = $this->language->get('text_order_id');
			$data['text_date_added'] = $this->language->get('text_date_added');
			$data['text_shipping_method'] = $this->language->get('text_shipping_method');
			$data['text_shipping_address'] = $this->language->get('text_shipping_address');
			$data['text_payment_method'] = $this->language->get('text_payment_method');
			$data['text_payment_address'] = $this->language->get('text_payment_address');
			$data['text_history'] = $this->language->get('text_history');
			$data['text_comment'] = $this->language->get('text_comment');
            $data['text_delivery_time'] = $this->language->get('text_delivery_time');
			$data['column_name'] = $this->language->get('column_name');
			$data['column_model'] = $this->language->get('column_model');
			$data['column_quantity'] = $this->language->get('column_quantity');
			$data['column_price'] = $this->language->get('column_price');
			$data['column_total'] = $this->language->get('column_total');
			$data['column_action'] = $this->language->get('column_action');
			$data['column_date_added'] = $this->language->get('column_date_added');
			$data['column_status'] = $this->language->get('column_status');
			$data['column_comment'] = $this->language->get('column_comment');

			$data['button_reorder'] = $this->language->get('button_reorder');
			$data['button_return'] = $this->language->get('button_return');
			$data['button_continue'] = $this->language->get('button_continue');
            
            $data['text_store'] = $this->language->get('text_store');
            $data['text_product'] = $this->language->get('text_product');
            $data['text_cart'] = $this->language->get('text_cart');

			if (isset($this->session->data['error'])) {
				$data['error_warning'] = $this->session->data['error'];

				unset($this->session->data['error']);
			} else {
				$data['error_warning'] = '';
			}

			if (isset($this->session->data['success'])) {
				$data['success'] = $this->session->data['success'];

				unset($this->session->data['success']);
			} else {
				$data['success'] = '';
			}

			if ($order_info['invoice_no']) {
				$data['invoice_no'] = $order_info['invoice_prefix'] . $order_info['invoice_no'];
			} else {
				$data['invoice_no'] = '';
			}

			$data['order_id'] = $this->request->get['order_id'];
			$data['date_added'] = date($this->language->get('date_format_short'), strtotime($order_info['date_added']));

			if ($order_info['payment_address_format']) {
				$format = $order_info['payment_address_format'];
			} else {
				$format = '{firstname} {lastname}' . "\n" . '{company}' . "\n" . '{address_1}' . "\n" . '{address_2}' . "\n" . '{city} {postcode}' . "\n" . '{zone}' . "\n" . '{country}';
			}

			$find = array(
				'{firstname}',
				'{lastname}',
				'{company}',
				'{address_1}',
				'{address_2}',
				'{city}',
				'{postcode}',
				'{zone}',
				'{zone_code}',
				'{country}'
			);

			$replace = array(
				'firstname' => $order_info['payment_firstname'],
				'lastname'  => $order_info['payment_lastname'],
				'company'   => $order_info['payment_company'],
				'address_1' => $order_info['payment_address_1'],
				'address_2' => $order_info['payment_address_2'],
				'city'      => $order_info['payment_city'],
				'postcode'  => $order_info['payment_postcode'],
				'zone'      => $order_info['payment_zone'],
				'zone_code' => $order_info['payment_zone_code'],
				'country'   => $order_info['payment_country']
			);

			$data['payment_address'] = str_replace(array("\r\n", "\r", "\n"), '<br />', preg_replace(array("/\s\s+/", "/\r\r+/", "/\n\n+/"), '<br />', trim(str_replace($find, $replace, $format))));

			$data['payment_method'] = $order_info['payment_method'];

			if ($order_info['shipping_address_format']) {
				$format = $order_info['shipping_address_format'];
			} else {
				$format = '{firstname} {lastname}' . "\n" . '{company}' . "\n" . '{address_1}' . "\n" . '{address_2}' . "\n" . '{city} {postcode}' . "\n" . '{zone}' . "\n" . '{country}';
			}

			$find = array(
				'{firstname}',
				'{lastname}',
				'{company}',
				'{address_1}',
				'{address_2}',
				'{city}',
				'{postcode}',
				'{zone}',
				'{zone_code}',
				'{country}'
			);

			$replace = array(
				'firstname' => $order_info['shipping_firstname'],
				'lastname'  => $order_info['shipping_lastname'],
				'company'   => $order_info['shipping_company'],
				'address_1' => $order_info['shipping_address_1'],
				'address_2' => $order_info['shipping_address_2'],
				'city'      => $order_info['shipping_city'],
				'postcode'  => $order_info['shipping_postcode'],
				'zone'      => $order_info['shipping_zone'],
				'zone_code' => $order_info['shipping_zone_code'],
				'country'   => $order_info['shipping_country']
			);

			$data['shipping_address'] = str_replace(array("\r\n", "\r", "\n"), '<br />', preg_replace(array("/\s\s+/", "/\r\r+/", "/\n\n+/"), '<br />', trim(str_replace($find, $replace, $format))));

			$data['shipping_method'] = $order_info['shipping_method'];

			$this->load->model('catalog/product');
			$this->load->model('tool/upload');

			// Products
			$data['products'] = array();

			$products = $this->model_account_order->getOrderProducts($this->request->get['order_id']);


			$data['text_seller'] = $this->language->get('text_seller');
			$data['text_rate'] = $this->language->get('text_rate');
			$data['text_status'] = $this->language->get('text_status');
			$data['text_products'] = $this->language->get('text_products');
			$data['text_review'] = $this->language->get('text_review');
				$data['text_review'] = $this->language->get('text_review');
			
			
			foreach ($products as $product) {
			 	
                
                $languageId = (int)$this->config->get('config_language_id');
                
                $name = ($languageId==3) ? $product['lastname'] : $product['firstname'] ;
                $product['firstname'] = $name; 
              
				$option_data = array();

				$options = $this->model_account_order->getOrderOptions($this->request->get['order_id'], $product['order_product_id']);

				foreach ($options as $option) {
					if ($option['type'] != 'file') {
						$value = $option['value'];
					} else {
						$upload_info = $this->model_tool_upload->getUploadByCode($option['value']);

						if ($upload_info) {
							$value = $upload_info['name'];
						} else {
							$value = '';
						}
					}

					$option_data[] = array(
						'name'  => $option['name'],
						'value' => (utf8_strlen($value) > 20 ? utf8_substr($value, 0, 20) . '..' : $value)
					);
				}

				$product_info = $this->model_catalog_product->getProduct($product['product_id']);

				if ($product_info) {
					$reorder = $this->url->link('account/order/reorder', 'order_id=' . $order_id . '&order_product_id=' . $product['order_product_id']. '&seller_id=' . $product['seller_id'], 'SSL');
				} else {
					$reorder = '';
				}
               $orderProductValidation = $this->validateOrderProduct($product['product_id'],$product['seller_id']);

				$data['products'][] = array(
					'name'     => $product['name'],
					'model'    => $product['model'],

			
			'seller'    => $product['firstname'],
			'sellerstatus'    => $product['statusname'],
			
					'option'   => $option_data,
					'quantity' => $product['quantity'],
					'price'    => $this->currency->format($product['price'] + ($this->config->get('config_tax') ? $product['tax'] : 0), $order_info['currency_code'], $order_info['currency_value']),
					'total'    => $this->currency->format($product['total'] + ($this->config->get('config_tax') ? ($product['tax'] * $product['quantity']) : 0), $order_info['currency_code'], $order_info['currency_value']),
                    'storeExist'  => $orderProductValidation['storeExist'],
                    'storeSame'  => $orderProductValidation['storeSame'],
                    'productExist'  => $orderProductValidation['productExist'],
                    'seller_req'  => $orderProductValidation['seller'],
                    'reorder'  => $reorder,
					'return'   => $this->url->link('account/return/add', 'order_id=' . $order_info['order_id'] . '&product_id=' . $product['product_id'], 'SSL')
				);
			}

           $data['cartProductsCount'] = $this->cart->countProducts();

			// Voucher
			$data['vouchers'] = array();

			$vouchers = $this->model_account_order->getOrderVouchers($this->request->get['order_id']);

			foreach ($vouchers as $voucher) {
				$data['vouchers'][] = array(
					'description' => $voucher['description'],
					'amount'      => $this->currency->format($voucher['amount'], $order_info['currency_code'], $order_info['currency_value'])
				);
			}

			// Totals
			$data['totals'] = array();

			$totals = $this->model_account_order->getOrderTotals($this->request->get['order_id']);

            //------------
            //add new code 
			$order_id = $this->request->get['order_id'];
					
			$deliveryChargesArr = $this->db->query("
				select oc_sellers.delivery_charges, oc_sellers.seller_id, oc_sellers.firstname, oc_sellers.lastname 
				from oc_order_seller_shipping_fee 
				join oc_sellers on oc_sellers.seller_id = oc_order_seller_shipping_fee.seller_id 
				where oc_order_seller_shipping_fee.order_id = '$order_id'")->rows;

				//print "<pre>";print_r($totals);
			//print "<pre>";print_r($deliveryChargesArr);
			
			//echo date("Y-m-d");	
			$date_added = date("Y-m-d", strtotime($order_info['date_added']));				
			$date = '2018-11-02';
	
			$beforDate = 0;
			$afterDate = 0;
			if($order_info['date_added'] != '0000-00-00 00:00:00'){
				if($date_added <= $date){
					$beforDate = 1;					
				} else {
					$afterDate = 1;				
				}				
			} else {
				$beforDate = 1;	
			}
			
			foreach ($totals as $total) {
				
				if( $total['code'] == 'vat' ){
					$deliveryCharges = 0;
					foreach( $deliveryChargesArr as $deliveryChargesRow ){
						
						$deliveryCharges = $deliveryChargesRow['delivery_charges'];
                        $name = ($languageId==3) ? $deliveryChargesRow['lastname'] : $deliveryChargesRow['firstname'] ;
						$data['totals'][] = array(
								'title' => $name.' '. $this->language->get('text_shipping_fee'),
								'text'  => $this->currency->format( $deliveryCharges ),
						);
						//$total['value'] += $deliveryCharges;
					}
					
				}
				
				if($beforDate == 1){
					if( $total['code'] == 'total' ){
						$deliveryCharges = 0;
						foreach( $deliveryChargesArr as $deliveryChargesRow ){
							
							$deliveryCharges = $deliveryChargesRow['delivery_charges'];
							
							$total['value'] += $deliveryCharges;
						}
						
					}
					
				}
				
				
				$data['totals'][] = array(
					'title' => $total['title'],
					'text'  => $this->currency->format($total['value'], $order_info['currency_code'], $order_info['currency_value']),
				);
			}

			$data['comment'] = nl2br($order_info['comment']);

			// History
			$data['histories'] = array();

			$results = $this->model_account_order->getOrderHistories($this->request->get['order_id']);

			foreach ($results as $result) {
				$data['histories'][] = array(
					'date_added' => date($this->language->get('date_format_short'), strtotime($result['date_added'])),
					'status'     => $result['status'],
					'comment'    => $result['notify'] ? nl2br($result['comment']) : ''
				);
			}

			$data['continue'] = $this->url->link('account/order', '', 'SSL');

			$data['column_left'] = $this->load->controller('common/column_left');
			$data['column_right'] = $this->load->controller('common/column_right');
			$data['content_top'] = $this->load->controller('common/content_top');
			$data['content_bottom'] = $this->load->controller('common/content_bottom');
			$data['footer'] = $this->load->controller('common/footer');
			$data['header'] = $this->load->controller('common/header');

			if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/template/account/order_info.tpl')) {
				$this->response->setOutput($this->load->view($this->config->get('config_template') . '/template/account/order_info.tpl', $data));
			} else {
				$this->response->setOutput($this->load->view('default/template/account/order_info.tpl', $data));
			}
		} else {
			$this->document->setTitle($this->language->get('text_order'));

			$data['heading_title'] = $this->language->get('text_order');

			$data['text_error'] = $this->language->get('text_error');

			$data['button_continue'] = $this->language->get('button_continue');

			$data['breadcrumbs'] = array();

			$data['breadcrumbs'][] = array(
				'text' => $this->language->get('text_home'),
				'href' => $this->url->link('common/home')
			);

			$data['breadcrumbs'][] = array(
				'text' => $this->language->get('text_account'),
				'href' => $this->url->link('account/account', '', 'SSL')
			);

			$data['breadcrumbs'][] = array(
				'text' => $this->language->get('heading_title'),
				'href' => $this->url->link('account/order', '', 'SSL')
			);

			$data['breadcrumbs'][] = array(
				'text' => $this->language->get('text_order'),
				'href' => $this->url->link('account/order/info', 'order_id=' . $order_id, 'SSL')
			);

			$data['continue'] = $this->url->link('account/order', '', 'SSL');

			$data['column_left'] = $this->load->controller('common/column_left');
			$data['column_right'] = $this->load->controller('common/column_right');
			$data['content_top'] = $this->load->controller('common/content_top');
			$data['content_bottom'] = $this->load->controller('common/content_bottom');
			$data['footer'] = $this->load->controller('common/footer');
			$data['header'] = $this->load->controller('common/header');

			if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/template/error/not_found.tpl')) {
				$this->response->setOutput($this->load->view($this->config->get('config_template') . '/template/error/not_found.tpl', $data));
			} else {
				$this->response->setOutput($this->load->view('default/template/error/not_found.tpl', $data));
			}
		}
	}

	public function reorder() {
		$this->load->language('account/order');

		if (isset($this->request->get['order_id'])) {
			$order_id = $this->request->get['order_id'];
		} else {
			$order_id = 0;
		}

		$this->load->model('account/order');

		$order_info = $this->model_account_order->getOrder($order_id);

		if ($order_info) {
			if (isset($this->request->get['order_product_id'])) {
				$order_product_id = $this->request->get['order_product_id'];
			} else {
				$order_product_id = 0;
			}

            if (isset($this->request->get['seller_id'])) {
                $seller_id = $this->request->get['seller_id'];
            } else {
                $seller_id = 0;
            }

            $currentSeller =  (isset($_SESSION['storeID'])) ? (int)$_SESSION['storeID'] : 0;

			$order_product_info = $this->model_account_order->getOrderProduct($order_id, $order_product_id);

			if ($order_product_info) {
				$this->load->model('catalog/product');

				$product_info = $this->model_catalog_product->getProduct($order_product_info['product_id']);

				if ($product_info) {
					$option_data = array();

					$order_options = $this->model_account_order->getOrderOptions($order_product_info['order_id'], $order_product_id);

					foreach ($order_options as $order_option) {
						if ($order_option['type'] == 'select' || $order_option['type'] == 'radio' || $order_option['type'] == 'image') {
							$option_data[$order_option['product_option_id']] = $order_option['product_option_value_id'];
						} elseif ($order_option['type'] == 'checkbox') {
							$option_data[$order_option['product_option_id']][] = $order_option['product_option_value_id'];
						} elseif ($order_option['type'] == 'text' || $order_option['type'] == 'textarea' || $order_option['type'] == 'date' || $order_option['type'] == 'datetime' || $order_option['type'] == 'time') {
							$option_data[$order_option['product_option_id']] = $order_option['value'];
						} elseif ($order_option['type'] == 'file') {
							$option_data[$order_option['product_option_id']] = $this->encryption->encrypt($order_option['value']);
						}
					}


   if($currentSeller != $seller_id ){
       $this->cart->clear();

       //Unset and Set seller in cookies
       unsetCareebCookie('seller');
       setCareebCookie('seller', $seller_id,false);
       $_SESSION['storeID'] = $seller_id;

   }
					$this->cart->add($order_product_info['product_id'], $order_product_info['quantity'], $option_data,0,$seller_id);

					$this->session->data['success'] = sprintf($this->language->get('text_success'), $this->url->link('product/product', 'product_id=' . $product_info['product_id']), $product_info['name'], $this->url->link('checkout/cart'));

					unset($this->session->data['shipping_method']);
					unset($this->session->data['shipping_methods']);
					unset($this->session->data['payment_method']);
					unset($this->session->data['payment_methods']);
				} else {
					$this->session->data['error'] = sprintf($this->language->get('error_reorder'), $order_product_info['name']);
				}
			}
		}

		$this->response->redirect($this->url->link('account/order/info', 'order_id=' . $order_id));
	}

    public function validateOrderProduct($product_id,$seller_req){

        $this->load->model('store/store');
        $this->load->model('catalog/product');
        $this->load->model('saccount/seller');
        $this->load->model('tool/image');



        $data = array();
        $data['storeExist'] = false; $data['productExist'] = false; $data['storeSame'] = false;

         if (isset($this->session->data['user_location'])) {

            $user_location = $this->session->data['user_location'];
            //$radiusKm = RADIUS; //5;
			$radiusKm = $this->config->get('config_radius');

            $lat = $user_location['u_lat'];
            $lng = $user_location['u_lng'];


            $storesArray = $this->model_store_store->getSellerByRadius($radiusKm, $lat, $lng);
        }else
        {
            $area_id = 0;
            $area_id = $this->session->data['area']['area_id'];
            $storesArray = $this->model_store_store->getStoresByAreaId($area_id);
        }

        $currentSeller =  (isset($_SESSION['storeID'])) ? (int)$_SESSION['storeID'] : 1;

        $data['storeSame'] = ($currentSeller == $seller_req) ? true : false;

        //If Stores Exist
        if($storesArray){
            foreach($storesArray as $store){
                $storesId[] = $store['seller_id'];
            }
            if(in_array($seller_req,$storesId)){
                $data['storeExist'] = true;

                $data['seller'] = $this->model_saccount_seller->getSeller($seller_req);

                $sellerProduct = $this->model_catalog_product->getProductBySellerIdAPI($product_id,$seller_req,1);
                $data['productExist'] = ($sellerProduct) ? true : false;
            }

        }

        return $data;
    }
}