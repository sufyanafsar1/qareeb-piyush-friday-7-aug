<?php 
class ControllerBossthemesBossAdd extends Controller {
	public function cart() {

		$this->load->model('catalog/product');
		$data['text_seller'] = $this->language->get('text_seller');
		if (isset($this->request->post['seller_id'])) {
			$seller_id = (int)$this->request->post['seller_id'];
			if($seller_id == 0){
				$seller_id = $this->model_catalog_product->getDefaultSeller($this->request->post['product_id']);
			}
		} else {
			$seller_id = $this->model_catalog_product->getDefaultSeller($this->request->post['product_id']);
		}
			
		require_once 'system/Mobile_Detect.php';
		$this->load->language('checkout/cart');
		$this->load->language('bossthemes/boss_add');


		$json = array();

		if (isset($this->request->post['product_id'])) {
			$product_id = (int)$this->request->post['product_id'];
		} else {
			$product_id = 0;
		}
 	      if (isset($this->request->post['seller_id'])) {
			$seller_id = (int)$this->request->post['seller_id'];
                $query1 = $this->db->query("SELECT * from oc_sellers where seller_id= '$seller_id'");
		 $customer = $query1->row;
		 
		 
		} else {
			$seller_id = 1;
		}

		$this->load->model('catalog/product');


		//$product_info = $this->model_catalog_product->getProduct($product_id,0);

            $product_info = $this->model_catalog_product->getProductBySellerId($product_id,$seller_id);

		if ($product_info) {
			if (isset($this->request->post['quantity']) && ((int)$this->request->post['quantity'] >= $product_info['minimum'])) {
				$quantity = (int)$this->request->post['quantity'];
			} else {
				$quantity = 1;
			}
			
			if (isset($this->request->post['option'])) {
				$option = array_filter($this->request->post['option']);
			} else {
				$option = array();
			}

			
				$product_options = $this->model_catalog_product->getSProductOptions($this->request->post['product_id'],$seller_id);
			
			

			foreach ($product_options as $product_option) {
				if ($product_option['required'] && empty($option[$product_option['product_option_id']])) {
					$json['error']['option'][$product_option['product_option_id']] = sprintf($this->language->get('error_required'), $product_option['name']);
				}
			}

			if (isset($this->request->post['recurring_id'])) {
				$recurring_id = $this->request->post['recurring_id'];
			} else {
				$recurring_id = 0;
			}

			$recurrings = $this->model_catalog_product->getProfiles($product_info['product_id']);

			if ($recurrings) {
				$recurring_ids = array();

				foreach ($recurrings as $recurring) {
					$recurring_ids[] = $recurring['recurring_id'];
				}

				if (!in_array($recurring_id, $recurring_ids)) {
					$json['error']['recurring'] = $this->language->get('error_recurring_required');
				}
			}

			if (!$json) {
			   
				
				$this->cart->add($this->request->post['product_id'], $quantity, $option,$recurring_id,$seller_id);
			
				
				$this->load->model('tool/image'); 
				//$image = $this->model_tool_image->resize($product_info['image'], $this->config->get('config_image_cart_width'), $this->config->get('config_image_cart_height'));
				
				$image = $this->model_tool_image->resize($product_info['image'],40,40);
        
				$json['title'] = $this->language->get('text_title_cart');
				$json['thumb'] = sprintf($this->language->get('text_thumb'), $image);
               
				$json['success'] = sprintf($this->language->get('text_success_cart'), $this->url->link('product/product', 'product_id=' . $this->request->post['product_id']), $product_info['name'], $this->url->link('checkout/cart'));

				unset($this->session->data['shipping_method']);
				unset($this->session->data['shipping_methods']);
				unset($this->session->data['payment_method']);
				unset($this->session->data['payment_methods']);

				// Totals
				$this->load->model('extension/extension');
				$json['next_day_delivery_active'] = $product_info['next_day_delivery_active'];

				$total_data = array();
				$total = 0;
				$taxes = $this->cart->getTaxes();

				// Display prices
				if (($this->config->get('config_customer_price') && $this->customer->isLogged()) || !$this->config->get('config_customer_price')) {
					$sort_order = array();

					$results = $this->model_extension_extension->getExtensions('total');

					foreach ($results as $key => $value) {
						$sort_order[$key] = $this->config->get($value['code'] . '_sort_order');
					}

					array_multisort($sort_order, SORT_ASC, $results);

				/**
                 * 	foreach ($results as $result) {
                 * 						if ($this->config->get($result['code'] . '_status')) {
                 * 							$this->load->model('total/' . $result['code']);
                
                 * 							//$this->{'model_total_' . $result['code']}->getTotal($total_data, $total, $taxes);
                 *                             
                 * 						}
                 * 					}
                 */
                 
            foreach ($results as $result) {
			   if($result['code']=="sub_total" || $result['code']=="coupon")
               {               
              
    				if ($this->config->get($result['code'] . '_status')) {
    					$this->load->model('total/' . $result['code']);
    			
                        $this->{'model_total_' . $result['code']}->getTotalV2($total_data, $total, $taxes,$seller_id);
    				}
                }
			}

					$sort_order = array();

					foreach ($total_data as $key => $value) {
						$sort_order[$key] = $value['sort_order'];
					}

					array_multisort($sort_order, SORT_ASC, $total_data);
				}
			
                $json['cartId']  ="";
                foreach ($this->cart->getProducts() as $product) {
                    if($product['product_id'] == $this->request->post['product_id'])
                    {
                        $json['cartId']  = $product['cart_id'];
                        break;
                    }
                    
                }
                $json['lang'] = $_SESSION['default']['language'];
		/* $json['total'] = '<img src="catalog/view/theme/careeb/image/cart-icn.png"/> '.sprintf($this->language->get('text_items'), $this->cart->countProducts() + (isset($this->session->data['vouchers']) ? count($this->session->data['vouchers']) : 0), $this->currency->format($total)); */
		
		$detect = new Mobile_Detect;		
		// Any mobile device (phones or tablets).
		if ( $detect->isMobile() ) {
			$json['total'] = '<img src="catalog/view/theme/careeb/img/cart.svg"/> <span id="cartTotal">'.$this->cart->countProducts()."</span>";
		} else {
			$json['total'] = '<img src="catalog/view/theme/careeb/img/cart-icon.svg"/> <span id="cartTotal">'.$this->cart->countProducts()."</span>";
		}
		
		

                $languageId  = $this->config->get('config_language_id');
                                
                if( $customer ){
                  //date_default_timezone_get();
                  $now = strtotime("now");
                  $interval = $customer["delivery_timegap"] * 60;
                  $package = $customer["package_ready"] * 60;
                  $nowdata = dayData("now");
                  if ( $customer[strtolower(substr($nowdata["half"], 0, 3)) . "_start_time"] != "00:00:00" && $customer[strtolower(substr($nowdata["half"], 0, 3)) . "_end_time"] != "00:00:00" ) {
                    $nowstarttime = strtotime($nowdata["date"]. " " . $customer[strtolower(substr($nowdata["half"], 0, 3)) . "_start_time"]) + $interval;
                    $nowendtime = strtotime($nowdata["date"]. " " . $customer[strtolower(substr($nowdata["half"], 0, 3)) . "_end_time"]);
                    $nownextdelivery = ""; $j = 0;
                    for( $i = $nowstarttime; $i < $nowendtime; $i += $interval) {
                      $nntt = $i + $interval;
                      if( $i < $now ) {

                      } else {
                        if ( $j == 0 ) {
                          $nextTime = $now + $package;
                          if ($i < $nextTime) {

                          } else {
                            $nownextdelivery =  arabicTime(strtotime(date("h:i a", $i)),$languageId);
                            $j++;
                          }
                        } else {
                          $j++;
                        }
                      }
                    }
                  }

                  $fdata = dayData(" +1 day");
                  if ( $customer[strtolower(substr($fdata["half"], 0, 3)) . "_start_time"] != "00:00:00" && $customer[strtolower(substr($fdata["half"], 0, 3)) . "_end_time"] != "00:00:00" ) {
                    $fstarttime = strtotime($fdata["date"]. " " . $customer[strtolower(substr($fdata["half"], 0, 3)) . "_start_time"]) + $interval;
                    $fendtime = strtotime($fdata["date"]. " " . $customer[strtolower(substr($fdata["half"], 0, 3)) . "_end_time"]);
                    $fnextdelivery = ""; $k = 0;
                    for( $i = $fstarttime; $i < $fendtime; $i += $interval) {
                      $nntt = $i + $interval;
                      if( $i < $now ) {

                      } else {
                        if ( $k == 0 ) {
                          $nextTime = $now + $package;
                          if ($i < $nextTime) {

                          } else {
                            $fnextdelivery =  arabicTime(strtotime(date("h:i a", $i)),$languageId);
                            $k++;
                          }
                        } else {
                          $k++;
                        }
                      }
                    }
                  }

                  $sdata = dayData(" +2 day");
                  if ( $customer[strtolower(substr($sdata["half"], 0, 3)) . "_start_time"] != "00:00:00" && $customer[strtolower(substr($sdata["half"], 0, 3)) . "_end_time"] != "00:00:00" ) {
                    $sstarttime = strtotime($sdata["date"]. " " . $customer[strtolower(substr($sdata["half"], 0, 3)) . "_start_time"]) + $interval;
                    $sendtime = strtotime($sdata["date"]. " " . $customer[strtolower(substr($sdata["half"], 0, 3)) . "_end_time"]);
                    $snextdelivery = ""; $l = 0;
                    for( $i = $sstarttime; $i < $sendtime; $i += $interval) {
                      $nntt = $i + $interval;
                      if( $i < $now ) {

                      } else {
                        if ( $l == 0 ) {
                          $nextTime = $now + $package;
                          if ($i < $nextTime) {

                          } else {
                            $snextdelivery = arabicHalfDay($sdata["half"],$languageId) . " at " . arabicTime(strtotime(date("h:i a", $i)),$languageId);
                            $l++;
                          }
                        } else {
                          $l++;
                        }
                      }
                    }
                  }

                  $tdata = dayData(" +3 day");
                  if ( $customer[strtolower(substr($tdata["half"], 0, 3)) . "_start_time"] != "00:00:00" && $customer[strtolower(substr($tdata["half"], 0, 3)) . "_end_time"] != "00:00:00" ) {
                    $tstarttime = strtotime($tdata["date"]. " " . $customer[strtolower(substr($tdata["half"], 0, 3)) . "_start_time"]) + $interval;
                    $tendtime = strtotime($tdata["date"]. " " . $customer[strtolower(substr($tdata["half"], 0, 3)) . "_end_time"]);
                    $tnextdelivery = ""; $m = 0;
                    for( $i = $tstarttime; $i < $tendtime; $i += $interval) {
                      $nntt = $i + $interval;
                      if( $i < $now ) {

                      } else {
                        if ( $m == 0 ) {
                          $nextTime = $now + $package;
                          if ($i < $nextTime) {

                          } else {
                            $tnextdelivery =     arabicHalfDay($tdata["half"],$languageId) . " at " . arabicTime(strtotime(date("h:i a", $i)),$languageId);
                            $m++;
                          }
                        } else {
                          $m++;
                        }
                      }
                    }
                  }

                  $ffdata = dayData(" +4 day");
                  if ( $customer[strtolower(substr($ffdata["half"], 0, 3)) . "_start_time"] != "00:00:00" && $customer[strtolower(substr($ffdata["half"], 0, 3)) . "_end_time"] != "00:00:00" ) {
                    $ffstarttime = strtotime($ffdata["date"]. " " . $customer[strtolower(substr($ffdata["half"], 0, 3)) . "_start_time"]) + $interval;
                    $ffendtime = strtotime($ffdata["date"]. " " . $customer[strtolower(substr($ffdata["half"], 0, 3)) . "_end_time"]);
                    $ffnextdelivery = ""; $n = 0;
                    for( $i = $ffstarttime; $i < $ffendtime; $i += $interval) {
                      $nntt = $i + $interval;
                      if( $i < $now ) {

                      } else {
                        if ( $n == 0 ) {
                          $nextTime = $now + $package;
                          if ($i < $nextTime) {

                          } else {
                            $ffnextdelivery = arabicHalfDay($ffdata["half"],$languageId)  . " at " . arabicTime(strtotime(date("h:i a", $i)),$languageId);
                            $n++;
                          }
                        } else {
                          $n++;
                        }
                      }
                    }
                  }

                  $fffdata = dayData(" +5 day");
                  if ( $customer[strtolower(substr($fffdata["half"], 0, 3)) . "_start_time"] != "00:00:00" && $customer[strtolower(substr($fffdata["half"], 0, 3)) . "_end_time"] != "00:00:00" ) {
                    $fffstarttime = strtotime($fffdata["date"]. " " . $customer[strtolower(substr($fffdata["half"], 0, 3)) . "_start_time"]) + $interval;
                    $fffendtime = strtotime($fffdata["date"]. " " . $customer[strtolower(substr($fffdata["half"], 0, 3)) . "_end_time"]);
                    $fffnextdelivery = ""; $o = 0;
                    for( $i = $fffstarttime; $i < $fffendtime; $i += $interval) {
                      $nntt = $i + $interval;
                      if( $i < $now ) {

                      } else {
                        if ( $o == 0 ) {
                          $nextTime = $now + $package;
                          if ($i < $nextTime) {

                          } else {
                            $fffnextdelivery = arabicHalfDay($fffdata["half"],$languageId)  . " at " . arabicTime(strtotime(date("h:i a", $i)),$languageId);
                            $o++;
                          }
                        } else {
                          $o++;
                        }
                      }
                    }
                  }

                  $ssdata = dayData(" +6 day");
                  if ( $customer[strtolower(substr($ssdata["half"], 0, 3)) . "_start_time"] != "00:00:00" && $customer[strtolower(substr($ssdata["half"], 0, 3)) . "_end_time"] != "00:00:00" ) {
                    $ssstarttime = strtotime($ssdata["date"]. " " . $customer[strtolower(substr($ssdata["half"], 0, 3)) . "_start_time"]) + $interval;
                    $ssendtime = strtotime($ssdata["date"]. " " . $customer[strtolower(substr($ssdata["half"], 0, 3)) . "_end_time"]);
                    $ssnextdelivery = ""; $p = 0;
                    for( $i = $ssstarttime; $i < $ssendtime; $i += $interval) {
                      $nntt = $i + $interval;
                      if( $i < $now ) {

                      } else {
                        if ( $p == 0 ) {
                          $nextTime = $now + $package;
                          if ($i < $nextTime) {

                          } else {
                            $ssnextdelivery = arabicHalfDay($ssdata["half"],$languageId) . " at " . arabicTime(strtotime(date("h:i a", $i)),$languageId);
                            $p++;
                          }
                        } else {
                          $p++;
                        }
                      }
                    }
                  }

                  if((isset($nownextdelivery)) && ($nownextdelivery != "" )){
                    $json['msg'] = $this->language->get('text_order_receive_today').$nownextdelivery.'.';
                  }elseif((isset($fnextdelivery)) && ($fnextdelivery != "" )){
                    $json['msg'] = $this->language->get('text_order_receive_tomorrow').$fnextdelivery.'.';
                  }elseif((isset($snextdelivery)) && ($snextdelivery != "" )){
                    $json['msg'] = $this->language->get('text_order_receive').$snextdelivery.'.';
                  }elseif((isset($tnextdelivery)) && ($tnextdelivery != "" )){
                    $json['msg'] = $this->language->get('text_order_receive').$tnextdelivery.'.';
                  }elseif((isset($ffnextdelivery)) && ($ffnextdelivery != "" )){ 
                    $json['msg'] = $this->language->get('text_order_receive').$ffnextdelivery.'.';
                  }elseif((isset($fffnextdelivery)) && ($fffnextdelivery != "" )){
                    $json['msg'] = $this->language->get('text_order_receive').$fffnextdelivery.'.';
                  }elseif((isset($ssnextdelivery)) && ($ssnextdelivery != "" )){ 
                    $json['msg'] = $this->language->get('text_order_receive').$ssnextdelivery.'.';
                  }else{
                    $json['msg'] = $this->language->get('text_no_delivery');
                  }
				  
				  $json['msg'] = ' ';
                }
                                
                                
                                
                 // $json['items'] = $this->language->get('text_items');
                //  $json['total'] =  $this->cart->countProducts() + (isset($this->session->data['vouchers']) ? count($this->session->data['vouchers']) : 0);
         			} else {
				
				$json['redirect'] = str_replace('&amp;', '&', $this->url->link('product/product', 'product_id=' . $this->request->post['product_id'].'&seller_id=' .$seller_id));
			
			}
		}

		$this->response->addHeader('Content-Type: application/json');
		$this->response->setOutput(json_encode($json));
	}
    
    public function checkWishlistOnLogin(){       
       
       // Get customer wishlist
     $this->load->model('account/wishlist');
	 $wishlistItems =   $this->model_account_wishlist->getWishlist();
    
    /**
 *  foreach($wishlistItems as $witem){  // set database items 
 *         
 *         $customerWishlist[] = $witem['product_id'];
 *       
 *      }
 */
   	
   	$existInList = true;
    
    
	if(isset($this->session->data['wishlist'])){

     foreach($this->session->data['wishlist'] as $item)
     {        
        //if (!in_array($item[0], $customerWishlist)) // if item not save in database but exists in session 
        //{            
         //  $this->model_account_wishlist->addWishlistCategory($item[0],$item[1]); //save item in database
        //}
        
        $totalItems = (int)$this->model_account_wishlist->getWishlistByProductAndSeller($item[0],$item[2]);
        
        if($totalItems == 0)
        {
             $this->model_account_wishlist->addWishlistCategory($item[0],$item[1],$item[2]); //save item in database
        }
     }
	}
     //if item exists in database but not in session
     foreach($wishlistItems as $witem)
     {   
     	$existInList = true;
		if(isset($this->session->data['wishlist'])){
			foreach ($this->session->data['wishlist'] as $key => $value) {
				if ($value[0] == $witem['product_id'] && $value[2] == $witem['seller_id'] ) {
					$existInList = false;
				}
			}
			
			if ($existInList) // if item not save in session but exists in database 
			{            
			   $this->session->data['wishlist'][] = array($witem['product_id'],$witem['category_name'],$witem['seller_id']);
			}
		}
                
     }
     
     
     
     
     //If wishlist is empty in session theme   remove the items from database
    // if (count($this->session->data['wishlist']) <= 0) {
//       
//            foreach($customerWishlist as $cWishlist)
//            {
//                $this->model_account_wishlist->deleteWishlist($cWishlist);
//            }
//		}
      
  }		
	
	public function wishlist() {
	  
      
		$this->load->language('account/wishlist');
		$this->load->language('bossthemes/boss_add');

		$json = array();

		if (!isset($this->session->data['wishlist'])) {
			$this->session->data['wishlist'] = array();
		}

		if (isset($this->request->post['product_id'])) {
			$product_id = $this->request->post['product_id'];
		} else {
			$product_id = 0;
		}

		$this->load->model('catalog/product');

		
				$product_info = $this->model_catalog_product->getSProduct($product_id,$seller_id);
			

		if ($product_info) {
		
			$json['title'] = $this->language->get('text_title_wishlist');
			
			if (!in_array($this->request->post['product_id'], $this->session->data['wishlist'])) {
				$this->session->data['wishlist'][] = (int)$this->request->post['product_id'];
				
				$this->load->model('tool/image'); 
				$image = $this->model_tool_image->resize($product_info['image'], $this->config->get('config_image_cart_width'), $this->config->get('config_image_cart_height'));
				
				$json['thumb'] = sprintf($this->language->get('text_thumb'), $image);

				if ($this->customer->isLogged()) {
				// Edit customers cart
				$this->load->model('account/wishlist');
				$this->model_account_wishlist->addWishlist($this->request->post['product_id']);
				$json['success'] = sprintf($this->language->get('text_success'), $this->url->link('product/product', 'product_id=' . (int)$this->request->post['product_id']), $product_info['name'], $this->url->link('account/wishlist'));
				$json['total'] = sprintf($this->language->get('text_wishlist'), $this->model_account_wishlist->getTotalWishlist());
			} else {
				if (!isset($this->session->data['wishlist'])) {
					$this->session->data['wishlist'] = array();
				}
				$this->session->data['wishlist'][] = $this->request->post['product_id'];
				$this->session->data['wishlist'] = array_unique($this->session->data['wishlist']);
				$json['success'] = sprintf($this->language->get('text_login'), $this->url->link('account/login', '', true), $this->url->link('account/register', '', true), $this->url->link('product/product', 'product_id=' . (int)$this->request->post['product_id']), $product_info['name'], $this->url->link('account/wishlist'));
				$json['total'] = sprintf($this->language->get('text_wishlist'), (isset($this->session->data['wishlist']) ? count($this->session->data['wishlist']) : 0));
			}
			} else {
				$json['info'] = sprintf($this->language->get('text_exists'), $this->url->link('product/product', 'product_id=' . (int)$this->request->post['product_id']), $product_info['name'], $this->url->link('account/wishlist'));
			}

			$json['total'] = sprintf($this->language->get('text_wishlist'), (isset($this->session->data['wishlist']) ? count($this->session->data['wishlist']) : 0));
		}

		$this->response->addHeader('Content-Type: application/json');
		$this->response->setOutput(json_encode($json));
	}

	public function wishlist_cat() {
	  
     
		$this->load->language('account/wishlist');
		$this->load->language('bossthemes/boss_add');

		$json = array();

		if (!isset($this->session->data['wishlist'])) {
			$this->session->data['wishlist'] = array();
		}

		if (isset($this->request->post['wishlist_product_id'])) {
			$product_id = $this->request->post['wishlist_product_id'];
		} else {
			$product_id = 0;
		}
        if (isset($this->request->post['wishlist_seller_id'])) {
			$seller_id = $this->request->post['wishlist_seller_id'];
		} else {
			$seller_id = 1;
		}

		$this->load->model('catalog/product');
		 $languageid =1;
		 if($_SESSION['default']['language'] == 'ar'){
		    $languageid = 3; 
		 }
		$product_info = $this->model_catalog_product->getProductes($product_id,$languageid);

		if ($product_info) {
		
			$json['title'] = $this->language->get('text_title_wishlist');
			$existInList = true;
			foreach ($this->session->data['wishlist'] as $key => $value) {
				if ($value[0] == $this->request->post['wishlist_product_id']  &&  $value[2] == $this->request->post['wishlist_seller_id']) {
					$existInList = false;
				}
			}
            
			if ($existInList) {
				if ($this->request->post['wishlist_catergory'] == 'custom') {
					$cat_name = $this->request->post['wishlist_new_category'] != ''? $this->request->post['wishlist_new_category'] : 'Main';
				}else{
					$cat_name = $this->request->post['wishlist_catergory'] != ''? $this->request->post['wishlist_catergory'] : 'Main';
				}
				
				
				$this->load->model('tool/image'); 
				$image = $this->model_tool_image->resize($product_info['image'], $this->config->get('config_image_cart_width'), $this->config->get('config_image_cart_height'));
				
				$json['thumb'] = sprintf($this->language->get('text_thumb'), $image);
				

				if ($this->customer->isLogged()) {
					// Edit customers cart
                 
					$this->load->model('account/wishlist');
					$this->model_account_wishlist->addWishlistCategory($this->request->post['wishlist_product_id'], $cat_name,$this->request->post['wishlist_seller_id']);
					$this->session->data['wishlist'][] = array($product_id,$cat_name,$seller_id);
                    $json['success'] = sprintf($this->language->get('text_success'), $this->url->link('product/product', 'product_id=' . $product_id), $product_info['name'], $this->url->link('account/wishlist'));
					$json['total'] = sprintf($this->language->get('text_wishlist'), $this->model_account_wishlist->getTotalWishlist());
				} else {
				    $this->session->data['wishlist'][] = array($product_id,$cat_name,$seller_id);
					$json['success'] = sprintf($this->language->get('text_login'), $this->url->link('account/login', '', true), $this->url->link('account/register', '', true), $this->url->link('product/product', 'product_id=' . $product_id), $product_info['name'], $this->url->link('account/wishlist'));
					$json['total'] = sprintf($this->language->get('text_wishlist'), (isset($this->session->data['wishlist']) ? count($this->session->data['wishlist']) : 0));
				}
			} else {
				$json['info'] = sprintf($this->language->get('text_exists'), $this->url->link('product/product', 'product_id=' . $product_id), $product_info['name'], $this->url->link('account/wishlist'));
			}

			$json['total'] = sprintf($this->language->get('text_wishlist'), (isset($this->session->data['wishlist']) ? count($this->session->data['wishlist']) : 0));
		}

		$this->response->addHeader('Content-Type: application/json');
		$this->response->setOutput(json_encode($json));
	}
	
	public function compare() {
		$this->load->language('product/compare');
		
		$this->load->language('bossthemes/boss_add');

		$json = array();

		if (!isset($this->session->data['compare'])) {
			$this->session->data['compare'] = array();
		}

		if (isset($this->request->post['product_id'])) {
			$product_id = $this->request->post['product_id'];
		} else {
			$product_id = 0;
		}

		$this->load->model('catalog/product');

		
				$product_info = $this->model_catalog_product->getSProduct($product_id,$seller_id);
			

		if ($product_info) {
			if (!in_array($this->request->post['product_id'], $this->session->data['compare'])) {
				if (count($this->session->data['compare']) >= 4) {
					array_shift($this->session->data['compare']);
				}

				$this->session->data['compare'][] = $this->request->post['product_id'];
			}
			
			$this->load->model('tool/image'); 
			$image = $this->model_tool_image->resize($product_info['image'], $this->config->get('config_image_cart_width'), $this->config->get('config_image_cart_height'));
      
			$json['title'] = $this->language->get('text_title_compare');
			$json['thumb'] = sprintf($this->language->get('text_thumb'), $image);

			$json['success'] = sprintf($this->language->get('text_success_compare'), $this->url->link('product/product', 'product_id=' . $this->request->post['product_id']), $product_info['name'], $this->url->link('product/compare'));

			$json['total'] = sprintf($this->language->get('text_compare'), (isset($this->session->data['compare']) ? count($this->session->data['compare']) : 0));
		}

		$this->response->addHeader('Content-Type: application/json');
		$this->response->setOutput(json_encode($json));
	}
}
?>