<?php
ob_start();
session_start();

//set timezone
date_default_timezone_set('Europe/London');

$dbhost = "localhost";
$dbname = "malik_salatty_new";
$dbusername = "malik_salatty";
$dbpassword = "Admin@123##";

try {

	//create PDO connection 
	$db = new PDO("mysql:host=$dbhost;dbname=$dbname",$dbusername,$dbpassword);
	$db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

} catch(PDOException $e) {
	//show error
    echo '<p class="bg-danger">'.$e->getMessage().'</p>';
    exit;
}
?>